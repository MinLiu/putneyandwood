﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruitful.PDF
{
    public class PDFTemplate
    {
        public string headerView;
        public string bodyView;
        public string footerView;
        public string pagerView; 
        public bool showHeaderEveryPage;
        public bool showFooterEveryPage;
    }
}
