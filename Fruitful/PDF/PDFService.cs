﻿using EvoPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fruitful.PDF
{

    public static class PDFService<Model>
    {

        public static byte[] GenerateOrder(Model model, ControllerContext controllerContext, PDFTemplate template)
        {
            return GenerateOrder(model, controllerContext, template.headerView, template.bodyView, template.footerView, template.pagerView, template.showHeaderEveryPage, template.showFooterEveryPage);
        }

        public static byte[] GenerateOrder(Model model, ControllerContext controllerContext, string headerView, string bodyView, string footerView, string pagerView, bool showHeaderEveryPage, bool showFooterEveryPage)
        {            
            var ViewData = new ViewDataDictionary();
            var TempData = new TempDataDictionary();

            ViewData.Model = model;

            string headerHtml = "";
            string bodyHtml = "";
            string footerHtml = "";
            string pagerHtml = "";

            if (!string.IsNullOrEmpty(headerView))
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult headerResult = ViewEngines.Engines.FindPartialView(controllerContext, headerView);
                    headerResult.View.Render(new ViewContext(controllerContext, headerResult.View, ViewData, TempData, sw), sw);
                    headerHtml = sw.GetStringBuilder().ToString();
                }
            }

            if (!string.IsNullOrEmpty(bodyView))
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult bodyResult = ViewEngines.Engines.FindPartialView(controllerContext, bodyView);
                    bodyResult.View.Render(new ViewContext(controllerContext, bodyResult.View, ViewData, TempData, sw), sw);
                    bodyHtml = sw.GetStringBuilder().ToString();
                }
            }

            if (!string.IsNullOrEmpty(footerView))
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult footerResult = ViewEngines.Engines.FindPartialView(controllerContext, footerView);
                    footerResult.View.Render(new ViewContext(controllerContext, footerResult.View, ViewData, TempData, sw), sw);
                    footerHtml = sw.GetStringBuilder().ToString();
                }
            }

            if (!string.IsNullOrEmpty(pagerView))
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult pagerResult = ViewEngines.Engines.FindPartialView(controllerContext, pagerView);
                    pagerResult.View.Render(new ViewContext(controllerContext, pagerResult.View, ViewData, TempData, sw), sw);
                    pagerHtml = sw.GetStringBuilder().ToString();
                }
            }

            // Use the current page URL as base URL
            string baseUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            var generator = new EvoPDFGenerator();
            generator.showHeaderEveryPage = showHeaderEveryPage;
            generator.showFooterEveryPage = showFooterEveryPage;

            // Convert the current page HTML string a PDF document in a memory buffer
            byte[] outPdfBuffer = generator.GeneratePDF(headerHtml, bodyHtml, footerHtml, pagerHtml, baseUrl);
            
            return outPdfBuffer;
           
        }



        public static byte[] GeneratePages(Model model, ControllerContext controllerContext, PDFTemplate [] templates)
        {            
            Document mergeResultPdfDocument = new Document();
            mergeResultPdfDocument.LicenseKey = "f/Hi8OPj8OPh8Of+4PDj4f7h4v7p6enp8OA=";

            foreach (var temp in templates)
            {
                MemoryStream pdfStream = new MemoryStream();
                var pdfArray = GenerateOrder(model, controllerContext, temp);


                pdfStream.Write(pdfArray, 0, pdfArray.Length);
                pdfStream.Seek(0, System.IO.SeekOrigin.Begin);

                Document document = new Document(pdfStream);
                document.LicenseKey = "f/Hi8OPj8OPh8Of+4PDj4f7h4v7p6enp8OA=";
                mergeResultPdfDocument.AppendDocument(document);
            }

            return mergeResultPdfDocument.Save();            
        }


        public static Document GenerateDocument(Model model, ControllerContext controllerContext, PDFTemplate[] templates)
        {
            Document mergeResultPdfDocument = new Document();
            mergeResultPdfDocument.LicenseKey = "f/Hi8OPj8OPh8Of+4PDj4f7h4v7p6enp8OA=";

            foreach (var temp in templates)
            {
                MemoryStream pdfStream = new MemoryStream();
                var pdfArray = GenerateOrder(model, controllerContext, temp);


                pdfStream.Write(pdfArray, 0, pdfArray.Length);
                pdfStream.Seek(0, System.IO.SeekOrigin.Begin);

                Document document = new Document(pdfStream);
                document.LicenseKey = "f/Hi8OPj8OPh8Of+4PDj4f7h4v7p6enp8OA=";
                mergeResultPdfDocument.AppendDocument(document);
            }

            return mergeResultPdfDocument;
        }



        public static byte[] GenerateErrorPage()
        {
            string baseUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            return new EvoPDFGenerator().GeneratePDF(null, "<h1>Error: Can not generate PDF</h1>", null, null, baseUrl);
        }


        

    }


    public class EvoPDFGenerator
    {
        private HtmlToPdfConverter htmlToPdfConverter;
        public bool showHeaderEveryPage = false;
        public bool showFooterEveryPage = false;

        public EvoPDFGenerator() {
            htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = "f/Hi8OPj8OPh8Of+4PDj4f7h4v7p6enp8OA=";
            htmlToPdfConverter.PdfDocumentOptions.TableHeaderRepeatEnabled = true;
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 30;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 30;
            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 30;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 30;
        }


        public byte[] GeneratePDF(string headerHtml, string bodyHtml, string footerHtml, string pagerHtml, string baseUrl)
        {
            if(!string.IsNullOrEmpty(headerHtml)) 
            {
                htmlToPdfConverter.PdfDocumentOptions.ShowHeader = true;
                HtmlToPdfElement headerHtmlElement = new HtmlToPdfElement(headerHtml, baseUrl);
                //headerHtmlElement.FitHeight = true;
                headerHtmlElement.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                htmlToPdfConverter.PdfHeaderOptions.AddElement(headerHtmlElement);
            }

            //if (!string.IsNullOrEmpty(pagerHtml))
            //{
            //    htmlToPdfConverter.PdfDocumentOptions.ShowFooter = true;
            //    HtmlToPdfElement pagerHtmlElement = new HtmlToPdfElement(pagerHtml, baseUrl);

            //    //pagerHtmlElement.FitWidth = false;
            //    //pagerHtmlElement.FitHeight = true;
            //    //pagerHtmlElement.NavigationCompletedEvent += new NavigationCompletedDelegate(footerHtml_NavigationCompletedEvent);
            //    htmlToPdfConverter.PdfFooterOptions.AddElement(pagerHtmlElement);
            //}

            if (!string.IsNullOrEmpty(footerHtml))
            {
                htmlToPdfConverter.PdfDocumentOptions.ShowFooter = true;
                HtmlToPdfElement footerHtmlElement = new HtmlToPdfElement(footerHtml, baseUrl);
                //footerHtmlElement.FitHeight = true;
                footerHtmlElement.NavigationCompletedEvent += new NavigationCompletedDelegate(footerHtml_NavigationCompletedEvent);
                htmlToPdfConverter.PdfFooterOptions.AddElement(footerHtmlElement);
            }
            
            htmlToPdfConverter.BeforeRenderPdfPageEvent += htmlToPdfConverter_BeforeRenderPdfPageEvent;

            return htmlToPdfConverter.ConvertHtml(bodyHtml, baseUrl);

        }



        void headerHtml_NavigationCompletedEvent(NavigationCompletedParams eventParams)
        {
            // Get the header HTML width and height from event parameters
            float headerHtmlWidth = eventParams.HtmlContentWidthPt;
            float headerHtmlHeight = eventParams.HtmlContentHeightPt;

            // Calculate the header width from coverter settings
            float headerWidth = htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Width - htmlToPdfConverter.PdfDocumentOptions.LeftMargin -
                        htmlToPdfConverter.PdfDocumentOptions.RightMargin;

            // Calculate a resize factor to fit the header width
            float resizeFactor = 1;
            if (headerHtmlWidth > headerWidth)
                resizeFactor = headerWidth / headerHtmlWidth;

            // Calculate the header height to preserve the HTML aspect ratio
            float headerHeight = headerHtmlHeight * resizeFactor;

            if (!(headerHeight < htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Height - htmlToPdfConverter.PdfDocumentOptions.TopMargin -
                        htmlToPdfConverter.PdfDocumentOptions.BottomMargin))
            {
                throw new Exception("The header height cannot be bigger than PDF page height");
            }

            // Set the calculated header height
            htmlToPdfConverter.PdfDocumentOptions.DocumentObject.Header.Height = headerHeight;
        }


        void footerHtml_NavigationCompletedEvent(NavigationCompletedParams eventParams)
        {
            // Get the footer HTML width and height from event parameters
            float footerHtmlWidth = eventParams.HtmlContentWidthPt;
            float footerHtmlHeight = eventParams.HtmlContentHeightPt;

            // Calculate the footer width from coverter settings
            float footerWidth = htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Width - htmlToPdfConverter.PdfDocumentOptions.LeftMargin -
                        htmlToPdfConverter.PdfDocumentOptions.RightMargin;

            // Calculate a resize factor to fit the footer width
            float resizeFactor = 1;
            if (footerHtmlWidth > footerWidth)
                resizeFactor = footerWidth / footerHtmlWidth;

            // Calculate the footer height to preserve the HTML aspect ratio
            float footerHeight = footerHtmlHeight * resizeFactor;

            if (!(footerHeight < htmlToPdfConverter.PdfDocumentOptions.PdfPageSize.Height - htmlToPdfConverter.PdfDocumentOptions.TopMargin -
                        htmlToPdfConverter.PdfDocumentOptions.BottomMargin))
            {
                throw new Exception("The footer height cannot be bigger than PDF page height");
            }

            // Set the calculated footer height
            htmlToPdfConverter.PdfDocumentOptions.DocumentObject.Footer.Height = footerHeight;
        }


        void htmlToPdfConverter_BeforeRenderPdfPageEvent(BeforeRenderPdfPageParams eventParams)
        {      
            PdfPage page = eventParams.Page;
            if (showFooterEveryPage) page.ShowFooter = true;
            else if (eventParams.PageNumber == eventParams.PageCount) page.ShowFooter = true;
            else page.ShowFooter = false;

            if (showHeaderEveryPage) page.ShowHeader = true;
            else if (eventParams.PageNumber == 1) page.ShowHeader = true;
            else page.ShowHeader = false;
        }

    }

}