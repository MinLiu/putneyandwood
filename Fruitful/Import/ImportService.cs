﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace Fruitful.Import
{

    public static class ImportService
    {
        public static DataTable CopyExcelFileToTable(string filePath, bool deleteFile)
        {
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 12.0;HDR={1}'";
            var table = new DataTable();

            using (var conn = new OleDbConnection(connString))
            {
                conn.Open();
                try
                {
                    var firstSheetName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();

                    using (var adapter = new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", conn))
                    {
                        adapter.Fill(table);
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            // Delete the file from the server to free up space if required.
            if (deleteFile)
            {
                try { File.Delete(filePath); }
                catch { }
            }

            return table;
        }

        public static bool IsTableValid(DataTable table, string[] columns, out string message)
        {
            bool valid = true;
            message = String.Empty;
            var missingColumns = new List<string>();

            // Check all columns exist and are labelled correctly. 
            foreach (var label in columns)
            {
                if (!table.Columns.Contains(label))
                {
                    valid = false;
                    missingColumns.Add(label);
                }
            }

            if (!valid)
            {
                message = String.Format("Import file is missing the following columns: '{0}'", String.Join(",", missingColumns));
                return false;
            }

            return valid;
        }
    }

}
