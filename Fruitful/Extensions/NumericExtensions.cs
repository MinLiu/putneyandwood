﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fruitful.Extensions
{
    public static class NumericExtensions
    {
        public static string Ordinalise(this int value)
        {
            var result = value.ToString();

            if ((value % 100) == 11 || (value % 100) == 12 || (value % 100) == 13)
                return result + "th";

            switch (value % 10)
            {
                case 1: result += "st"; break;
                case 2: result += "nd"; break;
                case 3: result += "rd"; break;
                default: result += "th"; break;
            }

            return result;
        }
    }
}
