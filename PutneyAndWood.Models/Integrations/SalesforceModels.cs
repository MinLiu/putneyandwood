﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Integrations.Salesforce
{
    public class SalesforceTokenResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        /// <summary>
        /// Identifies the Salesforce instance to which API calls should be sent.
        /// </summary>
        [JsonProperty("instance_url")]
        public string InstanceURL { get; set; }
        /// <summary>
        /// Identity URL that can be used to both identify the user as well
        /// as query for more information about the user.Can be used in an
        /// HTTP request to get more information about the end user.
        /// </summary>
        [JsonProperty("id")]
        public string ID { get; set; }
        /// <summary>
        /// When the signature was created, represented as the number of
        /// seconds since the Unix epoch(00:00:00 UTC on 1 January 1970).
        /// </summary>
        [JsonProperty("issued_at")]
        public long? IssuedAt { get; set; }
        /// <summary>
        /// Base64-encoded HMAC-SHA256 signature signed with the
        /// consumer's private key containing the concatenated ID and
        /// issued_at value. The signature can be used to verify
        /// that the identity URL wasn’t modified because it was sent by the server
        /// </summary>
        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    #region General

    public class ResponseInfo
    {
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }
        [JsonProperty("done")]
        public bool Done{ get; set; }
        [JsonProperty("nextRecordsUrl")]
        public string NextRecordURL { get; set; }
    }

    public class Attributes
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("url")]
        public string URL { get; set; }
    }

    public class AddressInfo
    {
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("geocodeAccuracy")]
        public string GeocodeAccuracy { get; set; }
        [JsonProperty("latitude")]
        public string Latitude { get; set; }
        [JsonProperty("longitude")]
        public string Longitude { get; set; }
        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
    }

    #endregion

    #region Accounts

    public class AccountResponse : ResponseInfo
    {
        [JsonProperty("records")]
        public List<Account> Records { get; set; }
    }

    public class Account
    {
        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Id")]
        public string ID { get; set; }
        [JsonProperty("BillingAddress")]
        public AddressInfo BillingAddress { get; set; }
        [JsonProperty("ShippingAddress")]
        public AddressInfo ShippingAddress { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("Website")]
        public string Website { get; set; }
    }

    #endregion

    #region Contacts

    public class ContactResponse : ResponseInfo
    {
        [JsonProperty("records")]
        public List<Contact> Records { get; set; }
    }

    public class Contact
    {
        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }
        [JsonProperty("name")]
        public string ID { get; set; }
        [JsonProperty("IsDeleted")]
        public bool? IsDeleted { get; set; }
        [JsonProperty("AccountId")]
        public string AccountID { get; set; }
        [JsonProperty("LastName")]
        public string LastName { get; set; }
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }
        [JsonProperty("Salutation")]
        public string Salutation { get; set; }
        [JsonProperty("OtherAddress")]
        public AddressInfo OtherAddress { get; set; }
        [JsonProperty("MailingAddress")]
        public AddressInfo MailingAddress { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Fax")]
        public string Fax { get; set; }
        [JsonProperty("MobilePhone")]
        public string MobilePhone { get; set; }
        [JsonProperty("HomePhone")]
        public string HomePhone { get; set; }
        [JsonProperty("OtherPhone")]
        public string OtherPhone { get; set; }
        [JsonProperty("AssistantPhone")]
        public string AssistantPhone { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Department")]
        public string Department { get; set; }
        [JsonProperty("Birthdate")]
        public DateTime? BirthDate { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("PhotoUrl")]
        public string PhotoURL { get; set; }
    }

    #endregion
}
