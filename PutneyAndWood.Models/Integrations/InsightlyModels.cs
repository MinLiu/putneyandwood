﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Integrations.Insightly
{
    public class Organisation
    {
        [JsonProperty("ORGANISATION_ID")]
        public long OrganisationID { get; set; }
        [JsonProperty("ORGANISATION_NAME")]
        public string OrganisationName { get; set; }
        [JsonProperty("BACKGROUND")]
        public string Background { get; set; }
        [JsonProperty("IMAGE_URL")]
        public string ImageUrl { get; set; }
        [JsonProperty("OWNER_USER_ID")]
        public long OwnerUserID { get; set; }
        [JsonProperty("DATE_CREATED_UTC")]
        public DateTime DateCreatedUTC { get; set; }
        [JsonProperty("DATE_UPDATED_UTC")]
        public DateTime DateUpdatedUTC { get; set; }
        [JsonProperty("VISIBLE_TO")]
        public string VisibleTo { get; set; }
        [JsonProperty("VISIBLE_TEAM_ID")]
        public long? VisibleTeamID { get; set; }
        [JsonProperty("VISIBLE_USER_IDS")]
        public string VisibleUserIDS { get; set; }
        //[JsonProperty("CUSTOMFIELDS")]
        //public string CustomeFields { get; set; }
        [JsonProperty("ADDRESSES")]
        public List<Address> Addresses { get; set; }
        [JsonProperty("CONTACTINFOS")]
        public List<ContactInfo> ContactInfos { get; set; }
    }

    public class Address
    {
        [JsonProperty("ADDRESS_ID")]
        public long AddressID { get; set; }
        [JsonProperty("ADDRESS_TYPE")]
        public string AddressType { get; set; }
        [JsonProperty("STREET")]
        public string Street { get; set; }
        [JsonProperty("CITY")]
        public string City { get; set; }
        [JsonProperty("STATE")]
        public string State { get; set; }
        [JsonProperty("POSTCODE")]
        public string Postcode { get; set; }
        [JsonProperty("COUNTRY")]
        public string Country { get; set; }
    }

    public class ContactInfo
    {
        [JsonProperty("CONTACT_INFO_ID")]
        public long ContactInfoID { get; set; }
        [JsonProperty("TYPE")]
        public string Type { get; set; }
        [JsonProperty("SUBTYPE")]
        public string Subtype { get; set; }
        [JsonProperty("LABEL")]
        public string Label { get; set; }
        [JsonProperty("DETAIL")]
        public string Detail { get; set; }
    }

    public class Contact
    {
        [JsonProperty("CONTACT_ID")]
        public long ContactID { get; set; }
        [JsonProperty("SALUTATION")]
        public string Salutation { get; set; }
        [JsonProperty("FIRST_NAME")]
        public string FirstName { get; set; }
        [JsonProperty("LAST_NAME")]
        public string LastName { get; set; }
        [JsonProperty("BACKGROUND")]
        public string Background { get; set; }
        [JsonProperty("IMAGE_URL")]
        public string ImageUrl { get; set; }
        [JsonProperty("DEFAULT_LINKED_ORGANISATION")]
        public long? DefaultLinkedOrganisation { get; set; }
        [JsonProperty("OWNER_USER_ID")]
        public long? OwnerUserID { get; set; }
        [JsonProperty("DATE_CREATED_UTC")]
        public DateTime DateCreatedUTC { get; set; }
        [JsonProperty("DATE_UPDATED_UTC")]
        public DateTime DateUpdatedUTC { get; set; }
        [JsonProperty("VISIBLE_TO")]
        public string VisibleTo { get; set; }
        [JsonProperty("VISIBLE_TEAM_ID")]
        public long? VisibleTeamID { get; set; }
        [JsonProperty("ADDRESSES")]
        public List<Address> Addresses { get; set; }
        [JsonProperty("CONTACTINFOS")]
        public List<ContactInfo> ContactInfos { get; set; }
        [JsonProperty("LINKS")]
        public List<Link> Links { get; set; }
    }

    public class Link
    {
        [JsonProperty("LINK_ID")]
        public long? LinkID { get; set; }
        [JsonProperty("CONTACT_ID")]
        public long? ContactID { get; set; }
        [JsonProperty("OPPORTUNITY_ID")]
        public long? OpportunityID { get; set; }
        [JsonProperty("ORGANISATION_ID")]
        public long? OrganisationID { get; set; }
        [JsonProperty("PROJECT_ID")]
        public long? ProjectID { get; set; }
        [JsonProperty("SECOND_PROJECT_ID")]
        public long? SecondProjectID { get; set; }
        [JsonProperty("SECOND_OPPORTUNITY_ID")]
        public long? SecondOpportunityID { get; set; }
        [JsonProperty("ROLE")]
        public string Role { get; set; }
        [JsonProperty("DETAILS")]
        public string Details { get; set; }
    }
}
