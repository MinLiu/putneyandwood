﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Integrations.Zoho
{
    public class ZohoTokenResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("expires_in_sec")]
        public int ExpiresInSec { get; set; }
        [JsonProperty("api_domain")]
        public string ApiDomain { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }

    #region General

    public class ResponseInfo
    {
        [JsonProperty("per_page")]
        public int PerPage { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }
        [JsonProperty("page")]
        public int Page { get; set; }
        [JsonProperty("more_records")]
        public bool MoreRecords { get; set; }
    }

    public class PropertyInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
    }

    public class Approval
    {
        [JsonProperty("delegate")]
        public bool? Delegate { get; set; }
        [JsonProperty("approve")]
        public bool? Approve { get; set; }
        [JsonProperty("reject")]
        public bool? Reject { get; set; }
        [JsonProperty("resubmit")]
        public bool? Resubmit { get; set; }
    }

    public class Tag
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
    }

    #endregion

    #region Accounts

    public class AccountResponse
    {
        [JsonProperty("data")]
        public List<Account> Data { get; set; }
        [JsonProperty("info")]
        public ResponseInfo Info { get; set; }
    }

    public class Account
    {
        [JsonProperty("Owner")]
        public PropertyInfo Owner { get; set; }
        [JsonProperty("Ownership")]
        public string Ownership { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("$currency_symbol")]
        public string CurrencySymbol { get; set; }
        [JsonProperty("Account_Type")]
        public string AccountType { get; set; }
        [JsonProperty("Rating")]
        public string Rating { get; set; }
        [JsonProperty("SIC_Code")]
        public string SicCode { get; set; }
        [JsonProperty("Shipping_State")]
        public string ShippingState { get; set; }
        [JsonProperty("Website")]
        public string Website { get; set; }
        [JsonProperty("Employees")]
        public int? Employees { get; set; }
        [JsonProperty("Last_Activity_Time")]
        public DateTime? LastActivityTime { get; set; }
        [JsonProperty("Industry")]
        public string Industry { get; set; }
        [JsonProperty("Record_Image")]
        public string RecordImage { get; set; }
        [JsonProperty("Modified_By")]
        public PropertyInfo ModifiedBy { get; set; }
        [JsonProperty("Account_Site")]
        public string AccountSite { get; set; }
        [JsonProperty("$process_flow")]
        public bool? ProcessFlow { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Billing_Country")]
        public string BillingCountry { get; set; }
        [JsonProperty("Account_Name")]
        public string AccountName { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
        [JsonProperty("Account_Number")]
        public string AccountNumber { get; set; }
        [JsonProperty("$approved")]
        public bool? Approved { get; set; }
        [JsonProperty("Ticker_Symbol")]
        public string TickerSymbol { get; set; }
        [JsonProperty("$approval")]
        public Approval Approval { get; set; }
        [JsonProperty("Modified_Time")]
        public DateTime ModifiedTime { get; set; }
        [JsonProperty("Billing_Street")]
        public string BillingStreet { get; set; }
        [JsonProperty("Created_Time")]
        public DateTime? CreatedTime { get; set; }
        [JsonProperty("$followed")]
        public bool? Followed { get; set; }
        [JsonProperty("$editable")]
        public bool? Editable { get; set; }
        [JsonProperty("Billing_Code")]
        public string BillingCode { get; set; }
        [JsonProperty("Parent_Account")]
        public string ParentAccount { get; set; }
        [JsonProperty("Shipping_City")]
        public string ShippingCity { get; set; }
        [JsonProperty("Shipping_Country")]
        public string ShippingCountry { get; set; }
        [JsonProperty("Shipping_Code")]
        public string ShippingCode { get; set; }
        [JsonProperty("Billing_City")]
        public string BillingCity { get; set; }
        [JsonProperty("Billing_State")]
        public string BillingState { get; set; }
        [JsonProperty("Created_By")]
        public PropertyInfo CreatedBy { get; set; }
        [JsonProperty("Tag")]
        public List<Tag> Tags { get; set; }
        [JsonProperty("Fax")]
        public string Fax { get; set; }
        [JsonProperty("Annual_Revenue")]
        public long? AnnualRevenue { get; set; }
        [JsonProperty("Shipping_Street")]
        public string ShippingStreet { get; set; }

    }

    #endregion

    #region Contacts

    public class ContactResponse
    {
        [JsonProperty("data")]
        public List<Contact> Data { get; set; }
        [JsonProperty("info")]
        public ResponseInfo Info { get; set; }
    }

    public class Contact
    {
        [JsonProperty("Owner")]
        public PropertyInfo Owner { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("$currency_symbol")]
        public string CurrencySymbol { get; set; }
        [JsonProperty("Other_Phone")]
        public string OtherPhone { get; set; }
        [JsonProperty("Mailing_State")]
        public string MailingState { get; set; }
        [JsonProperty("Other_State")]
        public string OtherState { get; set; }
        [JsonProperty("Other_Country")]
        public string OtherCountry { get; set; }
        [JsonProperty("Last_Activity_Time")]
        public DateTime? LastActivityTime { get; set; }
        [JsonProperty("Department")]
        public string Department { get; set; }
        [JsonProperty("$process_flow")]
        public string ProcessFlow { get; set; }
        [JsonProperty("Assistant")]
        public string Assistant { get; set; }
        [JsonProperty("Mailing_Country")]
        public string MailingCountry { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
        [JsonProperty("$approved")]
        public bool? Approved { get; set; }
        [JsonProperty("$approval")]
        public Approval Approval { get; set; }
        [JsonProperty("Other_City")]
        public string OtherCity { get; set; }
        [JsonProperty("Created_Time")]
        public DateTime? CreatedTime { get; set; }
        [JsonProperty("$followed")]
        public bool? Followed { get; set; }
        [JsonProperty("$editable")]
        public bool? Editable { get; set; }
        [JsonProperty("Home_Phone")]
        public string HomePhone { get; set; }
        [JsonProperty("Created_By")]
        public PropertyInfo CreatedBy { get; set; }
        [JsonProperty("Secondary_Email")]
        public string SecondaryEmail { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("Vendor_Name")]
        public string VendorName { get; set; }
        [JsonProperty("Mailing_Zip")]
        public string MailingZip { get; set; }
        [JsonProperty("Reports_To")]
        public string ReportsTo { get; set; }
        [JsonProperty("Twitter")]
        public string Twitter { get; set; }
        [JsonProperty("Other_Zip")]
        public string OtherZip { get; set; }
        [JsonProperty("Mailing_Street")]
        public string MailingStreet { get; set; }
        [JsonProperty("Salutation")]
        public string Salutation { get; set; }
        [JsonProperty("First_Name")]
        public string FirstName { get; set; }
        [JsonProperty("Full_Name")]
        public string FullName { get; set; }
        [JsonProperty("Asst_Phone")]
        public string AsstPhone { get; set; }
        [JsonProperty("Record_Image")]
        public string RecordImage { get; set; }
        [JsonProperty("Modified_By")]
        public PropertyInfo ModifiedBy { get; set; }
        [JsonProperty("Skype_ID")]
        public string SkypeID { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Account_Name")]
        public PropertyInfo AccountName { get; set; }
        [JsonProperty("Email_Opt_Out")]
        public bool? EmailOptOut { get; set; }
        [JsonProperty("Modified_Time")]
        public DateTime? ModifiedTime { get; set; }
        [JsonProperty("Date_of_Birth")]
        public DateTime? DateOfBirth { get; set; }
        [JsonProperty("Mailing_City")]
        public string MailingCity { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Other_Street")]
        public string OtherStreet { get; set; }
        [JsonProperty("Mobile")]
        public string Mobile { get; set; }
        [JsonProperty("Last_Name")]
        public string LastName { get; set; }
        [JsonProperty("Lead_Source")]
        public string LeadSource { get; set; }
        [JsonProperty("Tag")]
        public List<Tag> Tags { get; set; }
        [JsonProperty("Fax")]
        public string Fax { get; set; }
    }

    #endregion
}
