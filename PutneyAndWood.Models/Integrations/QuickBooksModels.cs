﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Models
{
    public class ClientQBTabViewModel
    {
        public Intuit.Ipp.Data.Customer Customer { get; set; }
        public List<Intuit.Ipp.Data.Invoice> Invoices { get; set; }
    }

    public class SupplierQBTabViewModel
    {
        public Intuit.Ipp.Data.Vendor Vendor { get; set; }
        public List<Intuit.Ipp.Data.Bill> Bills { get; set; }
    }
}
