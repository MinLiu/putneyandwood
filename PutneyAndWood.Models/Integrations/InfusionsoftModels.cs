﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Integrations.Infusionsoft
{
    public class InfusionsoftTokenResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }

    #region General

    public class ResponseInfo
    {
        [JsonProperty("count")]
        public int count { get; set; }
        [JsonProperty("next")]
        public string Next { get; set; }
        [JsonProperty("previous")]
        public string Previous { get; set; }
    }

    public class AddressInfo
    {
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("field")]
        public string Field { get; set; }
        [JsonProperty("line1")]
        public string Line1 { get; set; }
        [JsonProperty("line2")]
        public string Line2 { get; set; }
        [JsonProperty("locality")]
        public string Locality { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }
        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
        [JsonProperty("zip_four")]
        public string ZipFour { get; set; }
    }

    public class PhoneNumberInfo
    {
        [JsonProperty("extension")]
        public string Extension { get; set; }
        [JsonProperty("field")]
        public string Field { get; set; }
        [JsonProperty("number")]
        public string Number { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    #endregion

    #region Accounts

    public class CompanyResponse : ResponseInfo
    {
        [JsonProperty("companies")]
        public List<Company> Companies { get; set; }
    }

    public class Company
    {
        [JsonProperty("address")]
        public AddressInfo Address { get; set; }
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }
        [JsonProperty("email_address")]
        public string EmailAddress { get; set; }
        [JsonProperty("email_opted_in")]
        public bool? EmailOptedIn { get; set; }
        [JsonProperty("email_status")]
        public string EmailStatus { get; set; }
        [JsonProperty("fax_number")]
        public PhoneNumberInfo FaxNumber { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("phone_number")]
        public PhoneNumberInfo PhoneNumber { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }
    }

    #endregion

    #region Contacts

    public class ContactResponse : ResponseInfo
    {
        [JsonProperty("contacts")]
        public List<Contact> Contacts { get; set; }
    }

    public class Contact
    {
        [JsonProperty("addresses")]
        public List<AddressInfo> Addresses { get; set; }
        [JsonProperty("anniversary")]
        public DateTime? Anniversary { get; set; }
        [JsonProperty("birthday")]
        public DateTime? Birthday { get; set; }
        [JsonProperty("company")]
        public ContactCompanyInfo Company { get; set; }
        [JsonProperty("contact_type")]
        public string ContactType { get; set; }
        [JsonProperty("date_created")]
        public DateTime? DateCreated { get; set; }
        [JsonProperty("email_addresses")]
        public List<ContactEmailInfo> EmailAddresses { get; set; }
        [JsonProperty("email_opted_in")]
        public bool? EmailOptedIn { get; set; }
        [JsonProperty("email_status")]
        public string EmailStatus { get; set; }
        [JsonProperty("family_name")]
        public string FamilyName { get; set; }
        [JsonProperty("fax_numbers")]
        public List<PhoneNumberInfo> FaxNumbers { get; set; }
        [JsonProperty("given_name")]
        public string GivenName { get; set; }
        [JsonProperty("id")]
        public long ID{ get; set; }
        [JsonProperty("job_title")]
        public string JobTitle { get; set; }
        [JsonProperty("last_updated")]
        public DateTime? LastUpdated { get; set; }
        [JsonProperty("lead_source_id")]
        public long? LeadSourceID { get; set; }
        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("opt_in_reason")]
        public string OptInReason { get; set; }
        [JsonProperty("owner_id")]
        public long? OwnerID { get; set; }
        [JsonProperty("phone_numbers")]
        public List<PhoneNumberInfo> PhoneNumbers { get; set; }
        [JsonProperty("preferred_locale")]
        public string PreferredLocale { get; set; }
        [JsonProperty("preferred_name")]
        public string PreferredName { get; set; }
        [JsonProperty("prefix")]
        public string Prefix { get; set; }
        [JsonProperty("relationships")]
        public List<ContactRelationshipInfo> Relationships { get; set; }
        [JsonProperty("social_accounts")]
        public List<ContactSocialAccountInfo> SocialAccounts { get; set; }
        [JsonProperty("source_type")]
        public string SourceType { get; set; }
        [JsonProperty("spouse_name")]
        public string SpouseName { get; set; }
        [JsonProperty("suffix")]
        public string Suffix { get; set; }
        [JsonProperty("time_zone")]
        public string TimeZone { get; set; }
        [JsonProperty("website")]
        public string Website { get; set; }

    }

    public class ContactCompanyInfo
    {
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }
        [JsonProperty("id")]
        public long ID { get; set; }
    }

    public class ContactEmailInfo
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("field")]
        public string Field { get; set; }
    }

    public class ContactRelationshipInfo
    {
        [JsonProperty("id")]
        public long ID { get; set; }
        [JsonProperty("linked_contact_id")]
        public long? LinkedContactID { get; set; }
        [JsonProperty("relationship_type_id")]
        public long? RelationshipTypeID { get; set; }
    }

    public class ContactSocialAccountInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    #endregion
}
