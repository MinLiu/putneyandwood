﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using SnapSuite.Internal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SnapSuite.Models
{
    public class SnapDbContext : IdentityDbContext<User>
    {
        public SnapDbContext() : base("name=DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Disable cascade delete by default for one-to-many relationships.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            // Override default convertion for Decimals.
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(28, 5)); //Set max decimal places to 5

            // Specify entities which will cascade delete.
            modelBuilder.Entity<User>().HasMany(q => q.Claims).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Logins).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Roles).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasOptional(q => q.UserHelpTutorials).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<UserHelpTutorials>().HasRequired(q => q.User).WithOptional(q => q.UserHelpTutorials).WillCascadeOnDelete();

            modelBuilder.Entity<UserRoleOption>().HasRequired(q => q.IdentityRole).WithRequiredDependent();

            modelBuilder.Entity<Product>().HasMany(i => i.Images).WithRequired(i => i.Owner).WillCascadeOnDelete();
            modelBuilder.Entity<QuotationItem>().HasMany(i => i.Images).WithRequired(i => i.Owner).WillCascadeOnDelete();

            /*
            modelBuilder.Entity<Product>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<Quotation>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<Job>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<Invoice>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<PurchaseOrder>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<DeliveryNote>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<QuotationItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<JobItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<QuotationCostItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<JobCostItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<InvoiceItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<PurchaseOrderItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            modelBuilder.Entity<DeliveryNoteItem>().HasMany(i => i.CustomFieldValues).WithOptional().WillCascadeOnDelete();
            */

            base.OnModelCreating(modelBuilder);
        }

        public static SnapDbContext Create() { return new SnapDbContext(); }

        //Company Entities
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyAddress> CompanyAddresses { get; set; }
        public virtual DbSet<CompanyEmail> CompanyEmails { get; set; }
        public virtual DbSet<UserRoleOption> UserRoleOptions { get; set; }
        public virtual DbSet<UserHelpTutorials> UserHelpTutorials { get; set; }
        public virtual DbSet<FileEntry> FileEntries { get; set; }
        //public virtual DbSet<CustomField> CustomFields { get; set; }


        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamMember> TeamMembers { get; set; }
        public virtual DbSet<HierarchyMember> HierarchyMembers { get; set; }
        public virtual DbSet<SubscriptionPlan> SubscriptionPlans { get; set; }
        public virtual DbSet<SubscriptionPlanPayment> SubscriptionPlanPayments { get; set; }
        //public virtual DbSet<SubscriptionUserPriceBreak> SubscriptionUserPriceBreaks { get; set; }       
        public virtual DbSet<SubscriptionCartItem> SubscriptionCartItems { get; set; }
        public virtual DbSet<SubscriptionRequest> SubscriptionRequests { get; set; }
        public virtual DbSet<WorldPayResponse> WorldPayResponse { get; set; }
        
        //Auditing
        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }
        public virtual DbSet<ModuleType> ModuleTypes { get; set; }
        public virtual DbSet<StockLog> StockLogs { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<InternalLog> InternalLogs { get; set; }

        //Clients Entities
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ClientAddress> ClientAddresses { get; set; }
        public virtual DbSet<ClientAttachment> ClientAttachments { get; set; }
        public virtual DbSet<ClientContact> ClientContacts { get; set; }
        public virtual DbSet<ClientTagItem> ClientTagItems { get; set; }
        public virtual DbSet<ClientEvent> ClientEvents { get; set; }
        public virtual DbSet<ClientType> ClientTypes { get; set; }


        //Products Entities
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<ProductCost> ProductCosts { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductKitItem> ProductKitItems { get; set; }
        public virtual DbSet<ProductAssociative> ProductAssociatives { get; set; }
        public virtual DbSet<ProductAlternative> ProductAlternatives { get; set; }
        public virtual DbSet<ProductLink> ProductLinks { get; set; }
        public virtual DbSet<Labour> Labours { get; set; }
        public virtual DbSet<CustomProductField> CustomProductFields { get; set; }
        public virtual DbSet<ProductQualification> ProductQualifications { get; set; }

        public virtual DbSet<Currency> Currencies { get; set; }
        

        //Settings & Others Entities
        public virtual DbSet<Fruitful.Email.EmailSetting> EmailSettings { get; set; }
        public virtual DbSet<ClientTag> ClientTags { get; set; }
        public virtual DbSet<DefaultPdfTemplate> DefaultPdfTemplates { get; set; }



        //Quotation Entities
        public virtual DbSet<Quotation> Quotations { get; set; }
        public virtual DbSet<QuotationItem> QuotationItems { get; set; }
        public virtual DbSet<QuotationItemImage> QuotationItemImages { get; set; }
        public virtual DbSet<QuotationItemPriceBreak> QuotationItemPriceBreaks { get; set; }
        public virtual DbSet<QuotationAttachment> QuotationAttachments { get; set; }
        public virtual DbSet<QuotationComment> QuotationComments { get; set; }
        public virtual DbSet<QuotationNotification> QuotationNotifications { get; set; }
        public virtual DbSet<QuotationPreviewItem> QuotationPreviewItems { get; set; }
        public virtual DbSet<QuotationTemplate> QuotationTemplates { get; set; }
        public virtual DbSet<QuotationSection> QuotationSections { get; set; }
        public virtual DbSet<QuotationCostItem> QuotationCostItems { get; set; }
        public virtual DbSet<QuotationLabourItem> QuotationLabourItems { get; set; }
        public virtual DbSet<QuotationSignedAgreement> QuotationSignedAgreements { get; set; }
        public virtual DbSet<QuotationStripePayment> QuotationStripePayments { get; set; }

        public virtual DbSet<QuotationSettings> QuotationSettings { get; set; }
        public virtual DbSet<QuotationStatus> QuotationStatuses { get; set; }
        public virtual DbSet<DefaultQuotationAttachment> DefaultQuotationAttachments { get; set; }
        public virtual DbSet<DefaultQuotationNotification> DefaultQuotationNotifications { get; set; }
        public virtual DbSet<CustomQuotationField> CustomQuotationFields { get; set; }
        public virtual DbSet<QuotationQualification> QuotationQualifications { get; set; }
        public virtual DbSet<QuotationEvent> QuotationEvents { get; set; }

        public virtual DbSet<SageExportSettings> SageExportSettings { get; set; }

        // Opportunity Entities
        public virtual DbSet<OpportunityType> OpportunityTypes { get; set; }
        public virtual DbSet<OpportunityStatus> OpportunityStatus { get; set; }
        public virtual DbSet<Opportunity> Opportunities { get; set; }
        public virtual DbSet<OpportunityLog> OpportunityLogs { get; set; }

        // Project Entities
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectStatus> ProjectStatuses { get; set; }
        public virtual DbSet<Scope> Scopes { get; set; }
        public virtual DbSet<ProjectScope> ProjectScopes { get; set; }
        public virtual DbSet<EnquirySource> EnquirySources { get; set; }


        public virtual DbSet<BasisOfEstimate> BasisOfEstimates { get; set; }

        public static List<User> GetCompanyUsers(User CurrentUser) {

            using (var context = new SnapDbContext())
            {
                if (CurrentUser.Company.UseTeams)
                {
                    //Get the teams where the user is in
                    var teamIDs = context.TeamMembers.Where(t => t.UserId == CurrentUser.Id).Select(t => t.TeamID);

                    return context.TeamMembers
                            .Include(u => u.User)
                            .Where(u => teamIDs.Contains(u.TeamID))
                            .OrderBy(u => u.User.Email)
                            .Select(u => u.User).ToList();             
                }
                else if (CurrentUser.Company.UseHierarchy)
                {
                    var ids = new List<Guid>();
                    var allHiers = context.HierarchyMembers.Where(t => t.CompanyID == CurrentUser.CompanyID).Include(i => i.Underlings).Include(i => i.User).ToList();
                    var userHiers = allHiers.Where(i => i.UserId == CurrentUser.Id);

                    foreach (var h in userHiers)
                    {
                        HierarchyMemberExtensions.GetUnderlingIDs(h, ids, allHiers);

                        if(CurrentUser.Company.AccessSameLevelHierarchy)
                            HierarchyMemberExtensions.GetPeerIDs(h, ids, allHiers);
                    }

                    return allHiers.Where(i => ids.Contains(i.ID)).Select(u => u.User).ToList();  


                }
                else
                {
                    return context.Users.Where(u => u.CompanyID == CurrentUser.CompanyID)
                                        .OrderBy(u => u.Email).ToList();

                                        
                }
            }
        }
    }
}

