﻿using SnapSuite.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SnapSuite.Models
{
    public class StripeSettings : ICompanyEntity, ILoggable
    {
        public StripeSettings()
        {
            
        }

        [Key]
        [ForeignKey("Company"), Column("CompanyID")]
        public Guid CompanyID { get; set; }
        public string TokenType { get; set; }
        public string PublishableKey { get; set; }
        public string Scope { get; set; }
        public bool LiveMode { get; set; }
        public string UserID { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public bool DoNotChargeFee { get; set; }

        public virtual Company Company { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }



    public class StripeSettingsViewModel
    {
        //public string PublishableKey { get; set; }
        public string Scope { get; set; }
        public bool LiveMode { get; set; }
        //public string UserID { get; set; }
        public string AccessToken { get; set; }
    }




    public class StripeSettingsMapper : ModelMapper<StripeSettings, StripeSettingsViewModel>
    {
        public override void MapToViewModel(StripeSettings model, StripeSettingsViewModel viewModel)
        {
            //viewModel.PublishableKey = model.PublishableKey;
            viewModel.Scope = model.Scope;
            viewModel.LiveMode = model.LiveMode;
            //viewModel.UserID = model.UserID;
            viewModel.AccessToken = model.AccessToken;
        }

        public override void MapToModel(StripeSettingsViewModel viewModel, StripeSettings model)
        {
            throw new NotImplementedException();
        }

    }

}