﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class CompanyAddressRepository : EntityRespository<CompanyAddress>
    {
        public CompanyAddressRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyAddressRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
