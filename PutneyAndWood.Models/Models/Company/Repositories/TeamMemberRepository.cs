﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class TeamMemberRepository : EntityRespository<TeamMember>
    {
        public TeamMemberRepository()
            : this(new SnapDbContext())
        {

        }

        public TeamMemberRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
