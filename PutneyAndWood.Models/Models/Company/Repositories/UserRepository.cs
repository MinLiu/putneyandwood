﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class UserRepository : EntityRespository<User>
    {
        public UserRepository()
            : this(new SnapDbContext())
        {

        }

        public UserRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(User entity)
        {
            _db.Set<UserHelpTutorials>().RemoveRange(_db.Set<UserHelpTutorials>().Where(a => a.UserId == entity.Id));
            _db.Set<TeamMember>().RemoveRange(_db.Set<TeamMember>().Where(a => a.UserId == entity.Id));

            var hiers = _db.Set<HierarchyMember>().Where(a => a.UserId == entity.Id).ToList();

            foreach (var h in hiers)
            {
                var ids = h.GetAllUnderlingIDs();
                _db.Set<HierarchyMember>().RemoveRange(_db.Set<HierarchyMember>().Where(a => ids.Contains(a.ID) || a.ID == h.ID));
            }


            var set = _db.Set<User>().Where(a => a.Id == entity.Id)
                                    .Include(a => a.Quotations)
                                    .Include(a => a.AssignedClientEvents)
                                    .Include(a => a.CreatorClientEvents);
                                    //.Include(a => a.AssignedOpportunities)
                                    //.Include(a => a.CreatorOpportunities)
                                    //.Include(a => a.ActivityLogs);

            _db.Set<User>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
