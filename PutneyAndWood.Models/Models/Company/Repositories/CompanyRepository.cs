﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class CompanyRepository : EntityRespository<Company>
    {
        public CompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(Company entity)
        {
                       
            //Delete orders first because they are linked to the CRM entities.            
            _db.Set<QuotationSignedAgreement>().RemoveRange(_db.Set<QuotationSignedAgreement>().Where(x => x.CompanyID == entity.ID));
            _db.Set<QuotationStripePayment>().RemoveRange(_db.Set<QuotationStripePayment>().Where(x => x.Owner.CompanyID == entity.ID));
            _db.Set<QuotationAttachment>().RemoveRange(_db.Set<QuotationAttachment>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<QuotationItem>().RemoveRange(_db.Set<QuotationItem>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<QuotationCostItem>().RemoveRange(_db.Set<QuotationCostItem>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<QuotationLabourItem>().RemoveRange(_db.Set<QuotationLabourItem>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<QuotationNotification>().RemoveRange(_db.Set<QuotationNotification>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<QuotationComment>().RemoveRange(_db.Set<QuotationComment>().Where(x => x.Quotation.CompanyID == entity.ID));            
            _db.Set<DefaultQuotationAttachment>().RemoveRange(_db.Set<DefaultQuotationAttachment>().Where(x => x.QuotationSettings.CompanyID == entity.ID));
            _db.Set<DefaultQuotationNotification>().RemoveRange(_db.Set<DefaultQuotationNotification>().Where(x => x.QuotationSettings.CompanyID == entity.ID));
            _db.Set<QuotationSettings>().RemoveRange(_db.Set<QuotationSettings>().Where(x => x.CompanyID == entity.ID));
            _db.Set<QuotationPreviewItem>().RemoveRange(_db.Set<QuotationPreviewItem>().Where(x => x.Parent.CompanyID == entity.ID));
            _db.Set<QuotationSection>().RemoveRange(_db.Set<QuotationSection>().Where(x => x.Quotation.CompanyID == entity.ID));
            _db.Set<Quotation>().RemoveRange(_db.Set<Quotation>().Where(x => x.CompanyID == entity.ID));
            _db.Set<QuotationTemplate>().RemoveRange(_db.Set<QuotationTemplate>().Where(x => x.CompanyID == entity.ID));

            _db.Set<ProductPrice>().RemoveRange(_db.Set<ProductPrice>().Where(x => x.Product.CompanyID == entity.ID));
            _db.Set<ProductCost>().RemoveRange(_db.Set<ProductCost>().Where(x => x.Product.CompanyID == entity.ID));           
            _db.Set<ProductKitItem>().RemoveRange(_db.Set<ProductKitItem>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<ProductCategory>().RemoveRange(_db.Set<ProductCategory>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ProductAlternative>().RemoveRange(_db.Set<ProductAlternative>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<ProductAssociative>().RemoveRange(_db.Set<ProductAssociative>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<Product>().RemoveRange(_db.Set<Product>().Where(x => x.CompanyID == entity.ID));


            _db.Set<Client>().RemoveRange(_db.Set<Client>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ClientAddress>().RemoveRange(_db.Set<ClientAddress>().Where(x => x.Client.CompanyID == entity.ID));
            _db.Set<ClientAttachment>().RemoveRange(_db.Set<ClientAttachment>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientContact>().RemoveRange(_db.Set<ClientContact>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientTagItem>().RemoveRange(_db.Set<ClientTagItem>().Where(x => x.Client.CompanyID == entity.ID));
            _db.Set<ClientEvent>().RemoveRange(_db.Set<ClientEvent>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientTag>().RemoveRange(_db.Set<ClientTag>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ClientType>().RemoveRange(_db.Set<ClientType>().Where(x => x.CompanyID == entity.ID));

            _db.Set<OpportunityLog>().RemoveRange(_db.Set<OpportunityLog>().Where(x => x.CompanyID == entity.ID));
            _db.Set<Opportunity>().RemoveRange(_db.Set<Opportunity>().Where(x => x.CompanyID == entity.ID));
            _db.Set<OpportunityStatus>().RemoveRange(_db.Set<OpportunityStatus>().Where(x => x.CompanyID == entity.ID));
            _db.Set<OpportunityType>().RemoveRange(_db.Set<OpportunityType>().Where(x => x.CompanyID == entity.ID));

            _db.Set<Labour>().RemoveRange(_db.Set<Labour>().Where(x => x.CompanyID == entity.ID));

            //Always delete user stuff last.
            _db.Set<InternalLog>().RemoveRange(_db.Set<InternalLog>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ActivityLog>().RemoveRange(_db.Set<ActivityLog>().Where(x => x.CompanyID == entity.ID));
            _db.Set<UserRoleOption>().RemoveRange(_db.Set<UserRoleOption>().Where(x => x.CompanyID == entity.ID));
            _db.Set<UserHelpTutorials>().RemoveRange(_db.Set<UserHelpTutorials>().Where(x => x.User.CompanyID == entity.ID));
            _db.Set<User>().RemoveRange(_db.Set<User>().Where(x => x.CompanyID == entity.ID).Include(a => a.Quotations).Include(a => a.AssignedClientEvents).Include(a => a.CreatorClientEvents));
            _db.Set<CompanyAddress>().RemoveRange(_db.Set<CompanyAddress>().Where(x => x.CompanyID == entity.ID));
            _db.Set<TeamMember>().RemoveRange(_db.Set<TeamMember>().Where(x => x.Team.CompanyID == entity.ID));
            _db.Set<Team>().RemoveRange(_db.Set<Team>().Where(x => x.CompanyID == entity.ID));

            
            _db.Set<SageExportSettings>().RemoveRange(_db.Set<SageExportSettings>().Where(x => x.CompanyID == entity.ID));
            _db.Set<StripeSettings>().RemoveRange(_db.Set<StripeSettings>().Where(x => x.CompanyID == entity.ID));

            _db.Set<CustomProductField>().RemoveRange(_db.Set<CustomProductField>().Where(x => x.CompanyID == entity.ID));
            _db.Set<CustomQuotationField>().RemoveRange(_db.Set<CustomQuotationField>().Where(x => x.CompanyID == entity.ID));

            _db.Set<SubscriptionCartItem>().RemoveRange(_db.Set<SubscriptionCartItem>().Where(x => x.CompanyID == entity.ID));
            _db.Set<SubscriptionPlan>().RemoveRange(_db.Set<SubscriptionPlan>().Where(x => x.CompanyID == entity.ID));
            _db.Set<SubscriptionPlanPayment>().RemoveRange(_db.Set<SubscriptionPlanPayment>().Where(x => x.CompanyID == entity.ID));
            _db.Set<SubscriptionRequest>().RemoveRange(_db.Set<SubscriptionRequest>().Where(x => x.CompanyID == entity.ID));

            _db.SaveChanges();

            base.Delete(entity);
        }

    }

}
