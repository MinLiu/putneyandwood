﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class DefaultPdfTemplate: IEntity
    {
        [Key]
        public Guid ID { get; set; }
        public int ModuleTypeID { get; set; }
        [Required]
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string ThumbnailPath { get; set; }

        [Required]
        public TemplateType Type { get; set; }
        [Required]
        public TemplateFormat Format { get; set; }

        public virtual ModuleType ModuleType { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }


    public class DefaultPdfTemplateViewModel: IEntityViewModel
    {
        public string ID { get; set; }
        public string FileName { get; set; }
        public string ThumbnailPath { get; set; }

    }



    public class DefaultPdfTemplateMapper : ModelMapper<DefaultPdfTemplate, DefaultPdfTemplateViewModel>
    {
        public override void MapToViewModel(DefaultPdfTemplate model, DefaultPdfTemplateViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.FileName = model.FileName;
            viewModel.ThumbnailPath = model.ThumbnailPath;
        }

        public override void MapToModel(DefaultPdfTemplateViewModel viewModel, DefaultPdfTemplate model)
        {           
            throw new NotImplementedException();
        }

    }
}
