﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace SnapSuite.Models
{
    public enum FileFormat { File, Link, Image, Video, Pdf, Docx }

    public class FileEntry : IEntity
    {
        public FileEntry() { ID = Guid.NewGuid(); DateCreated = DateTime.Now; }

        [Key]
        public Guid ID { get; set; }
        //public string FileName { get; set; }
        public FileFormat FileFormat { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public int FileSize { get; set; }
        public DateTime DateCreated { get; set; }

        public string ToShortLabel() { return FilePath; }
        public string ToLongLabel() { return FilePath; }


        public virtual ICollection<QuotationItemImage> QuotationItemImages { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }

        
        [ForeignKey("LogoImageID")]
        public virtual ICollection<Company> LogoImageCompanies { get; set; }

        [ForeignKey("ScreenLogoImageID")]
        public virtual ICollection<Company> ScreenLogoImageCompanies { get; set; }
        

        public virtual ICollection<ClientAttachment> ClientAttachments { get; set; }

        public virtual ICollection<QuotationAttachment> QuotationAttachments { get; set; }

        public virtual ICollection<DefaultQuotationAttachment> DefaultQuotationAttachments { get; set; }

        public virtual ICollection<QuotationTemplate> QuotationTemplates { get; set; }


        [ForeignKey("SignatureImageID")]
        public virtual ICollection<QuotationSignedAgreement> QuotationAgreementSigntures { get; set; }
        [ForeignKey("PdfAgreementID")]
        public virtual ICollection<QuotationSignedAgreement> QuotationAgreementAgreements { get; set; }
        [ForeignKey("PdfApprovedID")]
        public virtual ICollection<QuotationSignedAgreement> QuotationAgreementApproves { get; set; }

    }


    public class FileEntryViewModel : IEntityViewModel
    {
        public string ID { get; set; }        
        //public string FileName { get; set; }
        public string FileFormat { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public string FileSize { get; set; }
        public string DateCreated { get; set; }
    }


    

    public static class FileEntryExtension
    {
        public static FileEntryViewModel ToViewModel(this FileEntry model)
        {
            if (model == null) return new FileEntryViewModel { FileFormat = "Error" };

            var viewModel = new FileEntryViewModel
            {
                ID = model.ID.ToString(),
                //FileName = model.FileName,
                FileFormat = model.FileFormat.ToString(),
                FilePath = model.FilePath,
                FileExtension = model.FileExtension,
                DateCreated = model.DateCreated.ToString("dd/MM/yy"),
            };

            if (model.FileSize < 100) viewModel.FileSize = "0Kbs";
            else if (model.FileSize > 1048576) viewModel.FileSize = string.Format("{0:#,##0.00}Mbs", model.FileSize / 1048576);
            else viewModel.FileSize = string.Format("{0:#,##0}Kbs", model.FileSize / 1024);

            return viewModel;
        }

        public static void MapViewModelToModel(this FileEntry model, FileEntryViewModel viewModel)
        {
            //model.FileName = viewModel.FileName;
        }

        public static string FileSizeToLabel(int fileSize)
        {
            if (fileSize < 100) return "0Kbs";
            else if (fileSize > 1048576) return string.Format("{0:#,##0.00}Mbs", fileSize / 1048576.0);
            else return string.Format("{0:#,##0}Kbs", fileSize / 1024.0);
        }
    }



}
