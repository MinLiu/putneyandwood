﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class CompanyEmail : IEntity
    {
        public CompanyEmail() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public string Email { get; set; }
        public string Username { get; set; }
        public string Host { get; set; }
        public string StoredPassword { get; set; }
        public int Port { get; set; }
        public bool EnableSSL { get; set; }
        public bool Default { get; set; }

        public bool SuccessfulTest { get; set; }

        public virtual Company Company { get; set; }

        [NotMapped]
        public string Password 
        {  
            get
            {
                try { return System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StoredPassword)); }
                catch { return ""; }
            }

            set { try { StoredPassword = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(value)); } catch { StoredPassword = ""; } }
        }  
  
        public string ToLongLabel() { return Email; }
        public string ToShortLabel() { return Email; }
    }



    public class CompanyEmailViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        public string User_email { get; set; }
        //public string Username { get; set; }
        public string Host { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool EnableSSL { get; set; }
        public bool Default { get; set; }

        public bool SuccessfulTest { get; set; }

    }




    public class CompanyEmailMapper : ModelMapper<CompanyEmail, CompanyEmailViewModel>
    {
        public override void MapToViewModel(CompanyEmail model, CompanyEmailViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.User_email = model.Email;
            //viewModel.Username = model.Username;
            viewModel.Host = model.Host;
            viewModel.Password = model.Password;
            viewModel.Port = model.Port;
            viewModel.EnableSSL = model.EnableSSL;
            viewModel.Default = model.Default;
            viewModel.SuccessfulTest = model.SuccessfulTest;
        }

        public override void MapToModel(CompanyEmailViewModel viewModel, CompanyEmail model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Email = viewModel.User_email;
            //model.Username = viewModel.Username;
            model.Host = viewModel.Host;
            model.Password = viewModel.Password;
            model.Port = viewModel.Port;
            model.Default = viewModel.Default;
            model.EnableSSL = viewModel.EnableSSL;
            //model.SuccessfulTest = viewModel.SuccessfulTest;
        }

    }


}
