﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    [Table("AspNetUserHelpTutorials")]
    public class UserHelpTutorials
    {
        [Key]
        [ForeignKey("User"), Column("UserId")]
        public string UserId { get; set; } //Be sure to set cascade delete

        public bool DoNotDashboardStartVideo { get; set; }
        public bool DoNotProductsStartVideo { get; set; }
        public bool DoNotCRMStartVideo { get; set; }
        public bool DoNotQuotationsStartVideo { get; set; }
        public bool DoNotJobsStartVideo { get; set; }
        public bool DoNotInvoicesStartVideo { get; set; }
        public bool DoNotDeliveryNotesStartVideo { get; set; }
        public bool DoNotPurchaseOrdersStartVideo { get; set; }
        public bool DoNotStockControlStartVideo { get; set; }

        public virtual User User { get; set; }

    }

}
