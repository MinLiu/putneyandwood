﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using SnapSuite.Internal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;

namespace SnapSuite.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    [Table("AspNetUsers")]
    public class User : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        //Addition user information. Stored along side in the same tables.
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid CompanyID { get; set; }
        public string Token { get; set; } //Login token

        public virtual Company Company { get; set; }

        public bool AccessCRM { get; set; } //Added but not needed
        public bool AccessProducts { get; set; } //Added but not needed
        public bool AccessQuotations { get; set; }
        public bool AccessJobs { get; set; }
        public bool AccessInvoices { get; set; }
        public bool AccessDeliveryNotes { get; set; }
        public bool AccessPurchaseOrders { get; set; }
        public bool AccessStockControl { get; set; }
         
        //Foreign References        
        public virtual ICollection<Quotation> Quotations { get; set; }

        [ForeignKey("AssignedUserID")]
        public virtual ICollection<ClientEvent> AssignedClientEvents { get; set; }

        [ForeignKey("CreatorID")]
        public virtual ICollection<ClientEvent> CreatorClientEvents { get; set; }

        //[ForeignKey("AssignedUserID")]
        //public virtual ICollection<Opportunity> AssignedOpportunities { get; set; }

        //[ForeignKey("CreatorID")]
        //public virtual ICollection<Opportunity> CreatorOpportunities { get; set; }

        //[ForeignKey("ActivityByUserID")]
        //public virtual ICollection<ActivityLog> ActivityLogs { get; set; }

        public virtual UserHelpTutorials UserHelpTutorials { get; set; }

        public string ToShortLabel() { return Email; }
        public string ToLongLabel() { return string.Join(" ", new string[] { FirstName, LastName, "-", Email }.Where(i => !string.IsNullOrEmpty(i))); }
        public string ToFullName() { return string.Join(" ", new string[] { FirstName, LastName }.Where(i => !string.IsNullOrEmpty(i))); }
        


    }


}