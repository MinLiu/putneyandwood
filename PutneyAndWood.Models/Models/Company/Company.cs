﻿using SnapSuite.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SnapSuite.Models
{
    public class Company : IEntity
    {
        public Company() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string AccountNo { get; set; }

        public DateTime SignupDate { get; set; }
        public bool AccountClosed { get; set; }
        public bool DeleteData { get; set; }
        public bool RemoveEmailFooter { get; set; }
        public int WizardStep { get; set; }
        public bool WizardComplete { get; set; }

        [Required]
        public string Name { get; set; }
        public string Motto { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string VatNumber { get; set; }
        public string RegNumber { get; set; }

        public Guid? LogoImageID { get; set; }
        public virtual FileEntry LogoImage { get; set; }
        public string LogoImageURL { get; set; }

        public Guid? ScreenLogoImageID { get; set; }
        public virtual FileEntry ScreenLogoImage { get; set; }
        public string ScreenLogoImageURL { get; set; }
        public string ThemeColor { get; set; }

        public decimal DefaultVatRate { get; set; }
        public int DefaultCurrencyID { get; set; }
        public virtual Currency DefaultCurrency { get; set; }

        public virtual ICollection<User> ApplicationUsers { get; set; }
        public virtual ICollection<CompanyAddress> Addresses { get; set; }
        public bool UseTeams { get; set; }
        public virtual ICollection<Team> Teams { get; set; }

        public bool UseHierarchy { get; set; }
        public bool AccessSameLevelHierarchy { get; set; }
        public virtual ICollection<HierarchyMember> HierarchyMembers { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        //public virtual ICollection<ProductCategory> ProductCategories { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<ClientTag> ClientTags { get; set; }


        public virtual ICollection<Quotation> Quotations { get; set; }
        public virtual QuotationSettings QuotationSettings { get; set; }
        public virtual SageExportSettings SageExportSettings { get; set; }


        public virtual ICollection<SubscriptionPlan> SubscriptionPlans { get; set; }
        public virtual ICollection<ActivityLog> ActivityLogs { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }



    public class CompanyViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string AccountNo { get; set; }

        public DateTime SignupDate { get; set; }
        public bool AccountClosed { get; set; }
        public bool DeleteData { get; set; }

        [Required]
        public string Name { get; set; }

        public string Motto { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string VatNumber { get; set; }
        public string RegNumber { get; set; }
        
        public string LogoImageURL { get; set; }
        public string ScreenLogoImageURL { get; set; }
        public string ThemeColor { get; set; }

        public decimal DefaultVatRate { get; set; }
        public int DefaultCurrencyID { get; set; }

        public bool UseTeams { get; set; }
        public bool UseHierarchy { get; set; }
        public bool AccessSameLevelHierarchy { get; set; }

        public int NumAccounts { get; set; }
        public int NumConfirmedAccounts { get; set; }

        public DateTime LastActivityDate { get; set; }
        public int QuietDays { get; set; }

    }




    public class CompanyMapper : ModelMapper<Company, CompanyViewModel>
    {
        public override void MapToViewModel(Company model, CompanyViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.AccountNo = model.AccountNo;
            viewModel.Name = model.Name;
            viewModel.Motto = model.Motto;
            viewModel.Address1 = model.Address1;
            viewModel.Address2 = model.Address2;
            viewModel.Address3 = model.Address3;
            viewModel.Town = model.Town;
            viewModel.County = model.County;
            viewModel.Postcode = model.Postcode;
            viewModel.Country = model.Country;
            viewModel.Telephone = model.Telephone;
            viewModel.Mobile = model.Mobile;
            viewModel.Fax = model.Fax;
            viewModel.Email = model.Email;
            viewModel.Website = model.Website;
            viewModel.VatNumber = model.VatNumber;
            viewModel.RegNumber = model.RegNumber;
            viewModel.LogoImageURL = (model.LogoImage != null) ? model.LogoImage.FilePath : null;
            viewModel.ScreenLogoImageURL = (model.ScreenLogoImage != null) ? model.ScreenLogoImage.FilePath : null;
            viewModel.ThemeColor = model.ThemeColor;
            viewModel.DefaultVatRate = model.DefaultVatRate;
            viewModel.DefaultCurrencyID = model.DefaultCurrencyID;

            viewModel.AccountClosed = model.AccountClosed;
            viewModel.SignupDate = model.SignupDate;
            viewModel.DeleteData = model.DeleteData;

            viewModel.UseTeams = model.UseTeams;
            viewModel.UseHierarchy = model.UseHierarchy;
            viewModel.AccessSameLevelHierarchy = model.AccessSameLevelHierarchy;

            try
            {
                viewModel.NumAccounts = model.ApplicationUsers.Count;
                viewModel.NumAccounts = model.ApplicationUsers.Where(u => u.EmailConfirmed == true).Count();
            }
            catch { }

            var lastActivityLog = model.ActivityLogs.OrderByDescending(x => x.Timestamp).FirstOrDefault();
            viewModel.LastActivityDate = lastActivityLog != null ? lastActivityLog.Timestamp.Date : model.SignupDate;
            viewModel.QuietDays = DateTime.Today.Subtract(lastActivityLog != null ? lastActivityLog.Timestamp.Date : model.SignupDate).Days;
        }

        public override void MapToModel(CompanyViewModel viewModel, Company model)
        {
            //ID = viewModel.ID;
            model.AccountNo = viewModel.AccountNo;
            model.Name = viewModel.Name;
            model.Motto = viewModel.Motto;
            model.Address1 = viewModel.Address1;
            model.Address2 = viewModel.Address2;
            model.Address3 = viewModel.Address3;
            model.Town = viewModel.Town;
            model.County = viewModel.County;
            model.Postcode = viewModel.Postcode;
            model.Country = viewModel.Country;
            model.Telephone = viewModel.Telephone;
            model.Mobile = viewModel.Mobile;
            model.Fax = viewModel.Fax;
            model.Email = viewModel.Email;
            model.Website = viewModel.Website;
            model.VatNumber = viewModel.VatNumber;
            model.RegNumber = viewModel.RegNumber;

            //model.LogoImageURL = viewModel.LogoImageURL;
            //model.ScreenLogoImageURL = viewModel.ScreenLogoImageURL;
            //model.ThemeColor = viewModel.ThemeColor;

            //model.UseTeams = viewModel.UseTeams;
            //model.UseHierarchy = viewModel.UseHierarchy;
            //model.AccessSameLevelHierarchy = viewModel.AccessSameLevelHierarchy;

            //model.AccountClosed = viewModel.AccountClosed;
            //model.SignupDate = viewModel.SignupDate;
            //model.DeleteData = viewModel.DeleteData;
            model.DefaultVatRate = viewModel.DefaultVatRate;
            model.DefaultCurrencyID = viewModel.DefaultCurrencyID;
        }

    }

}