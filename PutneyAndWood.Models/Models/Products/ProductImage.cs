﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductImage : IImageItem<Product>
    {
        public ProductImage() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid OwnerID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public int SortPos { get; set; }
        public virtual Product Owner { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }


    public class ImageViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string OwnerID { get; set; }
        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }
        public int SortPos { get; set; }
    }



    public class ProductImageMapper : ModelMapper<ProductImage, ImageViewModel>
    {

        public override void MapToViewModel(ProductImage model, ImageViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.OwnerID = model.OwnerID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.SortPos = model.SortPos;
        }


        public override void MapToModel(ImageViewModel viewModel, ProductImage model)
        {      
            //model.JobID = Guid.Parse(viewModel.JobID);
            ////model.File.MapViewModelToModel(viewModel.File);
            //model.Filename = viewModel.Filename;
            //model.FilePath = viewModel.FilePath;
            model.SortPos = viewModel.SortPos;    
        }

    }
}
