﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductCostRepository : EntityRespository<ProductCost>
    {
        public ProductCostRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductCostRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
