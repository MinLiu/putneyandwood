﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductAlternativeRepository : EntityRespository<ProductAlternative>
    {
        public ProductAlternativeRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductAlternativeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
