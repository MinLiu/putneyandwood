﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductRepository : EntityRespository<Product>
    {
        public ProductRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Product entity)
        {
            _db.Set<ProductPrice>().RemoveRange(_db.Set<ProductPrice>().Where(i => i.ProductID == entity.ID));
            _db.Set<ProductCost>().RemoveRange(_db.Set<ProductCost>().Where(i => i.ProductID == entity.ID));
            _db.Set<ProductKitItem>().RemoveRange(_db.Set<ProductKitItem>().Where(i => i.ProductID == entity.ID));
            _db.Set<ProductAlternative>().RemoveRange(_db.Set<ProductAlternative>().Where(i => i.ProductID == entity.ID));
            _db.Set<ProductAssociative>().RemoveRange(_db.Set<ProductAssociative>().Where(i => i.ProductID == entity.ID));
            _db.SaveChanges();

            var set = _db.Set<Product>().Where(a => a.ID == entity.ID);

            _db.Set<Product>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
