﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductPriceRepository : EntityRespository<ProductPrice>
    {
        public ProductPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductPriceRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
