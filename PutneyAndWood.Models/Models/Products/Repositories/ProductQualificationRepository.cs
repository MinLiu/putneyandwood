﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductQualificationRepository : EntityRespository<ProductQualification>
    {
        public ProductQualificationRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductQualificationRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
