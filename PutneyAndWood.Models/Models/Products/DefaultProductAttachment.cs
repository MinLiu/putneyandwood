﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class DefaultProductAttachment : IAttachment, IShowInPreview
    {
        public DefaultProductAttachment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }

        public virtual Product Product { get; set; }

        public string ToShortLabel() { return Filename; }
        public string ToLongLabel() { return Filename; }

    }



    public class DefaultProductAttachmentViewModel : IAttachmentViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }

        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }
        [Required(ErrorMessage = "* The Filename is required.")]
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }
    }


    public class DefaultProductAttachmentMapper : ModelMapper<DefaultProductAttachment, DefaultProductAttachmentViewModel>
    {
        public override void MapToViewModel(DefaultProductAttachment model, DefaultProductAttachmentViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.Filename = model.Filename;
            viewModel.ShowInPreview = model.ShowInPreview;
        }

        public override void MapToModel(DefaultProductAttachmentViewModel viewModel, DefaultProductAttachment model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            //model.FileID = Guid.Parse(viewModel.ProductID);
            //model.File.MapViewModelToModel(viewModel.File);
            model.Filename = viewModel.Filename;
            model.ShowInPreview = viewModel.ShowInPreview;
        }
    }

}
