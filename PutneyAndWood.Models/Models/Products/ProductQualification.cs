﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProductQualification : IEntity, IDeletable
    {
        public ProductQualification() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }

        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }

        public bool Deleted { get; set; }

        public virtual Company Company { get; set; }
        public virtual Product Product { get; set; }

        public string ToShortLabel() { return string.Format("{0} - {1}", Product.Description, (Description ?? "").Split('\n')[0].Truncate(50)); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Product.Description, (Description ?? "").Split('\n')[0]); }

    }


    public class ProductQualificationViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public Guid ProductID { get; set; }

        public string Description { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }
        public bool Deleted { get; set; }
    }


    public class ProductQualificationGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }        
        public string ProductID { get; set; }
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }
    }



    public class ProductQualificationMapper : OrderItemModelMapper<ProductQualification, ProductQualificationViewModel>
    {

        public override void MapToViewModel(ProductQualification model, ProductQualificationViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Description = model.Description ?? "";
            viewModel.Deleted = model.Deleted;
            viewModel.IsSubHeading = model.IsSubHeading;
            viewModel.IsSubItem = model.IsSubItem;
        }


        public override void MapToModel(ProductQualificationViewModel viewModel, ProductQualification model)
        {
            model.Description = viewModel.Description;
            model.IsSubHeading = viewModel.IsSubHeading;
            model.IsSubItem = viewModel.IsSubItem;
        }

    }



    public class ProductQualificationGridMapper : OrderItemModelMapper<ProductQualification, ProductQualificationGridViewModel>
    {

        public override void MapToViewModel(ProductQualification model, ProductQualificationGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Description = (model.Description ?? "").Split('\n')[0];
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.SortPos = model.SortPos;
            viewModel.IsSubItem = model.IsSubItem;
            viewModel.IsSubHeading = model.IsSubHeading;
        }

        public override void MapToModel(ProductQualificationGridViewModel viewModel, ProductQualification model)
        {
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.Description = viewModel.Description;
            model.IsSubItem = viewModel.IsSubItem;
            model.IsSubHeading = viewModel.IsSubHeading;
        }

    }
}

