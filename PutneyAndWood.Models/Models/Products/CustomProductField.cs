﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{
    public class CustomProductField : ICustomField
    {
        public CustomProductField() { ID = Guid.NewGuid(); ShowMainList = true; ShowQuotations = true; ShowJobs = true; ShowInvoices = true; ShowDeliveryNotes = true; ShowPurchaseOrders = true; }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public int SortPos { get; set; }

        public bool ShowMainList { get; set; }
        public bool ShowQuotations { get; set; }
        public bool ShowJobs { get; set; }
        public bool ShowInvoices { get; set; }
        public bool ShowDeliveryNotes { get; set; }
        public bool ShowPurchaseOrders { get; set; }

        public virtual Company Company { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }


    public class CustomProductFieldViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public int SortPos { get; set; }
        public bool ShowMainList { get; set; }
    }

    

    public class CustomProductFieldMapper : ModelMapper<CustomProductField, CustomProductFieldViewModel>
    {

        public override void MapToViewModel(CustomProductField model, CustomProductFieldViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;
            viewModel.Label = model.Label;
            viewModel.SortPos = model.SortPos;
            viewModel.ShowMainList = model.ShowMainList;
        }


        public override void MapToModel(CustomProductFieldViewModel viewModel, CustomProductField model)
        {
            viewModel.Name = new Regex("[^A-Za-z0-9 -$\n_]").Replace(viewModel.Name, "");
            viewModel.Name = viewModel.Name.Replace(" ", "_").Replace("__", "_");

            //ID = viewModel.ID,
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.Label = viewModel.Label;
            model.SortPos = viewModel.SortPos;
            model.ShowMainList = viewModel.ShowMainList;

        }

    }

    
}

