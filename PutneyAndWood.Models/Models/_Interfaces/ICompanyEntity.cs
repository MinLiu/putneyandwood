﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface ICompanyEntity 
    {
        Guid CompanyID { get; set; }
        Company Company { get; set; }
    }
    
}
