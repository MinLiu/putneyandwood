﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{
    public interface IAddress: IEntity
    {
        string Name { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string County { get; set; }
        string Town { get; set; }
        string Postcode { get; set; }
        string Country { get; set; }
        string Email { get; set; }
        string Telephone { get; set; }
        string Mobile { get; set; }

        string OwnerName { get; }
    }

}
