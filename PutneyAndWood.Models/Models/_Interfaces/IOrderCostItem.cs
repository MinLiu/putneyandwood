﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    public interface IOrderCostItem<TCostLineItem> : IPrice
        where TCostLineItem : class, IOrderCostItem<TCostLineItem>
    {
        
        string ProductCode { get; set; }
        string Description { get; set; }
        string Category { get; set; }
        string VendorCode { get; set; }
        string Manufacturer { get; set; }
        string Unit { get; set; }
        string AccountCode { get; set; }
        string PurchaseAccountCode { get; set; }
        string JsonCustomFieldValues { get; set; }
        List<CustomField> CustomFieldValues { get; set; }

        string ImageURL { get; set; }
        bool IsKit { get; set; }
        bool UseKitPrice { get; set; }
        Guid? ParentCostItemID { get; set; }
        int SortPos { get; set; }
        bool IsComment { get; set; }
        
        TCostLineItem ParentCostItem { get; set; }

        //Foreign References
        ICollection<TCostLineItem> KitCostItems{ get; set; }
    }
    

}