﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface IOrderLineItem<TSection, TLineItem, TLineItemImage> : IPrice
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>
        where TLineItemImage : class, IImageItem<TLineItem>, new()
    {
        Guid SectionID { get; set; }
        string ProductCode { get; set; }
        string Description { get; set; }
        string Category { get; set; }
        string VendorCode { get; set; }
        string Manufacturer { get; set; }
        string Unit { get; set; }
        string AccountCode { get; set; }
        string PurchaseAccountCode { get; set; }
        string JsonCustomFieldValues { get; set; }
        List<CustomField> CustomFieldValues { get; set; }


        string ImageURL { get; set; }
        bool IsKit { get; set; }
        bool UseKitPrice { get; set; }
        Guid? ParentItemID { get; set; }
        int SortPos { get; set; }
        bool IsComment { get; set; }
        bool HideOnPdf { get; set; }
        bool IsRequired { get; set; }
        

        TSection Section { get; set; }
        TLineItem ParentItem { get; set; }
        ICollection<TLineItem> KitItems { get; set; }
        ICollection<TLineItemImage> Images { get; set; }
    }

    

}
