﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IPrice : IEntity
    {
        decimal Quantity { get; set; }
        decimal SetupCost { get; set; }
        decimal VAT { get; set; }
        decimal VATPrice { get; set; }
        decimal Markup { get; set; }
        decimal Discount { get; set; }
        decimal ListPrice { get; set; }
        decimal Price { get; set; }
        decimal ListCost { get; set; }
        decimal Cost { get; set; }
        decimal Margin { get; set; }
        decimal MarginPrice { get; set; }
        decimal Gross { get; set; }
        decimal Total { get; set; }
    }
    

}
