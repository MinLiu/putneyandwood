﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface IImageItem<T> : IEntity
        where T : class, IEntity
    {        
        Guid OwnerID { get; set; }
        Guid FileID { get; set; }
        FileEntry File { get; set; }
        int SortPos { get; set; }

        T Owner { get; set; }
    }

    

}
