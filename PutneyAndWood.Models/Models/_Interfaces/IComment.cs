﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{
    public interface IComment: IEntity
    {
        DateTime Timestamp { get; set; }
        string Email { get; set; }
        string Message { get; set; }
    }

}
