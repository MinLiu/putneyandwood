﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    public interface IEvent: IEntity
    {
        Guid CompanyID { get; set; }
        string AssignedUserID { get; set; }
        string CreatorID { get; set; }
        //Guid? OpportunityID { get; set; }
        //Guid? ClientID { get; set; }
        //Guid? ClientContactID { get; set; }

        DateTime Timestamp { get; set; }
        string Message { get; set; }
        bool Complete { get; set; }

        Company Company { get; set; }
        //virtual Client Client { get; set; }
        //virtual ClientContact ClientContact { get; set; }

        User AssignedUser { get; set; }
        User Creator { get; set; }
    }

}
