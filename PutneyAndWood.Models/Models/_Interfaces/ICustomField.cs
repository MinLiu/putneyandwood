﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface ICustomField : ICompanyEntity, IEntity
    {        
        string Name { get; set; }
        string Label { get; set; }
        int SortPos { get; set; }
        bool ShowMainList { get; set; }
    }
    

}
