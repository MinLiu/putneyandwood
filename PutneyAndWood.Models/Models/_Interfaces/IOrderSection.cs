﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IOrderSection<TSection, TLineItem, TLineItemImage> : IEntity
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>
        where TLineItemImage : class, IImageItem<TLineItem>, new()
    {
        string Name { get; set; }
        bool IncludeInTotal { get; set; }
        int SortPos { get; set; }

        decimal Discount { get; set; }
        decimal Margin { get; set; }
        decimal TotalVat { get; set; }
        decimal TotalDiscount { get; set; }
        decimal TotalMargin { get; set; }
        decimal TotalCost { get; set; }
        decimal TotalNet { get; set; }
        decimal TotalPrice { get; set; }

        ICollection<TLineItem> Items { get; set; }
    }

    

}
