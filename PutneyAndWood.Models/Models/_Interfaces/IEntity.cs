﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IEntity : ILoggable
    {
        //public IEntity() { ID = Guid.NewGuid(); }
        Guid ID { get; set; }
        
    }


    public interface IEntityViewModel
    {
        [StringLength(128)]
        string ID { get; set; }
    }
    

}


namespace SnapSuite.Models
{
    public class EmptyViewModel : IEntityViewModel
    {
        public string ID { get; set; }
    }
}
