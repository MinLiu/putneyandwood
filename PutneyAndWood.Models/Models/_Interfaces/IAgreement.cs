﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public enum AgreementType {  }


    public interface IAgreement
    {
        string MD5Hash { get; }
        string Title { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        DateTime DateSigned { get; set; }
        string DeviceInformation { get; set; }
        string IP_Address { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        string SignatureImageURL { get; set; }
        Guid? SignatureImageID { get; set; }
        Models.FileEntry SignatureImage { get; set; }
        bool SignedUsingInput { get; set; }
        string PdfAgreementFilename { get; set; }
        string PdfAgreementURL { get; set; }
        Guid? PdfAgreementID { get; set; }
        Models.FileEntry PdfAgreement{ get; set; }
        string PdfApprovedFilename { get; set; }
        string PdfApprovedURL { get; set; }
        Guid? PdfApprovedID { get; set; }
        Models.FileEntry PdfApproved { get; set; }

        void CreateMD5Hash();
    }
    
}
