﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Models
{
    public enum PreviewItemType { Template, Attachment }
}

namespace SnapSuite.Internal
{
    
    public interface IOrderPreviewItem<TOrderPreviewTemplate, TOrderAttachment> : IEntity
        where TOrderPreviewTemplate : class, IOrderPreviewTemplate
        where TOrderAttachment : class, IAttachment
    {
        Guid ParentID { get; set; }
        PreviewItemType Type { get; set; }

        Guid? TemplateID { get; set; }
        TOrderPreviewTemplate Template { get; set; }

        Guid? AttachmentID { get; set; }
        TOrderAttachment Attachment { get; set; }
        
        int SortPos { get; set; }
    }

}
