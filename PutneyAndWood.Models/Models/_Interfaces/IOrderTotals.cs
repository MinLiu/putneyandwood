﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface IOrderTotals
    {        
        decimal TotalVat { get; set; }
        decimal TotalDiscount { get; set; }
        decimal TotalMargin { get; set; }
        decimal TotalCost { get; set; }
        decimal TotalNet { get; set; }
        decimal TotalPrice { get; set; }
    }
    

}
