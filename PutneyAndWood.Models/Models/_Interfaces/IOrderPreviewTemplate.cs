﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface IOrderPreviewTemplate : IEntity, ILoggable, IAttachment
    {
        Guid? CompanyID { get; set; }
        TemplateType Type { get; set; }
        TemplateFormat Format { get; set; }
        Company Company { get; set; }
    }
    
}
