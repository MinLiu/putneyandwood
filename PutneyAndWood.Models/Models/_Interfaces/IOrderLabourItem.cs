﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{
    public interface IOrderLabourItem: IEntity
    {
        string Description { get; set; }

        decimal Hours { get; set; }
        decimal HourlyRate { get; set; }
        decimal Total { get; set; }

        int SortPos { get; set; }
    }

}
