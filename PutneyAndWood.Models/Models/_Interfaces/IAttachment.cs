﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    public interface IAttachment: IEntity
    {
        Guid FileID { get; set; }
        FileEntry File { get; set; }
        string Filename { get; set; }
        //string FilePath { get; set; }
        //string Format { get; set; }
    }


    public interface IAttachmentViewModel: IEntityViewModel
    {
        string Filename { get; set; }
        string FileID { get; set; }
        FileEntryViewModel File { get; set; }
    }

}
