﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public interface IOrder<TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem> : ICompanyEntity, ICurrency, IEntity, IDeletable, ILastUpdated, IOrderTotals
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TAttachment : class, IAttachment
        where TComment : class, IComment
        where TNotification : class, INotification
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>
    {
        string CheckSum { get; set; }
        string Reference { get; set; }
        int Number { get; set; }
        string Description { get; set; }
        string Note { get; set; }
        string PoNumber { get; set; }
        string JsonCustomFieldValues { get; set; }
        List<CustomField> CustomFieldValues { get; set; }


        //One Off Totals
        decimal Discount { get; set; }
        decimal Margin { get; set; }

        /*
        decimal TotalVat { get; set; }
        decimal TotalDiscount { get; set; }
        decimal TotalMargin { get; set; }
        decimal TotalCost { get; set; }
        decimal TotalNet { get; set; }
        decimal TotalPrice { get; set; }
        */

        //Profit & Loss
        decimal TotalOtherCosts { get; set; }
        decimal TotalLabourCosts { get; set; }
        decimal TotalProfit { get; set; }


        string AssignedUserID { get; set; }
        int StatusID { get; set; }
        //int CurrencyID { get; set; }
        DateTime Created { get; set; }
        string CreatedBy { get; set; }
        DateTime? EmailOpened { get; set; }
        

        //Approval Stuff
        bool IsApproved { get; set; }
        User AssignedUser { get; set; }

        Guid ProjectID { get; set; }

        ICollection<TSection> Sections { get; }
        ICollection<TLineItem> Items { get; }
        ICollection<TAttachment> Attachments { get; }
        ICollection<TComment> Comments { get; }
        ICollection<TNotification> Notifications { get; }
        //ICollection<QuotationCostItem> CostItems { get; }
        //ICollection<QuotationLabourItem> LabourItems { get; }  
        ICollection<TPreviewItem> PreviewItems { get; }
    }
    

}
