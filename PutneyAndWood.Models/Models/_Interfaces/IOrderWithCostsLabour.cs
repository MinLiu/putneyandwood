﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IOrderWithCostsLabour<T, TSection, TLineItem, TLineItemImage, TCostItem, TLabourItem, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem> 
        : IOrder<TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>

        where T : class, ICompanyEntity, IEntity
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TCostItem : class, IOrderCostItem<TCostItem>
        where TLabourItem : class, IOrderLabourItem
        where TAttachment : class, IAttachment
        where TComment : class, IComment
        where TNotification : class, INotification
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>
    {    
        
        ICollection<TCostItem> CostItems { get; }
        ICollection<TLabourItem> LabourItems { get; }
    }
    

}

