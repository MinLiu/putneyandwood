﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    
    public interface IOrderStripPayment
    {
        Guid OwnerID { get; set; }

        //Pre Charge details
        string Description { get; set; }
        bool FixedAmount { get; set; }
        decimal? PercentageToPay { get; set; }
        decimal? AmountToPay { get; set; }

        bool IsPaid { get; set; }
        DateTime? DateOfPayment { get; set; }
        string StripeChargeID { get; set; }
        //string StripeCustomerID { get; set; }
        //string StripeDescription { get; set; }
        //string StripeCurrency { get; set; }
        //decimal? StripeAmount { get; set; }
        //DateTime? StripeDateCreated { get; set; }
        
    }
    
}
