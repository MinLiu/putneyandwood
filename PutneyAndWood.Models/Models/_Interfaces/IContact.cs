﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{
    public interface IContact: IEntity
    {
        string Title { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Position { get; set; }
        string Telephone { get; set; }
        string Mobile { get; set; }
        string Email { get; set; }

        string OwnerName { get; }
    }

}
