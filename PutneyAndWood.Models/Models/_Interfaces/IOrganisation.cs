﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IOrganisation
    {
        
        string Name { get; set; }
        Guid? TypeID { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string Town { get; set; }
        string County { get; set; }
        string Postcode { get; set; }
        string Country { get; set; }
        string Email { get; set; }
        string Telephone { get; set; }
        string Mobile { get; set; }
        string Website { get; set; }
        string Notes { get; set; }
        string SageAccountReference { get; set; }

        DateTime Created { get; set; }
        

        bool Deleted { get; set; }   //Marker used to place in the recycle bin.

        string TypeName();
    }

    

}
