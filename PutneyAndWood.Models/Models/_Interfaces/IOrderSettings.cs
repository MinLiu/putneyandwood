﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IOrderSettings<TAttachment, TNotification> : ICompanyEntity
        where TAttachment : class, IAttachment
        where TNotification : class, INotification
    {        
        string DefaultReference { get; set; }
        int StartingNumber { get; set; }
        string DefaultNote { get; set; }

        string DefaultFromEmail { get; set; }
        string EmailTitleTemplate { get; set; }
        string EmailBodyTemplate { get; set; }

        ICollection<TAttachment> DefaultAttachments { get; set; }
        ICollection<TNotification> DefaultNotifications { get; set; }
    }
    
}
