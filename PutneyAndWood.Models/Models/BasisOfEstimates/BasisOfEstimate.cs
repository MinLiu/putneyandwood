﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Mvc;

namespace SnapSuite.Models
{
    public class BasisOfEstimate : IEntity
    {
        public BasisOfEstimate() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }

        public string ToShortLabel() { return (Description ?? "").Split('\n')[0].Truncate(50); }
        public string ToLongLabel() { return (Description ?? "").Split('\n')[0].Truncate(50); }

    }


    public class BasisOfEstimateGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }
    }



    public class BasisOfEstimateGridMapper : ModelMapper<BasisOfEstimate, BasisOfEstimateGridViewModel>
    {

        public override void MapToViewModel(BasisOfEstimate model, BasisOfEstimateGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Description = model.Description;
            viewModel.IsSubHeading = model.IsSubHeading;
            viewModel.IsSubItem = model.IsSubItem;
        }


        public override void MapToModel(BasisOfEstimateGridViewModel viewModel, BasisOfEstimate model)
        {
            model.Description = viewModel.Description;
            model.IsSubHeading = viewModel.IsSubHeading;
            model.IsSubItem = viewModel.IsSubItem;
        }

    }
}

