﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class InternalLog : ICompanyEntity, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ActivityByUserName { get; set; }
        public string ActivityByUserID { get; set; }

        public virtual Company Company { get; set; }
        //public virtual User ActivityByUser { get; set; }

        public string ToShortLabel() { return string.Format("{0} - {1}", Timestamp.ToString("dd/MM/yy"), Description); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Timestamp.ToString("HH:mm dd/MM/yy"), Description); }
    }


    public class InternalLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        public DateTime Timestamp { get; set; }

        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ActivityByUserID { get; set; }
        public string ActivityByUserName { get; set; }

    }

    public class InternalLogMapper : ModelMapper<InternalLog, InternalLogViewModel>
    {
        public override void MapToViewModel(InternalLog model, InternalLogViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.Type = model.Type;
            viewModel.Title = model.Title;
            viewModel.Description = model.Description;
            viewModel.ActivityByUserID = model.ActivityByUserID;
            viewModel.ActivityByUserName = model.ActivityByUserName;
            //viewModel.ActivityByUserName = (model.ActivityByUser != null) ? string.Format("{0} {1} - {2}", model.ActivityByUser.FirstName, model.ActivityByUser.LastName, model.ActivityByUser.Email) : model.ActivityByUserID;
        }


        public override void MapToModel(InternalLogViewModel viewModel, InternalLog model)
        {
            throw new NotImplementedException();
        }

    }

}
