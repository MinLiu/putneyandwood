﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ActivityLog : ICompanyEntity, IEntity
    {
        
        //public ActivityLog() { ID = Guid.NewGuid(); }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public DateTime Timestamp { get; set; }
        public int ModuleTypeID { get; set; }
        public string Description { get; set; }
        public string ActivityByUserName { get; set; }
        public string ActivityByUserID { get; set; }

        public string LinkID { get; set; }

        public virtual Company Company { get; set; }
        public virtual ModuleType ModuleType { get; set; }
        //public virtual User ActivityByUser { get; set; }

        public string ToShortLabel() { return string.Format("{0} - {1}", Timestamp.ToString("dd/MM/yy"), Description); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Timestamp.ToString("HH:mm dd/MM/yy"), Description); }
    }


    public class ActivityLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        public DateTime Timestamp { get; set; }
        public int ModuleTypeID { get; set; }
        public string ModuleTypeName { get; set; }

        public string Description { get; set; }
        public string ActivityByUserID { get; set; }
        public string ActivityByUserName { get; set; }

        public string LinkID { get; set; }
        public string LinkURL { get; set; }
    }

    public class ActivityLogMapper : ModelMapper<ActivityLog, ActivityLogViewModel>
    {

        public override void MapToViewModel(ActivityLog model, ActivityLogViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.ModuleTypeID = model.ModuleType.ID;
            viewModel.ModuleTypeName = model.ModuleType.Name;
            viewModel.Description = model.Description;
            viewModel.ActivityByUserID = model.ActivityByUserID;
            viewModel.ActivityByUserName = model.ActivityByUserName;
            //viewModel.ActivityByUserName = (model.ActivityByUser != null) ? string.Format("{0} {1} - {2}", model.ActivityByUser.FirstName, model.ActivityByUser.LastName, model.ActivityByUser.Email) : model.ActivityByUserID;
            viewModel.LinkID = model.LinkID;
            viewModel.LinkURL = (string.IsNullOrEmpty(model.LinkID)) ? model.ModuleType.IndexURL : string.Format("{0}{1}", model.ModuleType.EditURL, model.LinkID);
        }


        public override void MapToModel(ActivityLogViewModel viewModel, ActivityLog model)
        {
            model.Timestamp = viewModel.Timestamp;
            model.ModuleTypeID = viewModel.ModuleTypeID;
            model.Description = viewModel.Description;
            model.ActivityByUserID = viewModel.ActivityByUserID;
            model.ActivityByUserName = viewModel.ActivityByUserName;
            model.LinkID = viewModel.LinkID;
        }

    }

}
