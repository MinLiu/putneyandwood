﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class StockLog : IEntity, ICompanyEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public DateTime Timestamp { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public string LocationName { get; set; }
        public decimal Quantity { get; set; }
        public StockActionType ActionType { get; set; }
        public string Narrative { get; set; }
        public string ActivityByUserID { get; set; }

        public int ModuleTypeID { get; set; }
        public string LinkID { get; set; }

        public virtual Company Company { get; set; }
        public virtual User ActivityByUser { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }


    public class StockLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        public DateTime Timestamp { get; set; }

        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public string ActivityByUserID { get; set; }
        public string ActivityByUserName { get; set; }
        public decimal Quantity { get; set; }
        public StockActionType ActionType { get; set; }
        public string LocationName { get; set; }
        public string Narrative { get; set; }
        public string LinkID { get; set; }
        public string LinkURL { get; set; }
    }

    public class StockLogMapper : ModelMapper<StockLog, StockLogViewModel>
    {
        public override void MapToModel(StockLogViewModel viewModel, StockLog model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(StockLog model, StockLogViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.ActivityByUserID = model.ActivityByUserID;
            viewModel.ActivityByUserName = (model.ActivityByUser != null) ? string.Format("{0} {1} - {2}", model.ActivityByUser.FirstName, model.ActivityByUser.LastName, model.ActivityByUser.Email) : model.ActivityByUserID;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.ProductDescription = model.ProductDescription;
            viewModel.ProductCode = model.ProductCode;
            viewModel.LinkID = model.LinkID;
            viewModel.LinkURL = string.IsNullOrWhiteSpace(model.LinkID) ? "#" : string.Format("{0}{1}", new SnapDbContext().ModuleTypes.Where(m => m.ID == model.ModuleTypeID).First().EditURL, model.LinkID);
            viewModel.Quantity = model.Quantity;
            viewModel.ActionType = model.ActionType;
            viewModel.Narrative = model.Narrative;
            viewModel.LocationName = model.LocationName;
        }
    }

}
