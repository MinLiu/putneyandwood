﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ErrorLogRepository : EntityRespository<ErrorLog>
    {
        public ErrorLogRepository()
            : this(new SnapDbContext())
        {

        }

        public ErrorLogRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
