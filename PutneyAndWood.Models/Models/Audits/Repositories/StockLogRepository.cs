﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class StockLogRepository : EntityRespository<StockLog>
    {
        public StockLogRepository()
            : this(new SnapDbContext())
        {

        }

        public StockLogRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
