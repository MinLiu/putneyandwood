﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class InternalLogRepository : EntityRespository<InternalLog>
    {
        public InternalLogRepository()
            : this(new SnapDbContext())
        {

        }

        public InternalLogRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
