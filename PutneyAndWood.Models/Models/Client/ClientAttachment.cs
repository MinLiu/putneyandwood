﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ClientAttachment : IAttachment
    {
        public ClientAttachment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ClientID { get; set; }
        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public string Filename { get; set; }
        
        public virtual Client Client { get; set; }


        public string ToShortLabel() { return Filename; }
        public string ToLongLabel() { return Filename; }
    }




    public class ClientAttachmentViewModel : IAttachmentViewModel
    {
        public string ID { get; set; }
        public string ClientID { get; set; }

        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }
        [Required(ErrorMessage = "* The name filed is required.")]
        public string Filename { get; set; }
    }




    public class ClientAttachmentMapper : ModelMapper<ClientAttachment, ClientAttachmentViewModel>
    {
        public override void MapToViewModel(ClientAttachment model, ClientAttachmentViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.Filename = model.Filename;
        }

        public override void MapToModel(ClientAttachmentViewModel viewModel, ClientAttachment model)
        {
            //ID = viewModel.ID;
            model.ClientID = Guid.Parse(viewModel.ClientID);
            //model.FileID = Guid.Parse(viewModel.ProductID);
            //model.File.MapViewModelToModel(viewModel.File);
            model.Filename = viewModel.Filename;
        }

    }

}
