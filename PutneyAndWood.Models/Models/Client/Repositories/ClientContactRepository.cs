﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ClientContactRepository : EntityRespository<ClientContact>
    {
        public ClientContactRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientContactRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(ClientContact entity)
        {
            var set = _db.Set<ClientContact>().Where(a => a.ID == entity.ID)
                                    .Include(a => a.ClientEvents)
                                    .Include(a => a.Quotations);

            _db.Set<ClientContact>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
