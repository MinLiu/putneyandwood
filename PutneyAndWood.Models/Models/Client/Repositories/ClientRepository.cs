﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ClientRepository : EntityRespository<Client>
    {
        public ClientRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Client entity)
        {

            _db.Set<ClientAddress>().RemoveRange(_db.Set<ClientAddress>()
                                                    .Where(x => x.ClientID == entity.ID)
                                                    .Include(x => x.DeliveryQuotations)
                                                    .Include(x => x.InvoiceQuotations)
            );

            _db.Set<ClientAttachment>().RemoveRange(_db.Set<ClientAttachment>().Where(x => x.ClientID == entity.ID));
            _db.Set<ClientContact>().RemoveRange(_db.Set<ClientContact>().Where(x => x.ClientID == entity.ID));
            _db.Set<ClientTagItem>().RemoveRange(_db.Set<ClientTagItem>().Where(x => x.ClientID == entity.ID));
            _db.Set<ClientEvent>().RemoveRange(_db.Set<ClientEvent>().Where(x => x.ClientID == entity.ID));


            var set = _db.Set<Client>().Where(a => a.ID == entity.ID)
                                    .Include(a => a.Quotations);
            _db.Set<Client>().RemoveRange(set);
            _db.SaveChanges();
        }


    }

}
