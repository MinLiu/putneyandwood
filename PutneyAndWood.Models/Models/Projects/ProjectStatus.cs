﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class ProjectStatus : ILoggable
    {
        [Key]
        public int ID { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }

        public string ToLongLabel() { return Name; }
        public string ToShortLabel() { return Name; }
    }

    public static class ProjectStatusValues
    {
        public static int Declined = 1;
        public static int PreTender = 2;
        public static int OutToTender = 3;
        public static int MainContractAwarded = 4;
        public static int OnHold = 5;
        public static int Dead = 6;
        public static int Won = 7;
        public static int Lost = 8;

        public static List<int> All = new List<int>()
        {
            Declined,
            PreTender,
            OutToTender,
            MainContractAwarded,
            OnHold,
            Dead,
            Won,
            Lost
        };
        /*         
        public static int DRAFT = 1;
        public static int SENT = 2;
        public static int ACCEPTED = 3;
        public static int DECLINED = 4;
        public static int COMMENCED = 5;
        public static int COMPLETE = 6;         
         */
    }



    public class ProjectStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
    }




    public class ProjectStatusMapper
    {
        public static void MapToViewModel(ProjectStatus model, ProjectStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.HexColour = model.HexColour;
        }

        public static ProjectStatusViewModel MapToViewModel(ProjectStatus model)
        {
            var viewModel = new ProjectStatusViewModel();
            MapToViewModel(model, viewModel);

            return viewModel;
        }
    }
        
}

