﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class ProjectScope
    {
        [Key, Column(Order = 0)]
        public Guid ProjectID { get; set; }
        [Key, Column(Order = 1)]
        public int ScopeID { get; set; }

        public virtual Project Project { get; set; }
        public virtual Scope Scope { get; set; }
    }

}
