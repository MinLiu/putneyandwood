﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class Project : ICompanyEntity, IDeletable
    {
        public Project() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public string AssignedUserID { get; set; }
        public int StatusID { get; set; }
        
        public string Name { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public DateTime? StoneworkStartDate { get; set; }
        public decimal Value { get; set; }
        //public string Note { get; set; }

        public Guid? Client1ID { get; set; }
        public Guid? ClientContact1ID { get; set; }
        public Guid? Client2ID { get; set; }
        public Guid? ClientContact2ID { get; set; }
        public Guid? Client3ID { get; set; }
        public Guid? ClientContact3ID { get; set; }
        public Guid? Client4ID { get; set; }
        public Guid? ClientContact4ID { get; set; }
        public Guid? Consultant1ID { get; set; }
        public Guid? ConsultantContact1ID { get; set; }
        public Guid? Consultant2ID { get; set; }
        public Guid? ConsultantContact2ID { get; set; }
        public Guid? Consultant3ID { get; set; }
        public Guid? ConsultantContact3ID { get; set; }
        public Guid? Consultant4ID { get; set; }
        public Guid? ConsultantContact4ID { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool Deleted { get; set; }
        public int? EnquirySourceID { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
        public virtual Company Company { get; set; }
        public virtual User AssignedUser { get; set; }
        public virtual ProjectStatus Status { get; set; }
        public virtual ICollection<Quotation> Quotations { get; set; }
        public virtual Client Client1 { get; set; }
        public virtual ClientContact ClientContact1 { get; set; }
        public virtual Client Client2 { get; set; }
        public virtual ClientContact ClientContact2 { get; set; }
        public virtual Client Client3 { get; set; }
        public virtual ClientContact ClientContact3 { get; set; }
        public virtual Client Client4 { get; set; }
        public virtual ClientContact ClientContact4 { get; set; }
        public virtual Client Consultant1 { get; set; }
        public virtual ClientContact ConsultantContact1 { get; set; }
        public virtual Client Consultant2 { get; set; }
        public virtual ClientContact ConsultantContact2 { get; set; }
        public virtual Client Consultant3 { get; set; }
        public virtual ClientContact ConsultantContact3 { get; set; }
        public virtual Client Consultant4 { get; set; }
        public virtual ClientContact ConsultantContact4 { get; set; }
        public virtual EnquirySource EnquirySource { get; set; }

        public virtual ICollection<ProjectScope> ProjectScopes { get; set; }
    }

    public class ProjectGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public DateTime? StoneworkStartDate { get; set; }
        public decimal Value { get; set; }
        public string AssignedUserName { get; set; }
        public ProjectStatusViewModel Status { get; set; }
        public DateTime Created { get; set; }
    }

    public class ProjectViewModel : IEntityViewModel, IValidatableObject
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int Number { get; set; }
        public DateTime? StoneworkStartDate { get; set; }
        public decimal Value { get; set; }
        public string AssignedUserID { get; set; }
        public ProjectStatusViewModel Status { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdated { get; set; }

        public string Client1ID { get; set; }
        public string ClientContact1ID { get; set; }
        public string Client2ID { get; set; }
        public string ClientContact2ID { get; set; }
        public string Client3ID { get; set; }
        public string ClientContact3ID { get; set; }
        public string Client4ID { get; set; }
        public string ClientContact4ID { get; set; }
        public string ClientContact1Telephone { get; set; }
        public string ClientContact2Telephone { get; set; }
        public string ClientContact3Telephone { get; set; }
        public string ClientContact4Telephone { get; set; }
        public string Consultant1ID { get; set; }
        public string ConsultantContact1ID { get; set; }
        public string Consultant2ID { get; set; }
        public string ConsultantContact2ID { get; set; }
        public string Consultant3ID { get; set; }
        public string ConsultantContact3ID { get; set; }
        public string Consultant4ID { get; set; }
        public string ConsultantContact4ID { get; set; }
        public string ConsultantContact1Telephone { get; set; }
        public string ConsultantContact2Telephone { get; set; }
        public string ConsultantContact3Telephone { get; set; }
        public string ConsultantContact4Telephone { get; set; }
        public List<int> Scopes { get; set; }
        public int? EnquirySourceID { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (Value != 0 && StoneworkStartDate == null)
            {
                results.Add(new ValidationResult("Must set stonework start date when setting stonework value", new List<string> { "StoneworkStartDate" }));
            }
            return results;
        }
    }

    public class ProjectGridMapper : ModelMapper<Project, ProjectGridViewModel>
    {
        public override void MapToModel(ProjectGridViewModel viewModel, Project model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(Project model, ProjectGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Status = ProjectStatusMapper.MapToViewModel(model.Status);
            viewModel.Name = model.Name;
            viewModel.Number = model.Number;
            viewModel.Value = model.Value;
            viewModel.StoneworkStartDate = model.StoneworkStartDate;
            viewModel.AssignedUserName = model.AssignedUser != null ? model.AssignedUser.ToFullName() : "";
            viewModel.Created = model.Created;
        }
    }

    public class ProjectMapper : ModelMapper<Project, ProjectViewModel>
    {
        protected ProjectScopeService ProjectScopeService = new ProjectScopeService(new SnapDbContext());
        public override void MapToModel(ProjectViewModel viewModel, Project model)
        {
            model.Name = viewModel.Name;
            model.Number = viewModel.Number;
            model.Value = viewModel.Value;
            model.StoneworkStartDate = viewModel.StoneworkStartDate;
            model.Description = viewModel.Description;
            model.AssignedUserID = viewModel.AssignedUserID;
            model.Note = viewModel.Note;
            model.Client1ID = !string.IsNullOrEmpty(viewModel.Client1ID)? Guid.Parse(viewModel.Client1ID) : null as Guid?;
            model.Client2ID = !string.IsNullOrEmpty(viewModel.Client2ID)? Guid.Parse(viewModel.Client2ID) : null as Guid?;
            model.Client3ID = !string.IsNullOrEmpty(viewModel.Client3ID)? Guid.Parse(viewModel.Client3ID) : null as Guid?;
            model.Client4ID = !string.IsNullOrEmpty(viewModel.Client4ID)? Guid.Parse(viewModel.Client4ID) : null as Guid?;
            model.ClientContact1ID = !string.IsNullOrEmpty(viewModel.ClientContact1ID) ? Guid.Parse(viewModel.ClientContact1ID) : null as Guid?;
            model.ClientContact2ID = !string.IsNullOrEmpty(viewModel.ClientContact2ID) ? Guid.Parse(viewModel.ClientContact2ID) : null as Guid?;
            model.ClientContact3ID = !string.IsNullOrEmpty(viewModel.ClientContact3ID) ? Guid.Parse(viewModel.ClientContact3ID) : null as Guid?;
            model.ClientContact4ID = !string.IsNullOrEmpty(viewModel.ClientContact4ID) ? Guid.Parse(viewModel.ClientContact4ID) : null as Guid?;
            model.Consultant1ID = !string.IsNullOrEmpty(viewModel.Consultant1ID) ? Guid.Parse(viewModel.Consultant1ID) : null as Guid?;
            model.Consultant2ID = !string.IsNullOrEmpty(viewModel.Consultant2ID) ? Guid.Parse(viewModel.Consultant2ID) : null as Guid?;
            model.Consultant3ID = !string.IsNullOrEmpty(viewModel.Consultant3ID) ? Guid.Parse(viewModel.Consultant3ID) : null as Guid?;
            model.Consultant4ID = !string.IsNullOrEmpty(viewModel.Consultant4ID) ? Guid.Parse(viewModel.Consultant4ID) : null as Guid?;
            model.ConsultantContact1ID = !string.IsNullOrEmpty(viewModel.ConsultantContact1ID) ? Guid.Parse(viewModel.ConsultantContact1ID) : null as Guid?;
            model.ConsultantContact2ID = !string.IsNullOrEmpty(viewModel.ConsultantContact2ID) ? Guid.Parse(viewModel.ConsultantContact2ID) : null as Guid?;
            model.ConsultantContact3ID = !string.IsNullOrEmpty(viewModel.ConsultantContact3ID) ? Guid.Parse(viewModel.ConsultantContact3ID) : null as Guid?;
            model.ConsultantContact4ID = !string.IsNullOrEmpty(viewModel.ConsultantContact4ID) ? Guid.Parse(viewModel.ConsultantContact4ID) : null as Guid?;
            model.EnquirySourceID = viewModel.EnquirySourceID;

            // Handl multi select scopes
            var oldScopeIDs = model.ProjectScopes.Select(x => x.ScopeID).ToList();
            var newScopeIDs = viewModel.Scopes ?? new List<int>();
            foreach (var scopeID in oldScopeIDs.Except(newScopeIDs))
            {
                ProjectScopeService.Destroy(new ProjectScope { ProjectID = model.ID, ScopeID = scopeID });
            }

            foreach (var scopeID in newScopeIDs.Except(oldScopeIDs))
            {
                ProjectScopeService.Create(new ProjectScope { ProjectID = model.ID, ScopeID = scopeID });
            }
        }

        public override void MapToViewModel(Project model, ProjectViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Status = ProjectStatusMapper.MapToViewModel(model.Status);
            viewModel.Name = model.Name;
            viewModel.Number = model.Number;
            viewModel.Value = model.Value;
            viewModel.StoneworkStartDate = model.StoneworkStartDate;
            viewModel.Description = model.Description;
            viewModel.AssignedUserID = model.AssignedUserID;
            viewModel.Note = model.Note;
            viewModel.Created = model.Created;
            viewModel.LastUpdated = model.LastUpdated;
            viewModel.EnquirySourceID = model.EnquirySourceID;

            viewModel.Client1ID = model.Client1ID.ToString();
            viewModel.Client2ID = model.Client2ID.ToString();
            viewModel.Client3ID = model.Client3ID.ToString();
            viewModel.Client4ID = model.Client4ID.ToString();
            viewModel.ClientContact1ID = model.ClientContact1ID.ToString();
            viewModel.ClientContact2ID = model.ClientContact2ID.ToString();
            viewModel.ClientContact3ID = model.ClientContact3ID.ToString();
            viewModel.ClientContact4ID = model.ClientContact4ID.ToString();

            viewModel.ClientContact1Telephone = model.ClientContact1 != null ? (!string.IsNullOrEmpty(model.ClientContact1.Telephone)? model.ClientContact1.Telephone : (!string.IsNullOrEmpty(model.ClientContact1.Mobile) ? model.ClientContact1.Mobile : "")) : "";
            viewModel.ClientContact2Telephone = model.ClientContact2 != null ? (!string.IsNullOrEmpty(model.ClientContact2.Telephone)? model.ClientContact2.Telephone : (!string.IsNullOrEmpty(model.ClientContact2.Mobile) ? model.ClientContact2.Mobile : "")) : "";
            viewModel.ClientContact3Telephone = model.ClientContact3 != null ? (!string.IsNullOrEmpty(model.ClientContact3.Telephone)? model.ClientContact3.Telephone : (!string.IsNullOrEmpty(model.ClientContact3.Mobile) ? model.ClientContact3.Mobile : "")) : "";
            viewModel.ClientContact4Telephone = model.ClientContact4 != null ? (!string.IsNullOrEmpty(model.ClientContact4.Telephone)? model.ClientContact4.Telephone : (!string.IsNullOrEmpty(model.ClientContact4.Mobile) ? model.ClientContact4.Mobile : "")) : "";
            viewModel.Scopes = model.ProjectScopes.Select(x => x.ScopeID).ToList();

            viewModel.Consultant1ID = model.Consultant1ID.ToString();
            viewModel.Consultant2ID = model.Consultant2ID.ToString();
            viewModel.Consultant3ID = model.Consultant3ID.ToString();
            viewModel.Consultant4ID = model.Consultant4ID.ToString();
            viewModel.ConsultantContact1ID = model.ConsultantContact1ID.ToString();
            viewModel.ConsultantContact2ID = model.ConsultantContact2ID.ToString();
            viewModel.ConsultantContact3ID = model.ConsultantContact3ID.ToString();
            viewModel.ConsultantContact4ID = model.ConsultantContact4ID.ToString();
            viewModel.ConsultantContact1Telephone = model.ConsultantContact1ID != null ? (!string.IsNullOrEmpty(model.ConsultantContact1.Telephone)? model.ConsultantContact1.Telephone : (!string.IsNullOrEmpty(model.ConsultantContact1.Mobile) ? model.ConsultantContact1.Mobile : "")) : "";
            viewModel.ConsultantContact2Telephone = model.ConsultantContact2ID != null ? (!string.IsNullOrEmpty(model.ConsultantContact2.Telephone)? model.ConsultantContact2.Telephone : (!string.IsNullOrEmpty(model.ConsultantContact2.Mobile) ? model.ConsultantContact2.Mobile : "")) : "";
            viewModel.ConsultantContact3Telephone = model.ConsultantContact3ID != null ? (!string.IsNullOrEmpty(model.ConsultantContact3.Telephone)? model.ConsultantContact3.Telephone : (!string.IsNullOrEmpty(model.ConsultantContact3.Mobile) ? model.ConsultantContact3.Mobile : "")) : "";
            viewModel.ConsultantContact4Telephone = model.ConsultantContact4ID != null ? (!string.IsNullOrEmpty(model.ConsultantContact4.Telephone)? model.ConsultantContact4.Telephone : (!string.IsNullOrEmpty(model.ConsultantContact4.Mobile) ? model.ConsultantContact4.Mobile : "")) : "";
        }
    }
}
