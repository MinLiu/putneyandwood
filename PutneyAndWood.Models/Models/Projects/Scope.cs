﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class Scope
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public virtual ICollection<ProjectScope> ProjectScopes { get; set; }
    }

    public class ScopeGridViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class ScopeGridMapper : ModelMapper<Scope, ScopeGridViewModel>
    {
        public override void MapToModel(ScopeGridViewModel viewModel, Scope model)
        {
            model.Name = viewModel.Name;
        }

        public override void MapToViewModel(Scope model, ScopeGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
        }
    }

}
