﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class EventViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationName { get; set; }

        public string EventAssignedUserID { get; set; }
        public string EventAssignedUserName { get; set; }

        public string EventCreatorID { get; set; }
        public string EventCreatorName { get; set; }

        public string EventClientID { get; set; }
        public string EventClientName { get; set; }

        public string EventContactID { get; set; }
        public string EventContactName { get; set; }

        public string EventProjectID { get; set; }
        public string EventProjectName { get; set; }


        [Required(ErrorMessage = "* The Timestamp is required.")]
        public DateTime Timestamp { get; set; }

        [Required(ErrorMessage = "* The Message is required.")]
        public string Message { get; set; }

        public bool Complete { get; set; }
        public bool Overdue { get; set; }
        public bool Due { get; set; }

    }

    public class EventMapper : ModelMapper<QuotationEvent, EventViewModel>
    {
        public override void MapToViewModel(QuotationEvent model, EventViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.EventClientID = model.ClientID.HasValue ? model.ClientID.ToString() : null;
            viewModel.EventClientName = model.Client != null ? model.Client.Name : "";
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Complete = model.Complete;
            viewModel.EventContactID = model.ClientContactID.HasValue ? model.ClientContactID.ToString() : null;
            viewModel.EventContactName = model.ClientContact != null ? string.Format("{0} {1} {2}", model.ClientContact.Title, model.ClientContact.FirstName, model.ClientContact.LastName) : "";
            viewModel.EventAssignedUserID = model.AssignedUserID;
            viewModel.EventAssignedUserName = model.AssignedUser != null ? model.AssignedUser.ToFullName() : "";
            viewModel.EventCreatorID = model.CreatorID;
            viewModel.EventCreatorName = model.Creator != null ? model.Creator.ToFullName() : "";
            viewModel.Message = model.Message;
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.Due = !viewModel.Complete && (model.Timestamp >= DateTime.Today && model.Timestamp < DateTime.Today.AddDays(1.0));
            viewModel.Overdue = !viewModel.Complete && model.Timestamp < DateTime.Today;
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationName = model.Quotation != null ? model.Quotation.Description : "";
            viewModel.EventProjectID = model.ProjectID.ToString();
            viewModel.EventProjectName = model.Project != null ? model.Project.Name : "";
        }

        public override void MapToModel(EventViewModel viewModel, QuotationEvent model)
        {
            model.ID = Guid.Parse(viewModel.ID);
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.AssignedUserID = viewModel.EventAssignedUserID;
            model.ClientContactID = !string.IsNullOrWhiteSpace(viewModel.EventContactID) ? Guid.Parse(viewModel.EventContactID) : null as Guid?;
            model.ClientID = !string.IsNullOrWhiteSpace(viewModel.EventClientID) ? Guid.Parse(viewModel.EventClientID) : null as Guid?;
            model.Complete = viewModel.Complete;
            model.Message = viewModel.Message;
            model.QuotationID = !string.IsNullOrEmpty(viewModel.QuotationID) ? Guid.Parse(viewModel.QuotationID) : null as Guid?;
            model.ProjectID = !string.IsNullOrEmpty(viewModel.EventProjectID) ? Guid.Parse(viewModel.EventProjectID) : null as Guid?;
            model.Timestamp = viewModel.Timestamp;
        }
    }
}
