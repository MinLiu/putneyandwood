﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class QuotationClientViewModel : IEntityViewModel
    {
        //[Key]
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyImageURL { get; set; }
        public string Reference { get; set; }
        public string CheckSum { get; set; }

        public int StatusID { get; set; }
        public string StatusHexColour { get; set; }
        public string StatusDescription { get; set; }
        public string ClientReference { get; set; }
        public string Description { get; set; }

        public string ClientName { get; set; }
        public string ClientContactName { get; set; }
        public string InvoiceAddress { get; set; }
        public string DeliveryAddress { get; set; }
        public string Note { get; set; }
        
        public decimal TotalVat { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }
        
        public string CurrencySymbol { get; set; }
        public DateTime Created { get; set; }

        [Display(Name = "Delivery Date")]
        public DateTime EstimatedDelivery { get; set; }        
        public Guid? MainVersionID { get; set; }

        public bool AllowClientEditLineItems { get; set; }
        public bool AllowClientEditSections { get; set; }
        public bool NeedsCheckedClientSubmission { get; set; }        

        public bool HasAgreement { get; set; }
        public bool UsePriceBreaks { get; set; }

        public IEnumerable<QuotationSectionClientViewModel> Sections { get; set; }
        public List<QuotationAttachmentViewModel> Attachments { get; set; }

        public bool StripeIsPaid { get; set; }
        public bool StripePaymentEnabled { get; set; }
        public string StripeKey { get; set; }
        public string StripeDescription { get; set; }
        public string StripeCurrency { get; set; }
        public decimal StripeAmount { get; set; }
        public string StripeCreated { get; set; }

    }
    


    public class QuotationClientMapper : OrderModelMapper<Quotation, CustomQuotationField, QuotationClientViewModel>
    {

        QuotationSectionClientMapper mapper = new QuotationSectionClientMapper();

        public override void MapToViewModel(Quotation model, QuotationClientViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CheckSum = model.CheckSum;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.CompanyImageURL = (model.Company.LogoImage != null) ? model.Company.LogoImage.FilePath : "";
            viewModel.ClientName = (model.Client != null) ? model.Client.Name : "";
            viewModel.ClientContactName = (model.ClientContact != null) ? model.ClientContact.ToLongLabel() : "";
            viewModel.StatusID = model.StatusID;
            viewModel.StatusDescription = model.Status.Description;
            viewModel.StatusHexColour = model.Status.HexColour;
            viewModel.HasAgreement = model.SignedAgreement != null;
            viewModel.Reference = model.ToShortLabel();
            viewModel.ClientReference = model.ClientReference ?? "";            
            viewModel.Description = model.Description ?? "";
            viewModel.Note = model.Note ?? "";
            viewModel.InvoiceAddress = (model.InvoiceAddress != null) ? model.InvoiceAddress.ToShortLabel() : (model.Client != null) ? model.Client.ToAddressShortLabel() : "";
            viewModel.DeliveryAddress = (model.DeliveryAddress != null) ? model.DeliveryAddress.ToLongLabel() : "";
            viewModel.Created = model.Created;
            viewModel.EstimatedDelivery = model.EstimatedDelivery;
            viewModel.MainVersionID = model.MainVersionID;
            
            viewModel.TotalVat = model.TotalVat;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalPrice = model.TotalPrice;            
            viewModel.CurrencySymbol = model.Currency.Symbol;
            
            viewModel.AllowClientEditLineItems = model.AllowClientEditLineItems;
            viewModel.AllowClientEditSections = model.AllowClientEditSections;
            viewModel.NeedsCheckedClientSubmission = model.NeedsCheckedClientSubmission;
            viewModel.UsePriceBreaks = model.UsePriceBreaks;

            viewModel.Sections = model.Sections.OrderBy(s => s.SortPos).Select(s => mapper.MapToViewModel(s)).ToList();

            viewModel.Attachments = model.Attachments.Where(i => i.ShowInPreview == true).Select(a => new QuotationAttachmentViewModel
                                                                {
                                                                    ID = a.ID.ToString(),
                                                                    QuotationID = a.QuotationID.ToString(),
                                                                    Filename = a.Filename,
                                                                    FileID = a.FileID.ToString(),
                                                                    File = a.File.ToViewModel()
                                                                }).ToList();

        }


        public override void MapToModel(QuotationClientViewModel viewModel, Quotation model)
        {
            throw new NotImplementedException();
        }


        public override void MapToModelHeader(QuotationClientViewModel viewModel, Quotation model)
        {
            throw new NotImplementedException();
        }

    }

    public class QuotationSectionClientViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SectionID { get; set; }
        public string QuotationID { get; set; }
        public string Name { get; set; }
        public bool IncludeInTotal { get; set; }
        public bool IsRequired { get; set; }

        //Section Totals
        public string CurrencySymbol { get; set; }
        public decimal TotalVat { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }

    }

    public class QuotationSectionClientMapper : ModelMapper<QuotationSection, QuotationSectionClientViewModel>
    {

        public override void MapToViewModel(QuotationSection model, QuotationSectionClientViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SectionID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Name = model.Name;
            viewModel.IncludeInTotal = model.IncludeInTotal;
            viewModel.IsRequired = model.IsRequired;

            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.TotalVat = model.TotalVat;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalPrice = model.TotalPrice;
        }


        public override void MapToModel(QuotationSectionClientViewModel viewModel, QuotationSection model)
        {
            model.IncludeInTotal = viewModel.IncludeInTotal;
        }
    }


    public class QuotationItemClientViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationItemID { get; set; }
        public string PriceBreakID { get; set; }
        public string SectionID { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }

        public decimal ParentQuantity { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        
        public string ImageURL { get; set; }
        public int NumImages { get; set; }

        public bool IsKit { get; set; }
        public bool IsKitItem { get; set; }
        public bool IsComment { get; set; }
        public bool IsRequired { get; set; }
        public string ParentItemID { get; set; }

        public bool HidePrices { get; set; }
        public bool CanDelete { get; set; }
        public bool IsPriceBreak { get; set; }
        public bool UsePriceBreaks { get; set; }

        public int SortPos { get; set; }
        public string CurrencySymbol { get; set; }
        public int StatusID { get; set; }
    }



    public class QuotationItemClientMapper : OrderItemModelMapper<QuotationItem, QuotationItemClientViewModel>        
    {

        public override void MapToViewModel(QuotationItem model, QuotationItemClientViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationItemID = model.ID.ToString();
            viewModel.SectionID = model.SectionID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Description = model.Description;
            viewModel.ImageURL = (model.Images ?? new QuotationItemImage[] { }).OrderBy(x => x.SortPos).Select(x => x.File.FilePath).DefaultIfEmpty("/Product Images/NotFound.png").FirstOrDefault(); //viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.NumImages = (model.Images != null) ? model.Images.Count : 0;
            
            viewModel.IsKit = model.IsKit;
            viewModel.IsComment = model.IsComment;
            viewModel.IsRequired = model.IsRequired;
            viewModel.IsKitItem = (model.ParentItem != null);
            viewModel.ParentItemID = (model.ParentItem != null) ? model.ParentItemID.Value.ToString() : null;
            viewModel.ParentQuantity = (model.ParentItem != null) ? model.ParentItem.Quantity : 1;
            viewModel.Quantity = model.Quantity;
                        
            viewModel.Unit = model.Unit;
            viewModel.Price = model.Price;
            viewModel.Total = (model.ParentItem != null) ? model.ParentItem.Quantity * model.Total : model.Total;

            viewModel.HidePrices = (model.ParentItem != null) ? (model.ParentItem.UseKitPrice) : (model.IsKit && !model.UseKitPrice);
            viewModel.CanDelete = (model.ParentItem != null) ? false : true;
            viewModel.SortPos = model.SortPos;

            viewModel.UsePriceBreaks = model.Quotation.UsePriceBreaks;
            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.StatusID = model.Quotation.StatusID;
        }


        public override void MapToModel(QuotationItemClientViewModel viewModel, QuotationItem model)
        {
            model.Quantity = viewModel.Quantity;
        }

    }
    

    public class QuotationItemPriceBreakClientMapper : ModelMapper<QuotationItemPriceBreak, QuotationItemClientViewModel>
    {
        public override void MapToViewModel(QuotationItemPriceBreak model, QuotationItemClientViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationItem.QuotationID.ToString();
            viewModel.QuotationItemID = model.QuotationItem.ID.ToString();
            viewModel.SectionID = model.QuotationItem.SectionID.ToString();

            viewModel.Description = "";            
            viewModel.Quantity = model.Quantity;
            viewModel.Price = model.Price;
            viewModel.Total = model.Total;            
            viewModel.CanDelete = true;
            viewModel.IsPriceBreak = true;
            viewModel.IsRequired = true;

            viewModel.UsePriceBreaks = model.QuotationItem.Quotation.UsePriceBreaks;
            viewModel.CurrencySymbol = model.QuotationItem.Quotation.Currency.Symbol;
            viewModel.StatusID = model.QuotationItem.Quotation.StatusID;
        }

        public override void MapToModel(QuotationItemClientViewModel viewModel, QuotationItemPriceBreak model)
        {
            model.Quantity = viewModel.Quantity;
        }
    }
    
}
