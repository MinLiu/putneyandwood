﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationQualification : IEntity
    {
        public QuotationQualification() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        public Guid? QuotationItemID { get; set; }
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }

        public virtual Quotation Quotation { get; set; }   
        public virtual QuotationItem QuotationItem { get; set; }

        public string ToLongLabel() { return ""; }
        public string ToShortLabel() { return ""; }
    }



    public class QuotationQualificationViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }
    }



    public class QuotationQualificationMapper : ModelMapper<QuotationQualification, QuotationQualificationViewModel>    
    {
        public override void MapToViewModel(QuotationQualification model, QuotationQualificationViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.SortPos = model.SortPos;
            viewModel.Description = model.Description;
            viewModel.IsSubHeading = model.IsSubHeading;
            viewModel.IsSubItem = model.IsSubItem;
        }

        public override void MapToModel(QuotationQualificationViewModel viewModel, QuotationQualification model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.SortPos = viewModel.SortPos;
            model.Description = viewModel.Description;
            model.IsSubHeading = viewModel.IsSubHeading;
            model.IsSubItem = viewModel.IsSubItem;
        }
    }
}
