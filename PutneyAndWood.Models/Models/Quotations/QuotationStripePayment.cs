﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using Stripe;
using System.Configuration;

namespace SnapSuite.Models
{
    public class QuotationStripePayment : IOrderStripPayment, ILoggable
    {
        [Key]
        [ForeignKey("Owner"), Column("OwnerID")]
        public Guid OwnerID { get; set; }

        //Pre Charge details
        public string Description { get; set; }
        public bool FixedAmount { get; set; }
        public decimal? PercentageToPay { get; set; }
        public decimal? AmountToPay { get; set; }

        public bool IsPaid { get; set; }
        public DateTime? DateOfPayment { get; set; }
        public string StripeChargeID { get; set; }
        //public string StripeCustomerID { get; set; }
        //public string StripeDescription { get; set; }
        //public string StripeCurrency { get; set; }
        //public decimal? StripeAmount { get; set; }
        //public DateTime? StripeDateCreated { get; set; }


        public virtual Quotation Owner { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }



    public class QuotationStripePaymentViewModel
    {
        public Guid OwnerID { get; set; }

        //Pre Charge details
        public string Description { get; set; }
        public bool FixedAmount { get; set; }
        public decimal? PercentageToPay { get; set; }
        public decimal? AmountToPay { get; set; }

        public bool IsPaid { get; set; }
        public DateTime? DateOfPayment { get; set; }
        public string StripeChargeID { get; set; }
        public string StripeCustomerID { get; set; }
        public string StripeDescription { get; set; }
        public string StripeCurrency { get; set; }
        public string StripeCurrencySymbol { get; set; }
        public decimal? StripeAmount { get; set; }
        public decimal? StripeFee { get; set; }
        public decimal? StripeNet { get; set; }
        public DateTime? StripeDateCreated { get; set; }
        public bool Refunded { get; set; }
    }




    public class QuotationStripePaymentMapper : ModelMapper<QuotationStripePayment, QuotationStripePaymentViewModel>
    {

        public override void MapToViewModel(QuotationStripePayment model, QuotationStripePaymentViewModel viewModel)
        {
            if (model.FixedAmount) model.PercentageToPay = null;
            else model.AmountToPay = null;

            viewModel.OwnerID = model.OwnerID;
            viewModel.Description = model.Description;
            viewModel.FixedAmount = model.FixedAmount;
            viewModel.PercentageToPay = model.PercentageToPay;
            viewModel.AmountToPay = model.AmountToPay;

            viewModel.IsPaid = model.IsPaid;
            viewModel.StripeChargeID = model.StripeChargeID;
            viewModel.DateOfPayment = model.DateOfPayment;

            try
            {
                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);
                var settings = new GenericService<StripeSettings>(new SnapDbContext()).Read().Where(i => i.CompanyID == model.Owner.CompanyID).First();

                var chargeService = new StripeChargeService();
                chargeService.ExpandBalanceTransaction = true;
                StripeCharge charge = chargeService.Get(model.StripeChargeID, new StripeRequestOptions { StripeConnectAccountId = settings.UserID });

                viewModel.StripeCustomerID = charge.CustomerId;
                viewModel.StripeDescription = charge.Description;
                viewModel.StripeCurrency = charge.Currency;
                viewModel.StripeCurrencySymbol = CurrenyExtensions.CodeToSymbol(charge.Currency);
                viewModel.StripeAmount = (decimal)charge.Amount * 0.01m;
                viewModel.StripeFee = (charge.BalanceTransaction != null) ? (decimal)charge.BalanceTransaction.Fee * 0.01m : 0m;
                viewModel.StripeNet = viewModel.StripeAmount - viewModel.StripeFee;
                viewModel.StripeDateCreated = charge.Created;
                viewModel.Refunded = charge.Refunded;
            }
            catch { }
        }


        public override void MapToModel(QuotationStripePaymentViewModel viewModel, QuotationStripePayment model)
        {
            if (!model.IsPaid)
            {
                if (viewModel.FixedAmount) viewModel.PercentageToPay = null;
                else viewModel.AmountToPay = null;

                //model.OwnerID = viewModel.OwnerID;
                model.Description = viewModel.Description;
                model.FixedAmount = viewModel.FixedAmount;
                model.PercentageToPay = viewModel.PercentageToPay;
                model.AmountToPay = viewModel.AmountToPay;
            }

            model.IsPaid = viewModel.IsPaid;
            model.DateOfPayment = viewModel.DateOfPayment;
            model.StripeChargeID = viewModel.StripeChargeID;
            //model.StripeCustomerID = viewModel.StripeCustomerID;
            //model.StripeDescription = viewModel.StripeDescription;
            //model.StripeCurrency = viewModel.StripeCurrency;
            //model.StripeAmount = viewModel.StripeAmount;
            //model.StripeDateCreated = viewModel.StripeDateCreated;
        }
    }
}
