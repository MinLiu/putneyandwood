﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationItem : IOrderLineItem<QuotationSection, QuotationItem, QuotationItemImage>
    {
        public QuotationItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        //public Guid? ProductID { get; set; }
        public Guid SectionID { get; set; }
        public string ProductCode { get; set; }        
        public string Description { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string Unit { get; set; }
        public string AccountCode { get; set; }
        public string PurchaseAccountCode { get; set; }
        public string JsonCustomFieldValues { get; set; }
        [NotMapped]
        public List<CustomField> CustomFieldValues { get { return JsonCustomFieldValues.CustomFieldsFromString(); } set { JsonCustomFieldValues = value.StringFromCustomFields(); } }

        public decimal Quantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        [NotMapped]
        public decimal VATPrice { get { return decimal.Round(Price * VAT * Quantity / 100m, 2);  } set { }  }
        public decimal Markup { get; set; }
        public decimal Discount { get; set; }
        public decimal ListPrice { get; set; }
        public decimal Price { get; set; }
        public decimal ListCost { get; set; }
        public decimal Cost { get; set; }
        public decimal Margin { get; set; }
        public decimal MarginPrice { get; set; }
        [NotMapped]
        public decimal Gross { get { return Total + VATPrice; } set { } }
        public decimal Total { get; set; }
        

        public string ImageURL { get; set; }
        public bool IsKit { get; set; }
        public bool UseKitPrice { get; set; }
        public Guid? ParentItemID { get; set; }
        public int  SortPos { get; set; }
        public bool IsComment { get; set; }
        public bool HideOnPdf { get; set; }
        public bool IsRequired { get; set; }

        public virtual Quotation Quotation { get; set; }                
        public virtual QuotationItem ParentItem { get; set; }
        public virtual QuotationSection Section { get; set; }

        //Foreign References
        public virtual ICollection<QuotationItemImage> Images { get; set; }
        public virtual ICollection<QuotationItem> KitItems { get; set; }
        public virtual ICollection<QuotationItemPriceBreak> PriceBreaks { get; set; }
        //public virtual Product Product { get; set; }

        public string ToShortLabel() { return (IsComment) ? Description.Split('\n')[0].Truncate(50) : string.Format("{0} - {1}", ProductCode, (Description ?? "").Split('\n')[0].Truncate(50)); }
        public string ToLongLabel() { return (IsComment) ? Description.Split('\n')[0].Truncate(50) : string.Format("{0} - {1}", ProductCode, (Description ?? "").Split('\n')[0]); }

    }



    public class QuotationItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationItemID { get; set; }
        public string PriceBreakID { get; set; }
        public string SectionID { get; set; }
        public string ProductCode { get; set; }
        public LineItemInfoViewModel Information { get; set; }
        public string Unit { get; set; }
        public string AccountCode { get; set; }
        public string PurchaseAccountCode { get; set; }

        public decimal Quantity { get; set; }
        //public decimal ParentQuantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        public decimal Markup { get; set; }
        public decimal Discount { get; set; }
        public decimal ListPrice { get; set; }
        public decimal Price { get; set; }
        public decimal ListCost { get; set; }
        public decimal Cost { get; set; }
        public decimal Margin { get; set; }
        public decimal MarginPrice { get; set; }
        public decimal Total { get; set; }
        
        public string ImageURL { get; set; }
        public int NumImages { get; set; }
        public bool IsKit { get; set; }
        public bool IsKitItem { get; set; }
        public bool IsComment { get; set; }
        public string ParentItemID { get; set; }

        public bool HidePrices { get; set; }
        public bool CanDelete { get; set; }
        public bool IsPriceBreak { get; set; }
        public bool UsePriceBreaks { get; set; }
        public bool HideOnPdf { get; set; }
        public bool IsRequired { get; set; }
        public int NumAltPdt { get; set; }
        public int NumAsctPdt { get; set; }

        public int SortPos { get; set; }
        public string CurrencySymbol { get; set; }
        public int StatusID { get; set; }
    }



    public class QuotationItemMapper : OrderItemModelMapper<QuotationItem, QuotationItemViewModel>    
    {
       
        public override void MapToViewModel(QuotationItem model, QuotationItemViewModel viewModel)
        {

            if(CustomFields == null)
                CustomFields = CustomFieldService.Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField {  Name = i.Name, Label = i.Label, SortPos = i.SortPos, Show = i.ShowQuotations }).ToList();


            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationItemID = model.ID.ToString();
            viewModel.SectionID = model.SectionID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.AccountCode = model.AccountCode;
            viewModel.PurchaseAccountCode = model.PurchaseAccountCode;

            viewModel.Information = new LineItemInfoViewModel
            {
                Description = model.Description ?? "",
                //DescriptionShort = (model.Description ?? "").Split('\n')[0] + (((model.Description ?? "").Split('\n').Count() <= 0) ? "..." : ""),
                Category = model.Category ?? "",
                //VendorCode = model.VendorCode ?? "",
                //Manufacturer = model.Manufacturer ?? "",
                CustomFieldValues = model.IsComment ? CustomFields : model.CustomFieldValues.AddRemoveRelevent(CustomFields),
            };


            viewModel.ImageURL = (model.Images ?? new QuotationItemImage[] { }).OrderBy(x => x.SortPos).Select(x => x.File.FilePath).DefaultIfEmpty("/Product Images/NotFound.png").FirstOrDefault(); //viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.NumImages = model.Images.Count;

            viewModel.IsKit = model.IsKit;
            viewModel.IsComment = model.IsComment;
            viewModel.IsKitItem = (model.ParentItem != null);
            viewModel.ParentItemID = (model.ParentItem != null) ? model.ParentItemID.Value.ToString() : null;

            //viewModel.ParentQuantity = (model.ParentItem != null) ? model.ParentItem.Quantity : 1;
            //viewModel.Quantity = (model.ParentItem != null) ? model.ParentItem.Quantity * model.Quantity : model.Quantity;
            viewModel.Quantity = model.Quantity;
                        
            viewModel.Unit = model.Unit;
            viewModel.SetupCost = model.SetupCost;
            viewModel.VAT = model.VAT;
            viewModel.Discount = model.Discount;
            viewModel.Markup = model.Markup;            
            viewModel.ListPrice = model.ListPrice;
            viewModel.Price = model.Price;
            viewModel.ListCost = model.ListCost;
            viewModel.Cost = model.Cost;
            viewModel.Margin = model.Margin;
            viewModel.MarginPrice = model.MarginPrice;
            viewModel.Total = (model.ParentItem != null) ? model.ParentItem.Quantity * model.Total : model.Total;

            viewModel.HidePrices = (model.ParentItem != null) ? (model.ParentItem.UseKitPrice) : (model.IsKit && !model.UseKitPrice);
            viewModel.CanDelete = (model.ParentItem != null) ? false : true;
            viewModel.SortPos = model.SortPos;

            viewModel.UsePriceBreaks = model.Quotation.UsePriceBreaks;
            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.StatusID = model.Quotation.StatusID;

            viewModel.HideOnPdf = model.HideOnPdf;
            viewModel.IsRequired = model.IsRequired;

            var pdtService = new ProductService<ProductGridViewModel>(CustomFieldService.CompanyID, CustomFieldService.CurrentUserID, new ProductGridMapper(), CustomFieldService.DbContext);
            viewModel.NumAltPdt = pdtService.ReadAlternatives(model.ProductCode).Count();
            viewModel.NumAsctPdt = pdtService.ReadAssociatives(model.ProductCode).Count();
        }


        public override void MapToModel(QuotationItemViewModel viewModel, QuotationItem model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.SectionID = Guid.Parse(viewModel.SectionID);
            model.ProductCode = viewModel.ProductCode;
            model.AccountCode = viewModel.AccountCode;
            model.PurchaseAccountCode = viewModel.PurchaseAccountCode;
            model.IsKit = viewModel.IsKit;
            model.IsComment = viewModel.IsComment;
            model.ParentItemID = (string.IsNullOrEmpty(viewModel.ParentItemID)) ? null : Guid.Parse(viewModel.ParentItemID) as Guid?;

            if (viewModel.Information != null)
            {
                model.Description = viewModel.Information.Description;
                model.Category = viewModel.Information.Category;
                //model.VendorCode = viewModel.Information.VendorCode;
                //model.Manufacturer = viewModel.Information.Manufacturer;
                model.CustomFieldValues = (model.IsComment) ? null : viewModel.Information.CustomFieldValues;
            }
            //model.ImageURL = viewModel.ImageURL ?? "/Product Images/NotFound.png";
            
            model.Unit = viewModel.Unit;
            model.SetupCost = viewModel.SetupCost;
            model.VAT = viewModel.VAT;
            model.Markup = viewModel.Markup;
            model.Discount = viewModel.Discount;
            model.Quantity = viewModel.Quantity;
            model.ListPrice = viewModel.ListPrice;
            model.Price = viewModel.Price;
            model.ListCost = viewModel.ListCost;
            model.Cost = viewModel.Cost;
            model.Margin = viewModel.Margin;
            model.MarginPrice = viewModel.MarginPrice;
            model.Total = viewModel.Total;

            model.SortPos = viewModel.SortPos;
            model.IsRequired = viewModel.IsRequired;

        }

    }
}
