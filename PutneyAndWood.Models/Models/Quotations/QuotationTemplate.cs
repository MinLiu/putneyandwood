﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationTemplate : IOrderPreviewTemplate
    {
        public QuotationTemplate() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid? CompanyID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }

        [Required]
        public TemplateType Type { get; set; }

        [Required]
        public string Filename { get; set; }
        
        [Required]
        public TemplateFormat Format { get; set; }
        
        public Company Company { get; set; }

        public virtual ICollection<QuotationPreviewItem> PreviewItems { get; set; }

        //[ForeignKey("FrontTemplateID")]
        //public ICollection<QuotationPreview> FrontPreviews { get; set; }

        //[ForeignKey("BodyTemplateID")]
        //public ICollection<QuotationPreview> BodyPreviews { get; set; }

        //[ForeignKey("BackTemplateID")]
        //public ICollection<QuotationPreview> BackPreviews { get; set; }

        public string ToShortLabel() { return Filename; }
        public string ToLongLabel() { return Filename; }
    }

    public class QuotationTemplateViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        [Required]
        public string CompanyID { get; set; }

        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public string Filename { get; set; }
               
        [Required]
        public string Format { get; set; }
    }

    public class QuotationTemplateMapper : ModelMapper<QuotationTemplate, QuotationTemplateViewModel>
    {
        public override void MapToViewModel(QuotationTemplate model, QuotationTemplateViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = (model.CompanyID != null) ? model.CompanyID.ToString() : null;
            viewModel.Type = model.Type.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.Filename = model.Filename;
            viewModel.Format = model.Format.ToString();
        }

        public override void MapToModel(QuotationTemplateViewModel viewModel, QuotationTemplate model)
        {
            //model.File.MapViewModelToModel(viewModel.File);
            model.Filename = viewModel.Filename;
        }
    }
}
