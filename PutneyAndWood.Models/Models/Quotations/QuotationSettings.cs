﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web;
using System.Web.Mvc;

namespace SnapSuite.Models
{
    public class QuotationSettings : IOrderSettings<DefaultQuotationAttachment, DefaultQuotationNotification>, ILoggable
    {
        
        [Key]
        [ForeignKey("Company"), Column("CompanyID")]
        public Guid CompanyID { get; set; } //Be sure to set cascade delete

        public string DefaultReference { get; set; }
        public int StartingNumber { get; set; }
        public string DefaultNote { get; set; }
        
        public bool ShowPhoto { get; set; }
        public bool ShowProductCode { get; set; }
        public bool ShowUnit { get; set; }
        public bool ShowMarkup { get; set; }
        public bool ShowVAT { get; set; }
        public bool ShowDiscount { get; set; }
        public bool ShowListPrice { get; set; }
        public bool ShowListCost { get; set; }
        public bool ShowCost { get; set; }
        public bool ShowMargin { get; set; }
        public bool ShowMarginPercentage { get; set; }
        public bool ShowSetupCost { get; set; }
        public bool ShowCategory { get; set; }
        public bool ShowVendorCode { get; set; }
        public bool ShowManufacturer { get; set; }
        public bool ShowAccountCode { get; set; }
        public bool ShowPurchaseAccountCode { get; set; }

        public bool DefaultAllowClientEditLineItems { get; set; }
        public bool DefaultAllowClientEditSections { get; set; }
        public bool DefaultNeedsCheckedClientSubmission { get; set; }

        public decimal DefaultHourlyRate { get; set; }
        public string DefaultFromEmail { get; set; }
        public string EmailTitleTemplate { get; set; }
        public string EmailBodyTemplate { get; set; }

        public bool StripePaymentsOnNew { get; set; }
        public string DefaultStripeDescription { get; set; }
        public bool DefaultStripeFixedAmount { get; set; }
        public decimal? DefaultStripePercentageToPay { get; set; }
        public decimal? DefaultStripeAmountToPay { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<DefaultQuotationAttachment> DefaultAttachments { get; set; }
        public virtual ICollection<DefaultQuotationNotification> DefaultNotifications { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }


        public QuotationSettings()
        {
            DefaultReference = "Q";
            StartingNumber = 1000;
            ShowPhoto = true;
            ShowProductCode = true;
            ShowUnit = false;
            ShowMarkup = false;
            ShowDiscount = true;
            ShowVAT = false;
            ShowListPrice = false;
            ShowListCost = false;
            ShowCost = false;
            ShowMargin = true;
            ShowMarginPercentage = false;
            ShowSetupCost = false;
            ShowCategory = false;
            //ShowVendorCode = true;
            //ShowManufacturer = true;
            ShowAccountCode = false;
            ShowPurchaseAccountCode = false;

            EmailTitleTemplate = string.Format("Quotation: {{QuotationNumber}} ~ {{ClientName}}");
            EmailBodyTemplate = string.Format("<p>{{BusinessLogo}}</p><p>Dear {{ClientContactFirstName}},</p><p>Please review the following quotation. </p><p>Thank you for your time.</p><p></p><p><a href=\"{{ReviewLink}}\">Click this link to review quotation, view additional attachments & submit feedback.</a></p>");

            DefaultAttachments = new List<DefaultQuotationAttachment>();
            DefaultNotifications = new List<DefaultQuotationNotification>();
        }

    }




    public class QuotationSettingsViewModel 
    {
        //public Guid ID { get; set; }
        public string CompanyID { get; set; }
        public string QuotationID { get; set; }

        public string DefaultReference { get; set; }
        public int StartingNumber { get; set; }
        public string DefaultNote { get; set; }
        
        public bool ShowPhoto { get; set; }
        public bool ShowProductCode { get; set; }
        public bool ShowUnit { get; set; }
        public bool ShowMarkup { get; set; }
        public bool ShowVAT { get; set; }
        public bool ShowListPrice { get; set; }
        public bool ShowListCost { get; set; }
        public bool ShowCost { get; set; }
        public bool ShowMargin { get; set; }        
        public bool ShowDiscount { get; set; }
        public bool ShowMarginPercentage { get; set; }
        public bool ShowSetupCost { get; set; }
        public bool ShowCategory { get; set; }
        //public bool ShowVendorCode { get; set; }
        //public bool ShowManufacturer { get; set; }
        public bool ShowAccountCode { get; set; }
        public bool ShowPurchaseAccountCode { get; set; }

        public bool DefaultAllowClientEditLineItems { get; set; }
        public bool DefaultAllowClientEditSections { get; set; }
        public bool DefaultNeedsCheckedClientSubmission { get; set; }

        public decimal DefaultHourlyRate { get; set; }

        [EmailAddress]
        [Display(Name = "Default From Email Address")]
        public string DefaultFromEmail { get; set; }

        [AllowHtml]
        public string EmailTitleTemplate { get; set; }
        [AllowHtml]
        public string EmailBodyTemplate { get; set; }

        public bool StripePaymentsOnNew { get; set; }
        public string DefaultStripeDescription { get; set; }
        public bool DefaultStripeFixedAmount { get; set; }
        public decimal? DefaultStripePercentageToPay { get; set; }
        public decimal? DefaultStripeAmountToPay { get; set; }

        public List<CustomProductField> CustomProducFields { get; set; }

    }



    public class QuotationSettingsMapper : ModelMapper<QuotationSettings, QuotationSettingsViewModel>
    {

        public override void MapToViewModel(QuotationSettings model, QuotationSettingsViewModel viewModel)
        {

            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.DefaultReference = model.DefaultReference;
            viewModel.StartingNumber = model.StartingNumber;
            viewModel.DefaultNote = model.DefaultNote;

            viewModel.ShowPhoto = model.ShowPhoto;
            viewModel.ShowProductCode = model.ShowProductCode;
            viewModel.ShowUnit = model.ShowUnit;
            viewModel.ShowMarkup = model.ShowMarkup;
            viewModel.ShowVAT = model.ShowVAT;
            viewModel.ShowListPrice = model.ShowListPrice;
            viewModel.ShowListCost = model.ShowListCost;
            viewModel.ShowCost = model.ShowCost;
            viewModel.ShowMargin = model.ShowMargin;
            viewModel.ShowDiscount = model.ShowDiscount;
            viewModel.ShowMarginPercentage = model.ShowMarginPercentage;
            viewModel.ShowSetupCost = model.ShowSetupCost;
            viewModel.ShowCategory = model.ShowCategory;
            ////viewModel.ShowVendorCode = model.ShowVendorCode;
            ////viewModel.ShowManufacturer = model.ShowManufacturer;
            viewModel.ShowAccountCode = model.ShowAccountCode;
            viewModel.ShowPurchaseAccountCode = model.ShowPurchaseAccountCode;

            viewModel.DefaultAllowClientEditLineItems = model.DefaultAllowClientEditLineItems;
            viewModel.DefaultAllowClientEditSections = model.DefaultAllowClientEditSections;
            viewModel.DefaultNeedsCheckedClientSubmission = model.DefaultNeedsCheckedClientSubmission;

            viewModel.DefaultHourlyRate = model.DefaultHourlyRate;
            viewModel.DefaultFromEmail = model.DefaultFromEmail;
            viewModel.EmailTitleTemplate = model.EmailTitleTemplate;
            viewModel.EmailBodyTemplate = model.EmailBodyTemplate;

            viewModel.StripePaymentsOnNew = model.StripePaymentsOnNew;
            viewModel.DefaultStripeDescription = model.DefaultStripeDescription;
            viewModel.DefaultStripeFixedAmount = model.DefaultStripeFixedAmount;
            viewModel.DefaultStripePercentageToPay = model.DefaultStripePercentageToPay;
            viewModel.DefaultStripeAmountToPay = model.DefaultStripeAmountToPay;
            
        }


        public override void MapToModel(QuotationSettingsViewModel viewModel, QuotationSettings model)
        {
           
            model.DefaultReference = viewModel.DefaultReference;
            model.StartingNumber = viewModel.StartingNumber;
            model.DefaultNote = viewModel.DefaultNote;

            model.ShowPhoto = viewModel.ShowPhoto;
            model.ShowProductCode = viewModel.ShowProductCode;
            model.ShowUnit = viewModel.ShowUnit;
            model.ShowMarkup = viewModel.ShowMarkup;
            model.ShowVAT = viewModel.ShowVAT;
            model.ShowListPrice = viewModel.ShowListPrice;
            model.ShowListCost = viewModel.ShowListCost;
            model.ShowCost = viewModel.ShowCost;
            model.ShowMargin = viewModel.ShowMargin;
            model.ShowDiscount = viewModel.ShowDiscount;
            model.ShowMarginPercentage = viewModel.ShowMarginPercentage;
            model.ShowSetupCost = viewModel.ShowSetupCost;
            model.ShowCategory = viewModel.ShowCategory;
            ////model.ShowVendorCode = viewModel.ShowVendorCode;
            ////model.ShowManufacturer = viewModel.ShowManufacturer;
            model.ShowAccountCode = viewModel.ShowAccountCode;
            model.ShowPurchaseAccountCode = viewModel.ShowPurchaseAccountCode;

            model.DefaultAllowClientEditLineItems = viewModel.DefaultAllowClientEditLineItems;
            model.DefaultAllowClientEditSections = viewModel.DefaultAllowClientEditSections;
            model.DefaultNeedsCheckedClientSubmission = viewModel.DefaultNeedsCheckedClientSubmission;

            model.DefaultHourlyRate = viewModel.DefaultHourlyRate;
            model.DefaultFromEmail = viewModel.DefaultFromEmail;
            model.EmailTitleTemplate = viewModel.EmailTitleTemplate;
            model.EmailBodyTemplate = HttpUtility.HtmlDecode(viewModel.EmailBodyTemplate);


            model.StripePaymentsOnNew = viewModel.StripePaymentsOnNew;
            model.DefaultStripeDescription = viewModel.DefaultStripeDescription;
            model.DefaultStripeFixedAmount = viewModel.DefaultStripeFixedAmount;
            model.DefaultStripePercentageToPay = viewModel.DefaultStripePercentageToPay;
            model.DefaultStripeAmountToPay = viewModel.DefaultStripeAmountToPay;

        }

    }

}
