﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationLabourItem : IOrderLabourItem
    {
        public QuotationLabourItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        public string Description { get; set; }


        public decimal Hours { get; set; }
        public decimal HourlyRate { get; set; }

        public decimal Total { get; set; }

        public int SortPos { get; set; }

        public virtual Quotation Quotation { get; set; }

        public string ToShortLabel() { return Description; }
        public string ToLongLabel() { return Description; }

    }

    public class QuotationLabourItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }

        public string Description { get; set; }

        public decimal Hours { get; set; }
        public decimal HourlyRate { get; set; }
        public decimal Total { get; set; }


        public int SortPos { get; set; }
        public string CurrencySymbol { get; set; }
        public int StatusID { get; set; }
    }



    public class QuotationLabourItemMapper : ModelMapper<QuotationLabourItem, QuotationLabourItemViewModel>
    {

        public override void MapToViewModel(QuotationLabourItem model, QuotationLabourItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Description = model.Description ?? "";

            //viewModel.ParentQuantity = (model.ParentLabourItem != null) ? model.ParentLabourItem.Quantity : 1;
            //viewModel.Quantity = (model.ParentLabourItem != null) ? model.ParentLabourItem.Quantity * model.Quantity : model.Quantity;
            viewModel.Hours = model.Hours;
            viewModel.HourlyRate = model.HourlyRate;
            viewModel.Total = model.Total;

            viewModel.SortPos = model.SortPos;

            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.StatusID = model.Quotation.StatusID;

        }


        public override void MapToModel(QuotationLabourItemViewModel viewModel, QuotationLabourItem model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.Description = viewModel.Description;

            model.Hours = viewModel.Hours;
            model.HourlyRate = viewModel.HourlyRate;
            model.Total = viewModel.Total;

            model.SortPos = viewModel.SortPos;

        }

    }

}
