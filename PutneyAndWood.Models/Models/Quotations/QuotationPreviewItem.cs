﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationPreviewItem : IOrderPreviewItem<QuotationTemplate, QuotationAttachment>
    {
        public QuotationPreviewItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ParentID { get; set; }

        public PreviewItemType Type { get; set; }

        public Guid? TemplateID { get; set; }
        public virtual QuotationTemplate Template { get; set; }

        public Guid? AttachmentID { get; set; }
        public virtual QuotationAttachment Attachment { get; set; }
        
        public int SortPos { get; set; }

        public virtual Quotation Parent { get; set; }

        public string ToShortLabel() { if (Template == null && Attachment == null) { return ""; } else { return (Type == PreviewItemType.Attachment) ? Attachment.Filename : string.Format("({0}) {1}", Template.Type.ToString(), Template.Filename); } }
        public string ToLongLabel() { if (Template == null && Attachment == null) { return ""; } else { return (Type == PreviewItemType.Attachment) ? Attachment.Filename : string.Format("({0}) {1}", Template.Type.ToString(), Template.Filename); } }
    }




    public class QuotationPreviewItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public Guid ParentID { get; set; }
        public string Type { get; set; }
        public SelectPreviewItemViewModel TemplateAttachment { get; set; }
    }



    public class QuotationPreviewItemMapper : ModelMapper<QuotationPreviewItem, QuotationPreviewItemViewModel>
    {

        public override void MapToViewModel(QuotationPreviewItem model, QuotationPreviewItemViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.ParentID = model.ParentID;
            viewModel.Type = model.Type.ToString();
            viewModel.TemplateAttachment = new SelectPreviewItemViewModel
            {
                OrderID = model.ParentID,
                Name = model.ToShortLabel() ?? "",
                TemplateAttachmentID = (model.Type != PreviewItemType.Attachment) ? model.TemplateID : model.AttachmentID,
                Type = model.Type
            };
        }


        public override void MapToModel(QuotationPreviewItemViewModel viewModel, QuotationPreviewItem model)
        {
            model.ParentID = viewModel.ParentID;
            model.Type = (PreviewItemType)Enum.Parse(typeof(PreviewItemType), viewModel.Type);
            //model.TemplateID = viewModel.TemplateID;
            //model.AttachmentID = viewModel.AttachmentID;
            //model.SortPos = viewModel.SortPos;

            if (model.Type != PreviewItemType.Attachment)
                model.TemplateID = (viewModel.TemplateAttachment != null) ? viewModel.TemplateAttachment.TemplateAttachmentID : null;
            else
                model.AttachmentID = (viewModel.TemplateAttachment != null) ? viewModel.TemplateAttachment.TemplateAttachmentID : null;
        }

    }
}
