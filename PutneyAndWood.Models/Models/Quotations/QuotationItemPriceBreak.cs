﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationItemPriceBreak : IEntity, IPrice
    {
        public QuotationItemPriceBreak() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationItemID { get; set; }

        public decimal Quantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        [NotMapped]
        public decimal VATPrice { get { return decimal.Round(Price * VAT / 100m, 2); } private set { } }
        public decimal Markup { get; set; }
        public decimal Discount { get; set; }
        public decimal ListPrice { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public decimal Margin { get; set; }
        public decimal MarginPrice { get; set; }
        [NotMapped]
        public decimal Gross { get { return Total + VATPrice; } private set { } }
        public decimal Total { get; set; }

        public virtual QuotationItem QuotationItem { get; set; }


        //NOT MAPPED
        decimal IPrice.ListCost { get => ((IPrice)QuotationItem).ListCost; set => ((IPrice)QuotationItem).ListCost = value; }
        decimal IPrice.VATPrice { get => ((IPrice)QuotationItem).VATPrice; set => ((IPrice)QuotationItem).VATPrice = value; }
        decimal IPrice.Gross { get => ((IPrice)QuotationItem).Gross; set => ((IPrice)QuotationItem).Gross = value; }

        public string ToShortLabel() { return string.Format("{0:#,##0.00}", Quantity); }
        public string ToLongLabel() { return string.Format("{0:#,##0.00} - {1}", Quantity, QuotationItem.ToLongLabel()); }
    }

    


    public class QuotationItemPriceBreakViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationItemID { get; set; }
        public string QuotationSectionID { get; set; }
        public bool IsMainPrice { get; set; }

        public decimal Quantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        public decimal Markup { get; set; }
        public decimal Discount { get; set; } 
        public decimal ListPrice { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public decimal Margin { get; set; }
        public decimal MarginPrice { get; set; }
        public decimal Total { get; set; }

    }


    public class QuotationItemPriceBreakMapper : ModelMapper<QuotationItemPriceBreak, QuotationItemPriceBreakViewModel>        
    {

        public override void MapToViewModel(QuotationItemPriceBreak model, QuotationItemPriceBreakViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationItem.QuotationID.ToString(); ;
            viewModel.QuotationItemID = model.QuotationItemID.ToString();
            viewModel.QuotationSectionID = model.QuotationItem.SectionID.ToString();
            viewModel.Quantity = model.Quantity;

            viewModel.SetupCost = model.SetupCost;
            viewModel.VAT = model.VAT;
            viewModel.Discount = model.Discount;
            viewModel.Markup = model.Markup;
            viewModel.ListPrice = model.ListPrice;
            viewModel.Price = model.Price;
            viewModel.Cost = model.Cost;
            viewModel.Margin = model.Margin;
            viewModel.MarginPrice = model.MarginPrice;
            viewModel.Total = model.Total;
        }


        public override void MapToModel(QuotationItemPriceBreakViewModel viewModel, QuotationItemPriceBreak model)
        {
            model.QuotationItemID = Guid.Parse(viewModel.QuotationItemID);
            model.SetupCost = viewModel.SetupCost;
            model.VAT = viewModel.VAT;
            model.Markup = viewModel.Markup;
            model.Discount = viewModel.Discount;
            model.Quantity = viewModel.Quantity;
            model.ListPrice = viewModel.ListPrice;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.Margin = viewModel.Margin;
            model.MarginPrice = viewModel.MarginPrice;
            model.Total = viewModel.Total;
        }


        public QuotationItemViewModel MapPriceBreakToItemViewModel(QuotationItemPriceBreak model)
        {
            var viewModel = new QuotationItemViewModel();
            MapToViewModel(model, viewModel);
            return viewModel;
        }


        public void MapToViewModel(QuotationItemPriceBreak model, QuotationItemViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationItem.QuotationID.ToString();
            viewModel.QuotationItemID = model.QuotationItemID.ToString();
            viewModel.SectionID = model.QuotationItem.SectionID.ToString();
            viewModel.PriceBreakID = model.ID.ToString();
            viewModel.ProductCode = "";
            viewModel.Information = new LineItemInfoViewModel
            {
                Description = "",
                //DescriptionShort = "",
                Category = "",
                //VendorCode = "",
                //Manufacturer = "",
                CustomFieldValues = new List<CustomField>(),
            };
            viewModel.ImageURL = "";

            viewModel.IsKit = model.QuotationItem.IsKit;
            viewModel.IsComment = model.QuotationItem.IsComment;
            viewModel.IsKitItem = (model.QuotationItem.ParentItem != null);
            viewModel.ParentItemID = (model.QuotationItem.ParentItem != null) ? model.QuotationItem.ParentItemID.Value.ToString() : null;

            //viewModel.ParentQuantity = 1;
            //viewModel.Quantity = (model.ParentItem != null) ? model.ParentItem.Quantity * model.Quantity : model.Quantity;
            viewModel.Quantity = model.Quantity;

            viewModel.Unit = "";
            viewModel.SetupCost = model.SetupCost;
            viewModel.VAT = model.VAT;
            viewModel.Discount = model.Discount;
            viewModel.Markup = model.Markup;
            viewModel.ListPrice = model.ListPrice;
            viewModel.Price = model.Price;
            viewModel.Cost = model.Cost;
            viewModel.Margin = model.Margin;
            viewModel.MarginPrice = model.MarginPrice;
            viewModel.Total = model.Total;

            viewModel.HidePrices = false;
            viewModel.CanDelete = true;
            viewModel.SortPos = model.QuotationItem.SortPos;
            viewModel.IsPriceBreak = true;
            viewModel.UsePriceBreaks = true;
        }



        public void MapToModel(QuotationItemViewModel viewModel, QuotationItemPriceBreak model)
        {
            model.QuotationItemID = Guid.Parse(viewModel.QuotationItemID);
            model.SetupCost = viewModel.SetupCost;
            model.VAT = viewModel.VAT;
            model.Markup = viewModel.Markup;
            model.Discount = viewModel.Discount;
            model.Quantity = viewModel.Quantity;
            model.ListPrice = viewModel.ListPrice;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.Margin = viewModel.Margin;
            model.MarginPrice = viewModel.MarginPrice;
            model.Total = viewModel.Total;
        }



        public QuotationItemPriceBreakViewModel MapItemToViewModel(QuotationItem model)
        {

            var viewModel = new QuotationItemPriceBreakViewModel();
            viewModel.ID = model.ID.ToString();
            viewModel.IsMainPrice = true;
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationItemID = model.ID.ToString();
            viewModel.QuotationSectionID = model.SectionID.ToString();
            viewModel.Quantity = model.Quantity;

            viewModel.SetupCost = model.SetupCost;
            viewModel.VAT = model.VAT;
            viewModel.Discount = model.Discount;
            viewModel.Markup = model.Markup;
            viewModel.ListPrice = model.ListPrice;
            viewModel.Price = model.Price;
            viewModel.Cost = model.Cost;
            viewModel.Margin = model.Margin;
            viewModel.MarginPrice = model.MarginPrice;
            viewModel.Total = model.Total;

            return viewModel;

        }
                        

    }


    public static class QuotationItemPriceBreakExtensions
    {
        public static QuotationItem ToQuotationItem(this QuotationItemPriceBreak model)
        {
            QuotationItem result = new QuotationItem
            {
                ProductCode = model.QuotationItem.ProductCode,
                Description = "",
                Unit = "",
                Quantity = model.Quantity,
                SetupCost = model.SetupCost,
                VAT = model.VAT,
                Markup = model.Markup,
                Discount = model.Discount,
                ListPrice = model.ListPrice,
                Price = model.Price,
                Cost = model.Cost,
                Margin = model.Margin,
                MarginPrice = model.MarginPrice,
                Total = model.Total,
            };

            return result;
        }
    }   
}
