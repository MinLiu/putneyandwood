﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationSignedAgreement : IAgreement, ICompanyEntity, ILoggable
    {
        [Key]
        [ForeignKey("Quotation"), Column("QuotationID")]
        public Guid QuotationID { get; set; }
        public Guid CompanyID { get; set; } 
                
        [Required]
        public string MD5Hash { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime DateSigned { get; set; }
        public string DeviceInformation { get; set; }
        public string IP_Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string SignatureImageURL { get; set; }
        public Guid? SignatureImageID { get; set; }
        public virtual FileEntry SignatureImage { get; set; }
        public bool SignedUsingInput { get; set; }

        public string PdfAgreementFilename { get; set; }
        public string PdfAgreementURL { get; set; }
        public Guid? PdfAgreementID { get; set; }
        public virtual FileEntry PdfAgreement { get; set; }

        public string PdfApprovedFilename { get; set; }
        public string PdfApprovedURL { get; set; }
        public Guid? PdfApprovedID { get; set; }
        public virtual FileEntry PdfApproved { get; set; }

        public virtual Company Company { get; set; }
        public virtual Quotation Quotation { get; set; }


        public string ToLongLabel() { return Quotation.ToLongLabel(); }
        public string ToShortLabel() { return Quotation.ToShortLabel(); }



        public string FullName()
        {
            return string.Format("{0} {1} {2}", Title, FirstName, LastName);
        }

        public void CreateMD5Hash()
        {
            string input = string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}:{8}:{9}:{10}",
                                        Title,
                                        FirstName,
                                        LastName,
                                        Email,
                                        DateSigned.ToString("HH.mm-dd-MM-yyyy"),
                                        DeviceInformation,
                                        IP_Address,
                                        City,
                                        Country,
                                        Latitude,
                                        Longitude
            );

            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            this.MD5Hash = sb.ToString();
        }

        
    }

    
}
