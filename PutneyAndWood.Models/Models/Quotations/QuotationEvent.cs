﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationEvent : IEvent, ICompanyEntity
    {
        public QuotationEvent() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? ProjectID { get; set; }
        public Guid? QuotationID { get; set; }
        public string AssignedUserID { get; set; }
        public string CreatorID { get; set; }

        public Guid? ClientID { get; set; }
        public Guid? ClientContactID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Message { get; set; } 
        public bool Complete { get; set; }

        [NotMapped]
        public int Weight {
            get
            {
                if (Timestamp.Date == DateTime.Today && Complete == false)
                    return 2;
                else if (DateTime.Now > Timestamp && Complete == false)
                    return 1;
                else if (Timestamp.Date > DateTime.Today && Complete == false)
                    return 0;
                else
                    return -1;
            }
            private set { }
        }

        public virtual Company Company { get; set; }
        public virtual Client Client { get; set; }
        public virtual ClientContact ClientContact { get; set; }
        
        public virtual User AssignedUser { get; set; }
        public virtual User Creator { get; set; }
        public virtual Quotation Quotation { get; set; }
        public virtual Project Project { get; set; }

        public string ToLongLabel() { return string.Format("{0}{2} at {1:HH:mm dd/MM/yy}", Message, Timestamp, (Complete) ? " (Completed)" : ""); }
        public string ToShortLabel() { return string.Format("{0}{2} at {1:HH:mm dd/MM/yy}", Message.Truncate(200), Timestamp, (Complete) ? " (Completed)" : ""); }

    }

    public class ContactItemViewModel 
    {
        public string ID { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Position { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class QuotationEventViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationName { get; set; }

        public string EventAssignedUserID { get; set; }
        public string EventAssignedUserName { get; set; }

        public string EventCreatorID { get; set; }
        public string EventCreatorName { get; set; }

        public string ClientID { get; set; }
        public string ClientName { get; set; }

        public string ContactID { get; set; }
        public string ContactName { get; set; }

        public string ProjectID { get; set; }
        public string ProjectName { get; set; }


        [Required(ErrorMessage = "* The Timestamp is required.")]
        public DateTime Timestamp { get; set; }

        [Required(ErrorMessage = "* The Message is required.")]
        public string Message { get; set; }

        public bool Complete { get; set; }
        public bool Overdue { get; set; }
        public bool Due { get; set; }

    }

    public class QuotationEventMapper : ModelMapper<QuotationEvent, QuotationEventViewModel>
    {
        public override void MapToViewModel(QuotationEvent model, QuotationEventViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.HasValue ? model.ClientID.ToString() : null;
            viewModel.ClientName = model.Client != null ? model.Client.Name : "";
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Complete = model.Complete;
            viewModel.ContactID = model.ClientContactID.HasValue ? model.ClientContactID.ToString() : null;
            viewModel.ContactName = model.ClientContact != null ? string.Format("{0} {1} {2}", model.ClientContact.Title, model.ClientContact.FirstName, model.ClientContact.LastName) : "";
            viewModel.EventAssignedUserID = model.AssignedUserID;
            viewModel.EventAssignedUserName = model.AssignedUser != null ? model.AssignedUser.Email : "";
            viewModel.EventCreatorID = model.CreatorID;
            viewModel.EventCreatorName = model.Creator != null ? model.Creator.Email : "";
            viewModel.Message = model.Message;
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.Due = (model.Timestamp > DateTime.Today && model.Timestamp < DateTime.Today.AddDays(1.0));
            viewModel.Overdue = model.Timestamp < DateTime.Now;
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationName = model.Quotation != null ? model.Quotation.Description : "";
            viewModel.ProjectID = model.ProjectID.ToString();
            viewModel.ProjectName = model.Project != null ? model.Project.Name : "";
        }

        public override void MapToModel(QuotationEventViewModel viewModel, QuotationEvent model)
        {
            model.ID = Guid.Parse(viewModel.ID);
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.AssignedUserID = viewModel.EventAssignedUserID;
            model.ClientContactID = string.IsNullOrWhiteSpace(viewModel.ContactID) ? null : Guid.Parse(viewModel.ContactID) as Guid?;
            model.ClientID = string.IsNullOrWhiteSpace(viewModel.ClientID) ? null : Guid.Parse(viewModel.ClientID) as Guid?;
            model.Complete = viewModel.Complete;
            model.Message = viewModel.Message;
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.ProjectID = Guid.Parse(viewModel.ProjectID);
            model.Timestamp = viewModel.Timestamp;
        }
    }
}
