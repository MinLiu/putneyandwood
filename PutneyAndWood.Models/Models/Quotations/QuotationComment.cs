﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationComment : IComment
    {
        public QuotationComment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }        
       
        public virtual Quotation Quotation { get; set; }

        public string ToShortLabel() { return Message.Truncate(100); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Timestamp.ToString("HH:mm dd/MM/yy"), Message.Truncate(100)); }
    }


    public class QuotationCommentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessage = "* The Message field is required.")]
        public string Message { get; set; }

    }

    public class QuotationCommentMapper : ModelMapper<QuotationComment, QuotationCommentViewModel>
    {

        public override void MapToViewModel(QuotationComment model, QuotationCommentViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Email = model.Email;
            viewModel.Timestamp = model.Timestamp;
            viewModel.Message = model.Message; 
        }


        public override void MapToModel(QuotationCommentViewModel viewModel, QuotationComment model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.Email = viewModel.Email;
            model.Timestamp = viewModel.Timestamp;
            model.Message = viewModel.Message;
        }

    }

}
