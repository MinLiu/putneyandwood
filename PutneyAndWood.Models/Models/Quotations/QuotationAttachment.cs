﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationAttachment : IAttachment, IShowInPreview
    {
        public QuotationAttachment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }

        public virtual Quotation Quotation { get; set; }
        public virtual ICollection<QuotationPreviewItem> PreviewItems { get; set; }

        public string ToShortLabel() { return Filename; }
        public string ToLongLabel() { return Filename; }
    }




    public class QuotationAttachmentViewModel : IAttachmentViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }

        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }
        [Required(ErrorMessage = "* The Filename is required.")]
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }
    }



    public class QuotationAttachmentMapper : ModelMapper<QuotationAttachment, QuotationAttachmentViewModel>
    {

        public override void MapToViewModel(QuotationAttachment model, QuotationAttachmentViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.Filename = model.Filename;
            viewModel.ShowInPreview = model.ShowInPreview;
        }


        public override void MapToModel(QuotationAttachmentViewModel viewModel, QuotationAttachment model)
        {      
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            //model.File.MapViewModelToModel(viewModel.File);
            model.Filename = viewModel.Filename;
            model.ShowInPreview = viewModel.ShowInPreview;
        }

    }
}
