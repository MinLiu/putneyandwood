﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationNotification : INotification
    {
        public QuotationNotification() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        public string Email { get; set; } 

        public virtual Quotation Quotation { get; set; }

        public string ToShortLabel() { return Email; }
        public string ToLongLabel() { return Email; }
    }


    public class QuotationNotificationViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class QuotationNotificationMapper : ModelMapper<QuotationNotification, QuotationNotificationViewModel>
    {

        public override void MapToViewModel(QuotationNotification model, QuotationNotificationViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Email = model.Email;            
        }


        public override void MapToModel(QuotationNotificationViewModel viewModel, QuotationNotification model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.Email = viewModel.Email;            
        }

    }

}
