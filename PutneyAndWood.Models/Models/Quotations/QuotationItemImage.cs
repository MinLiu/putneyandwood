﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationItemImage : IImageItem<QuotationItem>
    {
        public QuotationItemImage() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid OwnerID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public int SortPos { get; set; }
        public virtual QuotationItem Owner { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }
    
    


    public class QuotationItemImageMapper : ModelMapper<QuotationItemImage, ImageViewModel>
    {

        public override void MapToViewModel(QuotationItemImage model, ImageViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.OwnerID = model.OwnerID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.SortPos = model.SortPos;
        }


        public override void MapToModel(ImageViewModel viewModel, QuotationItemImage model)
        {      
            //model.QuotationID = Guid.Parse(viewModel.QuotationID);
            ////model.File.MapViewModelToModel(viewModel.File);
            //model.Filename = viewModel.Filename;
            //model.FilePath = viewModel.FilePath;
            model.SortPos = viewModel.SortPos;    
        }

    }
}
