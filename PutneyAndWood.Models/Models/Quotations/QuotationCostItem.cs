﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationCostItem : IOrderCostItem<QuotationCostItem>
    {
        public QuotationCostItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        //public Guid? ProductID { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string Unit { get; set; }
        public string JsonCustomFieldValues { get; set; }
        [NotMapped]
        public List<CustomField> CustomFieldValues { get { return JsonCustomFieldValues.CustomFieldsFromString(); } set { JsonCustomFieldValues = value.StringFromCustomFields(); } }


        public decimal Quantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        public decimal Cost { get; set; }
        public decimal Total { get; set; }

        [NotMapped]
        public string ImageURL { get; set; }
        public bool IsKit { get; set; }
        public bool UseKitPrice { get; set; }
        public Guid? ParentCostItemID { get; set; }
        public int SortPos { get; set; }
        public bool IsComment { get; set; }

        public virtual Quotation Quotation { get; set; }
        public virtual QuotationCostItem ParentCostItem { get; set; }

        //Foreign References
        public virtual ICollection<QuotationCostItem> KitCostItems { get; set; }
        

        public string ToShortLabel() { return (IsComment) ? Description.Split('\n')[0].Truncate(50) : string.Format("{0} - {1}", ProductCode, (Description ?? "").Split('\n')[0].Truncate(50)); }
        public string ToLongLabel() { return (IsComment) ? Description.Split('\n')[0].Truncate(50) : string.Format("{0} - {1}", ProductCode, (Description ?? "").Split('\n')[0]); }


        //NOT MAPPED VARIBLES
        [NotMapped]
        public string AccountCode { get; set; }
        [NotMapped]
        public string PurchaseAccountCode { get; set; }
        decimal IPrice.VATPrice { get; set; }
        decimal IPrice.Markup { get; set; }
        decimal IPrice.Discount { get; set; }
        decimal IPrice.ListPrice { get; set; }
        decimal IPrice.Price { get; set; }
        decimal IPrice.ListCost { get; set; }
        decimal IPrice.Margin { get; set; }
        decimal IPrice.MarginPrice { get; set; }
        decimal IPrice.Gross { get; set; }
    }


    public class QuotationCostItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        public string QuotationCostItemID { get; set; }
        public string ProductCode { get; set; }
        public LineItemInfoViewModel Information { get; set; }
        public string Unit { get; set; }

        public decimal Quantity { get; set; }

        public decimal SetupCost { get; set; }
        public decimal VAT { get; set; }
        public decimal Cost { get; set; }
        public decimal Total { get; set; }

        public bool IsKit { get; set; }
        public bool IsKitItem { get; set; }
        public bool IsComment { get; set; }
        public string ParentCostItemID { get; set; }

        public bool HidePrices { get; set; }
        public bool CanDelete { get; set; }


        public int SortPos { get; set; }
        public string CurrencySymbol { get; set; }
        public int StatusID { get; set; }
    }



    public class QuotationCostItemMapper : OrderItemModelMapper<QuotationCostItem, QuotationCostItemViewModel>
    {

        public override void MapToViewModel(QuotationCostItem model, QuotationCostItemViewModel viewModel)
        {
            if (CustomFields == null && CustomFieldService != null)
                CustomFields = CustomFieldService.Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField {  Name = i.Name, Label = i.Label, SortPos = i.SortPos, Show = i.ShowQuotations }).ToList();

            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.QuotationCostItemID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Information = new LineItemInfoViewModel
            {
                Description = model.Description ?? "",
                //DescriptionShort = (model.Description ?? "").Split('\n')[0] + (((model.Description ?? "").Split('\n').Count() <= 0) ? "..." : ""),
                Category = model.Category ?? "",
                //VendorCode = model.VendorCode ?? "",
                //Manufacturer = model.Manufacturer ?? "",
                CustomFieldValues = model.IsComment ? CustomFields : model.CustomFieldValues.AddRemoveRelevent(CustomFields),
            };

            viewModel.IsKit = model.IsKit;
            viewModel.IsComment = model.IsComment;
            viewModel.IsKitItem = (model.ParentCostItem != null);
            viewModel.ParentCostItemID = (model.ParentCostItem != null) ? model.ParentCostItemID.Value.ToString() : null;

            //viewModel.ParentQuantity = (model.ParentCostItem != null) ? model.ParentCostItem.Quantity : 1;
            //viewModel.Quantity = (model.ParentCostItem != null) ? model.ParentCostItem.Quantity * model.Quantity : model.Quantity;
            viewModel.Quantity = model.Quantity;

            viewModel.Unit = model.Unit;
            viewModel.SetupCost = model.SetupCost;
            viewModel.VAT = model.VAT;
            viewModel.Cost = model.Cost;
            viewModel.Total = (model.ParentCostItem != null) ? model.ParentCostItem.Quantity * model.Total : model.Total;

            viewModel.HidePrices = (model.ParentCostItem != null) ? (model.ParentCostItem.UseKitPrice) : (model.IsKit && !model.UseKitPrice);
            viewModel.CanDelete = (model.ParentCostItem != null) ? false : true;
            viewModel.SortPos = model.SortPos;

            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.StatusID = model.Quotation.StatusID;

        }


        public override void MapToModel(QuotationCostItemViewModel viewModel, QuotationCostItem model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.ProductCode = viewModel.ProductCode;
            model.IsKit = viewModel.IsKit;
            model.IsComment = viewModel.IsComment;
            model.ParentCostItemID = (string.IsNullOrEmpty(viewModel.ParentCostItemID)) ? null : Guid.Parse(viewModel.ParentCostItemID) as Guid?;

            if (viewModel.Information != null)
            {
                model.Description = viewModel.Information.Description;
                model.Category = viewModel.Information.Category;
                //model.VendorCode = viewModel.Information.VendorCode;
                //model.Manufacturer = viewModel.Information.Manufacturer;
                model.CustomFieldValues = (model.IsComment) ? null : viewModel.Information.CustomFieldValues;
            }
            //model.ImageURL = viewModel.ImageURL ?? "/Product Images/NotFound.png";
            

            model.Unit = viewModel.Unit;
            model.SetupCost = viewModel.SetupCost;
            model.VAT = viewModel.VAT;
            model.Quantity = viewModel.Quantity;
            model.Cost = viewModel.Cost;
            model.Total = viewModel.Total;

            model.SortPos = viewModel.SortPos;

        }

    }

}