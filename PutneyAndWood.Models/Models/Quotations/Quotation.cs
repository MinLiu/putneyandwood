﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class Quotation : IOrderWithCostsLabour<Quotation, QuotationSection, QuotationItem, QuotationItemImage, QuotationCostItem, QuotationLabourItem, QuotationAttachment, QuotationComment, QuotationNotification, QuotationTemplate, QuotationPreviewItem>
    {
        public Quotation() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? ClientID { get; set; }
        public Guid? ClientContactID { get; set; }
        public string AssignedUserID { get; set; }
        public int StatusID { get; set; }
        public int CurrencyID { get; set; }
        
        public Guid ProjectID { get;set; }
        [Required]
        public string CheckSum { get; set; }
        public string Reference { get; set; }
        public string ClientReference { get; set; }

        public int Number { get; set; }
        public int Version { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string ClosingParagraph { get; set; }
        public string PoNumber { get; set; }
        public string JsonCustomFieldValues { get; set; }
        [NotMapped]
        public List<CustomField> CustomFieldValues { get { return JsonCustomFieldValues.CustomFieldsFromString(); } set { JsonCustomFieldValues = value.StringFromCustomFields(); } }
        public string Greeting { get; set; }
        public string EstimatorTitle { get; set; }

        //One Off Totals
        public decimal Discount { get; set; }
        public decimal Margin { get; set; }
        
        public decimal TotalVat { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalMargin { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }

        //Profit & Loss
        public decimal TotalOtherCosts { get; set; }
        public decimal TotalLabourCosts { get; set; }
        public decimal TotalProfit { get; set; }

        public Guid? InvoiceAddressID { get; set; }
        public Guid? DeliveryAddressID { get; set; }


        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime EstimatedDelivery { get; set; }
        public DateTime? EmailOpened { get; set; }

        public bool UsePriceBreaks { get; set; }

        public bool Deleted { get; set; }

        //Approval Stuff
        public bool IsApproved { get; set; }

        public bool IsClientVersion { get; set; }
        public bool IsClientDraft { get; set; }
        public Guid? MainVersionID { get; set; } //Only keep the ID. Do NOT create foreign key reference. 
        public bool AllowClientEditLineItems { get; set; }
        public bool AllowClientEditSections { get; set; }
        public bool NeedsCheckedClientSubmission { get; set; }

        public virtual Company Company { get; set; }
        public virtual Client Client { get; set; }
        public virtual ClientContact ClientContact { get; set; }
        public virtual ClientAddress InvoiceAddress { get; set; }
        public virtual ClientAddress DeliveryAddress { get; set; }
        public virtual User AssignedUser { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual QuotationStatus Status { get; set; }
        public virtual QuotationSignedAgreement SignedAgreement { get; set; }
        public virtual QuotationStripePayment StripePayment { get; set; }
        public virtual Project Project { get; set; }

        public virtual ICollection<QuotationSection> Sections { get; set; }
        public virtual ICollection<QuotationItem> Items { get; set; }
        public virtual ICollection<QuotationQualification> Qualifications { get; set; }
        public virtual ICollection<QuotationAttachment> Attachments { get; set; }
        public virtual ICollection<QuotationComment> Comments { get; set; }
        public virtual ICollection<QuotationNotification> Notifications { get; set; }
        public virtual ICollection<QuotationCostItem> CostItems { get; set; }
        public virtual ICollection<QuotationLabourItem> LabourItems { get; set; }
        public virtual ICollection<QuotationPreviewItem> PreviewItems { get; set; }
        public virtual ICollection<QuotationEvent> Events { get; set; }
        public virtual ICollection<QuotationBasisOfEstimate> QuotationBasisOfEstimates { get; set; }

        public string ToShortLabel() { return string.Format("{0}{1}-{2}", this.Reference ?? "", this.Number, this.Version); }
        public string ToLongLabel()
        {
            if (this.Client != null) return string.Format("{0}{1}-{2} - {3}", this.Reference ?? "", this.Number, this.Version, this.Client.Name);
            else return string.Format("{0}{1}-{2}", this.Reference ?? "", this.Number, this.Version);
        }

    }




    public class QuotationViewModel : IEntityViewModel
    {
        //[Key]
        public string ID { get; set; }
        public string CompanyID { get; set; }

        public string ProjectID { get; set; }

        public string ClientID { get; set; }
        public string ClientName { get; set; }

        public string ClientContactID { get; set; }
        public string ClientContactName { get; set; }

        public string AssignedUserID { get; set; }
        public string AssignedUserName { get; set; }
        public QuotationStatusViewModel Status { get; set; }

        //[Required]
        public string CheckSum { get; set; }

        public string Reference { get; set; }
        public string ClientReference { get; set; }

        public int Number { get; set; }
        public int Version { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string ClosingParagraph { get; set; }

        public decimal Discount { get; set; }
        public decimal Margin { get; set; }
        public decimal TotalVat { get; set; }
        public decimal TotalMargin { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }

        //Profit & Loss
        public decimal TotalOtherCosts { get; set; }
        public decimal TotalLabourCosts { get; set; }
        public decimal TotalProfit { get; set; }
        public decimal Profit { get; set; }

        public int CurrencyID { get; set; }
        public string CurrencySymbol { get; set; }
        public string Currency { get; set; }
        public string PoNumber { get; set; }
        public List<CustomField> CustomFieldValues { get; set; }

        public string InvoiceAddressID { get; set; }
        public string DeliveryAddressID { get; set; }


        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdated { get; set; }

        [Display(Name = "Delivery Date")]
        public DateTime EstimatedDelivery { get; set; }
        public DateTime? EmailOpened { get; set; }

        public bool UsePriceBreaks { get; set; }

        //Approval Stuff
        public bool IsApproved { get; set; }
        public bool IsClientDraft { get; set; }
        public Guid? MainVersionID { get; set; } //Only keep the ID. Do NOT create foreign key reference. 
        public bool AllowClientEditLineItems { get; set; }
        public bool AllowClientEditSections { get; set; }
        public bool NeedsCheckedClientSubmission { get; set; }

        //public bool HasJobs { get; set; }
        //public bool HasInvoices { get; set; }
        public string SignedAgreementFilename { get; set; }
        public string SignedAgreementURL { get; set; }
        public string ApprovedFilename { get; set; }
        public string ApprovedURL { get; set; }

        public bool IsStripePaid { get; set; }
        public string Greeting { get; set; }
        public string EstimatorTitle { get; set; }

        public IEnumerable<QuotationSectionViewModel> Sections { get; set; }
    }


    public class QuotationGridViewModel : IEntityViewModel
    {
        //[Key]
        public string ID { get; set; }        
        //public int? ClientID { get; set; }
        public string ClientName { get; set; }

        //public string AssignedUserID { get; set; }
        //public string AssignedUserName { get; set; }
        public QuotationStatusViewModel Status { get; set; }
        
        public string Reference { get; set; }
        public string ClientReference { get; set; }

        public int Version { get; set; }
        public string Description { get; set; }
        public string DescriptionShort { get; set; }
        //public string Note { get; set; }
        public List<CustomField> CustomFieldValues { get; set; }

        public decimal Discount { get; set; }
        public decimal Margin { get; set; }
        public decimal TotalVat { get; set; }
        public decimal TotalMargin { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }
        

        //public int CurrencyID { get; set; }
        public string CurrencySymbol { get; set; }
        public string Currency { get; set; }
        public string PoNumber { get; set; }

        //public int InvoiceAddressID { get; set; }
        //public int DeliveryAddressID { get; set; }


        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        [Display(Name = "Delivery Date")]
        public DateTime EstimatedDelivery { get; set; }
        public DateTime? EmailOpened { get; set; }

        public bool UsePriceBreaks { get; set; }

        public bool IsClientDraft { get; set; }
        public bool IsStripePaid { get; set; }

        public string ProjectID { get; set; }
        public string ProjectName { get; set; }
        //Approval Stuff
        //public bool IsApproved { get; set; }
        //public bool NeedsApproval { get; set; }
        //public bool CanApprove { get; set; }

    }


    public class QuotationMapper : OrderModelMapper<Quotation, CustomQuotationField, QuotationViewModel>
    {
        QuotationSectionMapper mapper = new QuotationSectionMapper();
        
        public override void MapToViewModel(Quotation model, QuotationViewModel viewModel)
        {
            if (CustomFields == null && CustomFieldService != null)
                CustomFields = CustomFieldService.Read(model.CompanyID).OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField {  Name = i.Name, Label = i.Label, SortPos = i.SortPos, Show = !i.IsHidden }).ToList();

            viewModel.ID = model.ID.ToString();
            viewModel.CheckSum = model.CheckSum;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.ClientID = (model.Client != null) ? model.Client.ID.ToString() : null;
            viewModel.ClientName = (model.Client != null) ? model.Client.Name : "";
            viewModel.ClientContactID = (model.ClientContact != null) ? model.ClientContact.ID.ToString() : null;
            viewModel.ClientContactName = (model.ClientContact != null) ? model.ClientContact.ToLongLabel() : "";
            viewModel.Status = new QuotationStatusViewModel { ID = model.Status.ID, Name = model.Status.Name, HexColour = model.Status.HexColour };
            viewModel.Reference = model.Reference ?? "";
            viewModel.ClientReference = model.ClientReference ?? "";
            viewModel.PoNumber = model.PoNumber;
            viewModel.Number = model.Number;
            viewModel.Description = model.Description ?? "";
            viewModel.Note = model.Note ?? "";
            viewModel.ClosingParagraph = model.ClosingParagraph ?? "";
            viewModel.CustomFieldValues = model.CustomFieldValues.AddRemoveRelevent(CustomFields);

            viewModel.InvoiceAddressID = (model.InvoiceAddress != null) ? model.InvoiceAddressID.ToString() : null;
            viewModel.DeliveryAddressID = (model.DeliveryAddress != null) ? model.DeliveryAddressID.ToString() : null;
            viewModel.Version = model.Version;
            viewModel.Created = model.Created;
            viewModel.CreatedBy = model.CreatedBy;
            viewModel.LastUpdated = model.LastUpdated;
            viewModel.EstimatedDelivery = model.EstimatedDelivery;
            viewModel.AssignedUserID = (model.AssignedUser != null) ? model.AssignedUser.Id : null;
            viewModel.AssignedUserName = (model.AssignedUser != null) ? model.AssignedUser.Email : String.Empty;
                        
            viewModel.Discount = model.Discount;
            viewModel.TotalVat = model.TotalVat;
            viewModel.Margin = model.Margin;
            viewModel.TotalMargin = model.TotalMargin;
            viewModel.TotalCost = model.TotalCost;
            viewModel.TotalNet = model.TotalNet; 
            viewModel.TotalPrice = model.TotalPrice;
            viewModel.TotalOtherCosts = model.TotalOtherCosts;
            viewModel.TotalLabourCosts = model.TotalLabourCosts;
            viewModel.TotalProfit = model.TotalProfit;
            viewModel.Profit =  (model.TotalPrice != 0) ? model.TotalProfit / model.TotalNet * 100m: 0;

            viewModel.CurrencyID = model.Currency.ID;
            viewModel.CurrencySymbol = model.Currency.Symbol;
            viewModel.Currency = model.Currency.Code;

            viewModel.Greeting = model.Greeting;
            viewModel.EstimatorTitle = model.EstimatorTitle;

            //viewModel.HasJobs = (model.Jobs.Count() > 0);
            //viewModel.HasInvoices = (model.Invoices.Count() > 0);            
            viewModel.Sections = model.Sections.OrderBy(s => s.SortPos).Select(s => mapper.MapToViewModel(s)).ToList();

            if (model.SignedAgreement != null)
            {
                viewModel.SignedAgreementFilename = model.SignedAgreement.PdfAgreementFilename;
                viewModel.SignedAgreementURL = (model.SignedAgreement.PdfAgreement != null) ? model.SignedAgreement.PdfAgreement.FilePath : null;
                viewModel.ApprovedFilename = model.SignedAgreement.PdfApprovedFilename;
                viewModel.ApprovedURL = (model.SignedAgreement.PdfApproved != null) ? model.SignedAgreement.PdfApproved.FilePath : null;
            }

            viewModel.UsePriceBreaks = model.UsePriceBreaks;
            viewModel.EmailOpened = model.EmailOpened;
            viewModel.AllowClientEditLineItems = model.AllowClientEditLineItems;
            viewModel.AllowClientEditSections = model.AllowClientEditSections;
            viewModel.NeedsCheckedClientSubmission = model.NeedsCheckedClientSubmission;

            viewModel.IsClientDraft = model.IsClientDraft;
            viewModel.IsStripePaid = (model.StripePayment != null) ? model.StripePayment.IsPaid : false;
            viewModel.ProjectID = model.ProjectID.ToString();

        }


        public override void MapToModel(QuotationViewModel viewModel, Quotation model)
        {
            model.PoNumber = viewModel.PoNumber;

            if (model.StatusID == 1)
            {
                //model.CheckSum = viewModel.CheckSum;
                //model.CompanyID = viewModel.CompanyID;
                model.ClientID = (!string.IsNullOrEmpty(viewModel.ClientID)) ? Guid.Parse(viewModel.ClientID) as Guid? : null;
                model.ClientContactID = (!string.IsNullOrEmpty(viewModel.ClientContactID)) ? Guid.Parse(viewModel.ClientContactID) as Guid? : null;
                //model.StatusID = viewModel.Status.ID;
                model.Reference = viewModel.Reference;
                model.ClientReference = viewModel.ClientReference;                
                model.Number = viewModel.Number;
                model.Description = viewModel.Description;
                model.Note = viewModel.Note;
                model.ClosingParagraph = viewModel.ClosingParagraph;
                model.CustomFieldValues = viewModel.CustomFieldValues;

                model.InvoiceAddressID = (string.IsNullOrEmpty(viewModel.InvoiceAddressID)) ? null : Guid.Parse(viewModel.InvoiceAddressID) as Guid?;
                model.DeliveryAddressID = (string.IsNullOrEmpty(viewModel.DeliveryAddressID)) ? null : Guid.Parse(viewModel.DeliveryAddressID) as Guid?;
                model.Version = viewModel.Version;
                model.Created = viewModel.Created;
                model.LastUpdated = viewModel.LastUpdated;
                model.EstimatedDelivery = viewModel.EstimatedDelivery;
                model.AssignedUserID = (viewModel.AssignedUserID == "") ? null : viewModel.AssignedUserID;

                model.Discount = viewModel.Discount;
                model.TotalVat = viewModel.TotalVat;
                model.Margin = viewModel.Margin;
                model.TotalMargin = viewModel.TotalMargin;
                model.TotalCost = viewModel.TotalCost;
                model.TotalNet = viewModel.TotalNet;
                model.TotalPrice = viewModel.TotalPrice;
                model.TotalOtherCosts = viewModel.TotalOtherCosts;
                model.TotalLabourCosts = viewModel.TotalLabourCosts;
                model.TotalProfit = viewModel.TotalProfit;

                model.CurrencyID = viewModel.CurrencyID;

                model.EstimatorTitle = viewModel.EstimatorTitle;
                model.Greeting = viewModel.Greeting;
            }

            model.AllowClientEditLineItems = viewModel.AllowClientEditLineItems;
            model.AllowClientEditSections = viewModel.AllowClientEditSections;
            model.NeedsCheckedClientSubmission = viewModel.NeedsCheckedClientSubmission;

        }


        public override void MapToModelHeader(QuotationViewModel viewModel, Quotation model)
        {
            model.PoNumber = viewModel.PoNumber;

            if (model.StatusID == 1)
            {
                //model.CheckSum = viewModel.CheckSum;
                //model.CompanyID = viewModel.CompanyID;
                model.ClientID = (!string.IsNullOrEmpty(viewModel.ClientID)) ? Guid.Parse(viewModel.ClientID) as Guid? : null;
                model.ClientContactID = (!string.IsNullOrEmpty(viewModel.ClientContactID)) ? Guid.Parse(viewModel.ClientContactID) as Guid? : null;
                //model.StatusID = viewModel.Status.ID;
                //model.Reference = viewModel.Reference;
                model.ClientReference = viewModel.ClientReference;                
                //model.Number = viewModel.Number;
                model.Description = viewModel.Description;
                model.Note = viewModel.Note;
                model.ClosingParagraph = viewModel.ClosingParagraph;
                model.CustomFieldValues = viewModel.CustomFieldValues;

                model.InvoiceAddressID = (string.IsNullOrEmpty(viewModel.InvoiceAddressID)) ? null : Guid.Parse(viewModel.InvoiceAddressID) as Guid?;
                model.DeliveryAddressID = (string.IsNullOrEmpty(viewModel.DeliveryAddressID)) ? null : Guid.Parse(viewModel.DeliveryAddressID) as Guid?;
                //model.Version = viewModel.Version;
                //model.Created = viewModel.Created;
                model.LastUpdated = viewModel.LastUpdated;
                model.EstimatedDelivery = viewModel.EstimatedDelivery;
                model.AssignedUserID = (viewModel.AssignedUserID == "") ? null : viewModel.AssignedUserID;

                //model.Discount = viewModel.Discount;
                //model.TotalVat = viewModel.TotalVat;
                //model.Margin = viewModel.Margin;
                //model.TotalMargin = viewModel.TotalMargin;
                //model.TotalNet = viewModel.TotalNet;
                //model.TotalPrice = viewModel.TotalPrice;
                //model.TotalOtherCosts = viewModel.TotalOtherCosts;
                //model.TotalLabourCosts = viewModel.TotalLabourCosts;
                //model.TotalProfit = viewModel.TotalProfit;

                model.CurrencyID = viewModel.CurrencyID;

                model.EstimatorTitle = viewModel.EstimatorTitle;
                model.Greeting = viewModel.Greeting;
            }

            model.AllowClientEditLineItems = viewModel.AllowClientEditLineItems;
            model.AllowClientEditSections = viewModel.AllowClientEditSections;
            model.NeedsCheckedClientSubmission = viewModel.NeedsCheckedClientSubmission;
        }

    }


    public class QuotationGridMapper : OrderModelMapper<Quotation, CustomQuotationField, QuotationGridViewModel>
    {

        public override void MapToViewModel(Quotation model, QuotationGridViewModel viewModel)
        {
            if (CustomFields == null && CustomFieldService != null)
                CustomFields = CustomFieldService.Read(model.CompanyID).OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos, Show = !i.IsHidden }).ToList();

            viewModel.ID = model.ID.ToString();
            viewModel.ClientName = (model.Client != null) ? model.Client.Name : "";
            viewModel.Status = new QuotationStatusViewModel { ID = model.Status.ID, Name = model.Status.Name, HexColour = model.Status.HexColour };
            viewModel.Reference = string.Format("{0}{1}", model.Reference ?? "", model.Number);
            viewModel.ClientReference = model.ClientReference ?? "";
            viewModel.PoNumber = model.PoNumber;
            viewModel.Description = model.Description ?? "";
            viewModel.DescriptionShort = model.Description != null ? model.Description.Split(new[] { "\n" }, StringSplitOptions.None)[0].Truncate(150) : "";
            viewModel.CustomFieldValues = model.CustomFieldValues.AddRemoveRelevent(CustomFields);
            viewModel.Version = model.Version;
            viewModel.Created = model.Created;
            viewModel.LastUpdated = model.LastUpdated;

            viewModel.EstimatedDelivery = model.EstimatedDelivery;
            //viewModel.AssignedUserID = (model.AssignedUser != null) ? model.AssignedUser.Id : null;
            //viewModel.AssignedUserName = (model.AssignedUser != null) ? String.Format("{0} {1}", model.AssignedUser.FirstName, model.AssignedUser.LastName) : String.Empty;

            viewModel.Discount = model.Discount;
            viewModel.TotalVat = model.TotalVat;
            viewModel.Margin = model.Margin;
            viewModel.TotalMargin = model.TotalMargin;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalPrice = model.TotalPrice;

            viewModel.CurrencySymbol = model.Currency.Symbol;
            viewModel.Currency = model.Currency.Code;
            viewModel.EmailOpened = model.EmailOpened;
            viewModel.UsePriceBreaks = model.UsePriceBreaks;
            viewModel.IsClientDraft = model.IsClientDraft;
            viewModel.IsStripePaid = (model.StripePayment != null) ? model.StripePayment.IsPaid : false;

            viewModel.ProjectID = model.ProjectID.ToString();
            viewModel.ProjectName = model.Project.Name;

        }


        public override void MapToModel(QuotationGridViewModel viewModel, Quotation model)
        {
            throw new NotImplementedException();
        }

        public override void MapToModelHeader(QuotationGridViewModel viewModel, Quotation model)
        {
            throw new NotImplementedException();
        }
    }
}
