﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{
    public class CustomQuotationField : ICustomField
    {
        public CustomQuotationField() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public int SortPos { get; set; }

        public bool ShowMainList { get; set; }

        public bool IsHidden { get; set; }

        public virtual Company Company { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }


    public class CustomQuotationFieldViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public int SortPos { get; set; }

        public bool ShowMainList { get; set; }

        public bool IsHidden { get; set; }
    }

    

    public class CustomQuotationFieldMapper : ModelMapper<CustomQuotationField, CustomQuotationFieldViewModel>
    {

        public override void MapToViewModel(CustomQuotationField model, CustomQuotationFieldViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;
            viewModel.Label = model.Label;
            viewModel.SortPos = model.SortPos;
            viewModel.ShowMainList = model.ShowMainList;
            viewModel.IsHidden = model.IsHidden;
        }


        public override void MapToModel(CustomQuotationFieldViewModel viewModel, CustomQuotationField model)
        {
            viewModel.Name = new Regex("[^A-Za-z0-9 -$\n_]").Replace(viewModel.Name, "");
            viewModel.Name = viewModel.Name.Replace(" ", "_").Replace("__", "_");

            //ID = viewModel.ID,
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.Label = viewModel.Label;
            model.SortPos = viewModel.SortPos;
            model.ShowMainList = viewModel.ShowMainList;
            model.IsHidden = viewModel.IsHidden;

        }

    }

    
}

