﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationSection : IOrderSection<QuotationSection, QuotationItem, QuotationItemImage>
    {
        public QuotationSection() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }

        public string Name { get; set; }
        public bool IncludeInTotal { get; set; }
        public bool IsRequired { get; set; }
        public int SortPos { get; set; }

        //Section Totals (Calculated)
        public decimal Discount { get; set; }
        public decimal Margin { get; set; }
        public decimal TotalVat { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalMargin { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }
       
        public virtual Quotation Quotation { get; set; }
        public virtual ICollection<QuotationItem> Items { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }


    public class QuotationSectionViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SectionID { get; set; }
        public string QuotationID { get; set; }
        public string Name { get; set; }
        public bool IncludeInTotal { get; set; }
        public bool IsRequired { get; set; }
        public int SortPos { get; set; }

        //Section Totals
        public string CurrencySymbol { get; set; }
        public decimal Discount { get; set; }
        public decimal Margin { get; set; }
        public decimal TotalVat { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalMargin { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalPrice { get; set; }

    }

    public class QuotationSectionMapper : ModelMapper<QuotationSection, QuotationSectionViewModel>
    {

        public override void MapToViewModel(QuotationSection model, QuotationSectionViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.SectionID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.Name = model.Name;
            viewModel.IncludeInTotal = model.IncludeInTotal;
            viewModel.IsRequired = model.IsRequired;
            viewModel.SortPos = model.SortPos;

            viewModel.CurrencySymbol = model.Quotation.Currency.Symbol;
            viewModel.Discount = model.Discount;
            viewModel.Margin = model.Margin;
            viewModel.TotalVat = model.TotalVat;
            viewModel.TotalDiscount = model.TotalDiscount;
            viewModel.TotalMargin = model.TotalMargin;
            viewModel.TotalCost = model.TotalCost;
            viewModel.TotalNet = model.TotalNet;
            viewModel.TotalPrice = model.TotalPrice;
        }


        public override void MapToModel(QuotationSectionViewModel viewModel, QuotationSection model)
        {
            //model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.Name = viewModel.Name;
            model.IncludeInTotal = viewModel.IncludeInTotal;
            model.IsRequired = viewModel.IsRequired;
            //model.SortPos = viewModel.SortPos;

            //Calculated, no need to rebind
            //model.Discount = viewModel.Discount;
            //model.Margin = viewModel.Margin;
            //model.TotalVat = viewModel.TotalVat;
            //model.TotalDiscount = viewModel.TotalDiscount;
            //model.TotalMargin = viewModel.TotalMargin;
            //model.TotalNet = viewModel.TotalNet;
            //model.TotalPrice = viewModel.TotalPrice;
        }

    }

}
