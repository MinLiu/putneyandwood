﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationRepository : EntityRespository<Quotation>
    {
        public QuotationRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationRepository(SnapDbContext db)
            : base(db)
        {

        }

        //public override void Update(Quotation entity, string[] updatedColumns = null)
        //{
        //    if (updatedColumns != null)
        //    {
        //        var list = new List<string>(updatedColumns);
        //        list.Add("LastUpdated");
        //        updatedColumns = list.ToArray();
        //    }

        //    entity.LastUpdated = DateTime.Now;
        //    base.Update(entity, updatedColumns);
        //}
    

        public override void Delete(Quotation entity)
        {            
            _db.Set<QuotationItem>().RemoveRange(_db.Set<QuotationItem>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationSection>().RemoveRange(_db.Set<QuotationSection>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationAttachment>().RemoveRange(_db.Set<QuotationAttachment>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationComment>().RemoveRange(_db.Set<QuotationComment>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationNotification>().RemoveRange(_db.Set<QuotationNotification>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationCostItem>().RemoveRange(_db.Set<QuotationCostItem>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationLabourItem>().RemoveRange(_db.Set<QuotationLabourItem>().Where(p => p.QuotationID == entity.ID));
            _db.Set<QuotationItemPriceBreak>().RemoveRange(_db.Set<QuotationItemPriceBreak>().Where(p => p.QuotationItem.QuotationID == entity.ID));
            _db.SaveChanges();


            var set = _db.Set<Quotation>().Where(a => a.ID == entity.ID);

            _db.Set<Quotation>().RemoveRange(set);
            _db.SaveChanges();
            //base.Delete(entity);
        }

    }

}
