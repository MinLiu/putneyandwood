﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationTemplateRepository : EntityRespository<QuotationTemplate>
    {
        public QuotationTemplateRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationTemplateRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(QuotationTemplate entity)
        {
            var set = _db.Set<QuotationTemplate>().Where(a => a.ID == entity.ID)
                                    //.Include(a => a.FrontPreviews)
                                    //.Include(a => a.BodyPreviews)
                                    .Include(a => a.PreviewItems);

            _db.Set<QuotationTemplate>().RemoveRange(set);
            _db.SaveChanges();
            //base.Delete(entity);
        }

    }

}
