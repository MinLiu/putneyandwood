﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationSettingsRepository : EntityRespository<QuotationSettings>
    {
        public QuotationSettingsRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationSettingsRepository(SnapDbContext db)
            : base(db)
        {

        }


        public QuotationSettings GetSettings(Guid companyID)
        {
            var settings = _db.Set<QuotationSettings>().Where(s => s.CompanyID == companyID);

            if(settings.Count() <= 0)
            {
                var set = new QuotationSettings();
                set.CompanyID = companyID;
                _db.Set<QuotationSettings>().Add(set);
                _db.SaveChanges();

                return set;
            }
            else
            {
                return settings.First();
            }
        }

    }

}
