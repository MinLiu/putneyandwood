﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class DefaultQuotationAttachmentRepository : EntityRespository<DefaultQuotationAttachment>
    {
        public DefaultQuotationAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public DefaultQuotationAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
