﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationNotificationRepository : EntityRespository<QuotationNotification>
    {
        public QuotationNotificationRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationNotificationRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
