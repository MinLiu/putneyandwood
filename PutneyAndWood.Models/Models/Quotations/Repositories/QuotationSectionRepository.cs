﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationSectionRepository : EntityRespository<QuotationSection>
    {
        public QuotationSectionRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationSectionRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(QuotationSection entity)
        {
            _db.Set<QuotationItem>().RemoveRange(_db.Set<QuotationItem>().Where(p => p.SectionID == entity.ID));
            _db.SaveChanges();

            _db.Set<QuotationSection>().RemoveRange(_db.Set<QuotationSection>().Where(p => p.ID == entity.ID));
            _db.SaveChanges();

        }

    }

}
