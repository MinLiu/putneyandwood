﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationEventRepository : EntityRespository<QuotationEvent>
    {
        public QuotationEventRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationEventRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
