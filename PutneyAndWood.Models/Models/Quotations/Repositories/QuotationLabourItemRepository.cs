﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationLabourItemRepository : EntityRespository<QuotationLabourItem>
    {
        public QuotationLabourItemRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationLabourItemRepository(SnapDbContext db)
            : base(db)
        {

        }
        

    }

}
