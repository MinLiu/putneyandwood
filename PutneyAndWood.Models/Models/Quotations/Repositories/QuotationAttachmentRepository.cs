﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationAttachmentRepository : EntityRespository<QuotationAttachment>
    {
        public QuotationAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
