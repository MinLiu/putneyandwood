﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationCommentRepository : EntityRespository<QuotationComment>
    {
        public QuotationCommentRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationCommentRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
