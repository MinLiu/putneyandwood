﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationCostItemRepository : EntityRespository<QuotationCostItem>
    {
        public QuotationCostItemRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationCostItemRepository(SnapDbContext db)
            : base(db)
        {

        }
        

    }

}
