﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationQualificationRepository : EntityRespository<QuotationQualification>
    {
        public QuotationQualificationRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationQualificationRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
