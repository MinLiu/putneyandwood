﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationItemPriceBreakRepository : EntityRespository<QuotationItemPriceBreak>
    {
        public QuotationItemPriceBreakRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationItemPriceBreakRepository(SnapDbContext db)
            : base(db)
        {

        }



    }

}
