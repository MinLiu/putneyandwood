﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationItemRepository : EntityRespository<QuotationItem>
    {
        public QuotationItemRepository()
            : this(new SnapDbContext())
        {

        }

        public QuotationItemRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(QuotationItem entity)
        {
            _db.Set<QuotationItemPriceBreak>().RemoveRange(_db.Set<QuotationItemPriceBreak>().Where(a => a.QuotationItem.ID == entity.ID || a.QuotationItem.ParentItemID == entity.ID));
            var set = _db.Set<QuotationItem>().Where(a => a.ID == entity.ID || a.ParentItemID == entity.ID);
            _db.Set<QuotationItem>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
