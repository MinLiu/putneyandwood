﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class DefaultQuotationNotification : IEntity, INotification
    {
        public DefaultQuotationNotification() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationSettingsID { get; set; }
        public string Email { get; set; }

        public virtual QuotationSettings QuotationSettings { get; set; }

        public string ToShortLabel() { return Email; }
        public string ToLongLabel() { return Email; }
    }



    public class DefaultQuotationNotificationViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationSettingsID { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }


    public class DefaultQuotationNotificationMapper : ModelMapper<DefaultQuotationNotification, DefaultQuotationNotificationViewModel>
    {
        public override void MapToViewModel(DefaultQuotationNotification model, DefaultQuotationNotificationViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationSettingsID = model.QuotationSettingsID.ToString();
            viewModel.Email = model.Email;
        }

        public override void MapToModel(DefaultQuotationNotificationViewModel viewModel, DefaultQuotationNotification model)
        {
            model.QuotationSettingsID = Guid.Parse(viewModel.QuotationSettingsID);
            model.Email = viewModel.Email;
        }
    }
    
}
