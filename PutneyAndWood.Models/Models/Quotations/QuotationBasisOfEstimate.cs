﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Mvc;

namespace SnapSuite.Models
{
    public class QuotationBasisOfEstimate : IEntity
    {
        public QuotationBasisOfEstimate() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid QuotationID { get; set; }
        public string Description { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }

        public virtual Quotation Quotation { get; set; }   

        public string ToLongLabel() { return ""; }
        public string ToShortLabel() { return ""; }
    }



    public class QuotationBasisOfEstimateViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string QuotationID { get; set; }
        [AllowHtml]
        public string BasisOfEstimateDescription { get; set; }
        public int SortPos { get; set; }
        public bool IsSubHeading { get; set; }
        public bool IsSubItem { get; set; }
    }



    public class QuotationBasisOfEstimateMapper : ModelMapper<QuotationBasisOfEstimate, QuotationBasisOfEstimateViewModel>    
    {
        public override void MapToViewModel(QuotationBasisOfEstimate model, QuotationBasisOfEstimateViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationID = model.QuotationID.ToString();
            viewModel.SortPos = model.SortPos;
            viewModel.BasisOfEstimateDescription = model.Description;
            viewModel.IsSubHeading = model.IsSubHeading;
            viewModel.IsSubItem = model.IsSubItem;
        }

        public override void MapToModel(QuotationBasisOfEstimateViewModel viewModel, QuotationBasisOfEstimate model)
        {
            model.QuotationID = Guid.Parse(viewModel.QuotationID);
            model.SortPos = viewModel.SortPos;
            model.Description = viewModel.BasisOfEstimateDescription;
            model.IsSubHeading = viewModel.IsSubHeading;
            model.IsSubItem = viewModel.IsSubItem;
        }
    }
}
