﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class DefaultQuotationAttachment : IAttachment, IShowInPreview
    {
        public DefaultQuotationAttachment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid QuotationSettingsID { get; set; }

        public Guid FileID { get; set; }
        public virtual FileEntry File { get; set; }
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }

        public virtual QuotationSettings QuotationSettings { get; set; }

        public string ToShortLabel() { return Filename; }
        public string ToLongLabel() { return Filename; }
    }



    public class DefaultQuotationAttachmentViewModel : IAttachmentViewModel
    {
        public string ID { get; set; }
        public string QuotationSettingsID { get; set; }

        public string FileID { get; set; }
        public FileEntryViewModel File { get; set; }
        [Required(ErrorMessage = "* The Filename is required.")]
        public string Filename { get; set; }
        public bool ShowInPreview { get; set; }
    }


    public class DefaultQuotationAttachmentMapper : ModelMapper<DefaultQuotationAttachment, DefaultQuotationAttachmentViewModel>
    {
        public override void MapToViewModel(DefaultQuotationAttachment model, DefaultQuotationAttachmentViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.QuotationSettingsID = model.QuotationSettingsID.ToString();
            viewModel.FileID = model.FileID.ToString();
            viewModel.File = model.File.ToViewModel();
            viewModel.Filename = model.Filename;
            viewModel.ShowInPreview = model.ShowInPreview;
        }

        public override void MapToModel(DefaultQuotationAttachmentViewModel viewModel, DefaultQuotationAttachment model)
        {
            model.QuotationSettingsID = Guid.Parse(viewModel.QuotationSettingsID);
            //model.File.MapViewModelToModel(viewModel.File);
            model.Filename = viewModel.Filename;
            model.ShowInPreview = viewModel.ShowInPreview;
        }
    }

}
