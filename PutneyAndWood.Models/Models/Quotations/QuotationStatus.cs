﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class QuotationStatus : ILoggable
    {
        [Key]
        public int ID { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }

        public string ToLongLabel() { return Name; }
        public string ToShortLabel() { return Name; }
    }

    public static class QuotationStatusValues
    {
        public static int DRAFT = 1;
        public static int SENT = 2;
        public static int ACCEPTED = 3;
        public static int DECLINED = 4;
        public static int DEPRECATED = 5;
        public static int COMMENCED = 6;
        public static int COMPLETE = 7;

        /*         
        public static int DRAFT = 1;
        public static int SENT = 2;
        public static int ACCEPTED = 3;
        public static int DECLINED = 4;
        public static int COMMENCED = 5;
        public static int COMPLETE = 6;         
         */
    }



    public class QuotationStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
    }




    public class QuotationStatusMapper : ModelMapper<QuotationStatus, QuotationStatusViewModel>
    {
        public override void MapToViewModel(QuotationStatus model, QuotationStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(QuotationStatusViewModel viewModel, QuotationStatus model)
        {
            model.Name = viewModel.Name;
            model.Description = viewModel.Description;
            model.HexColour = viewModel.HexColour;
        }

    }
        
}

