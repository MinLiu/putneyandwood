﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionCartItem
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public Guid CompanyID { get; set; }

        [NotMapped]
        public string CartID { 
            get { return string.Format("QFLW-01-{0}", ID); }
            private set { } 
        }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberCRM { get; set; }
        public int NumberProducts { get; set; }
        public int NumberQuotations { get; set; }
        public int NumberJobs { get; set; }
        public int NumberInvoices { get; set; }
        public int NumberDeliveryNotes { get; set; }
        public int NumberPurchaseOrders { get; set; }
        public int NumberStockControl { get; set; }
        public bool EnableTeamManagement { get; set; }

        public decimal InitialAmount { get; set; }
        public decimal RegularAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool Complete { get; set; }

        [NotMapped]
        public string MD5Hash {
            get
            {
                //MD5 SignatureFields: <secret>:instId:amount:currency:cartId
                string key = "Apple&2Bananas&3Oranges";
                string input = string.Format("{0}:{1}:{2}:{3}:{4}",
                                            key,
                                            "1155755",
                                            string.Format("{0:0.00}", RegularAmount),
                                            "GBP",
                                            CartID
                );

                MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hash = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                return sb.ToString();
            }

            private set { }
        }

        public virtual Company Company { get; set; }

    }


    public class SubscriptionCartItemViewModel
    {

        public int ID { get; set; }
        public string CompanyID { get; set; }

        public string CartID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberCRM { get; set; }
        public int NumberProducts { get; set; }
        public int NumberQuotations { get; set; }
        public int NumberJobs { get; set; }
        public int NumberInvoices { get; set; }
        public int NumberDeliveryNotes { get; set; }
        public int NumberPurchaseOrders { get; set; }
        public int NumberStockControl { get; set; }
        public bool EnableTeamManagement { get; set; }

        public decimal InitialAmount { get; set; }
        public decimal RegularAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool Complete { get; set; }

        public string MD5Hash { get; set; }
    }



    public class SubscriptionCartItemMapper : ModelMapper<SubscriptionCartItem, SubscriptionCartItemViewModel>
    {
        public override void MapToViewModel(SubscriptionCartItem model, SubscriptionCartItemViewModel viewModel)
        {              
            viewModel.ID = model.ID;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CartID = model.CartID;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.NumberCRM = model.NumberCRM;
            viewModel.NumberProducts = model.NumberProducts;
            viewModel.NumberQuotations = model.NumberQuotations;
            viewModel.NumberJobs = model.NumberJobs;
            viewModel.NumberInvoices = model.NumberInvoices;
            viewModel.NumberDeliveryNotes = model.NumberDeliveryNotes;
            viewModel.NumberPurchaseOrders = model.NumberPurchaseOrders;
            viewModel.NumberStockControl = model.NumberStockControl;
            viewModel.EnableTeamManagement = model.EnableTeamManagement;

            viewModel.InitialAmount = model.InitialAmount;
            viewModel.RegularAmount = model.RegularAmount;
            viewModel.DiscountAmount = model.DiscountAmount;
            viewModel.Complete = model.Complete;

            viewModel.MD5Hash = model.MD5Hash;
        }

        public override void MapToModel(SubscriptionCartItemViewModel viewModel, SubscriptionCartItem model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            //model.CartID = viewModel.CartID;
            model.Name = viewModel.Name;
            model.Description = viewModel.Description;
            model.NumberCRM = viewModel.NumberCRM;
            model.NumberProducts = viewModel.NumberProducts;
            model.NumberQuotations = viewModel.NumberQuotations;
            model.NumberJobs = viewModel.NumberJobs;
            model.NumberInvoices = viewModel.NumberInvoices;
            model.NumberDeliveryNotes = viewModel.NumberDeliveryNotes;
            model.NumberPurchaseOrders = viewModel.NumberPurchaseOrders;
            model.NumberStockControl = viewModel.NumberStockControl;
            model.EnableTeamManagement = viewModel.EnableTeamManagement;

            model.InitialAmount = viewModel.InitialAmount;
            model.RegularAmount = viewModel.RegularAmount;
            model.DiscountAmount = viewModel.DiscountAmount;
            model.Complete = viewModel.Complete;

            //model.MD5_Reference = viewModel.MD5_Reference;
        }

    }

}
