﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionRequest : IEntity
    {
        public SubscriptionRequest() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }        
        [Required]
        public Guid CompanyID { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string Message { get; set; }
        public bool Complete { get; set; }

        public string ToShortLabel() { return string.Format("{0} - {1}", Timestamp.ToShortDateString(), CompanyName); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Timestamp.ToShortDateString(), CompanyName); }
    }

    public class SubscriptionRequestViewModel : IEntityViewModel
    {
        [Key]
        public string ID { get; set; }
                
        public DateTime Timestamp { get; set; }
        public string CompanyID { get; set; }

        [Required]
        public string Email { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string Message { get; set; }
        public bool Complete { get; set; }

    }



    public class SubscriptionRequestMapper : ModelMapper<SubscriptionRequest, SubscriptionRequestViewModel>
    {
        public override void MapToViewModel(SubscriptionRequest model, SubscriptionRequestViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Email = model.Email;
            viewModel.CompanyName = model.CompanyName;
            viewModel.Message = model.Message;
            viewModel.Complete = model.Complete;

        }

        public override void MapToModel(SubscriptionRequestViewModel viewModel, SubscriptionRequest model)
        {
            //model.ID = viewModel.ID;
            model.Timestamp = viewModel.Timestamp;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Email = viewModel.Email;
            model.CompanyName = viewModel.CompanyName;
            model.Message = viewModel.Message;
            model.Complete = viewModel.Complete;
        }

    }

}
