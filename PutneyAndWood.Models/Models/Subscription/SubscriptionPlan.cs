﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionPlan : ICompanyEntity, IEntity
    {
        public SubscriptionPlan() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime Created { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public decimal InitialAmount { get; set; }
        public decimal RegularAmount { get; set; }
        public bool Active { get; set; }
        public bool Trial { get; set; }

        public int NumberCRM { get; set; }
        public int NumberProducts { get; set; }
        public int NumberQuotations { get; set; }
        public int NumberJobs { get; set; }
        public int NumberInvoices { get; set; }
        public int NumberDeliveryNotes { get; set; }
        public int NumberPurchaseOrders { get; set; }
        public int NumberStockControl { get; set; }
        public bool EnableTeamManagement { get; set; }

        //World Pay (FuturePay) Values
        public string FuturePayID { get; set; }
        public int InternalCartID { get; set; }
        public string CartID { get; set; }
        

        public virtual Company Company { get; set; }


        public string ToShortLabel() { return string.Format("{0} - {1}", Name, Description); }
        public string ToLongLabel() { return string.Format("{0} {1} - {2} {3} {4}", StartDate.ToShortDateString(), EndDate.ToShortDateString(), Name, Description, (Active ? " (Active)" : "")); }

        /*
        public int MaxNumberUsers()
        {
            int[] users = new int[] { NumberCRM, NumberProducts, NumberQuotations, NumberJobs, NumberInvoices, NumberDeliveryNotes, NumberPurchaseOrders, NumberStockControl };
            return users.OrderByDescending(x => x).First();
        }
        */

    }


    public class SubscriptionPlanViewModel : IEntityViewModel
    {
        [Key]
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime Created { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public decimal InitialAmount { get; set; }
        public decimal RegularAmount { get; set; }
        public bool Active { get; set; }
        public bool Trial { get; set; }

        public int NumberCRM { get; set; }
        public int NumberProducts { get; set; }
        public int NumberQuotations { get; set; }
        public int NumberJobs { get; set; }
        public int NumberInvoices { get; set; }
        public int NumberDeliveryNotes { get; set; }
        public int NumberPurchaseOrders { get; set; }
        public int NumberStockControl { get; set; }
        public bool EnableTeamManagement { get; set; }

        //World Pay Values
        public string FuturePayID { get; set; }
        public int InternalCartID { get; set; }
        public string CartID { get; set; }


        

        
    }



    public class SubscriptionPlanMapper : ModelMapper<SubscriptionPlan, SubscriptionPlanViewModel>
    {
        public override void MapToViewModel(SubscriptionPlan model, SubscriptionPlanViewModel viewModel)
        {              
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.Created = model.Created;
            viewModel.StartDate = model.StartDate;
            viewModel.EndDate = model.EndDate;
            viewModel.InitialAmount = model.InitialAmount;
            viewModel.RegularAmount = model.RegularAmount;
            viewModel.Active = model.Active;
            viewModel.Trial = model.Trial;
            viewModel.EnableTeamManagement = model.EnableTeamManagement;

            viewModel.NumberCRM = model.NumberCRM;
            viewModel.NumberProducts = model.NumberProducts;
            viewModel.NumberQuotations = model.NumberQuotations;
            viewModel.NumberJobs = model.NumberJobs;
            viewModel.NumberInvoices = model.NumberInvoices;
            viewModel.NumberDeliveryNotes = model.NumberDeliveryNotes;
            viewModel.NumberPurchaseOrders = model.NumberPurchaseOrders;
            viewModel.NumberStockControl = model.NumberStockControl;

            viewModel.FuturePayID = model.FuturePayID;
            viewModel.InternalCartID = model.InternalCartID;
            viewModel.CartID = model.CartID;

        }

        public override void MapToModel(SubscriptionPlanViewModel viewModel, SubscriptionPlan model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.Description = viewModel.Description;
            //model.Created = viewModel.Created;
            model.StartDate = viewModel.StartDate;
            model.EndDate = viewModel.EndDate;
            model.InitialAmount = viewModel.InitialAmount;
            model.RegularAmount = viewModel.RegularAmount;
            model.Active = viewModel.Active;
            model.Trial = viewModel.Trial;

            model.NumberCRM = viewModel.NumberCRM;
            model.NumberProducts = viewModel.NumberProducts;
            model.NumberQuotations = viewModel.NumberQuotations;
            model.NumberJobs = viewModel.NumberJobs;
            model.NumberInvoices = viewModel.NumberInvoices;
            model.NumberDeliveryNotes = viewModel.NumberDeliveryNotes;
            model.NumberPurchaseOrders = viewModel.NumberPurchaseOrders;
            model.NumberStockControl = viewModel.NumberStockControl;
            model.EnableTeamManagement = viewModel.EnableTeamManagement;

            model.FuturePayID = viewModel.FuturePayID;
            model.InternalCartID = viewModel.InternalCartID;
            model.CartID = viewModel.CartID;
        }

    }

}
