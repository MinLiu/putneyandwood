﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionPlanPayment : IEntity
    {
        public SubscriptionPlanPayment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        [MaxLength(3)]
        public string Currency { get; set; } 

        //World Pay (FuturePay) Values
        public string FuturePayID { get; set; }
        
        public virtual Company Company { get; set; }

        public string ToShortLabel() { return string.Format("{0} {1} {2} {2}", Timestamp.ToShortDateString(), Description, Amount.ToString("#,##0.00"), Currency); }
        public string ToLongLabel() { return string.Format("{0} {1} {2} {2}", Timestamp.ToShortDateString(), Description, Amount.ToString("#,##0.00"), Currency); }
    }


    public class SubscriptionPlanPaymentViewModel : IEntityViewModel
    {
        [Key]
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }

        public DateTime Timestamp { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        [MaxLength(3)]
        public string Currency { get; set; }

        //World Pay (FuturePay) Values
        public string FuturePayID { get; set; }
    }



    public class SubscriptionPlanPaymentMapper : ModelMapper<SubscriptionPlanPayment, SubscriptionPlanPaymentViewModel>
    {
        public override void MapToViewModel(SubscriptionPlanPayment model, SubscriptionPlanPaymentViewModel viewModel)
        {              
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.CompanyName = model.Company.Name;
            viewModel.Timestamp = model.Timestamp;
            viewModel.Description = model.Description;
            viewModel.Amount = model.Amount;
            viewModel.FuturePayID = model.FuturePayID;   
            viewModel.Currency = model.Currency;
        }

        public override void MapToModel(SubscriptionPlanPaymentViewModel viewModel, SubscriptionPlanPayment model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);            
            model.Timestamp = viewModel.Timestamp;
            model.Description = viewModel.Description;
            model.Amount = viewModel.Amount;           
            model.FuturePayID = viewModel.FuturePayID;            
            model.Currency = viewModel.Currency;
        }

    }

}
