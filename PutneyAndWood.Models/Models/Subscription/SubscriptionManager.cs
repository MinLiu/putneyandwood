﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public enum SubscriptionPlanType { OneQuotations, FiveQuotations, OneEverything, FiveEverything }

    public static class SubscriptionManager 
    {

        public static SubscriptionCartItem Generate(SubscriptionCartItemViewModel viewModel, Guid companyID, SubscriptionPlan currentPlan = null)
        {
            decimal finalPrice = 0m;
            bool applyDiscount = false;

            int[] users = new int[] { viewModel.NumberCRM, viewModel.NumberProducts, viewModel.NumberQuotations, viewModel.NumberJobs, viewModel.NumberInvoices, viewModel.NumberDeliveryNotes, viewModel.NumberPurchaseOrders, viewModel.NumberStockControl };
            viewModel.NumberCRM = users.OrderByDescending(x => x).First();
            viewModel.NumberProducts = viewModel.NumberCRM;



            if (viewModel.NumberCRM > 0
                && viewModel.NumberQuotations <= 0
                && viewModel.NumberJobs <= 0
                && viewModel.NumberDeliveryNotes <= 0
                && viewModel.NumberInvoices <= 0
                && viewModel.NumberPurchaseOrders <= 0
                && viewModel.NumberStockControl <= 0)
            {
                finalPrice += (decimal)(viewModel.NumberCRM) * 10.8m;
            }

            if (viewModel.NumberQuotations > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }
            if (viewModel.NumberJobs > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }
            if (viewModel.NumberDeliveryNotes > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }
            if (viewModel.NumberInvoices > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }
            if (viewModel.NumberPurchaseOrders > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }
            if (viewModel.NumberStockControl > 0) { finalPrice += 18m + (decimal)(viewModel.NumberQuotations - 1) * 10.8m; }

            //Apply all modules discount
            if (viewModel.NumberQuotations > 0
                && viewModel.NumberJobs > 0
                && viewModel.NumberDeliveryNotes > 0
                && viewModel.NumberInvoices > 0
                && viewModel.NumberPurchaseOrders > 0
                && viewModel.NumberStockControl > 0)
            {
                applyDiscount = true;
                finalPrice -= 12m;
            }
            

            var cartItem = new SubscriptionCartItem
            {
                CompanyID = companyID,
                Name = string.Format("Quikflw Subscription - {0}{1}{2}{3}{4}{5}{6}{7}", 
                                                                            (viewModel.NumberCRM > 0) ? "CRM" : "",
                                                                            (viewModel.NumberProducts > 0) ? ", PDTS" : "",
                                                                            (viewModel.NumberQuotations > 0) ? ", Q" : "",
                                                                            (viewModel.NumberJobs > 0) ? ", JOBS" : "",
                                                                            (viewModel.NumberDeliveryNotes > 0) ? ", DN" : "",
                                                                            (viewModel.NumberInvoices > 0) ? ", INV" : "",
                                                                            (viewModel.NumberPurchaseOrders > 0) ? ", PO" : "",
                                                                            (viewModel.NumberStockControl > 0) ? ", SC" : ""
                        ),
                Description = string.Format("Monthly Quikflw subscription plan."),

                InitialAmount = Math.Round(finalPrice, 2),
                RegularAmount = Math.Round(finalPrice, 2),

                DiscountAmount = (applyDiscount) ? 12m : 0m,
                
                NumberCRM = viewModel.NumberCRM,
                NumberProducts = viewModel.NumberProducts,
                NumberQuotations = viewModel.NumberQuotations,
                NumberJobs = viewModel.NumberJobs,
                NumberDeliveryNotes = viewModel.NumberDeliveryNotes,
                NumberInvoices = viewModel.NumberInvoices,
                NumberPurchaseOrders = viewModel.NumberPurchaseOrders,
                NumberStockControl = viewModel.NumberStockControl
            };

            //Get calculate intial amount based on previous subscription
            if (currentPlan != null)
            {
                if(DateTime.Now < currentPlan.EndDate){

                    decimal days =  (decimal)(currentPlan.EndDate - currentPlan.StartDate).TotalDays;
                    decimal left = (decimal)(currentPlan.EndDate - DateTime.Now).TotalDays;

                    cartItem.InitialAmount = Math.Round( cartItem.RegularAmount - (currentPlan.RegularAmount * (1 - (left / days))), 2);

                    if (cartItem.InitialAmount < 0) cartItem.InitialAmount = 0;
                }
            }

            return cartItem;

        }


        public static SubscriptionCartItem CreatePlan(SubscriptionPlanType type, Guid companyID, SubscriptionPlan currentPlan = null)
        {
            decimal finalPrice = 0m;
            int numCRM = 0;
            int numPdts = 0;
            int numQuotes = 0;
            int numJobs = 0;
            int numInvoices = 0;
            int numDelNotes = 0;
            int numPOrders = 0;
            int numStockCtrl = 0;


            switch (type)
            {
                case SubscriptionPlanType.OneQuotations:
                    finalPrice = 18m;
                    numCRM = 1; numPdts = 1; numQuotes = 1; numJobs = 0; numInvoices = 0; numDelNotes = 0; numPOrders = 0; numStockCtrl = 0;
                    break;

                case SubscriptionPlanType.FiveQuotations:
                    finalPrice = 34.8m;
                    numCRM = 5; numPdts = 5; numQuotes = 5; numJobs = 0; numInvoices = 0; numDelNotes = 0; numPOrders = 0; numStockCtrl = 0;
                    break;

                case SubscriptionPlanType.OneEverything:
                    finalPrice = 70.8m;
                    numCRM = 1; numPdts = 1; numQuotes = 1; numJobs = 1; numInvoices = 1; numDelNotes = 1; numPOrders = 1; numStockCtrl = 1;
                    break;

                case SubscriptionPlanType.FiveEverything:
                    finalPrice = 118.8m;
                    numCRM = 5; numPdts = 5; numQuotes = 5; numJobs = 5; numInvoices = 5; numDelNotes = 5; numPOrders = 5; numStockCtrl = 5;
                    break;

                default:
                    finalPrice = 18m;
                    numCRM = 1; numPdts = 1; numQuotes = 1; numJobs = 0; numInvoices = 0; numDelNotes = 0; numPOrders = 0; numStockCtrl = 0;
                    break;
            }

            var cartItem = new SubscriptionCartItem
            {
                CompanyID = companyID,
                Name = string.Format("Quikflw Subscription - {0}{1}{2}{3}{4}{5}{6}{7}",
                                                                            (numCRM > 0) ? "CRM" : "",
                                                                            (numPdts > 0) ? ", PDTS" : "",
                                                                            (numQuotes > 0) ? ", Q" : "",
                                                                            (numJobs > 0) ? ", JOBS" : "",
                                                                            (numDelNotes > 0) ? ", DN" : "",
                                                                            (numInvoices > 0) ? ", INV" : "",
                                                                            (numPOrders > 0) ? ", PO" : "",
                                                                            (numStockCtrl > 0) ? ", SC" : ""
                        ),
                Description = string.Format("Monthly Quikflw subscription plan."),

                InitialAmount = Math.Round(finalPrice, 2),
                RegularAmount = Math.Round(finalPrice, 2),

                //DiscountAmount = (applyDiscount) ? 12m : 0m,

                NumberCRM = numCRM,
                NumberProducts = numPdts,
                NumberQuotations = numQuotes,
                NumberJobs = numJobs,
                NumberDeliveryNotes = numDelNotes,
                NumberInvoices = numInvoices,
                NumberPurchaseOrders = numPOrders,
                NumberStockControl = numStockCtrl,
                EnableTeamManagement = (type == SubscriptionPlanType.FiveEverything),
            };

            //Get calculate intial amount based on previous subscription
            if (currentPlan != null)
            {
                if (DateTime.Now < currentPlan.EndDate)
                {

                    decimal days = (decimal)(currentPlan.EndDate - currentPlan.StartDate).TotalDays;
                    decimal left = (decimal)(currentPlan.EndDate - DateTime.Now).TotalDays;

                    cartItem.InitialAmount = Math.Round(cartItem.RegularAmount - (currentPlan.RegularAmount * (1 - (left / days))), 2);

                    if (cartItem.InitialAmount < 0) cartItem.InitialAmount = 0;
                }
            }

            return cartItem;

        }

    }

}
