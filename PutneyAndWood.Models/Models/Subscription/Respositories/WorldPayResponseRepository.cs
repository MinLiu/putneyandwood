﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class WorldPayResponseRepository : EntityRespository<WorldPayResponse>
    {
        public WorldPayResponseRepository()
            : this(new SnapDbContext())
        {

        }

        public WorldPayResponseRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
