﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionRequestRepository : EntityRespository<SubscriptionRequest>
    {
        public SubscriptionRequestRepository()
            : this(new SnapDbContext())
        {

        }

        public SubscriptionRequestRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
