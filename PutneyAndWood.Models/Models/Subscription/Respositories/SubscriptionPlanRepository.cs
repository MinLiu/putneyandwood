﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionPlanRepository : EntityRespository<SubscriptionPlan>
    {
        public SubscriptionPlanRepository()
            : this(new SnapDbContext())
        {

        }

        public SubscriptionPlanRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
