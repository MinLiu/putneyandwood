﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class SubscriptionPlanPaymentRepository : EntityRespository<SubscriptionPlanPayment>
    {
        public SubscriptionPlanPaymentRepository()
            : this(new SnapDbContext())
        {

        }

        public SubscriptionPlanPaymentRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
