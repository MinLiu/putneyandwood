﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Models
{
    public class SubscriptionUserPriceBreakRepository : EntityRepository<SubscriptionUserPriceBreak>
    {
        public SubscriptionUserPriceBreakRepository()
            : this(new SnapDbContext())
        {

        }

        public SubscriptionUserPriceBreakRepository(SnapDbContext db)
            : base(db)
        {

        }


    }

}
