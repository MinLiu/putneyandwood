﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class WorldPayResponse : IEntity
    {
        public WorldPayResponse() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        public DateTime Timestamp { get; set; }
        public string InstID { get; set; }
        public string CartID { get; set; }
        public string Desc { get; set; }
        public decimal Cost { get; set; }
        public decimal Amount { get; set; }
        public string AmountString { get; set; }
        [MaxLength(3)]
        public string Currency { get; set; }
        public string AuthMode { get; set; }
        public string TestMode { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string Region { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string CountryString { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public string DelvName { get; set; }
        public string DelvAddress1 { get; set; }
        public string DelvAddress2 { get; set; }
        public string DelvAddress3 { get; set; }
        public string DelvTown { get; set; }
        public string DelvRegion { get; set; }
        public string DelvPostcode { get; set; }
        public string DelvCountry { get; set; }
        public string DelvCountryString { get; set; }

        public string CompName { get; set; }

        public string TransID { get; set; }
        public string TransStatus { get; set; }
        public string TransTime { get; set; }
        public decimal AuthAmount { get; set; }
        public decimal AuthCost { get; set; }
        public string AuthCurrency { get; set; }
        public string AuthAmountString { get; set; }
        public string RawAuthMessage { get; set; }
        public string RawAuthCode { get; set; }
        public string CallbackPW { get; set; }
        public string CardType { get; set; }
        public string CountryMatch { get; set; }
        public string AVS { get; set; }
        public string WafMerchMessage { get; set; }
        public string Authentication { get; set; }
        public string IpAddress { get; set; }
        public string Charenc { get; set; }
        public string _spCharEnc { get; set; }

        public string FuturePayID { get; set; }
        public string FuturePayStatusChange { get; set; }

        public string ToShortLabel() { return ""; }
        public string ToLongLabel() { return ""; }
    }

}
