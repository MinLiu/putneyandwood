﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class OpportunityLog : IEntity, ICompanyEntity
    {
        public OpportunityLog() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public string ClientName { get; set; }
        public Guid OpportunityID { get; set; }
        public Guid OpportunityStatusID { get; set; }
        public DateTime Timestamp { get; set; }

        public virtual Company Company { get; set; }
        public virtual OpportunityStatus OpportunityStatus { get; set; }
        public virtual Opportunity Opportunity { get; set; }

        public string ToShortLabel() { return string.Format("{0} - {1}", Timestamp.ToString("HH:mm dd/MM/yy"), ClientName); }
        public string ToLongLabel() { return string.Format("{0} - {1}", Timestamp.ToString("HH:mm dd/MM/yy"), ClientName); }
    }

    public class OpportunityLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ClientName { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class OpportunityLogMapper : ModelMapper<OpportunityLog, OpportunityLogViewModel>
    {
        public override void MapToModel(OpportunityLogViewModel viewModel, OpportunityLog model)
        {
            throw new NotImplementedException();
        }

        public override void MapToViewModel(OpportunityLog model, OpportunityLogViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ClientName = model.ClientName;
            viewModel.Timestamp = model.Timestamp;
        }
    }
}
