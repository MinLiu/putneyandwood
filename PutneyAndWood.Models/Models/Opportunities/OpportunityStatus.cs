﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class OpportunityStatus : IEntity, ICompanyEntity
    {
        public OpportunityStatus() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid OpportunityTypeID { get; set; }
        public int SortPos { get; set; }
        public string Name { get; set; }

        public virtual Company Company { get; set; }
        public virtual OpportunityType OpportunityType { get; set; }
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }

    public class OpportunityStatusViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public int SortPos { get; set; }
        [Required]
        public string Name { get; set; }
        public string OpportunityTypeID { get; set; }
    }

    public class OpportunityStatusMapper : ModelMapper<OpportunityStatus, OpportunityStatusViewModel>
    {
        public override void MapToModel(OpportunityStatusViewModel viewModel, OpportunityStatus model)
        {
            model.Name = viewModel.Name;
            model.OpportunityTypeID = Guid.Parse(viewModel.OpportunityTypeID);
            model.SortPos = viewModel.SortPos;
        }
        public override void MapToViewModel(OpportunityStatus model, OpportunityStatusViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SortPos = model.SortPos;
            viewModel.Name = model.Name;
            viewModel.OpportunityTypeID = model.OpportunityTypeID.ToString();
        }
    }
}
