﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class OpportunityType : IEntity, ICompanyEntity
    {
        public OpportunityType() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public string Name { get; set; }
        public string LabelValue1 { get; set; }
        public string LabelValue2 { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<OpportunityStatus> Statuses { get; set; }

        public string ToShortLabel() { return Name; }
        public string ToLongLabel() { return Name; }
    }

    public class OpportunityTypeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LabelValue1 { get; set; }
        [Required]
        public string LabelValue2 { get; set; }
    }

    public class OpportunityTypeMapper : ModelMapper<OpportunityType, OpportunityTypeViewModel>
    {
        public override void MapToModel(OpportunityTypeViewModel viewModel, OpportunityType model)
        {
            model.Name = viewModel.Name;
            model.LabelValue1 = viewModel.LabelValue1;
            model.LabelValue2 = viewModel.LabelValue2;
        }
        public override void MapToViewModel(OpportunityType model, OpportunityTypeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.LabelValue1 = model.LabelValue1;
            viewModel.LabelValue2 = model.LabelValue2;
        }
    }

}
