﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class Opportunity : IEntity, ICompanyEntity
    {
        public Opportunity() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? ClientID { get; set; }
        public Guid? ClientContactID { get; set; }
        public Guid OpportunityTypeID { get; set; }
        public Guid StatusID { get; set; }
        public int SortPos { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public string AssignedUserID { get; set; }
        public string CreatorID { get; set; }
        public int BarValue1 { get; set; }
        public int BarValue2 { get; set; }
        public decimal Value { get; set; }
        public bool Archived { get; set; }


        public virtual Company Company { get; set; }
        public virtual Client Client { get; set; }
        public virtual ClientContact ClientContact { get; set; }
        public virtual OpportunityType OpportunityType { get; set; }
        public virtual OpportunityStatus Status { get; set; }
        public virtual User AssignedUser { get; set; }
        public virtual User Creator { get; set; }
        public virtual ICollection<ClientEvent> Events { get; set; }
        public virtual ICollection<OpportunityLog> Logs { get; set; }

        public string ToShortLabel() { return Description; }
        public string ToLongLabel() { return Description; }
    }

    public class OpportunityViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        [Required]
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string ClientContactID { get; set; }
        [Required]
        public string OpportunityTypeID { get; set; }
        public string AssignedUserID { get; set; }
        [Required]
        public string StatusID { get; set; }
        public int SortPos { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Importance { get; set; }
        public int Probability { get; set; }
        public decimal Value { get; set; }
        public bool Archived { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }
        public List<OpportunityStatusViewModel> OpportunityStatuses { get; set; }
        public List<OpportunityStatusLogViewModel> OpportunityStatusLogs { get; set; }
        public string LabelValue1 { get; set; }
        public string LabelValue2 { get; set; }
    }

    public class OppotrunityListViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int Probability { get; set; }
        public int Importance { get; set; }
        public decimal Value { get; set; }
        public string ClientName { get; set; }
        public int Status { get; set; }
        public string EventDescription { get; set; }
        public string LabelValue1 { get; set; }
        public string LabelValue2 { get; set; }
    }

    public class OpportunityGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public int StatusSortPos { get; set; }
    }

    public class OpportunityMapper : ModelMapper<Opportunity, OpportunityViewModel>
    {
        public override void MapToModel(OpportunityViewModel viewModel, Opportunity model)
        {
            model.ClientID = !string.IsNullOrWhiteSpace(viewModel.ClientID) ? Guid.Parse(viewModel.ClientID) : (Guid?)null;
            model.ClientContactID = !string.IsNullOrWhiteSpace(viewModel.ClientContactID) ? Guid.Parse(viewModel.ClientContactID) : (Guid?)null;
            model.AssignedUserID = viewModel.AssignedUserID;
            model.StatusID = Guid.Parse(viewModel.StatusID);
            model.OpportunityTypeID = Guid.Parse(viewModel.OpportunityTypeID);
            model.SortPos = viewModel.SortPos;
            model.Name = viewModel.Name;
            model.Description = viewModel.Description;
            model.LastUpdated = DateTime.Now;
            model.BarValue1 = viewModel.Importance;
            model.BarValue2 = viewModel.Probability;
            model.Value = viewModel.Value;
            model.Archived = viewModel.Archived;
        }
        public override void MapToViewModel(Opportunity model, OpportunityViewModel viewModel)
        {
            viewModel.ClientID = model.ClientID.HasValue ? model.ClientID.ToString() : "";
            viewModel.ClientName = model.Client != null ? model.Client.Name : "";
            viewModel.ClientContactID = model.ClientContactID.HasValue ? model.ClientContactID.ToString() : "";
            viewModel.AssignedUserID = model.AssignedUserID;
            viewModel.ID = model.ID.ToString();
            viewModel.StatusID = model.StatusID.ToString();
            viewModel.OpportunityTypeID = model.OpportunityTypeID.ToString();
            viewModel.SortPos = model.SortPos;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.LastUpdated = model.LastUpdated;
            viewModel.Created = model.Created;
            viewModel.Importance = model.BarValue1;
            viewModel.Probability = model.BarValue2;
            viewModel.Value = model.Value;
            viewModel.Archived = model.Archived;
            viewModel.OpportunityStatuses = model.OpportunityType.Statuses.OrderBy(x => x.SortPos)
                                                                          .ToList()
                                                                          .Select(x => new OpportunityStatusMapper().MapToViewModel(x))
                                                                          .ToList();
            viewModel.OpportunityStatusLogs = model.OpportunityType.Statuses.OrderBy(x => x.SortPos)
                                                                          .ToList()
                                                                          .Select(x => new OpportunityStatusLogViewModel()
                                                                          {
                                                                              Status = new OpportunityStatusMapper().MapToViewModel(x),
                                                                              Log = x.SortPos <= model.Status.SortPos ? model.Logs.Where(l => l.OpportunityStatusID == x.ID)
                                                                                                                      .OrderByDescending(l => l.Timestamp)
                                                                                                                      .FirstOrDefault() : null
                                                                          })
                                                                          .ToList();
            viewModel.LabelValue1 = model.OpportunityType.LabelValue1;
            viewModel.LabelValue2 = model.OpportunityType.LabelValue2;
        }
    }

    public class OpportunityGridMapper : ModelMapper<Opportunity, OpportunityGridViewModel>
    {
        public override void MapToModel(OpportunityGridViewModel viewModel, Opportunity model)
        {
            throw new NotImplementedException();
        }
        public override void MapToViewModel(Opportunity model, OpportunityGridViewModel viewModel)
        {
            viewModel.Created = model.Created;
            viewModel.Description = model.Description;
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Value = model.Value;
            viewModel.Type = model.OpportunityType.Name;
            viewModel.Status = model.Status.Name;
            viewModel.StatusSortPos = model.Status.SortPos;
        }
    }

    public class OpportunityStatusLogViewModel
    {
        public OpportunityStatusViewModel Status { get; set; }
        public OpportunityLog Log { get; set; }
    }

}
