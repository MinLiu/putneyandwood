﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class OpportunityTypeRepository : EntityRespository<OpportunityType>
    {
        public OpportunityTypeRepository()
            : this(new SnapDbContext())
        {

        }

        public OpportunityTypeRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(OpportunityType entity)
        {
            _db.Set<OpportunityLog>().RemoveRange(_db.Set<OpportunityLog>().Where(x => x.OpportunityStatus.OpportunityType.ID == entity.ID));
            _db.Set<OpportunityStatus>().RemoveRange(_db.Set<OpportunityStatus>().Where(x => x.OpportunityTypeID == entity.ID));

            base.Delete(entity);
        }

    }

}
