﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class OpportunityRepository : EntityRespository<Opportunity>
    {
        public OpportunityRepository()
            : this(new SnapDbContext())
        {

        }

        public OpportunityRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
