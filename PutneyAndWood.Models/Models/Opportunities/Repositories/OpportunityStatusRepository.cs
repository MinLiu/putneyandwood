﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class OpportunityStatusRepository : EntityRespository<OpportunityStatus>
    {
        public OpportunityStatusRepository()
            : this(new SnapDbContext())
        {

        }

        public OpportunityStatusRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(OpportunityStatus entity)
        {
            _db.Set<OpportunityLog>().RemoveRange(_db.Set<OpportunityLog>().Where(x => x.OpportunityStatusID == entity.ID));
            base.Delete(entity);
        }

    }

}
