﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    public class OpportunityLogRepository : EntityRespository<OpportunityLog>
    {
        public OpportunityLogRepository()
            : this(new SnapDbContext())
        {

        }

        public OpportunityLogRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
