﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace SnapSuite.Models
{

    public class CustomField
    {
        //public CustomField() { ID = Guid.NewGuid(); }

        //[Key]
        //public Guid ID  { get; set; }
        public string Name  { get; set; }
        public string Label  { get; set; }
        public string Value  { get; set; }
        public int SortPos  { get; set; }
        public bool Show  { get; set; }

        /*
        public Guid? JobCostItemID  { get; set; }
        public Guid? JobID  { get; set; }
        public Guid? JobItemID  { get; set; }
        public Guid? DeliveryNoteItemID  { get; set; }
        public Guid? PurchaseOrderID  { get; set; }
        public Guid? PurchaseOrderItemID  { get; set; }
        public Guid? QuotationCostItemID  { get; set; }
        public Guid? QuotationID  { get; set; }
        public Guid? QuotationItemID  { get; set; }
        public Guid? ProductID  { get; set; }
        public Guid? InvoiceID  { get; set; }
        public Guid? InvoiceItemID  { get; set; }
        public Guid? DeliveryNoteID  { get; set; }
        */
    }
    

    public static class CustomFieldExtension
    {
       
        public static List<CustomField> AddRemoveRelevent(this List<CustomField> models, List<CustomField> addRemoveModels)
        {
            if (models == null && addRemoveModels == null)
            {
                models = new List<CustomField>();
                return models;
            }
            else if (models == null || models.Count <= 0)
            {
                models = addRemoveModels;
                return models;
            }
            else if (addRemoveModels == null)
            {
                return models;
            }

            var newList = models.Where(i => addRemoveModels.Select(x => x.Name).Contains(i.Name)).ToList();
            newList.AddRange(addRemoveModels.Where(i => !models.Select(x => x.Name).Contains(i.Name)).ToArray());


            foreach(var vModel in newList)
            {
                var ii = addRemoveModels.Where(i => i.Name == vModel.Name).DefaultIfEmpty(new CustomField { }).FirstOrDefault();
                vModel.SortPos = ii.SortPos;
                vModel.Show = ii.Show;
                vModel.Label = ii.Label;
            }

            return newList.OrderBy(i => i.SortPos).ToList();
        }


        public static List<CustomField> CustomFieldsFromString(this string JsonCustomFieldValues)
        {
            try { return Newtonsoft.Json.JsonConvert.DeserializeObject<List<CustomField>>(JsonCustomFieldValues ?? "[ ]", new Newtonsoft.Json.JsonSerializerSettings { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore }); }
            catch { return new List<CustomField>(); }
        }
        

        public static string StringFromCustomFields(this List<CustomField> customFields)
        {
            try { return Newtonsoft.Json.JsonConvert.SerializeObject(customFields); }
            catch { return "[ ]"; }
        }

        /*
        public static List<CustomField> CreateUpdateDelete(this List<CustomField> models, List<CustomField> newModels, DbContext db)
        {
            if (newModels == null) newModels = new List<CustomField>();
            if (models == null) return newModels;

            foreach (var model in models)
            {
                var ii = newModels.Where(i => i.ID == model.ID).DefaultIfEmpty(new CustomField { }).FirstOrDefault();
                model.Name = ii.Name;
                model.Label = ii.Label;
                model.Value = ii.Value;
                model.Show = ii.Show;
                model.SortPos = ii.SortPos;
                model.Show = ii.Show;
            }

            models.AddRange(newModels.Where(i => !models.Select(x => x.ID).Contains(i.ID)).ToArray());

            //Delete not needed
            foreach (var remove in models.Where(i => !newModels.Select(x => x.ID).Contains(i.ID)).ToArray())
            {
                db.Entry(remove).State = EntityState.Deleted;
            }

            //Delete duplicates
            foreach (var remove in models.GroupBy(i => i.Name).SelectMany(i => i.Skip(1)).ToArray())
            {
                db.Entry(remove).State = EntityState.Deleted;
            }

            return models;
        }


        public static CustomField Copy(this CustomField model)
        {
            return new CustomField
            {
                ID = Guid.NewGuid(),
                Name = model.Name,
                Label = model.Label,
                Value = model.Value,
                SortPos = model.SortPos,
                Show = model.Show
            };
        }

        public static List<CustomField> Copy(this List<CustomField> models)
        {
            if (models == null) return new List<CustomField>();

            return models.Select(i =>
                new CustomField
                {
                    ID = Guid.NewGuid(),
                    Name = i.Name,
                    Label = i.Label,
                    Value = i.Value,
                    SortPos = i.SortPos,
                    Show = i.Show,
                    //JobCostItemID  = null,
                    //JobID  = null,
                    //JobItemID  = null,
                    //DeliveryNoteItemID  = null,
                    //PurchaseOrderID  = null,
                    //PurchaseOrderItemID  = null,
                    //QuotationCostItemID  = null,
                    //QuotationID  = null,
                    //QuotationItemID  = null,
                    //ProductID  = null,
                    //InvoiceID  = null,
                    //InvoiceItemID  = null,
                    //DeliveryNoteID  = null,
            }
            ).ToList();
        }
        */

    }


}