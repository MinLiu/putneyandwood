﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class QuotationItemImportService : ImportEntityService<QuotationItem>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "Product Code", "Description", "Category", "Account Code", "Unit", "Setup Cost", "VAT", "List Cost", "Agreed Cost", "List Price", "Sales Price", "Qty" }; } }

        public Guid QuotationID { get; set; }

        public QuotationItemImportService(Guid CompanyID, Guid QuotationID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {
            this.QuotationID = QuotationID;
        }

        protected override ImportResult CompleteImport(DataTable dataTable)
        {
            int added = 0;

            List<QuotationItem> quotationItemsToAdd = new List<QuotationItem>();

            var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, QuotationID, null, null, _db);
            using (var context = new SnapDbContext())
            {   
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                var existingProducts = context.Set<Product>().Where(i => i.CompanyID == CompanyID && i.ProductCode != null).ToList();
                var existingSections = context.Set<QuotationSection>().Where(x => x.QuotationID == QuotationID).ToList();

                foreach (DataRow row in dataTable.Rows)
                {
                    string sectionName;
                    try { sectionName = row["Section"].ToString() != "" ? row["Section"].ToString() : null; }
                    catch { sectionName = null; }

                    var productCode = row["Product Code"].ToString();
                    var description = row["Description"].ToString();
                    var unit = row["Unit"].ToString();
                    var accountCode = row["Account Code"].ToString();
                    var category = row["Category"].ToString();

                    decimal VAT;
                    try { VAT = decimal.Parse(row["VAT"].ToString()); }
                    catch { VAT = 0; }

                    decimal listPrice;
                    try { listPrice = decimal.Parse(row["List Price"].ToString()); }
                    catch { listPrice = 0; }

                    decimal salesPrice;
                    try { salesPrice = decimal.Parse(row["Sales Price"].ToString()); }
                    catch { salesPrice = 0; }

                    decimal agreedCost;
                    try { agreedCost = decimal.Parse(row["Agreed Cost"].ToString()); }
                    catch { agreedCost = 0; }

                    decimal listCost;
                    try { listCost = decimal.Parse(row["List Cost"].ToString()); }
                    catch { listCost = 0; }

                    decimal quantity;
                    try { quantity = decimal.Parse(row["Qty"].ToString()); }
                    catch { quantity = 0; }



                    //if (string.IsNullOrWhiteSpace(row["Product Code"].ToString())) continue;

                    var section = existingSections.Where(x => x.Name == sectionName).FirstOrDefault();
                    if (section == null)
                    {
                        var sectionService = new QuotationSectionService<EmptyViewModel>(CompanyID, QuotationID, null, null, _db);
                        sectionService.ParentID = QuotationID;
                        var model = new QuotationSection
                        {
                            QuotationID = QuotationID,
                            IncludeInTotal = true,
                            Name = sectionName
                        };
                        section = sectionService.Create(model);
                        existingSections.Add(section);
                    }

                    var product = existingProducts.Where(x => productCode != null && productCode != "" && x.ProductCode == productCode).FirstOrDefault();
                    if (product != null)
                    {
                        var quotationItem = itemService.AddProduct(product.ID, QuotationID, section.ID, quantity);
                        quotationItem.Description = description;
                        quotationItem.AccountCode = accountCode;
                        quotationItem.Unit = unit;
                        quotationItem.VAT = VAT;
                        quotationItem.Price = salesPrice;
                        quotationItem.Cost = agreedCost;
                        quotationItem.ListCost = listCost;
                        quotationItem.Category = category;

                        itemService.Update(quotationItem);
                    }
                    else
                    {
                        var quotationItem = new QuotationItem
                        {
                            QuotationID = QuotationID,
                            SectionID = section.ID,
                            ProductCode = productCode,
                            Description = description,
                            Category = category,
                            Unit = unit,
                            VAT = VAT,
                            Quantity = quantity,
                            ParentItemID = null,
                            UseKitPrice = false,
                            IsComment = false,
                            AccountCode = accountCode,
                            Price = salesPrice,
                            ListPrice = listPrice,
                            Cost = agreedCost,
                            ListCost = listCost,
                        };
                        itemService.Create(quotationItem);
                        itemService.UpdateValues(quotationItem, true, true);
                    }
                    added++;
                }
            }


            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = 0 };
        }

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Items have been successfully imported.", result.NumberUpdated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.QUOTATIONS, meessage, user, null);
        }

    }

}

