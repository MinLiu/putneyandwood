﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ProductImageImportService : GenericService<ProductImage>
    {
        // <summary> The Company ID value.</summary>
        public virtual Guid CompanyID { get; set; }
        /// <summary>User ID who is using the service.</summary>
        public virtual string CurrentUserID { get; set; }

        public virtual string[] RequiredImportColumns { get { return new string[] { }; } }

        public ProductImageImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(db)
        {
            this.CompanyID = CompanyID;
            this.CurrentUserID = CurrentUserID;
        }
        

        public virtual ImportResult Import(IEnumerable<HttpPostedFileBase> images, DateTime importDate, bool ignoreCase = true, bool deleteExisting = false)
        {
            const string relativePath = "/Images/";

            int added = 0;

            if (images != null)
            {
                var imgsToDel = new List<Guid>();
                var fileService = new FileService(new SnapDbContext());
                foreach (var file in images)
                {
                    if (file == null) continue; //No Image uploaded then skip
                    if (file.ContentLength > 2 * 1024 * 1024) continue; //If larger then 2Mbs then skip
                    if (!file.ContentType.Contains("image")) continue; //If not an image the skip

                    var filename = Path.GetFileNameWithoutExtension(file.FileName) + ".";
                    var extension = Path.GetExtension(file.FileName);

                    //Get the products where the product code matchs the filename without the extension
                    var service = new ProductService<EmptyViewModel>(CompanyID, null, null, fileService.DbContext);
                    var imgService = new ProductImageService<EmptyViewModel>(CompanyID, null, null, null, fileService.DbContext);

                    var name = System.Text.RegularExpressions.Regex.Replace(filename, "(\\([0-999]\\)\\.| \\([0-999]\\)\\.|\\.)", "");
                    var products = ((ignoreCase) ? service.Read().Where(p => p.ProductCode.ToLower() == name.ToLower() && p.CompanyID == CompanyID) : service.Read().Where(p => p.ProductCode == name && p.CompanyID == CompanyID)).AsNoTracking().Select(i => i.ID).ToList();

                    if (products.Count <= 0) continue; //If no products match the description then skip
                    if (imgService.Read().Where(i => products.Contains(i.OwnerID) && i.File.DateCreated >= importDate).Count() >= 10) continue;

                    if (deleteExisting)
                    {
                        try
                        {
                            var delService = new GenericService<ProductImage>(new SnapDbContext());
                            var imgs = delService.Read().Where(i => products.Contains(i.OwnerID) && i.File.DateCreated < importDate).Include(i => i.Owner).Include(i => i.File).ToList();
                            delService.Destroy(imgs);
                        }
                        catch { }
                    }

                    //If a match is found then save and update every product image.
                    var pdtImg = fileService.SaveFile(CompanyID, file, relativePath);
                    pdtImg.DateCreated = importDate;
                    imgService.Create(products.Select((i, index) => new ProductImage { OwnerID = i, FileID = pdtImg.ID, SortPos = index }));
                    added++;
                }

                var result = new ImportResult { Success = true, NumberCreated = added, NumberUpdated = 0 };
                return result;
            }
            else
            {
                return new ImportResult { Success = false, Exception = new Exception("No images have been uploaded.") };
            }

            
        }

        

        protected virtual void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Product Images have been successfully imported.\n\nProduct Images Added: {1}", result.NumberUpdated + result.NumberCreated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.PRODUCTS, meessage, user, null);
        }

    }

}

