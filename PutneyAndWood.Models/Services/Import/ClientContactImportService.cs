﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ClientContactImportService : ImportEntityService<ClientContact>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "Client", "Title", "FirstName", "LastName", "Position", "Telephone", "Mobile", "Email" }; } }

        public ClientContactImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable data)
        {
            int updated = 0;
            int added = 0;

            List<ClientContact> listToAdd = new List<ClientContact>();

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing contacts
                var existing = context.ClientContacts.Where(c => c.Client.CompanyID == CompanyID && c.Client.Deleted == false).ToList();
                foreach (DataRow row in data.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["Client"].ToString())) continue;

                    var exist = existing.Where(p => p.Title == row["Title"].ToString()
                                                && p.FirstName == row["FirstName"].ToString()
                                                && p.LastName == row["LastName"].ToString())
                                        .ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var p in exist)
                        {
                            p.Position = row["Position"].ToString();
                            p.Telephone = row["Telephone"].ToString();
                            p.Mobile = row["Mobile"].ToString();
                            p.Email = row["Email"].ToString();
                            p.Deleted = false;
                            context.ClientContacts.Attach(p);
                            context.Entry(p).State = EntityState.Modified;
                        }
                        updated++;
                    }
                    else
                    {
                        try
                        {
                            var name = row["Client"].ToString();
                            var newContact = new ClientContact
                            {
                                ClientID = context.Clients.Where(o => o.Name == name && o.CompanyID == CompanyID && o.Deleted == false).Select(o => o.ID).First(),
                                Title = row["Title"].ToString(),
                                FirstName = row["FirstName"].ToString(),
                                LastName = row["LastName"].ToString(),
                                Position = row["Position"].ToString(),
                                Telephone = row["Telephone"].ToString(),
                                Mobile = row["Mobile"].ToString(),
                                Email = row["Email"].ToString(),
                            };

                            //Add contacts later in different context to prevent DbUpdateConcurrencyException
                            listToAdd.Add(newContact);
                            added++;
                        }
                        catch { }
                    }
                }
                context.SaveChanges();

            }

            //Add contacts in a different context to prevent DbUpdateConcurrencyException
            using (var context = new SnapDbContext())
            {
                context.ClientContacts.AddRange(listToAdd);
                context.SaveChanges();
            }


            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }


        

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Client Contacts have been successfully imported.\n\nClient Contacts Updated: {1}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.CLIENTS, meessage, user, null);
        }

    }

}

