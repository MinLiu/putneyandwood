﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ProductImportService : ImportEntityService<Product>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "ProductCode", "Description", "DetailedDescription", "Category", "Unit", "VendorCode", "Manufacturer", "AccountCode(Sales)", "AccountCode(Purchase)", "VATRate", "Currency", "Price", "Cost", "IsKit", "IsBoughtIn", "UsePriceBreaks" }; } }

        public ProductImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable dataTable)
        {
            int updated = 0;
            int added = 0;

            List<Product> productsToAdd = new List<Product>();


            using (var context = new SnapDbContext())
            {   
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                var existingProducts = context.Set<Product>().Where(i => i.CompanyID == CompanyID && i.ProductCode != null).ToList();
                var currenices = context.Set<Currency>().ToList();
                var customFields = context.Set<CustomProductField>().Where(c => c.CompanyID == CompanyID).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();

                foreach (DataRow row in dataTable.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["ProductCode"].ToString())) continue;

                    var code = row["ProductCode"].ToString().ToLower();
                    var exist = existingProducts.Where(p => p.ProductCode.ToLower() == code).ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var p in exist)
                        {
                            context.Entry(p).State = EntityState.Detached;

                            p.Description = row["Description"].ToString();
                            p.DetailedDescription = row["DetailedDescription"].ToString();
                            p.Category = row["Category"].ToString();
                            p.Unit = row["Unit"].ToString();
                            p.VendorCode = row["VendorCode"].ToString();
                            p.Manufacturer = row["Manufacturer"].ToString();
                            p.AccountCode = row["AccountCode(Sales)"].ToString();
                            p.PurchaseAccountCode = row["AccountCode(Purchase)"].ToString();

                            try { p.VAT = decimal.Parse(row["VATRate"].ToString()); }
                            catch { p.VAT = 0; }

                            try { p.Price = decimal.Parse(row["Price"].ToString()); }
                            catch { p.Price = 0; }

                            try { p.Cost = decimal.Parse(row["Cost"].ToString()); }
                            catch { p.Cost = 0; }

                            try { p.CurrencyID = currenices.Where(c => c.Code == row["Currency"].ToString() || c.Symbol == row["Currency"].ToString()).Select(c => c.ID).First(); }
                            catch { p.CurrencyID = 1; }

                            try { p.IsKit = (row["IsKit"].ToString().ToLower().Contains("t") || row["IsKit"].ToString().ToLower().Contains("y")); }
                            catch { p.IsKit = false; }

                            try { p.IsBought = (row["IsBoughtIn"].ToString().ToLower().Contains("t") || row["IsBoughtIn"].ToString().ToLower().Contains("y")); }
                            catch { p.IsBought = false; }

                            try { p.UsePriceBreaks = (row["UsePriceBreaks"].ToString().ToLower().Contains("t") || row["UsePriceBreaks"].ToString().ToLower().Contains("y")); }
                            catch { p.UsePriceBreaks = false; }

                            p.Deleted = false;
                            context.Set<Product>().Attach(p);
                            context.Entry(p).State = EntityState.Modified;

                            var cFields = p.CustomFieldValues.AddRemoveRelevent(customFields);
                            foreach (var field in cFields)
                            {
                                try { field.Value = row[field.Name].ToString(); }
                                catch { field.Value = ""; }
                            }
                            p.CustomFieldValues = cFields;
                            updated++;
                        }
                    }
                    else if (productsToAdd.Where(s => s.ProductCode == row["ProductCode"].ToString()).Count() <= 0)
                    {

                        var newPdt = new Product();
                        newPdt.CompanyID = CompanyID;
                        newPdt.ProductCode = row["ProductCode"].ToString();
                        newPdt.Description = row["Description"].ToString();
                        newPdt.DetailedDescription = row["DetailedDescription"].ToString();
                        newPdt.Category = row["Category"].ToString();
                        newPdt.Unit = row["Unit"].ToString();
                        newPdt.VendorCode = row["VendorCode"].ToString();
                        newPdt.Manufacturer = row["Manufacturer"].ToString();
                        newPdt.AccountCode = row["AccountCode(Sales)"].ToString();
                        newPdt.PurchaseAccountCode = row["AccountCode(Purchase)"].ToString();

                        try { newPdt.VAT = decimal.Parse(row["VATRate"].ToString().Replace("%", "")); }
                        catch { newPdt.VAT = 0; }

                        try { newPdt.Price = decimal.Parse(row["Price"].ToString()); }
                        catch { newPdt.Price = 0; }

                        try { newPdt.Cost = decimal.Parse(row["Cost"].ToString()); }
                        catch { newPdt.Cost = 0; }

                        try { newPdt.CurrencyID = currenices.Where(c => c.Code == row["Currency"].ToString() || c.Symbol == row["Currency"].ToString()).Select(c => c.ID).First(); }
                        catch { newPdt.CurrencyID = 1; }

                        try { newPdt.IsKit = (row["IsKit"].ToString().ToLower().Contains("t") || row["IsKit"].ToString().ToLower().Contains("y")); }
                        catch { newPdt.IsKit = false; }

                        try { newPdt.IsBought = (row["IsBought"].ToString().ToLower().Contains("t") || row["IsBought"].ToString().ToLower().Contains("y")); }
                        catch { newPdt.IsBought = false; }

                        try { newPdt.UsePriceBreaks = (row["UsePriceBreaks"].ToString().ToLower().Contains("t") || row["UsePriceBreaks"].ToString().ToLower().Contains("y")); }
                        catch { newPdt.UsePriceBreaks = false; }

                        newPdt.CustomFieldValues = customFields;

                        var cFields = newPdt.CustomFieldValues;
                        foreach (var field in cFields)
                        {
                            try { field.Value = row[field.Name].ToString(); }
                            catch { field.Value = ""; }
                        }
                        newPdt.CustomFieldValues = cFields;

                        //Add products later in different context to prevent DbUpdateConcurrencyException
                        productsToAdd.Add(newPdt);
                        added++;
                    }

                }
                context.SaveChanges();
            }

            using (var context = new SnapDbContext())
            {
                //Add products in a different context to prevent DbUpdateConcurrencyException
                context.Set<Product>().AddRange(productsToAdd);
                context.SaveChanges();
            }

            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };

            ////Log Activity
            //ActivityLogsController.CreateLog(ModuleTypeValues.PRODUCTS, string.Format("{0} Products successfully imported.", updated + added), CurrentUser);

            //ViewBag.SuccessMessage = string.Format("{0} Products have been successfully imported.\n\nProducts Updated: {1}\nProducts Added: {2}", updated + added, updated, added);
            //return View("UploadProducts");
        }

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Products have been successfully imported.\n\nProducts Updated: {1}\nProducts Added: {2}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.PRODUCTS, meessage, user, null);
        }

    }

}

