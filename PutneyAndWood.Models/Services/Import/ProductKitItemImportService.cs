﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ProductKitItemImportService : ImportEntityService<ProductKitItem>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "KitProductCode", "ItemProductCode", "Quantity" }; } }

        public ProductKitItemImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable data)
        {
            int updated = 0;
            int added = 0;

            using (var context = new SnapDbContext())
            {
                //First remove prices for products being imported        
                string code = string.Empty;
                foreach (DataRow row in data.Rows)
                {
                    if (row["KitProductCode"].ToString() == String.Empty)
                        continue;


                    code = row["KitProductCode"].ToString().ToLower();
                    context.ProductKitItems.RemoveRange(context.ProductKitItems.Where(p => p.Kit.ProductCode.ToLower() == code && p.Kit.Deleted == false && p.Kit.CompanyID == CompanyID));
                }
                context.SaveChanges();
            }
            

            List<Guid> productIDsToUpdate = new List<Guid>();
            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                var existingProducts = context.Products.Where(c => c.CompanyID == CompanyID && c.Deleted == false);
                foreach (DataRow row in data.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["KitProductCode"].ToString())) continue;
                    var code = row["KitProductCode"].ToString().ToLower();
                    var exist = existingProducts.Where(p => p.ProductCode.ToLower() == code).ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var p in exist)
                        {
                            try
                            {
                                var itemCode = row["ItemProductCode"].ToString().ToLower();
                                var newItem = new ProductKitItem
                                {
                                    KitID = p.ID,
                                    ProductID = existingProducts.Where(c => c.ProductCode == itemCode).Take(1).Select(c => c.ID).First(),
                                    Quantity = decimal.Parse(row["Quantity"].ToString())
                                };

                                try { newItem.HideOnPdf = (row["Hide"].ToString().ToLower().Contains("t") || row["Hide"].ToString().ToLower().Contains("y")); }
                                catch { newItem.HideOnPdf = false; }

                                context.ProductKitItems.Add(newItem);
                                productIDsToUpdate.Add(p.ID);
                                added++;
                            }
                            catch { }
                        }
                    }
                }
                context.SaveChanges();

            }

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                var products = context.Products.Where(c => c.CompanyID == CompanyID && productIDsToUpdate.Contains(c.ID)).ToList();
                foreach (var p in products)
                {
                    p.IsKit = true;
                    context.Products.Attach(p);
                    context.Entry(p).Property(x => x.IsKit).IsModified = true;
                }

                context.SaveChanges();
            }


            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }


        

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Product Kit Items have been successfully imported.\n\nProduct Kit Items Added: {1}", result.NumberUpdated + result.NumberCreated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.PRODUCTS, meessage, user, null);
        }

    }

}

