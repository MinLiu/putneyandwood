﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ProductPriceImportService : ImportEntityService<ProductPrice>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "ProductCode", "Currency", "Price", "Breakpoint" }; } }

        public ProductPriceImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable data)
        {
            int updated = 0;
            int added = 0;

            using (var context = new SnapDbContext())
            {
                //First remove prices for products being imported        
                string code = string.Empty;
                foreach (DataRow row in data.Rows)
                {
                    if (row["ProductCode"].ToString() == String.Empty)
                        continue;


                    code = row["ProductCode"].ToString().ToLower();
                    context.ProductPrices.RemoveRange(context.ProductPrices.Where(p => p.Product.CompanyID == CompanyID && p.Product.ProductCode.ToLower() == code && p.Product.Deleted == false));
                }
                context.SaveChanges();
            }

            List<Guid> productIDsToUpdate = new List<Guid>();
            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                var existingProducts = context.Products.Where(c => c.CompanyID == CompanyID && c.Deleted == false);
                var currencies = context.Currencies.ToList();

                foreach (DataRow row in data.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["ProductCode"].ToString())) continue;

                    var code = row["ProductCode"].ToString().ToLower();
                    var exist = existingProducts.Where(p => p.ProductCode.ToLower() == code).ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var p in exist)
                        {
                            productIDsToUpdate.Add(p.ID);

                            var newPrice = new ProductPrice { ProductID = p.ID };

                            try { newPrice.Price = decimal.Parse(row["Price"].ToString()); }
                            catch { newPrice.Price = 0; }

                            try { newPrice.BreakPoint = decimal.Parse(row["Breakpoint"].ToString()); }
                            catch { newPrice.BreakPoint = 0; }

                            try { newPrice.CurrencyID = currencies.Where(c => c.Code.ToLower() == row["Currency"].ToString().ToLower() || c.Symbol == row["Currency"].ToString()).Select(c => c.ID).First(); }
                            catch { newPrice.CurrencyID = 1; }

                            context.ProductPrices.Add(newPrice);
                        }
                    }
                }
                context.SaveChanges();

            }

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                var products = context.Products.Where(c => c.CompanyID == CompanyID && productIDsToUpdate.Contains(c.ID)).ToList();
                foreach (var p in products)
                {
                    p.UsePriceBreaks = true;
                    context.Products.Attach(p);
                    context.Entry(p).Property(x => x.UsePriceBreaks).IsModified = true;
                    updated++;
                }

                context.SaveChanges();
            }
            

            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }


        

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Product Prices have been successfully imported.\n\nProduct Prices Updated: {1}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.PRODUCTS, meessage, user, null);
        }

    }

}

