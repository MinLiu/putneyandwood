﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{

    public class ProductAlternativeImportService : ImportEntityServiceWithRemoveExisting<Product>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "ProductCode", "AlternativeProductCode" }; } }

        public ProductAlternativeImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }
        
        protected override ImportResult CompleteImport(DataTable data, bool removeExisting)
        {   
            int added = 0;
            int updated = 0;

            var productCodes = data.Rows.Cast<DataRow>()
                                              .Where(x => !string.IsNullOrEmpty(x["ProductCode"].ToString()))
                                              .GroupBy(x => x["ProductCode"].ToString())
                                              .Select(x => x.FirstOrDefault()["ProductCode"].ToString()).ToList();

            if (removeExisting)
            {
                using (var context = new SnapDbContext())
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    var pdts = context.ProductAlternatives.Where(p => productCodes.Contains(p.Product.ProductCode) && p.Product.CompanyID == CompanyID);
                    context.ProductAlternatives.RemoveRange(pdts);
                    context.SaveChanges();
                }
            }


            List<ProductAlternative> listToAdd = new List<ProductAlternative>();
            string alternativeCode = "";
            string productCode = "";

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                foreach (DataRow row in data.Rows)
                {
                    alternativeCode = row["AlternativeProductCode"].ToString();
                    productCode = row["ProductCode"].ToString();

                    if (string.IsNullOrWhiteSpace(productCode) || string.IsNullOrWhiteSpace(alternativeCode)) continue;

                    var exist = context.ProductAlternatives.Where(p => p.LinkedProduct.ProductCode == alternativeCode
                                                                 && p.Product.ProductCode == productCode
                                                                 && p.Product.CompanyID == CompanyID
                                                                 && p.LinkedProduct.CompanyID == CompanyID)
                                                                 .DefaultIfEmpty(null)
                                                                 .FirstOrDefault();

                    if (exist != null)
                    {

                    }
                    else
                    {
                        Guid? pdtID = context.Products.Where(p => p.CompanyID == CompanyID && p.ProductCode == productCode).ToList().Select(p => p.ID as Guid?).DefaultIfEmpty(null).FirstOrDefault();
                        Guid? altID = context.Products.Where(p => p.CompanyID == CompanyID && p.ProductCode == alternativeCode).ToList().Select(p => p.ID as Guid?).DefaultIfEmpty(null).FirstOrDefault();

                        if (pdtID.HasValue && altID.HasValue && listToAdd.Where(p => p.ProductID == pdtID && p.LinkedProductID == altID).Count() <= 0)
                        {
                            var newLink = new ProductAlternative
                            {
                                ProductID = pdtID.Value,
                                LinkedProductID = altID.Value,
                            };

                            listToAdd.Add(newLink);
                            added++;
                        }
                    }
                }

                context.SaveChanges();

            }


            //Add products in a different context to prevent DbUpdateConcurrencyException
            using (var context = new SnapDbContext())
            {
                context.ProductAlternatives.AddRange(listToAdd);
                context.SaveChanges();
            }


            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }


        

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Product Alternatives have been successfully imported.\n\nProduct Alternatives Updated: {1}; Product Alternatives Added: {2}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.PRODUCTS, meessage, user, null);
        }

    }

}

