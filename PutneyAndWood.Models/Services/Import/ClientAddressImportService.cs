﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ClientAddressImportService : ImportEntityService<ClientAddress>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "Client", "Name", "Address1", "Address2", "Address3", "Town", "Country", "Postcode", "Email", "Telephone", "Mobile" }; } }

        public ClientAddressImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable data)
        {
            int updated = 0;
            int added = 0;

            List<ClientAddress> listToAdd = new List<ClientAddress>();

            using (var context = new SnapDbContext())
            {

                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing contacts
                var existing = context.ClientAddresses.Where(c => c.Client.CompanyID == CompanyID && c.Client.Deleted == false).ToList();
                foreach (DataRow row in data.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["Client"].ToString())) continue;

                    var exist = existing.Where(p => p.Name == row["Name"].ToString()
                                                && p.Address1 == row["Address1"].ToString())
                                        .ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var a in exist)
                        {
                            a.Address1 = row["Address1"].ToString();
                            a.Address2 = row["Address2"].ToString();
                            a.Address3 = row["Address3"].ToString();
                            a.Town = row["Town"].ToString();
                            a.Postcode = row["Postcode"].ToString();
                            a.Country = row["Country"].ToString();
                            a.Telephone = row["Telephone"].ToString();
                            a.Mobile = row["Mobile"].ToString();
                            a.Email = row["Email"].ToString();
                            a.Deleted = false;

                            context.ClientAddresses.Attach(a);
                            context.Entry(a).State = EntityState.Modified;
                        }
                        updated++;
                    }
                    else
                    {
                        try
                        {
                            var name = row["Client"].ToString();

                            var newAdd = new ClientAddress
                            {
                                ClientID = context.Clients.Where(o => o.Name == name && o.CompanyID == CompanyID && o.Deleted == false).Select(o => o.ID).First(),
                                Name = row["Name"].ToString(),
                                Address1 = row["Address1"].ToString(),
                                Address2 = row["Address2"].ToString(),
                                Address3 = row["Address3"].ToString(),
                                Town = row["Town"].ToString(),
                                Postcode = row["Postcode"].ToString(),
                                Country = row["Country"].ToString(),
                                Telephone = row["Telephone"].ToString(),
                                Mobile = row["Mobile"].ToString(),
                                Email = row["Email"].ToString()
                            };

                            //Add addresses later in different context to prevent DbUpdateConcurrencyException
                            listToAdd.Add(newAdd);
                            added++;
                        }
                        catch { }
                    }
                }
                context.SaveChanges();
            }


            //Add addresses in a different context to prevent DbUpdateConcurrencyException
            using (var context = new SnapDbContext())
            {
                context.ClientAddresses.AddRange(listToAdd);
                context.SaveChanges();
            }


            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }


        

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Client Addresses have been successfully imported.\n\nClient Addresses Updated: {1}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.CLIENTS, meessage, user, null);
        }

    }

}

