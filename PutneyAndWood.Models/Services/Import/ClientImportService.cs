﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{

    public class ClientImportService : ImportEntityService<Client>
    {
        public override string[] RequiredImportColumns { get { return new string[] { "Name", "Type", "Address1", "Address2", "Address3", "Town", "County", "Postcode", "Country", "Email", "Telephone", "Mobile", "Website", "AccountReference", "Notes" }; } }

        public ClientImportService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        protected override ImportResult CompleteImport(DataTable dataTable)
        {
            int updated = 0;
            int added = 0;

            List<Client> listToAdd = new List<Client>();


            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing types
                var newTypeStrings = new List<string>();
                var typeStrings = context.ClientTypes.Where(c => c.CompanyID == CompanyID).Select(i => i.Name).ToList();
                foreach (DataRow row in dataTable.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["Type"].ToString())) continue;
                    newTypeStrings.Add(row["Type"].ToString());
                }

                context.ClientTypes.AddRange(newTypeStrings.Distinct().Where(i => !typeStrings.Contains(i)).Select(i => new ClientType { CompanyID = CompanyID, Name = i }));
                context.SaveChanges();
            }

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                var types = context.ClientTypes.Where(c => c.CompanyID == CompanyID).AsNoTracking().ToList();

                //Load existing clients
                var existing = context.Clients.Where(c => c.CompanyID == CompanyID).AsNoTracking().ToList();
                foreach (DataRow row in dataTable.Rows)
                {
                    if(string.IsNullOrWhiteSpace(row["Name"].ToString())) continue;
                    var exist = existing.Where(p => p.Name == row["Name"].ToString()).ToList();
                    
                    if (exist.Count > 0)
                    {
                        foreach (var o in exist)
                        {
                            o.Address1 = row["Address1"].ToString();
                            o.Address2 = row["Address2"].ToString();
                            o.Address3 = row["Address3"].ToString();
                            o.Town = row["Town"].ToString();
                            o.County = row["County"].ToString();
                            o.Postcode = row["Postcode"].ToString();
                            o.Country = row["Country"].ToString();
                            o.Email = row["Email"].ToString();
                            o.Telephone = row["Telephone"].ToString();
                            o.Mobile = row["Mobile"].ToString();
                            o.Website = row["Website"].ToString();
                            o.SageAccountReference = row["AccountReference"].ToString();
                            o.Notes = row["Notes"].ToString();
                            o.Created = DateTime.Now;
                            o.Type = types.Where(i => i.Name == row["Type"].ToString()).DefaultIfEmpty(null).FirstOrDefault();
                            o.TypeID = types.Where(i => i.Name == row["Type"].ToString()).Select(i => i.ID as Guid?).DefaultIfEmpty(null).FirstOrDefault();
                            o.Deleted = false;
                            context.Clients.Attach(o);
                            context.Entry(o).State = EntityState.Modified;
                            updated++;
                        }
                    }
                    else if (listToAdd.Where(s => s.Name == row["Name"].ToString()).Count() <= 0)
                    {
                        var newOrg = new Client
                        {
                            CompanyID = CompanyID,
                            Name = row["Name"].ToString(),
                            Address1 = row["Address1"].ToString(),
                            Address2 = row["Address2"].ToString(),
                            Address3 = row["Address3"].ToString(),
                            Town = row["Town"].ToString(),
                            County = row["County"].ToString(),
                            Postcode = row["Postcode"].ToString(),
                            Country = row["Country"].ToString(),
                            Email = row["Email"].ToString(),
                            Telephone = row["Telephone"].ToString(),
                            Mobile = row["Mobile"].ToString(),
                            Website = row["Website"].ToString(),
                            SageAccountReference = row["AccountReference"].ToString(),
                            Notes = row["Notes"].ToString(),
                            Type = null,
                            TypeID = types.Where(i => i.Name == row["Type"].ToString()).Select(i => i.ID as Guid?).DefaultIfEmpty(null).FirstOrDefault(),
                            Created = DateTime.Now,

                        };

                        //Add clients later in different context to prevent DbUpdateConcurrencyException
                        listToAdd.Add(newOrg);
                        added++;
                    }
                }
                context.SaveChanges();
            }


            //Add clients in a different context to prevent DbUpdateConcurrencyException
            using (var context = new SnapDbContext())
            {
                context.Clients.AddRange(listToAdd);
                context.SaveChanges();
            }

            return new ImportResult { Success = true, NumberCreated = added, NumberUpdated = updated };
        }
                

        protected override void OnImportCompleted(ImportResult result){
            if (string.IsNullOrWhiteSpace(CurrentUserID)) return;

            var user = new UserService(DbContext).FindById(CurrentUserID);
            
            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            var meessage = string.Format("{0} Clients have been successfully imported.\n\nClients Updated: {1}\nClients Added: {2}", result.NumberUpdated + result.NumberCreated, result.NumberUpdated, result.NumberCreated);
            logService.AddLog(user.CompanyID, ModuleTypeValues.CLIENTS, meessage, user, null);
        }

    }

}

