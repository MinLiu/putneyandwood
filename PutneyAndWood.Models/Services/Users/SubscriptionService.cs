﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    public class SubscriptionService<TViewModel> : CompanyEntityService<SubscriptionPlan, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        protected EntityRespository<SubscriptionCartItem> _cartItemRepo;
        protected EntityRespository<SubscriptionPlanPayment> _paymentRepo;
        protected EntityRespository<WorldPayResponse> _worldpayRepo;

        public EntityRespository<SubscriptionCartItem> CartItemRepository { get { if (_cartItemRepo == null) _cartItemRepo = new EntityRespository<SubscriptionCartItem>(DbContext); return _cartItemRepo; } }
        public EntityRespository<SubscriptionPlanPayment> PlanPaymentRepository { get { if (_paymentRepo == null) _paymentRepo = new EntityRespository<SubscriptionPlanPayment>(DbContext); return _paymentRepo; } }
        public EntityRespository<WorldPayResponse> WordPayRepository { get { if (_worldpayRepo == null) _worldpayRepo = new EntityRespository<WorldPayResponse>(DbContext); return _worldpayRepo; } }


        public SubscriptionService(Guid CompanyID, ModelMapper<SubscriptionPlan, TViewModel> mapper, DbContext db)
            : base(CompanyID, null, mapper, db)
        {
            
        }

        public SubscriptionService(ModelMapper<SubscriptionPlan, TViewModel> mapper, DbContext db)
            : base(Guid.Empty, null, mapper, db)
        {

        }

        //public SubscriptionPlan CurrentSubscriptionPlan()
        //{
        //    return CurrentSubscriptionPlan(CompanyID);
        //}

        //public SubscriptionPlan CurrentSubscriptionPlan(Guid CompanyID)
        //{
        //    return Read(CompanyID).Include(i => i.Company).Include(i => i.Company.StripeSettings)
        //                          .OrderBy(s => s.Active).OrderByDescending(s => s.StartDate)
        //                          .Take(1).ToArray().DefaultIfEmpty(null).FirstOrDefault();
        //}

        public SubscriptionUserAccessViewModel GetSubscriptionUserAccess(string userId)
        {
            var service = new UserService(DbContext);
            var user = service.FindById(userId);
            var role = service.GetUserRoleOption(user.Id);
            return GetSubscriptionUserAccess(user, role);
        }

        public SubscriptionUserAccessViewModel GetSubscriptionUserAccess(User user)
        {
            var role = new UserService(DbContext).GetUserRoleOption(user.Id);

            return GetSubscriptionUserAccess(user, role);
        }

        public SubscriptionUserAccessViewModel GetSubscriptionUserAccess(User user, UserRoleOption role)
        {
            return new SubscriptionUserAccessViewModel
            {
                //TrialMode = plan.Trial,
                //DaysRemaining = (plan.EndDate > DateTime.Today && plan.Trial) ? (plan.EndDate - DateTime.Today).Days : 0,
                //StripePaymentsEnabled = plan.Company.StripeSettings != null,
                //TeamManagementEnabled = plan.EnableTeamManagement,

                //NumberCRM = plan.NumberCRM,
                //NumberProducts = plan.NumberProducts,
                //NumberQuotations = plan.NumberQuotations,
                //NumberJobs = plan.NumberJobs,
                //NumberInvoices = plan.NumberInvoices,
                //NumberDeliveryNotes = plan.NumberDeliveryNotes,
                //NumberPurchaseOrders = plan.NumberPurchaseOrders,
                //NumberStockControl = plan.NumberStockControl,

                ViewQuotations = role.ViewQuotations && user.AccessQuotations,
                EditQuotations = role.EditQuotations && user.AccessQuotations,
                DeleteQuotations = role.DeleteQuotations && user.AccessQuotations,
                ExportQuotations = role.ExportQuotations && user.AccessQuotations,

                ViewJobs = role.ViewJobs && user.AccessJobs,
                EditJobs = role.EditJobs && user.AccessJobs,
                DeleteJobs = role.DeleteJobs && user.AccessJobs,
                ExportJobs = role.ExportJobs && user.AccessJobs,

                ViewInvoices = role.ViewInvoices && user.AccessInvoices,
                EditInvoices = role.EditInvoices && user.AccessInvoices,
                DeleteInvoices = role.DeleteInvoices && user.AccessInvoices,
                ExportInvoices = role.ExportInvoices && user.AccessInvoices,

                ViewPurchaseOrders = role.ViewPurchaseOrders && user.AccessPurchaseOrders,
                EditPurchaseOrders = role.EditPurchaseOrders && user.AccessPurchaseOrders,
                DeletePurchaseOrders = role.DeletePurchaseOrders && user.AccessPurchaseOrders,
                ExportPurchaseOrders = role.ExportPurchaseOrders && user.AccessPurchaseOrders,

                ViewDeliveryNotes = role.ViewDeliveryNotes && user.AccessDeliveryNotes,
                EditDeliveryNotes = role.EditDeliveryNotes && user.AccessDeliveryNotes,
                DeleteDeliveryNotes = role.DeleteDeliveryNotes && user.AccessDeliveryNotes,
                ExportDeliveryNotes = role.ExportDeliveryNotes && user.AccessDeliveryNotes,

                ViewStockControl = role.ViewStockControl && user.AccessStockControl,
                EditStockControl = role.EditStockControl && user.AccessStockControl,
                ExportStockControl = role.ExportStockControl && user.AccessStockControl,

                ViewCRM = role.ViewCRM && user.AccessCRM,
                EditCRM = role.EditCRM && user.AccessCRM,
                DeleteCRM = role.DeleteCRM && user.AccessCRM,
                ExportCRM = role.ExportCRM && user.AccessCRM,

                ViewProducts = role.ViewProducts && user.AccessProducts,
                EditProducts = role.EditProducts && user.AccessProducts,
                DeleteProducts = role.DeleteProducts && user.AccessProducts,
                ExportProducts = role.ExportProducts && user.AccessProducts,
            };
        }


        public SubscriptionPlan ConvertSubscriptionPlan(Guid cartItemID)
        {
            var item = CartItemRepository.Find(cartItemID);
            if (item != null) return ConvertSubscriptionPlan(item);
            else return null;
        }


        public SubscriptionPlan ConvertSubscriptionPlan(SubscriptionCartItem item)
        {
            return new SubscriptionPlan
            {
                CompanyID = item.CompanyID,
                Name = item.Name,
                Description = item.Description,
                Created = DateTime.Now,
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(1),
                InitialAmount = item.InitialAmount,
                RegularAmount = item.RegularAmount,
                Active = true,
                FuturePayID = null,
                InternalCartID = item.ID,
                CartID = item.CartID,

                NumberCRM = item.NumberCRM,
                NumberDeliveryNotes = item.NumberDeliveryNotes,
                NumberInvoices = item.NumberInvoices,
                NumberProducts = item.NumberProducts,
                NumberQuotations = item.NumberQuotations,
                NumberJobs = item.NumberJobs,
                NumberPurchaseOrders = item.NumberPurchaseOrders,
                NumberStockControl = item.NumberStockControl,
            };
        }

        public SubscriptionPlan ConvertSubscriptionPlan(WorldPayResponse response)
        {
            var item = CartItemRepository.Read().Where(c => ("QFLW-01-" + c.ID) == response.CartID).DefaultIfEmpty(null).FirstOrDefault();
            if (item == null) return null;

            var plan = ConvertSubscriptionPlan(item);
            plan.FuturePayID = response.FuturePayID;
            //plan.CartID = response.CartID;

            return plan;
        }

        
        public SubscriptionCartItem GenerateCartItem(SubscriptionCartItem model, SubscriptionPlan currentPlan = null)
        {
            decimal finalPrice = 0m;
            bool applyDiscount = false;

            int[] users = new int[] { model.NumberCRM, model.NumberProducts, model.NumberQuotations, model.NumberJobs, model.NumberInvoices, model.NumberDeliveryNotes, model.NumberPurchaseOrders, model.NumberStockControl };
            model.NumberCRM = users.OrderByDescending(x => x).First();
            model.NumberProducts = model.NumberCRM;
 

            if (model.NumberCRM > 0
                && model.NumberQuotations <= 0
                && model.NumberJobs <= 0
                && model.NumberDeliveryNotes <= 0
                && model.NumberInvoices <= 0
                && model.NumberPurchaseOrders <= 0
                && model.NumberStockControl <= 0)
            {
                finalPrice += (decimal)(model.NumberCRM) * 10.8m;
            }

            if (model.NumberQuotations > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }
            if (model.NumberJobs > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }
            if (model.NumberDeliveryNotes > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }
            if (model.NumberInvoices > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }
            if (model.NumberPurchaseOrders > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }
            if (model.NumberStockControl > 0) { finalPrice += 18m + (decimal)(model.NumberQuotations - 1) * 10.8m; }

            //Apply all modules discount
            if (model.NumberQuotations > 0
                && model.NumberJobs > 0
                && model.NumberDeliveryNotes > 0
                && model.NumberInvoices > 0
                && model.NumberPurchaseOrders > 0
                && model.NumberStockControl > 0)
            {
                applyDiscount = true;
                finalPrice -= 12m;
            }


            var cartItem = new SubscriptionCartItem
            {
                CompanyID = CompanyID,
                Name = string.Format("Quikflw Subscription - {0}{1}{2}{3}{4}{5}{6}{7}",
                                                                            (model.NumberCRM > 0) ? "CRM" : "",
                                                                            (model.NumberProducts > 0) ? ", PDTS" : "",
                                                                            (model.NumberQuotations > 0) ? ", Q" : "",
                                                                            (model.NumberJobs > 0) ? ", JOBS" : "",
                                                                            (model.NumberDeliveryNotes > 0) ? ", DN" : "",
                                                                            (model.NumberInvoices > 0) ? ", INV" : "",
                                                                            (model.NumberPurchaseOrders > 0) ? ", PO" : "",
                                                                            (model.NumberStockControl > 0) ? ", SC" : ""
                        ),
                Description = string.Format("Monthly Quikflw subscription plan."),

                InitialAmount = Math.Round(finalPrice, 2),
                RegularAmount = Math.Round(finalPrice, 2),

                DiscountAmount = (applyDiscount) ? 12m : 0m,

                NumberCRM = model.NumberCRM,
                NumberProducts = model.NumberProducts,
                NumberQuotations = model.NumberQuotations,
                NumberJobs = model.NumberJobs,
                NumberDeliveryNotes = model.NumberDeliveryNotes,
                NumberInvoices = model.NumberInvoices,
                NumberPurchaseOrders = model.NumberPurchaseOrders,
                NumberStockControl = model.NumberStockControl
            };

            //Get calculate intial amount based on previous subscription
            if (currentPlan != null)
            {
                if (DateTime.Now < currentPlan.EndDate)
                {

                    decimal days = (decimal)(currentPlan.EndDate - currentPlan.StartDate).TotalDays;
                    decimal left = (decimal)(currentPlan.EndDate - DateTime.Now).TotalDays;

                    cartItem.InitialAmount = Math.Round(cartItem.RegularAmount - (currentPlan.RegularAmount * (1 - (left / days))), 2);

                    if (cartItem.InitialAmount < 0) cartItem.InitialAmount = 0;
                }
            }

            return cartItem;
        }


        public SubscriptionPlan HandleWordPayResponse(WorldPayResponse response)
        {
            if (string.IsNullOrWhiteSpace(response.InstID))  return null;
            
            //Save reponse
            WordPayRepository.Create(response);
            
            if (response.RawAuthCode == "A") //Agreement completed OR Authorised payment made within a FuturePay agreement
            {
                
                var plan = Read().Where(p => p.FuturePayID == response.FuturePayID && p.FuturePayID != null).DefaultIfEmpty(null).FirstOrDefault();

                //Create payment record
                var payment = new SubscriptionPlanPayment
                {
                    Timestamp = DateTime.Now,
                    Description = response.Desc,
                    Amount = response.Amount,
                    Currency = response.Currency,
                    FuturePayID = response.FuturePayID
                };

                if (plan != null) //If a plan with the future id exists then update it. (Existing plan payment made)
                {
                    plan.Active = true;
                    plan.StartDate = DateTime.Today;
                    plan.EndDate = DateTime.Today.AddMonths(1);
                    Update(plan);

                    payment.CompanyID = plan.CompanyID;

                }
                else  //If a plan doesn't exist with the future id then create one.
                {
                    plan = ConvertSubscriptionPlan(response);
                    if (plan != null)
                    {
                        Create(plan);
                        payment.CompanyID = plan.CompanyID;

                        //Update the users of the company, de-assign users if less than subscription
                        try
                        {
                            var userService = new UserService(DbContext);
                            var users = userService.GetAllCompanyUsers(plan.CompanyID);

                            //Skip the users that are in the subscription range, but assign the users after.
                            foreach (var u in users.Where(u => u.AccessCRM == true).OrderBy(u => u.Id).Skip(plan.NumberCRM)) { u.AccessCRM = false; }
                            foreach (var u in users.Where(u => u.AccessProducts == true).OrderBy(u => u.Id).Skip(plan.NumberProducts)) { u.AccessProducts = false; }
                            foreach (var u in users.Where(u => u.AccessQuotations == true).OrderBy(u => u.Id).Skip(plan.NumberQuotations)) { u.AccessQuotations = false; }
                            foreach (var u in users.Where(u => u.AccessJobs == true).OrderBy(u => u.Id).Skip(plan.NumberJobs)) { u.AccessJobs = false; }
                            foreach (var u in users.Where(u => u.AccessInvoices == true).OrderBy(u => u.Id).Skip(plan.NumberInvoices)) { u.AccessInvoices = false; }
                            foreach (var u in users.Where(u => u.AccessDeliveryNotes == true).OrderBy(u => u.Id).Skip(plan.NumberDeliveryNotes)) { u.AccessDeliveryNotes = false; }
                            foreach (var u in users.Where(u => u.AccessPurchaseOrders == true).OrderBy(u => u.Id).Skip(plan.NumberPurchaseOrders)) { u.AccessPurchaseOrders = false; }
                            foreach (var u in users.Where(u => u.AccessStockControl == true).OrderBy(u => u.Id).Skip(plan.NumberStockControl)) { u.AccessStockControl = false; }

                            //update only the relevent information
                            userService.Update(users, new string[] { "AccessCRM", "AccessProducts", "AccessQuotations", "AccessJobs", "AccessInvoices", "AccessDeliveryNotes", "AccessPurchaseOrders", "AccessStockControl" });
                        }
                        catch { }
                    }
                }

                PlanPaymentRepository.Create(payment);

                return plan;
            }


            if (response.RawAuthCode == "D") //Payment declined
            {
                var plan = Read().Where(p => p.FuturePayID == response.FuturePayID).DefaultIfEmpty(null).FirstOrDefault();
                if (plan != null) return null;

                //Create payment record
                var payment = new SubscriptionPlanPayment
                {
                    CompanyID = plan.CompanyID,
                    Timestamp = DateTime.Now,
                    Description = "*Payment declined - " + response.Desc,
                    Amount = response.Amount,
                    Currency = response.Currency,
                    FuturePayID = response.FuturePayID
                };

                _paymentRepo.Create(payment);
                return plan;
            }

            if (response.FuturePayStatusChange.ToLower().Contains("cancelled"))
            {
                var plan = Read().Where(p => p.FuturePayID == response.FuturePayID).DefaultIfEmpty(null).FirstOrDefault();
                if (plan != null) return null;

                plan.Active = false;

                Update(plan, new string[] { "Active" });
                return plan;
            }

            return null;
        }        

    }



    public class SubscriptionUserAccessViewModel
    {

        public bool TrialMode { get; set; }
        public int DaysRemaining { get; set; }

        public int NumberModules { get; set; }

        public int NumberCRM { get; set; }
        public int NumberProducts { get; set; }
        public int NumberQuotations { get; set; }
        public int NumberJobs { get; set; }
        public int NumberInvoices { get; set; }
        public int NumberDeliveryNotes { get; set; }
        public int NumberPurchaseOrders { get; set; }
        public int NumberStockControl { get; set; }
        

        public bool ViewQuotations { get; set; }
        public bool EditQuotations { get; set; }
        public bool DeleteQuotations { get; set; }
        public bool ExportQuotations { get; set; }

        public bool ViewJobs { get; set; }
        public bool EditJobs { get; set; }
        public bool DeleteJobs { get; set; }
        public bool ExportJobs { get; set; }

        public bool ViewInvoices { get; set; }
        public bool EditInvoices { get; set; }
        public bool DeleteInvoices { get; set; }
        public bool ExportInvoices { get; set; } 

        public bool ViewPurchaseOrders { get; set; }
        public bool EditPurchaseOrders { get; set; }
        public bool DeletePurchaseOrders { get; set; }
        public bool ExportPurchaseOrders { get; set; }

        public bool ViewDeliveryNotes { get; set; }
        public bool EditDeliveryNotes { get; set; }
        public bool DeleteDeliveryNotes { get; set; }
        public bool ExportDeliveryNotes { get; set; }

        public bool ViewStockControl { get; set; }
        public bool EditStockControl { get; set; }
        public bool ExportStockControl { get; set; }

        public bool ViewCRM { get; set; }
        public bool EditCRM { get; set; }
        public bool DeleteCRM { get; set; }
        public bool ExportCRM { get; set; }

        public bool ViewProducts { get; set; }
        public bool EditProducts { get; set; }
        public bool DeleteProducts { get; set; }
        public bool ExportProducts { get; set; }

        public bool StripePaymentsEnabled { get; set; }
        public bool TeamManagementEnabled { get; set; }
    }
    

}

