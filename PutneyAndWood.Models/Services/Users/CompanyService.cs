﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using System.Web;
using System.IO;

namespace SnapSuite.Models
{
    public class CompanyService<TViewModel> : EntityService<Company, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        public override string[] ReferencesToInclude { get { return new string[] { "LogoImage", "ScreenLogoImage" }; } }

        public override string LoggingName() { return "Business details"; }
        public override string LoggingNamePlural() { return "Business details"; }


        public CompanyService(string UserId, ModelMapper<Company, TViewModel> mapper, DbContext db)
            : base(UserId, mapper, db)
        {
            
        }

        public Company UploadLogoImage(Guid CompanyID,  HttpPostedFileBase uploadedImage)
        {
            return UploadLogoImage(CompanyID, uploadedImage.ToByteArray(), uploadedImage.FileName, uploadedImage.ContentType);
        }


        public Company UploadLogoImage(Guid CompanyID, byte[] uploadedImage, string filename, string fileType)
        {
            return uploadLogo(CompanyID, uploadedImage, filename, fileType, true);
        }
        

        public Company UploadScreenLogoImage(Guid CompanyID, HttpPostedFileBase uploadedImage)
        {
            return uploadLogo(CompanyID, uploadedImage.ToByteArray(), uploadedImage.FileName, uploadedImage.ContentType, false);
        }


        public Company UploadScreenLogoImage(Guid CompanyID, byte[] uploadedImage, string filename, string fileType)
        {
            return uploadLogo(CompanyID, uploadedImage, filename, fileType, false);
        }


        public Company RemoveLogoImage(Guid CompanyID)
        {
            return RemoveLogo(CompanyID, true);
        }

        public Company RemoveScreenLogoImage(Guid CompanyID)
        {   
            return RemoveLogo(CompanyID, false);
        }


        public Company SetThemeColour(Guid CompanyID, string themeColor)
        {
            var model = Find(CompanyID);

            if (model == null) throw new Exception("Record not found");

            model.ThemeColor = themeColor;
            Update(model, new string[] { "ThemeColor" });

            return model;
        }


        public Company ResetThemeColour(Guid CompanyID)
        {
            var model = Find(CompanyID);

            if (model == null) throw new Exception("Record not found");

            model.ThemeColor = "#6e5097";
            Update(model, new string[] { "ThemeColor" });

            return model;
        }

        protected Company uploadLogo(Guid CompanyID, byte[] uploadedImage, string filename, string fileType, bool mainLogo)
        {
            const string relativePath = "/Images/";

            if (uploadedImage == null) throw new Exception("No Image uploaded.");
            if (!fileType.Contains("image")) throw new Exception("Uploaded file is not an Image.");
            if (uploadedImage.Length > 1 * 1024 * 1024) throw new Exception("Uploaded file is too big.");

            var model = Find(CompanyID);

            if (model == null) throw new Exception("Record not found");

            //Save file on Server
            var fileEntry = new FileService(DbContext).SaveFile(CompanyID, uploadedImage, Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename), relativePath);

            //if (string.IsNullOrEmpty(loc)) throw new Exception("Failed to upload file.");

            //Update the image URL
            if (mainLogo)
            {
                model.LogoImageURL = fileEntry.FilePath;
                model.LogoImage = fileEntry;
                //Update(model, new string[] { "LogoImageURL" });
            }
            else
            {
                model.ScreenLogoImageURL = fileEntry.FilePath;
                model.ScreenLogoImage = fileEntry;
                //Update(model, new string[] { "ScreenLogoImageURL" });
            }

            Update(model);
            return model;
        }


        protected Company RemoveLogo(Guid CompanyID, bool mainLogo)
        {
            var model = Read().Where(i => i.ID == CompanyID).Include(i => i.LogoImage).Include(i => i.ScreenLogoImage).First();

            if (model == null) throw new Exception("Record not found");

            if (mainLogo)
            {
                //FileService.DeleteFile(model.LogoImageURL);
                model.LogoImageURL = null;
                model.LogoImage = null;
                //Update(model, new string[] { "LogoImageURL" });
            }
            else
            {
                //FileService.DeleteFile(model.ScreenLogoImageURL);
                model.ScreenLogoImageURL = null;
                model.ScreenLogoImage = null;
                //Update(model, new string[] { "ScreenLogoImageURL" });
            }

            Update(model);
            return model;
        }

        

        public override void OnCreate(Company model) { }
        public override void OnCreate(IEnumerable<Company> models) { }
        public override void OnUpdate(Company model) { }
        public override void OnUpdate(IEnumerable<Company> models) { }
        public override void OnDestroy(Company model) { }
        public override void OnDestroy(IEnumerable<Company> models) { }

    }
    

}
