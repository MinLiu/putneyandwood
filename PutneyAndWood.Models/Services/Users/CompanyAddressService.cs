﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class CompanyAddressService<TViewModel> : ChildEntityService<Company, CompanyAddress, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        public override bool ApplyParentCompanyID { get { return false; } }

        public CompanyAddressService(Guid CompanyID, string UserId, ModelMapper<CompanyAddress, TViewModel> mapper, DbContext db)
            : base(CompanyID, CompanyID, UserId, mapper, db)
        {

        }


        public override void OnCreate(CompanyAddress model) { }
        public override void OnCreate(IEnumerable<CompanyAddress> models) { }
        public override void OnUpdate(CompanyAddress model) { }
        public override void OnUpdate(IEnumerable<CompanyAddress> models) { }
        public override void OnDestroy(CompanyAddress model) { }
        public override void OnDestroy(IEnumerable<CompanyAddress> models) { }
    }


}
