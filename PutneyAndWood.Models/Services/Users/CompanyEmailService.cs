﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Net.Mail;
using System.Net;

namespace SnapSuite.Models
{
    
    public class CompanyEmailService<TViewModel> : ChildEntityService<Company, CompanyEmail, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        public override bool ApplyParentCompanyID { get { return false; } }

        public CompanyEmailService(Guid CompanyID, string UserId, ModelMapper<CompanyEmail, TViewModel> mapper, DbContext db)
            : base(CompanyID, CompanyID, UserId, mapper, db)
        {

        }


        public override IQueryable<CompanyEmail> Read()
        {
            return base.Read().Where(i => i.CompanyID == CompanyID);
        }

        public override IQueryable<CompanyEmail> Read(Guid? ParentID)
        {
            return base.Read(ParentID).Where(i => i.CompanyID == CompanyID);
        }


        public override CompanyEmail Create(CompanyEmail entity)
        {
            if (Read(entity.CompanyID).Where(i => i.Default).Count() <= 0) entity.Default = true;
            return base.Create(entity);
        }

        public override CompanyEmail Update(CompanyEmail entity, string[] updatedColumns = null)
        {
            entity.SuccessfulTest = false;
            return base.Update(entity, updatedColumns);
        }

        public override IEnumerable<CompanyEmail> Update(IEnumerable<CompanyEmail> entities, string[] updatedColumns = null)
        {
            foreach(var e in entities){ e.SuccessfulTest = false; }
            return base.Update(entities, updatedColumns);
        }

        public virtual string TestEmail(Guid ID)
        {
            try
            {
                var model = FindByID(ID);
                
                var host = model.Host;
                var port = model.Port;
                var userName = model.Email;
                var password = model.Password;
                var enableSsl = model.EnableSSL;
                    
                var client = new SmtpClient(host, port)
                {
                    Credentials = new NetworkCredential(userName, password),
                    EnableSsl = enableSsl
                };
                
                client.Send(EmailMessageService.CreateSNAPSuiteMailMessage(model.Email, model.Email, "Quikflw Test Email", "This is a test email."));

                model.SuccessfulTest = true;
                base.Update(model, new string[] { "SuccessfulTest" });
                return "Sucess: test Email Sent";
            }
            catch(Exception ex) { return ex.Message; }
           
        }

        public override void OnCreate(CompanyEmail model) { }
        public override void OnCreate(IEnumerable<CompanyEmail> models) { }
        public override void OnUpdate(CompanyEmail model) { }
        public override void OnUpdate(IEnumerable<CompanyEmail> models) { }
        public override void OnDestroy(CompanyEmail model) { }
        public override void OnDestroy(IEnumerable<CompanyEmail> models) { }
    }


}
