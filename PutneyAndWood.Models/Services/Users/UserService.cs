﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    public class UserService : GenericService<User>
    {
        
        public UserService(DbContext db)
            : base(db)
        {
            
        }

        public virtual User FindById(string ID)
        {
            return Read().Where(u => u.Id == ID).Include(i => i.Company).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();
        }

        public virtual User FindByEmail(string email)
        {
            return Read().Where(u => u.Email == email).Include(i => i.Company).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();
        }

        public virtual UserRoleOption GetUserRoleOption(string ID)
        {
            var user = Read().Where(u => u.Id == ID).Include(u => u.Roles).DefaultIfEmpty(null).FirstOrDefault();
            if (user == null) return null;
            if (user.Roles.Count() <= 0) return null;

            var roleId = user.Roles.First().RoleId;
            return new GenericService<UserRoleOption>(DbContext).Read().Where(u => u.IdentityRoleID.ToString() == roleId).Include(i => i.Company).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();
        }

        public ICollection<User> GetAllCompanyUsers(Guid CompanyID)
        {
            return new GenericService<User>(DbContext).Read().Where(u => u.CompanyID == CompanyID).OrderBy(u => u.Email).ToList();
        }

        public ICollection<string> GetCompanyUserIds(User user)
        {
            return GetCompanyUsers(user.Id).Select(u => u.Id).ToList();
        }

        public ICollection<User> GetCompanyUsers(User user)
        {
            return GetCompanyUsers(user.Id);
        }

        public ICollection<string> GetCompanyUserIds(string userId)
        {
            return GetCompanyUsers(userId).Select(u => u.Id).ToList();
        }

        public ICollection<User> GetCompanyUsers(string userId)
        {
            var _user = FindById(userId);

            //If user can't if found then return the userID input
            if (_user == null) return new List<User>() { };
            
            
            if (_user.Company.UseTeams)
            {
                //Get the teams where the user is in
                var _teamRepo = new GenericService<TeamMember>(DbContext);
                var teamIDs = _teamRepo.Read().Where(t => t.UserId == _user.Id).Select(t => t.TeamID);

                return _teamRepo.Read()
                        .Where(u => teamIDs.Contains(u.TeamID))
                        .Include(u => u.User)                        
                        .OrderBy(u => u.User.Email)
                        .Select(u => u.User)
                        .ToList();
            }
            else if (_user.Company.UseHierarchy)
            {
                var ids = new List<Guid>();
                var allHiers = new GenericService<HierarchyMember>(DbContext).Read()
                                                                                .Where(t => t.CompanyID == _user.CompanyID)
                                                                                .Include(i => i.Underlings)
                                                                                .Include(i => i.User)
                                                                                .ToList();

                var userHiers = allHiers.Where(i => i.UserId == _user.Id);

                foreach (var h in userHiers)
                {
                    GetUnderlingIDs(h, ids, allHiers);

                    if (_user.Company.AccessSameLevelHierarchy)
                        GetPeerIDs(h, ids, allHiers);
                }

                return allHiers.Where(i => ids.Contains(i.ID)).OrderBy(u => u.User.Email).Select(u => u.User).ToList();
            }
            else
            {
                return new GenericService<User>(DbContext).Read().Where(u => u.CompanyID == _user.CompanyID).OrderBy(u => u.Email).ToList();
                
            }
         }
        
        

        protected void GetUnderlingIDs(HierarchyMember member, List<Guid> list, List<HierarchyMember> allHeirs)
        {
            var model = allHeirs.Where(i => i.ID == member.ID).First();
            list.Add(model.ID);

            foreach (var under in model.Underlings.ToList())
            {
                GetUnderlingIDs(under, list, allHeirs);
            }
        }

        protected void GetPeerIDs(HierarchyMember member, List<Guid> list, List<HierarchyMember> allHeirs)
        {           
            var model = allHeirs.Where(i => i.ID == member.ID).First();
            var peers = allHeirs.Where(i => i.ReportsToID == model.ReportsToID);
            list.Add(model.ID);

            foreach (var peer in peers)
            {
                list.Add(peer.ID);
            }
        }

    }
    

}
