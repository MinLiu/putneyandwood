﻿using DocumentFormat.OpenXml.Wordprocessing;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Models
{
    public static class DocumentService
    {

        public static byte[] GeneratePdfFromDocx(string templateFilePath, Dictionary<string, string> replacements, Dictionary<string, string> images)
        {
            byte[] byteArray = null;

            //Load the word documnet template
            using (FileStream stream = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    byteArray = binaryReader.ReadBytes((int)stream.Length);
                }
            }

            return GeneratePdfFromDocx(byteArray, replacements, images);

        }

        public static byte[] GeneratePdfFromDocx(byte[] byteArray, Dictionary<string, string> replacements, Dictionary<string, string> images)
        {
            
            //Editing the document is done in memory to prevent the tempalte from being changed
            using (MemoryStream ms = new MemoryStream())
            {
                //Convert the word document into a WordprocessingDocument
                ms.Write(byteArray, 0, (int)byteArray.Length);
                DocumentFormat.OpenXml.Packaging.WordprocessingDocument wordprocessingDocument = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(ms, true);
                Body body = wordprocessingDocument.MainDocumentPart.Document.Body;

                //Get all the paragraphs from the body, headers and footers.
                List<DocumentFormat.OpenXml.Wordprocessing.Paragraph> paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in images)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            try
                            {
                                paragraphs[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                Drawing pic = OpenXmlHelpers.CreateImageElement(dicItem.Value, wordprocessingDocument);
                                paragraphs[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                            }
                            catch { }
                        }
                    }
                }


                foreach (var header in wordprocessingDocument.MainDocumentPart.HeaderParts)
                {
                    var headerParas = header.Header.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                    for (int x = headerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in images)
                        {
                            if (headerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    headerParas[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                    Drawing pic = OpenXmlHelpers.CreateHeaderImageElement(dicItem.Value, header);
                                    headerParas[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(headerParas);
                }

                foreach (var footer in wordprocessingDocument.MainDocumentPart.FooterParts)
                {
                    var footerParas = footer.Footer.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                    for (int x = footerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in images)
                        {
                            if (footerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    footerParas[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                    Drawing pic = OpenXmlHelpers.CreateFooterImageElement(dicItem.Value, footer);
                                    footerParas[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(footerParas);
                }


                //Second replace paragrph texts, if text is empty then remove the whole paragraph (Prevent empty newlines).
                //Because a paragraph can be made of multiple Runs, which can sometimes split the paragraph text. 
                //We get the copy the paragraph text into a new paragraph with a single run that has the styling properties of the first run in the paragraph.
                //var splitArray = new char[] { '{', '}' };
                RunProperties runPr = null;
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in replacements)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            //Get the stying properties of the first run in the paragraph. 
                            //If  it fails to get the style then it should use the previous one found. This fixes the multiple tags in the some paragraph style remove issue. Example: {Tag} {Tag}
                            try { runPr = paragraphs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Run>().First().RunProperties.CloneNode(true) as DocumentFormat.OpenXml.Wordprocessing.RunProperties; }
                            catch { }

                            //Replace the tag in paragraph
                            OpenXmlHelpers.InsertParagraphText2(paragraphs[x], dicItem.Key ?? "", dicItem.Value ?? "");
                        }
                    }
                }

                var fontList = wordprocessingDocument.MainDocumentPart.FontTablePart.Fonts.Elements<DocumentFormat.OpenXml.Wordprocessing.Font>().Select(f => f.Name);

                //Fourth, save document and convert to PDF.
                wordprocessingDocument.Save();
                wordprocessingDocument.Close();


                using (MemoryStream exportMs = new MemoryStream())
                {
                    Spire.Doc.Document document = new Spire.Doc.Document();

                    ms.Position = 0;
                    document.LoadFromStream(ms, Spire.Doc.FileFormat.Docx);
                    Spire.Doc.ToPdfParameterList toPdf = new Spire.Doc.ToPdfParameterList();
                    toPdf.PdfConformanceLevel = Spire.Pdf.PdfConformanceLevel.None;
                    foreach (var font in fontList)
                    {
                        try { toPdf.EmbeddedFontNameList.Add(font); } catch { }
                    }
                    document.EmbedSystemFonts = true;
                    document.SaveToStream(exportMs, toPdf);
                    return exportMs.ToArray();
                }
            }

        }



        public static byte[] PdfToPdf(string filepath, Dictionary<string, string> replacements)
        {
            if (!File.Exists(filepath)) throw new Exception("File does not exist");

            for (int i = 0; i < 20; i++)
            {
                try
                {
                    using (FileStream existingFileStream = new FileStream(filepath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            PdfReader pdfReader = new PdfReader(existingFileStream);
                            PdfStamper stamper = new PdfStamper(pdfReader, ms);
                            AcroFields form = stamper.AcroFields;

                            //Replace Acro Fields ()
                            foreach (var field in replacements)
                            {
                                try
                                {
                                    form.SetField(field.Key, field.Value, true);
                                    form.SetFieldProperty(field.Key.ToString(), "setfflags", PdfFormField.FF_READ_ONLY, null);
                                }
                                catch { }
                            }

                            stamper.Close();
                            pdfReader.Close();

                            return ms.ToArray();
                        }
                    }

                }
                catch
                {
                    //System.Threading.Thread.Sleep(500);
                }
            }

            throw new Exception("Failed to open pdf file.");
        }


        public static byte[] GenerateErrorPDF(Exception exception)
        {
            return GenerateErrorPDF(exception.Message);
        }

        public static byte[] GenerateErrorPDF(string errorMessage)
        {
            Dictionary<string, string> errorMessages = new Dictionary<string, string>();
            errorMessages.Add("{ErrorMessage}", System.Text.RegularExpressions.Regex.Replace(errorMessage, @"[^a-zA-Z0-9\\:_\- ]", string.Empty));

            Assembly assembly = Assembly.GetAssembly(typeof(EmailMessageService));
            Stream stream = assembly.GetManifestResourceStream("SnapSuite.Models.Resources.ErrorTemplate.docx");

            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                ms.ToArray();
                return DocumentService.GeneratePdfFromDocx(ms.ToArray(), errorMessages, new Dictionary<string, string>());
            }
        }


    }
}
