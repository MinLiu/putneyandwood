﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using System.Web;

namespace SnapSuite.Models
{
    
    public class ProductStockService : ProductService<StockOverviewViewModel>
    {
        public ProductStockService(Guid CompanyID, string UserId, OrderItemModelMapper<Product, StockOverviewViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {
        }

        public StockOverviewViewModel UpdateStockSetting(StockOverviewViewModel viewModel)
        {
            //var model = base.FindByID(Guid.Parse(viewModel.ID));
            //if (model.EnableReference != viewModel.EnableReference)
            //{
            //    var pdtLocationService = new ProductStockLocationService<ProductStockLocationViewModel>(CompanyID, CurrentUserID, new ProductStockLocationMapper(), DbContext);
            //    if (viewModel.EnableReference == true)
            //    {
            //        var pdts = pdtLocationService.Read().Where(x => x.ProductCode == viewModel.ProductCode).ToList();
            //        foreach (var pdt in pdts)
            //        {
            //            while(pdt.Quantity > 1)
            //            {
            //                pdtLocationService.Create(new ProductStockLocation()
            //                {
            //                    CompanyID = pdt.CompanyID,
            //                    Description = pdt.Description,
            //                    ProductCode = pdt.ProductCode,
            //                    Quantity = 1,
            //                    StockLocationID = pdt.StockLocationID,
            //                    EnableReference = true
            //                });
            //                pdt.Quantity--;
            //            }
            //            pdt.EnableReference = true;
            //            pdtLocationService.Update(pdt);
            //        }
            //    }
            //    else
            //    {
            //        var pdts = pdtLocationService.Read().Where(x => x.ProductCode == viewModel.ProductCode).GroupBy(x => new { x.ProductCode, x.Description, x.StockLocationID }).ToList();
            //        var newPdtList = new List<ProductStockLocation>();
            //        foreach (var pdt in pdts)
            //        {
            //            newPdtList.Add(new ProductStockLocation()
            //            {
            //                CompanyID = pdt.First().CompanyID,
            //                ProductCode = pdt.Key.ProductCode,
            //                Description = pdt.Key.Description,
            //                StockLocationID = pdt.Key.StockLocationID,
            //                Quantity = pdt.Sum(x => x.Quantity),
            //                EnableReference = false
            //            });
            //        }
            //        pdtLocationService.Destroy(pdtLocationService.Read().Where(x => x.ProductCode == viewModel.ProductCode));
            //        pdtLocationService.Create(newPdtList);
            //    }
            //}
            return base.Update(viewModel);
        }
    }


}
