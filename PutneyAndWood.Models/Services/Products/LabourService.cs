﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    
    public class LabourService<TViewModel> : CompanyEntityService<Labour, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public LabourService(Guid CompanyID, string UserID, ModelMapper<Labour, TViewModel> mapper, DbContext context)
            : base(CompanyID, UserID, mapper, context)
        {
            
        }
        
    }
    

}
