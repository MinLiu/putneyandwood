﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ProductAssociativeService<TViewModel> : ChildEntityService<Product, ProductAssociative, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Product", "LinkedProduct" }; } }
        

        public ProductAssociativeService(Guid CompanyID, Guid? ProductID, string UserId, ModelMapper<ProductAssociative, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ProductID, UserId, mapper, dbContext)
        {
            
        }

        public override IQueryable<ProductAssociative> Read(Guid? ParentID)
        {
            return base.Read(ParentID).Where(p => p.Product.Deleted == false);
        }
                

        public override void OnCreate(ProductAssociative model)
        {
            AddLog(model, string.Format("Product '{1}' associated to product '{{0}}'.", 0, model.ToShortLabel()));
        }

        public override void OnUpdate(ProductAssociative model)
        {
            AddLog(model, string.Format("Associated  product '{1}' updated on product '{{0}}'.", 0, model.ToShortLabel()));
        }

        public override void OnDestroy(ProductAssociative model)
        {
            AddLog(model, string.Format("Associated product '{1}' removed from product '{{0}}'.", 0, model.ToShortLabel()));
        }
    }


}
