﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class ProductKitItemService<TViewModel> : ChildEntityService<Product, ProductKitItem, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public ProductKitItemService(Guid CompanyID, Guid? ProductID, string UserId, ModelMapper<ProductKitItem, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ProductID, UserId, mapper, dbContext)
        {
            
        }

        public override IQueryable<ProductKitItem> Read(Guid? ParentID)
        {
            return base.Read(ParentID).Where(i => i.Product.Deleted == false);
        }

        public override ProductKitItem Create(ProductKitItem entity)
        {
            //Get the last position
            int lastPos = 0;
            try { lastPos = Read(null).Where(i => i.KitID == entity.KitID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); }
            catch { }

            entity.SortPos = lastPos + 1;
            return base.Create(entity);
        }


        public override IEnumerable<ProductKitItem> Create(IEnumerable<ProductKitItem> entities)
        {
            foreach (var entity in entities)
            {
                Create(entity);
            }

            return entities;
        }


        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            ProductKitItem move = null;
            ProductKitItem top = null;
            move = Read().Where(i => i.ID == itemID).First();
            var items = Read().Where(i => i.KitID == move.KitID).OrderBy(i => i.SortPos).ToList();            
            items.Remove(move);

            top = (newPosition - 1 > 0) ? items.ElementAt(newPosition - 1) : null;
            items.Insert(newPosition, move);


            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
            }

            Update(items, new string[] { "SortPos" });
            return true;
        }


        public override void OnCreate(ProductKitItem model)
        {
            AddLog(model, string.Format("Product '{1}' added to kit '{{0}}'.", 0, model.ToShortLabel()));
        }

        public override void OnUpdate(ProductKitItem model)
        {
            AddLog(model, string.Format("Product '{1}' updated on kit '{{0}}'.", 0, model.ToShortLabel()));
        }

        public override void OnDestroy(ProductKitItem model)
        {
            AddLog(model, string.Format("Product '{1}' removed from kit '{{0}}'.", 0, model.ToShortLabel()));
        }
        


        public DataFileOutput Export(string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            var productIds = new ProductService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).Search(CompanyID, filterText, category, supplierID, deleted).Select(p => p.ID).ToArray();
            var kits = Read(null).Where(k => productIds.Contains(k.KitID))
                                .Include(p => p.Product)
                                .Include(p => p.Kit)
                                .OrderBy(p => p.Kit.ProductCode)
                                .ThenBy(p => p.Product.ProductCode);

            var list = kits.ToList();


            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            int x = 0;

            

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var item in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(item.Kit.ProductCode); x++;
                row.CreateCell(x).SetCellValue(item.Product.ProductCode); x++;
                row.CreateCell(x).SetCellValue((double)item.Quantity); x++;
            }


            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("KitProductCode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("ItemProductCode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Quantity"); sheet.AutoSizeColumn(x); x++;

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("ProductKitItemsExport {0}.xls", DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };
        }


        public virtual bool AddLog(ProductKitItem model, string message, string currentUserID)
        {
            var user = new UserService(DbContext).FindById(currentUserID);
            return AddLog(model, message, user);
        }

        public virtual bool AddLog(ProductKitItem model, string message, User currentUser)
        {

            if (model == null) return false;
            if (currentUser == null) return false;
            if (string.IsNullOrWhiteSpace(message)) return false;

            int type = ModuleTypeValues.GetLogType(typeof(ProductKitItem));
            if (type == 0) return false;
            

            var product = new GenericService<Product>(DbContext).Find(model.ProductID);
            var kit = new GenericService<Product>(DbContext).Find(model.KitID);
            var logService = new ActivityLogService<TViewModel>(currentUser.Id, currentUser.CompanyID, null, DbContext);
            logService.AddLog(type, string.Format(message, product.ToShortLabel(), kit.ToShortLabel()));

            return true;
        }

    }


}
