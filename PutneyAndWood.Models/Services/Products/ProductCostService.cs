﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class ProductCostService<TViewModel> : ChildEntityService<Product, ProductCost, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        

        public ProductCostService(Guid CompanyID, Guid? ProductID, string UserId, ModelMapper<ProductCost, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ProductID, UserId, mapper, dbContext)
        {
            
        }
        

        public override void OnCreate(ProductCost model){ AddLog(model, "Product '{0}' cost breaks updated."); }

        public override void OnUpdate(ProductCost model) { AddLog(model, "Product '{0}' cost breaks updated."); }

        public override void OnDestroy(ProductCost model) { AddLog(model, "Product '{0}' cost breaks updated."); }


        public DataFileOutput Export(string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {

            var productIds = new ProductService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).Search(CompanyID, filterText, category, supplierID, deleted).Select(p => p.ID).ToArray();
            var costs = Read(null).Where(k => productIds.Contains(k.ProductID))
                                   .Include(p => p.Product)
                                   .Include(p => p.Currency)
                                   .OrderBy(p => p.Product.ProductCode)
                                   .ThenBy(p => p.BreakPoint);

            var list = costs.ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            int x = 0;
            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var cost in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(cost.Product.ProductCode); x++;
                row.CreateCell(x).SetCellValue(cost.Currency.Code); x++;
                row.CreateCell(x).SetCellValue((double)cost.Cost); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)cost.BreakPoint); row.GetCell(x).CellStyle = numberStyle; x++;
            }


            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("ProductCode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Currency"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Cost"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("BreakPoint"); sheet.AutoSizeColumn(x); x++;


            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("ProductCostsExport{0}.xls", DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };

        }


        public virtual bool AddLog(ProductCost model, string message, string currentUserID)
        {
            var user = new UserService(DbContext).FindById(currentUserID);
            return AddLog(model, message, user);
        }

        public virtual bool AddLog(ProductCost model, string message, User currentUser)
        {

            if (model == null) return false;
            if (currentUser == null) return false;
            if (string.IsNullOrWhiteSpace(message)) return false;

            int type = ModuleTypeValues.GetLogType(typeof(ProductCost));
            if (type == 0) return false;
            

            var product = new GenericService<Product>(DbContext).Find(model.ProductID);
            var logService = new ActivityLogService<TViewModel>(currentUser.Id, CompanyID, null, DbContext);
            logService.AddLog(type, string.Format(message, product.ToShortLabel()));

            return true;
        }

    }


}
