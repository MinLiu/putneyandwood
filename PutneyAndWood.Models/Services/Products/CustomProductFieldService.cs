﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    
    public class CustomProductFieldService<TViewModel> : CustomFieldService<CustomProductField, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public CustomProductFieldService(Guid CompanyID, string UserID, ModelMapper<CustomProductField, TViewModel> mapper, DbContext context)
            : base(CompanyID, UserID, mapper, context)
        {
            
        }

    }
    

}
