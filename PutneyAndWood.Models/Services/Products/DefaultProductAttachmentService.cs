﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class DefaultProductAttachmentService<TViewModel> : AttachmentService<Product, DefaultProductAttachment, TViewModel>        
        where TViewModel : class, IEntityViewModel, new()
    {
               

        public DefaultProductAttachmentService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<DefaultProductAttachment, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }

        public override string LoggingName() { return "Default Attachment"; }
        public override string LoggingNamePlural() { return "Default Attachments"; }
        
        
    }


}
