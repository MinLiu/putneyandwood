﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ProductCategoryService        
    {
        public DbContext DbContext { get; set; }
        public Guid CompanyID { get; set; }


        public ProductCategoryService(Guid companyID, DbContext db)
        {
            DbContext = db;
            CompanyID = companyID;
        }



        public IEnumerable<string> GetCategorys()
        {
            return GetCategorys(CompanyID);
        }


        public IEnumerable<string> GetCategorys(Guid companyID)
        {
            var service = new GenericService<Product>(DbContext);
            return service.Read().Where(i => i.Deleted == false && i.CompanyID == companyID)
                                 .OrderBy(i => i.Category)
                                 .Select(i => i.Category)
                                 .Distinct()
                                 .ToArray()
                                 .ToList();
        }

    }


}
