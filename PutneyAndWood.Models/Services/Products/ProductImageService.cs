﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{

    public class ProductImageService<TViewModel> : ItemImageService<Product, ProductImage, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public ProductImageService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<ProductImage, TViewModel> mapper, DbContext db) :
            base(CompanyID, ParentID, UserId, mapper, db)
        {

        }
        
    }
    
}
