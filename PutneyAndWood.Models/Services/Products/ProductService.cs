﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using System.Web;

namespace SnapSuite.Models
{
    
    public class ProductService<TViewModel> : CompanyEntityDeletableService<Product, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Images", "Images.File" }; } }

        // <summary> The Company ID value. Leave Empty to avoid parent company ID check</summary>
        public override Guid CompanyID
        {
            get { return base.CompanyID; }
            set
            {
                base.CompanyID = value;
                if (this.Mapper != null)
                {
                    var mapper = this.Mapper as OrderItemModelMapper<Product, TViewModel>;
                    if (mapper.CustomFieldService != null) mapper.CustomFieldService.CompanyID = value;
                }
            }
        }

        public ProductService(Guid CompanyID, string UserId, OrderItemModelMapper<Product, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {
            if (mapper != null)
                mapper.CustomFieldService = new CustomProductFieldService<EmptyViewModel>(CompanyID, null, null, db);
        }
        

        public virtual IQueryable<Product> Search(string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            return Search(CompanyID, filterText, category, supplierID, deleted);
        }

        public virtual IQueryable<Product> Search(Guid? companyID, string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            filterText = (filterText ?? "").ToLower();

            return Read(deleted, CompanyID).Where(pdt =>
                (
                        (pdt.ProductCode.ToLower().Contains(filterText)
                            || pdt.Category.ToLower().Contains(filterText)
                            || pdt.Description.ToLower().Contains(filterText)
                            || pdt.Unit.ToLower().Contains(filterText)
                            || pdt.JsonCustomFieldValues.ToLower().Contains(filterText)
                        //|| pdt.VendorCode.ToLower().Contains(filterText)
                        //|| pdt.Manufacturer.ToLower().Contains(filterText)
                        ) || filterText.Equals("") || filterText.Equals(null)
                    )
                    && (pdt.Category.Equals(category) || (category.Equals("") || category.Equals(null))));
        }


        public IQueryable<Product> ReadAlternatives(string productCode, bool byCategory = false)
        {
           

            if (byCategory)
            {
                var cat = Read().Where(p => p.ProductCode == productCode).Select(p => p.Category).DefaultIfEmpty(null).FirstOrDefault();

                return Read().Where(p => p.Category == cat && !string.IsNullOrEmpty(cat))
                                       .OrderBy(p => p.ProductCode);
            }
            else
            {
                var altService = new ProductAlternativeService<TViewModel>(CompanyID, null, CurrentUserID, null, DbContext);
                var ids = Read().Where(p => p.ProductCode == productCode && p.IsKit == false).Select(p => p.ID).ToList();

                ids.AddRange(altService.Read().Where(p => (p.Product.ProductCode == productCode || p.LinkedProduct.ProductCode == productCode)
                                                       && p.Product.Deleted == false
                                                       && p.Product.IsKit == false
                                                     )
                                              .Select(p => p.ProductID).ToList());

                foreach (var id in ids.ToList())
                {
                    ids.AddRange(altService.Read().Where(p => p.ProductID == id
                                                    && p.Product.Deleted == false
                                                    && p.Product.IsKit == false
                                                    )
                                                .Select(p => p.LinkedProductID).ToList());
                }

                ids = ids.Distinct().ToList();


                return Read().Where(p => ids.Contains(p.ID))
                                       .OrderBy(p => p.ProductCode);
            }
        }


        public IQueryable<Product> ReadAssociatives(string productCode)
        {
            var altService = new ProductAssociativeService<TViewModel>(CompanyID, null, CurrentUserID, null, DbContext);
            var ids = altService.Read().Where(p => p.Product.ProductCode == productCode
                                           && p.Product.Deleted == false
                                           //&& p.Product.IsKit == false
                                       )
                                   .Select(p => p.LinkedProductID).Distinct().ToList();

            return Read().Where(p => ids.Contains(p.ID))
                                   .OrderBy(p => p.ProductCode);
        }


        public override Product Create(Product entity)
        {            
            if (new GenericService<Currency>(DbContext).Find(entity.CurrencyID) == null && CompanyID != Guid.Empty)
                entity.CurrencyID = new GenericService<Company>(DbContext).Find(CompanyID).DefaultCurrencyID;

            var result = base.Create(entity);
            return result;
        }

        public override IEnumerable<Product> Create(IEnumerable<Product> entities)
        {
            foreach (var entity in entities)
            {               
                if (new GenericService<Currency>(DbContext).Find(entity.CurrencyID) == null && CompanyID != Guid.Empty)
                    entity.CurrencyID = new GenericService<Company>(DbContext).Find(CompanyID).DefaultCurrencyID;
            }

            var result = base.Create(entities);
            return result;
        }

        public override Product Update(Product entity, string[] updatedColumns = null)
        {
            var pre = Read().Where(i => i.ID == entity.ID).AsNoTracking().FirstOrDefault();

            return base.Update(entity, updatedColumns);
        }


        public Product FindProduct(string productcode)
        {   
            return Read().Where(p => p.ProductCode == productcode).DefaultIfEmpty(null).FirstOrDefault();
        }


        public Product GetProduct(Guid id, decimal quantity, int currencyID, bool applySupplierDiscount = true)
        {
            var product = Read().Where(p => p.ID == id && p.Deleted == false)
                                    .Include(p => p.KitItems)
                                    .Include(p => p.Prices)
                                    .Include(p => p.Costs)
                                    .Include(p => p.Images)
                                    .Include(p => p.Images.Select(x => x.File))
                                    .Include(p => p.DefaultAttachments)
                                    .AsNoTracking()
                                    .DefaultIfEmpty(null)
                                    .FirstOrDefault();

            if (product == null) return null;
 

            decimal price = 0m;
            decimal cost = 0m;

            if (product.CurrencyID != currencyID && !product.UsePriceBreaks)
            {
                //Don't copy over the prices
            }
            else if (product.CurrencyID == currencyID && !product.UsePriceBreaks)
            {
                price = product.Price;
                cost = product.Cost;
            }
            else
            {
                try { price = product.Prices.Where(m => m.BreakPoint <= quantity && m.CurrencyID == currencyID).OrderByDescending(m => m.BreakPoint).Select(m => m.Price).First(); }
                catch { }

                try { cost = product.Costs.Where(m => m.BreakPoint <= quantity && m.CurrencyID == currencyID).OrderByDescending(m => m.BreakPoint).Select(m => m.Cost).First(); }
                catch { }
            }

            product.Price = price;
            product.Cost = cost;

            return product;
        }

        
        public DataFileOutput Export(string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            return Export(CompanyID, filterText, category, supplierID, deleted);
        }

        public DataFileOutput Export(Guid? companyID, string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            var products = Search(companyID, filterText, category, supplierID, deleted)
                                .Include(p => p.Currency)
                                .Include(p => p.Prices.Select(c => c.Currency))
                                .OrderBy(p => p.ProductCode)
                                .ThenBy(p => p.Description)
                                .AsNoTracking();
            var list = products.ToList();
            var customFields = new CustomProductFieldService<EmptyViewModel>(CompanyID, null, null, DbContext).Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();


            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            int x = 0;
            
            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var product in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(product.ProductCode); x++;
                row.CreateCell(x).SetCellValue(product.Description); x++;
                row.CreateCell(x).SetCellValue(product.DetailedDescription); x++;
                row.CreateCell(x).SetCellValue(product.Category); x++;
                row.CreateCell(x).SetCellValue(product.Unit); x++;
                //row.CreateCell(x).SetCellValue(product.VendorCode); x++;
                //row.CreateCell(x).SetCellValue(product.Manufacturer); x++;
                row.CreateCell(x).SetCellValue(product.AccountCode); x++;
                row.CreateCell(x).SetCellValue(product.PurchaseAccountCode); x++;
                row.CreateCell(x).SetCellValue((double)product.VAT ); x++;
                if (!product.UsePriceBreaks) { row.CreateCell(x).SetCellValue(product.Currency.Code); } x++;
                if (!product.UsePriceBreaks) { row.CreateCell(x).SetCellValue((double)product.Price); row.GetCell(x).CellStyle = numberStyle; } x++;
                if (!product.UsePriceBreaks) { row.CreateCell(x).SetCellValue((double)product.Cost); row.GetCell(x).CellStyle = numberStyle; } x++;
                row.CreateCell(x).SetCellValue(product.IsKit ? "True" : "False"); x++;
                row.CreateCell(x).SetCellValue(product.IsBought ? "True" : "False"); x++;
                row.CreateCell(x).SetCellValue(product.UsePriceBreaks ? "True" : "False"); x++;

                product.CustomFieldValues = product.CustomFieldValues.AddRemoveRelevent(customFields);
                foreach(var field in product.CustomFieldValues) { row.CreateCell(x).SetCellValue(field.Value); x++; }
            }

            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("ProductCode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Description"); x++;
            headerRow.CreateCell(x).SetCellValue("DetailedDescription"); x++;
            headerRow.CreateCell(x).SetCellValue("Category"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Unit"); sheet.AutoSizeColumn(x); x++;
            //headerRow.CreateCell(x).SetCellValue("VendorCode"); sheet.AutoSizeColumn(x); x++;
            //headerRow.CreateCell(x).SetCellValue("Manufacturer"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("AccountCode(Sales)"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("AccountCode(Purchase)"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("VATRate"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Currency"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Price"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Cost"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("IsKit"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("IsBoughtIn"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("UsePriceBreaks"); sheet.AutoSizeColumn(x); x++;

            foreach (var header in customFields) { headerRow.CreateCell(x).SetCellValue(header.Name); x++; }
           
            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("{0}Export{1}.xls", ClassName, DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };
        }

        public DataFileOutput ExportImages(string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            return ExportImages(CompanyID, filterText, category, supplierID, deleted);
        }

        public DataFileOutput ExportImages(Guid? companyID, string filterText, string category = "", Guid? supplierID = null, bool deleted = false)
        {
            if (!companyID.HasValue) return null;

            var products = Search(companyID, filterText, category, supplierID, deleted).Include(i => i.Images).Include(i => i.Images.Select(x => x.File));
            List<string> filePathList = new List<string>();
            List<string> fileNameList = new List<string>();

            foreach (var pdt in products)
            {
                for(int x = 0; x < pdt.Images.Count; x++)
                {
                    filePathList.Add(pdt.Images.OrderBy(i => i.SortPos).ElementAt(x).File.FilePath);
                    fileNameList.Add((x <= 0) ? pdt.ProductCode : string.Format("{0} ({1})", pdt.ProductCode, x));
                }
            }


            using (var ms = new MemoryStream())
            {
                using (var zipStream = new ZipOutputStream(ms))
                {
                    string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                    Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));

                    for (int x = 0; x < filePathList.Count; x++)
                    {
                        byte[] fileBytes = null;
                        try
                        {
                            fileBytes = System.IO.File.ReadAllBytes(FileService.ToPhysicalPath(filePathList[x]));
                            var extension = Path.GetExtension(filePathList[x]);
                            var fileEntry = new ZipEntry(r.Replace(fileNameList[x] + extension, ""))
                            {
                                Size = fileBytes.Length
                            };
                            zipStream.PutNextEntry(fileEntry);
                            zipStream.Write(fileBytes, 0, fileBytes.Length);
                        }
                        catch { }
                    }

                    // Finish the ZIP file  
                    zipStream.Finish();

                    ms.Position = 0;

                    var fileOutput = new DataFileOutput
                    {
                        DataArray = ms.ToArray(), //The binary data of the XLS file
                        MimeType = "application/zip", //MIME type of Excel files
                        Filename = string.Format("ProductImages{0}.zip", DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
                    };

                    zipStream.Flush();
                    zipStream.Close();

                    return fileOutput;
                }
                
                
            }

        }

        public override Product Destroy(Product entity)
        {
            DestroySet(Read(CompanyID).Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<Product> Destroy(IEnumerable<Product> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(CompanyID).Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<Product> set)
        {

            var IDs = set.Select(i => i.ID).ToArray();

            var imageService = new GenericService<ProductImage>(DbContext);
            var priceService = new GenericService<ProductPrice>(DbContext);
            var costService = new GenericService<ProductCost>(DbContext);
            var kitItemService = new GenericService<ProductKitItem>(DbContext);
            var pdtAltService = new GenericService<ProductAlternative>(DbContext);
            var pdtAssoService = new GenericService<ProductAssociative>(DbContext);

            imageService.Destroy(imageService.Read().Where(i => IDs.Contains(i.OwnerID)).ToList());
            priceService.Destroy(priceService.Read().Where(i => IDs.Contains(i.ProductID)).ToList());
            costService.Destroy(costService.Read().Where(i => IDs.Contains(i.ProductID)).ToList());
            kitItemService.Destroy(kitItemService.Read().Where(i => IDs.Contains(i.ProductID.Value) || IDs.Contains(i.KitID)).ToList());
            pdtAltService.Destroy(pdtAltService.Read().Where(i => IDs.Contains(i.ProductID) || IDs.Contains(i.LinkedProductID)).ToList());
            pdtAssoService.Destroy(pdtAssoService.Read().Where(i => IDs.Contains(i.ProductID) || IDs.Contains(i.LinkedProductID)).ToList());
            
            base.Destroy(set.ToList());
        }


    }


}
