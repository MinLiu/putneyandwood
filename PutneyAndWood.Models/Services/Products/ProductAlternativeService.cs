﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ProductAlternativeService<TViewModel> : ChildEntityService<Product, ProductAlternative, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Product", "LinkedProduct" }; } }

        public ProductAlternativeService(Guid CompanyID, Guid? ProductID, string UserId, ModelMapper<ProductAlternative, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ProductID, UserId, mapper, dbContext)
        {
            
        }

        public override IQueryable<ProductAlternative> Read(Guid? ParentID)
        {
            return base.Read(ParentID).Where(p => p.Product.Deleted == false  && p.Product.IsKit == false);
        }


        public override void OnCreate(ProductAlternative model)
        {
            AddLog(model, string.Format("Alternative product '{1}' added to Product '{{0}}' alternatives.", 0, model.ToShortLabel()));
        }

        public override void OnUpdate(ProductAlternative model)
        {
            AddLog(model, string.Format("Alternative  product '{1}' updated on product '{{0}}' alternatives.", 0, model.ToShortLabel()));
        }

        public override void OnDestroy(ProductAlternative model)
        {
            AddLog(model, string.Format("Alternative product '{1}' removed from product '{{0}}' alternatives.", 0, model.ToShortLabel()));
        }


    }


}
