﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{

    public class FileServiceOld
    {
        public static string WebURL = null;
        public static string ServerFilePath = null;
        private static string ContentFolderRoot = "/Uploads/";


        private FileServiceOld() { }

        public static string ToPhysicalPath(string virtualPath)
        {
            if (virtualPath.Contains(ServerFilePath))
            {
                return virtualPath;
            }
            else if (virtualPath.StartsWith("~/"))
            {
                var regex = new Regex(Regex.Escape("~/"));
                var corrected = virtualPath.Replace("~/", "/");
                return regex.Replace(corrected.Replace('/', '\\'), ServerFilePath, 1);
            }
            else if (virtualPath.StartsWith("/"))
            {
                var regex = new Regex(Regex.Escape("\\"));
                return regex.Replace(virtualPath.Replace('/', '\\'), ServerFilePath, 1);
            }
            else if (!string.IsNullOrEmpty(WebURL) || virtualPath.StartsWith(WebURL))
            {
                var regex = new Regex(Regex.Escape(WebURL));
                return regex.Replace(virtualPath.Replace('/', '\\'), ServerFilePath, 1);
            }
            else
            {
                return string.Format("{0}\\{1}", ServerFilePath, virtualPath.Replace('/', '\\'));
            }                

        }

        public static string ToVirtualPath(string physicalPath)
        {
            var regex = new Regex(Regex.Escape(ServerFilePath));
            return regex.Replace(physicalPath.Replace('\\', '/'), "/", 1);
        }


        public static string SaveFile(Guid companyID, System.Web.HttpPostedFileBase file, string subFolder = "/", bool genSaveFilename = true)
        {
            if (file == null) throw new Exception("Save file can not be null.");

            var virtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);

            var physicalPath = ToPhysicalPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var extension = Path.GetExtension(file.FileName);
            var str = (genSaveFilename) ? Guid.NewGuid().ToString() + extension : Path.GetFileNameWithoutExtension(file.FileName) + extension;
            var physicalSavePath = ToPhysicalPath(virtualPath) + str;
            file.SaveAs(physicalSavePath);
            return virtualPath + str;
        }
        

        public static string SaveFile(Guid companyID, byte[] bytes, string name, string extension, string subFolder = "/", bool genSaveFilename = true)
        {            
            var virtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);

            var physicalPath = ToPhysicalPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            if (!extension.Contains('.')) extension = "." + extension;
            var str = (genSaveFilename) ? Guid.NewGuid().ToString() + extension : name + extension;
            var physicalSavePath = ToPhysicalPath(virtualPath) + str;
            _SaveBytesToFile(bytes, physicalSavePath);
            return virtualPath + str;
        }


        public static bool DeleteFile(string virtualpath)
        {            
            try
            {
                var physicalPath = ToPhysicalPath(virtualpath);
                if (File.Exists(physicalPath))
                {
                    File.Delete(physicalPath);
                    return true;
                }
                else { return false; }
            }
            catch { return false; }
        }


        public static string CopyFile(Guid companyID, string sourceVirtualPath, string subFolder = "/", string filename = "")
        {           
            var physicalPath = ToPhysicalPath(sourceVirtualPath);
            var copyVirtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);
            var copyPhysicalPath = ToPhysicalPath(copyVirtualPath);

            if (!Directory.Exists(copyPhysicalPath))
            {
                Directory.CreateDirectory(copyPhysicalPath);
            }

            var extension = Path.GetExtension(sourceVirtualPath);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;
            copyPhysicalPath += str;

            File.Copy(physicalPath, copyPhysicalPath, true);

            return copyVirtualPath + str;
        }


        public static bool DoesFileExists(Guid companyID, string subFolder = "/", string filename = "")
        {           
            var virtualPath = string.Format("{0}{1}{2}{3}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename);

            return (File.Exists(virtualPath));
        }


        public static string GetUploadVirtualFilePath(Guid companyID, string subFolder = "/", string filename = "")
        {            
            return string.Format("{0}{1}{2}{3}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename);
        }

        public static string GetUploadPhysicalFilePath(Guid companyID, string subFolder = "/", string filename = "")
        {
            return ToPhysicalPath(GetUploadVirtualFilePath(companyID, (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename));
        }


        private static bool _SaveBytesToFile(byte[] bytes, string filename) 
        {
            try
            {
                using (FileStream file = new FileStream(filename, FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(bytes, 0, bytes.Length);
                }
                return true;
            }
            catch { return false; }
        }


    }
    

}
