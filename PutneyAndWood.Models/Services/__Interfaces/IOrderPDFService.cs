﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IOrderPDFService<T>
        where T : class, IEntity
    {
        byte[] GeneratePDF(Guid id, ref string filename);
        byte[] GenerateBodyPDF(Guid id, string templateFilePath);
        byte[] GenerateBodyPDF(T model, string templateFilePath);
        byte[] GenerateBodyPDF(T model, Dictionary<string, string> dictionary, string templateFilePath);

        Dictionary<string, string> CreateDictionary(T model);
        Dictionary<string, string> CreateDictionaryImages(T model);

    }
    

}
