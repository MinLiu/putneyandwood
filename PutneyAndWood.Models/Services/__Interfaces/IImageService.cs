﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Data.Entity;
using System.Web;

namespace SnapSuite.Internal
{

    public interface IImageService<TImage>
         where TImage : class
    {
        TImage FindByID(Guid ID);
        TImage UploadImage(Guid ownerID, HttpPostedFileBase uploadedImage);
        TImage UploadImage(Guid ownerID, byte[] uploadedImage, string filename, string fileType = "image");
        bool UpdatePositions(Guid itemID, int oldPosition, int newPosition);
    }
}
