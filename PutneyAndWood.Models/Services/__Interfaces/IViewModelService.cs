﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Data.Entity;

namespace SnapSuite.Internal
{

    public interface IViewModelService<TModel, TViewModel>
        where TModel : class
        where TViewModel : class
    {

        TViewModel Create(TViewModel viewModel);

        IEnumerable<TViewModel> Create(IEnumerable<TViewModel> viewModels);

        TViewModel Update(TViewModel viewModel, string[] updatedColumns = null);

        IEnumerable<TViewModel> Update(IEnumerable<TViewModel> viewModels, string[] updatedColumns = null);

        TViewModel Destroy(TViewModel viewModel);

        IEnumerable<TViewModel> Destroy(IEnumerable<TViewModel> viewModels);

        TModel MapToModel(TViewModel viewModel);

        TViewModel MapToViewModel(TModel model);

        IEnumerable<TModel> MapToModel(IEnumerable<TViewModel> viewModels);

        IEnumerable<TViewModel> MapToViewModel(IEnumerable<TModel> models);
    }

}
