﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Data.Entity;

namespace SnapSuite.Internal
{

    public interface IService<T>
        where T : class
    {
        
        T Create(T entity);

        IEnumerable<T> Create(IEnumerable<T> entities);
        
        IQueryable<T> Read();

        T Find(params object[] keyValues);

        T Update(T entity, string[] updatedColumns = null);

        IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null);
        T Destroy(T entity);

        IEnumerable<T> Destroy(IEnumerable<T> entities);

        T Deattach(T entity);

        IEnumerable<T> Deattach(IEnumerable<T> entities);

        void Dispose();
    }
    

}
