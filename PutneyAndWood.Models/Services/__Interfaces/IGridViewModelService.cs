﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Data.Entity;

namespace SnapSuite.Internal
{

    public interface IGridViewModelService<TModel, TGridViewModel>
        where TModel : class
        where TGridViewModel : class
    {

        TModel MapToModel(TGridViewModel viewModel);

        IEnumerable<TModel> MapToModel(IEnumerable<TGridViewModel> viewModels);

        TGridViewModel MapToGridViewModel(TModel model);

        IEnumerable<TGridViewModel> MapToGridViewModel(IEnumerable<TModel> models);
    }

}
