﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Internal
{

    public interface IDeletableService<TModel, TViewModel>
        where TModel : class
        where TViewModel : class
    {

        TModel Delete(Guid ID);

        TModel Delete(TViewModel viewModel);
        
        TModel Delete(TModel model);
        
        IEnumerable<Guid> Delete(IEnumerable<Guid> ids);
        
        IEnumerable<TViewModel> Delete(IEnumerable<TViewModel> viewModels);
        
        IEnumerable<TModel> Delete(IEnumerable<TModel> models);

        TModel Restore(Guid ID);

        TModel Restore(TViewModel viewModel);

        TModel Restore(TModel model);

        IEnumerable<Guid> Restore(IEnumerable<Guid> ids);

        IEnumerable<TViewModel> Restore(IEnumerable<TViewModel> viewModels);

        IEnumerable<TModel> Restore(IEnumerable<TModel> models);
        
        bool ClearDeleted();
    }
    

}
