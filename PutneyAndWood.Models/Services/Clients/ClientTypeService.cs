﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientTypeService<TViewModel> : OrganisationTypeService<ClientType, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Client Tag"; }
        public override string LoggingNamePlural() { return "Client Tags"; }

        public ClientTypeService(Guid CompanyID, string UserId, ModelMapper<ClientType, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {

        }


        public override ClientType Destroy(ClientType entity)
        {
            DestroySet(Read().Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<ClientType> Destroy(IEnumerable<ClientType> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read().Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<ClientType> set)
        {
            set = set.Include(x => x.Clients);
            DbSet().RemoveRange(set);
            DbContext.SaveChanges();
        }


    }


}
