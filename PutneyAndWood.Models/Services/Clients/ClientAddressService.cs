﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientAddressService<TViewModel> : OrganisationAddressService<Client, ClientAddress, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
              
        public ClientAddressService(Guid CompanyID, Guid? ClientID, string UserId, ModelMapper<ClientAddress, TViewModel> mapper, DbContext db)
            : base(CompanyID, ClientID, UserId, mapper, db)
        {

        }


        protected override IQueryable<ClientAddress> GetAddresses(Guid CompanyID, string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            var cltIDs = new ClientService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).Search(filterText, type, tag, deleted).Select(p => p.ID).ToArray();
            return Read(null).Where(k => cltIDs.Contains(k.ClientID))
                                      .Include(i => i.Client)
                                      .OrderBy(i => i.Client.Name)
                                      .ThenBy(i => i.Name)
                                      .ThenBy(i => i.Address1);
        }


        public override ClientAddress Destroy(ClientAddress entity)
        {
            DestroySet(Read(null).Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<ClientAddress> Destroy(IEnumerable<ClientAddress> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(null).Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<ClientAddress> set)
        {
            set = set.Include(x => x.DeliveryQuotations)
                     .Include(x => x.InvoiceQuotations);

            DbSet().RemoveRange(set);
            DbContext.SaveChanges();
        }

    }


}
