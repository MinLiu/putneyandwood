﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientAttachmentService<TViewModel> : OrganisationAttachmentService<Client, ClientAttachment, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
       
        public ClientAttachmentService(Guid CompanyID, Guid? ClientID, string UserId, ModelMapper<ClientAttachment, TViewModel> mapper, DbContext db)
            : base(CompanyID, ClientID, UserId, mapper, db)
        {

        }
        
    }


}
