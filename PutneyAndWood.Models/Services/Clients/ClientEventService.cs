﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientEventService<TViewModel> : OrganisationEventService<Client, ClientEvent, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
              
        public ClientEventService(Guid CompanyID, Guid? ClientID, string UserId, ModelMapper<ClientEvent, TViewModel> mapper, DbContext db)
            : base(CompanyID, ClientID, UserId, mapper, db)
        {

        }

        public override IQueryable<ClientEvent> Read(Guid CompanyID, Guid? ParentID)
        {
            return base.Read(CompanyID, ParentID)
                        .Include(i=> i.Client)
                        .Include(t => t.ClientContact)
                        .Include(t => t.AssignedUser)
                        .Include(t => t.Creator)
                        .Include(t => t.Client);
        }


        public override ClientEvent Create(ClientEvent entity)
        {
            if (string.IsNullOrEmpty(entity.CreatorID))
                entity.CreatorID = CurrentUserID;

            return base.Create(entity);
        }


        public override IEnumerable<ClientEvent> Create(IEnumerable<ClientEvent> entities)
        {
            foreach(var en in entities)
            {
                if (string.IsNullOrEmpty(en.CreatorID))
                    en.CreatorID = CurrentUserID;
            }

            return base.Create(entities);
        }


        public override ClientEvent Update(ClientEvent entity, string[] updatedColumns = null)
        {
            if (new GenericService<ClientContact>(DbContext).Read().Where(c => c.ID == entity.ClientContactID && c.ClientID == entity.ClientID).Count() <= 0)
            {
                entity.ClientContactID = null;
            }

            return base.Update(entity, updatedColumns);
        }


        public override IEnumerable<ClientEvent> Update(IEnumerable<ClientEvent> entities, string[] updatedColumns = null)
        {
            foreach(var entity in entities)
            {
                if (new GenericService<ClientContact>(DbContext).Read().Where(c => c.ID == entity.ClientContactID && c.ClientID == entity.ClientID).Count() <= 0)
                {
                    entity.ClientContactID = null;
                }
            }
            return base.Update(entities, updatedColumns);
        }
        
    }


}
