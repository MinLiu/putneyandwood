﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientContactService<TViewModel> : OrganisationContactService<Client, ClientContact, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        //public override string[] ReferencesToInclude { get { return new string[] { "Client" }; } }

        public ClientContactService(Guid CompanyID, Guid? ClientID, string UserId, ModelMapper<ClientContact, TViewModel> mapper, DbContext db)
            : base(CompanyID, ClientID, UserId, mapper, db)
        {

        }


        public IQueryable<ClientContact> Search(Guid companyID, string filterText)
        {
            return base.Read(null).Where(p => p.Client.CompanyID == companyID
                    && (
                            ((p.FirstName + " " + p.LastName).ToLower().Contains(filterText.ToLower())
                                || p.Position.ToLower().Contains(filterText.ToLower())
                                || p.Email.ToLower().Contains(filterText.ToLower())
                                || p.Mobile.ToLower().Contains(filterText.ToLower())
                                || p.Telephone.ToLower().Contains(filterText.ToLower())
                            ) || filterText.Equals("")
                        )
                    && p.Client.Deleted == false
                    )
                    .Include(p => p.Client)
                    .OrderBy(o => o.Client.Name)
                    .ThenBy(o => o.FirstName)
                    .ThenBy(o => o.LastName);
        }



        protected override IQueryable<ClientContact> GetContacts(Guid CompanyID, string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            var cltIDs = new ClientService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).Search(filterText, type, tag, deleted).Select(p => p.ID).ToArray();
            return Read(null).Where(k => cltIDs.Contains(k.ClientID))
                                      .Include(i => i.Client)
                                      .OrderBy(i => i.Client.Name)
                                      .ThenBy(i => i.Title)
                                      .ThenBy(i => i.FirstName)
                                      .ThenBy(i => i.LastName);
        }


        public override ClientContact Destroy(ClientContact entity)
        {
            DestroySet(Read(null).Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<ClientContact> Destroy(IEnumerable<ClientContact> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(null).Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<ClientContact> set)
        {
            set = set.Include(x => x.ClientEvents)
                     .Include(a => a.Quotations);

            DbSet().RemoveRange(set);
            DbContext.SaveChanges();
        }

    }
    
}
