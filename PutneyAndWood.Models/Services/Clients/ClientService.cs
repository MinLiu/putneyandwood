﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class ClientService<TViewModel> : OrganisationService<Client, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public ClientService(Guid CompanyID, string UserId, ModelMapper<Client, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {
            
        }


        public override IQueryable<Client> Read(Guid CompanyID)
        {
            return base.Read(CompanyID)
                       .Include(i => i.ClientTagItems)
                       .Include(i => i.Type);
        }

        
        public override IQueryable<Client> Search(string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            var query = Read(deleted, CompanyID);

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                filterText = filterText.ToLower().Trim();
                query = query.Where(c =>
                    c.Name.ToLower().Contains(filterText)
                    || c.Town.ToLower().Contains(filterText)
                    || c.Country.ToLower().Contains(filterText)
                    || c.Email.ToLower().Contains(filterText)
                    || c.Website.ToLower().Contains(filterText)
                    || c.Telephone.ToLower().Contains(filterText)
                    || c.ClientContacts.Any(i => (i.FirstName + " " + i.LastName).ToLower().Contains(filterText))
                );
            }

            if (type != null && type != Guid.Empty)
                query = query.Where(c => c.TypeID == type);
            
            if (tag != null && tag != Guid.Empty)
                query = query.Where(c => c.ClientTagItems.Any(t => t.ClientTag.ID == tag));

            return query.OrderBy(c => c.Name);
        }




        public override Client Destroy(Client entity)
        {            
            DestroySet(Read(true).Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<Client> Destroy(IEnumerable<Client> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(true).Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<Client> set)
        {

            var IDs = set.Select(i => i.ID).ToList();

            var adressService = new ClientAddressService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            var attachService = new ClientAttachmentService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            var contactService = new ClientContactService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            var tagItemService = new ClientTagItemService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            var eventService = new ClientEventService<EmptyViewModel>(CompanyID, null, null, null, DbContext);

            adressService.Destroy(adressService.Read().Where(i => IDs.Contains(i.ClientID)).ToList());
            attachService.Destroy(attachService.Read().Where(i => IDs.Contains(i.ClientID)).ToList());
            contactService.Destroy(contactService.Read().Where(i => IDs.Contains(i.ClientID)).ToList());
            tagItemService.Destroy(tagItemService.Read().Where(i => IDs.Contains(i.ClientID)).ToList());
            eventService.Destroy(eventService.Read().Where(i => IDs.Contains(i.ClientID.Value)).ToList());


            set = set.Include(a => a.Quotations);

            base.Destroy(set.ToList());
        }

    }
}
