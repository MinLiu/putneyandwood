﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientTagService<TViewModel> : OrganisationTagService<ClientTag, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public ClientTagService(Guid CompanyID,string UserId, ModelMapper<ClientTag, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {

        }


        public override ClientTag Destroy(ClientTag entity)
        {
            DestroySet(Read().Where(i => i.ID == entity.ID).Select(i => i.ID).ToArray());
            return entity;
        }

        public override IEnumerable<ClientTag> Destroy(IEnumerable<ClientTag> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read().Where(i => ids.Contains(i.ID)).Select(i => i.ID).ToArray());
            return entities;
        }

        private void DestroySet(ICollection<Guid> IDs)
        {
            var tagService = new ClientTagItemService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            tagService.Destroy(tagService.Read().Where(i => IDs.Contains(i.ClientTagID)).ToList());
        }

    }


}
