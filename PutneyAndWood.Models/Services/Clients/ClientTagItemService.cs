﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ClientTagItemService<TViewModel> : OrganisationTagItemService<Client, ClientTagItem, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "ClientTag" }; } }

        public ClientTagItemService(Guid CompanyID, Guid? ClientID, string UserId, ModelMapper<ClientTagItem, TViewModel> mapper, DbContext db)
            : base(CompanyID, ClientID, UserId, mapper, db)
        {

        }
        

    }


}
