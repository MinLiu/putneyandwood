﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service for child entities with a one to many relationship between the parent and the children. All classes should use the IEntity interface.
    /// To correctly use the  service, the reference ID in the child entity should be in the format "{Parent Class Name}ID". 
    /// But the parent column name can be overwritten.
    /// </summary>
    /// <typeparam name="TParent">Parent entity class. Class should use the IEntity interface.</typeparam>
    /// <typeparam name="T">Child entity class. Class should use the IEntity interface.</typeparam>
    /// <typeparam name="TViewModel">Child entity view model class. Class should use the IEntityViewModel interface.</typeparam>
    public class ChildEntityService<TParent, T, TViewModel> : EntityService<T, TViewModel>
        where TParent : class, IEntity, new()
        where T : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string ParentName { get { return typeof(TParent).Name; } }
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string ParentIdColumn { get { return string.Format("{0}ID", typeof(TParent).Name); } }


        // <summary> The Company ID value. Leave Empty to avoid parent company ID check</summary>
        public virtual Guid CompanyID { get; set; }
        public virtual bool ApplyParentCompanyID { get { return true; } }

        /// <summary> The parent ID value. Leave null to get all child entities regardless of parent ID.</summary>
        public virtual Guid? ParentID { get; set; }
        

        public ChildEntityService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(UserID, mapper, db)
        {
            this.CompanyID = CompanyID;
            this.ParentID = ParentID;
        }

        /// <summary>Read the entries from the database context. ParentID is automatically set if not null.</summary>
        /// <returns>A query of the items.</returns>
        public override IQueryable<T> Read()
        {
            return Read(ParentID);
        }


        /// <summary>Read the entries from the database context.</summary>
        /// <param name="ParentID">The Parent reference ID.</param>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<T> Read(Guid? ParentID)
        {
            var query = base.Read();

            if(ParentID.HasValue && ParentID.Value != Guid.Empty)
                query = base.Read().Where(string.Format("{0}.Equals(Guid(\"{1}\"))", ParentIdColumn, ParentID.Value));
            else
                query = base.Read();

            if (CompanyID != Guid.Empty && ApplyParentCompanyID)
                query = query.Where(string.Format("{0}.CompanyID.Equals(Guid(\"{1}\"))", ParentName, CompanyID));
           // else
                return query;

        }

        // <summary>Create entity in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override T Create(T entity)
        {
            SetParentIdIfEmpty(entity);
            return base.Create(entity);
        }
        

        /// <summary>Create entities in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach(var en in entities)
            {
                SetParentIdIfEmpty(en);
            }
            return base.Create(entities);
        }


        /// <summary>Method called during create function. Sets the parent ID if null or empty.</summary>
        protected virtual void SetParentIdIfEmpty(T entity)
        {
            var pInfo = entity.GetType().GetProperty(ParentIdColumn);
            if (pInfo != null)
            {
                if ((Guid)pInfo.GetValue(entity, null) == Guid.Empty && ParentID.HasValue)
                    pInfo.SetValue(entity, ParentID.Value, null);
            }
        }


        /// <summary>Method called after create operation is called. Used for logging purposes.</summary>
        public override void OnCreate(IEnumerable<T> models)
        {
            //Do not log anything
            //No accurate way to determine if models belong to different parents.
            return;
        }

        /// <summary>Method called after create operation is called. Used for logging purposes.</summary>
        public override void OnCreate(T model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(model, string.Format("{1} '{2}' added to {{0}}.", 0, LoggingName(), model.ToShortLabel())); }
            catch { }
        }

        /// <summary>Method called after update operation is called. Used for logging purposes.</summary>
        public override void OnUpdate(IEnumerable<T> models)
        {
            //Do not log anything
            //No accurate way to determine if models belong to different parents.
            return;
        }

        /// <summary>Method called after update operation is called. Used for logging purposes.</summary>
        public override void OnUpdate(T model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(model, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), model.ToShortLabel())); } catch { }
        }

        /// <summary>Method called after destroy operation is called. Used for logging purposes.</summary>
        public override void OnDestroy(IEnumerable<T> models)
        {
            //Do not log anything
            //No accurate way to determine if models belong to different parents.
            return;
        }

        /// <summary>Method called after destroy operation is called. Used for logging purposes.</summary>
        public override void OnDestroy(T model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(model, string.Format("{1} '{2}' permanently deleted from {{0}}.", 0, LoggingName(), model.ToShortLabel()) ); } catch { }
        }


        /// <summary>
        /// Add log using using Activity Log Service. 
        /// String tag {0} will be automatically replaced with the parent short lable string.
        /// </summary>
        /// <param name="message"> Log text. {0} will be automatically replaced with the parent short label string.</param>
        public virtual bool AddLog(T model, string message)
        {
            if (model == null) return false;
            
            var propInfo = model.GetType().GetProperty(ParentIdColumn);
            Guid? Id = propInfo.GetValue(model, null) as Guid?;
            var parent = new GenericService<TParent>(DbContext).Find(Id.Value);
            if (parent == null) return false;
            AddLog(string.Format(message, parent.ToShortLabel()), CurrentUserID, parent.ID.ToString());

            return true;
        }

    }

}
