﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service for entities consisting of a CompanyID. All classes should use the IEntity and ICompanyEntity interface.
    /// The CompanyID in the service can never be null. This prevents querying the database context for items the don't belogn to a company by mistake.
    /// </summary>
    /// <typeparam name="TModel">Entity class should use the IEntity and ICompanyEntity interface.</typeparam>
    /// <typeparam name="TViewModel">Entity view model class. Class should use the IEntityViewModel interface.</typeparam>
    public class CompanyEntityService<TModel, TViewModel> : EntityService<TModel, TViewModel>
        where TModel : class, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        // <summary> The Company ID value.</summary>
        public virtual Guid CompanyID { get; set; }


        public CompanyEntityService(Guid CompanyID, string UserId, ModelMapper<TModel, TViewModel> mapper, DbContext db)
            : base(UserId, mapper, db)
        {
            this.CompanyID = CompanyID;
        }

        /// <summary>Find the entity from the database context using ID. Will query items with the set company ID. </summary>
        /// <param name="ID">ID of entity</param>
        /// <returns>The first entity from the database context. Null if not found.</returns>
        public override TModel FindByID(Guid ID)
        { 
            //Only return the company items if the companyID is not null
            if(CompanyID != Guid.Empty)
                return base.Read().Where(i => i.CompanyID == CompanyID & i.ID == ID).DefaultIfEmpty(null).FirstOrDefault();
            else
                return base.Read().Where(i => i.ID == ID).DefaultIfEmpty(null).FirstOrDefault();
        }

        /// <summary>Read the entries from the database context. Will query items with the set company ID.</summary>
        /// <returns>A query of the items.</returns>
        public override IQueryable<TModel> Read()
        {
            return this.Read(this.CompanyID);
        }

        /// <summary>Read the entries from the database context. Will query items with the passed company ID.</summary>
        /// <param name="CompanyID">ID of Company</param>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<TModel> Read(Guid CompanyID)
        {
            //Only return the company items if the companyID is not null
            if(CompanyID != Guid.Empty)
                return base.Read().Where(m => m.CompanyID == CompanyID);
            else
                return base.Read();
        }


        /// <summary>Create entity in database context.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override TModel Create(TModel entity)
        {
            if (entity.CompanyID == Guid.Empty) //Set company ID of entity is empty or null
                entity.CompanyID = CompanyID;

            return base.Create(entity);
        }

        /// <summary>Create entities in database context.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override IEnumerable<TModel> Create(IEnumerable<TModel> entities)
        {
            foreach(var en in entities) //Set company ID of entities if empty or null
            {
                if (en.CompanyID == Guid.Empty)
                    en.CompanyID = CompanyID;
            }

            return base.Create(entities);
        }
        
    }

}
