﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service for child entities, consisting of a CompanyID, with a one to many relationship between the parent and the children. All classes should use the IEntity and ICompanyEntity interface.
    /// To correctly use the  service, the reference ID in the child entity should be in the format "{Parent Class Name}ID". 
    /// But the parent column name can be overwritten.
    /// </summary>
    /// <typeparam name="TParent">Parent entity class. Class should use the IEntity interface.</typeparam>
    /// <typeparam name="T">Child entity class. Class should use the IEntity interface.</typeparam>
    /// <typeparam name="TViewModel">Child entity view model class. Class should use the IEntityViewModel interface.</typeparam>
    public class ChildCompanyEntityService<TParent, T, TViewModel> : ChildEntityService<TParent, T, TViewModel>
        where TParent : class, IEntity, new()
        where T : class, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
       
        public ChildCompanyEntityService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(CompanyID, ParentID, UserID, mapper, db)
        {
            
        }


        /// <summary>Read the entries from the database context that being to the CompanyID. ParentID is automatically set if not null.</summary>
        /// <returns>A query of the items.</returns>
        public override IQueryable<T> Read()
        {
            return Read(CompanyID, ParentID);
        }


        /// <summary>Read the entries from the database context.</summary>
        /// <param name="CompanyID">The Company reference ID.</param>
        /// <param name="ParentID">The Parent reference ID.</param>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<T> Read(Guid CompanyID, Guid? ParentID)
        {
            return base.Read(ParentID).Where(i => i.CompanyID == CompanyID);
        }


        /// <summary>Method called during create function. Sets the parent ID and comapny ID if null or empty</summary>
        protected override void SetParentIdIfEmpty(T entity)
        {
            base.SetParentIdIfEmpty(entity);
            if (entity.CompanyID == Guid.Empty)
                entity.CompanyID = this.CompanyID;
        }
        
    }

}
