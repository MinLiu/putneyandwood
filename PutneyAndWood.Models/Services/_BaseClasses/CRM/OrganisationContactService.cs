﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class OrganisationContactService<TParent, T, TViewModel> : ChildEntityService<TParent, T, TViewModel>
        where TParent : class, IOrganisation, ICompanyEntity, IEntity, IDeletable, new()
        where T : class, IContact, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Contact"; }
        public override string LoggingNamePlural() { return "Contacts"; }

        public OrganisationContactService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserId, mapper, dbContext)
        {
            
        }

        
        protected virtual IQueryable<T> GetContacts(Guid CompanyID, string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            return new List<T>().AsQueryable();
        }


        public DataFileOutput Export(string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            if (CompanyID == Guid.Empty) throw new Exception("CompanyID has not been set.");

            var contacts = GetContacts(CompanyID, filterText, type, tag, deleted);

            var list = contacts.ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            int x = 0;

            

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var contact in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                //row.CreateCell(x).SetCellValue(address.Client.Name); x++;
                row.CreateCell(x).SetCellValue(contact.OwnerName); x++;
                row.CreateCell(x).SetCellValue(contact.Title); x++;
                row.CreateCell(x).SetCellValue(contact.FirstName); x++;
                row.CreateCell(x).SetCellValue(contact.LastName); x++;
                row.CreateCell(x).SetCellValue(contact.Position); x++;
                row.CreateCell(x).SetCellValue(contact.Telephone); x++;
                row.CreateCell(x).SetCellValue(contact.Mobile); x++;
                row.CreateCell(x).SetCellValue(contact.Email); x++;
            }

            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue(typeof(TParent).Name); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Title"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("FirstName"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("LastName"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Position"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Telephone"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Mobile"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Email"); sheet.AutoSizeColumn(x);  x++;

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("{0}ContactsExport{1}.xls", typeof(TParent).Name, DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };

        }
        

    }


}
