﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrganisationTagItemService<TParent, T, TViewModel> : ChildEntityService<TParent, T, TViewModel>
        where TParent : class, IOrganisation, IEntity, new()
        where T : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Tag"; }
        public override string LoggingNamePlural() { return "Tag"; }


        public OrganisationTagItemService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserId, mapper, dbContext)
        {
            
        }
        
    }


}
