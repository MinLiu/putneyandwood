﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrganisationService<T, TViewModel> : CompanyEntityDeletableService<T, TViewModel>
        where T : class, IOrganisation, IDeletable, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        

        public OrganisationService(Guid CompanyID, string CurrentUserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, CurrentUserID, mapper, dbContext)
        {

        }

        public OrganisationService(Guid CompanyID, string CurrentUserID, DbContext dbContext)
            : base(CompanyID, CurrentUserID, null, dbContext)
        {

        }


        public virtual IQueryable<T> Search(string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            return Read(deleted, CompanyID).Where(c =>
                (c.Name.ToLower().Contains(filterText.ToLower())
                    || c.Town.ToLower().Contains(filterText.ToLower())
                    || c.Country.ToLower().Contains(filterText.ToLower())
                    || c.Email.ToLower().Contains(filterText.ToLower())
                    || c.Website.ToLower().Contains(filterText.ToLower())
                    || c.Telephone.ToLower().Contains(filterText.ToLower())
                ) || string.IsNullOrEmpty(filterText)
                &&  (c.TypeID == type || type == Guid.Empty || type == null)
            )
            .OrderBy(c => c.Name);
        }



        public override T Create(T entity)
        {
            entity.Created = DateTime.Now;
            return base.Create(entity);
        }

        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach(var entity in entities)
            {
                entity.Created = DateTime.Now;
            }

            return base.Create(entities);
        }
        

        public DataFileOutput Export(string filterText, Guid? type, Guid? tag, bool deleted = false)
        {

            var orgs = Search(filterText, type, tag, deleted).Include("Type").OrderBy(c => c.Name);
            var list = orgs.ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            int x = 0;
            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var org in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(org.Name); x++;
                row.CreateCell(x).SetCellValue(org.TypeName()); x++;
                row.CreateCell(x).SetCellValue(org.Address1); x++;
                row.CreateCell(x).SetCellValue(org.Address2); x++;
                row.CreateCell(x).SetCellValue(org.Address3); x++;
                row.CreateCell(x).SetCellValue(org.Town); x++;
                row.CreateCell(x).SetCellValue(org.County); x++;
                row.CreateCell(x).SetCellValue(org.Postcode); x++;
                row.CreateCell(x).SetCellValue(org.Country); x++;
                row.CreateCell(x).SetCellValue(org.Email); x++;
                row.CreateCell(x).SetCellValue(org.Website); x++;
                row.CreateCell(x).SetCellValue(org.Telephone); x++;
                row.CreateCell(x).SetCellValue(org.Mobile); x++;
                row.CreateCell(x).SetCellValue(org.SageAccountReference); x++;

                row.CreateCell(x).SetCellValue(org.Notes); x++;
            }


            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("Name"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Type"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address1"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address2"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address3"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Town"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("County"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Postcode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Country"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Email"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Website"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Telephone"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Mobile"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("AccountReference"); sheet.AutoSizeColumn(x); x++;

            headerRow.CreateCell(x).SetCellValue("Notes"); sheet.AutoSizeColumn(x); x++;

            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("{0}Export{1}.xls", ClassName, DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };
        }

        
    }
    

}
