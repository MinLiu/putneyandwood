﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class OrganisationAddressService<TParent, T, TViewModel> : ChildEntityService<TParent, T, TViewModel>
        where TParent : class, IOrganisation, ICompanyEntity, IEntity, IDeletable, new()
        where T : class, IAddress, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        

        public OrganisationAddressService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserId, mapper, dbContext)
        {
            
        }

        public override string LoggingName() { return "Address"; }
        public override string LoggingNamePlural() { return "Addresses"; }


        protected virtual IQueryable<T> GetAddresses(Guid CompanyID, string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            return new List<T>().AsQueryable();
        }

        public DataFileOutput Export(string filterText, Guid? type, Guid? tag, bool deleted = false)
        {
            if (CompanyID == Guid.Empty) throw new Exception("CompanyID has not been set.");

            var addresses = GetAddresses(CompanyID, filterText, type, tag, deleted);

            var list = addresses.ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            int x = 0;
            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var address in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                //row.CreateCell(x).SetCellValue(address.Client.Name); x++;
                row.CreateCell(x).SetCellValue(address.OwnerName); x++;
                row.CreateCell(x).SetCellValue(address.Name); x++;
                row.CreateCell(x).SetCellValue(address.Address1); x++;
                row.CreateCell(x).SetCellValue(address.Address2); x++;
                row.CreateCell(x).SetCellValue(address.Address3); x++;
                row.CreateCell(x).SetCellValue(address.Town); x++;
                row.CreateCell(x).SetCellValue(address.County); x++;
                row.CreateCell(x).SetCellValue(address.Postcode); x++;
                row.CreateCell(x).SetCellValue(address.Country); x++;
                row.CreateCell(x).SetCellValue(address.Telephone); x++;
                row.CreateCell(x).SetCellValue(address.Mobile); x++;
                row.CreateCell(x).SetCellValue(address.Email); x++;
            }


            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue(typeof(TParent).Name); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Name"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address1"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address2"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Address3"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Town"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("County"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Postcode"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Country"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Telephone"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Mobile"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Email"); sheet.AutoSizeColumn(x); x++;


            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("{0}AddressesExport{1}.xls", typeof(TParent).Name,  DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };

        }
        

    }


}
