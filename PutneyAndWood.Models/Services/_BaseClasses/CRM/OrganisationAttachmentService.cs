﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrganisationAttachmentService<TParent, T, TViewModel> : AttachmentService<TParent, T, TViewModel>
        where TParent : class, IOrganisation, IEntity, new()
        where T : class, IAttachment, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
               

        public OrganisationAttachmentService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }

        public override string LoggingName() { return "Attachment"; }
        public override string LoggingNamePlural() { return "Attachments"; }
        
        
    }


}
