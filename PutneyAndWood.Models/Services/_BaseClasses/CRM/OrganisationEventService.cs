﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrganisationEventService<TParent, T, TViewModel> : ChildCompanyEntityService<TParent, T, TViewModel>
        where TParent : class, IOrganisation, IEntity, new()
        where T : class, IEvent, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Event"; }
        public override string LoggingNamePlural() { return "Events"; }


        public OrganisationEventService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserId, mapper, dbContext)
        {
            
        }



        public virtual IQueryable<T> ReadList(Guid ParentID, bool showComplete = true, bool ignoreTeams = false)
        {
            if (ignoreTeams)
            {
                return Read(ParentID).Where(i => (showComplete == true || (showComplete == false && i.Complete == false))).OrderBy(o => o.Timestamp);
            }
            else
            {
                var users = new UserService(DbContext).GetCompanyUserIds(CurrentUserID);
                return base.Read(ParentID).Where(o => users.Contains(o.AssignedUserID) || string.IsNullOrEmpty(o.AssignedUserID))
                                  .Where(i => (showComplete == true || (showComplete == false && i.Complete == false))).OrderBy(o => o.Timestamp);
            }
            
        }


        public virtual IQueryable<T> Search(DateType dateType, string assignedUser, string createdUser, string fromDate = null, string toDate = null, bool showComplete = true, bool ignoreTeams = false)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromDate, toDate);

            var query = Read(null).Where(i =>
                        (i.Timestamp >= fmDate && i.Timestamp <= tDate)
                        && (i.AssignedUserID.Equals(assignedUser) || i.AssignedUserID == null || string.IsNullOrEmpty(assignedUser))
                        && (i.CreatorID.Equals(createdUser) || string.IsNullOrEmpty(createdUser))
                        && (showComplete == true || (showComplete == false && i.Complete == false))
                    );

            if (ignoreTeams)
            {
                return query.OrderBy(o => o.Timestamp);
            }
            else
            {
                var users = new UserService(DbContext).GetCompanyUserIds(CurrentUserID);
                return query.Where(o => users.Contains(o.AssignedUserID) || string.IsNullOrEmpty(o.AssignedUserID)).OrderBy(o => o.Timestamp);
            }
        }


        public override T Create(T entity)
        {
            if (string.IsNullOrEmpty(entity.CreatorID))
                entity.CreatorID = CurrentUserID;

            return base.Create(entity);
        }

        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach(var e in entities)
            {
                if (string.IsNullOrEmpty(e.CreatorID))
                    e.CreatorID = CurrentUserID;
            }

            return base.Create(entities);
        }
        
    }


}
