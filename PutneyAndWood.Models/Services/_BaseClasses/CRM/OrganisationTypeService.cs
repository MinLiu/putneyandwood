﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrganisationTypeService<T, TViewModel> : CompanyEntityService<T, TViewModel>
        where T : class, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Type"; }
        public override string LoggingNamePlural() { return "Types"; }

        public OrganisationTypeService(Guid CompanyID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, UserId, mapper, dbContext)
        {

        }
        

    }


}
