﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.IO;

namespace SnapSuite.Internal
{

    public class AttachmentService<TParent, T, TViewModel> : ChildEntityService<TParent, T, TViewModel>
        where TParent : class, IEntity, new()
        where T : class, IAttachment, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public override Guid CompanyID { get { if (this.aService != null) return this.aService.CompanyID; else return Guid.Empty; } set { if (this.aService != null) this.aService.CompanyID = value; } }
        public override Guid? ParentID { get { if (this.aService != null) return this.aService.ParentID; else return null; } set { if(this.aService != null) this.aService.ParentID = value; } }
        public override bool ApplyParentCompanyID { get { if (this.aService != null) return this.aService.ApplyParentCompanyID; else return true; } }

        public override string LoggingName() { return "Attachment"; }
        public override string LoggingNamePlural() { return "Attachments"; }        

        public int MaxContentLengthInMbs { get { return this.aService.MaxContentLengthInMbs; } set { this.aService.MaxContentLengthInMbs = value; } }
        public string RelativeSavePath { get { return this.aService.RelativeSavePath; } set { this.aService.RelativeSavePath = value; } }
        

        protected readonly GenericAttachmentService<TParent, T> aService;


        public AttachmentService(Guid CompanyID, Guid? parentID, string userID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, parentID, userID, mapper, dbContext)
        {
            this.aService = new GenericAttachmentService<TParent, T>(CompanyID, parentID, dbContext);
            this.aService.RelativeSavePath = "/Attachments/";
            this.aService.MaxContentLengthInMbs = 20;
        }


        public override IQueryable<T> Read(Guid? ParentID)
        {
            return base.Read(ParentID).Include(i => i.File);
        }


        public override T Destroy(T entity)
        {
            return aService.Destroy(entity);
        }
        
        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            return aService.Destroy(entities);
        }


        public T UploadAttachment(Guid parentID, byte[] file, string filename)
        {
            this.ParentID = parentID;
            return UploadAttachment(file, filename);
        }


        public virtual T UploadAttachment(byte[] file, string filename)
        {
            var model = aService.UploadAttachment(file, filename);
            OnCreate(model);
            return model;
        }

        public T UploadAttachment(Guid parentID, HttpPostedFileBase file)
        {
            this.ParentID = parentID;
            return UploadAttachment(file);
        }


        public virtual T UploadAttachment(HttpPostedFileBase file)
        {
            var model = aService.UploadAttachment(file);
            OnCreate(model);
            return model;
        }


        public T LinkAttachment(Guid parentID, string link)
        {
            this.ParentID = parentID;
            return LinkAttachment(link);
        }


        public virtual T LinkAttachment(string link)
        {
            var model = aService.LinkAttachment(link);
            OnCreate(model);
            return model;
        }

    }

}
