﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Models
{
    /// <summary>A simple generic EF repository instance used for basic database CRUD operations.</summary>
    /// <typeparam name="T">Set the class instance type. Should be included as a DbSet in the database context.</typeparam>
    public class GenericService<T> : IDisposable, IService<T> 
        where T : class, new()
    {
        protected DbContext _db;

        public DbContext DbContext { get { return _db; } }
        public virtual string ClassName { get { return typeof(T).Name; } }
        public virtual string[] ReferencesToInclude { get { return new string[] { }; } }
        public virtual string SortingOrder { get; }

        public GenericService(DbContext db)
        {
            _db = db;
            //_db.Configuration.AutoDetectChangesEnabled = false;
        }        

        /// <summary>Create entity in database context.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public virtual T Create(T entity)
        {
            _db.Set<T>().Add(entity);
            _db.SaveChanges();
            AfterCreate(entity);
            return entity;
        }
        

        /// <summary>Create entities in database context.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public virtual IEnumerable<T> Create(IEnumerable<T> entities)
        {
            _db.Set<T>().AddRange(entities);
            _db.SaveChanges();
            foreach(var en in entities)
            {
                AfterCreate(en);
            }

            return entities;
        }

        public virtual void AfterCreate(T entity)
        {
            //Do  nothing
        }


        /// <summary>The set of items from thte database context.</summary>
        protected virtual DbSet<T> DbSet()
        {
            return _db.Set<T>();
        }


        /// <summary>Read the entries from the database context.</summary>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<T> Read()
        {
            var query = _db.Set<T>().AsQueryable();

            //Loop through and include indicated references
            foreach (var include in ReferencesToInclude)
            {
                query = query.Include(include);
            }

            //Set intital sort if avaiable
            if (!string.IsNullOrWhiteSpace(SortingOrder))
            {
                query = query.OrderBy(SortingOrder);
            }

            return query;
        }


        /// <summary>Find the entity from the database context.</summary>
        /// <param name="keyValues">Parameters used to find the entity.</param>
        /// <returns>The first entity from the database context.</returns>
        public virtual T Find(params object[] keyValues)
        {
            return _db.Set<T>().Find(keyValues);
        }


        /// <summary>Update the entity in the database context.</summary>
        /// <param name="entity">Instance with updated values.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>The updated entity.</returns>
        public virtual T Update(T entity, string[] updatedColumns = null)
        {

            if ((updatedColumns ?? new string[0]).Length > 0)
            {
                _db.Set<T>().Attach(entity);
                foreach (string col in updatedColumns)
                {
                    _db.Entry(entity).Property(col).IsModified = true;

                }
                _db.SaveChanges();
            }
            else
            {
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return entity;
        }


        /// <summary>Update the entities in the database context.</summary>
        /// <param name="entities">Instances with updated values.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>The collection of updated entities.</returns>
        public virtual IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null)
        {
            if ((updatedColumns ?? new string[0]).Length > 0)
            {
                foreach (var e in entities)
                {
                    _db.Set<T>().Attach(e);
                    foreach (string col in updatedColumns)
                    {
                        _db.Entry(e).Property(col).IsModified = true;
                    }
                }

                _db.SaveChanges();
            }
            else
            {
                foreach (var e in entities)
                {
                    _db.Entry(e).State = EntityState.Modified;
                }
                _db.SaveChanges();
            }

            return entities;
        }

        /// <summary>Destroy the entity in the database context.</summary>
        /// <param name="entity">Instance to destroy.</param>
        /// <returns>The copy of the destroyed entity.</returns>
        public virtual T Destroy(T entity)
        {
            _db.Entry(entity).State = EntityState.Deleted;
            _db.SaveChanges();
            return entity;
        }


        /// <summary>Destroy the entities in the database context.</summary>
        /// <param name="entities">Instances to destroy.</param>
        /// <returns>The copy of the destroyed entities.</returns>
        public virtual IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            foreach (var e in entities)
            {
                _db.Entry(e).State = EntityState.Deleted;
            }
            _db.SaveChanges();
            return entities;
        }


        /// <summary>Attach the entity from the database context. Allows entity to be updated and manuiplated with affecting context entity.</summary>
        /// <param name="entity">Instance to attach.</param>
        /// <returns>The copy of the attached entity.</returns>
        public virtual T Attach(T entity)
        {
            entity = DbSet().Attach(entity);
            return entity;
        }


        /// <summary>Attach the entities from the database context. Allows the entities to be updated and manuiplated with affecting context entities.</summary>
        /// <param name="entities">Instances to attach.</param>
        /// <returns>The copy of the attached entities.</returns>
        public virtual IEnumerable<T> Attach(IEnumerable<T> entities)
        {
            var set = DbSet();
            foreach (var e in entities)
            {
                set.Attach(e);
            }
            return entities;
        }


        /// <summary>Deattach the entity from the database context. Allows entity to be updated and manuiplated with affecting context entity.</summary>
        /// <param name="entity">Instance to deattach.</param>
        /// <returns>The copy of the deattached entity.</returns>
        public virtual T Deattach(T entity)
        {
            _db.Entry(entity).State = EntityState.Detached;
            return entity;
        }


        /// <summary>Deattach the entities from the database context. Allows the entities to be updated and manuiplated with affecting context entities.</summary>
        /// <param name="entities">Instances to deattach.</param>
        /// <returns>The copy of the deattached entities.</returns>
        public virtual IEnumerable<T> Deattach(IEnumerable<T> entities)
        {
            foreach (var e in entities)
            {
                _db.Entry(e).State = EntityState.Detached;
            }

            return entities;
        }




        /// <summary>Disbose the service and its database context.</summary>        
        public virtual void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }
    }
    

}
