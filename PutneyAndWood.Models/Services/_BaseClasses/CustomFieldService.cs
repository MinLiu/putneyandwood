﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{
    
    public class CustomFieldService<TModel, TViewModel> : CompanyEntityService<TModel, TViewModel>
        where TModel : class, IEntity, ICompanyEntity, ICustomField, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public CustomFieldService(Guid CompanyID, string UserID, ModelMapper<TModel, TViewModel> mapper, DbContext context)
            : base(CompanyID, UserID, mapper, context)
        {
            
        }


        public override TModel Create(TModel entity)
        {
            entity.Name = new Regex("[^A-Za-z0-9 -$\n_]").Replace(entity.Name, "");
            entity.Name = entity.Name.Replace(" ", "_").Replace("__", "_");

            if (Read().Where(i => i.Name == entity.Name).Count() > 0) throw new Exception("Can not have custom fields that share the same name.");
            try { entity.SortPos = Read(Guid.Empty).Where(i => i.CompanyID == CompanyID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First() + 1; }
            catch { }

            return base.Create(entity);
        }

        public override IEnumerable<TModel> Create(IEnumerable<TModel> entities)
        {
            throw new NotImplementedException();
        }


        public override TModel Update(TModel entity, string[] updatedColumns = null)
        {
            entity.Name = new Regex("[^A-Za-z0-9 -$\n_]").Replace(entity.Name, "");
            entity.Name = entity.Name.Replace(" ", "_").Replace("__", "_");

            if (Read().Where(i => i.Name == entity.Name && i.ID != entity.ID).Count() > 0) throw new Exception("Can not have custom fields that share the same name.");
            return base.Update(entity);
        }

        public override IEnumerable<TModel> Update(IEnumerable<TModel> entities, string[] updatedColumns = null)
        {
            throw new NotImplementedException();
        }


        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            TModel move = null;
            TModel top = null;
            move = Read().Where(i => i.ID == itemID).First();
            var items = Read().Where(i => i.CompanyID == move.CompanyID).OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            top = (newPosition - 1 > 0) ? items.ElementAt(newPosition - 1) : null;
            items.Insert(newPosition, move);


            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
            }

            base.Update(items, new string[] { "SortPos" });
            return true;
        }

    }
    

}
