﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service instance used for calculating prices, margins, discounts and totals of price entities.
    /// </summary>
    /// <typeparam name="TPriceItem">Entity model. Must use IPrice interface.</typeparam>
    public class PriceEntityService<TPriceItem> : GenericService<TPriceItem>
        where TPriceItem : class, IPrice, new()        
    {
        
        public PriceEntityService(DbContext db)
            : base(db)
        {
            
        }

        /// <summary>Recalculate and update the price values for the model.</summary>
        /// <param name="model">The model with price values to update.</param
        /// <param name="autoAdjust">Automatically adjust the totals base on the Discount, Margin, Markup & Margin Price input.</param>
        /// <returns>Copy of inserted view models.</returns>
        public virtual TPriceItem UpdateValuesNotSave(TPriceItem model, bool autoAdjust = true, bool isCostItem = false)
        {
            var original = Read().Where(i => i.ID == model.ID).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();

            if (original == null) return null;

            //First re-calulate the new price if any of percentage values change.
            if (autoAdjust)
            {
                if (model.Quantity != original.Quantity) { }
                else if (model.Discount != original.Discount) { model.Price = model.ListPrice - (model.ListPrice * (model.Discount / 100.0m)); }
                else if (model.Margin != original.Margin) { model.Price = (model.Margin == 100m) ? 0m : model.Cost / (1 - (model.Margin / 100.0m)); }
                else if (model.Markup != original.Markup) { model.Price = ((model.Markup / 100.0m) * model.Cost) + model.Cost; }
                else if (model.MarginPrice != original.MarginPrice) { model.Price = model.Cost + model.MarginPrice; }
            }

            decimal profit = model.Price - model.Cost;

            model.Discount = decimal.Round((model.ListPrice == 0m) ? 0m : ((model.ListPrice - model.Price) / model.ListPrice) * 100.0m, 2);

            model.VAT = decimal.Round(model.VAT, 2);
            model.ListPrice = decimal.Round(model.ListPrice, 2);
            model.ListCost = decimal.Round(model.ListCost, 2);
            model.Cost = decimal.Round(model.Cost, 2);
            model.Price = decimal.Round(model.Price, 2);
            model.Quantity = decimal.Round(model.Quantity, 2);
            model.SetupCost = decimal.Round(model.SetupCost, 2);

            model.Markup = decimal.Round((model.Cost == 0m) ? 0m : (profit / model.Cost) * 100.0m, 2);
            model.Margin = decimal.Round((model.Price == 0m) ? 0m : (profit / model.Price) * 100.0m, 2);
            model.MarginPrice = decimal.Round(profit * model.Quantity, 2);

            if (isCostItem)
            {
                model.Total = decimal.Round(model.Cost * model.Quantity + model.SetupCost, 2);
            }
            else
            {
                model.Total = decimal.Round(model.Price * model.Quantity + model.SetupCost, 2);
            }

            return model;
        }


        /// <summary>Recalculate and update the price values for the model.</summary>
        /// <param name="model">The model with price values to update.</param
        /// <param name="autoAdjust">Automatically adjust the totals base on the Discount, Margin, Markup & Margin Price input.</param>
        /// <returns>Copy of inserted view models.</returns>
        public virtual TPriceItem UpdateValues(TPriceItem model, bool autoAdjust = true, bool isCostItem = false)
        {
            model = UpdateValuesNotSave(model, autoAdjust, isCostItem);
            base.Update(model);
            
            return model;
        }
        



    }

}

