﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service instance used for basic database CRUD operations for Quikflw Entity Models and ViewModels.
    /// Service extends ModelViewService and has Mapper and Logging.
    /// </summary>
    /// <typeparam name="TModel">Class that uses the IEntity interface.</typeparam>
    /// <typeparam name="TViewModel">Class that uses the IEntityViewModel interface.</typeparam>
    public class EntityService<TModel, TViewModel> : ModelViewService<TModel, TViewModel>
        where TModel : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        
        

        public EntityService(string UserID, ModelMapper<TModel, TViewModel> mapper, DbContext db)
            : base(UserID, mapper, db)
        {
            
        }


        public override IQueryable<TModel> Read()
        {
            var query = base.Read();             
            return query;
        }


        /// <summary>Find the entity from the database context using ID.</summary>
        /// <param name="ID">ID of entity</param>
        /// <returns>The first entity from the database context. Null if not found.</returns>
        public virtual TModel FindByID(Guid ID)
        {
            return base.Read().Where(i => i.ID == ID).First();
        }
        

        public override void AfterCreate(TModel entity)
        {
            //After creating entity load it from database again to allow lazy loading of virtual referecne and collections
            //But only if ReferencesToInclude are avaible
            if(ReferencesToInclude.Length > 0)
                entity = FindByID(entity.ID);
        }


        /// <summary>Convert view model to model and create entity in database context. View models IDs automatically updated.</summary>
        /// <param name="viewModel">The view model to insert into database context.</param>
        /// <returns>Copy of inserted view model.</returns>
        public override TViewModel Create(TViewModel viewModel)
        {
            //Create model in database then assign new ID to view model
            viewModel.ID = Create(MapToModel(viewModel, false)).ID.ToString();
            return viewModel;            
        }

        


        /// <summary>Convert collection of view models to models and create entites in database context. View models IDs automatically updated.</summary>
        /// <param name="viewModels">The view model to insert into database context.</param>
        /// <returns>Copy of inserted view models.</returns>
        public override IEnumerable<TViewModel> Create(IEnumerable<TViewModel> viewModels)
        {
            //Create models in database
            var result = Create(MapToModel(viewModels, false));

            //Go through and assign IDs for viewModels 
            for (int i = 0; i < viewModels.Count(); i++)
            {
                viewModels.ElementAt(i).ID = result.ElementAt(i).ID.ToString();
            }

            return viewModels;
        }


        public override TViewModel Update(TViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, updatedColumns); }

        /// <summary>Convert view model to model and update entity in database context.</summary>
        /// <param name="viewModel">The view model to update in database context.</param>
        /// <param name="refreshModelFromDb">Whether to load entity from database before mapping and updating.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>Copy of remapped view model.</returns>
        public virtual TViewModel Update(TViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), updatedColumns);
            return MapToViewModel(model);

        }



        public override IEnumerable<TViewModel> Update(IEnumerable<TViewModel> entities, string[] updatedColumns = null) { return Update(entities, true, updatedColumns); }

        /// <summary>Convert view models to models and update entity in database context.</summary>
        /// <param name="viewModel">The view models to update in database context.</param>
        /// <param name="refreshModelFromDb">Whether to load entities from database before mapping and updating.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>Copy of remapped view model.</returns>
        public virtual IEnumerable<TViewModel> Update(IEnumerable<TViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), updatedColumns);
            return MapToViewModel(models);
        }



        /// <summary>Convert view model to model and destroy/remove the entity from database context.</summary>
        /// <param name="viewModel">The view model to delete in the database context.</param>
        /// <returns>Copy of destroyed view model.</returns>
        public override TViewModel Destroy(TViewModel viewModel)
        {
            var model = base.Read().Where(i => i.ID.ToString() == viewModel.ID).First();
            Destroy(model);
            return viewModel;
        }


        /// <summary>Convert view models to models and destroy/remove the entities from database context.</summary>
        /// <param name="viewModels">The view models to delete in the database context.</param>
        /// <returns>Copy of destroyed view models.</returns>
        public override IEnumerable<TViewModel> Destroy(IEnumerable<TViewModel> viewModels)
        {
            var ids = viewModels.Select(i => i.ID);
            var models = base.Read().Where(i => ids.Contains(i.ID.ToString()));
            Destroy(models);
            return viewModels;
        }





        /// <summary>Map entity view model to model. Automatically load entity from database before mapping.</summary>
        /// <param name="viewModel">The view model convert.</param>
        /// <returns>Converted model.</returns>
        public override TModel MapToModel(TViewModel viewModel)
        {
            return MapToModel(viewModel, true);
        }


        /// <summary>Map entity view model to model.</summary>
        /// <param name="viewModel">The view model convert.</param>
        /// <param name="loadModelFromDb">Whether to load entity from database before mapping and updating.</param>
        /// <returns>Converted model.</returns>
        public virtual TModel MapToModel(TViewModel viewModel, bool loadModelFromDb = true)
        {
            if (loadModelFromDb)
            {
                //Get model from database that matches.
                var model = Read().Where(i => i.ID.ToString() == viewModel.ID).DefaultIfEmpty(null).FirstOrDefault();
                if (model == null)
                {
                    return Mapper.MapToModel(viewModel); //return updated mapped model.
                }
                else
                {
                    Mapper.MapToModel(viewModel, model); //If model of not found that return new instance of model.
                    return model;
                }
            }
            else
            {
                return Mapper.MapToModel(viewModel);
            }
        }



        /// <summary>Map collection to entity view models to models. Automatically load entities from database before mapping.</summary>
        /// <param name="viewModels">Collection of view models convert.</param>
        /// <returns>Collection of converted models.</returns>
        public override IEnumerable<TModel> MapToModel(IEnumerable<TViewModel> viewModels)
        {
            return MapToModel(viewModels, true);
        }



        /// <summary>Map collection to entity view models to models.</summary>
        /// <param name="viewModels">Collection of view models convert.</param>
        /// <param name="loadModelFromDb">Whether to load entity from database before mapping and updating.</param>
        /// <returns>Collection of converted models.</returns>
        public virtual IEnumerable<TModel> MapToModel(IEnumerable<TViewModel> viewModels, bool loadModelsFromDb = true)
        {
            if (loadModelsFromDb)
            {
                var ids = viewModels.Select(vm => vm.ID).ToList();
                var models = Read().Where(i => ids.Contains(i.ID.ToString())).OrderBy(i => i.ID).ToList();
                
                foreach(var model in models) //For each view model, load and remap existing model
                {                    
                    Mapper.MapToModel(viewModels.Where(vm => vm.ID == model.ID.ToString()).First(), model);
                }
                return models;
            }
            else
            {
                return viewModels.Select(vm => Mapper.MapToModel(vm)).ToList();
            }
        }



        /// <summary>Convert entity model to SelectItemViewModel.</summary>
        /// <param name="entity">Model to convert</param>
        /// <returns>A select item view model.</returns>
        public virtual SelectItemViewModel ToSelectItemViewModel(TModel entity)
        {
            return new SelectItemViewModel { ID = entity.ID.ToString(), Name = entity.ToShortLabel() };
        }


        /// <summary>Convert entity models to SelectItemViewModels.</summary>
        /// <param name="entities">Models to convert.</param>
        /// <returns>A select item view models.</returns>
        public virtual IEnumerable<SelectItemViewModel> ToSelectItemViewModel(IEnumerable<TModel> entities)
        {
            return entities.Select(i => new SelectItemViewModel
            {
                ID = i.ID.ToString(),
                Name = i.ToShortLabel()
            });
        }
        

    }
    

}
