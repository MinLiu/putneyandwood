﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Internal
{

    /// <summary>
    /// A service for deletable entities consisting of a CompanyID. All classes should use the IEntity, IDeletable and ICompanyEntity interface.
    /// The CompanyID in the service can never be null. This prevents querying the database context for items the don't belogn to a company by mistake.
    /// </summary>
    /// <typeparam name="TModel">Entity class should use the IEntity, IDeletable and ICompanyEntity interface.</typeparam>
    /// <typeparam name="TViewModel">Entity view model class. Class should use the IEntityViewModel interface.</typeparam>
    public class CompanyEntityDeletableService<TModel, TViewModel> : CompanyEntityService<TModel, TViewModel>, IDeletableService<TModel, TViewModel>
        where TModel : class, IDeletable, ICompanyEntity, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        // <summary> The Company ID value.</summary>
        public override Guid CompanyID { get => base.CompanyID; set => base.CompanyID = value; }

        public CompanyEntityDeletableService(Guid CompanyID, string UserId, ModelMapper<TModel, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {
            
        }

        /// <summary>Read the entries from the database context. Only returns items that are active (not marked deleted)</summary>
        /// <returns>A query of the items.</returns>
        public override IQueryable<TModel> Read()
        {
            return Read(false, CompanyID);
        }

        /// <summary>Read the entries from the database context. </summary>
        /// <param name="deleted"> Whether to select active or deleted items</param>
        /// <returns>A query of the items.</returns>
        public IQueryable<TModel> Read(bool deleted)
        {
            return Read(deleted, CompanyID);
        }

        /// <summary>Read the entries from the database context. Only returns items that are active (not marked deleted)</summary>
        /// <param name="CompanyID"> The company ID the entites belong too.</param>
        /// <returns>A query of the items.</returns>
        public override IQueryable<TModel> Read(Guid CompanyID)
        {
            //return Read(false, CompanyID);
            return base.Read(CompanyID);
        }

        /// <summary>Read the entries from the database context.</summary>
        /// <param name="CompanyID"> The company ID the entites belong too.</param>
        /// <param name="deleted"> Whether to select active or deleted items</param>
        /// <returns>A query of the items.</returns>
        public IQueryable<TModel> Read(bool deleted, Guid CompanyID)
        {
            return base.Read(CompanyID).Where(o => o.Deleted == deleted);
        }


        /// <summary>Mark entity as deleted.</summary>
        /// <param name="ID"> ID of entity.</param>
        /// <returns>Copy of entity.</returns>
        public virtual TModel Delete(Guid ID)
        {
            var model = Read(false).Where(o => o.ID == ID).DefaultIfEmpty(null).FirstOrDefault();

            if (model != null)
                return Delete(model);
            else
                return null;
        }

        /// <summary>Mark entity as deleted.</summary>
        /// <param name="viewModel"> Viewmodel instance of entity.</param>
        /// <returns>Copy of entity.</returns>
        public virtual TModel Delete(TViewModel viewModel)
        {
            var model = Read(false).Where(o => o.ID.ToString() == viewModel.ID).DefaultIfEmpty(null).FirstOrDefault();

            if (model != null)
                return Delete(model);
            else
                return null;
        }

        /// <summary>Mark entity as deleted.</summary>
        /// <param name="model"> Instance of entity.</param>
        /// <returns>Copy of entity.</returns>
        public virtual TModel Delete(TModel model)
        {
            model.Deleted = true;
            var result = Update(model, new string[] { "Deleted" });
            OnDelete(model);
            return result;
        }

        /// <summary>Mark entities as deleted.</summary>
        /// <param name="ids"> IDs of entities.</param>
        /// <returns>Copy of entity IDs.</returns>
        public virtual IEnumerable<Guid> Delete(IEnumerable<Guid> ids)
        {
            var models = base.Read().Where(i => ids.Contains(i.ID)).ToList();
            return Delete(models).Select(i => i.ID).ToList();
        }

        /// <summary>Mark entities as deleted.</summary>
        /// <param name="viewModel"> View model instances of entities.</param>
        /// <returns>Copy of view models.</returns>
        public virtual IEnumerable<TViewModel> Delete(IEnumerable<TViewModel> viewModels)
        {
            var ids = viewModels.Select(i => i.ID).ToList();
            var models = base.Read().Where(i => ids.Contains(i.ID.ToString())).ToList();
            var result = Update(models, new string[] { "Deleted" });
            return viewModels;
        }

        /// <summary>Mark entity as deleted.</summary>
        /// <param name="ID"> ID of entity.</param>
        /// <returns>Copy of entity.</returns>
        public virtual IEnumerable<TModel> Delete(IEnumerable<TModel> models)
        {
            foreach (var m in models)
            {
                m.Deleted = true;
            }

            var result = Update(models, new string[] { "Deleted" });
            OnDelete(models);
            return result;
        }


        public virtual TModel Restore(Guid ID)
        {
            var model = Read(true).Where(o => o.ID == ID).DefaultIfEmpty(null).FirstOrDefault();

            if (model != null)
                return Restore(model);
            else
                return null;
        }

        public virtual TModel Restore(TViewModel viewModel)
        {
            var model = base.Read().Where(o => o.ID.ToString() == viewModel.ID).DefaultIfEmpty(null).FirstOrDefault();

            if (model != null)
                return Restore(model);
            else
                return null;

        }

        public virtual TModel Restore(TModel model)
        {
            model.Deleted = false;
            var result = Update(model, new string[] { "Deleted" });
            OnRestore(model);
            return result;
        }


        public virtual IEnumerable<Guid> Restore(IEnumerable<Guid> ids)
        {
            var models = base.Read().Where(i => ids.Contains(i.ID)).ToList();
            return Restore(models).Select(i => i.ID).ToList();
        }

        public virtual IEnumerable<TViewModel> Restore(IEnumerable<TViewModel> viewModels)
        {
            var ids = viewModels.Select(i => i.ID).ToList();
            var models = base.Read().Where(i => ids.Contains(i.ID.ToString())).ToList();
            var result = Restore(viewModels);
            return viewModels;
        }

        public virtual IEnumerable<TModel> Restore(IEnumerable<TModel> models)
        {
            foreach (var m in models)
            {
                m.Deleted = false;
            }
            var result = Update(models, new string[] { "Deleted" });
            OnRestore(models);
            return models;
        }


        public virtual bool ClearDeleted()
        {
            var deleted = Read(true, CompanyID).ToList();
            Destroy(deleted);
            return true;
        }


        public virtual void OnDelete(IEnumerable<TModel> models)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            if (models == null) return;
            if (models.Count() <= 0) return;
            try { AddLog(string.Format("{0} '{1}' deleted.", LoggingNamePlural(), string.Join(", ", models.Select(i => i.ToShortLabel()).ToArray())), CurrentUserID, null); }
            catch { AddLog(string.Format("{0} {1} deleted.", models.Count(), LoggingNamePlural()), CurrentUserID, null); }
        }

        public virtual void OnDelete(TModel model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' deleted.", LoggingName(), model.ToShortLabel()), CurrentUserID, model.ID.ToString()); } catch { }
        }

        public virtual void OnRestore(IEnumerable<TModel> models)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' restored.", LoggingNamePlural(), string.Join(", ", models.Select(i => i.ToShortLabel()).ToArray())), CurrentUserID, null); }
            catch { AddLog(string.Format("{0} {1} restored.", models.Count(), LoggingNamePlural()), CurrentUserID, null); }
        }

        public virtual void OnRestore(TModel model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' restored.", LoggingName(), model.ToShortLabel()), CurrentUserID, model.ID.ToString()); } catch { }
        }

    }

}
