﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Data;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{

    public class ImportEntityServiceWithRemoveExisting<TModel> : ImportEntityService<TModel>
        where TModel : class, new()
    {
        
        public ImportEntityServiceWithRemoveExisting(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(CompanyID, CurrentUserID, db)
        {

        }

        public override ImportResult ImportExcelFile(HttpPostedFileBase excelFile, string sheetName = "", bool deleteFile = true) { return ImportExcelFile(excelFile, false, sheetName, deleteFile); }
        public override ImportResult ImportExcelFile(byte[] bytes, string name, string extension, string sheetName = "", bool deleteFile = true) { return ImportExcelFile(bytes, name, extension, false, sheetName, deleteFile); }

        public virtual ImportResult ImportExcelFile(HttpPostedFileBase excelFile, bool removeExisting, string sheetName = "", bool deleteFile = true)
        {
            if (excelFile == null) return new ImportResult { Exception = new Exception("No file has been uploaded.") };
            if (Path.GetExtension(excelFile.FileName) != ".xls" && Path.GetExtension(excelFile.FileName) != ".xlsx") return new ImportResult { Exception = new Exception("File is the wrong file format.") };

            var dataTable = ImportService.CopyExcelFileToTable(CompanyID, excelFile, sheetName, deleteFile);
            return ImportDataTable(dataTable, removeExisting);
        }
        

        public virtual ImportResult ImportExcelFile(byte[] bytes, string name, string extension, bool removeExisting, string sheetName = "", bool deleteFile = true)
        {
            if (bytes == null) return new ImportResult { Exception = new Exception("No file has been uploaded.") };
            if (extension != ".xls" && extension != ".xlsx") return new ImportResult { Exception = new Exception("File is the wrong file format.") };

            var dataTable = ImportService.CopyExcelFileToTable(CompanyID, bytes, name, extension, sheetName, deleteFile);
            return ImportDataTable(dataTable, removeExisting);
        }


        public override ImportResult ImportDataTable(DataTable dataTable)
        {   
            return ImportDataTable(dataTable, false);
        }

        public virtual ImportResult ImportDataTable(DataTable dataTable, bool removeExisting)
        {
            if (dataTable == null) return new ImportResult { Exception = new Exception("No data table has been uploaded.") };

            string error = "";
            if (!ImportService.IsTableValid(dataTable, RequiredImportColumns, out error)) return new ImportResult { Exception = new Exception(error) };

            var result = CompleteImport(dataTable, removeExisting);
            OnImportCompleted(result);

            return result;
        }

        protected override ImportResult CompleteImport(DataTable data) { return CompleteImport(data, false); }

        protected virtual ImportResult CompleteImport(DataTable data, bool removeExisting)
        {
            return new ImportResult { Exception = new Exception("Import service not complete.") };
        }
        

        protected override void OnImportCompleted(ImportResult result){
            
        }

    }

}

