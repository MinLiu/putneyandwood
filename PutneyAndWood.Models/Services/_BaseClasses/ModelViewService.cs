﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service instance used for basic database CRUD operations for Quikflw Entity Models and ViewModels.
    /// Service has model mapper used map convert models to view models and vice versa.
    /// Service has logging methods from every CRUD operation. Current User ID must be set for logging.
    /// </summary>
    /// <typeparam name="TModel">Entity model. Must use ILoggable interface.</typeparam>
    /// <typeparam name="TViewModel">Entity view model.</typeparam>
    public class ModelViewService<TModel, TViewModel> : GenericService<TModel>, IViewModelService<TModel, TViewModel>, IUserService
        where TModel : class, ILoggable, new()
        where TViewModel : class, new()
    {
        /// <summary>Model and View Model Mapper</summary>
        public virtual ModelMapper<TModel, TViewModel> Mapper { get; private set; }
        /// <summary>User ID who is using the service.</summary>
        public virtual string CurrentUserID { get; set; }

        /// <summary>Activity log name.</summary>
        public virtual string LoggingName() { return typeof(TModel).Name; }
        /// <summary>Activity log name for multiple entites.</summary>
        public virtual string LoggingNamePlural() { return typeof(TModel).Name + "s"; }
        /// <summary>Enable/Disable activity logging.</summary>
        public virtual bool LogChanges { get; set; }

        public ModelViewService(string UserID, ModelMapper<TModel, TViewModel> mapper, DbContext db)
            : base(db)
        {
            Mapper = mapper;
            CurrentUserID = UserID;
            LogChanges = true;
        }


        /// <summary>Convert view model to model and create entity in database context.</summary>
        /// <param name="viewModel">The view model to insert into database context.</param>
        /// <returns>Copy of inserted view model.</returns>
        public virtual TViewModel Create(TViewModel viewModel)
        {
            Create(MapToModel(viewModel));
            return viewModel;
        }

        /// <summary>Convert collection of view models to models and create entites in database context.</summary>
        /// <param name="viewModels">The view model to insert into database context.</param>
        /// <returns>Copy of inserted view models.</returns>
        public virtual IEnumerable<TViewModel> Create(IEnumerable<TViewModel> viewModels)
        {
            Create(MapToModel(viewModels));            
            return viewModels;
        }


        /// <summary>Convert view model to model and update entity in database context.</summary>
        /// <param name="viewModel">The view model to update in database context.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>Copy of view model.</returns>
        public virtual TViewModel Update(TViewModel viewModel, string[] updatedColumns = null)
        {
            var model = MapToModel(viewModel);
            Update(model, updatedColumns);
            return viewModel;
        }


        /// <summary>Convert collection view models to models and update entitie in database context.</summary>
        /// <param name="viewModels">The view models to update in the database context.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>Copy of updated view models.</returns>
        public virtual IEnumerable<TViewModel> Update(IEnumerable<TViewModel> viewModels, string[] updatedColumns = null)
        {
            var models = MapToModel(viewModels);
            Update(models, updatedColumns);
            return viewModels;
        }


        /// <summary>Convert view model to model and destroy/remove the entity from database context.</summary>
        /// <param name="viewModel">The view model to delete in the database context.</param>
        /// <returns>Copy of destroyed view model.</returns>
        public virtual TViewModel Destroy(TViewModel viewModel)
        {
            Destroy(MapToModel(viewModel));
            return viewModel;
        }


        /// <summary>Convert view models to models and destroy/remove the entities from database context.</summary>
        /// <param name="viewModels">The view models to delete in the database context.</param>
        /// <returns>Copy of destroyed view models.</returns>
        public virtual IEnumerable<TViewModel> Destroy(IEnumerable<TViewModel> viewModels)
        {
            Destroy(MapToModel(viewModels));
            return viewModels;
        }


        /// <summary>Map entity view model to model.</summary>
        /// <param name="viewModel">The view model convert.</param>
        /// <returns>Converted model.</returns>
        public virtual TModel MapToModel(TViewModel viewModel)
        {
            return Mapper.MapToModel(viewModel);
        }


        /// <summary>Map entity model to view model.</summary>
        /// <param name="model">The model convert.</param>
        /// <returns>Converted view model.</returns>
        public virtual TViewModel MapToViewModel(TModel model)
        {
            return Mapper.MapToViewModel(model);
        }

        /// <summary>Map collection to entity view models to models.</summary>
        /// <param name="viewModels">Collection of view models convert.</param>
        /// <returns>Collection of converted models.</returns>
        public virtual IEnumerable<TModel> MapToModel(IEnumerable<TViewModel> viewModels)
        {            
            return viewModels.Select(vm => Mapper.MapToModel(vm)).ToList();
        }


        /// <summary>Map collection to entity models to view models.</summary>
        /// <param name="models">Collection of models convert.</param>
        /// <returns>Collection of converted view models.</returns>
        public virtual IEnumerable<TViewModel> MapToViewModel(IEnumerable<TModel> models)
        {
            return models.Select(vm => Mapper.MapToViewModel(vm));
        }


        /// <summary>Create entity in database context. Log changes after operation.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override TModel Create(TModel entity)
        {
            var result = base.Create(entity);
            OnCreate(entity);
            return result;
        }
        

        /// <summary>Create entities in database context. Log changes after operation.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override IEnumerable<TModel> Create(IEnumerable<TModel> entities)
        {
            var result = base.Create(entities);
            OnCreate(entities);
            return result;
        }


        /// <summary>Update the entity in the database context.</summary>
        /// <param name="entity">Instance with updated values.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>The updated entity.</returns>
        public override TModel Update(TModel entity, string[] updatedColumns = null)
        {
            var result = base.Update(entity, updatedColumns);
            OnUpdate(entity);
            return result;
        }


        /// <summary>Update the entities in the database context.</summary>
        /// <param name="entities">Instances with updated values.</param>
        /// <param name="updatedColumns">List of table columns to update. Leave null to update all. No need to include key values.</param>
        /// <returns>The collection of updated entities.</returns>
        public override IEnumerable<TModel> Update(IEnumerable<TModel> entities, string[] updatedColumns = null)
        {
            var result = base.Update(entities, updatedColumns);
            OnUpdate(entities);
            return result;
        }



        /// <summary>Destroy the entity in the database context.</summary>
        /// <param name="entity">Instance to destroy.</param>
        /// <returns>The copy of the destroyed entity.</returns>
        public override TModel Destroy(TModel entity)
        {
            OnDestroy(entity);
            var result = base.Destroy(entity);            
            return result;
        }


        /// <summary>Destroy the entities in the database context.</summary>
        /// <param name="entities">Instances to destroy.</param>
        /// <returns>The copy of the destroyed entities.</returns>
        public override IEnumerable<TModel> Destroy(IEnumerable<TModel> entities)
        {
            OnDestroy(entities);
            var result = base.Destroy(entities);
            return result;
        }


        /// <summary>Method called after create operation is called. Used for logging purposes.</summary>
        /// <param name="models">Models created.</param>
        public virtual void OnCreate(IEnumerable<TModel> models)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            if (models == null) return;
            if (models.Count() <= 0) return;

            try{ AddLog(string.Format("{0} '{1}' created.", LoggingNamePlural(), string.Join(", ", models.Select(i => i.ToShortLabel()).ToArray())), CurrentUserID, null); }
            catch { AddLog(string.Format("{0} {1} created.", models.Count(), LoggingNamePlural()), CurrentUserID, null); }
        }

        /// <summary>Method called after create operation is called. Used for logging purposes.</summary>
        /// <param name="model">Model created.</param>
        public virtual void OnCreate(TModel model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' created.", LoggingName(), model.ToShortLabel()), CurrentUserID, null); }
            catch { AddLog(string.Format("New {0} created.", LoggingName()), CurrentUserID, null); }
        }

        /// <summary>Method called after update operation is called. Used for logging purposes.</summary>
        /// <param name="models">Models updated.</param>
        public virtual void OnUpdate(IEnumerable<TModel> models)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            if (models == null) return;
            if (models.Count() <= 0) return;

            try { AddLog(string.Format("{0} '{1}' updated.", LoggingNamePlural(), string.Join(", ", models.Select(i => i.ToShortLabel()).ToArray())), CurrentUserID, null); }
            catch { AddLog(string.Format("{0} {1} updated.", models.Count(), LoggingNamePlural()), CurrentUserID, null); }
        }

        /// <summary>Method called after update operation is called. Used for logging purposes.</summary>
        /// <param name="model">Model updated.</param>
        public virtual void OnUpdate(TModel model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' updated.", LoggingName(), model.ToShortLabel()), CurrentUserID, null); } catch { }
        }

        /// <summary>Method called after destroy operation is called. Used for logging purposes.</summary>
        /// <param name="models">Models destroyed.</param>
        public virtual void OnDestroy(IEnumerable<TModel> models)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            if (models == null) return;
            if (models.Count() <= 0) return;
            try { AddLog(string.Format("{0} '{1}' permanently deleted.", LoggingNamePlural(), string.Join(", ", models.Select(i => i.ToShortLabel()).ToArray())), CurrentUserID, null); }
            catch { AddLog(string.Format("{0} {1} permanently deleted.", models.Count(), LoggingNamePlural()), CurrentUserID, null); }
        }

        /// <summary>Method called after destroyed operation is called. Used for logging purposes.</summary>
        /// <param name="model">Model destoryed.</param>
        public virtual void OnDestroy(TModel model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;
            try { AddLog(string.Format("{0} '{1}' permanently deleted.", LoggingName(),  model.ToShortLabel()), CurrentUserID, null); } catch { }
        }



        /// <summary>Add log using using Activity Log Service. Module automatically determined.</summary>
        public virtual bool AddLog(string message, string userID, string linkID)
        {
            return AddLog(ModuleTypeValues.GetLogType(typeof(TModel)), message, userID, linkID);
        }

        /// <summary>Add log using using Activity Log Service. Module automatically determined.</summary>
        public virtual bool AddLog(int moduleType, string message, string userID, string linkID)
        {
            if (!LogChanges) return false;
            if (string.IsNullOrWhiteSpace(userID)) return false;
            if (string.IsNullOrWhiteSpace(message)) return false;

            var user = new UserService(DbContext).FindById(userID);

            var logService = new ActivityLogService<ActivityLogViewModel>(user.Id, user.CompanyID, null, DbContext);
            logService.AddLog(user.CompanyID, moduleType, message, user, linkID);
            return true;
        }

        
    }
    

}
