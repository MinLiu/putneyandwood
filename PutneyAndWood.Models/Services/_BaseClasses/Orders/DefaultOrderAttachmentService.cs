﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using System.Web;

namespace SnapSuite.Models
{
    
    public class DefaultOrderAttachmentService<TSettings, T, TViewModel> : ChildOrderSettingsItemService<TSettings, T, TViewModel>
        where TSettings : class, ICompanyEntity, new()
        where T : class, IAttachment, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public override Guid CompanyID { get { if (this.aService != null) return this.aService.CompanyID; else return Guid.Empty; } set { if (this.aService != null) { this.aService.CompanyID = value; this.aService.ParentID = value; } } }
        
        public override string LoggingName() { return "Default Attachment"; }
        public override string LoggingNamePlural() { return "Default Attachments"; }

        public int MaxContentLengthInMbs { get { return this.aService.MaxContentLengthInMbs; } set { this.aService.MaxContentLengthInMbs = value; } }
        public string RelativeSavePath { get { return this.aService.RelativeSavePath; } set { this.aService.RelativeSavePath = value; } }

        protected readonly GenericAttachmentService<TSettings, T> aService;
                

        public DefaultOrderAttachmentService(Guid SettingsID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(SettingsID, UserID, mapper, dbContext)
        {
            this.aService = new GenericAttachmentService<TSettings, T>(SettingsID, null, dbContext);
            this.aService.RelativeSavePath = "/Attachments/";
        }


        public override T Destroy(T entity)
        {            
            return aService.Destroy(entity);
        }


        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            return aService.Destroy(entities);
        }


        public virtual T UploadAttachment(Guid SettingsID, byte[] file, string filename)
        {
            this.CompanyID = SettingsID;
            return UploadAttachment(file, filename);
        }


        public virtual T UploadAttachment(byte[] file, string filename)
        {
            var model = aService.UploadAttachment(file, filename);
            OnCreate(model);
            return model;
        }

        public virtual T UploadAttachment(Guid SettingsID, HttpPostedFileBase file)
        {
            this.CompanyID = SettingsID;
            return UploadAttachment(file);
        }


        public virtual T UploadAttachment(HttpPostedFileBase file)
        {
            var model = aService.UploadAttachment(file);
            OnCreate(model);
            return model;
        }


        public virtual T LinkAttachment(Guid SettingsID, string link)
        {
            this.CompanyID = SettingsID;
            return LinkAttachment(link);
        }


        public virtual T LinkAttachment(string link)
        {
            var model = aService.LinkAttachment(link);
            OnCreate(model);
            return model;
        }

    }


}
