﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrderCommentService<TOrder, T, TViewModel> : ChildEntityService<TOrder, T, TViewModel>
        where TOrder : class, IEntity, ICompanyEntity, new()
        where T : class, IComment, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public override string LoggingName() { return "Comment"; }
        public override string LoggingNamePlural() { return "Comments"; }

        public OrderCommentService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }


        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            var user = new UserService(DbContext).FindById(CurrentUserID);
            if(user != null)
            {
                foreach (var en in entities)
                    if(string.IsNullOrEmpty(en.Email)) en.Email = user.Email;
            }
            
            return base.Create(entities);
        }

        public override T Create(T entity)
        {
            if (string.IsNullOrEmpty(entity.Email))
            {
                var user = new UserService(DbContext).FindById(CurrentUserID);
                if (user != null) entity.Email = user.Email;
            }
            return base.Create(entity);
        }

        //public override void OnCreate(T model) { }
        //public override void OnCreate(IEnumerable<T> models) { }
        //public override void OnUpdate(T model) { }
        //public override void OnUpdate(IEnumerable<T> models) { }
        //public override void OnDestroy(T model) { }
        //public override void OnDestroy(IEnumerable<T> models) { }

    }


}
