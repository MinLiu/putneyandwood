﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Net.Mail;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace SnapSuite.Internal
{

    public class OrderConverterService
        <T, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification,
         ogT, ogTSection, ogTLineItem, ogTLineItemImage, ogTAttachment, ogTComment, ogTNotification, ogTPreviewTemplate, ogTPreviewItem>
        : CompanyEntityDeletableService<T, EmptyViewModel>
        where T : class, IOrder<TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>, new()
        //where TCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        //where TViewModel : class, IEntityViewModel, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TAttachment : class, IAttachment, IShowInPreview, new()
        where TComment : class, IComment, new()
        where TNotification : class, INotification, new()
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>
        where TSettings : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TDefaultAttachment : class, IAttachment, IShowInPreview, new()
        where TDefaultNotification : class, INotification, new()

        where ogT : class, IOrder<ogTSection, ogTLineItem, ogTLineItemImage, ogTAttachment, ogTComment, ogTNotification, ogTPreviewTemplate, ogTPreviewItem>, new()
        //where ogTCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        //where ogTViewModel : class, IEntityViewModel, new()
        where ogTSection : class, IOrderSection<ogTSection, ogTLineItem, ogTLineItemImage>, new()
        where ogTLineItem : class, IOrderLineItem<ogTSection, ogTLineItem, ogTLineItemImage>, new()
        where ogTLineItemImage : class, IImageItem<ogTLineItem>, new()
        where ogTAttachment : class, IAttachment, IShowInPreview, new()
        where ogTComment : class, IComment, new()
        where ogTNotification : class, INotification, new()
        where ogTPreviewTemplate : class, IOrderPreviewTemplate
        where ogTPreviewItem : class, IOrderPreviewItem<ogTPreviewTemplate, ogTAttachment>
        //where ogTSettings : class, ILoggable, IOrderSettings<ogTDefaultAttachment, ogTDefaultNotification>, new()
        //where ogTDefaultAttachment : class, IAttachment, IShowInPreview, new()
        //where ogTDefaultNotification : class, INotification, new()
        
    {

        public virtual string OrderIdColumn { get { return string.Format("{0}ID", typeof(T).Name); } }
        

        public OrderConverterService(Guid CompanyID, string UserID, DbContext db)
            : base(CompanyID, UserID, null, db)
        {
            
        }
        
        
        public virtual T Convert(Guid originalID, string userID)
        {
            if (CompanyID != Guid.Empty)
                return Convert(originalID, CompanyID, userID);
            else
                return null;
        }

        public virtual T Convert(Guid originalID, Guid companyID, string userID)
        {

            //Get original order
            var originalOrder = new GenericService<ogT>(DbContext).Read().Where(i => i.ID == originalID)
                                                                              .Include(i => i.Sections)
                                                                              .Include(i => i.Items)
                                                                              .Include(i => i.Attachments)
                                                                              .Include(i => i.Notifications)
                                                                              //.Include(i => i.PreviewItems)
                                                                              //.AsNoTracking()
                                                                              .DefaultIfEmpty(null)
                                                                              .FirstOrDefault();

            if (originalOrder == null) return null;

            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == companyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, EmptyViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(companyID);
            if (company == null || settings == null) return null;

            var newOrder = Convert1_ReturnNew(originalOrder, user, settings);
            LogChanges = false;
            Create(newOrder);
            LogChanges = false;
            Convert1_SetupReferences(newOrder, originalOrder, company, user, settings);
            Convert2_AddItems(newOrder, originalOrder, company, user, settings);
            Convert3_AddAttachments(newOrder, originalOrder, company, user, settings);
            Convert4_AddComments(newOrder, originalOrder, company, user, settings);
            Convert5_AddPreviews(newOrder, originalOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Convert6_AddLogs(newOrder, originalOrder, company, user, settings);
            Convert7_SendNotifications(newOrder, originalOrder, company, user, settings);
            Convert8_StripePayments(newOrder, originalOrder, company, user, settings);
            Convert9_Recalculate(newOrder, company, user, settings);

            return Find(newOrder.ID);
        }

        

        protected virtual T Convert1_ReturnNew(ogT originalOrder, User user, TSettings settings)
        {
            //Convert original
            //Can not query database again or else overwrites origanl values in memory. Strange.
            //var newOrder = Mapper.MapToModel(Mapper.MapToViewModel(originalOrder));
            var newOrder = ConvertModel(originalOrder);

            //Get Last Order number based on Settings
            var query = Read(originalOrder.CompanyID).OrderByDescending(q => q.Number).Select(i => i.Number).ToList();
            int lastNum = (query.Count > 0) ? query.First() : settings.StartingNumber - 1;

            newOrder.ID = Guid.NewGuid();
            newOrder.CheckSum = Guid.NewGuid().ToString("N");
            newOrder.Reference = settings.DefaultReference; //Get from defaults
            newOrder.Number = lastNum + 1;
            newOrder.StatusID = 1;
            newOrder.Created = DateTime.Now;
            newOrder.LastUpdated = DateTime.Now;
            newOrder.AssignedUserID = (user != null) ? user.Id : originalOrder.AssignedUserID; //Assign to order the use
            newOrder.CreatedBy = (user != null) ? user.Email : originalOrder.CreatedBy;
            newOrder.IsApproved = false;
            newOrder.EmailOpened = null;
            newOrder.Deleted = false;

            return newOrder;
        }

        protected virtual void Convert1_SetupReferences(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {

        }

        protected virtual void Convert2_AddItems(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            var sectionService = new GenericService<TSection>(DbContext);
            var itemService = new GenericService<TLineItem>(DbContext);
            var imageService = new GenericService<TLineItemImage>(DbContext);

            var sections = originalOrder.Sections.Select(i => ConvertSection(i)).ToList();
            var sectionIds = sections.Select(i => i.ID).ToList();

            //Copy Order Items
            var items = originalOrder.Items.Select(i => ConvertLineItem(i)).ToList();

            var images = new List<TLineItemImage>();
            foreach(var item in originalOrder.Items)
            {
                images.AddRange(item.Images.Select(x => new TLineItemImage { OwnerID = x.OwnerID, FileID = x.FileID, SortPos = x.SortPos }).ToList());
            }            

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var s in sections)
            {
                DbContext.Entry(s).State = EntityState.Detached;
                var propInfo = s.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(s, order.ID, null);
                s.ID = Guid.NewGuid();
            }

            sectionService.Create(sections);

            foreach (var i in items)
            {
                DbContext.Entry(i).State = EntityState.Detached;
                var propInfo = i.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(i, order.ID, null);
                i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                
                //New Guid assign below
            }

            //Because items (kits) can have sub items. We have to add them with differently
            foreach (var i in items)
            {
                if (i.IsKit)
                {
                    //If the item is a kit then:
                    //1. Get old ID
                    //2. Add item then save to get new ID
                    //3. Get sub items, update parent ID then add them too
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();                    
                    itemService.Create(i);
                    foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }

                    foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                    {
                        Guid oldSubID = sub.ID;
                        sub.ParentItemID = i.ID;
                        sub.ID = Guid.NewGuid();                        
                        itemService.Create(sub);

                        foreach (var img in images.Where(p => p.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }
                    }
                }
                else if (i.ParentItemID != null)
                {
                    //Do nothing. Because they should added with the above code.
                }
                else
                {
                    //Normal non-kit itme are added directly.
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();                    
                    itemService.Create(i);

                    foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                }                
            }
            
            imageService.Create(images);
        }


        protected virtual void Convert3_AddAttachments(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            var attachmentService = new GenericService<TAttachment>(DbContext);
            var attachments = originalOrder.Attachments.Select(i => ConvertAttachment(i)).ToList();

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var a in attachments)
            {
                DbContext.Entry(a).State = EntityState.Detached;
                var propInfo = a.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(a, order.ID, null);
                a.ID = Guid.NewGuid();
            }

            attachmentService.Create(attachments);            
        }


        protected virtual void Convert4_AddComments(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            var commentsService = new GenericService<TComment>(DbContext);

            var comment = new TComment
            {
                Timestamp = DateTime.Now,
                Email = (user != null) ? user.Email : "", 
                Message = string.Format("Converted from {0}", originalOrder.ToShortLabel())
            };

            var propInfo = comment.GetType().GetProperty(OrderIdColumn);
            if (propInfo != null) propInfo.SetValue(comment, order.ID, null);

            commentsService.Create(comment);
        }


        protected virtual void Convert5_AddPreviews(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            try
            {
                var lastCreatedID = DbContext.Set<T>().Where(i => i.CompanyID == order.CompanyID && i.ID != order.ID).OrderByDescending(q => q.Created).Select(i => i.ID).First();

                if (lastCreatedID != Guid.Empty)
                {
                    var previewItems = DbContext.Set<TPreviewItem>().Where(i => i.ParentID == lastCreatedID && i.Type != PreviewItemType.Attachment)
                                                                    .AsNoTracking().ToList();

                    foreach (var item in previewItems)
                    {
                        item.ID = Guid.NewGuid();
                        item.ParentID = order.ID;
                    }

                    DbContext.Set<TPreviewItem>().AddRange(previewItems);
                    DbContext.SaveChanges();
                }
            }
            catch { }
        }

        protected virtual void Create5_AddDefaultNotifications(T order, Company company, User user, TSettings settings)
        {

            //Add Default Notificatins
            var defNotifications = new GenericService<TDefaultNotification>(DbContext).Read()
                                                                                 .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(TSettings).Name), company.ID))
                                                                                 .Where(i => i.Email != (!string.IsNullOrEmpty(user.Email) ? user.Email : ""))
                                                                                 .ToList();
            var notificationService = new GenericService<TNotification>(DbContext);
            foreach (var a in defNotifications)
            {
                var noti = new TNotification
                {
                    Email = a.Email
                };

                var propInfo = noti.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(noti, order.ID, null);

                notificationService.Create(noti);
            }


            //Add the users email address. 
            if (!string.IsNullOrEmpty(user.Email))
            {
                var noti = new TNotification { Email = user.Email };

                var pInfo = noti.GetType().GetProperty(OrderIdColumn);
                if (pInfo != null) pInfo.SetValue(noti, order.ID, null);

                notificationService.Create(noti);
            }
        }




        protected virtual void Convert6_AddLogs(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            if (user == null) return;

            var text = string.Format("{0} {1} converted from {2}.",  LoggingName(),
                                                                     order.ToShortLabel(),
                                                                     originalOrder.ToLongLabel());
            AddLog(text, user.Id, order.ID.ToString());
        }


        protected virtual void Convert7_SendNotifications(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            if (user == null) return;

            //Log Activity
            var text = string.Format("{0} {1} converted from {2} by {2}.", LoggingName(),
                                                                     order.ToShortLabel(),
                                                                    originalOrder.ToLongLabel(),
                                                                    (user != null) ? user.ToLongLabel() : "");

            var orderService = new OrderService<T, CustomQuotationField, EmptyViewModel, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification>(order.CompanyID, user.Id, null, DbContext);
            orderService.SendNotifications(order.ID, string.Format("New {0} Created; {1}", LoggingName(), order.ToShortLabel()), text);
        }


        protected virtual void Convert8_StripePayments(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            
        }

        protected virtual void Convert9_Recalculate(T order, Company company, User user, TSettings settings)
        {

        }


        protected virtual T ConvertModel(ogT model)
        {
            var newInstance = new T { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(T).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        protected virtual TSection ConvertSection(ogTSection model)
        {
            var newInstance = new TSection { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TSection).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        protected virtual TLineItem ConvertLineItem(ogTLineItem model)
        {
            var newInstance = new TLineItem { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TLineItem).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        protected virtual TAttachment ConvertAttachment(ogTAttachment model)
        {
            var newInstance = new TAttachment { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TAttachment).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        protected virtual TLineItem CopyLineItem(TLineItem model)
        {
            var newInstance = new TLineItem { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TLineItem).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }


    }
    
}
