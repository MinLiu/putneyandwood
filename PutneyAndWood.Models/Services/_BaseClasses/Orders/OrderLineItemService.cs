﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.IO;

namespace SnapSuite.Internal
{

    public class OrderLineItemService<T, TSection, TLineItem, TLineItemImage, TAttachment, TLineItemViewModel> : ChildEntityService<T, TLineItem, TLineItemViewModel>
        where T : class, ICurrency, ICompanyEntity, IEntity, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TAttachment : class, IAttachment, IShowInPreview, new()
        where TLineItemViewModel : class, IEntityViewModel, new()
    { 

        public override string[] ReferencesToInclude { get { return new string[] { "ParentItem", "Images", "Images.File",  }; } }

        public virtual bool SupportsKits { get { return true; } private set {  } }
        public override string LoggingName() { return "Line Item"; }
        public override string LoggingNamePlural() { return "Line Item"; }
        
        
        // <summary> The Company ID value. Leave Empty to avoid parent company ID check</summary>
        public override Guid CompanyID
        {
            get { return base.CompanyID; }
            set
            {
                base.CompanyID = value;
                if (this.Mapper != null)
                {
                    var mapper = this.Mapper as OrderItemModelMapper<TLineItem, TLineItemViewModel>;
                    if (mapper.CustomFieldService != null) mapper.CustomFieldService.CompanyID = value;
                }
            }
        }


        public OrderLineItemService(Guid CompanyID, Guid? OrderID, string UserId, OrderItemModelMapper<TLineItem, TLineItemViewModel> mapper, DbContext db)
            : base(CompanyID, OrderID, UserId, mapper, db)
        {
            
            if (mapper != null)
                mapper.CustomFieldService = new CustomProductFieldService<EmptyViewModel>(CompanyID, null, null, db);
        }


        public override TLineItem Create(TLineItem entity)
        {
            //Get the last position
            int lastPos = 0;
            try
            {
                if(SupportsKits && entity.ParentItemID != null)
                    lastPos = Read(null).Where(i => i.SectionID == entity.SectionID && (i.ID == entity.ParentItemID || i.ParentItemID == entity.ParentItemID)).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First();
                else
                    lastPos = Read(null).Where(i => i.SectionID == entity.SectionID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First();
            }
            catch { }

            entity.SortPos = lastPos + 1;
            return base.Create(entity);
        }


        public override IEnumerable<TLineItem> Create(IEnumerable<TLineItem> entities)
        {
            foreach (var entity in entities)
            {
                Create(entity);
            }

            return entities;
        }


        public override TLineItem Update(TLineItem entity, string[] updatedColumns = null) { return Update(entity, true, true, updatedColumns); }

        public virtual TLineItem Update(TLineItem entity, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            //base.Update(entity, updatedColumns);
            return UpdateValues(entity, UpdateParent, autoAdjust);
        }


        public override IEnumerable<TLineItem> Update(IEnumerable<TLineItem> entities, string[] updatedColumns = null) { return Update(entities, true, true, updatedColumns); }

        public virtual IEnumerable<TLineItem> Update(IEnumerable<TLineItem> entities, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                //base.Update(entity, updatedColumns);
                UpdateValues(entity, UpdateParent, autoAdjust);
            }
            return entities;
        }



        public override TLineItemViewModel Update(TLineItemViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, true, true, updatedColumns); }

        public override TLineItemViewModel Update(TLineItemViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModel, true, true, refreshModelFromDb, updatedColumns); }

        public virtual TLineItemViewModel Update(TLineItemViewModel viewModel, bool UpdateParent, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), UpdateParent, autoAdjust, updatedColumns);
            return MapToViewModel(model);
        }


        public override IEnumerable<TLineItemViewModel> Update(IEnumerable<TLineItemViewModel> viewModels, string[] updatedColumns = null) { return Update(viewModels, true, true, true, updatedColumns); }

        public override IEnumerable<TLineItemViewModel> Update(IEnumerable<TLineItemViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModels, true, true, refreshModelFromDb, updatedColumns); }

        public virtual IEnumerable<TLineItemViewModel> Update(IEnumerable<TLineItemViewModel> viewModels, bool UpdateParent, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), UpdateParent, autoAdjust, updatedColumns);
            return MapToViewModel(models);
        }



        public virtual TLineItem UpdateValues(Guid ID, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            var query = (!SupportsKits) ? Read().Where(i => i.ID == ID) : Read().Where(i => i.ID == ID).Include(i => i.ParentItem);
            if(query.Count() <= 0) return null;
            else  return UpdateValues(query.First(), UpdateParent, autoAdjust, updatedColumns);
        }
        
        public virtual TLineItem UpdateValues(TLineItem model, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            if (model.IsComment) return base.Update(model, new string[] { "Description", "SortPos" });

            var priceService = new PriceEntityService<TLineItem>(DbContext);
            var updated = priceService.UpdateValuesNotSave(model, autoAdjust, false);

            base.Update(updated, updatedColumns);

            if (UpdateParent)
                OnValuesUpdated(model);

            return updated;
        }

        public override TLineItem Destroy(TLineItem entity)
        {
            return Destroy(entity, true);
        }

        public virtual TLineItem Destroy(TLineItem entity, bool UpdateParent)
        {
            try { Deattach(entity); } catch { }
            var items = (!SupportsKits) ? base.Read().Where(i => i.ID == entity.ID).ToList() : Read(null).Where(i => i.ID == entity.ID || i.ParentItemID == entity.ID)
                                                    //.Include(i => i.ParentItem)
                                                    .Include(i => i.Images)
                                                    .ToList();

            foreach (var item in items.ToArray())
            {
                foreach (var remove in item.Images.ToArray())
                    DbContext.Entry(remove).State = EntityState.Deleted;
            }
            
            base.Destroy(items);
            if(UpdateParent) OnValuesUpdated(entity);
            return entity;
        }

        public override IEnumerable<TLineItem> Destroy(IEnumerable<TLineItem> entities)
        {
            return Destroy(entities, true);
        }

        public virtual IEnumerable<TLineItem> Destroy(IEnumerable<TLineItem> entities, bool UpdateParent)
        {
            foreach (var entity in entities)
                Destroy(entity, UpdateParent);

            return entities;
        }



        public virtual TLineItem ConvertItemToCustom(Guid id)
        {
            var items = (!SupportsKits) ? Read(null).Where(i => i.ID == id).ToList() : Read(null).Where(i => i.ID == id || i.ParentItemID == id).Include(i => i.ParentItem).ToList();
            if (items.Count <= 0) throw new Exception("Not records found");

            foreach (var item in items)
            {
                item.ParentItemID = null;
                //item.ProductID = null;
                //if (item.IsKit) item.IsComment = true;  //Is the Item is a kit then is head is converted to a comment.                
                item.IsKit = false;
                item.UseKitPrice = false;

                if (item.ParentItem != null) item.Quantity *= item.ParentItem.Quantity;
                if (item.ParentItem != null) item.Total *= item.ParentItem.Quantity;
            }

            Update(items);
            OnValuesUpdated(items.First());
            return items.First();
        }


        public virtual TLineItem ConvertItemToKit(Guid id)
        {
            var item = FindByID(id);
            if (item.GetType() == typeof(QuotationItem))
            {
                if ((item as QuotationItem).Quotation.UsePriceBreaks)
                    throw new Exception("Can not convert cost item to kit if quotation use prices breaks");
            }

            item.IsKit = true;
            Update(item, new string[] { "IsKit" });
            OnValuesUpdated(item);
            return item;
        }


        public virtual TLineItem SetUseKitPrice(Guid id, bool useKitPrice)
        {
            var item = FindByID(id);

            item.UseKitPrice = useKitPrice;
            Update(item, new string[] { "UseKitPrice" });
            return item;
        }

        public virtual TLineItem SetUseKitItemTotals(Guid id, bool useKitPrice)
        {
            var model = FindByID(id);

            model.UseKitPrice = useKitPrice;

            model.ListPrice = (model.KitItems.Count > 0) ? model.KitItems.Sum(i => i.ListPrice) : 0m;
            model.ListCost = (model.KitItems.Count > 0) ? model.KitItems.Sum(i => i.ListCost) : 0m;
            model.Cost = (model.KitItems.Count > 0) ? model.KitItems.Sum(i => i.Cost) : 0m;
            model.Price = (model.KitItems.Count > 0) ? model.KitItems.Sum(i => i.Price) : 0m;            
            model.SetupCost = (model.KitItems.Count > 0) ? model.KitItems.Sum(i => i.SetupCost) : 0m;

            Update(model);
            return model;
        }


        public virtual TLineItem SetHideItemOnPdf(Guid id, bool hideItem)
        {
            var item = FindByID(id);
            item.HideOnPdf = hideItem;
            Update(item, true, false, new string[] { "HideOnPdf" });


            if (hideItem) //If hide the item is true, be sure to also hide it's kit items
            {
                var kitItems = item.KitItems.ToList();
                foreach (var i in kitItems)
                    i.HideOnPdf = hideItem;

                Update(kitItems, false, false, new string[] { "HideOnPdf" });
            }
                
            return item;
        }

        public virtual TLineItem SetRequired(Guid id, bool required)
        {
            var item = FindByID(id);
            item.IsRequired = required;
            Update(item, false, false, new string[] { "IsRequired" });            
            return item;
        }


        public virtual TLineItem CopyItem(Guid id)
        {
            var copy =  new GenericService<TLineItem>(DbContext).Read().Where(i => i.ID == id).Include(i => i.KitItems).AsNoTracking().First();
            var copyKitItems = copy.KitItems.OrderBy(i => i.SortPos).ToList();

            var imageService = new GenericService<TLineItemImage>(DbContext);
            var itemIds = copyKitItems.Select(i => i.ID).ToArray();

            var images = imageService.Read().Where(i => itemIds.Contains(i.OwnerID) || i.OwnerID == copy.ID)
                                            .Include(i => i.File)
                                            .ToList()
                                            .Select(i => new TLineItemImage { OwnerID = i.OwnerID, SortPos = i.SortPos, FileID = i.FileID }).ToList();

            Guid oldID = copy.ID;
            copy.ID = Guid.NewGuid();
            foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = copy.ID; }
            copy.KitItems = null;
            LogChanges = false;
            Create(copy);
            
            foreach(var k in copyKitItems)
            {
                Guid oldSubID = k.ID;
                k.ID = Guid.NewGuid();
                foreach (var img in images.Where(p => p.OwnerID == oldSubID)) { img.OwnerID = k.ID; }
                k.ParentItemID = copy.ID;
            }
            Create(copyKitItems);
            LogChanges = true;

            imageService.Create(images);

            AddLog(copy, string.Format("Line Item '{1}' copied on {{0}}.", 0, copy.ToShortLabel()));
            OnValuesUpdated(copy);
            return copy;
        }



        public virtual TLineItem SwitchItemWithProduct(Guid orderID, Guid itemID, Guid productID)
        {
            bool GetSupplierDiscount = (typeof(T) == (typeof(Quotation))) ? false : true;

            var order = new GenericService<T>(DbContext).Find(orderID);
            var item = FindByID(itemID);
            var productService = new ProductService<TLineItemViewModel>(order.CompanyID, null, null, DbContext);
            var product = productService.GetProduct(productID, item.Quantity, order.CurrencyID, GetSupplierDiscount);

            if (product.IsKit) throw new Exception("Can not switch items with kits");
            if (item.IsKit) throw new Exception("Can not switch items with kits");

            item.ProductCode = product.ProductCode;
            item.Description = product.Description;
            item.Category = product.Category;
            item.VendorCode = product.VendorCode;
            item.Manufacturer = product.Manufacturer;
            //item.VendorCode = product.VendorCode;
            //item.Manufacturer = product.Manufacturer;
            item.AccountCode = product.AccountCode;
            item.JsonCustomFieldValues = product.JsonCustomFieldValues;
            item.Unit = product.Unit;
            item.VAT = product.VAT;
            item.ImageURL = product.ImageURL;
            item.ListPrice = product.Price;
            item.Price = product.Price;
            item.ListCost = productService.GetProduct(productID, item.Quantity, order.CurrencyID, false).Cost;
            item.Cost = product.Cost;
            Update(item);

            return item;
        }
       



        public virtual TLineItem AddProduct(Guid productID, Guid sectionID, decimal qty, Guid? parentItemID = null, int lastPos = 0, bool hidden = false)
        {
            if (ParentID.HasValue)
                return AddProduct(productID, ParentID.Value, sectionID, qty, parentItemID, lastPos, hidden);
            else
                return null;
        }


        public virtual TLineItem AddProduct(Guid productID, Guid orderID, Guid sectionID, decimal qty, Guid? parentItemID = null, int lastPos = 0, bool hidden = false)
        {
            var order = new GenericService<T>(DbContext).Find(orderID);
            

            bool GetSupplierDiscount = (typeof(T) == (typeof(Quotation))) ? false : true;

            var productService = new ProductService<TLineItemViewModel>(order.CompanyID, null, null, DbContext);
            var product = productService.GetProduct(productID, qty, order.CurrencyID, GetSupplierDiscount);
            

            if (!parentItemID.HasValue) //Only get last pos if not in kit item loop
            {
                try { lastPos = Read(orderID).Where(i => i.SectionID == sectionID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); lastPos++; }
                catch { }
            }

            if (!parentItemID.HasValue)
            {
                //Disable Logging changes
                LogChanges = false;
            }

            var toAdd = new TLineItem
            {
                SectionID = sectionID,
                ProductCode = product.ProductCode,
                Description = product.Description,
                Category = product.Category,
                VendorCode = product.VendorCode,
                Manufacturer = product.Manufacturer,
                //VendorCode = product.VendorCode,
                //Manufacturer = product.Manufacturer,
                AccountCode = product.AccountCode,
                PurchaseAccountCode = product.PurchaseAccountCode,
                JsonCustomFieldValues = product.JsonCustomFieldValues,
                Unit = product.Unit,
                VAT = product.VAT,
                Quantity = qty,
                HideOnPdf = hidden,
                ImageURL = product.ImageURL,
                ParentItemID = (UsePriceBreaks(order)) ? null : parentItemID,
                IsKit = (parentItemID.HasValue) ? false : product.IsKit,
                UseKitPrice = (parentItemID.HasValue) ? false : product.UseKitPrice,
                SortPos = lastPos,
                IsComment = (UsePriceBreaks(order) && product.IsKit && !product.UseKitPrice && !parentItemID.HasValue) ? true : false,
                IsRequired = product.IsRequired,

                ListPrice = product.Price,
                Price = product.Price,
                ListCost = productService.GetProduct(productID, qty, order.CurrencyID, false).Cost,
                Cost = product.Cost,
            };
            
            //Create line item in database
            var result = base.Create(toAdd);

            //Then add product images
            var imgService = new GenericService<TLineItemImage>(DbContext);
            var images = new GenericService<ProductImage>(DbContext).Read().Include(i => i.File)
                                                                    .Where(i => i.OwnerID == product.ID).ToList()
                                                                    .Select(x => new TLineItemImage { OwnerID = toAdd.ID, FileID = x.FileID, SortPos = x.SortPos });
            imgService.Create(images).ToList();

            //Then add default attachments if not added
            //Set the parent ID so that the attachement service automatically sets the parent ID.
            var aService = new OrderAttachmentService<T, TAttachment, EmptyViewModel>(CompanyID, order.ID, null, null, DbContext);
            var existingIDs = aService.Read().Select(i => i.FileID);
            var newAttachments = product.DefaultAttachments.Where(i => !existingIDs.Contains(i.FileID)).Select(i => new TAttachment { Filename = i.Filename, FileID = i.FileID, ShowInPreview = i.ShowInPreview }).ToArray();
            aService.Create(newAttachments);


            //Update order values including parent
            UpdateValues(result.ID, false, true);
            
            if (product.IsKit && !parentItemID.HasValue)
            {
                var kitItemService = new ProductKitItemService<EmptyViewModel>(CompanyID, null, null, null, DbContext);

                foreach (var i in kitItemService.Read().Where(i => i.KitID == product.ID).OrderBy(p => p.SortPos).ThenBy(p => p.Product.ProductCode).ThenBy(p => p.Product.Description).ToList())
                {
                    lastPos++;

                    if(!i.ProductID.HasValue)
                    {
                        // Do nothing
                    }
                    else if (i.IsComment)
                    {
                        var kitAdd = new TLineItem
                        {
                            //QuotationID = Guid.Parse(orderID),
                            SectionID = sectionID,
                            Description = i.Description,
                            IsComment = true,
                            ParentItemID = (UsePriceBreaks(order)) ? null : toAdd.ID as Guid?,
                            SortPos = lastPos
                        };

                        base.Create(kitAdd);
                    }
                    else
                    {
                        AddProduct(i.ProductID.Value, sectionID, i.Quantity, result.ID, lastPos, i.HideOnPdf);
                    }
                }

            }

            if (!parentItemID.HasValue)
            {
                //Enable Logging changes
                LogChanges = true;
                AddLog(result, string.Format("Product '{1}' added to {{0}}.", 0, product.ToShortLabel()));
                OnValuesUpdated(result);
            }

            return result;
        }

        public virtual TLineItem AddProduct(Product product, Guid sectionID, decimal qty)
        {            
            return AddProduct(product, ParentID.Value, sectionID, qty);
        }

        public virtual TLineItem AddProduct(Product product, Guid orderID, Guid sectionID, decimal qty)
        {
            ParentID = orderID;

            var lastPos = Read(orderID).Where(i => i.SectionID == sectionID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).Take(1).ToArray().DefaultIfEmpty(0).FirstOrDefault();
            lastPos++;

            var toAdd = new TLineItem
            {
                SectionID = sectionID,
                ProductCode = product.ProductCode,
                Description = product.Description,
                Category = product.Category,
                VendorCode = product.VendorCode,
                Manufacturer = product.Manufacturer,
                //VendorCode = product.VendorCode,
                //Manufacturer = product.Manufacturer,
                AccountCode = product.AccountCode,
                PurchaseAccountCode = product.PurchaseAccountCode,
                JsonCustomFieldValues = product.JsonCustomFieldValues,
                Unit = product.Unit,
                VAT = product.VAT,
                Quantity = qty,

                //ImageURL = product.ImageURL,
                ParentItemID = null,
                IsKit = product.IsKit,
                UseKitPrice = product.UseKitPrice,
                SortPos = lastPos,
                IsComment = false,
                IsRequired = product.IsRequired,

                ListPrice = product.Price,
                Price = product.Price,
                ListCost = product.Cost,
                Cost = product.Cost,
            };

            LogChanges = false;
            var result = Create(toAdd);            
            AddLog(result, string.Format("Product '{1}' added to {{0}}.", 0, product.ToShortLabel()));
            LogChanges = true;

            OnValuesUpdated(result);

            return result;
        }

        public virtual bool UsePriceBreaks(T order) { return false; }

        public virtual bool UpdatePositions(Guid itemID, Guid newSectionID, int oldPosition, int newPosition)
        {
            string tempUserId = CurrentUserID;
            CurrentUserID = null;

            Guid oldSection = Guid.Empty;
            TLineItem move = null;
            TLineItem top = null;
            TLineItem bottom = null;

            move = (!SupportsKits) ? Read().Where(i => i.ID == itemID).First() : Read().Include(i => i.ParentItem).Include(i => i.KitItems).Where(i => i.ID == itemID).First();

            oldSection = move.SectionID;

            move.SectionID = newSectionID;
            Update(move, new string[] { "SectionID" });

            if (SupportsKits)
            {
                var moveItems = move.KitItems.ToList();
                foreach (var item in move.KitItems.ToList())
                {
                    item.SectionID = newSectionID;
                }

                Update(moveItems, new string[] { "SectionID" });
            }

            var items = (!SupportsKits) ? Read().Where(i => i.SectionID == newSectionID).OrderBy(i => i.SortPos).ToList() : Read().Include(i => i.ParentItem).Where(i => i.SectionID == newSectionID).OrderBy(i => i.SortPos).ThenBy(i => i.ParentItemID != null).ToList();
            items.Remove(move);

            top = (newPosition - 1 >= 0) ? items.ElementAt(newPosition - 1) : null;
            bottom = (newPosition < items.Count) ? items.ElementAt(newPosition) : null;

            if (top == null || move.IsKit)
            { //If the top item is null or the item is a kit, set it's parent to null.
                move.ParentItemID = null;
            }
            else if (top != null && bottom == null) //If the top exists but bottom doesn't
            {
                if (top.IsKit) //If the top is a kit then set to parent to the top
                    move.ParentItemID = top.ID;
                else if (move.ParentItemID != null && move.ParentItemID != top.ParentItemID) //If the move and top parent item don't match (moved item to the bottom of another kit) then set parent to null
                    move.ParentItemID = null;
            }
            else if (top != null && bottom != null) //If the top and bottom items exist
            {
                if (top.IsKit /*&& (bottom.ParentItemID != null || bottom.IsKit)*/) //If the top is a kit and the bottom belongs to top or bottonm is a kit, bacisally empty bottom). then set the parent Id as the top
                    move.ParentItemID = top.ID;
                else if (top.ParentItemID != null && bottom.ParentItemID != null) //if top and bottom have parent item, (item moved between kit items) then set parent id as the same as top
                    move.ParentItemID = top.ParentItemID;
                else if (move.ParentItemID != null && move.ParentItemID != top.ParentItemID) //If the move and top parent item don't match (moved item to the bottom of another kit) then set parent to null
                    move.ParentItemID = null;
            }


            items.Insert((newPosition >= 0) ? newPosition : 0, move);


            int sortPos = 0;
            foreach (var i in items.Where(i => i.ParentItemID == null))
            {
                if (i.IsKit == false)
                {
                    i.SortPos = sortPos;
                    i.SectionID = newSectionID;
                    sortPos++;
                }
                else
                {
                    i.SortPos = sortPos;
                    var sortables = items.Where(u => u.ParentItemID == i.ID).ToList();
                    foreach (var u in sortables)
                    {
                        sortPos++;
                        u.SortPos = sortPos;
                        u.SectionID = newSectionID;
                        if(SupportsKits) Update(u, false, false, new string[] { "SortPos", "ParentItemID", "SectionID" });
                        else Update(u, false, false, new string[] { "SortPos", "SectionID" });
                    }
                    sortPos++;
                }

                if (SupportsKits) Update(i, false, false, new string[] { "SortPos", "ParentItemID", "SectionID" });
                else Update(i, false, false, new string[] { "SortPos", "SectionID" });
                
            }

            UpdateOldSection(oldSection);
            OnValuesUpdated(move);


            CurrentUserID = tempUserId;
            AddLog(move, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), move.ToShortLabel()));
            return true;
        }

        public virtual void UpdateOldSection(Guid oldSection)
        {
            //Do nothing
        }
        

        public override void OnCreate(TLineItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if(model.IsComment) AddLog(model, string.Format("{1} '{2}' created for {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' created for {{0}}.", 0, LoggingName(), model.ToShortLabel()));
        }

        public override void OnUpdate(TLineItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if (model.IsComment) AddLog(model, string.Format("{1} '{2}' update from {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), model.ToShortLabel()));
        }
        

        public override void OnDestroy(TLineItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if (model.IsComment) AddLog(model, string.Format("{1} '{2}' permanently deleted from {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' permanently deleted from {{0}}.", 0, LoggingName(), model.ToShortLabel()));

        }


        public virtual void OnValuesUpdated(TLineItem model)
        {
            throw new NotImplementedException("OnValuesUpdated function has not been overwritten.");
        }

    }

}
