﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class ChildOrderSettingsItemService<TSettings, T, TViewModel> : EntityService<T, TViewModel>
        where TSettings : class, ICompanyEntity, new()
        where T : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        
        /// <summary> The settings ID value. Leave null to get all child entities regardless of settings ID.</summary>
        public virtual Guid CompanyID { get; set; }
        /// <summary> Returns the name of the settings reference ID column.</summary>
        public virtual string CompanyName { get { return typeof(TSettings).Name; } }
        /// <summary> Returns the name of the settings reference ID column.</summary>
        public virtual string CompanyIdColumn { get { return string.Format("{0}ID", typeof(TSettings).Name); } }


        public ChildOrderSettingsItemService(Guid CompanyID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(UserId, mapper, dbContext)
        {
            this.CompanyID = CompanyID;
        }



        /// <summary>Read the entries from the database context. Will query items with the set company ID.</summary>
        /// <returns>A query of the items.</returns>
        public override IQueryable<T> Read()
        {
            return Read(this.CompanyID);
        }


        /// <summary>Read the entries from the database context. Will query items with the passed company ID.</summary>
        /// <param name="CompanyID">ID of Settings</param>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<T> Read(Guid CompanyID)
        {
            //Only return the company items if the companyID is not null
            return base.Read().Where(string.Format("{0}.Equals(Guid(\"{1}\"))", CompanyIdColumn, CompanyID));
        }



        // <summary>Create entity in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override T Create(T entity)
        {
            SetParentIdIfEmpty(entity);
            return base.Create(entity);
        }


        /// <summary>Create entities in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach (var en in entities)
            {
                SetParentIdIfEmpty(en);
            }
            return base.Create(entities);
        }


        /// <summary>Method called during create function. Sets the parent ID if null or empty.</summary>
        protected virtual void SetParentIdIfEmpty(T entity)
        {
            var pInfo = entity.GetType().GetProperty(CompanyIdColumn);
            if (pInfo != null)
            {
                if ((Guid)pInfo.GetValue(entity, null) == Guid.Empty && CompanyID != Guid.Empty)
                    pInfo.SetValue(entity, CompanyID, null);
            }
        }

    }


}
