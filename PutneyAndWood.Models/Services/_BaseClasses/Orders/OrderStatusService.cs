﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrderStatusService<T, TViewModel> : GenericService<T>
        where T : class, new()
        where TViewModel : class, new()
    {
        public virtual ModelMapper<T, TViewModel> Mapper { get; set; }

        public OrderStatusService(ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(dbContext)
        {
            Mapper = mapper;
        }
        

        public override IEnumerable<T> Create(IEnumerable<T> entities) { throw new Exception("Not possible"); }
        public override T Create(T entity) { throw new Exception("Not possible"); }
        public override IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override T Update(T entity, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override IEnumerable<T> Destroy(IEnumerable<T> entities) { throw new Exception("Not possible"); }
        public override T Destroy(T entity) { throw new Exception("Not possible"); }
        

        public override void AfterCreate(T model) { }
    }


}
