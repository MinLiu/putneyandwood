﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Net.Mail;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using HtmlToOpenXml;

namespace SnapSuite.Internal
{
    
    public class OrderPdfGenService<T, TCustomField, TViewModel, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification> 
        :  CompanyEntityService<T, TViewModel>, IOrderPDFService<T>
        where T : class, IOrder<TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>, new()
        where TCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        where TViewModel : class, IEntityViewModel, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TAttachment : class, IAttachment, new()
        where TComment : class, IComment, new()
        where TNotification : class, INotification, new()
        where TPreviewTemplate : class, IOrderPreviewTemplate, new()
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>, new()
        where TSettings : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TDefaultAttachment : class, IAttachment, new()
        where TDefaultNotification : class, INotification, new()
    {

        public virtual string OrderIdColumn { get { return string.Format("{0}ID", typeof(T).Name); } }
        

        public OrderPdfGenService(Guid CompanyID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserID, mapper, db)
        {
            
        }

        public string GetDownloadUrl(T model)
        {   
            var filepath = FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", string.Format("{0}.pdf", model.ID.ToString()));
            var url = FileService.GetUploadVirtualFilePath(model.CompanyID, "/_Temp/", string.Format("{0}.pdf", model.ID.ToString()));
            if (System.IO.File.Exists(filepath))
            {
                if (model.LastUpdated <= System.IO.File.GetCreationTime(filepath)) { return url; }
            }

            string name = "";
            GeneratePDF(model.ID, ref name);
            return url;
        }


        public string GetAttachmentPdfFilePath(T model)
        {
            var filepath = FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", string.Format("{0}.pdf", model.ID.ToString()));
            if (System.IO.File.Exists(filepath)) 
            {
                if (model.LastUpdated <= System.IO.File.GetCreationTime(filepath)) { return filepath; }
            }

            string name = "";
            GeneratePDF(model.ID, ref name);
            return filepath;
        }

        public string GetPdfFilename(T model)
        {
            //Create dictionary of tag details
            var dictionary = CreateDictionary(model);
            var filename = new OrderSettingsService<TSettings, TViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(CompanyID).EmailTitleTemplate ?? "";


            if (string.IsNullOrEmpty(filename))
                filename = string.Format("{0} {1}", LoggingName(), model.ToLongLabel()).Truncate(80);
            else
                foreach (var dicItem in dictionary)
                {
                    filename = filename.Replace(dicItem.Key, dicItem.Value);
                }

            return new string(filename.Select(ch => System.IO.Path.GetInvalidFileNameChars().Contains(ch) ? '_' : ch).ToArray()) + ".pdf";
        }


        public byte[] GeneratePDF(Guid id, ref string filename)
        {

            /*
             * The generation of the PDF document is done in 6 stages.
             * 1. Load the model information and create a dictionary
             * 2. Create a blank pdf document
             * 3. Create a front cover from the selected template and use the dictionary to replace tagged words. Add pages to final document.
             * 4. Create a body from the selected template and use the dictionary and model to replace tagged words. Add pages to final document.
             * 5. Create a back cover from the selected template and use the dictionary to replace tagged words. Add pages to final document.
             * 6. Output final pdf document as byte array.
             */

            //Load model from database including necesscary items
            var model = Read(Guid.Empty).Where(i => i.ID == id).First();

            //Create dictionary of tag details
            var dictionary = CreateDictionary(model);

            filename = GetPdfFilename(model);

            var filepath = FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", string.Format("{0}.pdf", model.ID.ToString()));
            //var filepath2 = FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", string.Format("{0}-uncompressed.pdf", model.ID.ToString()));

            if (System.IO.File.Exists(filepath)) //Check to see if the PDF has already been created
            {
                if (model.LastUpdated <= System.IO.File.GetCreationTime(filepath)) //If the PDF CreationDate and quote Last Updated date do not match then the PDF is out of date and a new one needs to be generated
                {
                    using (FileStream stream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        using (BinaryReader binaryReader = new BinaryReader(stream))
                        {
                            //The up to date PDF bytes are returned.
                            return binaryReader.ReadBytes((int)stream.Length);
                        }
                    }
                }
            }
            

            EvoPdf.Document mergeResultPdfDocument = new EvoPdf.Document();
            mergeResultPdfDocument.AutoCloseAppendedDocs = true;
            mergeResultPdfDocument.LicenseKey = "mBYFFwIHFwQGBxcAGQcXBAYZBgUZDg4ODhcH";

            var previews = model.PreviewItems.OrderBy(i => i.SortPos).Take(10).ToList();
            if (previews.Count <= 0) previews.Add(new TPreviewItem { Type = PreviewItemType.Template });

            foreach (var previewItem in previews)
            {
                try
                {
                    using (MemoryStream pdfStream = new MemoryStream())
                    {
                        byte[] pdfArray = null;
                        if(previewItem.Type == PreviewItemType.Template && previewItem.TemplateID.HasValue)
                        {
                            if (previewItem.Template.Format == TemplateFormat.Pdf) pdfArray = DocumentService.PdfToPdf(FileService.ToPhysicalPath(previewItem.Template.File.FilePath), dictionary);
                            else pdfArray = GenerateBodyPDF(model, dictionary, FileService.ToPhysicalPath(previewItem.Template.File.FilePath));
                        }
                        else if(previewItem.Type == PreviewItemType.Template && !previewItem.TemplateID.HasValue)
                        {
                            var path = new GenericService<TPreviewTemplate>(DbContext).Read().Select(i => i.File).First().FilePath;
                            pdfArray = GenerateBodyPDF(model, dictionary, FileService.ToPhysicalPath(path));
                        }
                        else 
                        {
                            if (!previewItem.AttachmentID.HasValue) continue;
                            else if (previewItem.Attachment.File.FileFormat == FileFormat.Pdf) pdfArray = DocumentService.PdfToPdf(FileService.ToPhysicalPath(previewItem.Attachment.File.FilePath), dictionary);
                            else if (previewItem.Attachment.File.FileFormat == FileFormat.Docx) pdfArray = DocumentService.GeneratePdfFromDocx(FileService.ToPhysicalPath(previewItem.Attachment.File.FilePath), dictionary, new Dictionary<string, string>());
                            else continue;
                        }

                        pdfStream.Write(pdfArray, 0, pdfArray.Length);
                        pdfStream.Seek(0, System.IO.SeekOrigin.Begin);

                        EvoPdf.Document pdfMerge = new EvoPdf.Document(pdfStream);
                        mergeResultPdfDocument.AppendDocument(pdfMerge);
                    }
                }
                catch (Exception ex)
                {
                    //Add error page if fail to build pdf
                    try
                    {
                        using (MemoryStream pdfStream = new MemoryStream())
                        {
                            byte[] pdfArray = DocumentService.GenerateErrorPDF(ex);
                            pdfStream.Write(pdfArray, 0, pdfArray.Length);
                            pdfStream.Seek(0, System.IO.SeekOrigin.Begin);

                            EvoPdf.Document pdfMerge = new EvoPdf.Document(pdfStream);
                            mergeResultPdfDocument.AppendDocument(pdfMerge);
                        }
                    }
                    catch { }
                }
            }            

            mergeResultPdfDocument.CompressCrossReference = true;
            mergeResultPdfDocument.CompressionLevel = EvoPdf.PdfCompressionLevel.Best;
            mergeResultPdfDocument.JpegCompressionEnabled = true;
            //mergeResultPdfDocument.JpegCompressionLevel = 75;
            //return mergeResultPdfDocument.Save();

            //model = null; //Save on memory;
            //PdfReader reader = new PdfReader("D:/pdfcontentadded.pdf");
            //PdfStamper stamper = new PdfStamper(reader, new FileStream("d:/pdfdoccompressed.pdf", FileMode.Create), PdfWriter.VERSION_1_5);
            //int pageNum = reader.NumberOfPages;
            //for (int i = 1; i <= pageNum; i++)
            //{
            //    reader.SetPageContent(i, reader.GetPageContent(i));
            //}
            //stamper.SetFullCompression();
            //stamper.Close();

            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    //Load the data in memory then compress the     
                    
                    if (!Directory.Exists(FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", "")))
                    {
                        Directory.CreateDirectory(FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", ""));
                    }

                    var data = mergeResultPdfDocument.Save();
                    System.IO.File.WriteAllBytes(filepath, data);
                    mergeResultPdfDocument = null;                    
                    System.IO.File.SetCreationTime(filepath, model.LastUpdated);
                    System.IO.File.SetLastWriteTime(filepath, model.LastUpdated);
                    return data;
                }

            }
            catch (Exception ex)
            {
                var cc = ex.Message;
                return null;
            }

            /*
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    //Load the data in memory then compress the 
                    var data = mergeResultPdfDocument.Save();
                    Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument(data);
                    System.IO.File.WriteAllBytes(filepath2, data);
                    mergeResultPdfDocument = null; //Save on memory

                    doc.FileInfo.IncrementalUpdate = false;
                    foreach (Spire.Pdf.PdfPageBase page in doc.Pages)
                    {
                        if (page != null)
                        {
                            if (page.ImagesInfo != null)
                            {
                                foreach (Spire.Pdf.Exporting.PdfImageInfo info in page.ImagesInfo)
                                {
                                    page.TryCompressImage(info.Index);
                                }
                            }
                        }
                    }


                    if (!Directory.Exists(FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", "")))
                    {
                        Directory.CreateDirectory(FileService.GetUploadPhysicalFilePath(model.CompanyID, "/_Temp/", ""));
                    }
                                        

                    doc.SaveToFile(filepath);
                    System.IO.File.SetCreationTime(filepath, model.LastUpdated);
                    System.IO.File.SetLastWriteTime(filepath, model.LastUpdated);

                    doc.SaveToStream(stream);
                    doc = null; //Save on memory
                    return stream.ToArray();
                }

            }
            catch (Exception ex)
            {
                var cc = ex.Message;
                return null;
            }
            //return null;
            */
        }

        /*
        protected virtual TPreviewTemplate GetFrontTemplate(TPreview preview) { return null; }
        protected virtual TPreviewTemplate GetBodyTemplate(TPreview preview) { return null; }
        protected virtual TPreviewTemplate GetBackTemplate(TPreview preview) { return null; }
        */
        


        public byte[] GenerateBodyPDF(Guid id, string templateFilePath)
        {
            var model = base.Read().Where(i => i.CompanyID == CompanyID & i.ID == id).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) throw new Exception("Record not found");
            return GenerateBodyPDF(model, null, templateFilePath);
        }


        //Can be used to generate the Front, Body or Back PDFs
        public byte[] GenerateBodyPDF(T model, string templateFilePath)
        {
            return GenerateBodyPDF(model, null, templateFilePath);
        }

        //Can be used to generate the Front, Body or Back PDFs
        public virtual byte[] GenerateBodyPDF(T model, Dictionary<string, string> dictionary, string templateFilePath)
        {
            
            if (dictionary == null) dictionary = CreateDictionary(model);
            
            //Return PDF to PDF if template is PDF format
            if (templateFilePath.ToLower().Contains(".pdf")) return DocumentService.PdfToPdf(templateFilePath, dictionary);

            var imagesDictionary = CreateDictionaryImages(model);

            var productIds = model.Items.Select(s => s.ProductCode).Distinct().ToList();
            List<Product> products = new ProductService<EmptyViewModel>(model.CompanyID, null, null, DbContext).Read().Where(p => productIds.Contains(p.ProductCode)).DistinctBy(p => p.ProductCode).ToList();
            List<CustomField> customItemFields = new CustomProductFieldService<EmptyViewModel>(model.CompanyID, null, null, DbContext).Read().ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();

            

            byte[] byteArray = null;

            //Load the word documnet template
            using (FileStream stream = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    byteArray = binaryReader.ReadBytes((int)stream.Length);
                }
            }

            //Editing the document is done in memory to prevent the tempalte from being changed
            using (MemoryStream ms = new MemoryStream())
            {
                //Convert the word document into a WordprocessingDocument
                ms.Write(byteArray, 0, (int)byteArray.Length);
                WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(ms, true);
                Body body = wordprocessingDocument.MainDocumentPart.Document.Body;
                List<Table> tables = null;


                //var xml = body.InnerXml; 
                var inner = body.InnerText;

                ////First remove Totals Table if section number is one or less.
                //if (model.Sections.Count() <= 1)
                //{
                //    tables = body.Descendants<Table>().ToList();
                //    for (int x = tables.Count - 1; x >= 0; x--)
                //    {
                //        var innerText = tables[x].InnerText;

                //        if (System.Text.RegularExpressions.Regex.IsMatch(innerText, "{(Cost|Net|VAT|Margin|Discount|Total)")
                //            && !System.Text.RegularExpressions.Regex.IsMatch(innerText, "{(NotDel|Item|Business|Client|Invoice|Delivery|PoNumber|Contact|Notes|Review|Date|Associated)"))
                //        {
                //            tables[x].Remove();
                //        }
                //    }
                //}
                ////tables = null;

                //Get all the paragraphs from the body, headers and footers.
                List<Paragraph> paragraphs = body.Descendants<Paragraph>().ToList();
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in imagesDictionary)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            try
                            {
                                paragraphs[x].RemoveAllChildren<Run>();
                                Drawing pic = OpenXmlHelpers.CreateImageElement(dicItem.Value, wordprocessingDocument);
                                paragraphs[x].AppendChild(new Run(pic));
                            }
                            catch { }
                        }
                    }
                }

                foreach (var header in wordprocessingDocument.MainDocumentPart.HeaderParts)
                {
                    var headerParas = header.Header.Descendants<Paragraph>().ToList();
                    for (int x = headerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in imagesDictionary)
                        {
                            if (headerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    headerParas[x].RemoveAllChildren<Run>();
                                    Drawing pic = OpenXmlHelpers.CreateHeaderImageElement(dicItem.Value, header);
                                    headerParas[x].AppendChild(new Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(headerParas);
                }

                foreach (var footer in wordprocessingDocument.MainDocumentPart.FooterParts)
                {
                    var footerParas = footer.Footer.Descendants<Paragraph>().ToList();
                    for (int x = footerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in imagesDictionary)
                        {
                            if (footerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    footerParas[x].RemoveAllChildren<Run>();
                                    Drawing pic = OpenXmlHelpers.CreateFooterImageElement(dicItem.Value, footer);
                                    footerParas[x].AppendChild(new Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(footerParas);
                }

                /*
                //Second replace paragrph texts        
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    if (paragraphs[x].InnerText.Contains("{BusinessLogo}"))
                    {
                        //Replace logo tag with an image
                        try
                        {
                            paragraphs[x].RemoveAllChildren<Run>();
                            Drawing pic = OpenXmlHelpers.CreateImageElement(model.Company.LogoImage.FilePath, wordprocessingDocument);
                            paragraphs[x].AppendChild(new Run(pic));
                        }
                        catch { }
                    }
                }
                */

                OpenXmlHelpers.ReplaceParagraphTexts(paragraphs, dictionary);

                //var xml2 = body.InnerXml;

                //Third, Start constracting the items.
                tables = body.Descendants<Table>().ToList();
                for (int x = tables.Count - 1; x >= 0; x--)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(tables[x].InnerText, "{(Item|Section|Item)"))
                    {
                        string innerText = tables[x].InnerText;
                        bool sectionSpec = innerText.Contains("{SectionSpecification}"); //Check to see if table is a section spec table
                        bool spec = innerText.Contains("{Specification}"); //Check to see if table is a full spec table
                        bool pageBreak = innerText.Contains("{PageBreak}"); //Check added page break after every section
                        bool keeptogether = innerText.Contains("{KeepTogether}"); //Check to keep section tables as whole. (table rows do not spill over to next page.
                        bool keepSectionTotals = innerText.Contains("{KeepSectionTotals}") || model.Sections.Count() > 1; //Check to keep section total row in table if section count is one.
                        bool ignoreKeepTogether = innerText.Contains("{IgnoreKeepTogether}"); //Check to make all section table spill over.
                        bool separatePriceBreaks = System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Item(Qty|VAT|Discount|ListPrice|Price|Cost|Margin|MarginPrice|Total)\\d}");
                        int secNum = model.Sections.Count - 1;

                        innerText = null;

                        foreach (var section in model.Sections.OrderByDescending(s => s.SortPos))
                        {
                            var tableClone = tables[x].CloneNode(true) as Table;
                            //Get the table rows from the template. Go through the loop backwards to prevent change in array errors
                            List<TableRow> rows = tableClone.Descendants<TableRow>().ToList();
                            List<TableRow> rowTemps = new List<TableRow>();
                            

                            for (int y = rows.Count - 1; y >= 0; y--)
                            {
                                innerText = rows[y].InnerText;
                                //Get the rows  that have the text "{Item". These table rows will be duplicated.
                                if (innerText.Contains("{Item"))
                                {
                                    rowTemps.Insert(0, rows[y]);
                                }
                                else if (!keepSectionTotals 
                                        && System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Section(MarginPer|DiscountPer|Cost|Net|VAT|Margin|Discount|Total)}")
                                        && !System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Section(Name|IncludedInTotal|IncludedInTotalYesNo)}"))
                                {
                                    rows[y].Remove();
                                }
                            }

                            bool repeat = false;
                            IEnumerable<TLineItem> sectionItems;

                            if (sectionSpec)
                            {
                                sectionItems = section.Items.Where(s => /*s.ParentItemID == null &&*/ s.IsComment == false && s.HideOnPdf == false).OrderBy(s => s.SortPos).DistinctBy(s => s.ProductCode); //If section spec then only get the unqiue section items by code 
                            }
                            else if (spec)
                            {
                                sectionItems = model.Items.Where(s => /*s.ParentItemID == null &&*/ s.IsComment == false && s.HideOnPdf == false).OrderBy(s => s.SortPos).DistinctBy(s => s.ProductCode); //If full spec then only get the unqiue model items by code
                            }
                            else if (UsePriceBreaks(model) && !separatePriceBreaks)
                            {
                                List<TLineItem> items = new List<TLineItem>();

                                foreach (var i in section.Items.Where(s => s.ParentItemID == null && s.HideOnPdf == false).OrderBy(s => s.SortPos))
                                {
                                    items.Add(i);
                                    //items.AddRange(i.PriceBreaks.OrderBy(p => p.Quantity).Select(p => p.ToTLineItem()).ToList());
                                    items.AddRange(GetPriceBreaks(i));
                                }

                                sectionItems = items.AsEnumerable();

                            }
                            else
                            {
                                //sectionItems = section.Items.Where(s => s.ParentItemID == null).OrderBy(s => s.SortPos); //Get the items in the section
                                sectionItems = section.Items.Where(s => s.HideOnPdf == false).OrderBy(s => s.SortPos); //Get the items in the section
                            }

                            for (int z = 0; z < sectionItems.Count(); z++)
                            {
                                //if (z <= 0) repeat = false;
                                //else repeat = sectionItems.ElementAt(z).ProductCode == sectionItems.ElementAt(z - 1).ProductCode;
                                sectionItems.ElementAt(z).CustomFieldValues = sectionItems.ElementAt(z).CustomFieldValues.AddRemoveRelevent(customItemFields);

                                foreach (var row in rowTemps)
                                {
                                    var rowClone = row.CloneNode(true) as TableRow;
                                    tableClone.InsertBefore(ConstractWordTableRow(wordprocessingDocument, tableClone, rowClone, sectionItems.ElementAt(z), repeat, (sectionSpec || spec), model, products), rowTemps.Last());
                                }
                            }

                            foreach (var row in rowTemps)
                            {
                                row.Remove(); //Remove the template row
                            }


                            var sectionDic = CreateSectionDictionary(section, model.Currency.Symbol);
                            List<Paragraph> sectionParagraphs = tableClone.Descendants<Paragraph>().ToList();
                            OpenXmlHelpers.ReplaceParagraphTexts(sectionParagraphs, sectionDic, false);

                            if (keeptogether || (!ignoreKeepTogether && secNum > 0)) //Only keep pages together if stated or not ignore and not the first section.
                            {
                                foreach (var par in sectionParagraphs)
                                {
                                    ParagraphProperties parProperties = new ParagraphProperties();
                                    if (par.ParagraphProperties != null) parProperties = par.ParagraphProperties;
                                    else { par.Append(parProperties); }

                                    if ((!ignoreKeepTogether && secNum > 0))
                                    {
                                        parProperties.Append(new KeepNext());
                                    }
                                    else
                                    {
                                        parProperties.Append(new KeepLines());
                                        parProperties.Append(new KeepNext());
                                    }

                                }
                            }

                            tableClone = body.InsertAfter(tableClone, tables[x]);

                            //Add page breaks only if true and no the last section
                            if (pageBreak && secNum < model.Sections.Count - 1)
                            {
                                tableClone.InsertAfterSelf(new Paragraph(new Run(new Break() { Type = BreakValues.Page })));
                            }
                            //Add new line at end of table, Space them out a little.
                            else if (secNum < model.Sections.Count - 1 && (!spec || sectionSpec))
                            {
                                Run run = new Run(new Text { Text = "...." });
                                run.RunProperties = new RunProperties { FontSize = new FontSize { Val = "1" } };

                                Paragraph para = new Paragraph(run);
                                para.ParagraphProperties = new ParagraphProperties { SpacingBetweenLines = new SpacingBetweenLines { After = "0", Before = "0" }, ContextualSpacing = new ContextualSpacing { Val = false } };

                                tableClone.InsertAfterSelf(para);
                            }

                            secNum--;

                            if (spec) break; //only produce one table if specification
                        }
                        tables[x].Remove(); //Remove the template table
                    }
                }

                var fontList = wordprocessingDocument.MainDocumentPart.FontTablePart.Fonts.Elements<Font>().Select(f => f.Name);

                //Fourth, save document and convert to PDF.
                wordprocessingDocument.Save();
                //wordprocessingDocument.SaveAs("C:\\Users\\Zee\\Desktop\\test2.docx");
                wordprocessingDocument.Close();



                using (MemoryStream exportMs = new MemoryStream())
                {
                    Spire.Doc.Document document = new Spire.Doc.Document();

                    ms.Position = 0;
                    document.LoadFromStream(ms, Spire.Doc.FileFormat.Docx);
                    Spire.Doc.ToPdfParameterList toPdf = new Spire.Doc.ToPdfParameterList();
                    toPdf.PdfConformanceLevel = Spire.Pdf.PdfConformanceLevel.None;
                    var standardfonts = new string[] { "arial", "cambria", "candara", "comic", "consolas", "constantia", "corbel", "courier", "franklin", "gabriola", "georgia", "impact", "lucida", "microsoft", "segoe", "sitka", "tahoma", "times", "trebuchet", "webdings", "wingdings" };

                    foreach (var font in fontList.Where(i => standardfonts.Where(x => i.Value.ToLower().Contains(x)).Count() <= 0))
                    {
                        try { toPdf.EmbeddedFontNameList.Add(font); } catch { }
                    }
                    document.EmbedSystemFonts = true;
                    document.JPEGQuality = 75;
                    document.SaveToStream(exportMs, toPdf);
                    return exportMs.ToArray();
                }

            }
        }
        


        protected TableRow ConstractWordTableRow(WordprocessingDocument wordDoc, Table table, TableRow row, TLineItem item, bool repeat, bool spec, T model, List<Product> products)
        {

            //Get all the paragrpahs in the table row
            IEnumerable<Paragraph> paragraphs = row.Descendants<Paragraph>();

            //Because a paragraph has multiple runs. make sure all paragraphs has have a single run with a single text element.
            //E.g. <p> <run><text></text></run> <run></run> <run><text></text></run> </p> becomes:
            //     <p> <run><text></text></run> </p>

            foreach (var p in paragraphs)
            {
                List<Run> runs = p.Descendants<Run>().ToList();
                if (runs.Count <= 0)
                {
                    p.Append(new Run(new Text()));
                }
                else
                {
                    if (runs[0].Descendants<Text>().Count() <= 0)
                        runs[0].Append(new Text());

                    var txt = runs[0].Descendants<Text>().First();
                    for (int x = 1; x < runs.Count; x++)
                    {
                        txt.Text += runs[x].Descendants<Text>().DefaultIfEmpty(new Text()).FirstOrDefault().Text;
                        runs[x].Remove();
                    }
                }
            }

            IEnumerable<TableCell> cells = row.Descendants<TableCell>();

            foreach (var c in cells)
            {
                if (c.TableCellProperties == null) c.TableCellProperties = new TableCellProperties();
                var inner = c.InnerText;
                if (inner.Contains("{NoRepeat}") || (System.Text.RegularExpressions.Regex.IsMatch(inner, "{Item(Image|Code|Description|Unit)}") /* && model.UsePriceBreaks*/))
                {
                    if (repeat)
                    {
                        c.TableCellProperties.VerticalMerge = new VerticalMerge() { Val = MergedCellValues.Continue };
                        foreach (Run r in c.Descendants<Run>())
                        {
                            r.Remove();
                        }
                    }
                    else
                    {
                        c.TableCellProperties.VerticalMerge = new VerticalMerge() { Val = MergedCellValues.Restart };
                    }

                    foreach (Text txt in c.Descendants<Text>())
                    {
                        txt.Text = txt.Text.Replace("{NoRepeat}", "");
                    }
                }

                ReplaceTableCellImage(c, wordDoc, cells, table, item);
            };

            paragraphs = row.Descendants<Paragraph>();

            foreach (var p in paragraphs)
            {
                ReplaceTableRowParagrph(cells, p, wordDoc, table, row, item, repeat, spec, model, products);
            }
            
            return row;
        }

        protected virtual void ReplaceTableCellImage(TableCell cell, WordprocessingDocument wordDoc, IEnumerable<TableCell> cells, Table table, TLineItem item)
        {

            //Below, replace the tags with the correct text.
            //Replace the tag, remove all the runs in the paragraph with a single run.
            foreach(var paragraph in cell.Descendants<Paragraph>())
            {
                //Get all the image tags list
                var matches = System.Text.RegularExpressions.Regex.Matches(paragraph.InnerText, "{(ItemImage|ItemImage\\d)}");
                
                if (matches.Count > 0)
                {
                    try
                    {   
                        double colWidth = 0.0;
                        try
                        {
                            //Try get the column/cell width from the table column grids
                            for (int x = 0; x < cells.Count(); x++)
                            {
                                //if (cells.ElementAt(x).InnerText.Contains("{ItemImage"))
                                if (System.Text.RegularExpressions.Regex.IsMatch(cells.ElementAt(x).InnerText, "{(ItemImage|ItemImage\\d)}"))
                                {
                                    GridColumn colum = table.Descendants<GridColumn>().ElementAt(x);
                                    colWidth = double.Parse(colum.Width) / 1440.0 * 2.0;
                                    break;
                                }
                            }
                        }
                        catch { }

                        //Remove all image tags
                        foreach (Text txt in paragraph.Descendants<Text>()) {
                            txt.Text = System.Text.RegularExpressions.Regex.Replace(txt.Text, "{(ItemImage|ItemImage\\d)}", "");
                        }
                        if (item.IsComment) return;

                        TableCell tblCell = cells.First();

                        //if getting colWidth failed then just use the cell width
                        if (colWidth <= 0.0) colWidth = double.Parse(tblCell.TableCellProperties.TableCellWidth.Width) / 1440.0 * 2.0;
                        colWidth /= matches.Count; //split the col width by the number of tags in the paragraph
                        

                        var imageURL = "";
                        int num = 0;
                        //Go through all the matches and add the images.
                        foreach (System.Text.RegularExpressions.Match match in matches)
                        {
                            //Get the image number
                            num = 0;
                            try { num = int.Parse(System.Text.RegularExpressions.Regex.Match(match.Value, "\\d+").Value); } catch { } 
                            try
                            { 
                                if (num <= 1) imageURL = item.Images.OrderBy(x => x.SortPos).First().File.FilePath;
                                else imageURL = item.Images.OrderBy(x => x.SortPos).ElementAt(num - 1).File.FilePath;

                                Drawing pic = OpenXmlHelpers.CreateImageElement(imageURL, wordDoc, colWidth, true, true);
                                paragraph.Descendants<Run>().First().PrependChild(pic);
                            }
                            catch { }
                        }
                    }
                    catch { }
                    //return;
                }
            }
        }

        protected virtual void ReplaceTableRowParagrph(IEnumerable<TableCell> cells, Paragraph p, WordprocessingDocument wordDoc, Table table, TableRow row, TLineItem item, bool repeat, bool spec, T model, List<Product> products)
        {
            
            if (p.InnerText.Contains("{ItemCode}"))
            {
                UpdateTableCellText(p, item, "{ItemCode}", item.ProductCode);
            }

            if (p.InnerText.Contains("{ItemDescription}"))
            {
                UpdateTableCellText(p, item, "{ItemDescription}", item.Description);
            }

            if (p.InnerText.Contains("{ItemUnit}"))
            {
                UpdateTableCellText(p, item, "{ItemUnit}", item.Unit);
            }

            if (p.InnerText.Contains("{ItemCategory}"))
            {
                UpdateTableCellText(p, item, "{ItemCategory}", item.Category);
            }

            //if (p.InnerText.Contains("{ItemVendorCode}"))
            //{
            //    UpdateTableCellText(p, item, "{ItemVendorCode}", item.VendorCode);
            //}

            //if (p.InnerText.Contains("{ItemManufacturer}"))
            //{
            //    UpdateTableCellText(p, item, "{ItemManufacturer}", item.Manufacturer);
            //}


            if (item.CustomFieldValues != null)
            {
                foreach (var c in item.CustomFieldValues)
                {
                    if (p.InnerText.Contains("{Item" + c.Name + "}"))
                    {
                        UpdateTableCellText(p, item, "{Item" + c.Name + "}", c.Value);
                    }
                }
            }


            if (p.InnerText.Contains("{ItemDetailedDescription}"))
            {
                UpdateTableCellText(p, item, "{ItemDetailedDescription}", products.Where(q => q.ProductCode == item.ProductCode).Select(q => q.DetailedDescription).DefaultIfEmpty("").FirstOrDefault());
            }

            if (spec) return; //Do not add quantity or pricing information if a specification

            if (p.InnerText.Contains("{ItemQty"))
            {                
                UpdateTableCellText(p, item, "{ItemQty}", string.Format("{0}{1:#,##0.##}", (item.ParentItemID != null) ? "x " : "", item.Quantity), "", true);
            }

            if (p.InnerText.Contains("{ItemQtyDash"))
            {                
                UpdateTableCellText(p, item, "{ItemQtyDash}", string.Format("{0}{1:#,##0.##}", (item.ParentItemID != null) ? "- " : "", item.Quantity), "", true);
            }

            if (p.InnerText.Contains("{ItemQtyNone"))
            {                
                UpdateTableCellText(p, item, "{ItemQtyNon}", string.Format("{0}{1:#,##0.##}", (item.ParentItemID != null) ? "" : "", item.Quantity), "", true);
            }

            bool HidePrices = (item.ParentItem != null) ? (item.ParentItem.UseKitPrice) : (item.IsKit && !item.UseKitPrice);

            if (p.InnerText.Contains("{ItemVAT}"))
            {                
                UpdateTableCellText(p, item, "{ItemVAT}", item.VAT, HidePrices, "-", "{0:#,###.##}%");
            }

            if (p.InnerText.Contains("{ItemVATPrice}"))
            {                
                UpdateTableCellText(p, item, "{ItemVATPrice}", item.VATPrice, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemDiscount}"))
            {                
                UpdateTableCellText(p, item, "{ItemDiscount}", item.Discount, HidePrices, "-", "{0:#,###.##}%");
            }

            if (p.InnerText.Contains("{ItemListPrice}"))
            {                
                UpdateTableCellText(p, item, "{ItemListPrice}", item.ListPrice, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemPrice}"))
            {               
                UpdateTableCellText(p, item, "{ItemPrice}", item.Price, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemSetupCost}"))
            {                
                UpdateTableCellText(p, item, "{ItemSetupCost}", item.SetupCost, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemCost}"))
            {                
                UpdateTableCellText(p, item, "{ItemCost}", item.Cost, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemMargin}"))
            {                
                UpdateTableCellText(p, item, "{ItemMargin}", item.Margin, HidePrices, "-", "{0:#,##0.##}%");
            }

            if (p.InnerText.Contains("{ItemMarginPrice}"))
            {                
                UpdateTableCellText(p, item, "{ItemMarginPrice}", item.MarginPrice, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemGross}"))
            {                
                UpdateTableCellText(p, item, "{ItemGross}", item.Gross, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains("{ItemTotal}"))
            {                
                UpdateTableCellText(p, item, "{ItemTotal}", item.Total, HidePrices, "-", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            
            if (!UsePriceBreaks(model)) return;

            int num = 0;

            try { num = int.Parse(System.Text.RegularExpressions.Regex.Match(p.InnerText, "\\d+").Value); } catch{ }

            TLineItem price = GetPriceBreak(item, p.InnerText, num);
            
            if (p.InnerText.Contains(string.Format("{{ItemQty{0}}}", num)))
            {                
                UpdateTableCellText(p, item, string.Format("{{ItemQty{0}}}", num), (price != null) ? string.Format("{0:#,##0.##}", price.Quantity) : "", "", true);
            }

            if (p.InnerText.Contains(string.Format("{{ItemVAT{0}}}", num)))
            {
                UpdateTableCellText(p, item, string.Format("{{ItemVAT{0}}}", num), (price != null) ? price.VAT : 0, (price == null), "", "{0:#,###.##}%");
            }

            if (p.InnerText.Contains(string.Format("{{ItemVATPrice{0}}}", num)))
            {             
                UpdateTableCellText(p, item, string.Format("{{ItemVATPrice{0}}}", num), (price != null) ? price.VATPrice : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemDiscount{0}}}", num)))
            {               
                UpdateTableCellText(p, item, string.Format("{{ItemDiscount{0}}}", num), (price != null) ? price.Discount : 0, (price == null), "", "{0:#,###.##}%");
            }

            if (p.InnerText.Contains(string.Format("{{ItemListPrice{0}}}", num)))
            {                
                UpdateTableCellText(p, item, string.Format("{{ItemListPrice{0}}}", num), (price != null) ? price.ListPrice : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemPrice{0}}}", num)))
            {                
                UpdateTableCellText(p, item, string.Format("{{ItemPrice{0}}}", num), (price != null) ? price.Price : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemSetupCost{0}}}", num)))
            {                
                UpdateTableCellText(p, item, string.Format("{{ItemSetupCost{0}}}", num), (price != null) ? price.SetupCost : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemCost{0}}}", num)))
            {               
                UpdateTableCellText(p, item, string.Format("{{ItemCost{0}}}", num), (price != null) ? price.Cost : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemMargin{0}}}", num)))
            {               
                UpdateTableCellText(p, item, string.Format("{{ItemMargin{0}}}", num), (price != null) ? price.Margin : 0, (price == null), "", "{0:#,##0.##}%");
            }

            if (p.InnerText.Contains(string.Format("{{ItemMarginPrice{0}}}", num)))
            {               
                UpdateTableCellText(p, item, string.Format("{{ItemMarginPrice{0}}}", num), (price != null) ? price.MarginPrice : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemGross{0}}}", num)))
            {               
                UpdateTableCellText(p, item, string.Format("{{ItemGross{0}}}", num), (price != null) ? price.Gross : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }

            if (p.InnerText.Contains(string.Format("{{ItemTotal{0}}}", num)))
            {                
                UpdateTableCellText(p, item, string.Format("{{ItemTotal{0}}}", num), (price != null) ? price.Total : 0, (price == null), "", "{0:" + model.Currency.Symbol + "#,##0.#0}");
            }
        }



        protected virtual bool UsePriceBreaks(T model) { return false; }


        protected virtual TLineItem GetPriceBreak(TLineItem item, string innerText, int num)
        {
            return null;            
        }

        protected virtual ICollection<TLineItem> GetPriceBreaks(TLineItem item)
        {
            return new List<TLineItem>();
        }


        protected virtual void UpdateTableCellText(Paragraph p, TLineItem item, string tag, string text, /*IEnumerable<string> subStrings,*/ string empty = "", bool HideIfComment = false)
        {

            if (item.IsComment && HideIfComment)
            {
               p.RemoveAllChildren<Run>();
                return;
            }

            var run = p.Descendants<Run>().First();
            var txt = run.Descendants<Text>().First();

            try
            {
                string replacedText = txt.Text.Replace(tag, text ?? "");
                if (!string.IsNullOrEmpty(text) && text.Contains(string.Format("\n")))
                {
                    var split = text.Split(new string[] { string.Format("\n") }, StringSplitOptions.None);
                    for (int y = 0; y < split.Length; y++)
                    {
                        Text clone = txt.CloneNode(true) as Text;
                        clone.Text = split[y];
                        run.AppendChild(clone);
                        if (y < split.Length - 1)
                            run.AppendChild(new Break());
                    }
                    txt.Remove();
                }
                else
                {
                    txt.Text = replacedText;
                }

                if (item.IsComment)
                {
                    try { run.RunProperties.Bold = new Bold(); }
                    catch { }
                    return;
                }
            }
            catch { }
            /*RunProperties subRunPr = new RunProperties(); //Used for kit items
            try
            {
                //Try get the styling of the paragraph. Be sure to reduce the font size for the kit items
                subRunPr = run.RunProperties.CloneNode(true) as RunProperties;
                subRunPr.FontSize.Val = (int.Parse(subRunPr.FontSize.Val) - ((int.Parse(subRunPr.FontSize.Val) > 20) ? 8 : 4)).ToString();
                subRunPr.Italic = new Italic { Val = OnOffValue.FromBoolean(true) };
            }
            catch { subRunPr.FontSize = new FontSize { Val = "18" }; subRunPr.Italic = new Italic { Val = OnOffValue.FromBoolean(true) }; }


            foreach (var subItem in subStrings)
            {
                Paragraph para = p.Parent.AppendChild(p.CloneNode(true) as Paragraph);

                Run subRun = para.Descendants<Run>().First();
                txt = subRun.Descendants<Text>().First();
                txt.Text = originalText.Replace(tag, subItem);
                try
                {
                    subRun.RunProperties = subRunPr.CloneNode(true) as RunProperties;
                }
                catch { }
            }*/
        }


        protected virtual void UpdateTableCellText(Paragraph p, TLineItem item, string tag, decimal value, /*IEnumerable<decimal> subValues,*/ bool HidePrices, string empty = "-", string format = "{0:#,###.#0}")
        {
            if (item.IsComment)
            {
                p.RemoveAllChildren<Run>();
                return;
            }

            var run = p.Descendants<Run>().First();
            var txt = run.Descendants<Text>().First();

            var originalText = txt.Text;
            txt.Text = (!HidePrices) ? originalText.Replace(tag, string.Format(format, value)) : empty;

            /*RunProperties subRunPr = new RunProperties(); //Used for kit items
            try
            {
                //Try get the styling of the paragraph. Be sure to reduce the font size for the kit items
                subRunPr = run.RunProperties.CloneNode(true) as RunProperties;
                subRunPr.FontSize.Val = (int.Parse(subRunPr.FontSize.Val) - ((int.Parse(subRunPr.FontSize.Val) > 20) ? 8 : 4)).ToString();
                subRunPr.Italic = new Italic { Val = OnOffValue.FromBoolean(true) };
            }
            catch { subRunPr.FontSize = new FontSize { Val = "18" }; subRunPr.Italic = new Italic { Val = OnOffValue.FromBoolean(true) }; }


            foreach (var subItem in subValues)
            {
                Paragraph para = p.Parent.AppendChild(p.CloneNode(true) as Paragraph);

                Run subRun = para.Descendants<Run>().First();
                txt = subRun.Descendants<Text>().First();
                txt.Text = (!HidePrices) ? empty : originalText.Replace(tag, string.Format(format, subItem));

                try
                {
                    subRun.RunProperties = subRunPr.CloneNode(true) as RunProperties;
                }
                catch { }
            }*/
        }

        
        

        public virtual Dictionary<string, string> CreateDictionary(T model)
        {
            List<CustomField> customItems = new CustomFieldService<TCustomField, EmptyViewModel>(model.CompanyID, null, null, DbContext).Read().ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();
            model.CustomFieldValues = model.CustomFieldValues.AddRemoveRelevent(customItems);

            //Create a dictionary with all the relevant quotation information.
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("{BusinessName}", model.Company.Name);
            dic.Add("{BusinessMotto}", model.Company.Motto);
            dic.Add("{BusinessAddress1}", model.Company.Address1);
            dic.Add("{BusinessAddress2}", model.Company.Address2);
            dic.Add("{BusinessAddress3}", model.Company.Address3);
            dic.Add("{BusinessTown}", model.Company.Town);
            dic.Add("{BusinessCounty}", model.Company.County);
            dic.Add("{BusinessPostcode}", model.Company.Postcode);
            dic.Add("{BusinessCountry}", model.Company.Country);
            dic.Add("{BusinessMobile}", model.Company.Mobile);
            dic.Add("{BusinessTelephone}", model.Company.Telephone);
            dic.Add("{BusinessEmail}", model.Company.Email);
            dic.Add("{BusinessWebsite}", model.Company.Website);
            dic.Add("{BusinessFax}", model.Company.Fax);
            dic.Add("{BusinessVAT}", model.Company.VatNumber);
            dic.Add("{BusinessRegistrationNumber}", model.Company.RegNumber);
            

            dic.Add( string.Format("{{{0}Number}}", typeof(T).Name), model.ToShortLabel());
            dic.Add("{Description}", model.Description);
            dic.Add("{ContactName}", (model.AssignedUser != null) ? string.Format("{0} {1}", model.AssignedUser.FirstName, model.AssignedUser.LastName) : string.Empty);
            dic.Add("{ContactTelephone}", (model.AssignedUser != null) ? model.AssignedUser.PhoneNumber : string.Empty);
            dic.Add("{ContactEmail}", (model.AssignedUser != null) ? model.AssignedUser.Email : string.Empty);
            dic.Add("{Notes}", model.Note);
            dic.Add("{DateCreated}", model.Created.ToString("dd/MM/yyyy"));
            dic.Add("{DateCreatedLong}", model.Created.ToString("dd MMMM yyyy"));

            //if (!model.UsePriceBreaks)
            if (model.Currency != null)
            {
                dic.Add("{MarginPer}", string.Format("{0:#,###.##}%", model.Margin));
                dic.Add("{DiscountPer}", string.Format("{0:#,###.##}%", model.Discount));
                dic.Add("{Cost}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalCost));
                dic.Add("{Net}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalNet));
                dic.Add("{NetBeforeDiscount}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalNet + model.TotalDiscount));
                dic.Add("{VAT}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalVat));
                dic.Add("{Margin}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalMargin));
                dic.Add("{Discount}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalDiscount));
                dic.Add("{Total}", string.Format("{0:" + model.Currency.Symbol + "#,##0.#0}", model.TotalPrice));

                dic.Add("{ItemsMarginPer}", dic["{MarginPer}"]);
                dic.Add("{ItemsDiscountPer}", dic["{DiscountPer}"]);
                dic.Add("{ItemsCost}", dic["{Cost}"]);
                dic.Add("{ItemsNet}", dic["{Net}"]);
                dic.Add("{ItemsVAT}", dic["{VAT}"]);
                dic.Add("{ItemsMargin}", dic["{Margin}"]);
                dic.Add("{ItemsDiscount}", dic["{Discount}"]);
                dic.Add("{ItemsTotal}", dic["{Total}"]);
            }

            if (model.CustomFieldValues != null)
            {
                foreach (var c in model.CustomFieldValues)
                {
                    if (!dic.ContainsKey("{" + c.Name + "}"))
                        dic.Add("{" + c.Name + "}", c.Value);
                }
            }

            /*
            dic.Add("{AgreementName}", (model.SignedAgreement != null) ? model.SignedAgreement.FullName() : "");
            dic.Add("{AgreementTitle}", (model.SignedAgreement != null) ? model.SignedAgreement.Title : "");
            dic.Add("{AgreementFirstName}", (model.SignedAgreement != null) ? model.SignedAgreement.FirstName : "");
            dic.Add("{AgreementLastName}", (model.SignedAgreement != null) ? model.SignedAgreement.LastName : "");
            dic.Add("{AgreementDateSigned}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("dd/MM/yyyy") : "");
            dic.Add("{AgreementDateSignedLong}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("dd MMMM yyyy") : "");
            dic.Add("{AgreementDateTime}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("HH:mm") : "");
            dic.Add("{AgreementEmail}", (model.SignedAgreement != null) ? model.SignedAgreement.Email : "");
            */

            dic.Add("{NotDel}", "");
            dic.Add("{ReviewLink}", string.Format("{0}/{1}Review/?ID={2}&Jasq={3}", FileService.WebURL, typeof(T).Name, model.ID, model.CheckSum));

            return dic;
        }


        public virtual  Dictionary<string, string> CreateSectionDictionary(TSection model, string currencySymbol)
        {
            //Create a dictionary with all the relevant quotation information.
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("{SectionName}", model.Name ?? "");

            //if (model.T != null && !model.T.UsePriceBreaks)
           // {
                dic.Add("{SectionMarginPer}", string.Format("{0:#,###.##}%", model.Margin));
                dic.Add("{SectionDiscountPer}", string.Format("{0:#,###.##}%", model.Discount));
                dic.Add("{SectionCost}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalCost));
                dic.Add("{SectionNet}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalNet));
                dic.Add("{SectionNetBeforeDiscount}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalNet + model.TotalDiscount));
                dic.Add("{SectionVAT}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalVat));
                dic.Add("{SectionMargin}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalMargin));
                dic.Add("{SectionDiscount}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalDiscount));
                dic.Add("{SectionTotal}", string.Format("{0:" + currencySymbol + "#,##0.#0}", model.TotalPrice));

                dic.Add("{SectionIncludedInTotal}", model.IncludeInTotal ? "Included In Total" : "");
                dic.Add("{SectionIncludedInTotalYesNo}", model.IncludeInTotal ? "Yes" : "No");
            //}

            dic.Add("{PageBreak}", "");
            dic.Add("{KeepTogether}", "");
            dic.Add("{IgnoreKeepTogether}", "");
            dic.Add("{SectionSpecification}", "");
            dic.Add("{Specification}", "");
            dic.Add("{KeepSectionTotals}", "");

            return dic;
        }

        public virtual Dictionary<string, string> CreateDictionaryImages(T model)
        {
            //Create a dictionary with all the relevant quotation information.
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("{BusinessLogo}", (model.Company.LogoImage != null) ? model.Company.LogoImage.FilePath : "");
            //dic.Add("{AgreementSignature}", (model.SignedAgreement != null) ? model.SignedAgreement.SignatureImageURL : "");
            return dic;
        }



        public virtual byte[] GenerateSamplePreview(string filePath)
        {
            ICollection<TLineItemImage> images = new List<TLineItemImage>() { new TLineItemImage { File = new FileEntry { FilePath = "/Templates/All/box.png" } } };
            var section1 = new TSection { Name = "Section 1", SortPos = 0, IncludeInTotal = true };            
            section1.Items = new List<TLineItem>() {
                    new TLineItem { ProductCode = "CODE1", Description = "Line Item 1", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE2", Description = "Line Item 2", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE3", Description = "Line Item 3", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE4", Description = "Line Item 4", Unit = "Unit", Images = images }
                };
            var section2 = new TSection { Name = "Section 2", SortPos = 1, IncludeInTotal = true };
            section2.Items = new List<TLineItem>() {
                    new TLineItem { ProductCode = "CODE1", Description = "Line Item 1", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE2", Description = "Line Item 2", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE3", Description = "Line Item 3", Unit = "Unit", Images = images },
                    new TLineItem { ProductCode = "CODE4", Description = "Line Item 4", Unit = "Unit", Images = images }
                };

            var client = new Client
            {
                Name = "Client Name",
                Address1 = "Address Line 1",
                Address2 = "Address Line 2",
                Town = "Town",
                Postcode = "POSTCODE",
                Telephone = "012314235654",
                Email = "email@email.com",
            };

            var order = new T
            {
                CompanyID = CompanyID,
                Company = new CompanyService<EmptyViewModel>(null, null, DbContext).FindByID(CompanyID),
                //Client = client,
                //Supplier = supplier,
                //Sections = new List<TSection>() { section1, section2 },
                //Items = new List<TLineItem>() { },
                Currency = new Currency { Symbol = "£" },
            };

            var propInfo = order.GetType().GetProperty("Client"); if (propInfo != null) propInfo.SetValue(order, client, null);
            propInfo = order.GetType().GetProperty("Sections"); if (propInfo != null) propInfo.SetValue(order, new List<TSection>() { section1, section2 }, null);
            propInfo = order.GetType().GetProperty("Items"); if (propInfo != null) propInfo.SetValue(order, new List<TLineItem>() { }, null);

            propInfo = section1.GetType().GetProperty(typeof(T).Name); if (propInfo != null) propInfo.SetValue(section1, order, null);
            propInfo = section2.GetType().GetProperty(typeof(T).Name); if (propInfo != null) propInfo.SetValue(section2, order, null);            


            var extension = System.IO.Path.GetExtension(filePath);
            var dictionary = CreateDictionary(order);

            if (extension == ".pdf")
                return DocumentService.PdfToPdf(filePath, dictionary);
            else
                return GenerateBodyPDF(order, dictionary, filePath);

        }
    }
    
}
