﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Reflection;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace SnapSuite.Models
{
    
    public class OrderAgreementService<TOrder, T, TViewModel> : ModelViewService<T, TViewModel>
        where TOrder : class, IEntity, ICompanyEntity, ILastUpdated, ILoggable, new()
        where T : class, IAgreement, ICompanyEntity, ILoggable, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string OrderName { get { return typeof(TOrder).Name; } }
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string OrderIdColumn { get { return string.Format("{0}ID", typeof(TOrder).Name); } }

        // <summary> The Company ID value.</summary>
        public virtual Guid CompanyID { get; set; }

        public override string LoggingName() { return "Signed Agreement"; }
        public override string LoggingNamePlural() { return "Signed Agreement"; }


        public OrderAgreementService(Guid CompanyID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(UserID, mapper, db)
        {
            this.CompanyID = CompanyID;
        }


        public virtual T FindByID(Guid OrderID)
        {
            return Read().Where(i => i.CompanyID == CompanyID)
                                .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", OrderIdColumn, OrderID))
                                .DefaultIfEmpty(null).FirstOrDefault();
        }


        public T AcceptAgreement(Guid Id, string title, string firstName, string lastName, string email,
                                 string deviceInfo, string ipAddress, string city, string country, string latitude,
                                 string longitude, string filename, string extension, byte[] uploadSignature, bool handDrawnSignature, string PoNumber = null)
        {

            var model = new GenericService<TOrder>(DbContext).Read().Where(i => i.ID == Id).First();

            return AcceptAgreement(model, title, firstName, lastName, email, deviceInfo, ipAddress, city, country, latitude, longitude, filename, extension, uploadSignature, handDrawnSignature, PoNumber);
        }


        public virtual T AcceptAgreement(TOrder model, string title, string firstName, string lastName, string email,  
                                 string deviceInfo, string ipAddress, string city, string country, string latitude, 
                                 string longitude, string filename, string extension, byte[] uploadSignature, bool handDrawnSignature, string PoNumber = null)
        {
            
            CompanyID = model.CompanyID;
            
            //Check to see if form submitted is valid
            if (CompanyID == Guid.Empty) throw new Exception("Company ID has not been set.");
            if ((uploadSignature == null || uploadSignature.Length <= 0)) throw new Exception("No signature was uploaded.");
            if (uploadSignature.Length > 1 * 1024 * 1024) throw new Exception("Uploaded signature is too big.");

            if (string.IsNullOrWhiteSpace(title)) throw new Exception("The Title field is required.");
            if (string.IsNullOrWhiteSpace(firstName)) throw new Exception("The First Name field is required.");
            if (string.IsNullOrWhiteSpace(lastName)) throw new Exception("The Last Name field is required.");
            if (string.IsNullOrWhiteSpace(email)) throw new Exception("The Email field is required.");
            if (new EmailAddressAttribute().IsValid(email) == false) throw new Exception("The Email is not valid.");

            var fileService = new FileService(DbContext);
            var signatureFile = fileService.SaveFile(CompanyID, uploadSignature, filename, extension, "/Signatures/");

            var agreement = new T
            {
                //OrderID = Id,
                CompanyID = CompanyID,
                Title = title,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                DateSigned = DateTime.Now,
                DeviceInformation = deviceInfo,
                IP_Address = ipAddress,
                City = city,
                Country = country,
                Latitude = latitude,
                Longitude = longitude,
                SignatureImageURL = signatureFile.FilePath,
                SignatureImageID = signatureFile.ID,
                SignedUsingInput = handDrawnSignature,
            };

            var pInfo = agreement.GetType().GetProperty(OrderIdColumn);
            if (pInfo != null) pInfo.SetValue(agreement, model.ID, null);

            agreement.CreateMD5Hash();
            Create(agreement);

            OnAgreementCreated(agreement, PoNumber);

            var saveName = "";
            var pdfBytes = GeneratePDF(model.ID, ref saveName);
            for (int t = 0; t < 4; t++)
            {
                try
                {
                    var agreementFile = fileService.SaveFile(CompanyID, pdfBytes, "agreement", ".pdf", "/Signatures/");
                    agreement.PdfAgreementURL = agreementFile.FilePath;
                    agreement.PdfAgreementID = agreementFile.ID;
                    agreement.PdfAgreementFilename = string.Format("{0}.pdf", saveName);
                    Update(agreement, new string[] { "PdfAgreementID", "PdfAgreementURL", "PdfAgreementFilename" });
                    break;
                }
                catch { System.Threading.Thread.Sleep(500); }
            }

            
            string quoteTitle = "";
            var quoteBytes = GetOrderPDF(model.ID, ref quoteTitle);

            for (int t = 0; t < 4; t++)
            {
                try
                {
                    var approvedFile = fileService.SaveFile(CompanyID, quoteBytes, "order", ".pdf", "/Signatures/");
                    agreement.PdfApprovedURL = approvedFile.FilePath;
                    agreement.PdfApprovedID = approvedFile.ID;
                    agreement.PdfApprovedFilename = quoteTitle + ".pdf";
                    Update(agreement, new string[] { "PdfApprovedID", "PdfApprovedURL", "PdfApprovedFilename" });
                    break;
                }
                catch { System.Threading.Thread.Sleep(500); }
            }

            SendAgreement(agreement);

            return agreement;
        }



        public virtual byte[] GetOrderPDF(Guid id, ref string filename)
        {
            throw new NotImplementedException();
        }

        public virtual void OnAgreementCreated(T model, string PoNumber)
        {
            throw new NotImplementedException();
        }

        public virtual void SendAgreement(T model)
        {
            throw new NotImplementedException();
        }


        public virtual  byte[] GeneratePDF(Guid id, ref string filename)
        {
            var agreement = FindByID(id);
            var dictionary = CreateDictionary(agreement);
            dictionary.Add("{OrderNumber}", agreement.ToLongLabel());
            var imageDictionary = CreateDictionaryImages(agreement);

            filename = string.Format("Signed {0} Agreement - {1}", typeof(T).Name, agreement.ToShortLabel());
            
            Assembly assembly = Assembly.GetAssembly(typeof(EmailMessageService));
            Stream stream = assembly.GetManifestResourceStream("SnapSuite.Models.Resources.AgreementTemplate.docx");

            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                ms.ToArray();
                return DocumentService.GeneratePdfFromDocx(ms.ToArray(), dictionary, imageDictionary);
            }
        }


        public virtual  Dictionary<string, string> CreateDictionary(T model)
        {
            //Create a dictionary with all the relevant quotation information.
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("{OrderType}", typeof(T).Name);
            //dic.Add("{OrderNumber}", model.Quotation.ToLongLabel());
            dic.Add("{FullName}", string.Format("{0} {1} {2}", model.Title, model.FirstName, model.LastName));
            dic.Add("{Title}", model.Title ?? "");
            dic.Add("{FirstName}", model.FirstName ?? "");
            dic.Add("{LastName}", model.LastName ?? "");
            dic.Add("{Email}", model.Email ?? "");
            dic.Add("{DateSigned}", model.DateSigned.ToString("HH:mm dd/MM/yyyy") ?? "");
            dic.Add("{DeviceInformation}", model.DeviceInformation ?? "");
            dic.Add("{IP_Address}", model.IP_Address ?? "");
            dic.Add("{City}", model.City ?? "");
            dic.Add("{Country}", model.Country ?? "");
            dic.Add("{Latitude}", model.Latitude ?? "");
            dic.Add("{Longitude}", model.Longitude ?? "");
            dic.Add("{MD5Hash}", model.MD5Hash ?? "");

            dic.Add("{HandDrawnSignature}", model.SignedUsingInput ? "No" : "Yes");

            return dic;
        }

        public virtual Dictionary<string, string> CreateDictionaryImages(T model)
        {
            //Create a dictionary with all the relevant quotation information.
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("{SignatureImage}", model.SignatureImageURL);
            return dic;
        }


        //public override void OnDestroy(T model)
        //{
        //    var fileService = new FileService(DbContext);
        //    fileService.DeleteFile(model.PdfAgreement.ID);
        //    fileService.DeleteFile(model.PdfApproved.ID);
        //    fileService.DeleteFile( model.SignatureImage.ID);

        //    base.OnDestroy(model);
        //}


        //public override void OnDestroy(IEnumerable<T> models)
        //{
        //    var fileService = new FileService(DbContext);
        //    foreach (var m in models)
        //    {
        //        fileService.DeleteFile(m.PdfAgreement.ID);
        //        fileService.DeleteFile(m.PdfApproved.ID);
        //        fileService.DeleteFile(m.SignatureImage.ID);
        //    }
        //    base.OnDestroy(models);
        //}


        //public override void OnCreate(T model) { }
        //public override void OnCreate(IEnumerable<T> models) { }
        //public override void OnUpdate(T model) { }
        //public override void OnUpdate(IEnumerable<T> models) { }
        //public override void OnDestroy(T model) { }
        //public override void OnDestroy(IEnumerable<T> models) { }

    }


}
