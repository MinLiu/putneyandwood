﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{
    
    public class OrderWithCostsLabourService<T, TCustomField, TViewModel, TSection, TLineItem, TLineItemImage, TCostItem, TLabourItem, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification> 
                              : OrderService<T, TCustomField, TViewModel, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification>
        where T : class, IOrderWithCostsLabour<T, TSection, TLineItem, TLineItemImage, TCostItem, TLabourItem, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>, new()
        where TCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        where TViewModel : class, IEntityViewModel, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TCostItem : class, IOrderCostItem<TCostItem>, new()
        where TLabourItem : class, IOrderLabourItem, new()
        where TAttachment : class, IAttachment, IShowInPreview, new()
        where TComment : class, IComment, new()
        where TNotification : class, INotification, new()
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>
        where TSettings : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TDefaultAttachment : class, IAttachment, IShowInPreview, new()
        where TDefaultNotification : class, INotification, new()
    {        
        
        public OrderWithCostsLabourService(Guid CompanyID, string UserID, OrderModelMapper<T, TCustomField, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserID, mapper, db)
        {

        }

        public virtual new T New(Guid companyID, string userID, Guid projectID)
        {
           //Get Last Order number based on Settings
            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == companyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, TViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(companyID);
            if (company == null || settings == null) return null;

            var newOrder = Create1_ReturnNew(company, user, settings, projectID);
            Create(newOrder);
            Create2_AddItems(newOrder, company, user, settings);
            Create3_AddPreviews(newOrder, company, user, settings);
            Create4_AddDefaultAttahments(newOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Create6_AddLogs(newOrder, company, user, settings);
            Create7_SendNotifications(newOrder, company, user, settings);
            Create8_StripePayments(newOrder, company, user, settings);

            return Find(newOrder.ID);
        }
        

        public virtual new T Copy(Guid originalID, string userID)
        {
            //Get original order
            var originalOrder = Read().Where(i => i.ID == originalID)
                                      .Include(i => i.Sections)
                                      .Include(i => i.Items)
                                      .Include(i => i.Attachments)
                                      .Include(i => i.Notifications)
                                      .Include(i => i.PreviewItems)
                                      //.AsNoTracking()
                                      .DefaultIfEmpty(null)
                                      .FirstOrDefault();

            if (originalOrder == null) return null;

            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == CompanyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, TViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(CompanyID);
            if (company == null || settings == null) return null;

            var newOrder = Copy1_ReturnNew(originalOrder, user, settings);
            Create(newOrder);

            Copy2_AddItems(newOrder, originalOrder, company, user, settings);
            Copy2_AddCostItems(newOrder, originalOrder, company, user, settings);
            Copy2_AddLabour(newOrder, originalOrder, company, user, settings);
            Copy3_AddAttachments(newOrder, originalOrder, company, user, settings);
            Copy4_AddComments(newOrder, originalOrder, company, user, settings);
            Copy5_AddPreviews(newOrder, originalOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Copy6_AddLogs(newOrder, originalOrder, company, user, settings);
            Copy7_SendNotifications(newOrder, originalOrder, company, user, settings);
            Copy8_StripePayments(newOrder, originalOrder, company, user, settings);

            return Find(newOrder.ID);
        }
        

        protected virtual void Copy2_AddCostItems(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var costItemService = new GenericService<TCostItem>(DbContext);
            var costItems = originalOrder.CostItems.ToList();

            foreach (var i in costItems)
            {
                DbContext.Entry(i).State = EntityState.Detached;
                var propInfo = i.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(i, order.ID, null);
                //New Guid assign below
            }

            //Because items (kits) can have sub items. We have to add them with differently
            foreach (var i in costItems)
            {
                if (i.IsKit)
                {
                    //If the item is a kit then:
                    //1. Get old ID
                    //2. Add item then save to get new ID
                    //3. Get sub items, update parent ID then add them too
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();
                    costItemService.Create(i);

                    foreach (var sub in costItems.Where(s => s.ParentCostItemID == oldID))
                    {
                        sub.ParentCostItemID = i.ID;
                        sub.ID = Guid.NewGuid();
                        costItemService.Create(sub);
                    }
                }
                else if (i.ParentCostItemID != null)
                {
                    //Do nothing. Because they should added with the above code.
                }
                else
                {
                    //Normal non-kit itme are added directly.
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();
                    costItemService.Create(i);
                }
            }
        }

        protected virtual void Copy2_AddLabour(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var labourService = new GenericService<TLabourItem>(DbContext);
            var labours = originalOrder.LabourItems.ToList();

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var a in labours)
            {
                DbContext.Entry(a).State = EntityState.Detached;
                var propInfo = a.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(a, order.ID, null);
                a.ID = Guid.NewGuid();
            }

            labourService.Create(labours);
        }


        public override decimal CalculateTotalOtherCosts(Guid ID)
        {
            decimal total_vat = 0.0m;
            decimal total_net = 0.0m;
            var costItems = new GenericService<TCostItem>(DbContext).Read()
                                                                            .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(T).Name), ID))
                                                                            .ToList();

            foreach (var item in costItems)
            {
                if ((item.IsKit && !item.UseKitPrice) || item.IsComment)
                {
                    //Do nothing
                }
                else if (item.ParentCostItem != null)
                {
                    if (!item.ParentCostItem.UseKitPrice)
                    {
                        var net = decimal.Round(item.Price * item.Quantity * item.ParentCostItem.Quantity + item.SetupCost, 2);
                        total_net += net;
                        total_vat += decimal.Round((net * (item.VAT / 100.0m)), 2);
                    }
                }
                else
                {
                    var net = decimal.Round(item.Price * item.Quantity + item.SetupCost, 2);
                    total_net += net;
                    total_vat += decimal.Round((net * (item.VAT / 100m)), 2);
                }
            }
            
            return decimal.Round(total_net + total_vat, 2);            
        }


        public override decimal CalculateTotalLabourCosts(Guid ID)
        {
            var labourItems = new GenericService<TLabourItem>(DbContext).Read()
                                                                              .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(T).Name), ID))
                                                                              .ToList();

            if (labourItems.Count() > 0) return labourItems.Sum(i => i.Total);
            else return 0;
        }
        

    }
    

}
