﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.IO;

namespace SnapSuite.Internal
{

    public class OrderTemplateService<T, TViewModel> : EntityService<T, TViewModel>
        where T : class, IOrderPreviewTemplate, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public virtual Guid CompanyID { get; set; }
        
        public override string LoggingName() { return "Template"; }
        public override string LoggingNamePlural() { return "Templates"; }        

        public int MaxContentLengthInMbs { get; set; }
        public string RelativeSavePath { get; set; }
        //public override string[] ReferencesToInclude { get { return new string[] { "PreviewItems" } } }
        

        public OrderTemplateService(Guid CompanyID, string userID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(userID, mapper, dbContext)
        {
            this.CompanyID = CompanyID;
            RelativeSavePath = "/Templates/";
            MaxContentLengthInMbs = 20;
        }


        public virtual IQueryable<T> Read(Guid CompanyID)
        {
            return base.Read().Where(i => i.CompanyID == CompanyID).Include(i => i.File);
        }

        public override IQueryable<T> Read()
        {
            return Read(CompanyID);
        }


        public string GetTemplateVirtualFilePath(TemplateFormat format)
        {
            return  FileService.GetUploadVirtualFilePath(CompanyID, RelativeSavePath, (format == TemplateFormat.Docx) ? "_template.docx" : "_template.pdf");
        }

        public string GetTemplatePhysicalFilePath(TemplateFormat format)
        {
            return FileService.GetUploadPhysicalFilePath(CompanyID, RelativeSavePath, (format == TemplateFormat.Docx) ? "_template.docx" : "_template.pdf");
        }


        public TemplateFormat UploadTemplate(Guid CompanyID, byte[] file, string filename)
        {
            this.CompanyID = CompanyID;
            return UploadTemplate(file, filename);
        }


        public virtual TemplateFormat UploadTemplate(byte[] file, string filename)
        {
            var extension = Path.GetExtension(filename);
            if (extension != ".docx" && extension != ".pdf") throw new Exception("Uploaded template is not the file correct format.");
            new FileService(DbContext, MaxContentLengthInMbs).SaveFileWithoutEntry(CompanyID, file, RelativeSavePath, "_template");
            return (extension == ".docx") ? TemplateFormat.Docx : TemplateFormat.Pdf;
        }

        public TemplateFormat UploadTemplate(Guid CompanyID, HttpPostedFileBase file)
        {
            this.CompanyID = CompanyID;
            return UploadTemplate(file);
        }


        public virtual TemplateFormat UploadTemplate(HttpPostedFileBase file)
        {
            var extension = Path.GetExtension(file.FileName);
            if (extension != ".docx" && extension != ".pdf") throw new Exception("Uploaded template is not the file correct format.");
            new FileService(DbContext, MaxContentLengthInMbs).SaveFileWithoutEntry(CompanyID, file, RelativeSavePath, "_template");
            return (extension == ".docx") ? TemplateFormat.Docx : TemplateFormat.Pdf;
        }
        
        
        public virtual T CompleteTemplateUpload(TemplateFormat format, TemplateType type, string name)
        {
            string filePath = GetTemplatePhysicalFilePath(format);
            string newFilePath = filePath.Replace("_template", Guid.NewGuid().ToString());
            System.IO.File.Copy(filePath, newFilePath);

            var upload = new T
            {
                CompanyID = CompanyID,
                Type = type,
                Filename = name,
                Format = format,
                File = new FileEntry {
                    FilePath = FileService.ToVirtualPath(newFilePath),
                    FileExtension = (format == TemplateFormat.Docx) ? ".docx" : ".pdf",
                    FileFormat = (format == TemplateFormat.Docx) ? FileFormat.Docx : FileFormat.Pdf,
                    FileSize = (int)new System.IO.FileInfo(newFilePath).Length
                }
            };

            return Create(upload);
        }



        public override T Destroy(T entity)
        {
            DestroySet(base.Read().Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(base.Read().Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<T> set)
        {
            set = set.Include("PreviewItems");
            var list = set.ToList();
            base.Destroy(set.ToList());
        }



    }

}
