﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public class OrderLineItemPriceBreakService<T, TLineItem, TLineItemImage, TSection,  TPriceBreak, TPriceBreakViewModel> : ChildEntityService<TLineItem, TPriceBreak, TPriceBreakViewModel>
        where T : class, IEntity, ICurrency, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, IEntity, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TPriceBreak : class, IEntity, IPrice, new()
        where TPriceBreakViewModel : class, IEntityViewModel, new()
    {       

        public virtual bool SupportsKits { get { return true; } private set {  } }
        public override string LoggingName() { return "Price Break"; }
        public override string LoggingNamePlural() { return "Price Break"; }

        public override bool ApplyParentCompanyID { get { return false; } }


        public OrderLineItemPriceBreakService(Guid CompanyID, Guid OrderID, string UserId, ModelMapper<TPriceBreak, TPriceBreakViewModel> mapper, DbContext db)
            : base(CompanyID, OrderID, UserId, mapper, db)
        {
            
        }


        public override TPriceBreak Update(TPriceBreak entity, string[] updatedColumns = null) { return Update(entity, true, updatedColumns); }

        public virtual TPriceBreak Update(TPriceBreak entity, bool autoAdjust = true, string[] updatedColumns = null)
        {
            return UpdateValues(entity, autoAdjust);
        }


        public override IEnumerable<TPriceBreak> Update(IEnumerable<TPriceBreak> entities, string[] updatedColumns = null) { return Update(entities, true, updatedColumns); }

        public virtual IEnumerable<TPriceBreak> Update(IEnumerable<TPriceBreak> entities, bool autoAdjust = true, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                UpdateValues(entity, autoAdjust);
            }
            return entities;
        }



        public override TPriceBreakViewModel Update(TPriceBreakViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, true, updatedColumns); }

        public override TPriceBreakViewModel Update(TPriceBreakViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModel, true, refreshModelFromDb, updatedColumns); }

        public virtual TPriceBreakViewModel Update(TPriceBreakViewModel viewModel, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), autoAdjust, updatedColumns);
            return MapToViewModel(model);
        }


        public override IEnumerable<TPriceBreakViewModel> Update(IEnumerable<TPriceBreakViewModel> viewModels, string[] updatedColumns = null) { return Update(viewModels, true, true, updatedColumns); }

        public override IEnumerable<TPriceBreakViewModel> Update(IEnumerable<TPriceBreakViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModels, true, refreshModelFromDb, updatedColumns); }

        public virtual IEnumerable<TPriceBreakViewModel> Update(IEnumerable<TPriceBreakViewModel> viewModels, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), autoAdjust, updatedColumns);
            return MapToViewModel(models);
        }


        public virtual TPriceBreak UpdateValues(Guid ID, bool autoAdjust = true)
        {
            var query = Read().Where(i => i.ID == ID);
            if (query.Count() <= 0) return null;
            else return UpdateValues(query.First(), autoAdjust);
        }


        public virtual TPriceBreak UpdateValues(TPriceBreak model,  bool autoAdjust = true)
        {
            var priceService = new PriceEntityService<TPriceBreak>(DbContext);
            var updated = priceService.UpdateValues(model, autoAdjust, true);

            //if (UpdateParent) OnValuesUpdated(model);

            return updated;
        }


        public virtual void OnValuesUpdated(TPriceBreak model)
        {
            throw new NotImplementedException("OnValuesUpdated function has not been overwritten.");
        }




        public virtual TPriceBreak AddPriceBreak(Guid orderID, Guid itemID, decimal quantity)
        {
           

            var productService = new ProductService<EmptyViewModel>(CompanyID, null, null, DbContext);            
            var model = new GenericService<TLineItem>(DbContext).Read().Where(p => p.ID == itemID).First();
            var currencyID = new GenericService<T>(DbContext).Read().Where(p => p.ID == orderID).Select(i => i.CurrencyID).First();

            var find = productService.FindProduct(model.ProductCode);
            

            var priceBreak = new TPriceBreak {  Quantity = quantity };
            var pInfo = priceBreak.GetType().GetProperty(ParentIdColumn);
            if (pInfo != null) pInfo.SetValue(priceBreak, itemID, null);

            if (find != null)
            {
                var product = productService.GetProduct(find.ID, quantity, currencyID);
                priceBreak.VAT = product.VAT;
                priceBreak.ListPrice = product.Price;
                priceBreak.Price = product.Price;
                priceBreak.Cost = product.Cost;
            }
            else
            {
                priceBreak.VAT = model.VAT;
                priceBreak.ListPrice = model.ListPrice;
                priceBreak.Price = model.Price;
                priceBreak.Cost = model.Cost;
            }
            
            Create(priceBreak);
            UpdateValues(priceBreak, true);
            return priceBreak;
        }


        
    }

}
