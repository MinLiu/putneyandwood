﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{
    
    public class DefaultOrderNotificationService<TSettings, T, TViewModel> : ChildOrderSettingsItemService<TSettings, T, TViewModel>
        where TSettings : class, ICompanyEntity, new()
        where T : class, INotification, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Default Notification"; }
        public override string LoggingNamePlural() { return "Default Notifications"; }
        

        public DefaultOrderNotificationService(Guid CompanyID, string UserId, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, UserId, mapper, dbContext)
        {
            
        }
        

    }


}
