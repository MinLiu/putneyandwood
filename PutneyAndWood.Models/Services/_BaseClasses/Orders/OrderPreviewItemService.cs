﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrderPreviewItemService<TOrder, T, TPreviewTemplate, TAttachment, TViewModel> : ChildEntityService<TOrder, T, TViewModel>
        where TOrder : class, IEntity, ILastUpdated, ICompanyEntity, new()
        where TAttachment : class, IAttachment
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where T : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public override string LoggingName() { return "Preview"; }
        public override string LoggingNamePlural() { return "Previews"; }

        public override bool ApplyParentCompanyID { get { return false; } }
        public override string ParentName { get { return "Parent"; } }
        public override string ParentIdColumn { get { return "ParentID"; } }


        public OrderPreviewItemService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }


        public override IQueryable<T> Read(Guid? ParentID)
        {
            if (ParentID.HasValue && base.Read(ParentID).Count() <= 0)
                base.Create(new T { ParentID = ParentID.Value, Type = PreviewItemType.Template });

            var query = base.Read(ParentID);
            return query;
        }


        public override T Create(T entity)
        {
            try { entity.SortPos = Read(entity.ParentID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First() + 1; }
            catch { }
            return base.Create(entity);
        }

        /*
        public override IEnumerable<T> Create(IEnumerable<T> entities) { throw new NotImplementedException(); }
        public override IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null) { throw new NotImplementedException(); }
        public override IEnumerable<T> Destroy(IEnumerable<T> entities) { throw new NotImplementedException(); }
        */

        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            T move = null;
            T top = null;
            move = Read().Where(i => i.ID == itemID).First();
            var items = Read(null).Where(i => i.ParentID == move.ParentID).OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            top = (newPosition - 1 > 0) ? items.ElementAt(newPosition - 1) : null;
            items.Insert(newPosition, move);


            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
            }

            base.Update(items, new string[] { "SortPos" });
            UpdateLastUpated(move);
            return true;
        }

        
        public override void OnCreate(IEnumerable<T> models) { return; }
        public override void OnCreate(T model)
        {            
            try { AddLog(model, string.Format("{1} updated for {{0}}.", 0, LoggingName())); }
            catch { }
            UpdateLastUpated(model);
        }

        public override void OnUpdate(IEnumerable<T> models) { return; }
        public override void OnUpdate(T model)
        {            
            try { AddLog(model, string.Format("{1} updated for {{0}}.", 0, LoggingName())); }
            catch { }
            UpdateLastUpated(model);
        }

        public override void OnDestroy(IEnumerable<T> models) { return; }
        public override void OnDestroy(T model)
        {
            try { AddLog(model, string.Format("{1} updated for {{0}}.", 0, LoggingName())); }
            catch { }
            UpdateLastUpated(model);
        }

        private void UpdateLastUpated(T model)
        {
            var service = new GenericService<TOrder>(DbContext);
            var order = service.Find(model.ParentID);
            order.LastUpdated = DateTime.Now;
            service.Update(order, new string[] { "LastUpdated" });
        }

    }


}
