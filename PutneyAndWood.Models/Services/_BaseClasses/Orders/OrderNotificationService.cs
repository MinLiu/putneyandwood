﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrderNotificationService<TOrder, T, TViewModel> : ChildEntityService<TOrder, T, TViewModel>
        where TOrder : class, IEntity, ICompanyEntity, new()
        where T : class, INotification, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public override string LoggingName() { return "Notification Email"; }
        public override string LoggingNamePlural() { return "Notification Emails"; }

        public OrderNotificationService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }        

        //public override void OnCreate(T model) { }
        //public override void OnCreate(IEnumerable<T> models) { }
        //public override void OnUpdate(T model) { }
        //public override void OnUpdate(IEnumerable<T> models) { }
        //public override void OnDestroy(T model) { }
        //public override void OnDestroy(IEnumerable<T> models) { }

    }


}
