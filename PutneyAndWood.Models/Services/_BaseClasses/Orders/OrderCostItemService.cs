﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public class OrderCostItemService<T, TCostItem, TCostItemViewModel> : ChildEntityService<T, TCostItem, TCostItemViewModel>
        where T : class, ICompanyEntity, ICurrency, IEntity, new()
        where TCostItem : class, IOrderCostItem<TCostItem>, new()
        where TCostItemViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "ParentCostItem",  }; } }

        public virtual bool SupportsKits { get { return true; } private set {  } }
        public override string LoggingName() { return "Cost Item"; }
        public override string LoggingNamePlural() { return "Cost Items"; }

        // <summary> The Company ID value. Leave Empty to avoid parent company ID check</summary>
        public override Guid CompanyID
        {
            get { return base.CompanyID; }
            set {
                base.CompanyID = value;
                if (this.Mapper != null) {
                    var mapper = this.Mapper as OrderItemModelMapper<TCostItem, TCostItemViewModel>;
                    if(mapper.CustomFieldService != null) mapper.CustomFieldService.CompanyID = value;
                }
            }
        }

        public OrderCostItemService(Guid CompanyID, Guid? OrderID, string UserId, OrderItemModelMapper<TCostItem, TCostItemViewModel> mapper, DbContext db)
            : base(CompanyID, OrderID, UserId, mapper, db)
        {
            if (mapper != null)
                mapper.CustomFieldService = new CustomProductFieldService<EmptyViewModel>(CompanyID, null, null, db);
        }


        public override TCostItem Create(TCostItem entity)
        {
            //Get the last position
            int lastPos = 0;
            try { lastPos = Read(null).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); }
            catch { }

            entity.SortPos = lastPos + 1;
            return base.Create(entity);
        }


        public override IEnumerable<TCostItem> Create(IEnumerable<TCostItem> entities)
        {
            foreach (var entity in entities)
            {
                Create(entity);
            }

            return entities;
        }
        

        public override TCostItem Update(TCostItem entity, string[] updatedColumns = null) { return Update(entity, true, true, updatedColumns); }

        public virtual TCostItem Update(TCostItem entity, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            //base.Update(entity, updatedColumns);
            return UpdateValues(entity, UpdateParent, autoAdjust);
        }


        public override IEnumerable<TCostItem> Update(IEnumerable<TCostItem> entities, string[] updatedColumns = null) { return Update(entities, true, true, updatedColumns); }

        public virtual IEnumerable<TCostItem> Update(IEnumerable<TCostItem> entities, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                //base.Update(entity, updatedColumns);
                UpdateValues(entity, UpdateParent, autoAdjust);
            }
            return entities;
        }


        
        public override TCostItemViewModel Update(TCostItemViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, true, true, updatedColumns); }

        public override TCostItemViewModel Update(TCostItemViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModel, true, true, refreshModelFromDb, updatedColumns); }

        public virtual TCostItemViewModel Update(TCostItemViewModel viewModel, bool UpdateParent, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), UpdateParent, autoAdjust, updatedColumns);
            return MapToViewModel(model);
        }


        public override IEnumerable<TCostItemViewModel> Update(IEnumerable<TCostItemViewModel> viewModels, string[] updatedColumns = null) { return Update(viewModels, true, true, true, updatedColumns); }

        public override IEnumerable<TCostItemViewModel> Update(IEnumerable<TCostItemViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModels, true, true, refreshModelFromDb, updatedColumns); }

        public virtual IEnumerable<TCostItemViewModel> Update(IEnumerable<TCostItemViewModel> viewModels, bool UpdateParent, bool autoAdjust = true, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), UpdateParent, autoAdjust, updatedColumns);
            return MapToViewModel(models);
        }
        



        public virtual TCostItem UpdateValues(Guid ID, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            var model = Read().Where(i => i.ID == ID).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) return null;
            else return UpdateValues(model, UpdateParent, autoAdjust, updatedColumns);
        }


        public virtual TCostItem UpdateValues(TCostItem model, bool UpdateParent, bool autoAdjust = true, string[] updatedColumns = null)
        {
            if (model.IsComment) return model;

            var priceService = new PriceEntityService<TCostItem>(DbContext);
            var updated = priceService.UpdateValuesNotSave(model, autoAdjust, true);

            base.Update(updated, updatedColumns);

            if (UpdateParent)
                OnValuesUpdated(model);

            return updated;
        }

        public override TCostItem Destroy(TCostItem entity)
        {
            return Destroy(entity, true);
        }

        public virtual TCostItem Destroy(TCostItem entity, bool UpdateParent)
        {
            Deattach(entity);
            var items = Read(null).Where(i => i.ID == entity.ID || i.ParentCostItemID == entity.ID).Include(i => i.ParentCostItem).ToList();
            
            base.Destroy(items);
            if(UpdateParent) OnValuesUpdated(entity);
            return entity;
        }


        public override IEnumerable<TCostItem> Destroy(IEnumerable<TCostItem> entities)
        {
            return Destroy(entities, true);
        }

        public virtual IEnumerable<TCostItem> Destroy(IEnumerable<TCostItem> entities, bool UpdateParent)
        {
            foreach(var entity in entities)
                Destroy(entity, UpdateParent);

            return entities;
        }


        public virtual TCostItem ConvertItemToCustom(Guid id)
        {            
            var items = Read(null).Where(i => i.ID == id || i.ParentCostItemID == id).Include(i => i.ParentCostItem).ToList();
            if (items.Count <= 0) throw new Exception("Not records found");

            foreach (var item in items)
            {
                item.ParentCostItemID = null;
                //item.ProductID = null;
                //if (item.IsKit) item.IsComment = true;  //Is the Item is a kit then is head is converted to a comment.                
                item.IsKit = false;
                item.UseKitPrice = false;

                if (item.ParentCostItem != null) item.Quantity *= item.ParentCostItem.Quantity;
                //if (item.ParentCostItem != null) item.Total *= item.ParentCostItem.Quantity;

                Update(item);
                UpdateValues(item.ID, false, true);                
            }

            OnValuesUpdated(items.First());
            return items.First();
        }

        
        public TCostItem ConvertItemToKit(Guid id)
        {   
            var item = FindByID(id);
            if(item.GetType() == typeof(QuotationCostItem))
            {
                if ((item as QuotationCostItem).Quotation.UsePriceBreaks)
                    throw new Exception("Can not convert cost item to kit if quotation use prices breaks");
            }
                
            item.IsKit = true;
            Update(item, new string[] { "IsKit" });
            OnValuesUpdated(item);
            return item;
        }



        public TCostItem SetUseKitPrice(Guid id, bool useKitPrice)
        {            
            var item = FindByID(id);

            item.UseKitPrice = useKitPrice;
            Update(item, new string[] { "UseKitPrice" });
            OnValuesUpdated(item);
            return item;            
        }



        public TCostItem CopyItem(Guid id)
        {
            var copy = Read(null).Where(i => i.ID == id).Include(i => i.KitCostItems).AsNoTracking().First();
            var copyKitItems = copy.KitCostItems.OrderBy(i => i.SortPos).ToList();
            
            copy.ID = Guid.NewGuid();
            copy.KitCostItems = null;
            LogChanges = false;
            Create(copy);
            foreach (var k in copyKitItems) { k.ParentCostItemID = copy.ID; }
            Create(copyKitItems);
            LogChanges = true;

            AddLog(copy, string.Format("Cost Item '{1}' copied on {{0}}.", 0, copy.ToShortLabel()));
            OnValuesUpdated(copy);
            return copy;
        }
        
        



        public virtual TCostItem AddProduct(Guid productID, decimal qty, Guid? parentCostItemID = null, int lastPos = 0)
        {
            if (ParentID.HasValue)
                return AddProduct(productID, ParentID.Value, qty, parentCostItemID, lastPos);
            else
                return null;
        }


        public virtual TCostItem AddProduct(Guid productID, Guid orderID, decimal qty, Guid? parentCostItemID = null, int lastPos = 0)
        {
            var order = new GenericService<T>(DbContext).Find(orderID);
            if (order == null) return null;

            bool GetSupplierDiscount = (typeof(T) == (typeof(Quotation))) ? false : true;

            var productService = new ProductService<TCostItemViewModel>(order.CompanyID, null, null, DbContext);
            var product = productService.GetProduct(productID, qty, order.CurrencyID, GetSupplierDiscount);
            if (product == null) return null;


            if (!parentCostItemID.HasValue) //Only get last pos if not in kit item loop
            {
                try { lastPos = Read(orderID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); lastPos++; }
                catch { }
            }

            if (!parentCostItemID.HasValue)
            {
                //Disable Logging changes
                LogChanges = false;
            }

            var toAdd = new TCostItem
            {
                ProductCode = product.ProductCode,
                Description = product.Description,
                Category = product.Category,
                VendorCode = product.VendorCode,
                Manufacturer = product.Manufacturer,
                AccountCode = product.AccountCode,
                PurchaseAccountCode = product.PurchaseAccountCode,
                JsonCustomFieldValues = product.JsonCustomFieldValues,
                VAT = product.VAT,
                Quantity = qty,

                ImageURL = product.ImageURL,
                ParentCostItemID = (UsePriceBreaks(order)) ? null : parentCostItemID,
                IsKit = (parentCostItemID.HasValue) ? false: product.IsKit,
                UseKitPrice = (parentCostItemID.HasValue) ? false : product.UseKitPrice,
                SortPos = lastPos,
                IsComment = (UsePriceBreaks(order) && product.IsKit && !product.UseKitPrice && !parentCostItemID.HasValue) ? true : false,

                ListPrice = product.Price,
                Price = product.Price,
                ListCost = productService.GetProduct(productID, qty, order.CurrencyID, false).Cost,
                Cost = product.Cost,
            };


            var result = base.Create(toAdd);
            UpdateValues(result.ID, false, true);

            if (product.IsKit && !parentCostItemID.HasValue)
            {

                foreach (var i in product.KitItems.OrderBy(p => p.SortPos).ThenBy(p => p.Product.ProductCode).ThenBy(p => p.Product.Description).ToList())
                {
                    lastPos++;

                    if (!i.ProductID.HasValue)
                    {
                        // Do nothing
                    }
                    else if (i.IsComment)
                    {
                        var kitAdd = new TCostItem
                        {
                            //QuotationID = Guid.Parse(orderID),                            
                            Description = i.Description,
                            IsComment = true,
                            ParentCostItemID = (UsePriceBreaks(order)) ? null : toAdd.ID as Guid?,
                            SortPos = lastPos
                        };

                        base.Create(kitAdd);
                    }
                    else
                    {
                        AddProduct(i.ProductID.Value, i.Quantity, result.ID, lastPos);
                    }
                }

            }

            if (!parentCostItemID.HasValue)
            {
                //Enable Logging changes
                LogChanges = true;
                AddLog(result, string.Format("Cost Item '{1}' added to {{0}}.", 0, product.ToShortLabel()));
                OnValuesUpdated(result);
            }

            return result;
        }

        public virtual TCostItem AddProduct(Product product, Guid sectionID, decimal qty)
        {
            if (ParentID.HasValue)
                return AddProduct(product, ParentID.Value, sectionID, qty);
            else
                return null;
        }

        public virtual TCostItem AddProduct(Product product, Guid orderID, Guid sectionID, decimal qty)
        {
            ParentID = orderID;

            var lastPos = Read(orderID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).Take(1).ToArray().DefaultIfEmpty(0).FirstOrDefault();
            lastPos++;

            var toAdd = new TCostItem
            {   
                ProductCode = product.ProductCode,
                Description = product.Description,
                Category = product.Category,
                VendorCode = product.VendorCode,
                Manufacturer = product.Manufacturer,
                AccountCode = product.AccountCode,
                PurchaseAccountCode = product.PurchaseAccountCode,
                JsonCustomFieldValues = product.JsonCustomFieldValues,
                Unit = product.Unit,
                VAT = product.VAT,
                Quantity = qty,

                //ImageURL = product.ImageURL,
                ParentCostItemID = null,
                IsKit = product.IsKit,
                UseKitPrice = product.UseKitPrice,
                SortPos = lastPos,
                IsComment = false,

                ListPrice = product.Price,
                Price = product.Price,
                ListCost = product.Cost,
                Cost = product.Cost,
            };

            LogChanges = false;
            var result = Create(toAdd);            
            AddLog(result, string.Format("Cost Item '{1}' added to {{0}}.", 0, product.ToShortLabel()));
            LogChanges = true;

            OnValuesUpdated(result);

            return result;
        }


        public virtual bool UsePriceBreaks(T order) { return false; }


        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            string tempUserId = CurrentUserID;
            CurrentUserID = null;

            TCostItem move = null;
            TCostItem top = null;
            TCostItem bottom = null;

            move = Read().Include(i => i.ParentCostItem).Include(i => i.KitCostItems).Where(i => i.ID == itemID).First();

            var items = Read().Include(i => i.ParentCostItem).OrderBy(i => i.SortPos).ThenBy(i => i.ParentCostItemID != null).ToList();
            items.Remove(move);

            top = (newPosition - 1 >= 0) ? items.ElementAt(newPosition - 1) : null;
            bottom = (newPosition < items.Count) ? items.ElementAt(newPosition) : null;

            if (top == null || move.IsKit)
            { //If the top item is null or the item is a kit, set it's parent to null.
                move.ParentCostItemID = null;
            }
            else if (top != null && bottom == null) //If the top exists but bottom doesn't
            {
                if (top.IsKit) //If the top is a kit then set to parent to the top
                    move.ParentCostItemID = top.ID;
                else if (move.ParentCostItemID != null && move.ParentCostItemID != top.ParentCostItemID) //If the move and top parent item don't match (moved item to the bottom of another kit) then set parent to null
                    move.ParentCostItemID = null;
            }
            else if (top != null && bottom != null) //If the top and bottom items exist
            {
                if (top.IsKit /*&& (bottom.ParentItemID != null || bottom.IsKit)*/) //If the top is a kit and the bottom belongs to top or bottonm is a kit, bacisally empty bottom). then set the parent Id as the top
                    move.ParentCostItemID = top.ID;
                else if (top.ParentCostItemID != null && bottom.ParentCostItemID != null) //if top and bottom have parent item, (item moved between kit items) then set parent id as the same as top
                    move.ParentCostItemID = top.ParentCostItemID;
                else if (move.ParentCostItemID != null && move.ParentCostItemID != top.ParentCostItemID) //If the move and top parent item don't match (moved item to the bottom of another kit) then set parent to null
                    move.ParentCostItemID = null;
            }


            items.Insert((newPosition >= 0) ? newPosition : 0, move);


            int sortPos = 0;
            foreach (var i in items.Where(i => i.ParentCostItemID == null))
            {
                if (i.IsKit == false)
                {
                    i.SortPos = sortPos;
                    sortPos++;
                }
                else
                {
                    i.SortPos = sortPos;
                    var sortables = items.Where(u => u.ParentCostItemID == i.ID).ToList();
                    foreach (var u in sortables)
                    {
                        sortPos++;
                        u.SortPos = sortPos;
                        if(SupportsKits) Update(u, false, false, new string[] { "SortPos", "ParentCostItemID" });
                        else Update(u, false, false, new string[] { "SortPos" });
                    }
                    sortPos++;
                }

                if (SupportsKits) Update(i, false, false, new string[] { "SortPos", "ParentCostItemID" });
                else Update(i, false, false, new string[] { "SortPos" });
                
            }

            OnValuesUpdated(move);

            CurrentUserID = tempUserId;
            AddLog(move, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), move.ToShortLabel()));

            return true;
        }


        public override void OnCreate(TCostItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if (model.IsComment) AddLog(model, string.Format("{1} '{2}' created for {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' created for {{0}}.", 0, LoggingName(), model.ToShortLabel()));
        }

        public override void OnUpdate(TCostItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if (model.IsComment) AddLog(model, string.Format("{1} '{2}' update from {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), model.ToShortLabel()));
        }


        public override void OnDestroy(TCostItem model)
        {
            if (string.IsNullOrEmpty(CurrentUserID)) return;

            if (model.IsComment) AddLog(model, string.Format("{1} '{2}' permanently deleted from {{0}}.", 0, "Comment", model.ToShortLabel()));
            else AddLog(model, string.Format("{1} '{2}' permanently deleted from {{0}}.", 0, LoggingName(), model.ToShortLabel()));
        }


        public virtual void OnValuesUpdated(TCostItem model)
        {
            throw new NotImplementedException("OnValuesUpdated function has not been overwritten.");
        }

    }

}
