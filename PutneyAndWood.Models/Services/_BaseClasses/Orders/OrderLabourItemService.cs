﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public class OrderLabourItemService<T, TLabourItem, TLabourItemViewModel> : ChildEntityService<T, TLabourItem, TLabourItemViewModel>
        where T : class, ICompanyEntity, ICurrency, IEntity, new()
        where TLabourItem : class, IOrderLabourItem, new()
        where TLabourItemViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Labour Item"; }
        public override string LoggingNamePlural() { return "Labour Items"; }

        public OrderLabourItemService(Guid CompanyID, Guid? OrderID, string UserId, ModelMapper<TLabourItem, TLabourItemViewModel> mapper, DbContext db)
            : base(CompanyID, OrderID, UserId, mapper, db)
        {
            
        }


        public override TLabourItem Update(TLabourItem entity, string[] updatedColumns = null) { return Update(entity, true, updatedColumns); }

        public virtual TLabourItem Update(TLabourItem entity, bool UpdateParent, string[] updatedColumns = null)
        {
            //base.Update(entity, updatedColumns);
            return UpdateValues(entity, UpdateParent);
        }


        public override IEnumerable<TLabourItem> Update(IEnumerable<TLabourItem> entities, string[] updatedColumns = null) { return Update(entities, true, updatedColumns); }

        public virtual IEnumerable<TLabourItem> Update(IEnumerable<TLabourItem> entities, bool UpdateParent, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                //base.Update(entity, updatedColumns);
                UpdateValues(entity, UpdateParent);
            }
            return entities;
        }



        public override TLabourItemViewModel Update(TLabourItemViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, true, updatedColumns); }

        public override TLabourItemViewModel Update(TLabourItemViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModel, true, refreshModelFromDb, updatedColumns); }

        public virtual TLabourItemViewModel Update(TLabourItemViewModel viewModel, bool UpdateParent, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), UpdateParent, updatedColumns);
            return MapToViewModel(model);
        }


        public override IEnumerable<TLabourItemViewModel> Update(IEnumerable<TLabourItemViewModel> viewModels, string[] updatedColumns = null) { return Update(viewModels, true, true, updatedColumns); }

        public override IEnumerable<TLabourItemViewModel> Update(IEnumerable<TLabourItemViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModels, true, refreshModelFromDb, updatedColumns); }

        public virtual IEnumerable<TLabourItemViewModel> Update(IEnumerable<TLabourItemViewModel> viewModels, bool UpdateParent, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), UpdateParent, updatedColumns);
            return MapToViewModel(models);
        }


        public virtual TLabourItem UpdateValues(Guid ID, bool UpdateParent)
        {
            var model = Read().Where(i => i.ID == ID).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) return null;
            else return UpdateValues(model, UpdateParent);
        }

        
        public virtual TLabourItem UpdateValues(TLabourItem model, bool UpdateParent)
        {
            model.Total = decimal.Round(model.Hours * model.HourlyRate, 2);
            base.Update(model);

            if(UpdateParent)
                OnValuesUpdated(model);

            return model;
        }

       

        public override TLabourItem Destroy(TLabourItem entity)
        {
            return Destroy(entity, true);
        }

        public virtual TLabourItem Destroy(TLabourItem entity, bool UpdateParent)
        {
            base.Destroy(entity);
            if(UpdateParent) OnValuesUpdated(entity);
            return entity;
        }

        public override IEnumerable<TLabourItem> Destroy(IEnumerable<TLabourItem> entities)
        {
            return Destroy(entities, true);
        }

        public virtual IEnumerable<TLabourItem> Destroy(IEnumerable<TLabourItem> entities, bool UpdateParent)
        {
            foreach (var entity in entities)
                Destroy(entity, UpdateParent);

            return entities;
        }


        public virtual TLabourItem AddLabour(Guid labourID, decimal qty)
        {
            if (ParentID.HasValue)
                return AddLabour(labourID, ParentID.Value, qty);
            else
                return null;
        }

        public virtual TLabourItem AddLabour(Guid labourID, Guid orderID, decimal qty)
        {
            var order = new GenericService<T>(DbContext).Find(orderID);
            if (order == null) return null;

            this.ParentID = orderID;

            var labour = new GenericService<Labour>(DbContext).Read().Where(i => i.CompanyID == order.CompanyID && i.ID == labourID).DefaultIfEmpty(null).FirstOrDefault();
            if (labour == null) return null;
            
            var lastPos = Read(orderID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).Take(1).ToArray().DefaultIfEmpty(0).FirstOrDefault();
            lastPos++;
            
            var toAdd = new TLabourItem
            {
                Description = labour.Description,
                SortPos = lastPos,
                //EstimateHours = qty,
                Hours = qty,
                HourlyRate = (labour.CurrencyID == order.CurrencyID) ? labour.HourlyRate : 0m,
                Total = (labour.CurrencyID == order.CurrencyID) ? Math.Round(labour.HourlyRate * qty, 2) : 0m,
            };

            var pInfo = toAdd.GetType().GetProperty("EstimateHours");
            if (pInfo != null) pInfo.SetValue(toAdd, qty, null);

            var result = Create(toAdd);
            OnValuesUpdated(result);
            return result;
        }


        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            string tempUserId = CurrentUserID;
            CurrentUserID = null;

            TLabourItem move = null;

            move = Read().Where(i => i.ID == itemID).First();

            var items = Read().OrderBy(i => i.SortPos).ToList();
            items.Remove(move);            
            items.Insert((newPosition >= 0) ? newPosition : 0, move);


            int sortPos = 0;
            foreach (var i in items)
            {                
                i.SortPos = sortPos;
                sortPos++;

                Update(i, false, new string[] { "SortPos" });                
            }

            CurrentUserID = tempUserId;
            AddLog(move, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), move.ToShortLabel()));

            return true;
        }

        public virtual void OnValuesUpdated(TLabourItem model)
        {
            throw new NotImplementedException("OnValuesUpdated function has not been overwritten.");
        }

    }

}
