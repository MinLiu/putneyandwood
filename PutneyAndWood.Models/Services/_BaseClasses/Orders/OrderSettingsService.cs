﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Net.Mail;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SnapSuite.Internal
{
    
    public class OrderSettingsService<T, TViewModel, TDefaultAttachment, TDefaultNotification> 
        :  ModelViewService<T, TViewModel>
        where T : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TViewModel : class, new()
        where TDefaultAttachment : class, IAttachment, new()
        where TDefaultNotification : class, INotification, new()
    {
        public override string[] ReferencesToInclude { get { return new string[] { "DefaultAttachments", "DefaultNotifications" }; } }

        public OrderSettingsService(string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(UserID, mapper, db)
        {
            
        }


        public virtual T FindByID(Guid CompanyID)
        {
            var settings = Read().Where(i => i.CompanyID == CompanyID).DefaultIfEmpty(null).FirstOrDefault();

            //If settings do not existing then create a new record.
            if (settings == null)
            {
                settings = new T();
                settings.CompanyID = CompanyID;
                return Create(settings);
            }
            else
            {
                return settings;
            }
        }


        public override void OnCreate(T model) { AddLog(string.Format("{0} updated.", LoggingName()), CurrentUserID, null); }
        public override void OnCreate(IEnumerable<T> models) {  }
        public override void OnUpdate(T model) { AddLog(string.Format("{0} updated.", LoggingName()), CurrentUserID, null); }
        public override void OnUpdate(IEnumerable<T> models) { }
        public override void OnDestroy(T model) { AddLog(string.Format("{0} updated.", LoggingName()), CurrentUserID, null); }
        public override void OnDestroy(IEnumerable<T> models) { }
        
    }
    
}
