﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Reflection;
using System.IO;
using System.ComponentModel.DataAnnotations;
using Stripe;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SnapSuite.Models
{
    
    public class OrderStripePaymentService<TOrder, T, TViewModel> : ModelViewService<T, TViewModel>
        where TOrder : class, IEntity, ICurrency, ICompanyEntity, ILoggable, IOrderTotals, new()
        where T : class, IOrderStripPayment, ILoggable, new()
        where TViewModel : class, new()
    {
        
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string OrderName { get { return typeof(TOrder).Name; } }
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string OrderIdColumn { get { return string.Format("{0}ID", typeof(TOrder).Name); } }
        

        // <summary> The Company ID value.</summary>
        public virtual Guid CompanyID { get; set; }

        public override string LoggingName() { return "Stripe Payment"; }
        public override string LoggingNamePlural() { return "Stripe Payment"; }


        public OrderStripePaymentService(Guid CompanyID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext db)
            : base(UserID, mapper, db)
        {
            this.CompanyID = CompanyID;
        }
        

        public virtual T FindByID(Guid OrderID)
        {
            return Read().Where(i => i.OwnerID == OrderID).DefaultIfEmpty(null).FirstOrDefault();
        }

        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach(var model in entities)
            {
                if (model.FixedAmount) model.PercentageToPay = null;
                else model.AmountToPay = null;
            }
            return base.Create(entities);
        }

        public override T Create(T entity)
        {
            if (entity.FixedAmount) entity.PercentageToPay = null;
            else entity.AmountToPay = null;
            return base.Create(entity);
        }
        

        public override IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                if (entity.FixedAmount) entity.PercentageToPay = null;
                else entity.AmountToPay = null;

                if (entity.IsPaid && !entity.DateOfPayment.HasValue) entity.DateOfPayment = DateTime.Now;
                else if (!entity.IsPaid && entity.DateOfPayment.HasValue) entity.DateOfPayment = null;
            }
            return base.Update(entities, updatedColumns);
        }


        public override T Update(T entity, string[] updatedColumns = null)
        {
            if (entity.FixedAmount) entity.PercentageToPay = null;
            else entity.AmountToPay = null;

            if (entity.IsPaid && !entity.DateOfPayment.HasValue) entity.DateOfPayment = DateTime.Now;
            else if (!entity.IsPaid && entity.DateOfPayment.HasValue) entity.DateOfPayment = null;
            return base.Update(entity, updatedColumns);
        }

        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            foreach (var model in entities)
            {
                if (model.FixedAmount) model.PercentageToPay = null;
                else model.AmountToPay = null;
            }
            return base.Destroy(entities);
        }

        public override T Destroy(T entity)
        {
            if (entity.FixedAmount) entity.PercentageToPay = null;
            else entity.AmountToPay = null;
            return base.Destroy(entity);
        }


        public virtual T CompleteChargePayment(Guid OrderID, string token)
        {
            var model = FindByID(OrderID);
            if (model == null) throw new Exception("Unable to complete payment.  Payment not enabled or found.");
            if (model.IsPaid == true) throw new Exception("Unable to complete payment. Payment already processed.");

            var order = new GenericService<TOrder>(DbContext).Read().Where(i => i.ID == OrderID).DefaultIfEmpty(null).FirstOrDefault();
            if (order == null) throw new Exception("Unable to complete payment.  Payment not enabled or found.");

            //Becuaew we don't trust the user to input the correct in the form, we get them again.
            var description = (!string.IsNullOrWhiteSpace(model.Description)) ? string.Format("{0} {1}", model.Description, order.ToShortLabel()) : order.ToLongLabel();
            var currency = order.Currency.Code.ToLower();
            var amount = 0m;

            if (model.FixedAmount)
                amount = model.AmountToPay ?? 0;
            else
                amount = order.TotalPrice * (model.PercentageToPay ?? 0m) / 100m;
            

            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);

            // Charge the user's card:
            //A positive integer in the smallest currency unit (e.g., 100 cents to charge $1.00 or 100 to charge ¥100, a zero-decimal currency) representing how much to charge. 
            //The minimum amount is $0.50 US or equivalent in charge currency.
            var charges = new StripeChargeService();
            //var charge = charges.Create(new StripeChargeCreateOptions
            //{
            //    Amount = (int)(amount * 100m),
            //    Currency = currency,
            //    Description = Regex.Replace(description, "[^a-zA-Z\\d\\s:]", "").Truncate(20),
            //    SourceTokenOrExistingSourceId = token,
            //    ApplicationFee = (order.Company.StripeSettings.DoNotChargeFee) ? null : (int?)(amount * 100m * 0.011m),
            //}, new StripeRequestOptions { StripeConnectAccountId = order.Company.StripeSettings.UserID });

            model.IsPaid = true;
            //model.DateOfPayment = charge.Created;
            //model.StripeAmount = amount;
            //model.StripeChargeID = charge.Id;
            //model.StripeCustomerID = charge.CustomerId;
            //model.StripeDescription = charge.Description;
            //model.StripeCurrency = charge.Currency;
            //model.StripeDateCreated = charge.Created;

            return Update(model);
        }


        //public override void OnCreate(T model) { }
        //public override void OnCreate(IEnumerable<T> models) { }
        //public override void OnUpdate(T model) { }
        //public override void OnUpdate(IEnumerable<T> models) { }
        //public override void OnDestroy(T model) { }
        //public override void OnDestroy(IEnumerable<T> models) { }

    }


}
