﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Net.Mail;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace SnapSuite.Internal
{
    
    public class OrderWithCostsLabourConverterService
        <T, TSection, TLineItem, TLineItemImage, TCostItem, TLabourItem, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification,
         ogT, ogTSection, ogTLineItem, ogTLineItemImage, ogTCostItem, ogTLabourItem, ogTAttachment, ogTComment, ogTNotification, ogTPreviewTemplate, ogTPreviewItem> 
        : OrderConverterService
        <T, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification,
         ogT, ogTSection, ogTLineItem, ogTLineItemImage, ogTAttachment, ogTComment, ogTNotification, ogTPreviewTemplate, ogTPreviewItem>

        where T : class, IOrderWithCostsLabour<T, TSection, TLineItem, TLineItemImage, TCostItem, TLabourItem, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>, new()
        //where TCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        //where TViewModel : class, IEntityViewModel, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TCostItem : class, IOrderCostItem<TCostItem>, new()
        where TLabourItem : class, IOrderLabourItem, new()
        where TAttachment : class, IAttachment, IShowInPreview, new()
        where TComment : class, IComment, new()
        where TNotification : class, INotification, new()
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>        
        where TSettings : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TDefaultAttachment : class, IAttachment, IShowInPreview, new()
        where TDefaultNotification : class, INotification, new()

        where ogT : class, IOrderWithCostsLabour<ogT, ogTSection, ogTLineItem, ogTLineItemImage, ogTCostItem, ogTLabourItem, ogTAttachment, ogTComment, ogTNotification, ogTPreviewTemplate, ogTPreviewItem>, new()
        //where ogTCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        //where ogTViewModel : class, IEntityViewModel, new()
        where ogTSection : class, IOrderSection<ogTSection, ogTLineItem, ogTLineItemImage>, new()
        where ogTLineItem : class, IOrderLineItem<ogTSection, ogTLineItem, ogTLineItemImage>, new()
        where ogTLineItemImage : class, IImageItem<ogTLineItem>, new()
        where ogTCostItem : class, IOrderCostItem<ogTCostItem>, new()
        where ogTLabourItem : class, IOrderLabourItem, new()
        where ogTAttachment : class, IAttachment, IShowInPreview, new()
        where ogTComment : class, IComment, new()
        where ogTNotification : class, INotification, new()
        where ogTPreviewTemplate : class, IOrderPreviewTemplate
        where ogTPreviewItem : class, IOrderPreviewItem<ogTPreviewTemplate, ogTAttachment>
        //where ogTSettings : class, ILoggable, IOrderSettings<ogTDefaultAttachment, ogTDefaultNotification>, new()
        //where ogTDefaultAttachment : class, IAttachment, IShowInPreview, new()
        //where ogTDefaultNotification : class, INotification, new()


    {
        
        public OrderWithCostsLabourConverterService(Guid CompanyID, string UserID, DbContext db)
            : base(CompanyID, UserID, db)
        {
            
        }
        
        
        public override T Convert(Guid originalID, Guid companyID, string userID)
        {

            //Get original order
            var originalOrder = new GenericService<ogT>(DbContext).Read().Where(i => i.ID == originalID)
                                                                              .Include(i => i.Sections)
                                                                              .Include(i => i.Items)
                                                                              .Include(i => i.CostItems)
                                                                              .Include(i => i.LabourItems)
                                                                              .Include(i => i.Attachments)
                                                                              .Include(i => i.Notifications)
                                                                              //.Include(i => i.PreviewItems)
                                                                              //.AsNoTracking()
                                                                              .DefaultIfEmpty(null)
                                                                              .FirstOrDefault();

            if (originalOrder == null) return null;

            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == companyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, EmptyViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(companyID);
            if (company == null || settings == null) return null;

            var newOrder = Convert1_ReturnNew(originalOrder, user, settings);
            LogChanges = false;
            Create(newOrder);
            LogChanges = false;

            Convert1_SetupReferences(newOrder, originalOrder, company, user, settings);
            Convert2_AddItems(newOrder, originalOrder, company, user, settings);
            Convert2_AddCostItems(newOrder, originalOrder, company, user, settings);
            Convert2_AddLabour(newOrder, originalOrder, company, user, settings);
            Convert3_AddAttachments(newOrder, originalOrder, company, user, settings);
            Convert4_AddComments(newOrder, originalOrder, company, user, settings);
            Convert5_AddPreviews(newOrder, originalOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Convert6_AddLogs(newOrder, originalOrder, company, user, settings);
            Convert7_SendNotifications(newOrder, originalOrder, company, user, settings);
            Convert8_StripePayments(newOrder, originalOrder, company, user, settings);
            Convert9_Recalculate(newOrder, company, user, settings);
            return Find(newOrder.ID);
        }
        

        protected virtual void Convert2_AddCostItems(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            
            var costItemService = new GenericService<TCostItem>(DbContext);

            //Copy Order Cost Items
            var costItems = originalOrder.CostItems.Select(i => ConvertCostItem(i)).ToList();

            foreach (var i in costItems)
            {
                DbContext.Entry(i).State = EntityState.Detached;
                var propInfo = i.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(i, order.ID, null);
                //New Guid assign below
            }

            //Because items (kits) can have sub items. We have to add them with differently
            foreach (var i in costItems)
            {
                if (i.IsKit)
                {
                    //If the item is a kit then:
                    //1. Get old ID
                    //2. Add item then save to get new ID
                    //3. Get sub items, update parent ID then add them too
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();
                    costItemService.Create(i);

                    foreach (var sub in costItems.Where(s => s.ParentCostItemID == oldID))
                    {
                        sub.ParentCostItemID = i.ID;
                        sub.ID = Guid.NewGuid();
                        costItemService.Create(sub);
                    }
                }
                else if (i.ParentCostItemID != null)
                {
                    //Do nothing. Because they should added with the above code.
                }
                else
                {
                    //Normal non-kit itme are added directly.
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();
                    costItemService.Create(i);
                }
            }
        }


        protected virtual void Convert2_AddLabour(T order, ogT originalOrder, Company company, User user, TSettings settings)
        {
            var labourService = new GenericService<TLabourItem>(DbContext);
            var labours = originalOrder.LabourItems.Select(i => ConvertLabour(i)).ToList();

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var a in labours)
            {
                DbContext.Entry(a).State = EntityState.Detached;
                var propInfo = a.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(a, order.ID, null);
                a.ID = Guid.NewGuid();
            }

            labourService.Create(labours);
        }



        protected virtual TCostItem ConvertCostItem(ogTCostItem model)
        {
            var newInstance = new TCostItem { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TCostItem).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        protected virtual TLabourItem ConvertLabour(ogTLabourItem model)
        {
            var newInstance = new TLabourItem { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(TLabourItem).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;
        }

        

    }
    
}
