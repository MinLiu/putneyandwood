﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OrderAttachmentService<TOrder, T, TViewModel> : AttachmentService<TOrder, T, TViewModel>
        where TOrder : class, IEntity, ICompanyEntity, new()
        where T : class, IAttachment, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        public override string LoggingName() { return "Attachment"; }
        public override string LoggingNamePlural() { return "Attachments"; }

        public OrderAttachmentService(Guid CompanyID, Guid? ParentID, string UserID, ModelMapper<T, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, ParentID, UserID, mapper, dbContext)
        {
            
        }


        public override T Destroy(T entity)
        {
            DestroySet(Read(Guid.Empty).Where(i => i.ID == entity.ID));
            return entity;
        }

        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(Guid.Empty).Where(i => ids.Contains(i.ID)));
            return entities;
        }

        private void DestroySet(IQueryable<T> set)
        {
            set = set.Include("PreviewItems");
            base.Destroy(set.ToList());
        }

        //public override void OnCreate(T model) { }
        //public override void OnCreate(IEnumerable<T> models) { }
        //public override void OnUpdate(T model) { }
        //public override void OnUpdate(IEnumerable<T> models) { }
        //public override void OnDestroy(T model) { }
        //public override void OnDestroy(IEnumerable<T> models) { }

    }


}
