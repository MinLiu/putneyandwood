﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Net.Mail;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace SnapSuite.Internal
{
    
    public class OrderService<T, TCustomField, TViewModel, TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem, TSettings, TDefaultAttachment, TDefaultNotification> 
        :  CompanyEntityDeletableService<T, TViewModel>
        where T : class, IOrder<TSection, TLineItem, TLineItemImage, TAttachment, TComment, TNotification, TPreviewTemplate, TPreviewItem>, new()
        where TCustomField : class, IEntity, ICompanyEntity, ICustomField, new()
        where TViewModel : class, IEntityViewModel, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TAttachment : class, IAttachment, IShowInPreview, new()
        where TComment : class, IComment, new()
        where TNotification : class, INotification, new()
        where TPreviewTemplate : class, IOrderPreviewTemplate
        where TPreviewItem : class, IOrderPreviewItem<TPreviewTemplate, TAttachment>        
        where TSettings : class, ILoggable, IOrderSettings<TDefaultAttachment, TDefaultNotification>, new()
        where TDefaultAttachment : class, IAttachment, IShowInPreview, new()
        where TDefaultNotification : class, INotification, new()

    {
        public virtual string OrderIdColumn { get { return string.Format("{0}ID", typeof(T).Name); } }

        /// <summary>Model and View Model Mapper</summary>
        public virtual new  OrderModelMapper<T, TCustomField, TViewModel> Mapper { get; private set; }

        // <summary> The Company ID value. Leave Empty to avoid parent company ID check</summary>
        public override Guid CompanyID
        {
            get { return base.CompanyID; }
            set
            {
                base.CompanyID = value;
                if (this.Mapper != null)
                {
                    var mapper = this.Mapper as OrderItemModelMapper<Product, TViewModel>;
                    if (mapper != null && mapper.CustomFieldService != null) mapper.CustomFieldService.CompanyID = value;
                }
            }
        }

        public OrderService(Guid CompanyID, string UserID, OrderModelMapper<T, TCustomField, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserID, mapper, db)
        {
            CurrentUserID = UserID;
            this.Mapper = mapper;
            if (Mapper != null)
                Mapper.CustomFieldService = new CustomFieldService<TCustomField, EmptyViewModel>(CompanyID, null, null, db);
        }
        

        public override IQueryable<T> Read()
        {            
            return Read(false, false);
        }

        public virtual IQueryable<T> Read(bool deleted, bool ignoreTeams)
        {            
            if (ignoreTeams)
            {
                return base.Read(deleted);
            }                
            else
            {
                var users = new UserService(DbContext).GetCompanyUserIds(CurrentUserID);
                return base.Read(deleted).Where(o => users.Contains(o.AssignedUserID) || string.IsNullOrEmpty(o.AssignedUserID));
            } 
        }
        

        public virtual IQueryable<T> Search(DateType dateType, ICollection<int> statuses, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            if(statuses != null)
                return Search(dateType, fromDate, toDate, assignedUserId, deleted).Where(o => statuses.Contains(o.StatusID));
            else
                return Search(dateType, fromDate, toDate, assignedUserId, deleted);
        }
        

        public virtual IQueryable<T> Search(DateType dateType, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromDate, toDate);
            return Read(deleted, false).Where(o => o.Created >= fmDate && o.Created <= tDate && (o.AssignedUserID == assignedUserId || string.IsNullOrEmpty(assignedUserId))).OrderByDescending(o => o.Created);
        }

        


        public virtual T New(string userID, Guid projectID)
        {
            if (CompanyID != Guid.Empty)
                return New(CompanyID, userID, projectID);
            else
                throw new Exception("CompanyID has not been set.");
        }

        public virtual T New(Guid companyID, string userID, Guid projectID)
        {
            
            //Get Last Order number based on Settings
            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == companyID).DefaultIfEmpty(null).FirstOrDefault(); 
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, TViewModel,  TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(companyID);
            if (company == null || settings == null) return null;
            
            var newOrder = Create1_ReturnNew(company, user, settings, projectID);
            Create(newOrder);

            Create2_AddItems(newOrder, company, user, settings);
            Create3_AddPreviews(newOrder, company, user, settings);
            Create4_AddDefaultAttahments(newOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Create6_AddLogs(newOrder, company, user, settings);
            Create7_SendNotifications(newOrder, company, user, settings);
            Create8_StripePayments(newOrder, company, user, settings);

            return Find(newOrder.ID);
        }


        protected virtual T Create1_ReturnNew(Company company, User user, TSettings settings, Guid projectID)
        {
            //Get Last Order number based on Settings            
            int lastNum = 0; //Get from defaults

            var query = Read(false, company.ID).Where(x => x.ProjectID == projectID).OrderByDescending(q => q.Number).Select(i => i.Number).ToList();
            int nextNum = (query.Count > 0) ? query.First() : 0;

            if (lastNum < nextNum) lastNum = nextNum;

            return new T
            {
                CheckSum = Guid.NewGuid().ToString("N"),
                CompanyID = company.ID,
                Reference = settings.DefaultReference, //Get from settings
                Number = lastNum + 1,
                StatusID = 1,  //Draft status if always 1
                CurrencyID = company.DefaultCurrencyID,
                Created = DateTime.Now,
                LastUpdated = DateTime.Now,
                AssignedUserID = (user != null) ? user.Id : null,
                CreatedBy = (user != null) ? user.Email : null,
                ProjectID = projectID
            };
        }

        protected virtual void Create2_AddItems(T order, Company company, User user, TSettings settings)
        { 
            var sectionService = new GenericService<TSection>(DbContext);
            var newSection = new TSection { IncludeInTotal = true };
            

            var propInfo = newSection.GetType().GetProperty(OrderIdColumn);
            if (propInfo != null) propInfo.SetValue(newSection, order.ID, null);
            sectionService.Create(newSection);
        }

        protected virtual void Create3_AddPreviews(T order, Company company, User user, TSettings settings)
        {
            try
            {
                var lastCreatedID = DbContext.Set<T>().Where(i => i.CompanyID == order.CompanyID && i.ID != order.ID).OrderByDescending(q => q.Created).Select(i => i.ID).First();

                if (lastCreatedID != Guid.Empty)
                {
                    var previewItems = DbContext.Set<TPreviewItem>().Where(i => i.ParentID == lastCreatedID && i.Type != PreviewItemType.Attachment)
                                                                    .AsNoTracking().ToList();

                    foreach (var item in previewItems)
                    {
                        item.ID = Guid.NewGuid();
                        item.ParentID = order.ID;
                    }

                    DbContext.Set<TPreviewItem>().AddRange(previewItems);
                    DbContext.SaveChanges();
                }
            }
            catch { }
        }

        protected virtual void Create4_AddDefaultAttahments(T order, Company company, User user, TSettings settings)
        {
            
            //Add Default Attachments
            var defAttachs = new GenericService<TDefaultAttachment>(DbContext).Read()
                                                                                 .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(TSettings).Name), company.ID))
                                                                                 .ToList();
            var attachService = new GenericService<TAttachment>(DbContext);
            foreach (var a in defAttachs)
            {
                var attachment = new TAttachment
                {
                    FileID = a.File.ID,
                    Filename = a.Filename,
                    ShowInPreview = a.ShowInPreview,
                    //FilePath = a.FilePath,
                    //Format = a.Format
                };
                
                var propInfo = attachment.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(attachment, order.ID, null);

                attachService.Create(attachment);
            }
            
        }

        protected virtual void Create5_AddDefaultNotifications(T order, Company company, User user, TSettings settings)
        {
            
            //Add Default Notificatins
            var defNotifications = new GenericService<TDefaultNotification>(DbContext).Read()
                                                                                 .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(TSettings).Name), company.ID))
                                                                                 .Where(i => i.Email != (!string.IsNullOrEmpty(user.Email) ? user.Email : ""))
                                                                                 .ToList();
            var notificationService = new GenericService<TNotification>(DbContext);
            foreach (var a in defNotifications)
            {
                var noti = new TNotification
                {
                    Email = a.Email
                };

                var propInfo = noti.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(noti, order.ID, null);

                notificationService.Create(noti);
            }


            //Add the users email address. 
            if (!string.IsNullOrEmpty(user.Email))
            {
                var noti = new TNotification { Email = user.Email };

                var pInfo = noti.GetType().GetProperty(OrderIdColumn);
                if (pInfo != null) pInfo.SetValue(noti, order.ID, null);

                notificationService.Create(noti);
            }
        }

        protected virtual void Create6_AddLogs(T order, Company company, User user, TSettings settings)
        {
            //Do nothing, service will automatcially log change
        }


        protected virtual void Create7_SendNotifications(T order, Company company, User user, TSettings settings)
        {            
            //Log Activity
            SendNotifications(order.ID, string.Format("New {0} Created; {1}", LoggingName(), order.ToShortLabel()), string.Format("{0} {1} created by {2}", LoggingName(), 
                                                                                                                                                            order.ToShortLabel(), 
                                                                                                                                                            (user != null) ? user.ToLongLabel() : "")
            );
        }

        protected virtual void Create8_StripePayments(T order, Company company, User user, TSettings settings)
        {
            
        }

        public virtual T Copy(Guid originalID, string userID)
        {
            if (CompanyID != Guid.Empty)
                return Copy(originalID, CompanyID, userID);
            else
                return null;
        }

        public virtual T Copy(Guid originalID, Guid companyID, string userID)
        {

            //Get original order
            var originalOrder = Read().Where(i => i.ID == originalID)
                                      .Include(i => i.Sections)
                                      .Include(i => i.Items)
                                      .Include(i => i.Attachments)
                                      .Include(i => i.Notifications)
                                      .Include(i => i.PreviewItems)
                                      //.AsNoTracking()
                                      .DefaultIfEmpty(null)
                                      .FirstOrDefault();

            if (originalOrder == null) return null;

            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == companyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<TSettings, TViewModel, TDefaultAttachment, TDefaultNotification>(null, null, DbContext).FindByID(companyID);
            if (company == null || settings == null) return null;

            var newOrder = Copy1_ReturnNew(originalOrder, user, settings);
            LogChanges = false;
            Create(newOrder);
            LogChanges = false;

            Copy2_AddItems(newOrder, originalOrder, company, user, settings);
            Copy3_AddAttachments(newOrder, originalOrder, company, user, settings);
            Copy4_AddComments(newOrder, originalOrder, company, user, settings);
            Copy5_AddPreviews(newOrder, originalOrder, company, user, settings);
            Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Copy6_AddLogs(newOrder, originalOrder, company, user, settings);
            Copy7_SendNotifications(newOrder, originalOrder, company, user, settings);
            Copy8_StripePayments(newOrder, originalOrder, company, user, settings);

            return Find(newOrder.ID);
        }


        protected virtual T Copy1_ReturnNew(T originalOrder, User user, TSettings settings)
        {
            //Copy original
            //Can not query database again or else overwrites origanl values in memory. Strange.
            //var newOrder = Mapper.MapToModel(Mapper.MapToViewModel(originalOrder));
            var newOrder = MapNewCopy(originalOrder);

            //Get Last Order number based on Settings
            var query = Read(false, originalOrder.CompanyID).OrderByDescending(q => q.Number).Select(i => i.Number).ToList();
            int lastNum = (query.Count > 0) ? query.First() : 0;

            newOrder.ID = Guid.NewGuid();
            newOrder.CheckSum = Guid.NewGuid().ToString("N");
            newOrder.Reference = settings.DefaultReference; //Get from defaults
            newOrder.Number = lastNum + 1;
            newOrder.StatusID = 1;
            newOrder.Created = DateTime.Now;
            newOrder.LastUpdated = DateTime.Now;
            newOrder.AssignedUserID = (user != null) ? user.Id : originalOrder.AssignedUserID; //Assign to order the use
            newOrder.CreatedBy = (user != null) ? user.Email : originalOrder.CreatedBy;
            newOrder.IsApproved = false;
            newOrder.EmailOpened = null;
            newOrder.Deleted = false;
            newOrder.ProjectID = originalOrder.ProjectID;

            return newOrder;
        }

        protected virtual T MapNewCopy(T model)
        {
            var newInstance = new T { };

            //Only get the properties that are non Quikflw classes. The prevents the copying of EF collections and duplicating of entities.
            var fields = typeof(T).GetProperties().Where(i => !i.PropertyType.FullName.Contains("SnapSuite")).ToArray();

            foreach (var field in fields)
            {
                var newInfo = newInstance.GetType().GetProperty(field.Name);
                var orgInfo = model.GetType().GetProperty(field.Name);

                if (newInfo != null && orgInfo != null) newInfo.SetValue(newInstance, orgInfo.GetValue(model), null);
            }

            return newInstance;

        }

        protected virtual void Copy2_AddItems(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var sectionService = new GenericService<TSection>(DbContext);
            var itemService = new GenericService<TLineItem>(DbContext);
            var imageService = new GenericService<TLineItemImage>(DbContext);
            var sections = originalOrder.Sections.ToList();
            var sectionIds = sections.Select(i => i.ID).ToList();
            var items = originalOrder.Items.ToList();
            var itemIds = items.Select(i => i.ID).ToArray();

            var images = imageService.Read().Where(i => itemIds.Contains(i.OwnerID))
                                            .Include(i => i.File)
                                            .ToList()
                                            .Select(i => new TLineItemImage { OwnerID = i.OwnerID, SortPos = i.SortPos, FileID = i.FileID }).ToList();

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var s in sections)
            {
                DbContext.Entry(s).State = EntityState.Detached;
                var propInfo = s.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(s, order.ID, null);
                s.ID = Guid.NewGuid();
            }

            sectionService.Create(sections);

            foreach (var i in items)
            {
                DbContext.Entry(i).State = EntityState.Detached;
                var propInfo = i.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(i, order.ID, null);
                i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                
                //New Guid assign below
            }

            //Because items (kits) can have sub items. We have to add them with differently
            foreach (var i in items)
            {
                if (i.IsKit)
                {
                    //If the item is a kit then:
                    //1. Get old ID
                    //2. Add item then save to get new ID
                    //3. Get sub items, update parent ID then add them too
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();                    
                    itemService.Create(i);
                    foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }

                    foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                    {
                        Guid oldSubID = sub.ID;
                        sub.ParentItemID = i.ID;
                        sub.ID = Guid.NewGuid();
                        itemService.Create(sub);

                        foreach (var img in images.Where(p => p.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }
                    }
                }
                else if (i.ParentItemID != null)
                {
                    //Do nothing. Because they should added with the above code.
                }
                else
                {
                    //Normal non-kit itme are added directly.
                    Guid oldID = i.ID;
                    i.ID = Guid.NewGuid();                    
                    itemService.Create(i);

                    foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                }                
            }
            
            imageService.Create(images);

        }

        protected virtual void Copy3_AddAttachments(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var attachmentService = new GenericService<TAttachment>(DbContext);
            var attachments = originalOrder.Attachments.ToList();

            //When copying first detached. This insure copies are made instead of updating existing records
            foreach (var a in attachments)
            {
                DbContext.Entry(a).State = EntityState.Detached;
                var propInfo = a.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(a, order.ID, null);
                a.ID = Guid.NewGuid();
            }

            attachmentService.Create(attachments);            
        }

        protected virtual void Copy4_AddComments(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var commentsService = new GenericService<TComment>(DbContext);

            var comment = new TComment
            {
                Timestamp = DateTime.Now,
                Email = (user != null) ? user.Email : "", 
                Message = string.Format("Copied from {0}: {1}", LoggingName(), originalOrder.ToShortLabel())
            };

            var propInfo = comment.GetType().GetProperty(OrderIdColumn);
            if (propInfo != null) propInfo.SetValue(comment, order.ID, null);

            commentsService.Create(comment);
        }


        protected virtual void Copy5_AddPreviews(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            var previewItems = DbContext.Set<TPreviewItem>().Where(i => i.ParentID == originalOrder.ID)
                                                            .Include(i => i.Attachment)
                                                            .AsNoTracking().ToList();

            //Get list of attachment in new order
            var attachments = DbContext.Set<T>().Where(i => i.ID == order.ID).AsNoTracking().First().Attachments;

            foreach (var item in previewItems)
            {
                item.ID = Guid.NewGuid();
                item.ParentID = order.ID;
                item.AttachmentID = (item.Type == PreviewItemType.Attachment) ? attachments.Where(i => i.FileID == item.Attachment.FileID).Select(i => i.ID as Guid?).DefaultIfEmpty(null).FirstOrDefault() : null;
                item.Attachment = null;
            }

            DbContext.Set<TPreviewItem>().AddRange(previewItems);
            DbContext.SaveChanges();
        }

        

        protected virtual void Copy6_AddLogs(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            if (user == null) return;

            var text = string.Format("{1} {2} copied. {0} created.", order.ToShortLabel(), 
                                                                     LoggingName(), 
                                                                     originalOrder.ToLongLabel());
            AddLog(text, user.Id, order.ID.ToString());
        }


        protected virtual void Copy7_SendNotifications(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            if (user == null) return;

            //Log Activity
            var text = string.Format("{1} {2} copied. {0} created by {3}", order.ToShortLabel(), 
                                                                           LoggingName(), 
                                                                           originalOrder.ToLongLabel(), 
                                                                           order.ToShortLabel(), (user != null) ? user.ToLongLabel() : "");
            SendNotifications(order.ID, string.Format("New {0} Created; {1}", LoggingName(), order.ToShortLabel()), text);
        }


        protected virtual void Copy8_StripePayments(T order, T originalOrder, Company company, User user, TSettings settings)
        {
            
        }
        

        public override T Update(T entity, string[] updatedColumns = null)
        {
            if (updatedColumns != null && updatedColumns.Contains("LastUpdated") == false) updatedColumns.Concat(new string[] { "LastUpdated" });
            entity.LastUpdated = DateTime.Now;
            return base.Update(entity, updatedColumns);
        }

        public override IEnumerable<T> Update(IEnumerable<T> entities, string[] updatedColumns = null)
        {
            if (updatedColumns != null && updatedColumns.Contains("LastUpdated") == false) updatedColumns.Concat(new string[] { "LastUpdated" });
            foreach (var e in entities) { e.LastUpdated = DateTime.Now; }
            return base.Update(entities, updatedColumns);
        }

        public T UpdateValues(Guid ID)
        {
            var model = FindByID(ID);
            return UpdateValues(model);
        }

        public virtual T UpdateValues(T model)
        {           
            if (model == null) return null;

            var sectionService = new GenericService<TSection>(DbContext);

            var sections = sectionService.Read().Include(s => s.Items)
                                            .Where(string.Format("{0}.Equals(Guid(\"{1}\"))", string.Format("{0}ID", typeof(T).Name), model.ID))
                                            .ToList();
            
            //Reset model total values
            model.TotalDiscount = 0;
            model.Discount = 0;
            model.TotalVat = 0;
            model.TotalMargin = 0;
            model.Margin = 0;
            model.TotalCost = 0;
            model.TotalNet = 0;
            model.TotalPrice = 0;
            model.TotalOtherCosts = CalculateTotalOtherCosts(model.ID);
            model.TotalLabourCosts = CalculateTotalLabourCosts(model.ID);
            
            
            foreach (var section in sections)
            {
                if (!section.IncludeInTotal) continue;

                model.TotalDiscount += section.TotalDiscount;
                model.Discount += section.Discount;

                model.TotalVat += section.TotalVat;
                model.TotalMargin += section.TotalMargin;
                model.Margin += section.Margin;
                model.TotalCost += section.TotalCost;
                model.TotalNet += section.TotalNet;
                model.TotalPrice += section.TotalPrice;
            }

            if (sections.Where(s => s.IncludeInTotal == true).Count() > 0)
            {
                model.Discount /= sections.Where(s => s.IncludeInTotal == true).Count();
                model.Margin /= sections.Where(s => s.IncludeInTotal == true).Count();
            }
            else
            {
                model.Discount = 0;
                model.Margin = 0;
            }


            model.Discount = decimal.Round(model.Discount, 2);
            model.Margin = decimal.Round(model.Margin, 2);
            model.TotalProfit = model.TotalNet - model.TotalCost - model.TotalOtherCosts - model.TotalLabourCosts;
            
            Update(model);

            return model;
        }


        public virtual decimal CalculateTotalOtherCosts(Guid ID)
        {
            return 0;
        }

        public virtual decimal CalculateTotalLabourCosts(Guid ID)
        {
            return 0;
        }

        public virtual decimal CalculatePOCosts(Guid ID)
        {
            return 0;
        }


        public virtual T ChangeStatus(Guid ID, int newStatusID)
        {
            var model = FindByID(ID);
            return ChangeStatus(model, newStatusID);
        }

        public virtual T ChangeStatus(T model, int newStatusID)
        {
            model.StatusID = newStatusID;
            model.LastUpdated = DateTime.Now;
            return Update(model, new string[] { "StatusID", "LastUpdated" } );
        }


        public virtual TViewModel UpdateHeader(TViewModel viewModel)
        {
            var model = FindByID(Guid.Parse(viewModel.ID));
            
            //Refresh the whole page if the curreny changes.
            Mapper.MapToModelHeader(viewModel, model);
            model.LastUpdated = DateTime.Now;
            Update(model);

            return Mapper.MapToViewModel(model);
        }


        public virtual T SetOpenEmailDateTime(Guid ID, DateTime? dateTime, string email)
        {
            var model = new GenericService<T>(DbContext).Read().Where(i => i.ID == ID).First();
            return SetOpenEmailDateTime(model, dateTime, email);
        }


        public virtual T SetOpenEmailDateTime(T model, DateTime? dateTime, string email)
        {
            model.EmailOpened = dateTime;
            Update(model, new string[] { "EmailOpened", "LastUpdated" });

            if (!dateTime.HasValue)
            {
                AddLog(string.Format("{0} email opened date reset.", model.ToShortLabel()), CurrentUserID, model.ID.ToString());
            }
            else
            {
                var commentsService = new GenericService<TComment>(DbContext);
                var comment = new TComment { Timestamp = DateTime.Now, Email = email, Message = string.Format("Email opened by {0}", email) };
                var propInfo = comment.GetType().GetProperty(OrderIdColumn);
                if (propInfo != null) propInfo.SetValue(comment, model.ID, null);
                commentsService.Create(comment);
            }

            return model;
        }


        
        public virtual void ApplyDiscountMarginMarkupVat(Guid ID, decimal? discount, decimal? margin, decimal? markup, decimal? vat)
        {
            var itemService = new OrderLineItemService<T, TSection, TLineItem, TLineItemImage, TAttachment, TViewModel>(Guid.Empty, ID, null, null, DbContext);           
            var items = itemService.Read(ID).ToList();
            foreach (var i in items)
            {
                if (discount != null) i.Discount = discount.Value;
                if (margin != null) i.Margin = margin.Value;
                if (markup != null) i.Markup = markup.Value;
                if (vat != null) i.VAT = vat.Value;
                itemService.UpdateValues(i, false, true);
            }

            RecalculateAllTotalsMinusItems(ID);
            //UpdateValues(ID);

            //Log Activity
            string logText = "";
            if (discount != null) logText = string.Format("All items' discounts set to {1} on {{{0}}}", 0, discount.Value);
            if (margin != null) logText = string.Format("All items' margins set to {1} on {{{0}}}", 0, margin.Value);
            if (markup != null) logText = string.Format("All items' markups set to {1} on {{{0}}}", 0, markup.Value);
            if (vat != null) logText = string.Format("All items' VATs set to {1} on {{{0}}}", 0, vat.Value);

            AddLog(logText, CurrentUserID, ID.ToString());
        }


        public virtual void ApplyAccountCode(Guid ID, string accountCode)
        {
            var itemService = new OrderLineItemService<T, TSection, TLineItem, TLineItemImage, TAttachment, TViewModel>(Guid.Empty, ID, null, null, DbContext);
            var items = itemService.Read(ID).ToList();
            foreach (var i in items)
            {
                i.AccountCode = accountCode;
                itemService.UpdateValues(i, false, false);
            }
            
            //Log Activity
            string logText = string.Format("All items' Account Codes set to {1} on {{{0}}}", 0, accountCode);

            AddLog(logText, CurrentUserID, ID.ToString());
        }

        public virtual void ApplyPurchaseAccountCode(Guid ID, string accountCode)
        {
            var itemService = new OrderLineItemService<T, TSection, TLineItem, TLineItemImage, TAttachment, TViewModel>(Guid.Empty, ID, null, null, DbContext);
            var items = itemService.Read(ID).ToList();
            foreach (var i in items)
            {
                i.PurchaseAccountCode = accountCode;
                itemService.UpdateValues(i, false, false);
            }

            //Log Activity
            string logText = string.Format("All items' Purchase Account Codes set to {1} on {{{0}}}", 0, accountCode);

            AddLog(logText, CurrentUserID, ID.ToString());
        }


        public virtual T RecalculateAllTotals(Guid ID)
        {
            throw new NotImplementedException();
        }


        public virtual T RecalculateAllTotalsMinusItems(Guid ID)
        {
            throw new NotImplementedException();
        }



        public virtual OrderEmailViewModel GetEmailTitleMessage(Guid ID)
        {
            throw new NotImplementedException();
        }



        public virtual bool EmailPDF(Guid ID, string title, string message, string fromEmail, ICollection<string> toEmails, string replyToEmail, bool includePdf, string sendCopyToUserId, ICollection<Guid> attachmentIDs, ref List<string> succussEmails, ref List<string> errorEmails)
        {
            var order = FindByID(ID);

            var mailMessage = EmailMessageService.CreateMailMessage("", fromEmail, title, "", null);

            try { if(!string.IsNullOrEmpty(replyToEmail)) mailMessage.ReplyToList.Add(replyToEmail); }
            catch { }

            //MailMessage mailMessage = new MailMessage();
            //mailMessage.Subject = model.Title;
            //mailMessage.IsBodyHtml = true;

            string body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\">"
                                            + "{0}"
                                            + "EMAIL_CHECK_EMAIL"
                                            + " </div></div>",
                                            System.Web.HttpUtility.HtmlDecode(message.Replace(string.Format("\n"), "<br />"))
                                            );



            //Generate PDF and attach it the mail message if ticked
            if (includePdf)
            {
                AttachPDF(mailMessage, order);
            }

            //Save copy of sent version (only if the version doesn't exist.)
            var attachmentService = new OrderAttachmentService<T, TAttachment, EmptyViewModel>(CompanyID, order.ID, null, null, DbContext);
            var fileService = new FileService(DbContext);
            var filename = string.Format("{0}{1}", order.ID, order.LastUpdated.ToString("ssmmddMMyy"));
            if(attachmentService.Read(order.ID).Where(i => i.File.FilePath.Contains(filename + ".pdf")).Count() <= 0)
            {
                var filepath = FileService.GetUploadPhysicalFilePath(order.CompanyID, "/_Temp/", string.Format("{0}.pdf", order.ID));
                var fileEntry = fileService.SaveFile(order.CompanyID, fileService.DeepCopyFile(order.CompanyID, filepath, "/Attachments/", filename), (int)new System.IO.FileInfo(filepath).Length); 
                attachmentService.Create(new TAttachment { Filename = string.Format("{0} sent on {1}", order.ToShortLabel(),  DateTime.Now.ToString("dd-MM-yy")), FileID = fileEntry.ID });
            }


            if (attachmentIDs != null)
            {
                //Include attachments 
                var attachments = new GenericService<TAttachment>(DbContext).Read().Where(a => attachmentIDs.Contains(a.ID) && a.File.FileFormat != FileFormat.Link).ToList();
                foreach (var attach in attachments)
                {
                    try
                    {
                        var file = new Attachment(FileService.ToPhysicalPath(attach.File.FilePath));
                        file.ContentId = attach.Filename;
                        file.NameEncoding = System.Text.Encoding.Unicode;
                        file.Name = attach.Filename;
                        file.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
                        mailMessage.Attachments.Add(file);
                    }
                    catch { }
                }
            }
            
            bool sent = false;            

            foreach (var address in toEmails)
            {
                try
                {
                    mailMessage.To.Clear();
                    mailMessage.To.Add(address);
                    mailMessage.Body = body.Replace("EMAIL_CHECK_EMAIL", string.Format("<img src=\"{0}/EmailCheck/{1}?id={2}&email={3}\" />", FileService.WebURL, typeof(T).Name, ID.ToString(), address));

                    //if (EmailMessageService.SendEmailWithSendGrid(mailMessage)) { succussEmails.Add(address); sent = true; }
                    if (new EmailMessageService(CompanyID, DbContext).SendEmail(order.CompanyID, mailMessage, true)) { succussEmails.Add(address); sent = true; }
                    else { errorEmails.Add(address); }

                }
                catch { errorEmails.Add(address); }
            }


            var user = new UserService(DbContext).FindById(sendCopyToUserId);
            if (user != null && sent)
            {
                try
                {
                    mailMessage.To.Clear();
                    mailMessage.To.Add(user.Email);
                    mailMessage.Body = body.Replace("EMAIL_CHECK_EMAIL", "");
                    new EmailMessageService(CompanyID, DbContext).SendEmail(order.CompanyID, mailMessage, true);
                }
                catch { }
            }

            if (sent)
            {
                try { OnEmailSent(order, succussEmails, errorEmails); } catch { }                    
            }
            
            return true;
        }

        protected virtual void AttachPDF(MailMessage mailMessage, T model)
        {
            throw new NotImplementedException();
            //string fileName = Miscellaneous.Truncate(model.Title, 40) + ".pdf";
            //string empty = "";
            //Attachment pdf = new Attachment(new MemoryStream(GeneratePDF(Server, model.QuotationID, model.CheckSum, ref empty)), fileName);
            //mailMessage.Attachments.Add(pdf);
        }

        protected virtual void OnEmailSent(T model, List<string> successEmails, List<string> errorEmails)
        {
            throw new NotImplementedException();
            /*
            using (var context = new SnapDbContext())
            {
                var quote = context.Quotations.Where(q => q.ID.ToString() == model.QuotationID).First();

                if (quote.StatusID < QuotationStatusValues.SENT)
                {
                    quote.StatusID = QuotationStatusValues.SENT;
                    context.Quotations.Attach(quote);
                    context.Entry(quote).Property(q => q.StatusID).IsModified = true;
                    context.SaveChanges();

                    //Send notifications here
                    QuotationsController.SendNotifications(quote.ID, "Status Changed to Sent", "The status of the quotation has been changed to Sent.", context, CurrentUser, this.Server, this.Request);
                }

                context.QuotationComments.Add(new QuotationComment { QuotationID = quote.ID, Email = CurrentUser.Email, Message = string.Format("Sent to {0}", sentEmails), Timestamp = DateTime.Now });
                context.SaveChanges();

                //Log Activity
                QuotationsController.AddLog(quote.ID.ToString(), string.Format("{{{0}}} sent to {1}.", 0, sentEmails), CurrentUser);
            }
            */
        }


        public virtual T AcceptOrder(Guid ID, int newStatus, string acceptBy, string notes, string PoNumber)
        {
            var repo = new QuotationRepository();
            var order = FindByID(ID);
                        

            //Update order based on the option selected by user.           
            order.StatusID = newStatus;
            if (!string.IsNullOrEmpty(PoNumber)) order.PoNumber = PoNumber;
                       
            Update(order, new string[] { "StatusID", "PoNumber" });


            //Add internal comment
            new  OrderCommentService<T, TComment, EmptyViewModel>(CompanyID, order.ID, CurrentUserID, null, DbContext).Create(new TComment
            {                
                Timestamp = DateTime.Now,
                Email = acceptBy,
                Message = string.Format("Accept by {0}; Note: {1}", acceptBy, notes)
            });

            var title = string.Format("{{0}} Accepted by {1}", 0, acceptBy);
            var message = string.Format("{{0}} was accepted by {1}. \nNote: {2}", 0, acceptBy, notes);
            SendNotifications(order, title, message);

            return order;
        }



        public virtual bool SendNotifications(Guid ID, string title, string message)
        {
            var model = Read().Where(i => i.ID == ID).DefaultIfEmpty(null).FirstOrDefault();
            return SendNotifications(model, title, message);
        }

        public virtual bool SendNotifications(T model, string title, string message)
        {

#if !DEBUG
            try
            {
                
                var user = new UserService(DbContext).Find(CurrentUserID);
                if (user == null) user = new User { FirstName = "Quikflw", LastName = "", Email = "noreply@quikflw.com" };
                var company = new GenericService<Company>(DbContext).Find(model.CompanyID);
                
                MailMessage mailMessage = new MailMessage();
                
                foreach (var address in model.Notifications.Where(u => u.Email != user.Email))
                {
                    mailMessage.Bcc.Add(address.Email);
                }
                //mailMessage.From = new MailAddress(RadComboBoxFromEmail.Text, RadComboBoxFromEmail.Text);
                mailMessage.Subject = string.Format("{0}; {1}: {2}", title, LoggingName(), model.ToLongLabel()).Truncate(60);
                mailMessage.IsBodyHtml = true;

                mailMessage.Body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\">"
                                                + "<br /> <img src=\"cid:company_logo.png\" height=\"30\" style=\"height: 30px; max-height:30px;\"/> <hr />"
                                                + "Name: {0}<br />"
                                                + "Email: {1}<br />"
                                                + "Message:-<br /><br />"
                                                + "{2}"
                                                + "<br /><br /><br />"
                                                + "<span style=\"font-weight:bold;\">Click Here to Edit {3}.</span><br /> "
                                                + "<a href=\"{4}\" style=\"font-size:small;\">{4}</a>"
                                                + "<br /><br /><br />"
                                                + " </div></div>",

                                                string.Format("{0} {1}", user.FirstName, user.LastName),
                                                user.Email,
                                                message.Replace(string.Format("\n"), "<br />"),
                                                LoggingName(),
                                                string.Format("{0}/{1}s/Edit/{2}", "https://customer.quikflw.com", typeof(T).Name, model.ID.ToString()));

                Attachment img2;
                
                try {
                    img2 = new Attachment(FileService.ToPhysicalPath(company.LogoImageURL)); img2.ContentId = "company_logo.png"; mailMessage.Attachments.Add(img2);
                }
                catch {
                    try
                    {
                        Assembly assembly = Assembly.GetAssembly(typeof(EmailMessageService));
                        Stream imageStream = assembly.GetManifestResourceStream("SnapSuite.Models.Resources.email_logo.png");
                        Attachment img1 = new Attachment(imageStream, "image");
                        img1.ContentId = "company_logo.png";
                        mailMessage.Attachments.Add(img1);
                    }
                    catch { }
                }
                
                return new EmailMessageService(model.CompanyID, DbContext).SendEmail(mailMessage, false);
                
            }
            catch
            {
                return false;
            }
#else
            return true;
#endif
        }
        
    }
    
}
