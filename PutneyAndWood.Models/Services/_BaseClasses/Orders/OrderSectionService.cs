﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;

namespace SnapSuite.Internal
{

    public class OrderSectionService<T, TSection, TLineItem, TLineItemImage,  TSectionViewModel> : ChildEntityService<T, TSection, TSectionViewModel>
        where T : class, ICompanyEntity, ICurrency, IEntity, new()
        where TSection : class, IOrderSection<TSection, TLineItem, TLineItemImage>, new()
        where TLineItem : class, IOrderLineItem<TSection, TLineItem, TLineItemImage>, new()
        where TLineItemImage : class, IImageItem<TLineItem>, new()
        where TSectionViewModel : class, IEntityViewModel, new()
    {
        public override string LoggingName() { return "Section"; }
        public override string LoggingNamePlural() { return "Sections"; }

        public OrderSectionService(Guid CompanyID, Guid? OrderID, string UserId, ModelMapper<TSection, TSectionViewModel> mapper, DbContext db)
            : base(CompanyID, OrderID, UserId, mapper, db)
        {
            
        }


        public override TSection Create(TSection entity)
        {
            //Get the last position
            int lastPos = 0;
            try { lastPos = Read().OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); }
            catch { }

            entity.SortPos = lastPos + 1;
            return base.Create(entity);
        }


        public override IEnumerable<TSection> Create(IEnumerable<TSection> entities)
        {
            foreach (var entity in entities)
            {
                Create(entity);
            }

            return entities;
        }


        public override TSection Update(TSection entity, string[] updatedColumns = null) { return Update(entity, true, updatedColumns); }

        public virtual TSection Update(TSection entity, bool UpdateParent, string[] updatedColumns = null)
        {
            //base.Update(entity, updatedColumns);
            return UpdateValues(entity.ID, UpdateParent);
        }


        public override IEnumerable<TSection> Update(IEnumerable<TSection> entities, string[] updatedColumns = null) { return Update(entities, true, updatedColumns); }

        public virtual IEnumerable<TSection> Update(IEnumerable<TSection> entities, bool UpdateParent, string[] updatedColumns = null)
        {
            foreach (var entity in entities)
            {
                //base.Update(entity, updatedColumns);
                UpdateValues(entity.ID, UpdateParent);
            }
            return entities;
        }



        public override TSectionViewModel Update(TSectionViewModel viewModel, string[] updatedColumns = null) { return Update(viewModel, true, true, updatedColumns); }

        public override TSectionViewModel Update(TSectionViewModel viewModel, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModel, true, refreshModelFromDb, updatedColumns); }

        public virtual TSectionViewModel Update(TSectionViewModel viewModel, bool UpdateParent, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var model = Update(MapToModel(viewModel, refreshModelFromDb), UpdateParent, updatedColumns);
            return MapToViewModel(model);
        }


        public override IEnumerable<TSectionViewModel> Update(IEnumerable<TSectionViewModel> viewModels, string[] updatedColumns = null) { return Update(viewModels, true, true, updatedColumns); }

        public override IEnumerable<TSectionViewModel> Update(IEnumerable<TSectionViewModel> viewModels, bool refreshModelFromDb = true, string[] updatedColumns = null) { return Update(viewModels, true, refreshModelFromDb, updatedColumns); }

        public virtual IEnumerable<TSectionViewModel> Update(IEnumerable<TSectionViewModel> viewModels, bool UpdateParent, bool refreshModelFromDb = true, string[] updatedColumns = null)
        {
            var models = Update(MapToModel(viewModels, refreshModelFromDb), UpdateParent, updatedColumns);
            return MapToViewModel(models);
        }

        public virtual TSection UpdateValues(Guid ID, bool UpdateParent)
        {
            var model = FindByID(ID);
            if (model == null) return null;
            else return UpdateValues(model, UpdateParent);
        }


        public virtual TSection UpdateValues(TSection model, bool UpdateParent)
        {
            decimal total_vat = 0.0m;
            decimal total_margin = 0.0m;
            decimal total_cost = 0.0m;
            decimal total_net = 0.0m;
            decimal total_list = 0.0m;
            decimal total_net_discount = 0.0m;
            
            foreach (var item in model.Items)
            {
                if ((item.IsKit && !item.UseKitPrice) || item.IsComment)
                {
                    //Do nothing
                }
                else if (item.ParentItem != null)
                {
                    if (!item.ParentItem.UseKitPrice)
                    {
                        var net = decimal.Round(item.Price * item.Quantity * item.ParentItem.Quantity + item.SetupCost, 2);
                        total_net += net;
                        total_net_discount += (item.ListPrice > 0m) ? net : 0m;
                        total_vat += decimal.Round((net * (item.VAT / 100.0m)), 2);
                        //total_discount += (item.ListPrice - item.Price) * item.Quantity * item.ParentItem.Quantity, 2);
                        total_margin += decimal.Round(item.MarginPrice * item.ParentItem.Quantity, 2);
                        total_cost += decimal.Round(item.Cost * item.Quantity * item.ParentItem.Quantity, 2);
                        total_list += decimal.Round(item.ListPrice * item.Quantity * item.ParentItem.Quantity + item.SetupCost, 2);

                    }
                }
                else
                {
                    var net = decimal.Round(item.Price * item.Quantity + item.SetupCost, 2);
                    total_net += net;
                    total_net_discount += (item.ListPrice > 0m) ? net : 0m;
                    total_vat += decimal.Round((net * (item.VAT / 100m)), 2);
                    //total_discount += decimal.Round((item.ListPrice - item.Price) * item.Quantity, 2);
                    total_margin += decimal.Round(item.MarginPrice, 2);
                    total_cost += decimal.Round(item.Cost * item.Quantity, 2);
                    total_list += decimal.Round(item.ListPrice * item.Quantity + item.SetupCost, 2);
                }
            }

            //One Offs
            model.TotalCost = decimal.Round(total_cost, 2);
            model.TotalDiscount = decimal.Round(total_list - total_net_discount, 2);
            model.Discount = decimal.Round((total_list == 0.0m) ? 0.0m : model.TotalDiscount / total_list * 100.0m, 2);
            //if (order.Discount < 0.0m) order.Discount = 0.0m;

            model.TotalVat = decimal.Round(total_vat, 2);
            model.TotalMargin = decimal.Round(total_margin, 2);
            model.Margin = decimal.Round((total_net == 0.0m) ? 0.0m : total_margin / total_net * 100.0m, 2);
            model.TotalNet = decimal.Round(total_net, 2);
            model.TotalPrice = decimal.Round(total_net + total_vat, 2);

            base.Update(model);
            if(UpdateParent) OnValuesUpdated(model);
            return model;
        }



        public override TSection Destroy(TSection entity)
        {
            return Destroy(entity, true);
        }

        public virtual TSection Destroy(TSection entity, bool UpdateParent)
        {
            base.Destroy(entity);
            if(UpdateParent) OnValuesUpdated(entity);
            return entity;
        }

        public override IEnumerable<TSection> Destroy(IEnumerable<TSection> entities)
        {
           return Destroy(entities, true);
        }


        public virtual IEnumerable<TSection> Destroy(IEnumerable<TSection> entities, bool UpdateParent)
        {
            foreach (var entity in entities)
                Destroy(entity, UpdateParent);

            return entities;
        }
        

        public virtual TSection Copy(Guid sectionID)
        {
            var model = Read().Where(i => i.ID == sectionID).AsNoTracking().First();            
            model.ID = Guid.NewGuid();
            Create(model);
            
            //Copy section items
            var itemService = new GenericService<TLineItem>(DbContext);
            var imageService = new GenericService<TLineItemImage>(DbContext);
            var items = itemService.Read().Where(i => i.SectionID == sectionID && i.ParentItemID == null).Include(i => i.KitItems).OrderBy(i => i.SortPos).AsNoTracking().ToList();
            var itemIds = items.Select(i => i.ID).ToArray();

            var images = imageService.Read().Where(i => itemIds.Contains(i.OwnerID))
                                            .Include(i => i.File)
                                            .ToList()
                                            .Select(i => new TLineItemImage { OwnerID = i.OwnerID, SortPos = i.SortPos, FileID = i.FileID }).ToList();

            foreach (var copy in items)
            {
                Guid oldID = copy.ID;
                var copyKitItems = copy.KitItems.OrderBy(i => i.SortPos).ToList();
                copy.ID = Guid.NewGuid();
                copy.SectionID = model.ID;
                copy.KitItems = null;
                itemService.Create(copy);

                foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = copy.ID; }                

                foreach (var k in copyKitItems) {

                    images.AddRange(imageService.Read().Where(i => i.OwnerID == k.ID).Include(i => i.File).ToArray().Select(i => new TLineItemImage { OwnerID = i.OwnerID, SortPos = i.SortPos, FileID = i.FileID }));

                    Guid oldSubID = k.ID;                    
                    k.ID = Guid.NewGuid();
                    k.SectionID = model.ID;
                    k.ParentItemID = copy.ID;
                    foreach (var img in images.Where(p => p.OwnerID == oldSubID)) { img.OwnerID = k.ID; }

                }
                itemService.Create(copyKitItems);
            }

            imageService.Create(images);

            //Update totals
            OnValuesUpdated(model);

            AddLog(model, string.Format("Section '{1}' copied on {{0}}.", 0, model.ToShortLabel()));

            return model;
        }



        public virtual bool UpdatePositions(Guid sectionID, string direction)
        {
            if (direction.ToLower() == "up") return UpdatePositions(sectionID, MoveDirection.Up);
            else if (direction.ToLower() == "down") return UpdatePositions(sectionID, MoveDirection.Down);
            else if (direction.ToLower() == "top") return UpdatePositions(sectionID, MoveDirection.Top);
            else if (direction.ToLower() == "bottom") return UpdatePositions(sectionID, MoveDirection.Bottom);
            else return false;
        }


        public virtual bool UpdatePositions(Guid sectionID, MoveDirection direction)
        {
            if (!ParentID.HasValue) return false;

            if (direction == MoveDirection.Up || direction == MoveDirection.Down || direction == MoveDirection.Top || direction == MoveDirection.Bottom)
            {
                string tempUserId = CurrentUserID;
                CurrentUserID = null;

                var move = Read().Where(i => i.ID == sectionID).First();
                List<Guid> ids = Read(ParentID).OrderBy(i => i.SortPos)
                                              .Select(i => i.ID)
                                              .ToList();

                // Rearranges item in requested order
                if (ids.Count > 0)
                {
                    // Get the index of destination row                        
                    int destinationIndex = ids.IndexOf(move.ID);

                    //Adjust (-/+) the position based on command.
                    if (direction == MoveDirection.Up) destinationIndex--;
                    else if (direction == MoveDirection.Down) destinationIndex++;
                    else if (direction == MoveDirection.Top) destinationIndex = 0;
                    else if (direction == MoveDirection.Bottom) destinationIndex = ids.Count - 1;

                    if (destinationIndex < 0) destinationIndex = 0;
                    else if (destinationIndex >= ids.Count - 1) destinationIndex = ids.Count - 1;

                    // Remove and re-insert at specified index
                    ids.Remove(move.ID);
                    ids.Insert(destinationIndex, move.ID);
                }


                var items = Read(ParentID).ToList();

                foreach (var i in items)
                {
                    i.SortPos = ids.IndexOf(i.ID);
                }

                Update(items, false, new string[] { "SortPos" });

                CurrentUserID = tempUserId;
                AddLog(move, string.Format("{1} '{2}' update from {{0}}.", 0, LoggingName(), move.ToShortLabel()));

                return true;
            }
            else
            {
                return false;
            }            
            
        }



        public virtual void OnValuesUpdated(TSection model)
        {
            throw new NotImplementedException("OnValuesUpdated function has not been overwritten.");
        }
    }

    

}
