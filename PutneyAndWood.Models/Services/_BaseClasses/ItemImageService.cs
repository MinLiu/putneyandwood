﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.IO;

namespace SnapSuite.Internal
{

    public class ItemImageService<TLineItem, TImage, TImageViewModel> : ChildEntityService<TLineItem, TImage, TImageViewModel>, IImageService<TImage>
        where TLineItem : class, IEntity, new()
        where TImage : class, IImageItem<TLineItem>, new()
        where TImageViewModel : class, IEntityViewModel, new()
    {
        public override string[] ReferencesToInclude { get { return new string[] { "Owner", "File" }; } }
        public override string LoggingName() { return "Image"; }
        public override string LoggingNamePlural() { return "Image"; }
        public string RelativeSavePath { get; set; }

        public override bool ApplyParentCompanyID { get { return false; } }
        public override string ParentIdColumn { get { return "OwnerID"; } }

        protected readonly FileService fileService;

        public ItemImageService(Guid CompanyID, Guid? OrderID, string UserId, ModelMapper<TImage, TImageViewModel> mapper, DbContext dbContext)
            : base(CompanyID, OrderID, UserId, mapper, dbContext)
        {
            this.fileService = new FileService(dbContext);
            this.RelativeSavePath = "/Images/";
        }

        public override TImage Create(TImage entity)
        {
            //Get the last position
            int lastPos = 0;
            try { lastPos = Read(null).Where(i => i.OwnerID == entity.OwnerID).OrderByDescending(i => i.SortPos).Select(i => i.SortPos).First(); }
            catch { }

            entity.SortPos = lastPos + 1;
            return base.Create(entity);
        }


        public override IEnumerable<TImage> Create(IEnumerable<TImage> entities)
        {
            foreach (var entity in entities) { Create(entity); }
            return entities;
        }

        /*
        public override TImage Destroy(TImage entity)
        {
            var file = entity.File;
            base.Destroy(entity);

            if (file != null) fileService.DeleteFile(file.ID);

            return entity;
        }


        public override IEnumerable<TImage> Destroy(IEnumerable<TImage> entities)
        {
            foreach (var en in entities)
                Destroy(en);

            return entities;
        }
        */

        public TImage UploadImage(Guid ownerID, HttpPostedFileBase uploadedImage)
        {
            return UploadImage(ownerID, uploadedImage.ToByteArray(), uploadedImage.FileName, uploadedImage.ContentType);
        }


        public TImage UploadImage(Guid ownerID, byte[] uploadedImage, string filename, string fileType = "image")
        {
            const string relativePath = "/Images/";

            if (uploadedImage == null) throw new Exception("No Image uploaded.");
            if (!fileType.Contains("image")) throw new Exception("Uploaded file is not an Image.");
            if (uploadedImage.Length > 2 * 1024 * 1024) throw new Exception("Uploaded file is too big.");
            if (Read(null).Where(i => i.OwnerID == ownerID).Count() >= 10) throw new Exception("10 image limit reached.");

            //Save file on Server
            var model = new TImage();
            model.OwnerID = ownerID;
            model.FileID = fileService.SaveFile(CompanyID, uploadedImage, Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename), relativePath).ID;

            return Create(model);
        }
        

        public virtual bool UpdatePositions(Guid itemID, int oldPosition, int newPosition)
        {
            TImage move = null;
            TImage top = null;
            move = Read().Where(i => i.ID == itemID).First();
            var items = Read().Where(i => i.OwnerID == move.OwnerID).OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            top = (newPosition - 1 > 0) ? items.ElementAt(newPosition - 1) : null;
            items.Insert(newPosition, move);


            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
            }

            Update(items, new string[] { "SortPos" });
            return true;
        }

    }

}
