﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.IO;

namespace SnapSuite.Internal
{

    public class GenericAttachmentService<TParent, T> : GenericService<T>
        where TParent : class, new()
        where T : class, IAttachment, IEntity, new()
    {
        public Guid CompanyID { get; set; }
        public Guid? ParentID { get; set; }
        public int MaxContentLengthInMbs { get { if (this.fileService != null) return this.fileService.MaxContentLengthInMbs; else return 5; } set { if (this.fileService != null) this.fileService.MaxContentLengthInMbs = value; } }
        public string RelativeSavePath { get; set; }
        

        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string ParentName { get { return typeof(TParent).Name; } }
        /// <summary> Returns the name of the parent reference ID column.</summary>
        public virtual string ParentIdColumn { get { return string.Format("{0}ID", typeof(TParent).Name); } }

        public virtual bool ApplyParentCompanyID { get; set; }

        protected readonly FileService fileService;

        public GenericAttachmentService(Guid CompanyID, Guid? ParentID, DbContext dbContext)
            : base(dbContext)
        {
            this.fileService = new FileService(dbContext);
            this.RelativeSavePath = "/Attachments/";
            this.CompanyID = CompanyID;
            this.ParentID = ParentID;
            this.MaxContentLengthInMbs = 5;
            this.ApplyParentCompanyID = true;
        }


        /// <summary>Read the entries from the database context.</summary>
        /// <param name="ParentID">The Parent reference ID.</param>
        /// <returns>A query of the items.</returns>
        public virtual IQueryable<T> Read(Guid? ParentID)
        {
            var query = base.Read();

            if (ParentID.HasValue && ParentID.Value != Guid.Empty)
                query = base.Read().Where(string.Format("{0}.Equals(Guid(\"{1}\"))", ParentIdColumn, ParentID.Value));
            else
                query = base.Read();

            if (CompanyID != Guid.Empty && ApplyParentCompanyID)
                query = query.Where(string.Format("{0}.CompanyID.Equals(Guid(\"{1}\"))", ParentName, CompanyID));
            // else

            return query.Include(i => i.File);

        }


        // <summary>Create entity in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entity">The entity to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override T Create(T entity)
        {
            SetCompanyParentIdIfEmpty(entity);
            return base.Create(entity);
        }


        /// <summary>Create entities in database context. ParentID is automatically set if entity parent ID is null.</summary>
        /// <param name="entities">Collection of entities to insert into database.</param>
        /// <returns>Copy of newly created entity in database.</returns>
        public override IEnumerable<T> Create(IEnumerable<T> entities)
        {
            foreach (var en in entities)
            {
                SetCompanyParentIdIfEmpty(en);
            }
            return base.Create(entities);
        }


        /// <summary>Method called during create function. Sets the parent ID if null or empty.</summary>
        protected virtual void SetCompanyParentIdIfEmpty(T entity)
        {
            var pInfo = entity.GetType().GetProperty(ParentIdColumn);
            if (pInfo != null && ParentID.HasValue) pInfo.SetValue(entity, ParentID.Value, null);

            pInfo = entity.GetType().GetProperty("CompanyID");
            if (pInfo != null && CompanyID != Guid.Empty) pInfo.SetValue(entity, CompanyID, null);
            
        }

        /*
        public override T Destroy(T entity)
        {
            var file = entity.File;
            base.Destroy(entity);

            if (file != null) fileService.DeleteFile(file.ID);

            return entity;
        }


        public override IEnumerable<T> Destroy(IEnumerable<T> entities)
        {
            foreach (var en in entities)
                Destroy(en);

            return entities;
        }
        */

        public virtual T UploadAttachment(byte[] file, string filename)
        {
            if (CompanyID == Guid.Empty) throw new Exception("Can not upload attachment if company ID is not set.");

            //If larger then Xmbs then skip
            if (file.Length > MaxContentLengthInMbs * 1024 * 1024) throw new Exception(string.Format("Attachment file is too big. Max file size is {0}Mbs.", MaxContentLengthInMbs));

            //Save file on Server            
            var fileEntry = fileService.SaveFile(CompanyID, file, Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename), RelativeSavePath);

            return CompleteUpload(CompanyID, Path.GetFileName(filename), fileEntry);
        }



        public T UploadAttachment(HttpPostedFileBase file)
        {
            return UploadAttachment(file, Path.GetFileName(file.FileName));
        }

        public virtual T UploadAttachment(HttpPostedFileBase file, string filename)
        {
            if (CompanyID == Guid.Empty) throw new Exception("Can not upload attachment if company ID is not set.");

            //If larger then Xmbs then skip
            if (file.ContentLength > MaxContentLengthInMbs * 1024 * 1024) throw new Exception(string.Format("Attachment file is too big. Max file size is {0}Mbs.", MaxContentLengthInMbs));

            //Save file on Server            
            var fileEntry = fileService.SaveFile(CompanyID, file, RelativeSavePath);

            return CompleteUpload(CompanyID, filename, fileEntry);
        }



        private T CompleteUpload(Guid companyID, string filename, FileEntry file)
        {           
            var upload = new T { Filename = filename, FileID = file.ID };            
            return Create(upload);
        }


       
        public virtual T LinkAttachment(string link)
        {
            if (CompanyID == Guid.Empty) throw new Exception("Can not upload attachment if company ID is not set.");

            var upload = new T { Filename = link, FileID = fileService.SaveLink(link).ID };
            return Create(upload);
        }
        
       

    }

}
