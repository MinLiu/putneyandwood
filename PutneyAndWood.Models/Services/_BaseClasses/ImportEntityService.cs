﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using SnapSuite.Models;
using System.Web;
using System.Data;
using System.IO;

namespace SnapSuite.Internal
{
    /// <summary>
    /// A service instance used for importing models.
    /// </summary>
    /// <typeparam name="TModel">Entity model.</typeparam>
    public class ImportEntityService<TModel> : GenericService<TModel>
        where TModel : class, new()        
    {
        // <summary> The Company ID value.</summary>
        public virtual Guid CompanyID { get; set; }
        /// <summary>User ID who is using the service.</summary>
        public virtual string CurrentUserID { get; set; }

        public virtual string[] RequiredImportColumns { get { return new string[] { }; } }

        public ImportEntityService(Guid CompanyID, string CurrentUserID, DbContext db)
            : base(db)
        {
            this.CompanyID = CompanyID;
            this.CurrentUserID = CurrentUserID;
        }


        public virtual ImportResult ImportExcelFile(HttpPostedFileBase excelFile, string sheetName = "", bool deleteFile = true)
        {
            if (excelFile == null) return new ImportResult { Exception = new Exception("No file has been uploaded.") };
            if (Path.GetExtension(excelFile.FileName) != ".xls" && Path.GetExtension(excelFile.FileName) != ".xlsx") return new ImportResult { Exception = new Exception("File is the wrong file format.") };

            var dataTable = ImportService.CopyExcelFileToTable(CompanyID, excelFile, sheetName, deleteFile);
            return ImportDataTable(dataTable);
        }

        public virtual ImportResult ImportExcelFile(byte[] bytes, string name, string extension, string sheetName = "", bool deleteFile = true)
        {
            if (bytes == null) return new ImportResult { Exception = new Exception("No file has been uploaded.") };
            if (extension != ".xls" && extension != ".xlsx") return new ImportResult { Exception = new Exception("File is the wrong file format.") };

            var dataTable = ImportService.CopyExcelFileToTable(CompanyID, bytes, name, extension, sheetName, deleteFile);
            return ImportDataTable(dataTable);
        }

        public virtual ImportResult ImportDataTable(DataTable dataTable)
        {
            if (dataTable == null) return new ImportResult { Exception = new Exception("No data table has been uploaded.") };

            string error = "";
            if (!ImportService.IsTableValid(dataTable, RequiredImportColumns, out error)) return new ImportResult { Exception = new Exception(error) };

            var result = CompleteImport(dataTable);
            OnImportCompleted(result);

            return result;
        }


        protected virtual ImportResult CompleteImport(DataTable dataTable)
        {
            return new ImportResult { Exception = new Exception("Import service not complete.") };
        }


        protected virtual void OnImportCompleted(ImportResult result)
        {
            //To override
        }

    }

}

