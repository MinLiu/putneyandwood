﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class OpportunityLogService<TViewModel> : CompanyEntityService<OpportunityLog, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Opportunity Log"; }
        public override string LoggingNamePlural() { return "Opportunity Logs"; }

        //public override string[] ReferencesToInclude { get { return new string[] { "OpportunityStatus", "Opportunity" }; } }
        
        public OpportunityLogService(Guid CompanyID, string UserId, ModelMapper<OpportunityLog, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {

        }




        public OpportunityLog AddLog(Opportunity opp)
        {
            var oppLog = new OpportunityLog()
            {
                OpportunityStatusID = opp.StatusID,
                CompanyID = opp.CompanyID,
                Timestamp = DateTime.Now,
                ClientName = opp.Client.Name,
                OpportunityID = opp.ID
            };

            return Create(oppLog);
        }

    }


}
