﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OpportunityTypeService<TViewModel> : CompanyEntityService<OpportunityType, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Opportunity Type"; }
        public override string LoggingNamePlural() { return "Opportunity Types"; }

        public OpportunityTypeService(Guid CompanyID, string UserId, ModelMapper<OpportunityType, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {

        }



        public override OpportunityType Destroy(OpportunityType entity)
        {
            
            //Lazy load of relevent references and collections      
            DbContext.Entry(entity).Collection(i => i.Statuses).Load();

            if (entity.Statuses.Count() == 0 || entity.Statuses.Sum(x => x.Opportunities.Count()) == 0)
            {
                var statusService = new OpportunityStatusService<EmptyViewModel>(CompanyID, null, null, DbContext);
                statusService.Destroy(statusService.Read().Where(i => i.OpportunityTypeID == entity.ID).ToList());
                return base.Destroy(entity);
            }
            else
            {
                throw new Exception("Failed to remove this opportunity type because there may be relevant opportunities.");
            }
        }


        public override IEnumerable<OpportunityType> Destroy(IEnumerable<OpportunityType> entities)
        {
            foreach(var entity in entities)
            {
                Destroy(entity);
            }
            return entities;
        }

    }


}
