﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OpportunityService<TViewModel> : CompanyEntityService<Opportunity, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Opportunity"; }
        public override string LoggingNamePlural() { return "Opportunities"; }

        public override string[] ReferencesToInclude { get { return new string[] { "Logs", "OpportunityType", "AssignedUser", "Client", "ClientContact", "Creator", "Events" }; } }
        public override string SortingOrder { get { return "SortPos"; } }


        public OpportunityService(Guid CompanyID, string UserId, ModelMapper<Opportunity, TViewModel> mapper, DbContext dbContext)
            : base(CompanyID, UserId, mapper, dbContext)
        {
            
        }


        public IQueryable<Opportunity> Search(Guid oppStatusID, string filterText, string assignedUser, bool showArchived = false, bool ignoreTeams = false)
        {            
            return ReadList(showArchived, ignoreTeams).Where(t =>
                    t.StatusID == oppStatusID
                    && (t.AssignedUserID.Equals(assignedUser) || assignedUser == "-1" && t.AssignedUserID == null || string.IsNullOrEmpty(assignedUser))
                    && (string.IsNullOrEmpty(filterText) || t.Name.ToLower().Contains(filterText.ToLower()) || t.Client.Name.ToLower().Contains(filterText.ToLower()))
                );
        }

        public virtual IQueryable<Opportunity> Search(DateType dateType, string fromDate = null, string toDate = null, bool showArchived = false, bool ignoreTeams = false)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromDate, toDate);
            return ReadList(showArchived, ignoreTeams).Where(o => o.LastUpdated >= fmDate && o.LastUpdated <= tDate);
        }


        public virtual IQueryable<Opportunity> ReadList(bool showArchived = false, bool ignoreTeams = false)
        {
            if (ignoreTeams)
            {
                return Read().Where(i => (showArchived == true || (showArchived == false && i.Archived == false)));
            }
            else
            {
                var users = new UserService(DbContext).GetCompanyUserIds(CurrentUserID);
                return base.Read().Where(o => users.Contains(o.AssignedUserID) || string.IsNullOrEmpty(o.AssignedUserID))
                                  .Where(i => (showArchived == true || (showArchived == false && i.Archived == false)));
            }

        }


        public override Opportunity Create(Opportunity entity)
        {
            entity.SortPos = Read().Where(x => x.OpportunityTypeID == entity.OpportunityTypeID && x.StatusID == entity.StatusID)
                                   .OrderByDescending(x => x.SortPos)
                                   .Select(x => x.SortPos)
                                   .FirstOrDefault() + 1;

            entity.LastUpdated = DateTime.Now;
            entity.Created = DateTime.Now;
            entity = base.Create(entity);

            // Add Log
            new OpportunityLogService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).AddLog(entity);

            return entity;
        }


        public override IEnumerable<Opportunity> Create(IEnumerable<Opportunity> entities)
        {
            foreach (var entity in entities)
            {
                entity.SortPos = Read().Where(x => x.OpportunityTypeID == entity.OpportunityTypeID && x.StatusID == entity.StatusID)
                                   .OrderByDescending(x => x.SortPos)
                                   .Select(x => x.SortPos)
                                   .FirstOrDefault() + 1;

                entity.LastUpdated = DateTime.Now;
                entity.Created = DateTime.Now;
            }
                
            return base.Create(entities);
        }

        public override Opportunity Update(Opportunity entity, string[] updatedColumns = null)
        {
            entity.LastUpdated = DateTime.Now;
            return base.Update(entity, updatedColumns);
        }

        public override IEnumerable<Opportunity> Update(IEnumerable<Opportunity> entities, string[] updatedColumns = null)
        {
            foreach(var entity in entities)
                entity.LastUpdated = DateTime.Now;

            return base.Update(entities, updatedColumns);
        }


        public Opportunity ChangeStatus(Guid ID, Guid statusID, int pos)
        {
            var model = FindByID(ID);
            var oriStatusID = model.StatusID;

            var statusService = new OpportunityStatusService<TViewModel>(CompanyID, CurrentUserID, null, DbContext);

            model.StatusID = statusID;
            model.SortPos = pos;
            model.LastUpdated = DateTime.Now;
            Update(model, new string[] { "StatusID", "SortPos", "LastUpdated" });

            // Reorder
            int index = 1;
            var oppsInOriStatus = Read().Where(x => x.StatusID == oriStatusID)
                                       .OrderBy(x => x.SortPos);

            foreach (var item in oppsInOriStatus)
            {
                item.SortPos = index++;
            }

            LogChanges = false;
            Update(oppsInOriStatus, new string[] { "SortPos", "LastUpdated" } );

            var oppsInNewStatus = Read()
                                       .Where(x => x.StatusID == statusID)
                                       .Where(x => x.SortPos >= pos)
                                       .Where(x => x.ID != model.ID)
                                       .OrderBy(x => x.SortPos);
            foreach (var item in oppsInNewStatus)
            {
                item.SortPos += 1;
            }

            Update(oppsInNewStatus, new string[] { "SortPos", "LastUpdated" });
            LogChanges = false;

            // Add Log
            new OpportunityLogService<TViewModel>(CompanyID, CurrentUserID, null, DbContext).AddLog(model);

            return model;
        }

        
        public Opportunity Reorder(Guid id, int offset)
        {
            LogChanges = false;

            var opp = FindByID(id);
            var oldPosition = opp.SortPos;
            var newPosition = opp.SortPos + offset;

            var opps = opp.Status.Opportunities
                                 .Where(x => (x.SortPos >= oldPosition && x.SortPos <= newPosition) || (x.SortPos <= oldPosition && x.SortPos >= newPosition))
                                 .OrderBy(x => x.SortPos)
                                 .ToList();

            for (int i = 0; i < opps.Count; i++)
            {
                var item = opps[i];

                if (i == 0 && offset > 0)
                    item.SortPos = item.SortPos + offset;
                else if (i == opps.Count - 1 && offset < 0)
                    item.SortPos = item.SortPos + offset;
                else
                    item.SortPos = item.SortPos - (offset / Math.Abs(offset));
            }

            Update(opps, new string[] { "SortPos", "LastUpdated" });

            LogChanges = true;

            OnUpdate(opp);

            return opp;
        }

    }


}
