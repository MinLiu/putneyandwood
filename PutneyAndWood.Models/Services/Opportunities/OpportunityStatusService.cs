﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class OpportunityStatusService<TViewModel> : CompanyEntityService<OpportunityStatus, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Opportunity Status"; }
        public override string LoggingNamePlural() { return "Opportunity Statuses"; }

        public OpportunityStatusService(Guid CompanyID, string UserId, ModelMapper<OpportunityStatus, TViewModel> mapper, DbContext db)
            : base(CompanyID, UserId, mapper, db)
        {

        }

        

        public OpportunityStatus Reorder(Guid ID, int offset)
        {
            
            var status = FindByID(ID);
            if (status == null) return null;

            //Load relevent references adn collections
            DbContext.Entry(status).Reference(i => i.OpportunityType).Load();
            //DbContext.Entry(status).Reference(i => i.OpportunityType.Statuses).Load();

            var oldPosition = status.SortPos;
            var newPosition = oldPosition + offset;

            var statuses = status.OpportunityType.Statuses
                                                 .Where(x => (x.SortPos >= oldPosition && x.SortPos <= newPosition) || (x.SortPos <= oldPosition && x.SortPos >= newPosition))
                                                 .OrderBy(x => x.SortPos)
                                                 .ToList();

            LogChanges = false;
            for (int i = 0; i < statuses.Count; i++)
            {
                var item = statuses[i];
                if (i == 0 && offset > 0)
                {
                    item.SortPos = item.SortPos + offset;
                }
                else if (i == statuses.Count - 1 && offset < 0)
                {
                    item.SortPos = item.SortPos + offset;
                }
                else
                {
                    item.SortPos = item.SortPos - (offset / Math.Abs(offset));
                }
            }

            Update(statuses);           
            LogChanges = true;

            OnUpdate(status);
            return status;
        }




        public override OpportunityStatus Destroy(OpportunityStatus entity)
        {
            //Load relevent references adn collections
            DbContext.Entry(entity).Collection(i => i.Opportunities).Load();

            if (entity.Opportunities.Count() > 0)
            {
                var logService = new OpportunityLogService<EmptyViewModel>(CompanyID, null, null, DbContext);
                logService.Destroy(logService.Read().Where(i => i.OpportunityStatusID == entity.ID).ToList());
                return base.Destroy(entity);
            }
            else
            {
                throw new Exception("Failed to remove this opportunity type because there may be relevant opportunities.");
            }
        }


        public override IEnumerable<OpportunityStatus> Destroy(IEnumerable<OpportunityStatus> entities)
        {
            foreach (var entity in entities)
            {
                Destroy(entity);
            }
            return entities;
        }
                

    }


}
