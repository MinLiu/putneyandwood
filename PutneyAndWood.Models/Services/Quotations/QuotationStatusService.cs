﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationStatusService<TViewModel> : OrderStatusService<QuotationStatus, TViewModel>
        where TViewModel : class, new()
    {
        
        public QuotationStatusService(ModelMapper<QuotationStatus, TViewModel> mapper, DbContext dbContext)
            : base(mapper, dbContext)
        {

        }
        
    }


}
