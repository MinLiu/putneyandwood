﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationBasisOfEstimateService : GenericService<QuotationBasisOfEstimate>
    {
        public QuotationBasisOfEstimateService(SnapDbContext dbContext): base(dbContext)
        {
        }

        public override IQueryable<QuotationBasisOfEstimate> Read()
        {
            return base.Read().OrderBy(x => x.SortPos);
        }

        public virtual QuotationBasisOfEstimate FindByID(Guid ID)
        {
            return base.Read().Where(i => i.ID == ID).First();
        }

        public virtual QuotationBasisOfEstimate AddBasisFromTemplate(Guid templateID, Guid quotationID)
        {
            var template = new BasisOfEstimateService(new SnapDbContext()).FindByID(templateID);
            var last = Read().Where(x => x.QuotationID == quotationID).OrderByDescending(x => x.SortPos).FirstOrDefault();
            var model = new QuotationBasisOfEstimate()
            {
                Description = template.Description,
                QuotationID = quotationID,
                SortPos = last?.SortPos + 1 ?? 1,
                IsSubHeading = template.IsSubHeading,
                IsSubItem = template.IsSubItem,
            };
            Create(model);

            UpdateQuotationLastUpdateTime(quotationID);

            return model;
        }

        public virtual List<QuotationBasisOfEstimate> AddAllBasisesFromTemplate(Guid quotationID)
        {
            var templates = new BasisOfEstimateService(new SnapDbContext()).Read().OrderBy(x => x.SortPos).ToList();
            var toAdd = new List<QuotationBasisOfEstimate>();
            var sortPos = 1;
            foreach(var template in templates)
            {
                var model = new QuotationBasisOfEstimate()
                {
                    Description = template.Description,
                    QuotationID = quotationID,
                    SortPos = sortPos++,
                    IsSubHeading = template.IsSubHeading,
                    IsSubItem = template.IsSubItem,
                };
                toAdd.Add(model);
            }
            Create(toAdd);

            UpdateQuotationLastUpdateTime(quotationID);

            return toAdd;
        }

        private void UpdateQuotationLastUpdateTime(Guid quotationID)
        {
            // Update quotation to make sure it generate PDF
            var quotatoinservice = new QuotationService<EmptyViewModel>(Guid.Empty, null, null, new SnapDbContext());
            var quote = quotatoinservice.FindByID(quotationID);
            quote.LastUpdated = DateTime.Now;
            quotatoinservice.Update(quote, new string[] { "LastUpdated" });
        }

    }

}
