﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationAttachmentService<TViewModel> : OrderAttachmentService<Quotation, QuotationAttachment, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationAttachmentService(Guid CompanyID, Guid quotationID, string UserId, ModelMapper<QuotationAttachment, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }
        
    }
}
