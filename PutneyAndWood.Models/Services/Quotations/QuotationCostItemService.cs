﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationCostItemService<TViewModel> : OrderCostItemService<Quotation, QuotationCostItem, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationCostItemService(Guid CompanyID, Guid quotationID, string UserId, OrderItemModelMapper<QuotationCostItem, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }


        public override void OnValuesUpdated(QuotationCostItem model)
        {
            var quotationService = new QuotationService<TViewModel>(CompanyID, null, null, DbContext);
            quotationService.UpdateValues(model.QuotationID);
        }
    }
}
