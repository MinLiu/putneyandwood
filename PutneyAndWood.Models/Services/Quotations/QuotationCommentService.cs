﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationCommentService<TViewModel> : OrderCommentService<Quotation, QuotationComment, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string LoggingName() { return "Comment"; }
        public override string LoggingNamePlural() { return "Comment"; }

        public QuotationCommentService(Guid CompanyID, Guid quotationID, string UserId, ModelMapper<QuotationComment, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }


        public override QuotationComment Create(QuotationComment entity)
        {
            entity.Timestamp = DateTime.Now;
            var user = new UserService(DbContext).FindById(CurrentUserID);
            entity.Email = (user != null) ? user.Email : "";

            return base.Create(entity);
        }


        public override IEnumerable<QuotationComment> Create(IEnumerable<QuotationComment> entities)
        {
            foreach (var entity in entities)
            {
                entity.Timestamp = DateTime.Now;
                var user = new UserService(DbContext).FindById(CurrentUserID);
                entity.Email = (user != null) ? user.Email : "";
            }

            return base.Create(entities);
        }
        

    }
}
