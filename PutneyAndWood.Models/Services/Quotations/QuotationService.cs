﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Models
{
    
    public class QuotationService<TViewModel> : OrderWithCostsLabourService<Quotation, CustomQuotationField, TViewModel, QuotationSection, QuotationItem, QuotationItemImage, QuotationCostItem, QuotationLabourItem, QuotationAttachment, QuotationComment, QuotationNotification, QuotationTemplate, QuotationPreviewItem, QuotationSettings, DefaultQuotationAttachment, DefaultQuotationNotification>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Client", "ClientContact", "Status", "AssignedUser", "SignedAgreement", "Currency", "StripePayment",  }; } }

        
        public QuotationService(Guid CompanyID, string currentUserID, OrderModelMapper<Quotation, CustomQuotationField, TViewModel> mapper, DbContext db) :
            base(CompanyID, currentUserID, mapper, db)
        {

        }

        public override IQueryable<Quotation> Read()
        {
            return Read(false, false, false);
        }

        public override IQueryable<Quotation> Read(bool deleted, bool ignoreTeams)
        {
            return Read(deleted, ignoreTeams, false);
        }

        public virtual IQueryable<Quotation> Read(bool deleted, bool ignoreTeams, bool includeClientVersion)
        {
            if (includeClientVersion)
            {
                return base.Read(deleted, ignoreTeams);
            }
            else
            {
                return base.Read(deleted, ignoreTeams).Where(i => i.IsClientVersion == false || includeClientVersion);
            }
        }

        public virtual IQueryable<Quotation> Search(string filterText, DateType dateType, ICollection<int> statuses, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            filterText = (filterText ?? "").ToLower();

            var query = base.Search(dateType, statuses, fromDate, toDate, assignedUserId, deleted);
            return query.Where(q => q.IsClientVersion == false && 
                                    (((q.Reference + q.Number).ToLower().Contains(filterText)
                                        || q.Description.ToLower().Contains(filterText)
                                        || q.Client.Name.ToLower().Contains(filterText)
                                        || q.ClientReference.ToLower().Contains(filterText)
                                        || q.JsonCustomFieldValues.ToLower().Contains(filterText)
                                    ) || string.IsNullOrEmpty(filterText)));
        }

        public virtual IQueryable<Quotation> Search(Guid? projectID, string filterText, string statuses)
        {
            var statusList = string.IsNullOrWhiteSpace(statuses) ? 
                new List<int>() 
                : statuses.Split(',').Select(x => int.Parse(x)).ToList();
            filterText = (filterText ?? "").ToLower();
            return Read()
                .Where(x => projectID == null || x.ProjectID == projectID)
                .Where(x => projectID != null || statusList.Contains(x.StatusID))
                .Where(x => filterText == "" || x.Description.ToLower().Contains(filterText) || x.Client.Name.ToLower().Contains(filterText));
        }

        protected override Quotation Create1_ReturnNew(Company company, User user, QuotationSettings settings, Guid projectID)
        {
            var quotation = base.Create1_ReturnNew(company, user, settings, projectID);
            quotation.Note = settings.DefaultNote;
            quotation.EstimatedDelivery = DateTime.Now;
            quotation.InvoiceAddressID = null;
            quotation.DeliveryAddressID = null;
            quotation.Version = 1;
            quotation.AllowClientEditLineItems = settings.DefaultAllowClientEditLineItems;
            quotation.AllowClientEditSections = settings.DefaultAllowClientEditSections;
            quotation.NeedsCheckedClientSubmission = settings.DefaultNeedsCheckedClientSubmission;
            quotation.Note = "Following your recent enquiry/our recent meeting/conversation, we are pleased to submit our estimate for cleaning the existing stone facade, storage of dismantled stonework and new stone cladding as detailed below:";
            quotation.ClosingParagraph = "Please feel free to contact us if you need any clarifications or further information.";
            quotation.Greeting = "Sirs";
            quotation.EstimatorTitle = "Estimator";
            return quotation;
        }

        protected override void Create2_AddItems(Quotation order, Company company, User user, QuotationSettings settings)
        {
            base.Create2_AddItems(order, company, user, settings);
            AddBasisOfEstimates(order, company, user, settings);
        }

        protected void AddBasisOfEstimates(Quotation order, Company company, User user, QuotationSettings settings)
        {
            var service = new QuotationBasisOfEstimateService(new SnapDbContext());
            service.AddAllBasisesFromTemplate(order.ID);
        }

        protected override void Create8_StripePayments(Quotation order, Company company, User user, QuotationSettings settings)
        {
            //base.Create8_StripePayments(order, company, user, settings);
            if (settings.StripePaymentsOnNew)
            {
                var paymentService = new GenericService<QuotationStripePayment>(DbContext);
                var payment = new QuotationStripePayment
                {
                    OwnerID = order.ID, 
                    Description = settings.DefaultStripeDescription,
                    FixedAmount = settings.DefaultStripeFixedAmount,
                    PercentageToPay = settings.DefaultStripePercentageToPay,
                    AmountToPay = settings.DefaultStripeAmountToPay,
                };

                paymentService.Create(payment);
            }
            
        }

        protected override Quotation Copy1_ReturnNew(Quotation originalOrder, User user, QuotationSettings settings)
        {
            var quotation = base.Copy1_ReturnNew(originalOrder, user, settings);
            quotation.Version = 1;
            quotation.IsClientDraft = false;
            quotation.IsClientVersion = false;
            return quotation;
        }

        protected override void Copy2_AddItems(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            base.Copy2_AddItems(order, originalOrder, company, user, settings);
            CopyQualifications(order, originalOrder, company, user, settings);
            CopyBasis(order, originalOrder, company, user, settings);
        }

        protected virtual void CopyQualifications(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            // Add Qualifications
            var qualificationRepo = new QuotationQualificationRepository();
            var qualificationsToAdd = new List<QuotationQualification>();
            foreach (var qualification in originalOrder.Qualifications)
            {
                qualificationsToAdd.Add(new QuotationQualification
                {
                    QuotationID = order.ID,
                    SortPos = qualification.SortPos,
                    Description = qualification.Description,
                    IsSubHeading = qualification.IsSubHeading,
                    IsSubItem = qualification.IsSubItem,
                });
            }
            qualificationRepo.Create(qualificationsToAdd);
        }

        protected virtual void CopyBasis(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            // Add Basis of Estimates
            var basisOfEstimateRepo = new QuotationBasisOfEstimateRepository();
            var basisToAdd = new List<QuotationBasisOfEstimate>();
            foreach (var basis in originalOrder.QuotationBasisOfEstimates)
            {
                basisToAdd.Add(new QuotationBasisOfEstimate()
                {
                    QuotationID = order.ID,
                    Description = basis.Description,
                    IsSubHeading = basis.IsSubHeading,
                    SortPos = basis.SortPos,
                    IsSubItem = basis.IsSubItem,
                });
            }
            basisOfEstimateRepo.Create(basisToAdd);
        }

        protected override void Copy8_StripePayments(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            //base.Copy8_StripePayments(order, originalOrder, company, user, settings);
            var paymentService = new GenericService<QuotationStripePayment>(DbContext);
            var payment = paymentService.Read().Where(i => i.OwnerID == originalOrder.ID).AsNoTracking().DefaultIfEmpty(null).FirstOrDefault();

            if(payment != null)
            {
                payment.OwnerID = order.ID;
                payment.IsPaid = false;
                payment.DateOfPayment = null;
                payment.StripeChargeID = null;
                //payment.StripeCustomerID = null;
                //payment.StripeDescription = null;
                //payment.StripeCurrency = null;
                //payment.StripeAmount = null;
                //payment.StripeDateCreated = null;

                paymentService.Create(payment);
            }

        }


        public virtual Quotation Version(Guid originalID, string userID, bool isClientVersion = false)
        {
            
            //Get original order
            var original = Read(CompanyID).Where(i => i.ID == originalID)
                                      .Include(i => i.Sections)
                                      .Include(i => i.Items)
                                      .Include(i => i.Attachments)
                                      .Include(i => i.Notifications)
                                      .Include(i => i.PreviewItems)
                                      //.AsNoTracking()
                                      .DefaultIfEmpty(null)
                                      .FirstOrDefault();

            if (original == null) return null;

            var company = new GenericService<Company>(DbContext).Read().Where(i => i.ID == CompanyID).DefaultIfEmpty(null).FirstOrDefault();
            var user = new GenericService<User>(DbContext).Read().Where(i => i.Id == userID).DefaultIfEmpty(null).FirstOrDefault();
            var settings = new OrderSettingsService<QuotationSettings, TViewModel, DefaultQuotationAttachment, DefaultQuotationNotification>(null, null, DbContext).FindByID(CompanyID);
            if (company == null || settings == null) return null;

            var newOrder = Version1_ReturnNew(original, user, settings);
            if (isClientVersion)
            {
                if (original.IsClientVersion) throw new Exception("You can not create a client version of an existing client version of a quotation");
                newOrder.Version = original.Version;
                newOrder.IsClientVersion = true;
                newOrder.IsClientDraft = true;
                newOrder.MainVersionID = originalID;
                newOrder.CreatedBy = "Client";
            }
            
            LogChanges = false;
            Create(newOrder);
            LogChanges = false;

            Copy2_AddItems(newOrder, original, company, user, settings);
            Copy3_AddAttachments(newOrder, original, company, user, settings);
            Version4_AddComments(newOrder, original, company, user, settings);
            Copy5_AddPreviews(newOrder, original, company, user, settings);
            //Create5_AddDefaultNotifications(newOrder, company, user, settings);
            Version6_AddLogs(newOrder, original, company, user, settings);
            Version7_SendNotifications(newOrder, original, company, user, settings);
            Copy8_StripePayments(newOrder, original, company, user, settings);

            return Find(newOrder.ID);
        }


        protected virtual Quotation Version1_ReturnNew(Quotation originalOrder, User user, QuotationSettings settings)
        {
            //Copy original
            //Can not query database again or else overwrites origanl values in memory. Strange.
            ///var newOrder = originalOrder;
            var newOrder = MapNewCopy(originalOrder);

            //Get Last Order number based on Settings
            var query = Read(originalOrder.CompanyID).Where(i => i.Reference == originalOrder.Reference && i.Number == originalOrder.Number).OrderByDescending(q => q.Version).Select(i => i.Version).ToList();
            int version = (query.Count > 0) ? query.First() : 0;

            newOrder.ID = Guid.NewGuid();
            newOrder.CheckSum = Guid.NewGuid().ToString("N");
            newOrder.Reference = settings.DefaultReference; //Get from defaults
            newOrder.Version = version + 1;
            newOrder.StatusID = 1;
            newOrder.Created = DateTime.Now;
            newOrder.LastUpdated = DateTime.Now;
            newOrder.AssignedUserID = (user != null) ? user.Id : originalOrder.AssignedUserID; //Assign to order the use
            newOrder.CreatedBy = (user != null) ? user.Email : originalOrder.CreatedBy;
            newOrder.IsApproved = false;
            newOrder.EmailOpened = null;
            newOrder.Deleted = false;
            newOrder.IsClientDraft = false;
            newOrder.IsClientVersion = false;
            
            return newOrder;
        }
        
        protected virtual void Version4_AddComments(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            var commentsService = new GenericService<QuotationComment>(DbContext);

            var comment = new QuotationComment
            {
                Timestamp = DateTime.Now,
                Email = (user != null) ? user.Email : "",
                Message = string.Format("Versioned from {0}: {1}", LoggingName(), originalOrder.ToShortLabel())
            };

            var propInfo = comment.GetType().GetProperty(OrderIdColumn);
            if (propInfo != null) propInfo.SetValue(comment, order.ID, null);

            commentsService.Create(comment);
        }



        protected virtual void Version6_AddLogs(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            if (user == null) return;

            var text = string.Format("{1} {2} versioned. {0} created.", order.ToShortLabel(),
                                                                     LoggingName(),
                                                                     originalOrder.ToLongLabel());
            AddLog(text, user.Id, order.ID.ToString());
        }


        protected virtual void Version7_SendNotifications(Quotation order, Quotation originalOrder, Company company, User user, QuotationSettings settings)
        {
            if (user == null) return;

            //Log Activity
            var text = string.Format("{1} {2} versioned. {0} created by {3}", order.ToShortLabel(),
                                                                           LoggingName(),
                                                                           originalOrder.ToLongLabel(),
                                                                           order.ToShortLabel(), (user != null) ? user.ToLongLabel() : "");
            SendNotifications(order.ID, string.Format("New version {0} created; {1}", LoggingName(), order.ToShortLabel()), text);
        }


        public override decimal CalculateTotalLabourCosts(Guid ID)
        {
            try
            {
                var labourService = new QuotationLabourItemService<QuotationLabourItemViewModel>(CompanyID, ID, null, null, DbContext);
                return decimal.Round(labourService.Read(ID).Sum(c => c.Total), 2);
            }
            catch { return 0; }
        }

        public override decimal CalculateTotalOtherCosts(Guid ID)
        {
            var costItemService = new QuotationCostItemService<EmptyViewModel>(CompanyID, ID, null, null, DbContext);
            
            decimal total_cost = 0.0m;

            foreach (var item in costItemService.Read(ID).ToList())
            {
                if ((item.IsKit && !item.UseKitPrice) || item.IsComment)
                {
                    //Do nothing
                }
                else if (item.ParentCostItem != null)
                {
                    if (!item.ParentCostItem.UseKitPrice)
                    {                        
                        total_cost += decimal.Round(item.Cost * item.Quantity * item.ParentCostItem.Quantity, 2);

                    }
                }
                else
                {
                    total_cost += decimal.Round(item.Cost * item.Quantity, 2);
                }
            }
            
            return decimal.Round(total_cost, 2);
        }



        public override Quotation ChangeStatus(Quotation model, int newStatusID)
        {
            //if (model.StatusID > 1 && newStatusID == QuotationStatusValues.DRAFT && model.SignedAgreement != null)
            //    throw new Exception("Can not convert quotation to Draft if agreement was signed.");

            model = base.ChangeStatus(model, newStatusID);

            var stat = new QuotationStatusService<EmptyViewModel>(null, DbContext).Read().Where(s => s.ID == newStatusID).First();

            SendNotifications(model, "Status Changed to " + stat.Name, "The status of the quotation has been changed to " + stat.Description);
            AddLog(string.Format("{0} status changed to {1}", model.ToLongLabel(), stat.Description), CurrentUserID, model.ID.ToString());

            return model;
        }

        public virtual Quotation SetPriceBreaks(Guid quoteID, bool use)
        {
            var model = FindByID(quoteID);
            return SetPriceBreaks(model, use);
        }

        public virtual Quotation SetPriceBreaks(Quotation model, bool use)
        {
            if (model.StatusID != QuotationStatusValues.DRAFT) throw new Exception("Can not change price breaks settings if not in draft mode.");


            model.UsePriceBreaks = use;
            model.LastUpdated = DateTime.Now;
            Update(model, new string[] { "UsePriceBreaks", "LastUpdated" });


            if (use)
            {
                var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, model.ID, null, null, DbContext);
                var items = itemService.Read(model.ID).ToList();
                foreach (var i in items)
                {
                    i.IsComment = (i.IsComment) ? true : (i.IsKit && i.UseKitPrice);
                    i.IsKit = false;
                    i.ParentItemID = null;
                    itemService.Update(i, new string[] { "IsComment", "IsKit", "ParentItemID" });
                }
            }
            
            //Log Activity
            AddLog(string.Format("Price Breaks {1} for {0}", model.ToShortLabel(), (use) ? "enabled" : "disabled"), CurrentUserID, model.ID.ToString());

            return model;
        }


        public override TViewModel UpdateHeader(TViewModel viewModel)
        {
            var model = FindByID(Guid.Parse(viewModel.ID));

            //Refresh the whole page if the curreny changes.
            Mapper.MapToModelHeader(viewModel, model);
            model.LastUpdated = DateTime.Now;
            Update(model);

            viewModel = Mapper.MapToViewModel(model);


            if (model.UsePriceBreaks)
            {
                var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, model.ID, null, null, DbContext);
                var items = itemService.Read(model.ID).Where(i => i.IsKit == true && i.ParentItemID != null).ToList();
                foreach (var item in items)
                {
                    item.IsKit = false;
                    item.ParentItemID = null;
                }

                itemService.Update(items, true, true, new string[] { "IsKit", "ParentItemID" });
            }

            return viewModel;
        }


        public override Quotation RecalculateAllTotals(Guid ID)
        {
            var quotation = Find(ID);
            if (quotation == null) return null;

            var itemService = new QuotationItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var sectionService = new QuotationSectionService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var costsService = new QuotationCostItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var labourService = new QuotationLabourItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);

            foreach(var id in itemService.Read(ID).Select(i => i.ID).ToArray()) itemService.UpdateValues(id, false, false);
            foreach (var id in sectionService.Read(ID).Select(i => i.ID).ToArray()) sectionService.UpdateValues(id, false);
            foreach (var id in costsService.Read(ID).Select(i => i.ID).ToArray()) costsService.UpdateValues(id, false);
            foreach (var id in labourService.Read(ID).Select(i => i.ID).ToArray()) labourService.UpdateValues(id, false);
            UpdateValues(quotation.ID);

            return quotation;
        }

        public override Quotation RecalculateAllTotalsMinusItems(Guid ID)
        {
            var quotation = Find(ID);
            if (quotation == null) return null;

            //var itemService = new QuotationItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var sectionService = new QuotationSectionService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var costsService = new QuotationCostItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);
            var labourService = new QuotationLabourItemService<TViewModel>(Guid.Empty, quotation.ID, null, null, DbContext);

            //foreach (var id in itemService.Read(ID).Select(i => i.ID).ToArray()) itemService.UpdateValues(id, false, false);
            foreach (var id in sectionService.Read(ID).Select(i => i.ID).ToArray()) sectionService.UpdateValues(id, false);
            foreach (var id in costsService.Read(ID).Select(i => i.ID).ToArray()) costsService.UpdateValues(id, false);
            foreach (var id in labourService.Read(ID).Select(i => i.ID).ToArray()) labourService.UpdateValues(id, false);
            UpdateValues(quotation.ID);

            return quotation;
        }


        public override Quotation AcceptOrder(Guid ID, int newStatus, string acceptBy, string notes, string PoNumber)
        {           
            var quote = FindByID(ID);

            //Update order based on the option selected by user.           
            quote.StatusID = newStatus;
            if (!string.IsNullOrEmpty(PoNumber)) quote.PoNumber = PoNumber;

            if (quote.IsClientVersion)
            {
                if (quote.NeedsCheckedClientSubmission) throw new Exception("You can not accept client custom quotations that need review.");

                quote.IsClientVersion = false;
                var query = Read(quote.CompanyID).Where(i => i.Reference == quote.Reference && i.Number == quote.Number).OrderByDescending(q => q.Version).Select(i => i.Version).ToList();
                quote.Version = (query.Count > 0) ? query.First() + 1 : 1;

                //Delete zeroed items that are not comments or required items.
                var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
                var delItems = itemService.Read().Where(i => i.QuotationID == quote.ID && ((i.IsComment == false && (i.Quantity <= 0 || i.ParentItem.Quantity <= 0) && i.IsRequired == false) || i.Section.IncludeInTotal == false)).ToArray();
                itemService.Destroy(delItems);

                var sectionService = new QuotationSectionService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
                var delSections = sectionService.Read().Where(i => i.QuotationID == quote.ID && i.IncludeInTotal == false && i.IsRequired == false).ToArray();
                sectionService.Destroy(delSections);
                                
                try
                {
                    //Set the origianl to deprecated
                    var original = FindByID(quote.MainVersionID.Value);
                    original.StatusID = QuotationStatusValues.DEPRECATED;
                    Update(original, new string[] { "StatusID" });
                }
                catch { }
            }
            
            Update(quote, new string[] { "StatusID", "PoNumber", "IsClientVersion", "Version" });
            
            //Add internal comment
            new QuotationCommentService<EmptyViewModel>(CompanyID, quote.ID, CurrentUserID, null, DbContext).Create(new QuotationComment
            {
                Timestamp = DateTime.Now,
                Email = acceptBy,
                Message = string.Format("Accept by {0}; Note: {1}", acceptBy, notes)
            });

            var title = string.Format("{{0}} Accepted by {1}", 0, acceptBy);
            var message = string.Format("{{0}} was accepted by {1}. \nNote: {2}", 0, acceptBy, notes);
            SendNotifications(quote, title, message);

            return quote;
        }

        public virtual Quotation SubmitClientVersionForReview(Guid ID)
        {
            var quote = FindByID(ID);
            return SubmitClientVersionForReview(quote);
        }

        public virtual Quotation SubmitClientVersionForReview(Quotation quote)
        {
            //Update order based on the option selected by user.  
            if (!quote.IsClientVersion) throw new Exception("Can not submit non  client version quotations.");

            quote.StatusID = QuotationStatusValues.DRAFT;
            quote.IsClientVersion = false;
            var query = Read(quote.CompanyID).Where(i => i.Reference == quote.Reference && i.Number == quote.Number).OrderByDescending(q => q.Version).Select(i => i.Version).ToList();
            quote.Version = (query.Count > 0) ? query.First() + 1 : 1;

            //Delete zeroed items that are not comments or required items.
            var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var delItems = itemService.Read().Where(i => i.QuotationID == quote.ID && ((i.IsComment == false && (i.Quantity <= 0 || i.ParentItem.Quantity <= 0) && i.IsRequired == false) || i.Section.IncludeInTotal == false)).ToArray();
            itemService.Destroy(delItems);

            var sectionService = new QuotationSectionService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var delSections = sectionService.Read().Where(i => i.QuotationID == quote.ID && i.IncludeInTotal == false && i.IsRequired == false).ToArray();
            sectionService.Destroy(delSections);

            try
            {
                var original = FindByID(quote.MainVersionID.Value);
                original.StatusID = QuotationStatusValues.DEPRECATED;
                Update(original, new string[] { "StatusID" });
            }
            catch { }

            Update(quote, new string[] { "StatusID", "IsClientVersion", "Version" });

            var commentsService = new GenericService<QuotationComment>(DbContext);
            var comment = new QuotationComment { QuotationID = quote.ID, Timestamp = DateTime.Now, Email = "Client", Message = "Client has submitted a customized revision of quotation for review." };
            commentsService.Create(comment);

            SendNotifications(quote, "Client Version Submitted for Review ", "Your client has submitted a customized revision of your quotation for review.");

            return quote;
        }
        public override OrderEmailViewModel GetEmailTitleMessage(Guid ID)
        {
            var quote = FindByID(ID);
            var setting = new QuotationSettingsService<EmptyViewModel>(null, null, DbContext).FindByID(CompanyID);            
            var dictionary = new QuotationPdfGenService(CompanyID, null, DbContext).CreateDictionary(quote);
            dictionary.Add("{BusinessLogo}", (string.IsNullOrWhiteSpace(quote.Company.LogoImageURL)) ? string.Format("<strong>{0}</strong>", quote.Company.Name) : string.Format("<img alt=\"\" height=\"50\" src=\"{0}\" />", FileService.WebURL + quote.Company.LogoImageURL));
            
            string title = setting.EmailTitleTemplate ?? "";
            string body = setting.EmailBodyTemplate ?? "";

            foreach (var dicItem in dictionary)
            {
                title = title.Replace(dicItem.Key, dicItem.Value);
                body = body.Replace(dicItem.Key, dicItem.Value);
            }

            return new OrderEmailViewModel
            {
                Title = title,
                Body = body,
            };
        }


        protected override void AttachPDF(MailMessage mailMessage, Quotation model)
        {
            string filename = model.ToLongLabel().Truncate(80);
            var orderPdfGen = new QuotationPdfGenService(CompanyID, null, DbContext);
            var pdf = orderPdfGen.GetAttachmentPdfFilePath(model);
            Attachment attachment = new Attachment(pdf);
            attachment.ContentId = filename + ".pdf";
            attachment.NameEncoding = System.Text.Encoding.Unicode;
            attachment.Name = filename + ".pdf";
            attachment.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
            mailMessage.Attachments.Add(attachment);
        }


        protected override void OnEmailSent(Quotation model, List<string> successEmails, List<string> errorEmails)
        {
            if (model.StatusID < QuotationStatusValues.SENT)
            {
                model.StatusID = QuotationStatusValues.SENT;
                Update(model, new string[] { "StatusID" });

                //Send notifications here
                SendNotifications(model, "Status Changed to Sent", "The status of the quotation has been changed to Sent.");
            }

            var user = new UserService(DbContext).FindById(CurrentUserID);
            var commentService = new QuotationCommentService<EmptyViewModel>(CompanyID, model.ID, user.Id, null, DbContext);
           
            if (user != null)
                commentService.Create(new QuotationComment { QuotationID = model.ID, Email = user.Email, Message = string.Format("Sent to {0}", string.Join(", ", successEmails)), Timestamp = DateTime.Now });
                
            //Log Activity
            AddLog(string.Format("Quotation '{0}' sent to {1}.", model.ToShortLabel(), string.Join(", ", successEmails)), CurrentUserID, model.ID.ToString());
            
        }




        public DataFileOutput Export(string filterText, DateType dateType, ICollection<int> statuses, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            
            var orders = Search(filterText, dateType, statuses, fromDate, toDate, assignedUserId).OrderByDescending(q => q.Created);            
            var list = orders.AsNoTracking().ToList();
            var customFields = new CustomQuotationFieldService<EmptyViewModel>(CompanyID, null, null, DbContext).Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            int x = 0;

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var quote in list)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(quote.Status.Name); x++;
                row.CreateCell(x).SetCellValue(quote.ToShortLabel()); x++;
                row.CreateCell(x).SetCellValue(quote.Description); x++;
                row.CreateCell(x).SetCellValue((quote.Client != null) ? quote.Client.Name : ""); x++;
                row.CreateCell(x).SetCellValue(quote.ClientReference); x++;
                row.CreateCell(x).SetCellValue(quote.PoNumber); x++;

                quote.CustomFieldValues = quote.CustomFieldValues.AddRemoveRelevent(customFields);
                foreach (var field in quote.CustomFieldValues) { row.CreateCell(x).SetCellValue(field.Value); x++; }

                row.CreateCell(x).SetCellValue(quote.Version); x++;
                row.CreateCell(x).SetCellValue(quote.LastUpdated.ToString("HH:mm dd/MM/yyyy")); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                row.CreateCell(x).SetCellValue((quote.EmailOpened != null) ? quote.EmailOpened.Value.ToString("HH:mm dd/MM/yyyy") : ""); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                row.CreateCell(x).SetCellValue(quote.Created.ToString("HH:mm dd/MM/yyyy")); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                row.CreateCell(x).SetCellValue(quote.CreatedBy); x++;
                row.CreateCell(x).SetCellValue(quote.EstimatedDelivery.ToString("HH:mm dd/MM/yyyy")); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                row.CreateCell(x).SetCellValue(quote.Currency.Code); x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalNet); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalVat); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.Discount); x++;
                row.CreateCell(x).SetCellValue((double)quote.Margin); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalCost); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalOtherCosts); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalLabourCosts); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalProfit); row.GetCell(x).CellStyle = numberStyle; x++;
                row.CreateCell(x).SetCellValue((double)quote.TotalPrice); row.GetCell(x).CellStyle = numberStyle; x++;
            }

            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("Status"); sheet.AutoSizeColumn(x);  x++;
            headerRow.CreateCell(x).SetCellValue("Quote"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Description"); x++;
            headerRow.CreateCell(x).SetCellValue("Client"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Client Ref"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("PO #"); sheet.AutoSizeColumn(x); x++;
            foreach (var header in customFields) { headerRow.CreateCell(x).SetCellValue(header.Name); x++; }
            headerRow.CreateCell(x).SetCellValue("Version"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Modified"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Email Opened"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Created"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Created By"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Delivery Date"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Currency"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Net"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("VAT"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Discount"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Margin"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Item Costs"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Additional Costs"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Labour"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Profit"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Total"); sheet.AutoSizeColumn(x); x++;

            
            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("Quotations Export {0}.xls",DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };            
        }



        public override Quotation Destroy(Quotation entity)
        {
            DestroySet(Read(Guid.Empty).Where(i => i.ID == entity.ID || i.MainVersionID == entity.ID));
            return entity;
        }

        public override IEnumerable<Quotation> Destroy(IEnumerable<Quotation> entities)
        {
            var ids = entities.Select(i => i.ID).ToArray();
            DestroySet(Read(Guid.Empty).Where(i => ids.Contains(i.ID) || ids.Contains(i.MainVersionID.Value)));
            return entities;
        }

        private void DestroySet(IQueryable<Quotation> set)
        {
            var IDs = set.Select(i => i.ID).ToArray();

            var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var sectionService = new QuotationSectionService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var attachService = new QuotationAttachmentService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var commentService = new QuotationCommentService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var notificationService = new QuotationNotificationService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var costItemService = new QuotationCostItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
            var labourService = new QuotationLabourItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);

            var previewItemService = new QuotationPreviewItemService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            var stripeService = new QuotationStripePaymentService<EmptyViewModel>(CompanyID, null, null, DbContext);
            var agreementService = new QuotationAgreementService<EmptyViewModel>(CompanyID, null, null, DbContext);


            itemService.Destroy(itemService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList(), false);
            sectionService.Destroy(sectionService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList(), false);
            attachService.Destroy(attachService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList());
            commentService.Destroy(commentService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList());
            notificationService.Destroy(notificationService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList());
            costItemService.Destroy(costItemService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList(), false);
            labourService.Destroy(labourService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList(), false);
            
            previewItemService.Destroy(previewItemService.Read().Where(i => IDs.Contains(i.ParentID)).ToList());
            stripeService.Destroy(stripeService.Read().Where(i => IDs.Contains(i.OwnerID)).ToList());
            agreementService.Destroy(agreementService.Read().Where(i => IDs.Contains(i.QuotationID)).ToList());

            base.Destroy(set.ToList());
        }


    }
}
