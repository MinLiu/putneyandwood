﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationSectionService<TViewModel> : OrderSectionService<Quotation, QuotationSection, QuotationItem, QuotationItemImage, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
                
        public QuotationSectionService(Guid CompanyID, Guid? quotationID, string UserId, ModelMapper<QuotationSection, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }

        public override void OnValuesUpdated(QuotationSection model)
        {
            var quotationService = new QuotationService<TViewModel>(CompanyID, null, null, DbContext);
            quotationService.UpdateValues(model.QuotationID);
        }


        public override QuotationSection Destroy(QuotationSection entity)
        {
            var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, entity.QuotationID, null, null, DbContext);
            itemService.Destroy(itemService.Read().Where(i => i.SectionID == entity.ID).ToList(), false);
            return base.Destroy(entity);
        }
        
        
    }
}
