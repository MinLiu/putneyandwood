﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationLabourItemService<TViewModel> : OrderLabourItemService<Quotation, QuotationLabourItem, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        
        public QuotationLabourItemService(Guid CompanyID, Guid quotationID, string UserId, ModelMapper<QuotationLabourItem, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }


        public override void OnValuesUpdated(QuotationLabourItem model)
        {
            var quotationService = new QuotationService<TViewModel>(CompanyID, null, null, DbContext);
            quotationService.UpdateValues(model.QuotationID);
        }
    }
}
