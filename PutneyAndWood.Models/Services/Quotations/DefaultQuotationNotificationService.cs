﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class DefaultQuotationNotificationService<TViewModel> : DefaultOrderNotificationService<QuotationSettings, DefaultQuotationNotification, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public DefaultQuotationNotificationService(Guid CompanyID, string UserId, ModelMapper<DefaultQuotationNotification, TViewModel> mapper, DbContext db) :
            base(CompanyID, UserId, mapper, db)
        {

        }
        
    }
}
