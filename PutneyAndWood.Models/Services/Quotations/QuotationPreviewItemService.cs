﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationPreviewItemService<TViewModel> : OrderPreviewItemService<Quotation, QuotationPreviewItem, QuotationTemplate, QuotationAttachment,TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationPreviewItemService(Guid CompanyID, Guid? quotationID, string UserId, ModelMapper<QuotationPreviewItem, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }
        
    }
}
