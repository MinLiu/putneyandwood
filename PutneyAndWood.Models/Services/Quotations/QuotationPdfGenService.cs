﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using HtmlToOpenXml;
using System.Text.RegularExpressions;
using Fruitful.Extensions;

namespace SnapSuite.Models
{
    
    public class QuotationPdfGenService : OrderPdfGenService<Quotation, CustomQuotationField, QuotationViewModel, QuotationSection, QuotationItem, QuotationItemImage, QuotationAttachment, QuotationComment, QuotationNotification, QuotationTemplate, QuotationPreviewItem, QuotationSettings, DefaultQuotationAttachment, DefaultQuotationNotification>
    {
        
        public override string[] ReferencesToInclude { get { return new string[] { "Company", "Client", "ClientContact", "InvoiceAddress", "DeliveryAddress", "Sections", "Items", "Items.Images", "Items.Images.File", "SignedAgreement", "Currency" }; } }

        public QuotationPdfGenService(Guid CompanyID, string UserID, DbContext db) :
            base(CompanyID, UserID, null, db)
        {

        }


       // protected override QuotationTemplate GetFrontTemplate(QuotationPreview preview) { return preview.FrontTemplate; }
        //protected override QuotationTemplate GetBodyTemplate(QuotationPreview preview) { return preview.BodyTemplate; }
        //protected override QuotationTemplate GetBackTemplate(QuotationPreview preview) { return preview.BackTemplate; }


        protected override ICollection<QuotationItem> GetPriceBreaks(QuotationItem item)
        {
            return item.PriceBreaks.OrderBy(p => p.Quantity).Select(p => p.ToQuotationItem()).ToList();
        }

        protected override bool UsePriceBreaks(Quotation model) { return model.UsePriceBreaks; }
        
        protected override QuotationItem GetPriceBreak(QuotationItem item, string innerText, int num)
        {            
            try
            {
                if (num > 1)
                {
                    var price = item.PriceBreaks.OrderBy(x => x.Quantity).ElementAt(num - 2);
                    return new QuotationItem
                    {
                        Quantity = price.Quantity,
                        SetupCost = price.SetupCost,
                        VAT = price.VAT,
                        Discount = price.Discount,
                        Markup = price.Markup,
                        ListPrice = price.ListPrice,
                        Price = price.Price,
                        Cost = price.Cost,
                        Margin = price.Margin,
                        MarginPrice = price.MarginPrice,
                        Total = price.Total,
                    };

                }
                else if (num == 1 || num == 0) { return item; }
                else { return null; }
            }
            catch { return null; }
        }


        public override Dictionary<string, string> CreateDictionary(Quotation model)
        {
            var dic = base.CreateDictionary(model);
            

            dic.Add("{InvoiceName}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Name : (model.Client != null) ? model.Client.Name : "");
            dic.Add("{InvoiceAddress1}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Address1 : (model.Client != null) ? model.Client.Address1 : "");
            dic.Add("{InvoiceAddress2}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Address2 : (model.Client != null) ? model.Client.Address2 : "");
            dic.Add("{InvoiceAddress3}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Address3 : (model.Client != null) ? model.Client.Address3 : "");
            dic.Add("{InvoiceTown}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Town : (model.Client != null) ? model.Client.Town : "");
            dic.Add("{InvoiceCounty}", (model.InvoiceAddress != null) ? model.InvoiceAddress.County : (model.Client != null) ? model.Client.County : "");
            dic.Add("{InvoicePostcode}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Postcode : (model.Client != null) ? model.Client.Postcode : "");
            dic.Add("{InvoiceCountry}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Country : (model.Client != null) ? model.Client.Country : "");
            dic.Add("{InvoiceTelephone}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Telephone : (model.Client != null) ? model.Client.Telephone : "");
            dic.Add("{InvoiceEmail}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Email : (model.Client != null) ? model.Client.Email : "");
            dic.Add("{InvoiceMobile}", (model.InvoiceAddress != null) ? model.InvoiceAddress.Mobile : (model.Client != null) ? model.Client.Mobile : "");

            dic.Add("{DeliveryName}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Name : (model.Client != null) ? model.Client.Name : "");
            dic.Add("{DeliveryAddress1}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Address1 : (model.Client != null) ? model.Client.Address1 : "");
            dic.Add("{DeliveryAddress2}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Address2 : (model.Client != null) ? model.Client.Address2 : "");
            dic.Add("{DeliveryAddress3}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Address3 : (model.Client != null) ? model.Client.Address3 : "");
            dic.Add("{DeliveryTown}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Town : (model.Client != null) ? model.Client.Town : "");
            dic.Add("{DeliveryCounty}", (model.DeliveryAddress != null) ? model.DeliveryAddress.County : (model.Client != null) ? model.Client.County : "");
            dic.Add("{DeliveryPostcode}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Postcode : (model.Client != null) ? model.Client.Postcode : "");
            dic.Add("{DeliveryCountry}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Country : (model.Client != null) ? model.Client.Country : "");
            dic.Add("{DeliveryTelephone}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Telephone : (model.Client != null) ? model.Client.Telephone : "");
            dic.Add("{DeliveryEmail}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Email : (model.Client != null) ? model.Client.Email : "");
            dic.Add("{DeliveryMobile}", (model.DeliveryAddress != null) ? model.DeliveryAddress.Mobile : (model.Client != null) ? model.Client.Mobile : "");

            dic.Add("{ClientContactName}", (model.ClientContact != null) ? model.ClientContact.ToLongLabel() : "");
            dic.Add("{ClientContactTitle}", (model.ClientContact != null) ? model.ClientContact.Title ?? "" : "");
            dic.Add("{ClientContactFirstName}", (model.ClientContact != null) ? model.ClientContact.FirstName ?? "" : "");
            dic.Add("{ClientContactLastName}", (model.ClientContact != null) ? model.ClientContact.LastName ?? "" : "");
            dic.Add("{ClientContactPosition}", (model.ClientContact != null) ? model.ClientContact.Position ?? "" : "");
            dic.Add("{ClientContactTelephone}", (model.ClientContact != null) ? model.ClientContact.Telephone ?? "" : "");
            dic.Add("{ClientContactEmail}", (model.ClientContact != null) ? model.ClientContact.Email ?? "" : "");
            dic.Add("{ClientContactMobile}", (model.ClientContact != null) ? model.ClientContact.Mobile ?? "" : "");

            dic.Add("{ClientName}", (model.Client != null) ? model.Client.Name : "");
            dic.Add("{ClientType}", (model.Client != null) ? (model.Client.Type != null ? model.Client.Type.Name ?? "" : "") : "");
            dic.Add("{ClientReference}", model.ClientReference);
            dic.Add("{PoNumber}", model.PoNumber);
            dic.Add("{DateDelivery}", model.EstimatedDelivery.ToString("dd/MM/yyyy"));
            dic.Add("{DateDeliveryLong}", model.EstimatedDelivery.ToString("dd MMMM yyyy"));
            //dic.Add("{AssociatedQuotation}", (model.Quotation != null) ? model.Quotation.ToShortLabel() : "");


            dic.Add("{AgreementName}", (model.SignedAgreement != null) ? model.SignedAgreement.FullName() : "");
            dic.Add("{AgreementTitle}", (model.SignedAgreement != null) ? model.SignedAgreement.Title : "");
            dic.Add("{AgreementFirstName}", (model.SignedAgreement != null) ? model.SignedAgreement.FirstName : "");
            dic.Add("{AgreementLastName}", (model.SignedAgreement != null) ? model.SignedAgreement.LastName : "");
            dic.Add("{AgreementDateSigned}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("dd/MM/yyyy") : "");
            dic.Add("{AgreementDateSignedLong}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("dd MMMM yyyy") : "");
            dic.Add("{AgreementDateTime}", (model.SignedAgreement != null) ? model.SignedAgreement.DateSigned.ToString("HH:mm") : "");
            dic.Add("{AgreementEmail}", (model.SignedAgreement != null) ? model.SignedAgreement.Email : "");
            //dic.Add("{AgreementSignature}", "");

            dic.Add("{EstimatorName}", (model.Project.AssignedUser != null) ? String.Format("{0} {1}", model.Project.AssignedUser.FirstName, model.Project.AssignedUser.LastName) : "");
            //dic.Add("{EstimatorTelephone}", (model.Project.AssignedUser != null) ? model.Project.AssignedUser.PhoneNumber : "");
            var phoneNumber = model.Project?.AssignedUser?.PhoneNumber ?? "";
            phoneNumber = phoneNumber.Length >= 5? phoneNumber.Insert(5, " ") : phoneNumber;
            dic.Add("{EstimatorTelephone}", phoneNumber);
            dic.Add("{EstimatorEmail}", (model.Project.AssignedUser != null) ? model.Project.AssignedUser.Email : "");

            dic.Add("{ProjectName}", model.Project.Name);
            dic.Add("{ProjectNumber}", model.Project.Number.ToString());
            dic.Add("{ClosingParagraph}", model.ClosingParagraph);
            dic.Add("{Greeting}", model.Greeting ?? "");
            dic.Add("{EstimatorTitle}", model.EstimatorTitle ?? "");

            dic.Add("{DateGenerated}", String.Format("{0} {1} {2}", DateTime.Today.ToString("dddd"), DateTime.Today.Day.Ordinalise(), DateTime.Today.ToString("MMMM yyyy")));
            return dic;
        }


        public override Dictionary<string, string> CreateDictionaryImages(Quotation model)
        {
            var dic = base.CreateDictionaryImages(model);
            dic.Add("{AgreementSignature}", (model.SignedAgreement != null) ? model.SignedAgreement.SignatureImageURL : "");

            return dic;

        }

        public override byte[] GenerateBodyPDF(Quotation model, Dictionary<string, string> dictionary, string templateFilePath)
        {
            if (dictionary == null) dictionary = CreateDictionary(model);

            //Return PDF to PDF if template is PDF format
            if (templateFilePath.ToLower().Contains(".pdf")) return DocumentService.PdfToPdf(templateFilePath, dictionary);

            var imagesDictionary = CreateDictionaryImages(model);

            var productIds = model.Items.Select(s => s.ProductCode).Distinct().ToList();
            List<Product> products = new ProductService<EmptyViewModel>(model.CompanyID, null, null, DbContext).Read().Where(p => productIds.Contains(p.ProductCode)).DistinctBy(p => p.ProductCode).ToList();
            List<CustomField> customItemFields = new CustomProductFieldService<EmptyViewModel>(model.CompanyID, null, null, DbContext).Read().ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();



            byte[] byteArray = null;

            //Load the word documnet template
            using (FileStream stream = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    byteArray = binaryReader.ReadBytes((int)stream.Length);
                }
            }

            //Editing the document is done in memory to prevent the tempalte from being changed
            using (MemoryStream ms = new MemoryStream())
            {
                //Convert the word document into a WordprocessingDocument
                ms.Write(byteArray, 0, (int)byteArray.Length);
                WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(ms, true);
                Body body = wordprocessingDocument.MainDocumentPart.Document.Body;
                List<Table> tables = null;


                //var xml = body.InnerXml; 
                var inner = body.InnerText;

                ////First remove Totals Table if section number is one or less.
                //if (model.Sections.Count() <= 1)
                //{
                //    tables = body.Descendants<Table>().ToList();
                //    for (int x = tables.Count - 1; x >= 0; x--)
                //    {
                //        var innerText = tables[x].InnerText;

                //        if (System.Text.RegularExpressions.Regex.IsMatch(innerText, "{(Cost|Net|VAT|Margin|Discount|Total)")
                //            && !System.Text.RegularExpressions.Regex.IsMatch(innerText, "{(NotDel|Item|Business|Client|Invoice|Delivery|PoNumber|Contact|Notes|Review|Date|Associated)"))
                //        {
                //            tables[x].Remove();
                //        }
                //    }
                //}
                ////tables = null;

                //Get all the paragraphs from the body, headers and footers.
                List<Paragraph> paragraphs = body.Descendants<Paragraph>().ToList();
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in imagesDictionary)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            try
                            {
                                paragraphs[x].RemoveAllChildren<Run>();
                                Drawing pic = OpenXmlHelpers.CreateImageElement(dicItem.Value, wordprocessingDocument);
                                paragraphs[x].AppendChild(new Run(pic));
                            }
                            catch { }
                        }
                    }
                }

                foreach (var header in wordprocessingDocument.MainDocumentPart.HeaderParts)
                {
                    var headerParas = header.Header.Descendants<Paragraph>().ToList();
                    for (int x = headerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in imagesDictionary)
                        {
                            if (headerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    headerParas[x].RemoveAllChildren<Run>();
                                    Drawing pic = OpenXmlHelpers.CreateHeaderImageElement(dicItem.Value, header);
                                    headerParas[x].AppendChild(new Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(headerParas);
                }

                foreach (var footer in wordprocessingDocument.MainDocumentPart.FooterParts)
                {
                    var footerParas = footer.Footer.Descendants<Paragraph>().ToList();
                    for (int x = footerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in imagesDictionary)
                        {
                            if (footerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    footerParas[x].RemoveAllChildren<Run>();
                                    Drawing pic = OpenXmlHelpers.CreateFooterImageElement(dicItem.Value, footer);
                                    footerParas[x].AppendChild(new Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(footerParas);
                }

                /*
                //Second replace paragrph texts        
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    if (paragraphs[x].InnerText.Contains("{BusinessLogo}"))
                    {
                        //Replace logo tag with an image
                        try
                        {
                            paragraphs[x].RemoveAllChildren<Run>();
                            Drawing pic = OpenXmlHelpers.CreateImageElement(model.Company.LogoImage.FilePath, wordprocessingDocument);
                            paragraphs[x].AppendChild(new Run(pic));
                        }
                        catch { }
                    }
                }
                */

                OpenXmlHelpers.ReplaceParagraphTexts(paragraphs, dictionary);

                //var xml2 = body.InnerXml;

                //Third, Start constracting the items.
                tables = body.Descendants<Table>().ToList();
                for (int x = tables.Count - 1; x >= 0; x--)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(tables[x].InnerText, "{(Item|Section|Item|Qualification|BasisOfEstimateItem)"))
                    {
                        string innerText = tables[x].InnerText;
                        bool sectionSpec = innerText.Contains("{SectionSpecification}"); //Check to see if table is a section spec table
                        bool spec = innerText.Contains("{Specification}"); //Check to see if table is a full spec table
                        bool pageBreak = innerText.Contains("{PageBreak}"); //Check added page break after every section
                        bool keeptogether = innerText.Contains("{KeepTogether}"); //Check to keep section tables as whole. (table rows do not spill over to next page.
                        bool keepSectionTotals = innerText.Contains("{KeepSectionTotals}") || model.Sections.Count() > 1; //Check to keep section total row in table if section count is one.
                        bool ignoreKeepTogether = innerText.Contains("{IgnoreKeepTogether}"); //Check to make all section table spill over.
                        bool separatePriceBreaks = System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Item(Qty|VAT|Discount|ListPrice|Price|Cost|Margin|MarginPrice|Total)\\d}");
                        int secNum = model.Sections.Count - 1;

                        innerText = null;

                        foreach (var section in model.Sections.OrderByDescending(s => s.SortPos))
                        {
                            var tableClone = tables[x].CloneNode(true) as Table;
                            //Get the table rows from the template. Go through the loop backwards to prevent change in array errors
                            List<TableRow> rows = tableClone.Descendants<TableRow>().ToList();
                            List<TableRow> rowTemps = new List<TableRow>();
                            List<TableRow> rowTemps_Quolification = new List<TableRow>();
                            List<TableRow> rowTemps_BasisOfEstimate= new List<TableRow>();

                            for (int y = rows.Count - 1; y >= 0; y--)
                            {
                                innerText = rows[y].InnerText;
                                //Get the rows  that have the text "{Item". These table rows will be duplicated.
                                if (innerText.Contains("{Item"))
                                {
                                    rowTemps.Insert(0, rows[y]);
                                }
                                else if (!keepSectionTotals
                                        && System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Section(MarginPer|DiscountPer|Cost|Net|VAT|Margin|Discount|Total)}")
                                        && !System.Text.RegularExpressions.Regex.IsMatch(innerText, "{Section(Name|IncludedInTotal|IncludedInTotalYesNo)}"))
                                {
                                    rows[y].Remove();
                                }

                                if (innerText.Contains("{Qualification"))
                                {
                                    rowTemps_Quolification.Insert(0, rows[y]);
                                }

                                if (innerText.Contains("{BasisOfEstimateItem"))
                                {
                                    rowTemps_BasisOfEstimate.Insert(0, rows[y]);
                                }
                            }

                            bool repeat = false;
                            IEnumerable<QuotationItem> sectionItems;
                            IEnumerable<QuotationQualification> quotationQualifications = model.Qualifications.OrderBy(q => q.SortPos);
                            IEnumerable<QuotationBasisOfEstimate> basisOfEstimates = model.QuotationBasisOfEstimates.OrderBy(q => q.SortPos);

                            if (sectionSpec)
                            {
                                sectionItems = section.Items.Where(s => /*s.ParentItemID == null &&*/ s.IsComment == false && s.HideOnPdf == false).OrderBy(s => s.SortPos).DistinctBy(s => s.ProductCode); //If section spec then only get the unqiue section items by code 
                            }
                            else if (spec)
                            {
                                sectionItems = model.Items.Where(s => /*s.ParentItemID == null &&*/ s.IsComment == false && s.HideOnPdf == false).OrderBy(s => s.SortPos).DistinctBy(s => s.ProductCode); //If full spec then only get the unqiue model items by code
                            }
                            else if (UsePriceBreaks(model) && !separatePriceBreaks)
                            {
                                List<QuotationItem> items = new List<QuotationItem>();

                                foreach (var i in section.Items.Where(s => s.ParentItemID == null && s.HideOnPdf == false).OrderBy(s => s.SortPos))
                                {
                                    items.Add(i);
                                    //items.AddRange(i.PriceBreaks.OrderBy(p => p.Quantity).Select(p => p.ToTLineItem()).ToList());
                                    items.AddRange(GetPriceBreaks(i));
                                }

                                sectionItems = items.AsEnumerable();

                            }
                            else
                            {
                                //sectionItems = section.Items.Where(s => s.ParentItemID == null).OrderBy(s => s.SortPos); //Get the items in the section
                                sectionItems = section.Items.Where(s => s.HideOnPdf == false).OrderBy(s => s.SortPos); //Get the items in the section
                            }

                            for (int z = 0; z < sectionItems.Count(); z++)
                            {
                                //if (z <= 0) repeat = false;
                                //else repeat = sectionItems.ElementAt(z).ProductCode == sectionItems.ElementAt(z - 1).ProductCode;
                                sectionItems.ElementAt(z).CustomFieldValues = sectionItems.ElementAt(z).CustomFieldValues.AddRemoveRelevent(customItemFields);

                                foreach (var row in rowTemps)
                                {
                                    var rowClone = row.CloneNode(true) as TableRow;
                                    var tableRow = ConstractWordTableRow(wordprocessingDocument, tableClone, rowClone, sectionItems.ElementAt(z), repeat, (sectionSpec || spec), model, products);
                                    tableClone.InsertBefore(tableRow, rowTemps.Last());
                                }
                            }
                            for (int z = 0; z < quotationQualifications.Count(); z++)
                            {
                                foreach (var row in rowTemps_Quolification)
                                {
                                    var rowClone = row.CloneNode(true) as TableRow;
                                    var tableRow = ConstractWordTableRow(wordprocessingDocument, rowClone, quotationQualifications.ElementAt(z), repeat);
                                    tableClone.InsertBefore(tableRow, rowTemps_Quolification.Last());
                                }
                            }
                            for (int z = 0; z < basisOfEstimates.Count(); z++)
                            {
                                foreach (var row in rowTemps_BasisOfEstimate)
                                {
                                    var rowClone = row.CloneNode(true) as TableRow;
                                    var tableRow = ConstractWordTableRow(wordprocessingDocument, rowClone, basisOfEstimates.ElementAt(z), repeat);
                                    tableClone.InsertBefore(tableRow, rowTemps_BasisOfEstimate.Last());
                                }
                            }


                            foreach (var row in rowTemps)
                            {
                                row.Remove(); //Remove the template row
                            }
                            foreach (var row in rowTemps_Quolification)
                            {
                                row.Remove();
                            }
                            foreach (var row in rowTemps_BasisOfEstimate)
                            {
                                row.Remove();
                            }


                            var sectionDic = CreateSectionDictionary(section, model.Currency.Symbol);
                            List<Paragraph> sectionParagraphs = tableClone.Descendants<Paragraph>().ToList();
                            OpenXmlHelpers.ReplaceParagraphTexts(sectionParagraphs, sectionDic, false);

                            if (keeptogether || (!ignoreKeepTogether && secNum > 0)) //Only keep pages together if stated or not ignore and not the first section.
                            {
                                foreach (var par in sectionParagraphs)
                                {
                                    ParagraphProperties parProperties = new ParagraphProperties();
                                    if (par.ParagraphProperties != null) parProperties = par.ParagraphProperties;
                                    else { par.Append(parProperties); }

                                    if ((!ignoreKeepTogether && secNum > 0))
                                    {
                                        parProperties.Append(new KeepNext());
                                    }
                                    else
                                    {
                                        parProperties.Append(new KeepLines());
                                        parProperties.Append(new KeepNext());
                                    }

                                }
                            }

                            tableClone = body.InsertAfter(tableClone, tables[x]);

                            //Add page breaks only if true and no the last section
                            if (pageBreak && secNum < model.Sections.Count - 1)
                            {
                                tableClone.InsertAfterSelf(new Paragraph(new Run(new Break() { Type = BreakValues.Page })));
                            }
                            //Add new line at end of table, Space them out a little.
                            else if (secNum < model.Sections.Count - 1 && (!spec || sectionSpec))
                            {
                                Run run = new Run(new Text { Text = "...." });
                                run.RunProperties = new RunProperties { FontSize = new FontSize { Val = "1" } };

                                Paragraph para = new Paragraph(run);
                                para.ParagraphProperties = new ParagraphProperties { SpacingBetweenLines = new SpacingBetweenLines { After = "0", Before = "0" }, ContextualSpacing = new ContextualSpacing { Val = false } };

                                tableClone.InsertAfterSelf(para);
                            }

                            secNum--;

                            if (spec) break; //only produce one table if specification
                        }
                        tables[x].Remove(); //Remove the template table
                    }
                }

                var fontList = wordprocessingDocument.MainDocumentPart.FontTablePart.Fonts.Elements<Font>().Select(f => f.Name);

                //Fourth, save document and convert to PDF.
                wordprocessingDocument.Save();
                //wordprocessingDocument.SaveAs("C:\\Users\\Fruitful\\Documents\\Min\\test2.docx");
                wordprocessingDocument.Close();



                using (MemoryStream exportMs = new MemoryStream())
                {
                    Spire.Doc.Document document = new Spire.Doc.Document();

                    ms.Position = 0;
                    document.LoadFromStream(ms, Spire.Doc.FileFormat.Docx);
                    Spire.Doc.ToPdfParameterList toPdf = new Spire.Doc.ToPdfParameterList();
                    toPdf.PdfConformanceLevel = Spire.Pdf.PdfConformanceLevel.None;
                    var standardfonts = new string[] { "arial", "cambria", "candara", "comic", "consolas", "constantia", "corbel", "courier", "franklin", "gabriola", "georgia", "impact", "lucida", "microsoft", "segoe", "sitka", "tahoma", "times", "trebuchet", "webdings", "wingdings" };

                    foreach (var font in fontList.Where(i => standardfonts.Where(x => i.Value.ToLower().Contains(x)).Count() <= 0))
                    {
                        try { toPdf.EmbeddedFontNameList.Add(font); } catch { }
                    }
                    document.EmbedSystemFonts = true;
                    document.JPEGQuality = 75;
                    document.SaveToStream(exportMs, toPdf);
                    return exportMs.ToArray();
                }

            }
        }

        protected TableRow ConstractWordTableRow(WordprocessingDocument wordDoc, TableRow row, QuotationQualification item, bool repeat)
        {

            var paragraphs = PrepareParagraph(wordDoc, row, repeat);

            foreach (var p in paragraphs)
            {
                ReplaceTableRowParagrph(p, item);
            }

            return row;
        }

        protected TableRow ConstractWordTableRow(WordprocessingDocument wordDoc, TableRow row, QuotationBasisOfEstimate item, bool repeat)
        {

            var paragraphs = PrepareParagraph(wordDoc, row, repeat);

            foreach (var p in paragraphs)
            {
                ReplaceTableRowParagrph(wordDoc.MainDocumentPart, p, item);
            }

            return row;
        }

        protected virtual IEnumerable<Paragraph> PrepareParagraph(WordprocessingDocument wordDoc, TableRow row, bool repeat)
        {
            //Get all the paragrpahs in the table row
            IEnumerable<Paragraph> paragraphs = row.Descendants<Paragraph>();

            //Because a paragraph has multiple runs. make sure all paragraphs has have a single run with a single text element.
            //E.g. <p> <run><text></text></run> <run></run> <run><text></text></run> </p> becomes:
            //     <p> <run><text></text></run> </p>

            foreach (var p in paragraphs)
            {
                List<Run> runs = p.Descendants<Run>().ToList();
                if (runs.Count <= 0)
                {
                    p.Append(new Run(new Text()));
                }
                else
                {
                    if (runs[0].Descendants<Text>().Count() <= 0)
                        runs[0].Append(new Text());

                    var txt = runs[0].Descendants<Text>().First();
                    for (int x = 1; x < runs.Count; x++)
                    {
                        txt.Text += runs[x].Descendants<Text>().DefaultIfEmpty(new Text()).FirstOrDefault().Text;
                        runs[x].Remove();
                    }
                }
            }

            IEnumerable<TableCell> cells = row.Descendants<TableCell>();

            foreach (var c in cells)
            {
                if (c.TableCellProperties == null) c.TableCellProperties = new TableCellProperties();
                var inner = c.InnerText;
                if (inner.Contains("{NoRepeat}") || (System.Text.RegularExpressions.Regex.IsMatch(inner, "{Item(Image|Code|Description|Unit)}") /* && model.UsePriceBreaks*/))
                {
                    if (repeat)
                    {
                        c.TableCellProperties.VerticalMerge = new VerticalMerge() { Val = MergedCellValues.Continue };
                        foreach (Run r in c.Descendants<Run>())
                        {
                            r.Remove();
                        }
                    }
                    else
                    {
                        c.TableCellProperties.VerticalMerge = new VerticalMerge() { Val = MergedCellValues.Restart };
                    }

                    foreach (Text txt in c.Descendants<Text>())
                    {
                        txt.Text = txt.Text.Replace("{NoRepeat}", "");
                    }
                }
            };

            return paragraphs;
        }

        protected virtual void ReplaceTableRowParagrph(Paragraph p, QuotationQualification item)
        {

            if (p.InnerText.Contains("{QualificationDescription}"))
            {
                UpdateTableCellText(p, "{QualificationDescription}", item.Description, item.IsSubHeading, item.IsSubItem);
            }
        }

        protected virtual void ReplaceTableRowParagrph(MainDocumentPart mainPart, Paragraph p, QuotationBasisOfEstimate item)
        {

            if (p.InnerText.Contains("{BasisOfEstimateItem}"))
            {
                //UpdateTableCellText(mainPart, p, "{BasisOfEstimateItem}", item.Description);
                UpdateTableCellText(p, "{BasisOfEstimateItem}", item.Description, item.IsSubHeading, item.IsSubItem);
            }
        }

        protected virtual void UpdateTableCellText(Paragraph p, string tag, string text, bool isSubHeading = false, bool isSubItem = false)
        {
            // TODO: Remove list style based on the settings
            //p.Descendants<ParagraphProperties>().First().RemoveAllChildren();
            //p.ParagraphProperties.Indentation = new Indentation() { Left = "720" };
            var run = p.Descendants<Run>().First();
            var txt = run.Descendants<Text>().First();
            try
            {
                if (isSubHeading || isSubItem)
                {
                    p.Descendants<ParagraphProperties>().First().RemoveAllChildren();
                    p.ParagraphProperties.Indentation = new Indentation() { Left = "495" };
                }
                if (isSubItem)
                {
                    p.Descendants<ParagraphProperties>().First().RemoveAllChildren();
                    p.ParagraphProperties.Indentation = new Indentation() { Left = "1000" };
                }
                if (isSubHeading)
                {
                    run.RunProperties.Bold = new Bold();
                    p.ParagraphProperties.SpacingBetweenLines = new SpacingBetweenLines() { Before = "80" };
                }
                string replacedText = txt.Text.Replace(tag, text ?? "");
                if (!string.IsNullOrEmpty(text) && text.Contains(string.Format("\n")))
                {
                    var split = text.Split(new string[] { string.Format("\n") }, StringSplitOptions.None);
                    for (int y = 0; y < split.Length; y++)
                    {
                        Text clone = txt.CloneNode(true) as Text;
                        clone.Text = split[y];
                        run.AppendChild(clone);
                        if (y < split.Length - 1)
                            run.AppendChild(new Break());
                    }
                    txt.Remove();
                }
                else
                {
                    txt.Text = replacedText;
                }

            }
            catch { }
        }

        protected virtual void UpdateTableCellText(MainDocumentPart mainPart, Paragraph p, string tag, string text)
        {
            ReplaceHtml(mainPart, new List<Paragraph>() { p }, text, tag);
        }

        protected virtual void ReplaceHtml(MainDocumentPart mainPart, List<Paragraph> paragraphs, string note, string tag)
        {
            note = CorrectNotes(note ?? "");
            var htmlConverter = new HtmlConverter(mainPart);
            var noteParagraphs = htmlConverter.Parse(note).ToList();

            for (int x = paragraphs.Count - 1; x >= 0; x--)
            {
                var paragraph = paragraphs[x];
                if (paragraph.InnerText.Contains(tag))
                {
                    for (int i = noteParagraphs.Count - 1; i >= 0; --i)
                    {
                        var clonedNode = noteParagraphs[i].CloneNode(true);
                        paragraph.InsertAfterSelf(clonedNode);
                    }

                    OpenXmlHelpers.InsertParagraphText2(paragraph, tag, "");
                    //if (paragraphs[x].InnerText == tag) //If the whole paragraph is just "{Notes}" then remove it.
                    //{
                    //    try { paragraphs[x].Remove(); }
                    //    catch { OpenXmlHelpers.InsertParagraphText2(paragraphs[x], tag, ""); }
                    //}
                    //else //But if the notes in embedded with other text then just blank it out.
                    //{
                    //    OpenXmlHelpers.InsertParagraphText2(paragraphs[x], tag, "");
                    //}
                }
            }
        }

        protected string CorrectNotes(string note)
        {
            // replace floating number with integer in width
            string pattern = @"width:([0-9]*[.])?[0-9]+%";

            var dict = new Dictionary<string, string>();
            foreach (Match m in Regex.Matches(note, pattern))
            {
                try
                {
                    // width:33.333333333336%
                    var widthValue = m.Value.Split(':')[1]; // 33.333333336%
                    var floatNum = float.Parse(widthValue.Split('%')[0]);
                    var newWidth = String.Format("width:{0}%", floatNum.ToString("0.0"));
                    dict[m.Value] = newWidth;
                }
                catch { }
            }

            foreach (var keyValue in dict)
            {
                note = note.Replace(keyValue.Key, keyValue.Value);
            }

            return note;
        }


        //public virtual byte[] GenerateSamplePreview(string filePath)
        //{
        //    var section1 = new QuotationSection { Name = "Section 1", SortPos = 0, IncludeInTotal = true };
        //    section1.Items = new List<QuotationItem>() {
        //            new QuotationItem { ProductCode = "CODE1", Description = "Job Item 1", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE2", Description = "Job Item 2", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE3", Description = "Job Item 3", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE4", Description = "Job Item 4", Unit = "Unit", ImageURL = "/Templates/All/box.png" }
        //        };
        //    var section2 = new QuotationSection { Name = "Section 2", SortPos = 1, IncludeInTotal = true };
        //    section2.Items = new List<QuotationItem>() {
        //            new QuotationItem { ProductCode = "CODE1", Description = "Job Item 1", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE2", Description = "Job Item 2", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE3", Description = "Job Item 3", Unit = "Unit", ImageURL = "/Templates/All/box.png" },
        //            new QuotationItem { ProductCode = "CODE4", Description = "Job Item 4", Unit = "Unit", ImageURL = "/Templates/All/box.png" }
        //        };

        //    var client = new Client
        //    {
        //        Name = "Client Name",
        //        Address1 = "Address Line 1",
        //        Address2 = "Address Line 2",
        //        Town = "Town",
        //        Postcode = "POSTCODE",
        //        Telephone = "012314235654",
        //        Email = "email@email.com",
        //    };

        //    var order = new Quotation
        //    {
        //        Company = new CompanyService<EmptyViewModel>(null, null, DbContext).FindByID(CompanyID),
        //        Client = client,
        //        Sections = new List<QuotationSection>() { section1, section2 },
        //        Items = new List<QuotationItem>() { },
        //        Currency = new Currency { Symbol = "£" },
        //    };


        //    section1.Quotation = order;
        //    section2.Quotation = order;


        //    var extension = Path.GetExtension(filePath);
        //    var dictionary = CreateDictionary(order);

        //    if (extension == ".pdf")
        //        return DocumentService.PdfToPdf(filePath, dictionary);
        //    else
        //        return GenerateBodyPDF(order, dictionary, filePath);

        //}

    }
}
