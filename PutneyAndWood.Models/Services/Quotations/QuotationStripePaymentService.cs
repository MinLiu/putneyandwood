﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationStripePaymentService<TViewModel> : OrderStripePaymentService<Quotation, QuotationStripePayment, TViewModel>
        where TViewModel : class, new()
    {

        public override string LoggingName() { return "Quotation Stripe Payment"; }
        public override string LoggingNamePlural() { return "Quotation Stripe Payments"; }

        public QuotationStripePaymentService(Guid CompanyID, string UserId, ModelMapper<QuotationStripePayment, TViewModel> mapper, DbContext db) :
            base(CompanyID, UserId, mapper, db) 
        {

        }
        
    }
}
