﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{

    public class QuotationItemService<TViewModel> : OrderLineItemService<Quotation, QuotationSection, QuotationItem, QuotationItemImage, QuotationAttachment, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        

        public QuotationItemService(Guid CompanyID, Guid quotationID, string UserId, OrderItemModelMapper<QuotationItem, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {
            
        }

        public override bool UsePriceBreaks(Quotation order)
        {
            return order.UsePriceBreaks;
        }


        public override void OnValuesUpdated(QuotationItem model)
        {
            var secService = new QuotationSectionService<EmptyViewModel>(CompanyID, null, null, null, DbContext);
            secService.UpdateValues(model.SectionID, true);
        }

        public override void UpdateOldSection(Guid oldSection)
        {
            var sectionService = new QuotationSectionService<TViewModel>(CompanyID, null, null, null, DbContext);
            sectionService.UpdateValues(oldSection, false);
        }

        public DataFileOutput Export(Guid quotationID, bool exportAll)
        {
            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");
            
            var quotation = new GenericService<Quotation>(DbContext).Read().Where(i => i.ID == quotationID).First();

            if (exportAll)
            {
                var list = Read(quotationID).Where(i => i.IsComment == false)
                                            .OrderBy(i => i.Section.SortPos)
                                            .ThenBy(i => i.SortPos)
                                            .AsNoTracking()
                                            .ToList();
                var customFields = new CustomProductFieldService<EmptyViewModel>(CompanyID, null, null, DbContext).Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList();

                int x = 0;
                int rowNumber = 1;

                //Populate the sheet with values from the grid data
                foreach (var item in list)
                {
                    x = 0;

                    //Create a new row
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(x).SetCellValue(item.Section.Name); x++;
                    row.CreateCell(x).SetCellValue(item.ProductCode); x++;
                    row.CreateCell(x).SetCellValue(item.Description); x++;
                    row.CreateCell(x).SetCellValue(item.Category); x++;
                    //row.CreateCell(x).SetCellValue(item.VendorCode); x++;
                    //row.CreateCell(x).SetCellValue(item.Manufacturer); x++;

                    item.CustomFieldValues = item.CustomFieldValues.AddRemoveRelevent(customFields);
                    foreach (var field in item.CustomFieldValues) { row.CreateCell(x).SetCellValue(field.Value); x++; }

                    row.CreateCell(x).SetCellValue(item.AccountCode); x++;
                    row.CreateCell(x).SetCellValue(item.Unit); x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.SetupCost);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.VAT);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.ListCost);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.Cost);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.ListPrice);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;

                    if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    {
                        row.CreateCell(x).SetCellValue((double)item.Price);
                        row.GetCell(x).CellStyle = numberStyle;
                    }
                    x++;


                    row.CreateCell(x).SetCellValue((double)((item.ParentItem != null) ? item.Quantity * item.ParentItem.Quantity : item.Quantity));
                    row.GetCell(x).CellStyle = numberStyle;
                    x++;


                    //if ((!item.IsKit && item.ParentItem == null) || (item.IsKit && item.UseKitPrice) || (item.ParentItem != null && !item.ParentItem.UseKitPrice))
                    //{
                    //    row.CreateCell(x).SetCellValue((double)((item.ParentItem != null) ? item.Total * item.ParentItem.Quantity : item.Total));
                    //    row.GetCell(x).CellStyle = numberStyle;
                    //}
                    //x++;
                }


                x = 0;
                //Set the column names in the header row
                //Set headers after to make auto resizing easier
                headerRow.CreateCell(x).SetCellValue("Section"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Product Code"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Description"); x++;
                headerRow.CreateCell(x).SetCellValue("Category"); sheet.AutoSizeColumn(x); x++;
                //headerRow.CreateCell(x).SetCellValue("Vendor Code"); sheet.AutoSizeColumn(x); x++;
                //headerRow.CreateCell(x).SetCellValue("Manufacturer"); sheet.AutoSizeColumn(x); x++;
                foreach (var header in customFields) { headerRow.CreateCell(x).SetCellValue(header.Name); x++; }
                headerRow.CreateCell(x).SetCellValue("Account Code"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Unit"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Setup Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("VAT"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("List Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Agreed Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("List Price"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Sales Price"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Qty"); sheet.AutoSizeColumn(x); x++;
                //headerRow.CreateCell(x).SetCellValue("Net Total"); sheet.AutoSizeColumn(x); x++;
            }
            else //(type == "ItemsTotalled")
            {
                var list = Read(quotationID).Where(i => i.IsComment == false)
                                            .Where(i => (!i.IsKit && i.ParentItem == null) || (i.IsKit && i.UseKitPrice) || (i.ParentItem != null && !i.ParentItem.UseKitPrice))
                                            .GroupBy(i => new { i.ProductCode, i.Description, i.Category, i.AccountCode, i.Unit, i.VAT, i.Price, i.SetupCost, i.Cost , i.ListCost, i.ListPrice })
                                            .OrderBy(g => g.Key.ProductCode)
                                            .AsNoTracking()
                                            .ToList();
                int x = 0;

                int rowNumber = 1;

                //Populate the sheet with values from the grid data
                foreach (var item in list)
                {
                    x = 0;

                    //Create a new row
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(x).SetCellValue(item.Key.ProductCode); x++;
                    row.CreateCell(x).SetCellValue(item.Key.Description); x++;
                    row.CreateCell(x).SetCellValue(item.Key.Category); x++;
                    //row.CreateCell(x).SetCellValue(item.Key.VendorCode); x++;
                    //row.CreateCell(x).SetCellValue(item.Key.Manufacturer); x++;
                    row.CreateCell(x).SetCellValue(item.Key.AccountCode); x++;
                    row.CreateCell(x).SetCellValue(item.Key.Unit); x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.SetupCost); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.VAT); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.ListCost); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.Cost); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.ListPrice); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Key.Price); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Sum(i => (i.ParentItem != null) ? i.ParentItem.Quantity * i.Quantity : i.Quantity)); row.GetCell(x).CellStyle = numberStyle; x++;
                    row.CreateCell(x).SetCellValue((double)item.Sum(i => (i.ParentItem != null) ? i.ParentItem.Quantity * i.Total : i.Total)); row.GetCell(x).CellStyle = numberStyle; x++;
                }


                x = 0;
                //Set the column names in the header row
                //Set headers after to make auto resizing easier
                headerRow.CreateCell(x).SetCellValue("Product Code"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Description"); x++;
                headerRow.CreateCell(x).SetCellValue("Category"); sheet.AutoSizeColumn(x); x++;
                //headerRow.CreateCell(x).SetCellValue("Vendor Code"); sheet.AutoSizeColumn(x); x++;
                //headerRow.CreateCell(x).SetCellValue("Manufacturer"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Account Code"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Unit"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Setup Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("VAT"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("List Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Agreed Cost"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("List Price"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Sales Price"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Qty"); sheet.AutoSizeColumn(x); x++;
                headerRow.CreateCell(x).SetCellValue("Net Total"); sheet.AutoSizeColumn(x); x++;

            }

            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("Quotation Items {0}{1}.xls", quotation.Reference, quotation.Number)  //Suggested file name
            };
        }


        public override QuotationItem Destroy(QuotationItem entity)
        {
            var priceService = new QuotationItemPriceBreakService<EmptyViewModel>(CompanyID, entity.QuotationID, null, null, DbContext);
            priceService.Destroy(priceService.Read().Where(i => i.QuotationItemID == entity.ID).ToList());
            return base.Destroy(entity);
        }


    }
    

}
