﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationItemPriceBreakService<TViewModel> : OrderLineItemPriceBreakService<Quotation, QuotationItem, QuotationItemImage, QuotationSection, QuotationItemPriceBreak, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationItemPriceBreakService(Guid CompanyID, Guid quotationID, string UserId, ModelMapper<QuotationItemPriceBreak, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }
        
        
        public override void OnValuesUpdated(QuotationItemPriceBreak model)
        {
            var quotationService = new QuotationItemService<QuotationItemViewModel>(CompanyID, model.QuotationItemID, null, null, DbContext);
            quotationService.UpdateValues(model.QuotationItemID, true, true);
        }
        
    }
}
