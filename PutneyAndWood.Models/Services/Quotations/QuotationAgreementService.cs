﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Net.Mail;
using System.IO;

namespace SnapSuite.Models
{
    
    public class QuotationAgreementService<TViewModel> : OrderAgreementService<Quotation, QuotationSignedAgreement, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Quotation" }; } }

        public QuotationAgreementService(Guid CompanyID, string currentUserID, ModelMapper<QuotationSignedAgreement, TViewModel> mapper, DbContext db) :
            base(CompanyID, currentUserID, mapper, db)
        {

        }


        public override byte[] GetOrderPDF(Guid id, ref string filename)
        {
            var quotePdfGen = new QuotationPdfGenService(CompanyID, null, DbContext);
            return quotePdfGen.GeneratePDF(id, ref filename);
        }

        public override QuotationSignedAgreement AcceptAgreement(Quotation quote, string title, string firstName, string lastName, string email, string deviceInfo, string ipAddress, string city, string country, string latitude, string longitude, string filename, string extension, byte[] uploadSignature, bool handDrawnSignature, string PoNumber = null)
        {
            var quoteService = new QuotationService<EmptyViewModel>(CompanyID, null, null, DbContext);

            if (quote.IsClientVersion)
            {
                if (quote.NeedsCheckedClientSubmission) throw new Exception("You can not accept client custom quotations that need review.");

                quote.IsClientVersion = false;
                var query = quoteService.Read(quote.CompanyID).Where(i => i.Reference == quote.Reference && i.Number == quote.Number).OrderByDescending(q => q.Version).Select(i => i.Version).ToList();
                quote.Version = (query.Count > 0) ? query.First() + 1 : 1;

                //Delete zeroed items that are not comments or required items.
                var itemService = new QuotationItemService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
                var delItems = itemService.Read().Where(i => i.QuotationID == quote.ID && ((i.IsComment == false && (i.Quantity <= 0 || i.ParentItem.Quantity <= 0) && i.IsRequired == false) || i.Section.IncludeInTotal == false)).ToArray();
                itemService.Destroy(delItems);

                var sectionService = new QuotationSectionService<EmptyViewModel>(CompanyID, Guid.Empty, null, null, DbContext);
                var delSections = sectionService.Read().Where(i => i.QuotationID == quote.ID && i.IncludeInTotal == false && i.IsRequired == false).ToArray();
                sectionService.Destroy(delSections);

                try
                {
                    //Set the origianl to deprecated
                    var original = quoteService.FindByID(quote.MainVersionID.Value);
                    original.StatusID = QuotationStatusValues.DEPRECATED;
                    quoteService.Update(original, new string[] { "StatusID" });
                }
                catch { }
            }

            return base.AcceptAgreement(quote, title, firstName, lastName, email, deviceInfo, ipAddress, city, country, latitude, longitude, filename, extension, uploadSignature, handDrawnSignature, PoNumber);
        }

        public override void OnAgreementCreated(QuotationSignedAgreement model, string PoNumber)
        {
            var quoteService = new QuotationService<EmptyViewModel>(CompanyID, null, null, DbContext);
            var quote = quoteService.Read(Guid.Empty).Where(i => i.ID == model.QuotationID).First();
            
            //Update quote based on the option selected by user.           
            quote.StatusID = QuotationStatusValues.ACCEPTED;
            if (!string.IsNullOrEmpty(PoNumber)) quote.PoNumber = PoNumber;
            
            quoteService.Update(quote, new string[] { "StatusID", "PoNumber", "LastUpdated" });            
        }


        public override void SendAgreement(QuotationSignedAgreement model)
        {
            var quoteService = new QuotationService<EmptyViewModel>(CompanyID, null, null, DbContext);
            var quote = quoteService.Read(Guid.Empty).Where(i => i.ID == model.QuotationID).First();

            var title = "";
            var message = "";
            
            var name = string.Join(", ", new string[] { model.Title, model.FirstName, model.LastName }.Where(i => !string.IsNullOrEmpty(i)));
            title = string.Format("Quotation {0}: Accepted by {1}", quote.ToShortLabel(), name);
            message = string.Format("Quotation {0} was accepted by {1}.", quote.ToShortLabel(), name);
            
            //Add internal comment
            new QuotationCommentService<EmptyViewModel>(CompanyID, quote.ID, CurrentUserID, null, DbContext).Create(new QuotationComment
            {
                QuotationID = quote.ID,
                Timestamp = DateTime.Now,
                Email = name,
                Message = message
            });


            //Send Notifcation message
            message += string.Format("\n\n\n{0}/Quotations/Edit/{1}", FileService.WebURL, quote.ID);
            var settings = new QuotationSettingsService<EmptyViewModel>(null, null, DbContext).FindByID(CompanyID);
            var mailMessage = EmailMessageService.CreateCompanyMessage("", "", title, message, quote.Company.LogoImageURL, null);
                        
            mailMessage.Bcc.Add(model.Email);
            foreach (var address in quote.Notifications)
            {
                mailMessage.Bcc.Add(address.Email);
            }
            
            if (settings != null && !string.IsNullOrEmpty(settings.DefaultFromEmail))
                try { mailMessage.From = new MailAddress(settings.DefaultFromEmail, settings.DefaultFromEmail); } catch { }
            else
                try { mailMessage.From = new MailAddress(quote.Company.Email, quote.Company.Email); } catch { }

            if (mailMessage.From == null || string.IsNullOrEmpty(mailMessage.From.Address))
                mailMessage.From = new MailAddress("noreply@quikflw.com");

            //Try attach receipt
            try
            {
                Attachment pdf = new Attachment(new FileStream(FileService.ToPhysicalPath(model.PdfAgreement.FilePath), FileMode.Open, FileAccess.Read, FileShare.Read), model.PdfAgreementFilename);
                mailMessage.Attachments.Add(pdf);
            }
            catch (Exception ex) { }

            //Try attach deliveryNote (generate deliveryNote after to add signature stuff)
            try
            {
                Attachment pdf2 = new Attachment(new FileStream(FileService.ToPhysicalPath(model.PdfApproved.FilePath), FileMode.Open, FileAccess.Read, FileShare.Read), model.PdfAgreementFilename);
                mailMessage.Attachments.Add(pdf2);
            }
            catch (Exception ex) { }

            //EmailMessageService.SendEmailWithSendGrid(mailMessage);
            new EmailMessageService(CompanyID, DbContext).SendEmail(model.CompanyID, mailMessage, true);
        }

    }


}
