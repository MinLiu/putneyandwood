﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationSettingsService<TViewModel> : OrderSettingsService<QuotationSettings, TViewModel, DefaultQuotationAttachment, DefaultQuotationNotification>
        where TViewModel : class, new()
    {

        public override string LoggingName() { return "Quotation settings"; }
        public override string LoggingNamePlural() { return "Quotation settings"; }

        public QuotationSettingsService(string UserId, ModelMapper<QuotationSettings, TViewModel> mapper, DbContext db) :
            base(UserId, mapper, db)
        {

        }

        
    }
}
