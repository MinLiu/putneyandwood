﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationNotificationService<TViewModel> : OrderNotificationService<Quotation, QuotationNotification, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationNotificationService(Guid CompanyID, Guid quotationID, string UserId, ModelMapper<QuotationNotification, TViewModel> mapper, DbContext db) :
            base(CompanyID, quotationID, UserId, mapper, db)
        {

        }
        
    }
}
