﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Models
{
    
    public class QuotationEventService<TViewModel> : ModelViewService<QuotationEvent, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public override string[] ReferencesToInclude { get { return new string[] { "Client", "ClientContact", "Status", "AssignedUser", "SignedAgreement", "Currency", "StripePayment",  }; } }

        public QuotationEventService(string currentUserID, ModelMapper<QuotationEvent, TViewModel> mapper, DbContext db) :
            base(currentUserID, mapper, db)
        {

        }
    }
}
