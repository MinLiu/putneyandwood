﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;

namespace SnapSuite.Models
{

    public class QuotationItemImageService<TViewModel> : ItemImageService<QuotationItem, QuotationItemImage, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public QuotationItemImageService(Guid CompanyID, Guid? ParentID, string UserId, ModelMapper<QuotationItemImage, TViewModel> mapper, DbContext db) :
            base(CompanyID, ParentID, UserId, mapper, db)
        {

        }
        
    }
    
}
