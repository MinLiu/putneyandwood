﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class QuotationTemplateService<TViewModel> : OrderTemplateService<QuotationTemplate, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public QuotationTemplateService(Guid CompanyID, string UserId, ModelMapper<QuotationTemplate, TViewModel> mapper, DbContext db) :
            base(CompanyID, UserId, mapper, db)
        {

        }

        public IQueryable<QuotationTemplate> Search(TemplateType templateType)
        {
            return Read().Where(x => x.Type == templateType);
        }

    }
}
