﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class DefaultQuotationAttachmentService<TViewModel> : DefaultOrderAttachmentService<QuotationSettings, DefaultQuotationAttachment, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {
        
        public DefaultQuotationAttachmentService(Guid CompanyID, string UserId, ModelMapper<DefaultQuotationAttachment, TViewModel> mapper, DbContext db) :
            base(CompanyID, UserId, mapper, db)
        {

        }
        
    }
}
