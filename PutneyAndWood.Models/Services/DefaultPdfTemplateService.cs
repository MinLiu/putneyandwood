﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{

    public class DefaultPdfTemplateService<TViewModel> : EntityService<DefaultPdfTemplate, TViewModel>
        where TViewModel : class, IEntityViewModel, new()
    {

        public DefaultPdfTemplateService(string UserId, ModelMapper<DefaultPdfTemplate, TViewModel> mapper, DbContext db)
            : base(UserId, mapper, db)
        {

        }

        public IQueryable<DefaultPdfTemplate> Search(int moduleTypeID, TemplateType templateType)
        {
            return Read().Where(x => x.ModuleTypeID == moduleTypeID)
                         .Where(x => x.Type == templateType);

        }
    }


}
