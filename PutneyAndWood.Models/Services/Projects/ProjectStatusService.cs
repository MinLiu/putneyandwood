﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ProjectStatusService: GenericService<ProjectStatus>
    {
        public ProjectStatusService(DbContext dbContext): base(dbContext)
        {
        }

        public override IQueryable<ProjectStatus> Read()
        {
            return base.Read().OrderBy(x => x.SortPos);
        }

    }


}
