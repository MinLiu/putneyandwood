﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Models
{

    public class ProjectService: GenericService<Project>
    {
        public Guid CompanyID { get; set; }
        public string CurrentUserID { get; set; }

        public ProjectService(Guid CompanyID, string CurrentUserID, SnapDbContext db): base(db)
        {
            this.CompanyID = CompanyID;
            this.CurrentUserID = CurrentUserID;
        }

        public Project FindByID(Guid ID)
        {
            var model = Read().Where(x => x.ID == ID).FirstOrDefault();
            return model;
        }

        public override IQueryable<Project> Read()
        {
            return base.Read().Where(x => x.Deleted == false).OrderByDescending(x => x.Created);
        }

        public IQueryable<Project> ReadContactAssociated(Guid contactID)
        {
            var query = Read().Where(x => x.ClientContact1ID == contactID
                                       || x.ClientContact2ID == contactID
                                       || x.ClientContact3ID == contactID
                                       || x.ClientContact4ID == contactID);
            return query;
        }

        public IQueryable<Project> Search(string filterText, DateType dateType, ICollection<int> statuses, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromDate, toDate);

            var query = Read().Where(x => x.CompanyID == CompanyID)
                                    .Where(x => x.Created >= fmDate && x.Created <= tDate)
                                    .Where(x => assignedUserId == null || assignedUserId == "" || x.AssignedUserID == assignedUserId)
                                    .Where(x => statuses.Contains(x.StatusID));

            if (!string.IsNullOrWhiteSpace(filterText))
            {
                filterText = filterText.ToLower();
                query = query.Where(x => x.Name.ToLower().Contains(filterText)
                                          || x.Description.ToLower().Contains(filterText)
                                          || x.Client1.Name.ToLower().Contains(filterText)
                                          || x.Client2.Name.ToLower().Contains(filterText)
                                          || x.Client3.Name.ToLower().Contains(filterText)
                                          || x.Client4.Name.ToLower().Contains(filterText)
                                          || (x.ClientContact1.FirstName + " " + x.ClientContact1.LastName).ToLower().Contains(filterText)
                                          || (x.ClientContact2.FirstName + " " + x.ClientContact2.LastName).ToLower().Contains(filterText)
                                          || (x.ClientContact3.FirstName + " " + x.ClientContact3.LastName).ToLower().Contains(filterText)
                                          || (x.ClientContact4.FirstName + " " + x.ClientContact4.LastName).ToLower().Contains(filterText)
                                          || x.Note.ToLower().Contains(filterText)
                                          || x.Consultant1.Name.ToLower().Contains(filterText)
                                          || x.Consultant2.Name.ToLower().Contains(filterText)
                                          || x.Consultant3.Name.ToLower().Contains(filterText)
                                          || x.Consultant4.Name.ToLower().Contains(filterText)
                                          || (x.ConsultantContact1.FirstName + " " + x.ConsultantContact1.LastName).ToLower().Contains(filterText)
                                          || (x.ConsultantContact2.FirstName + " " + x.ConsultantContact2.LastName).ToLower().Contains(filterText)
                                          || (x.ConsultantContact3.FirstName + " " + x.ConsultantContact3.LastName).ToLower().Contains(filterText)
                                          || (x.ConsultantContact4.FirstName + " " + x.ConsultantContact4.LastName).ToLower().Contains(filterText));
            }

            return query;
        }

        public Project New(string userID)
        {
            var lastProject = Search("", DateType.AllTime, ProjectStatusValues.All).OrderByDescending(x => x.Number).FirstOrDefault();
            var lastNumber = lastProject != null ? lastProject.Number : 0;
            var project = Create(new Project()
            {
                Number = lastNumber + 1,
                CreatedBy = userID,
                Created = DateTime.Now,
                LastUpdated = DateTime.Now,
                CompanyID = CompanyID,
                StatusID = ProjectStatusValues.PreTender
            });
            return project;
        }

        public Project Edit(string ID)
        {
            var project = FindByID(Guid.Parse(ID));
            return project;
        }

        public Project Update(ProjectViewModel viewModel)
        {
            var model = FindByID(Guid.Parse(viewModel.ID));
            new ProjectMapper().MapToModel(viewModel, model);
            return Update(model);
        }

        public Project Update(Project model)
        {
            model.LastUpdated = DateTime.Now;
            DbContext.SaveChanges();
            return model;
        }

        public DataFileOutput Export(string filterText, DateType dateType, ICollection<int> statuses, string fromDate = null, string toDate = null, string assignedUserId = null, bool deleted = false)
        {
            var projects = Search(filterText, dateType, statuses, fromDate, toDate, assignedUserId).OrderByDescending(q => q.Created);
            var list = projects.AsNoTracking().ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            int x = 0;
            int rowNumber = 1;
            foreach (var project in list)
            {
                var quotationList = project.Quotations
                                       .Where(i => i.Deleted == false)
                                       .OrderBy(i => i.Number)
                                       .ToList();

                //Populate the sheet with values from the grid data
                foreach (var quotation in quotationList)
                {
                    x = 0;
                    var row = sheet.CreateRow(rowNumber++);
                    //Set values for the cells
                    row.CreateCell(x).SetCellValue(project.Number); x++;
                    row.CreateCell(x).SetCellValue(project.Name); x++;
                    row.CreateCell(x).SetCellValue(quotation.Number); x++;
                    row.CreateCell(x).SetCellValue(quotation.Version); x++;
                    row.CreateCell(x).SetCellValue(quotation.Client != null ? quotation.Client.Name : ""); x++;
                    row.CreateCell(x).SetCellValue(quotation.ClientContact != null ? quotation.ClientContact.ToShortLabel() : ""); x++;

                    row.CreateCell(x).SetCellValue(project.StoneworkStartDate != null ? project.StoneworkStartDate.Value.ToString("dd/MM/yyyy") : "");
                    row.GetCell(x).CellStyle = dateTimeStyle;
                    x++;

                    row.CreateCell(x).SetCellValue((double)project.Value);
                    row.GetCell(x).CellStyle = numberStyle;
                    x++;

                    row.CreateCell(x).SetCellValue(project.AssignedUser != null ? project.AssignedUser.ToLongLabel() : ""); x++;

                    var nextEvent = quotation.Events.Where(e => e.Timestamp >= DateTime.Now).FirstOrDefault();
                    if (nextEvent != null)
                    {
                        row.CreateCell(x).SetCellValue(nextEvent.Timestamp.ToString("dd/MM/yyyy")); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                        row.CreateCell(x).SetCellValue(nextEvent.Message); x++;
                    }
                }
            }

            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("Project No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Product name"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Quote No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Version No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Contractor"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("MC Contact"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Stonework start date"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Project value"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Estimator"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Date Next Event"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Next Event"); sheet.AutoSizeColumn(x); x++;

            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("Projects Export {0}.xls", DateTime.Today.ToString("dd-MM-yyyy"))  //Suggested file name
            };
        }
        public DataFileOutput Export(Guid projectID)
        {
            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            var project = FindByID(projectID);

            var quotationList = project.Quotations
                                       .Where(i => i.Deleted == false)
                                       .OrderBy(i => i.Number)
                                       .ToList();

            int x = 0;
            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var quotation in quotationList)
            {
                x = 0;
                var row = sheet.CreateRow(rowNumber++);
                //Set values for the cells
                row.CreateCell(x).SetCellValue(project.Number); x++;
                row.CreateCell(x).SetCellValue(project.Name); x++;
                row.CreateCell(x).SetCellValue(quotation.Number); x++;
                row.CreateCell(x).SetCellValue(quotation.Version); x++;
                row.CreateCell(x).SetCellValue(quotation.Client != null ? quotation.Client.Name : ""); x++;
                row.CreateCell(x).SetCellValue(quotation.ClientContact != null ? quotation.ClientContact.ToShortLabel() : ""); x++;

                row.CreateCell(x).SetCellValue(project.StoneworkStartDate != null ? project.StoneworkStartDate.Value.ToString("dd/MM/yyyy") : "");
                row.GetCell(x).CellStyle = dateTimeStyle;
                x++;

                row.CreateCell(x).SetCellValue((double)project.Value);
                row.GetCell(x).CellStyle = numberStyle;
                x++;

                row.CreateCell(x).SetCellValue(project.AssignedUser != null ? project.AssignedUser.ToLongLabel() : ""); x++;

                var nextEvent = quotation.Events.Where(e => e.Timestamp >= DateTime.Now).FirstOrDefault();
                if (nextEvent != null)
                {
                    row.CreateCell(x).SetCellValue(nextEvent.Timestamp.ToString("dd/MM/yyyy")); row.GetCell(x).CellStyle = dateTimeStyle; x++;
                    row.CreateCell(x).SetCellValue(nextEvent.Message); x++;
                }
            }

            x = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(x).SetCellValue("Project No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Product name"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Quote No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Version No"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Contractor"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("MC Contact"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Stonework start date"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Project value"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Estimator"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Date Next Event"); sheet.AutoSizeColumn(x); x++;
            headerRow.CreateCell(x).SetCellValue("Next Event"); sheet.AutoSizeColumn(x); x++;

            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return new DataFileOutput
            {
                DataArray = output.ToArray(), //The binary data of the XLS file
                MimeType = "application/vnd.ms-excel", //MIME type of Excel files
                Filename = string.Format("Project {0}-{1}.xls", project.Number, project.Name)  //Suggested file name
            };
        }

        public Project ChangeStatus(Guid projectID, int statusID)
        {
            var project = FindByID(projectID);
            project.StatusID = statusID;
            project.LastUpdated = DateTime.Now;
            DbContext.SaveChanges();
            return project;
        }

        public ProjectViewModel Delete(ProjectViewModel viewModel)
        {
            var project = FindByID(Guid.Parse(viewModel.ID));
            project.Deleted = true;
            project.LastUpdated = DateTime.Now;
            DbContext.SaveChanges();

            return viewModel;
        }
    }
}
