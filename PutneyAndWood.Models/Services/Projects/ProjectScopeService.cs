﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ProjectScopeService : GenericService<ProjectScope>
    {
        public ProjectScopeService(SnapDbContext dbContext): base(dbContext)
        {
        }
    }
}
