﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class ScopeService: GenericService<Scope>
    {
        public ScopeService(SnapDbContext dbContext): base(dbContext)
        {
        }

        public override IQueryable<Scope> Read()
        {
            return base.Read().Where(x => x.Deleted == false).OrderBy(x => x.ID);
        }

        public virtual Scope Delete(ScopeGridViewModel entity)
        {
            var model = Find(int.Parse(entity.ID));
            return Delete(model);
        }

        public virtual Scope Delete(Scope entity)
        {
            entity.Deleted = true;
            _db.SaveChanges();
            return entity;
        }
    }

}
