﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;

namespace SnapSuite.Models
{
    
    public class BasisOfEstimateService : GenericService<BasisOfEstimate>
    {
        public BasisOfEstimateService(SnapDbContext dbContext): base(dbContext)
        {
        }

        public override IQueryable<BasisOfEstimate> Read()
        {
            return base.Read().OrderBy(x => x.SortPos);
        }

        public virtual BasisOfEstimate Delete(BasisOfEstimateGridViewModel entity)
        {
            var model = Find(Guid.Parse(entity.ID));
            return Destroy(model);
        }

        public virtual BasisOfEstimate FindByID(Guid ID)
        {
            return base.Read().Where(i => i.ID == ID).First();
        }

    }

}
