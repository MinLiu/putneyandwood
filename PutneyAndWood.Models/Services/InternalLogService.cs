﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    
    public class InternalLogService<TViewModel> : CompanyEntityService<InternalLog, TViewModel>
        where TViewModel: class, IEntityViewModel, new()
    {
        
        public override bool LogChanges { get { return false; } set { } }


        public InternalLogService(string UserID, Guid CompanyID, ModelMapper<InternalLog, TViewModel> mapper, DbContext context)
            : base(CompanyID, UserID, mapper, context)
        {
            
        }


        public virtual IQueryable<InternalLog> Search(string filterText)
        {   
            var accessSettings = new SubscriptionService<TViewModel>(null,DbContext).GetSubscriptionUserAccess(CurrentUserID);
            if (accessSettings == null) return null;

            var query = Read();

            filterText = (filterText ?? "").ToLower();

            return query.Where(p => 
                                (
                                    (p.Description.ToLower().Contains(filterText) || p.Title.ToLower().Contains(filterText))
                                    || (filterText == "" || filterText == null)
                                )
                )
                .OrderByDescending(p => p.Timestamp);
        }


        public InternalLog AddLog(string type, string title, string description)
        {
            return AddLog(CompanyID, CurrentUserID, type, title, description);
        }

        public new InternalLog AddLog(string userID, string type ,string title,string description)
        {
            if (CompanyID == Guid.Empty)
                return null;

            return AddLog(CompanyID, userID, type, title, description);
        }

        public InternalLog AddLog(Guid companyID, string userID, string type, string title, string description)
        {
            var user = new UserService(DbContext).FindById(userID);
            if (user == null) return null;

            return AddLog(companyID, user, type, title, description);
        }


        public InternalLog AddLog(Guid companyID, User user, string type, string title, string description)
        {
            if (companyID == Guid.Empty) return null;
            if (user == null) return null;

            var model = new InternalLog
            {
                CompanyID = companyID,
                Timestamp = DateTime.Now,
                Type = type,
                Title = title,
                Description = description,
                ActivityByUserID = user.Id,
                ActivityByUserName = string.Format("{0} {1} - {2}", user.FirstName, user.LastName, user.Email),
            };

            try { return Create(model); }
            catch { return null; }
        }


        public override IEnumerable<InternalLog> Update(IEnumerable<InternalLog> entities, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override InternalLog Update(InternalLog entity, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override IEnumerable<InternalLog> Destroy(IEnumerable<InternalLog> entities) { throw new Exception("Not possible"); }
        public override InternalLog Destroy(InternalLog entity) { throw new Exception("Not possible"); }

        public override void AfterCreate(InternalLog model) { }
        public override void OnCreate(InternalLog model) { }
        public override void OnCreate(IEnumerable<InternalLog> models) { }
        public override void OnUpdate(InternalLog model) { }
        public override void OnUpdate(IEnumerable<InternalLog> models) { }
        public override void OnDestroy(InternalLog model) { }
        public override void OnDestroy(IEnumerable<InternalLog> models) { }

    }
    

}
