﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;



namespace SnapSuite.Models
{
    public class ImportResult
    {
        public bool Success { get; set; }
        public int NumberCreated { get; set; }
        public int NumberUpdated { get; set; }
        public Exception Exception { get; set; }
    }

    public static class ImportService
    {

        public static DataTable CopyExcelFileToTable(Guid companyID, System.Web.HttpPostedFileBase file, string sheetName = "", bool deleteFile = true)
        {
            var path = new FileService(null).SaveFileWithoutEntry(companyID, file, "/Imports/");
            path = FileService.ToPhysicalPath(path);
            return CopyExcelFileToTable(path, sheetName, deleteFile);
        }


        public static DataTable CopyExcelFileToTable(Guid companyID, byte[] bytes, string name, string extension, string sheetName = "", bool deleteFile = true)
        {
            var path = new FileService(null).SaveFileWithoutEntry(companyID, bytes, name, extension, "/Imports/");
            path = FileService.ToPhysicalPath(path);
            return CopyExcelFileToTable(path, sheetName, deleteFile);
        }

        public static DataTable CopyExcelFileToTable(string physicalFilePath, string sheetName = "", bool deleteFile = true)
        {
            try
            {
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalFilePath + ";Extended Properties='Excel 12.0;HDR={1}'";
                var table = new DataTable();

                using (var conn = new OleDbConnection(connString))
                {
                    conn.Open();
                    try
                    {
                        var firstSheetName = (!string.IsNullOrWhiteSpace(sheetName)) ? sheetName : conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();

                        using (var adapter = new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", conn))
                        {
                            adapter.Fill(table);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                return table;
            }
            finally
            {
                // Delete the file from the server to free up space if required.
                if (deleteFile)
                {
                    try { File.Delete(physicalFilePath); }
                    catch { }
                }
            }
            
        }


        public static bool IsTableValid(DataTable table, string[] columns, out string message)
        {
            bool valid = true;
            message = String.Empty;
            var missingColumns = new List<string>();

            // Check all columns exist and are labelled correctly. 
            foreach (var label in columns)
            {
                if (!table.Columns.Contains(label))
                {
                    valid = false;
                    missingColumns.Add(label);
                }
            }

            if (!valid)
            {
                message = String.Format("Import file is missing the following columns: '{0}'", String.Join(",", missingColumns));
                return false;
            }

            return valid;
        }
    }


    

}
