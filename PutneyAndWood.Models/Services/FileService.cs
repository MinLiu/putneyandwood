﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{

    public class FileService : GenericService<FileEntry>
    {
        public static string WebURL = null;
        public static string ServerFilePath = null;
        private static string ContentFolderRoot = "/Uploads/";
                
        public int MaxContentLengthInMbs { get; set; } //Default 5mbs

        public FileService(DbContext dbContext)
            : base(dbContext)
        {
            this.MaxContentLengthInMbs = 5;
        }

        public FileService(DbContext dbContext, int maxSizeInMbs)
            : base(dbContext)
        {
            this.MaxContentLengthInMbs = maxSizeInMbs; //Default 5mbs
        }

        public static string ToPhysicalPath(string virtualPath)
        {
            if (virtualPath.Contains(ServerFilePath))
            {
                return virtualPath;
            }
            else if (virtualPath.StartsWith("~/"))
            {
                var regex = new Regex(Regex.Escape("~/"));
                var corrected = virtualPath.Replace("~/", "/");
                return regex.Replace(corrected.Replace('/', '\\'), ServerFilePath, 1);
            }
            else if (virtualPath.StartsWith("/"))
            {
                var regex = new Regex(Regex.Escape("\\"));
                return regex.Replace(virtualPath.Replace('/', '\\'), ServerFilePath, 1);
            }
            else if (!string.IsNullOrEmpty(WebURL) || virtualPath.StartsWith(WebURL))
            {
                var regex = new Regex(Regex.Escape(WebURL));
                return regex.Replace(virtualPath.Replace('/', '\\'), ServerFilePath, 1);
            }
            else
            {
                return string.Format("{0}\\{1}", ServerFilePath, virtualPath.Replace('/', '\\'));
            }                

        }

        public static string ToVirtualPath(string physicalPath)
        {
            var regex = new Regex(Regex.Escape(ServerFilePath.Replace('\\', '/')));
            return regex.Replace(physicalPath.Replace('\\', '/'), "/", 1);
        }


        //public override FileEntry Create(FileEntry entity) { throw new Exception("Not possible"); }
        //public override IEnumerable<FileEntry> Create(IEnumerable<FileEntry> entities) { throw new Exception("Not possible"); }
        //public override FileEntry Update(FileEntry entity, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        //public override IEnumerable<FileEntry> Update(IEnumerable<FileEntry> entities, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        //public override FileEntry Destroy(FileEntry entity) { throw new Exception("Not possible"); }
        //public override IEnumerable<FileEntry> Destroy(IEnumerable<FileEntry> entities) { throw new Exception("Not possible"); }

        public FileEntry SaveFile(Guid companyID, string path, int contentLength)
        {   
            var extension = Path.GetExtension(path);
            return CompleteUpload(path, extension, contentLength);
        }

        public FileEntry SaveFile(Guid companyID, System.Web.HttpPostedFileBase file, string subFolder = "/", string saveFilename = null)
        {
            var path = SaveFileWithoutEntry(companyID, file, subFolder, saveFilename);            
            var extension = Path.GetExtension(file.FileName);
            return CompleteUpload(path, extension, file.ContentLength);
        }
        

        public FileEntry SaveFile(Guid companyID, byte[] bytes, string name, string extension, string subFolder = "/", bool genSaveFilename = true)
        {   
            var path = SaveFileWithoutEntry(companyID, bytes, name, extension, subFolder, genSaveFilename);
            if (!extension.Contains('.')) extension = "." + extension;            
            return CompleteUpload(path, extension, bytes.Length);
        }

        public string SaveFileWithoutEntry(Guid companyID, System.Web.HttpPostedFileBase file, string subFolder = "/", string saveFilename = null)
        {
            if (file == null) throw new Exception("Save file can not be null.");

            //If larger then Xmbs then skip
            if (file.ContentLength > MaxContentLengthInMbs * 1024 * 1024) throw new Exception(string.Format("File is too big. Max file size is {0}Mbs.", MaxContentLengthInMbs));

            var virtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);

            var physicalPath = ToPhysicalPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var extension = Path.GetExtension(file.FileName);
            var str = (string.IsNullOrEmpty(saveFilename)) ? Guid.NewGuid().ToString() + extension : Path.GetFileNameWithoutExtension(saveFilename) + extension;
            var physicalSavePath = ToPhysicalPath(virtualPath) + str;
            file.SaveAs(physicalSavePath);
            return virtualPath + str;
        }


        public string SaveFileWithoutEntry(Guid companyID, byte[] bytes, string name, string extension, string subFolder = "/", bool genSaveFilename = true)
        {
            var virtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);

            var physicalPath = ToPhysicalPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            if (!extension.Contains('.')) extension = string.Format(".{0}", extension);
            var str = (genSaveFilename) ? Guid.NewGuid().ToString() + extension : name + extension;
            var physicalSavePath = ToPhysicalPath(virtualPath) + str;
            _SaveBytesToFile(bytes, physicalSavePath);
            return virtualPath + str;
        }


        public FileEntry SaveLink(string link)
        {
            var upload = new FileEntry { FilePath = link, FileExtension = "", FileFormat = FileFormat.Link, FileSize = 0 };
            return base.Create(upload);
        }


        private FileEntry CompleteUpload(string fileLoc, string extension, int size)
        {
            var format = FileFormat.File;
            switch (extension)
            {
                case ".jpg": format = FileFormat.Image; break;
                case ".jpeg": format = FileFormat.Image; break;
                case ".bmp": format = FileFormat.Image; break;
                case ".png": format = FileFormat.Image; break;
                case ".gif": format = FileFormat.Image; break;
                case ".tif": format = FileFormat.Image; break;
                case ".mp4": format = FileFormat.Video; break;
                case ".ogg": format = FileFormat.Video; break;
                case ".webm": format = FileFormat.Video; break;
                case ".pdf": format = FileFormat.Pdf; break;
                case ".docx": format = FileFormat.Docx; break;
                default: format = FileFormat.File; break;
            }
            
            return base.Create(new FileEntry { FilePath = fileLoc, FileExtension = extension, FileFormat = format, FileSize = size });
        }


        public FileEntry DeepCopyFile(Guid companyID, Guid entryID, string subFolder = "/", string filename = "")
        {
            return DeepCopyFile(companyID, Read().Where(i => i.ID == entryID).First(), subFolder, filename);
        }

        public FileEntry DeepCopyFile(Guid companyID, FileEntry entry, string subFolder = "/", string filename = "")
        {
            var copyPath = DeepCopyFile(companyID, entry.FilePath, subFolder, filename);           
            return base.Create(new FileEntry { FilePath = copyPath, FileExtension = entry.FileExtension, FileFormat = entry.FileFormat, FileSize = entry.FileSize });
        }


        public string DeepCopyFile(Guid companyID, string path, string subFolder = "/", string filename = "")
        {
            var physicalPath = ToPhysicalPath(path);
            var copyVirtualPath = string.Format("{0}{1}{2}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder);
            var copyPhysicalPath = ToPhysicalPath(copyVirtualPath);

            if (!Directory.Exists(copyPhysicalPath))
            {
                Directory.CreateDirectory(copyPhysicalPath);
            }

            var extension = Path.GetExtension(path);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;
            copyPhysicalPath += str;

            File.Copy(physicalPath, copyPhysicalPath, true);

            return copyVirtualPath + str;
        }

        public FileEntry DeleteFile(Guid entryID)
        {
            try
            {
                var entry = base.Read().Where(i => i.ID == entryID).First();
                var filePath = ToPhysicalPath(entry.FilePath);

                //Destroy entry first
                base.Destroy(entry);

                //Once entry is destroyed then delete file from server
                if (File.Exists(filePath)) File.Delete(filePath);
                
                return entry;
            }
            catch(Exception ex) { return null; }
        }

        
        public bool DeleteFile(string virtualPath)
        {
            try
            {               
                var filePath = ToPhysicalPath(virtualPath);
                if (File.Exists(filePath)) File.Delete(filePath);
                return true;
            }
            catch { return false; }
        }


        public static bool DoesFileExists(Guid companyID, string subFolder = "/", string filename = "")
        {           
            var virtualPath = string.Format("{0}{1}{2}{3}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename);

            return (File.Exists(virtualPath));
        }


        public static string GetUploadVirtualFilePath(Guid companyID, string subFolder = "/", string filename = "")
        {            
            return string.Format("{0}{1}{2}{3}", ContentFolderRoot, companyID.ToString(), (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename);
        }

        public static string GetUploadPhysicalFilePath(Guid companyID, string subFolder = "/", string filename = "")
        {
            return ToPhysicalPath(GetUploadVirtualFilePath(companyID, (string.IsNullOrWhiteSpace(subFolder)) ? "/" : subFolder, filename));
        }


        private static bool _SaveBytesToFile(byte[] bytes, string filename) 
        {
            try
            {
                using (FileStream file = new FileStream(filename, FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(bytes, 0, bytes.Length);
                }
                return true;
            }
            catch { return false; }
        }



        public void CleanUpFiles()
        {
            var files = Read().Where(i => i.LogoImageCompanies.Count() <= 0
                                        && i.FileFormat != FileFormat.Link
                                        && i.ScreenLogoImageCompanies.Count() <= 0
                                        && i.QuotationItemImages.Count() <= 0
                                        && i.ProductImages.Count() <= 0
                                        && i.LogoImageCompanies.Count() <= 0
                                        && i.ScreenLogoImageCompanies.Count() <= 0
                                        && i.ClientAttachments.Count() <= 0
                                        && i.QuotationAttachments.Count() <= 0
                                        && i.DefaultQuotationAttachments.Count() <= 0
                                        && i.QuotationTemplates.Count() <= 0
                                        && i.QuotationAgreementSigntures.Count() <= 0
                                        && i.QuotationAgreementAgreements.Count() <= 0
                                        && i.QuotationAgreementApproves.Count() <= 0
                                        )
                                        .Select(i => i.ID)
                                    .ToArray();

            foreach(var id in files)
            {
                DeleteFile(id);
            }
        }


    }
    

}
