﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net;
using System.Reflection;
using System.IO;
using System.Net.Mime;

namespace SnapSuite.Models
{
    
    public class EmailMessageService : GenericService<CompanyEmail>
    {

        public virtual Guid? CompanyID { get; set; }
                
        public EmailMessageService(Guid? CompanyID, DbContext context)
            : base(context)
        {
            this.CompanyID = CompanyID;
        }


        public bool SendEmail(string email, string subject, string message)
        {
            MailMessage mailMessage = CreateSNAPSuiteMailMessage(email, null, subject, message);            
            return SendEmail(mailMessage);

        }

        public bool SendEmails(string[] emails, string subject, string message)
        {            
            MailMessage mailMessage = CreateSNAPSuiteMailMessage(null, null, subject, message);
            foreach (string e in emails)
            {
                mailMessage.To.Add(e);
            }
            return SendEmail(mailMessage);
        }

        public bool SendEmail(MailMessage mailMessage, bool UseSendGridAsDefault = false)
        {
            return SendEmail(null, mailMessage, UseSendGridAsDefault);
        }

        public bool SendEmail(Guid? companyID, MailMessage mailMessage, bool UseSendGridAsDefault = false)
        {
            try
            {
                var _settings = DbContext.Set<Fruitful.Email.EmailSetting>().First();   //Get Default emails settings
                
                //Add branding footer. But not if the company dones't allow it.
                if (DbContext.Set<Company>().Where(i => i.ID == companyID).Select(i => i.RemoveEmailFooter).DefaultIfEmpty(false).FirstOrDefault() == false)
                {
                    try
                    {
                        mailMessage.Body += "<br /><br /><br /><hr /><a href=\"https://quikflw.com/\" ><img src=\"cid:powered_by_logo.png\" style=\"max-height:20px;\"/></a>";
                        Stream imageStream = Assembly.GetAssembly(typeof(EmailMessageService)).GetManifestResourceStream("SnapSuite.Models.Resources.powered_by_logo.png");
                        Attachment img1 = new Attachment(imageStream, "powered_by_logo.png");
                        img1.ContentId = "powered_by_logo.png";
                        mailMessage.Attachments.Add(img1);
                    }
                    catch { }
                }

                //Default SendGrid details
                var host = "smtp.sendgrid.net";
                var port = 587;
                var userName = "tim.randall";
                var password = "SBhp2xb546";
                var enableSsl = true;

                if ((companyID == null || companyID == Guid.Empty) && UseSendGridAsDefault) //Use the SendGrid SMTP values
                {
                    //Do nothing
                }
                else if((companyID == null || companyID == Guid.Empty) && !UseSendGridAsDefault) //Use the gmail SNAP SMTP Values if told not to send with Sendgrid
                {   
                    host = _settings.Host;
                    port = _settings.Port;
                    userName = _settings.Email;
                    password = _settings.Password;
                    enableSsl = _settings.EnableSSL;
                }
                else
                {
                    CompanyEmail emailSettings = null;

                    if (mailMessage.From != null) //Find SMTP details for email address in the from adress
                       emailSettings = Read().Where(i => i.CompanyID == companyID && i.Email == mailMessage.From.Address && i.SuccessfulTest == true).DefaultIfEmpty(null).FirstOrDefault();

                    if (emailSettings == null) //If you can't find the from SMTP, then find the default ones
                    {
                        emailSettings = Read().Where(i => i.CompanyID == companyID && i.Default == true && i.SuccessfulTest == true).DefaultIfEmpty(null).FirstOrDefault();
                        mailMessage.ReplyToList.Add(mailMessage.From); //Add the reply to email address so incase the mail server rewrites the from address e.eg GMail
                    }

                    if(emailSettings != null) //Only apply the SMTP infomation is company email settings are found
                    {
                        if (mailMessage.From == null || string.IsNullOrEmpty(mailMessage.From.Address)) mailMessage.From = new MailAddress(emailSettings.Email);
                        host = emailSettings.Host;
                        port = emailSettings.Port;
                        userName = emailSettings.Email;
                        password = emailSettings.Password;
                        enableSsl = emailSettings.EnableSSL;
                    }
                }
           
                var client = new SmtpClient(host, port)
                {
                    Credentials = new NetworkCredential(userName, password),
                    EnableSsl = enableSsl
                };

                if (mailMessage.From == null || string.IsNullOrEmpty(mailMessage.From.Address)) mailMessage.From = new MailAddress(_settings.Email); //IF a from address if not added then add the standard from address.
                client.Send(mailMessage);
                return true;
            }
            catch { return false; }
        }

        /*
        public bool SendEmailWithSendGrid(MailMessage mailMessage)
        {
            try
            {
                var _settings = new GenericService<Fruitful.Email.EmailSetting>(new SnapDbContext()).Read().First();
               
                

                var client = new SmtpClient(host, port)
                {
                    Credentials = new NetworkCredential(userName, password),
                    EnableSsl = enableSsl
                };

                if (mailMessage.From == null || string.IsNullOrEmpty(mailMessage.From.Address)) mailMessage.From = new MailAddress(_settings.Email);
                client.Send(mailMessage);
                return true;
            }
            catch { return false; }
        }
        */

        public static MailMessage CreateCompanyMessage(string email, string fromEmail, string subject, string message, string logofilePath, IEnumerable<Attachment> attachments = null)
        {
            var mailMessage = CreateMailMessage(email, fromEmail, subject, "", attachments);
            mailMessage.Body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\"> <p><img src=\"cid:company_logo.png\" style=\"height: 30px; max-height:30px;\"/></p> <hr /><br />{0} </div> </div>",
                                                       message.Replace("\n", "<br />"));
            mailMessage.IsBodyHtml = true;

            try
            {
                Attachment img1 = new Attachment(FileService.ToPhysicalPath(logofilePath));
                img1.ContentId = "company_logo.png"; mailMessage.Attachments.Add(img1);
            }
            catch { }

            return mailMessage;
        }



        public static MailMessage CreateSNAPSuiteMailMessage(string email, string fromEmail, string subject, string message, IEnumerable<Attachment> attachments = null)
        {

            var mailMessage = CreateMailMessage(email, fromEmail, subject, "", attachments);
            mailMessage.Body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\"> <p><img src=\"cid:email_logo.png\" style=\"height: 30px; max-height:30px;\"/></p> <hr /><br />{0} </div> </div>",
                                                       message.Replace("\n", "<br />"));
            mailMessage.IsBodyHtml = true;

            try {
                Assembly assembly = Assembly.GetAssembly(typeof(EmailMessageService));
                Stream imageStream = assembly.GetManifestResourceStream("SnapSuite.Models.Resources.email_logo.png");
                Attachment img1 = new Attachment(imageStream, "image");
                img1.ContentId = "email_logo.png";
                mailMessage.Attachments.Add(img1);
            }
            catch { }

            return mailMessage;
        }
        

        public static MailMessage CreateMailMessage(string email, string fromEmail, string subject, string body, IEnumerable<Attachment> attachments = null)
        {
            MailMessage mailMessage = new MailMessage();
            if(!string.IsNullOrEmpty(email)) mailMessage.To.Add(email);
            if (!string.IsNullOrEmpty(fromEmail)) mailMessage.From = new MailAddress(fromEmail);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    mailMessage.Attachments.Add(attachment);
                }
            }

            return mailMessage;
        }


        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }



    }
    

}
