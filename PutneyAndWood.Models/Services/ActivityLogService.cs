﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using SnapSuite.Internal;
using System.Web.Script.Serialization;

namespace SnapSuite.Models
{
    
    public class ActivityLogService<TViewModel> : CompanyEntityService<ActivityLog, TViewModel>
        where TViewModel: class, IEntityViewModel, new()
    {
        
        public override bool LogChanges { get { return false; } set { } }


        public ActivityLogService(string UserID, Guid CompanyID, ModelMapper<ActivityLog, TViewModel> mapper, DbContext context)
            : base(CompanyID, UserID, mapper, context)
        {
            
        }


        public virtual IQueryable<ActivityLog> Search(string filterText, int? moduleID, bool ignoreTeams = false)
        {   
            var accessSettings = new SubscriptionService<TViewModel>(null,DbContext).GetSubscriptionUserAccess(CurrentUserID);
            if (accessSettings == null) return null;

            var query = Read();

            if (!ignoreTeams)           
            {
                var users = new UserService(DbContext).GetCompanyUserIds(CurrentUserID);
                query = query.Where(o => users.Contains(o.ActivityByUserID) || string.IsNullOrEmpty(o.ActivityByUserID));
            }

            filterText = (filterText ?? "").ToLower();

            return query.Where(p => (p.ModuleTypeID == moduleID || moduleID == null)
                                &&
                                (
                                    (p.ModuleTypeID == ModuleTypeValues.PRODUCTS && accessSettings.ViewProducts)
                                    || (p.ModuleTypeID == ModuleTypeValues.CLIENTS && accessSettings.ViewCRM)
                                    || (p.ModuleTypeID == ModuleTypeValues.SUPPLIERS && accessSettings.ViewPurchaseOrders)
                                    || (p.ModuleTypeID == ModuleTypeValues.QUOTATIONS && accessSettings.ViewQuotations)
                                    || (p.ModuleTypeID == ModuleTypeValues.JOBS && accessSettings.ViewJobs)
                                    || (p.ModuleTypeID == ModuleTypeValues.DELIVERY_NOTES && accessSettings.ViewDeliveryNotes)
                                    || (p.ModuleTypeID == ModuleTypeValues.INVOICES && accessSettings.ViewInvoices)
                                    || (p.ModuleTypeID == ModuleTypeValues.PURCHASE_ORDERS && accessSettings.ViewPurchaseOrders)
                                )
                                &&
                                (
                                    (p.Description.ToLower().Contains(filterText) || p.ActivityByUserName.ToLower().Contains(filterText) )
                                    || (filterText == "" || filterText == null)
                                )
                )
                .OrderByDescending(p => p.Timestamp);
        }


        public ActivityLog AddLog(int moduleID, string description)
        {
            return AddLog(CompanyID, moduleID, description, CurrentUserID, null);
        }


        public ActivityLog AddLog(int moduleID, string description, string linkID)
        {
            return AddLog(CompanyID, moduleID, description, CurrentUserID, linkID);
        }


        public new ActivityLog AddLog(int moduleID, string description, string userID, string linkID)
        {
            if (CompanyID == Guid.Empty)
                return null;

            return AddLog(CompanyID, moduleID, description, CurrentUserID, linkID);
        }

        public ActivityLog AddLog(Guid companyID, int moduleID, string description, string userID, string linkID)
        {
            var user = new UserService(DbContext).FindById(userID);
            if (user == null) return null;

            return AddLog(companyID, moduleID, description, user, linkID);
        }


        public ActivityLog AddLog(Guid companyID, int moduleID, string description, User user, string linkID)
        {
            if (companyID == Guid.Empty) return null;
            if (moduleID <= 0) return null;
            if (user == null) return null;

            var model = new ActivityLog
            {
                CompanyID = companyID,
                Timestamp = DateTime.Now,
                ModuleTypeID = moduleID,
                Description = description,
                ActivityByUserID = user.Id,
                ActivityByUserName = string.Format("{0} {1} - {2}", user.FirstName, user.LastName, user.Email),
                LinkID = linkID,
            };

            try { return Create(model); }
            catch { return null; }
        }


        public override IEnumerable<ActivityLog> Update(IEnumerable<ActivityLog> entities, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override ActivityLog Update(ActivityLog entity, string[] updatedColumns = null) { throw new Exception("Not possible"); }
        public override IEnumerable<ActivityLog> Destroy(IEnumerable<ActivityLog> entities) { throw new Exception("Not possible"); }
        public override ActivityLog Destroy(ActivityLog entity) { throw new Exception("Not possible"); }

        public override void AfterCreate(ActivityLog model) { }
        public override void OnCreate(ActivityLog model) { }
        public override void OnCreate(IEnumerable<ActivityLog> models) { }
        public override void OnUpdate(ActivityLog model) { }
        public override void OnUpdate(IEnumerable<ActivityLog> models) { }
        public override void OnDestroy(ActivityLog model) { }
        public override void OnDestroy(IEnumerable<ActivityLog> models) { }

    }
    

}
