namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuotation3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "Greeting", c => c.String());
            AddColumn("dbo.Quotations", "EstimatorTitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quotations", "EstimatorTitle");
            DropColumn("dbo.Quotations", "Greeting");
        }
    }
}
