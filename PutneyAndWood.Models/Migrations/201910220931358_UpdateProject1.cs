namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProject1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Consultant1ID", c => c.Guid());
            AddColumn("dbo.Projects", "Consultant2ID", c => c.Guid());
            AddColumn("dbo.Projects", "Consultant3ID", c => c.Guid());
            AddColumn("dbo.Projects", "Consultant4ID", c => c.Guid());
            CreateIndex("dbo.Projects", "Consultant1ID");
            CreateIndex("dbo.Projects", "Consultant2ID");
            CreateIndex("dbo.Projects", "Consultant3ID");
            CreateIndex("dbo.Projects", "Consultant4ID");
            AddForeignKey("dbo.Projects", "Consultant1ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Consultant2ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Consultant3ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Consultant4ID", "dbo.Clients", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "Consultant4ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Consultant3ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Consultant2ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Consultant1ID", "dbo.Clients");
            DropIndex("dbo.Projects", new[] { "Consultant4ID" });
            DropIndex("dbo.Projects", new[] { "Consultant3ID" });
            DropIndex("dbo.Projects", new[] { "Consultant2ID" });
            DropIndex("dbo.Projects", new[] { "Consultant1ID" });
            DropColumn("dbo.Projects", "Consultant4ID");
            DropColumn("dbo.Projects", "Consultant3ID");
            DropColumn("dbo.Projects", "Consultant2ID");
            DropColumn("dbo.Projects", "Consultant1ID");
        }
    }
}
