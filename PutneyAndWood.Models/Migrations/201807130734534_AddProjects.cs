namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjects : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        Name = c.String(),
                        Number = c.Int(nullable: false),
                        Description = c.String(),
                        StoneworkStartDate = c.DateTime(),
                        Value = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.ProjectStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.ProjectStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "StatusID", "dbo.ProjectStatus");
            DropForeignKey("dbo.Projects", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Projects", "AssignedUserID", "dbo.AspNetUsers");
            DropIndex("dbo.Projects", new[] { "StatusID" });
            DropIndex("dbo.Projects", new[] { "AssignedUserID" });
            DropIndex("dbo.Projects", new[] { "CompanyID" });
            DropTable("dbo.ProjectStatus");
            DropTable("dbo.Projects");
        }
    }
}
