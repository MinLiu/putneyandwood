namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuotation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "ProjectID", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quotations", "ProjectID");
        }
    }
}
