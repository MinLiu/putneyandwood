namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBasisOfEstimate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductQualifications", "IsSubHeading", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductQualifications", "IsSubItem", c => c.Boolean(nullable: false));
            AddColumn("dbo.QuotationQualifications", "IsSubHeading", c => c.Boolean(nullable: false));
            AddColumn("dbo.QuotationQualifications", "IsSubItem", c => c.Boolean(nullable: false));
            AddColumn("dbo.QuotationBasisOfEstimates", "IsSubHeading", c => c.Boolean(nullable: false));
            AddColumn("dbo.QuotationBasisOfEstimates", "IsSubItem", c => c.Boolean(nullable: false));
            AddColumn("dbo.BasisOfEstimates", "IsSubHeading", c => c.Boolean(nullable: false));
            AddColumn("dbo.BasisOfEstimates", "IsSubItem", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BasisOfEstimates", "IsSubItem");
            DropColumn("dbo.BasisOfEstimates", "IsSubHeading");
            DropColumn("dbo.QuotationBasisOfEstimates", "IsSubItem");
            DropColumn("dbo.QuotationBasisOfEstimates", "IsSubHeading");
            DropColumn("dbo.QuotationQualifications", "IsSubItem");
            DropColumn("dbo.QuotationQualifications", "IsSubHeading");
            DropColumn("dbo.ProductQualifications", "IsSubItem");
            DropColumn("dbo.ProductQualifications", "IsSubHeading");
        }
    }
}
