namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConsultantContact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "ConsultantContact1ID", c => c.Guid());
            AddColumn("dbo.Projects", "ConsultantContact2ID", c => c.Guid());
            AddColumn("dbo.Projects", "ConsultantContact3ID", c => c.Guid());
            AddColumn("dbo.Projects", "ConsultantContact4ID", c => c.Guid());
            CreateIndex("dbo.Projects", "ConsultantContact1ID");
            CreateIndex("dbo.Projects", "ConsultantContact2ID");
            CreateIndex("dbo.Projects", "ConsultantContact3ID");
            CreateIndex("dbo.Projects", "ConsultantContact4ID");
            AddForeignKey("dbo.Projects", "ConsultantContact1ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ConsultantContact2ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ConsultantContact3ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ConsultantContact4ID", "dbo.ClientContacts", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "ConsultantContact4ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ConsultantContact3ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ConsultantContact2ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ConsultantContact1ID", "dbo.ClientContacts");
            DropIndex("dbo.Projects", new[] { "ConsultantContact4ID" });
            DropIndex("dbo.Projects", new[] { "ConsultantContact3ID" });
            DropIndex("dbo.Projects", new[] { "ConsultantContact2ID" });
            DropIndex("dbo.Projects", new[] { "ConsultantContact1ID" });
            DropColumn("dbo.Projects", "ConsultantContact4ID");
            DropColumn("dbo.Projects", "ConsultantContact3ID");
            DropColumn("dbo.Projects", "ConsultantContact2ID");
            DropColumn("dbo.Projects", "ConsultantContact1ID");
        }
    }
}
