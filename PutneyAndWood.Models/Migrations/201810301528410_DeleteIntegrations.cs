namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteIntegrations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HubSpotSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InfusionsoftSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InsightlySettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SalesforceSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StripeSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ZohoSettings", "CompanyID", "dbo.Companies");
            DropIndex("dbo.HubSpotSettings", new[] { "CompanyID" });
            DropIndex("dbo.InfusionsoftSettings", new[] { "CompanyID" });
            DropIndex("dbo.InsightlySettings", new[] { "CompanyID" });
            DropIndex("dbo.SalesforceSettings", new[] { "CompanyID" });
            DropIndex("dbo.StripeSettings", new[] { "CompanyID" });
            DropIndex("dbo.ZohoSettings", new[] { "CompanyID" });
            DropTable("dbo.HubSpotSettings");
            DropTable("dbo.InfusionsoftSettings");
            DropTable("dbo.InsightlySettings");
            DropTable("dbo.SalesforceSettings");
            DropTable("dbo.StripeSettings");
            DropTable("dbo.ZohoSettings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ZohoSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                        ApiDomain = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.StripeSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        TokenType = c.String(),
                        PublishableKey = c.String(),
                        Scope = c.String(),
                        LiveMode = c.Boolean(nullable: false),
                        UserID = c.String(),
                        RefreshToken = c.String(),
                        AccessToken = c.String(),
                        DoNotChargeFee = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.SalesforceSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                        IdURL = c.String(),
                        InstanceURL = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.InsightlySettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        ApiKey = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.InfusionsoftSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateTable(
                "dbo.HubSpotSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateIndex("dbo.ZohoSettings", "CompanyID");
            CreateIndex("dbo.StripeSettings", "CompanyID");
            CreateIndex("dbo.SalesforceSettings", "CompanyID");
            CreateIndex("dbo.InsightlySettings", "CompanyID");
            CreateIndex("dbo.InfusionsoftSettings", "CompanyID");
            CreateIndex("dbo.HubSpotSettings", "CompanyID");
            AddForeignKey("dbo.ZohoSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.StripeSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.SalesforceSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InsightlySettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.InfusionsoftSettings", "CompanyID", "dbo.Companies", "ID");
            AddForeignKey("dbo.HubSpotSettings", "CompanyID", "dbo.Companies", "ID");
        }
    }
}
