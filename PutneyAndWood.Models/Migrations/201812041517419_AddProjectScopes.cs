namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectScopes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectScopes",
                c => new
                    {
                        ProjectID = c.Guid(nullable: false),
                        ScopeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProjectID, t.ScopeID })
                .ForeignKey("dbo.Projects", t => t.ProjectID)
                .ForeignKey("dbo.Scopes", t => t.ScopeID)
                .Index(t => t.ProjectID)
                .Index(t => t.ScopeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectScopes", "ScopeID", "dbo.Scopes");
            DropForeignKey("dbo.ProjectScopes", "ProjectID", "dbo.Projects");
            DropIndex("dbo.ProjectScopes", new[] { "ScopeID" });
            DropIndex("dbo.ProjectScopes", new[] { "ProjectID" });
            DropTable("dbo.ProjectScopes");
        }
    }
}
