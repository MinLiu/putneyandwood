namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuotation2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "ClosingParagraph", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quotations", "ClosingParagraph");
        }
    }
}
