namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuotationEvent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuotationEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.CompanyID)
                .Index(t => t.QuotationID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.CreatorID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuotationEvents", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationEvents", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.QuotationEvents", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationEvents", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.QuotationEvents", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.QuotationEvents", "AssignedUserID", "dbo.AspNetUsers");
            DropIndex("dbo.QuotationEvents", new[] { "ClientContactID" });
            DropIndex("dbo.QuotationEvents", new[] { "ClientID" });
            DropIndex("dbo.QuotationEvents", new[] { "CreatorID" });
            DropIndex("dbo.QuotationEvents", new[] { "AssignedUserID" });
            DropIndex("dbo.QuotationEvents", new[] { "QuotationID" });
            DropIndex("dbo.QuotationEvents", new[] { "CompanyID" });
            DropTable("dbo.QuotationEvents");
        }
    }
}
