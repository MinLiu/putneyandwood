namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectClient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Client1ID", c => c.Guid());
            AddColumn("dbo.Projects", "ClientContact1ID", c => c.Guid());
            AddColumn("dbo.Projects", "Client2ID", c => c.Guid());
            AddColumn("dbo.Projects", "ClientContact2ID", c => c.Guid());
            AddColumn("dbo.Projects", "Client3ID", c => c.Guid());
            AddColumn("dbo.Projects", "ClientContact3ID", c => c.Guid());
            AddColumn("dbo.Projects", "Client4ID", c => c.Guid());
            AddColumn("dbo.Projects", "ClientContact4ID", c => c.Guid());
            CreateIndex("dbo.Projects", "Client1ID");
            CreateIndex("dbo.Projects", "ClientContact1ID");
            CreateIndex("dbo.Projects", "Client2ID");
            CreateIndex("dbo.Projects", "ClientContact2ID");
            CreateIndex("dbo.Projects", "Client3ID");
            CreateIndex("dbo.Projects", "ClientContact3ID");
            CreateIndex("dbo.Projects", "Client4ID");
            CreateIndex("dbo.Projects", "ClientContact4ID");
            AddForeignKey("dbo.Projects", "Client1ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Client2ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Client3ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "Client4ID", "dbo.Clients", "ID");
            AddForeignKey("dbo.Projects", "ClientContact1ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ClientContact2ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ClientContact3ID", "dbo.ClientContacts", "ID");
            AddForeignKey("dbo.Projects", "ClientContact4ID", "dbo.ClientContacts", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "ClientContact4ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ClientContact3ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ClientContact2ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "ClientContact1ID", "dbo.ClientContacts");
            DropForeignKey("dbo.Projects", "Client4ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Client3ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Client2ID", "dbo.Clients");
            DropForeignKey("dbo.Projects", "Client1ID", "dbo.Clients");
            DropIndex("dbo.Projects", new[] { "ClientContact4ID" });
            DropIndex("dbo.Projects", new[] { "Client4ID" });
            DropIndex("dbo.Projects", new[] { "ClientContact3ID" });
            DropIndex("dbo.Projects", new[] { "Client3ID" });
            DropIndex("dbo.Projects", new[] { "ClientContact2ID" });
            DropIndex("dbo.Projects", new[] { "Client2ID" });
            DropIndex("dbo.Projects", new[] { "ClientContact1ID" });
            DropIndex("dbo.Projects", new[] { "Client1ID" });
            DropColumn("dbo.Projects", "ClientContact4ID");
            DropColumn("dbo.Projects", "Client4ID");
            DropColumn("dbo.Projects", "ClientContact3ID");
            DropColumn("dbo.Projects", "Client3ID");
            DropColumn("dbo.Projects", "ClientContact2ID");
            DropColumn("dbo.Projects", "Client2ID");
            DropColumn("dbo.Projects", "ClientContact1ID");
            DropColumn("dbo.Projects", "Client1ID");
        }
    }
}
