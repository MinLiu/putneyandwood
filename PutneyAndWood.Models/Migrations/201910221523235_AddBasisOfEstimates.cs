namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBasisOfEstimates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BasisOfEstimates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Description = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BasisOfEstimates");
        }
    }
}
