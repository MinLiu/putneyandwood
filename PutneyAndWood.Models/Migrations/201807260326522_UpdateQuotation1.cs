namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuotation1 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Quotations", "ProjectID");
            AddForeignKey("dbo.Quotations", "ProjectID", "dbo.Projects", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Quotations", "ProjectID", "dbo.Projects");
            DropIndex("dbo.Quotations", new[] { "ProjectID" });
        }
    }
}
