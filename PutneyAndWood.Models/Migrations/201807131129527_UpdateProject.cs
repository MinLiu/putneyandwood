namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Note");
        }
    }
}
