namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScopes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Scopes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Scopes");
        }
    }
}
