namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteIntegrations2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.QuickBooksSettings", "CompanyID", "dbo.Companies");
            DropIndex("dbo.QuickBooksSettings", new[] { "CompanyID" });
            DropTable("dbo.QuickBooksSettings");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.QuickBooksSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        QuickBooks = c.Boolean(nullable: false),
                        RefreshToken = c.String(),
                        ReamID = c.String(),
                        AccessToken = c.String(),
                        UseQBDocNumber = c.Boolean(nullable: false),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID);
            
            CreateIndex("dbo.QuickBooksSettings", "CompanyID");
            AddForeignKey("dbo.QuickBooksSettings", "CompanyID", "dbo.Companies", "ID");
        }
    }
}
