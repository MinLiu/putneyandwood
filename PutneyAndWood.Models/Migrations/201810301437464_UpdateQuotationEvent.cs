namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuotationEvent : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.QuotationEvents", new[] { "QuotationID" });
            AddColumn("dbo.QuotationEvents", "ProjectID", c => c.Guid());
            AlterColumn("dbo.QuotationEvents", "QuotationID", c => c.Guid());
            CreateIndex("dbo.QuotationEvents", "ProjectID");
            CreateIndex("dbo.QuotationEvents", "QuotationID");
            AddForeignKey("dbo.QuotationEvents", "ProjectID", "dbo.Projects", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuotationEvents", "ProjectID", "dbo.Projects");
            DropIndex("dbo.QuotationEvents", new[] { "QuotationID" });
            DropIndex("dbo.QuotationEvents", new[] { "ProjectID" });
            AlterColumn("dbo.QuotationEvents", "QuotationID", c => c.Guid(nullable: false));
            DropColumn("dbo.QuotationEvents", "ProjectID");
            CreateIndex("dbo.QuotationEvents", "QuotationID");
        }
    }
}
