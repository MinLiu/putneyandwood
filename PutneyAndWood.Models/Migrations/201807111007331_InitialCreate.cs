namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ModuleTypeID = c.Int(nullable: false),
                        Description = c.String(),
                        ActivityByUserName = c.String(),
                        ActivityByUserID = c.String(),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.ModuleTypes", t => t.ModuleTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.ModuleTypeID);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AccountNo = c.String(),
                        SignupDate = c.DateTime(nullable: false),
                        AccountClosed = c.Boolean(nullable: false),
                        DeleteData = c.Boolean(nullable: false),
                        RemoveEmailFooter = c.Boolean(nullable: false),
                        WizardStep = c.Int(nullable: false),
                        WizardComplete = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        Motto = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VatNumber = c.String(),
                        RegNumber = c.String(),
                        LogoImageID = c.Guid(),
                        LogoImageURL = c.String(),
                        ScreenLogoImageID = c.Guid(),
                        ScreenLogoImageURL = c.String(),
                        ThemeColor = c.String(),
                        DefaultVatRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        DefaultCurrencyID = c.Int(nullable: false),
                        UseTeams = c.Boolean(nullable: false),
                        UseHierarchy = c.Boolean(nullable: false),
                        AccessSameLevelHierarchy = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.LogoImageID)
                .ForeignKey("dbo.FileEntries", t => t.ScreenLogoImageID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.LogoImageID)
                .Index(t => t.ScreenLogoImageID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.CompanyAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        CompanyID = c.Guid(nullable: false),
                        Token = c.String(),
                        AccessCRM = c.Boolean(nullable: false),
                        AccessProducts = c.Boolean(nullable: false),
                        AccessQuotations = c.Boolean(nullable: false),
                        AccessJobs = c.Boolean(nullable: false),
                        AccessInvoices = c.Boolean(nullable: false),
                        AccessDeliveryNotes = c.Boolean(nullable: false),
                        AccessPurchaseOrders = c.Boolean(nullable: false),
                        AccessStockControl = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ClientEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        OpportunityID = c.Guid(),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorID)
                .ForeignKey("dbo.Opportunities", t => t.OpportunityID)
                .Index(t => t.CompanyID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.CreatorID)
                .Index(t => t.OpportunityID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        SageAccountReference = c.String(),
                        StripeClientID = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.ClientTypes", t => t.ClientTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientTypeID);
            
            CreateTable(
                "dbo.ClientAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        County = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.Quotations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        AssignedUserID = c.String(maxLength: 128),
                        StatusID = c.Int(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        CheckSum = c.String(nullable: false),
                        Reference = c.String(),
                        ClientReference = c.String(),
                        Number = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Description = c.String(),
                        Note = c.String(),
                        PoNumber = c.String(),
                        JsonCustomFieldValues = c.String(),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalOtherCosts = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalLabourCosts = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalProfit = c.Decimal(nullable: false, precision: 28, scale: 5),
                        InvoiceAddressID = c.Guid(),
                        DeliveryAddressID = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        LastUpdated = c.DateTime(nullable: false),
                        EstimatedDelivery = c.DateTime(nullable: false),
                        EmailOpened = c.DateTime(),
                        UsePriceBreaks = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        IsClientVersion = c.Boolean(nullable: false),
                        IsClientDraft = c.Boolean(nullable: false),
                        MainVersionID = c.Guid(),
                        AllowClientEditLineItems = c.Boolean(nullable: false),
                        AllowClientEditSections = c.Boolean(nullable: false),
                        NeedsCheckedClientSubmission = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.ClientAddresses", t => t.DeliveryAddressID)
                .ForeignKey("dbo.ClientAddresses", t => t.InvoiceAddressID)
                .ForeignKey("dbo.QuotationStatus", t => t.StatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.StatusID)
                .Index(t => t.CurrencyID)
                .Index(t => t.InvoiceAddressID)
                .Index(t => t.DeliveryAddressID);
            
            CreateTable(
                "dbo.QuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        Filename = c.String(),
                        ShowInPreview = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.FileEntries",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FileFormat = c.Int(nullable: false),
                        FilePath = c.String(),
                        FileExtension = c.String(),
                        FileSize = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        Filename = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .Index(t => t.ClientID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.DefaultQuotationAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        Filename = c.String(),
                        ShowInPreview = c.Boolean(nullable: false),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .ForeignKey("dbo.QuotationSettings", t => t.QuotationSettings_CompanyID)
                .Index(t => t.FileID)
                .Index(t => t.QuotationSettings_CompanyID);
            
            CreateTable(
                "dbo.QuotationSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        DefaultReference = c.String(),
                        StartingNumber = c.Int(nullable: false),
                        DefaultNote = c.String(),
                        ShowPhoto = c.Boolean(nullable: false),
                        ShowProductCode = c.Boolean(nullable: false),
                        ShowUnit = c.Boolean(nullable: false),
                        ShowMarkup = c.Boolean(nullable: false),
                        ShowVAT = c.Boolean(nullable: false),
                        ShowDiscount = c.Boolean(nullable: false),
                        ShowListPrice = c.Boolean(nullable: false),
                        ShowListCost = c.Boolean(nullable: false),
                        ShowCost = c.Boolean(nullable: false),
                        ShowMargin = c.Boolean(nullable: false),
                        ShowMarginPercentage = c.Boolean(nullable: false),
                        ShowSetupCost = c.Boolean(nullable: false),
                        ShowCategory = c.Boolean(nullable: false),
                        ShowVendorCode = c.Boolean(nullable: false),
                        ShowManufacturer = c.Boolean(nullable: false),
                        ShowAccountCode = c.Boolean(nullable: false),
                        ShowPurchaseAccountCode = c.Boolean(nullable: false),
                        DefaultAllowClientEditLineItems = c.Boolean(nullable: false),
                        DefaultAllowClientEditSections = c.Boolean(nullable: false),
                        DefaultNeedsCheckedClientSubmission = c.Boolean(nullable: false),
                        DefaultHourlyRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        DefaultFromEmail = c.String(),
                        EmailTitleTemplate = c.String(),
                        EmailBodyTemplate = c.String(),
                        StripePaymentsOnNew = c.Boolean(nullable: false),
                        DefaultStripeDescription = c.String(),
                        DefaultStripeFixedAmount = c.Boolean(nullable: false),
                        DefaultStripePercentageToPay = c.Decimal(precision: 28, scale: 5),
                        DefaultStripeAmountToPay = c.Decimal(precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.DefaultQuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationSettingsID = c.Guid(nullable: false),
                        Email = c.String(),
                        QuotationSettings_CompanyID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationSettings", t => t.QuotationSettings_CompanyID)
                .Index(t => t.QuotationSettings_CompanyID);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OwnerID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .ForeignKey("dbo.Products", t => t.OwnerID, cascadeDelete: true)
                .Index(t => t.OwnerID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        DetailedDescription = c.String(),
                        Category = c.String(),
                        Unit = c.String(),
                        VendorCode = c.String(),
                        Manufacturer = c.String(),
                        ImageURL = c.String(),
                        AccountCode = c.String(),
                        PurchaseAccountCode = c.String(),
                        JsonCustomFieldValues = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        UsePriceBreaks = c.Boolean(nullable: false),
                        IsBought = c.Boolean(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MinimumQty = c.Int(nullable: false),
                        MaximumQty = c.Int(nullable: false),
                        EnableReference = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductCosts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 128),
                        Symbol = c.String(nullable: false, maxLength: 3),
                        UseInCommerce = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DefaultProductAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        Filename = c.String(),
                        ShowInPreview = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.ProductKitItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        KitID = c.Guid(nullable: false),
                        ProductID = c.Guid(),
                        IsComment = c.Boolean(nullable: false),
                        HideOnPdf = c.Boolean(nullable: false),
                        Description = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SortPos = c.Int(nullable: false),
                        Product_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.KitID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Products", t => t.Product_ID)
                .Index(t => t.KitID)
                .Index(t => t.ProductID)
                .Index(t => t.Product_ID);
            
            CreateTable(
                "dbo.ProductPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.QuotationSignedAgreements",
                c => new
                    {
                        QuotationID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        MD5Hash = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        DateSigned = c.DateTime(nullable: false),
                        DeviceInformation = c.String(),
                        IP_Address = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        SignatureImageURL = c.String(),
                        SignatureImageID = c.Guid(),
                        SignedUsingInput = c.Boolean(nullable: false),
                        PdfAgreementFilename = c.String(),
                        PdfAgreementURL = c.String(),
                        PdfAgreementID = c.Guid(),
                        PdfApprovedFilename = c.String(),
                        PdfApprovedURL = c.String(),
                        PdfApprovedID = c.Guid(),
                    })
                .PrimaryKey(t => t.QuotationID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.FileEntries", t => t.PdfAgreementID)
                .ForeignKey("dbo.FileEntries", t => t.PdfApprovedID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.FileEntries", t => t.SignatureImageID)
                .Index(t => t.QuotationID)
                .Index(t => t.CompanyID)
                .Index(t => t.SignatureImageID)
                .Index(t => t.PdfAgreementID)
                .Index(t => t.PdfApprovedID);
            
            CreateTable(
                "dbo.QuotationItemImages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OwnerID = c.Guid(nullable: false),
                        FileID = c.Guid(nullable: false),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .ForeignKey("dbo.QuotationItems", t => t.OwnerID, cascadeDelete: true)
                .Index(t => t.OwnerID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.QuotationItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        SectionID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Category = c.String(),
                        VendorCode = c.String(),
                        Manufacturer = c.String(),
                        Unit = c.String(),
                        AccountCode = c.String(),
                        PurchaseAccountCode = c.String(),
                        JsonCustomFieldValues = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SetupCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Markup = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ListPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ListCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MarginPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ImageURL = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                        HideOnPdf = c.Boolean(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationItems", t => t.ParentItemID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .ForeignKey("dbo.QuotationSections", t => t.SectionID)
                .Index(t => t.QuotationID)
                .Index(t => t.SectionID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.QuotationItemPriceBreaks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationItemID = c.Guid(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SetupCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Markup = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ListPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MarginPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationItems", t => t.QuotationItemID)
                .Index(t => t.QuotationItemID);
            
            CreateTable(
                "dbo.QuotationSections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Name = c.String(),
                        IncludeInTotal = c.Boolean(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        SortPos = c.Int(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Margin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalVat = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalDiscount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalMargin = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalNet = c.Decimal(nullable: false, precision: 28, scale: 5),
                        TotalPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(),
                        FileID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Filename = c.String(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.FileEntries", t => t.FileID)
                .Index(t => t.CompanyID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.QuotationPreviewItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ParentID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        TemplateID = c.Guid(),
                        AttachmentID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationAttachments", t => t.AttachmentID)
                .ForeignKey("dbo.Quotations", t => t.ParentID)
                .ForeignKey("dbo.QuotationTemplates", t => t.TemplateID)
                .Index(t => t.ParentID)
                .Index(t => t.TemplateID)
                .Index(t => t.AttachmentID);
            
            CreateTable(
                "dbo.ClientContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.QuotationComments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Email = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationCostItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        Category = c.String(),
                        VendorCode = c.String(),
                        Manufacturer = c.String(),
                        Unit = c.String(),
                        JsonCustomFieldValues = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SetupCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        IsKit = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        ParentCostItemID = c.Guid(),
                        SortPos = c.Int(nullable: false),
                        IsComment = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.QuotationCostItems", t => t.ParentCostItemID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID)
                .Index(t => t.ParentCostItemID);
            
            CreateTable(
                "dbo.QuotationLabourItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Description = c.String(),
                        Hours = c.Decimal(nullable: false, precision: 28, scale: 5),
                        HourlyRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Total = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationNotifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
            CreateTable(
                "dbo.QuotationStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuotationStripePayments",
                c => new
                    {
                        OwnerID = c.Guid(nullable: false),
                        Description = c.String(),
                        FixedAmount = c.Boolean(nullable: false),
                        PercentageToPay = c.Decimal(precision: 28, scale: 5),
                        AmountToPay = c.Decimal(precision: 28, scale: 5),
                        IsPaid = c.Boolean(nullable: false),
                        DateOfPayment = c.DateTime(),
                        StripeChargeID = c.String(),
                    })
                .PrimaryKey(t => t.OwnerID)
                .ForeignKey("dbo.Quotations", t => t.OwnerID)
                .Index(t => t.OwnerID);
            
            CreateTable(
                "dbo.ClientTagItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        ClientTagID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientTags", t => t.ClientTagID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientTagID);
            
            CreateTable(
                "dbo.ClientTags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ClientTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Opportunities",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        OpportunityTypeID = c.Guid(nullable: false),
                        StatusID = c.Guid(nullable: false),
                        SortPos = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Created = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        BarValue1 = c.Int(nullable: false),
                        BarValue2 = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Archived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorID)
                .ForeignKey("dbo.OpportunityStatus", t => t.StatusID)
                .ForeignKey("dbo.OpportunityTypes", t => t.OpportunityTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID)
                .Index(t => t.OpportunityTypeID)
                .Index(t => t.StatusID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.CreatorID);
            
            CreateTable(
                "dbo.OpportunityLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientName = c.String(),
                        OpportunityID = c.Guid(nullable: false),
                        OpportunityStatusID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Opportunities", t => t.OpportunityID)
                .ForeignKey("dbo.OpportunityStatus", t => t.OpportunityStatusID)
                .Index(t => t.CompanyID)
                .Index(t => t.OpportunityID)
                .Index(t => t.OpportunityStatusID);
            
            CreateTable(
                "dbo.OpportunityStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        OpportunityTypeID = c.Guid(nullable: false),
                        SortPos = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.OpportunityTypes", t => t.OpportunityTypeID)
                .Index(t => t.CompanyID)
                .Index(t => t.OpportunityTypeID);
            
            CreateTable(
                "dbo.OpportunityTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(),
                        LabelValue1 = c.String(),
                        LabelValue2 = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUserHelpTutorials",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DoNotDashboardStartVideo = c.Boolean(nullable: false),
                        DoNotProductsStartVideo = c.Boolean(nullable: false),
                        DoNotCRMStartVideo = c.Boolean(nullable: false),
                        DoNotQuotationsStartVideo = c.Boolean(nullable: false),
                        DoNotJobsStartVideo = c.Boolean(nullable: false),
                        DoNotInvoicesStartVideo = c.Boolean(nullable: false),
                        DoNotDeliveryNotesStartVideo = c.Boolean(nullable: false),
                        DoNotPurchaseOrdersStartVideo = c.Boolean(nullable: false),
                        DoNotStockControlStartVideo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.HierarchyMembers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                        ReportsToID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.HierarchyMembers", t => t.ReportsToID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CompanyID)
                .Index(t => t.UserId)
                .Index(t => t.ReportsToID);
            
            CreateTable(
                "dbo.HubSpotSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.InfusionsoftSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.InsightlySettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        ApiKey = c.String(),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.QuickBooksSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        QuickBooks = c.Boolean(nullable: false),
                        RefreshToken = c.String(),
                        ReamID = c.String(),
                        AccessToken = c.String(),
                        UseQBDocNumber = c.Boolean(nullable: false),
                        OrgName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SageExportSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        ExchangeRateGBP = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ExchangeRateUSD = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ExchangeRateEUR = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SalesforceSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                        IdURL = c.String(),
                        InstanceURL = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.StripeSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        TokenType = c.String(),
                        PublishableKey = c.String(),
                        Scope = c.String(),
                        LiveMode = c.Boolean(nullable: false),
                        UserID = c.String(),
                        RefreshToken = c.String(),
                        AccessToken = c.String(),
                        DoNotChargeFee = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionPlans",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Created = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Active = c.Boolean(nullable: false),
                        Trial = c.Boolean(nullable: false),
                        NumberCRM = c.Int(nullable: false),
                        NumberProducts = c.Int(nullable: false),
                        NumberQuotations = c.Int(nullable: false),
                        NumberJobs = c.Int(nullable: false),
                        NumberInvoices = c.Int(nullable: false),
                        NumberDeliveryNotes = c.Int(nullable: false),
                        NumberPurchaseOrders = c.Int(nullable: false),
                        NumberStockControl = c.Int(nullable: false),
                        EnableTeamManagement = c.Boolean(nullable: false),
                        FuturePayID = c.String(),
                        InternalCartID = c.Int(nullable: false),
                        CartID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.TeamMembers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TeamID = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Teams", t => t.TeamID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.TeamID)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ZohoSettings",
                c => new
                    {
                        CompanyID = c.Guid(nullable: false),
                        AccessToken = c.String(),
                        RefreshToken = c.String(),
                        OrgName = c.String(),
                        ApiDomain = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ModuleTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        IndexURL = c.String(),
                        EditURL = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CompanyEmails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Email = c.String(),
                        Username = c.String(),
                        Host = c.String(),
                        StoredPassword = c.String(),
                        Port = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                        Default = c.Boolean(nullable: false),
                        SuccessfulTest = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.CustomProductFields",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Label = c.String(nullable: false),
                        SortPos = c.Int(nullable: false),
                        ShowMainList = c.Boolean(nullable: false),
                        ShowQuotations = c.Boolean(nullable: false),
                        ShowJobs = c.Boolean(nullable: false),
                        ShowInvoices = c.Boolean(nullable: false),
                        ShowDeliveryNotes = c.Boolean(nullable: false),
                        ShowPurchaseOrders = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.CustomQuotationFields",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Label = c.String(nullable: false),
                        SortPos = c.Int(nullable: false),
                        ShowMainList = c.Boolean(nullable: false),
                        IsHidden = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.DefaultPdfTemplates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ModuleTypeID = c.Int(nullable: false),
                        FileName = c.String(nullable: false),
                        FilePath = c.String(),
                        ThumbnailPath = c.String(),
                        Type = c.Int(nullable: false),
                        Format = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ModuleTypes", t => t.ModuleTypeID)
                .Index(t => t.ModuleTypeID);
            
            CreateTable(
                "dbo.EmailSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        Host = c.String(),
                        Port = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.String(),
                        CompanyName = c.String(),
                        UserID = c.String(),
                        UserName = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        TargetSite = c.String(),
                        SourceError = c.String(),
                        StackTrace = c.String(),
                        URL = c.String(),
                        GetValues = c.String(),
                        PostValues = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.InternalLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Type = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        ActivityByUserName = c.String(),
                        ActivityByUserID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Labours",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        HourlyRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductLinks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        LinkedProductID = c.Guid(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.LinkedProductID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.LinkedProductID);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.StockLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        ProductCode = c.String(),
                        ProductDescription = c.String(),
                        LocationName = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        ActionType = c.Int(nullable: false),
                        Narrative = c.String(),
                        ActivityByUserID = c.String(maxLength: 128),
                        ModuleTypeID = c.Int(nullable: false),
                        LinkID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ActivityByUserID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID)
                .Index(t => t.ActivityByUserID);
            
            CreateTable(
                "dbo.SubscriptionCartItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        NumberCRM = c.Int(nullable: false),
                        NumberProducts = c.Int(nullable: false),
                        NumberQuotations = c.Int(nullable: false),
                        NumberJobs = c.Int(nullable: false),
                        NumberInvoices = c.Int(nullable: false),
                        NumberDeliveryNotes = c.Int(nullable: false),
                        NumberPurchaseOrders = c.Int(nullable: false),
                        NumberStockControl = c.Int(nullable: false),
                        EnableTeamManagement = c.Boolean(nullable: false),
                        InitialAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        RegularAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        DiscountAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionPlanPayments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Description = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Currency = c.String(maxLength: 3),
                        FuturePayID = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.SubscriptionRequests",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Email = c.String(nullable: false),
                        CompanyName = c.String(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetUserRoleOptions",
                c => new
                    {
                        IdentityRoleID = c.String(nullable: false, maxLength: 128),
                        CompanyID = c.Guid(),
                        ViewQuotations = c.Boolean(nullable: false),
                        EditQuotations = c.Boolean(nullable: false),
                        DeleteQuotations = c.Boolean(nullable: false),
                        ExportQuotations = c.Boolean(nullable: false),
                        ViewJobs = c.Boolean(nullable: false),
                        EditJobs = c.Boolean(nullable: false),
                        DeleteJobs = c.Boolean(nullable: false),
                        ExportJobs = c.Boolean(nullable: false),
                        ViewInvoices = c.Boolean(nullable: false),
                        EditInvoices = c.Boolean(nullable: false),
                        DeleteInvoices = c.Boolean(nullable: false),
                        ExportInvoices = c.Boolean(nullable: false),
                        ViewPurchaseOrders = c.Boolean(nullable: false),
                        EditPurchaseOrders = c.Boolean(nullable: false),
                        DeletePurchaseOrders = c.Boolean(nullable: false),
                        ExportPurchaseOrders = c.Boolean(nullable: false),
                        ViewDeliveryNotes = c.Boolean(nullable: false),
                        EditDeliveryNotes = c.Boolean(nullable: false),
                        DeleteDeliveryNotes = c.Boolean(nullable: false),
                        ExportDeliveryNotes = c.Boolean(nullable: false),
                        ViewStockControl = c.Boolean(nullable: false),
                        EditStockControl = c.Boolean(nullable: false),
                        ExportStockControl = c.Boolean(nullable: false),
                        ViewCRM = c.Boolean(nullable: false),
                        EditCRM = c.Boolean(nullable: false),
                        DeleteCRM = c.Boolean(nullable: false),
                        ExportCRM = c.Boolean(nullable: false),
                        ViewProducts = c.Boolean(nullable: false),
                        EditProducts = c.Boolean(nullable: false),
                        DeleteProducts = c.Boolean(nullable: false),
                        ExportProducts = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdentityRoleID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetRoles", t => t.IdentityRoleID)
                .Index(t => t.IdentityRoleID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.WorldPayResponses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        InstID = c.String(),
                        CartID = c.String(),
                        Desc = c.String(),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Amount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AmountString = c.String(),
                        Currency = c.String(maxLength: 3),
                        AuthMode = c.String(),
                        TestMode = c.String(),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        Region = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        CountryString = c.String(),
                        Tel = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        DelvName = c.String(),
                        DelvAddress1 = c.String(),
                        DelvAddress2 = c.String(),
                        DelvAddress3 = c.String(),
                        DelvTown = c.String(),
                        DelvRegion = c.String(),
                        DelvPostcode = c.String(),
                        DelvCountry = c.String(),
                        DelvCountryString = c.String(),
                        CompName = c.String(),
                        TransID = c.String(),
                        TransStatus = c.String(),
                        TransTime = c.String(),
                        AuthAmount = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        AuthCurrency = c.String(),
                        AuthAmountString = c.String(),
                        RawAuthMessage = c.String(),
                        RawAuthCode = c.String(),
                        CallbackPW = c.String(),
                        CardType = c.String(),
                        CountryMatch = c.String(),
                        AVS = c.String(),
                        WafMerchMessage = c.String(),
                        Authentication = c.String(),
                        IpAddress = c.String(),
                        Charenc = c.String(),
                        _spCharEnc = c.String(),
                        FuturePayID = c.String(),
                        FuturePayStatusChange = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoleOptions", "IdentityRoleID", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoleOptions", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionPlanPayments", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionCartItems", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StockLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StockLogs", "ActivityByUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ProductLinks", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductLinks", "LinkedProductID", "dbo.Products");
            DropForeignKey("dbo.ProductCategories", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Labours", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Labours", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InternalLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultPdfTemplates", "ModuleTypeID", "dbo.ModuleTypes");
            DropForeignKey("dbo.CustomQuotationFields", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.CustomProductFields", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.CompanyEmails", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ActivityLogs", "ModuleTypeID", "dbo.ModuleTypes");
            DropForeignKey("dbo.ZohoSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.TeamMembers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TeamMembers", "TeamID", "dbo.Teams");
            DropForeignKey("dbo.Teams", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SubscriptionPlans", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.StripeSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SalesforceSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.SageExportSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuickBooksSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InsightlySettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.InfusionsoftSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.HubSpotSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.HierarchyMembers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.HierarchyMembers", "ReportsToID", "dbo.HierarchyMembers");
            DropForeignKey("dbo.HierarchyMembers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Companies", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.AspNetUserHelpTutorials", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Opportunities", "OpportunityTypeID", "dbo.OpportunityTypes");
            DropForeignKey("dbo.OpportunityLogs", "OpportunityStatusID", "dbo.OpportunityStatus");
            DropForeignKey("dbo.OpportunityStatus", "OpportunityTypeID", "dbo.OpportunityTypes");
            DropForeignKey("dbo.OpportunityTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Opportunities", "StatusID", "dbo.OpportunityStatus");
            DropForeignKey("dbo.OpportunityStatus", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.OpportunityLogs", "OpportunityID", "dbo.Opportunities");
            DropForeignKey("dbo.OpportunityLogs", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientEvents", "OpportunityID", "dbo.Opportunities");
            DropForeignKey("dbo.Opportunities", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Opportunities", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Opportunities", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.Opportunities", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Opportunities", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Clients", "ClientTypeID", "dbo.ClientTypes");
            DropForeignKey("dbo.Clients", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTagItems", "ClientTagID", "dbo.ClientTags");
            DropForeignKey("dbo.ClientTags", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTagItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.QuotationStripePayments", "OwnerID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "StatusID", "dbo.QuotationStatus");
            DropForeignKey("dbo.QuotationNotifications", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationLabourItems", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "InvoiceAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Quotations", "DeliveryAddressID", "dbo.ClientAddresses");
            DropForeignKey("dbo.Quotations", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.QuotationCostItems", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationCostItems", "ParentCostItemID", "dbo.QuotationCostItems");
            DropForeignKey("dbo.Quotations", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationComments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.ClientEvents", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.ClientContacts", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Quotations", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.QuotationAttachments", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.Companies", "ScreenLogoImageID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationPreviewItems", "TemplateID", "dbo.QuotationTemplates");
            DropForeignKey("dbo.QuotationPreviewItems", "ParentID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationPreviewItems", "AttachmentID", "dbo.QuotationAttachments");
            DropForeignKey("dbo.QuotationTemplates", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationTemplates", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.QuotationSections", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItems", "SectionID", "dbo.QuotationSections");
            DropForeignKey("dbo.QuotationItems", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationItemPriceBreaks", "QuotationItemID", "dbo.QuotationItems");
            DropForeignKey("dbo.QuotationItems", "ParentItemID", "dbo.QuotationItems");
            DropForeignKey("dbo.QuotationItemImages", "OwnerID", "dbo.QuotationItems");
            DropForeignKey("dbo.QuotationItemImages", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationAttachments", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationSignedAgreements", "SignatureImageID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationSignedAgreements", "QuotationID", "dbo.Quotations");
            DropForeignKey("dbo.QuotationSignedAgreements", "PdfApprovedID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationSignedAgreements", "PdfAgreementID", "dbo.FileEntries");
            DropForeignKey("dbo.QuotationSignedAgreements", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductPrices", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPrices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductKitItems", "Product_ID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "KitID", "dbo.Products");
            DropForeignKey("dbo.ProductImages", "OwnerID", "dbo.Products");
            DropForeignKey("dbo.DefaultProductAttachments", "ProductID", "dbo.Products");
            DropForeignKey("dbo.DefaultProductAttachments", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.Products", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductCosts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductCosts", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Products", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductImages", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.Companies", "LogoImageID", "dbo.FileEntries");
            DropForeignKey("dbo.DefaultQuotationNotifications", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.DefaultQuotationAttachments", "QuotationSettings_CompanyID", "dbo.QuotationSettings");
            DropForeignKey("dbo.QuotationSettings", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.DefaultQuotationAttachments", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.ClientAttachments", "FileID", "dbo.FileEntries");
            DropForeignKey("dbo.ClientAttachments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Quotations", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientAddresses", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.CompanyAddresses", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ActivityLogs", "CompanyID", "dbo.Companies");
            DropIndex("dbo.AspNetUserRoleOptions", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUserRoleOptions", new[] { "IdentityRoleID" });
            DropIndex("dbo.SubscriptionPlanPayments", new[] { "CompanyID" });
            DropIndex("dbo.SubscriptionCartItems", new[] { "CompanyID" });
            DropIndex("dbo.StockLogs", new[] { "ActivityByUserID" });
            DropIndex("dbo.StockLogs", new[] { "CompanyID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ProductCategories", new[] { "CompanyID" });
            DropIndex("dbo.ProductLinks", new[] { "LinkedProductID" });
            DropIndex("dbo.ProductLinks", new[] { "ProductID" });
            DropIndex("dbo.Labours", new[] { "CurrencyID" });
            DropIndex("dbo.Labours", new[] { "CompanyID" });
            DropIndex("dbo.InternalLogs", new[] { "CompanyID" });
            DropIndex("dbo.DefaultPdfTemplates", new[] { "ModuleTypeID" });
            DropIndex("dbo.CustomQuotationFields", new[] { "CompanyID" });
            DropIndex("dbo.CustomProductFields", new[] { "CompanyID" });
            DropIndex("dbo.CompanyEmails", new[] { "CompanyID" });
            DropIndex("dbo.ZohoSettings", new[] { "CompanyID" });
            DropIndex("dbo.TeamMembers", new[] { "UserId" });
            DropIndex("dbo.TeamMembers", new[] { "TeamID" });
            DropIndex("dbo.Teams", new[] { "CompanyID" });
            DropIndex("dbo.SubscriptionPlans", new[] { "CompanyID" });
            DropIndex("dbo.StripeSettings", new[] { "CompanyID" });
            DropIndex("dbo.SalesforceSettings", new[] { "CompanyID" });
            DropIndex("dbo.SageExportSettings", new[] { "CompanyID" });
            DropIndex("dbo.QuickBooksSettings", new[] { "CompanyID" });
            DropIndex("dbo.InsightlySettings", new[] { "CompanyID" });
            DropIndex("dbo.InfusionsoftSettings", new[] { "CompanyID" });
            DropIndex("dbo.HubSpotSettings", new[] { "CompanyID" });
            DropIndex("dbo.HierarchyMembers", new[] { "ReportsToID" });
            DropIndex("dbo.HierarchyMembers", new[] { "UserId" });
            DropIndex("dbo.HierarchyMembers", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUserHelpTutorials", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.OpportunityTypes", new[] { "CompanyID" });
            DropIndex("dbo.OpportunityStatus", new[] { "OpportunityTypeID" });
            DropIndex("dbo.OpportunityStatus", new[] { "CompanyID" });
            DropIndex("dbo.OpportunityLogs", new[] { "OpportunityStatusID" });
            DropIndex("dbo.OpportunityLogs", new[] { "OpportunityID" });
            DropIndex("dbo.OpportunityLogs", new[] { "CompanyID" });
            DropIndex("dbo.Opportunities", new[] { "CreatorID" });
            DropIndex("dbo.Opportunities", new[] { "AssignedUserID" });
            DropIndex("dbo.Opportunities", new[] { "StatusID" });
            DropIndex("dbo.Opportunities", new[] { "OpportunityTypeID" });
            DropIndex("dbo.Opportunities", new[] { "ClientContactID" });
            DropIndex("dbo.Opportunities", new[] { "ClientID" });
            DropIndex("dbo.Opportunities", new[] { "CompanyID" });
            DropIndex("dbo.ClientTypes", new[] { "CompanyID" });
            DropIndex("dbo.ClientTags", new[] { "CompanyID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientTagID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientID" });
            DropIndex("dbo.QuotationStripePayments", new[] { "OwnerID" });
            DropIndex("dbo.QuotationNotifications", new[] { "QuotationID" });
            DropIndex("dbo.QuotationLabourItems", new[] { "QuotationID" });
            DropIndex("dbo.QuotationCostItems", new[] { "ParentCostItemID" });
            DropIndex("dbo.QuotationCostItems", new[] { "QuotationID" });
            DropIndex("dbo.QuotationComments", new[] { "QuotationID" });
            DropIndex("dbo.ClientContacts", new[] { "ClientID" });
            DropIndex("dbo.QuotationPreviewItems", new[] { "AttachmentID" });
            DropIndex("dbo.QuotationPreviewItems", new[] { "TemplateID" });
            DropIndex("dbo.QuotationPreviewItems", new[] { "ParentID" });
            DropIndex("dbo.QuotationTemplates", new[] { "FileID" });
            DropIndex("dbo.QuotationTemplates", new[] { "CompanyID" });
            DropIndex("dbo.QuotationSections", new[] { "QuotationID" });
            DropIndex("dbo.QuotationItemPriceBreaks", new[] { "QuotationItemID" });
            DropIndex("dbo.QuotationItems", new[] { "ParentItemID" });
            DropIndex("dbo.QuotationItems", new[] { "SectionID" });
            DropIndex("dbo.QuotationItems", new[] { "QuotationID" });
            DropIndex("dbo.QuotationItemImages", new[] { "FileID" });
            DropIndex("dbo.QuotationItemImages", new[] { "OwnerID" });
            DropIndex("dbo.QuotationSignedAgreements", new[] { "PdfApprovedID" });
            DropIndex("dbo.QuotationSignedAgreements", new[] { "PdfAgreementID" });
            DropIndex("dbo.QuotationSignedAgreements", new[] { "SignatureImageID" });
            DropIndex("dbo.QuotationSignedAgreements", new[] { "CompanyID" });
            DropIndex("dbo.QuotationSignedAgreements", new[] { "QuotationID" });
            DropIndex("dbo.ProductPrices", new[] { "CurrencyID" });
            DropIndex("dbo.ProductPrices", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "Product_ID" });
            DropIndex("dbo.ProductKitItems", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "KitID" });
            DropIndex("dbo.DefaultProductAttachments", new[] { "FileID" });
            DropIndex("dbo.DefaultProductAttachments", new[] { "ProductID" });
            DropIndex("dbo.ProductCosts", new[] { "CurrencyID" });
            DropIndex("dbo.ProductCosts", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "CurrencyID" });
            DropIndex("dbo.Products", new[] { "CompanyID" });
            DropIndex("dbo.ProductImages", new[] { "FileID" });
            DropIndex("dbo.ProductImages", new[] { "OwnerID" });
            DropIndex("dbo.DefaultQuotationNotifications", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.QuotationSettings", new[] { "CompanyID" });
            DropIndex("dbo.DefaultQuotationAttachments", new[] { "QuotationSettings_CompanyID" });
            DropIndex("dbo.DefaultQuotationAttachments", new[] { "FileID" });
            DropIndex("dbo.ClientAttachments", new[] { "FileID" });
            DropIndex("dbo.ClientAttachments", new[] { "ClientID" });
            DropIndex("dbo.QuotationAttachments", new[] { "FileID" });
            DropIndex("dbo.QuotationAttachments", new[] { "QuotationID" });
            DropIndex("dbo.Quotations", new[] { "DeliveryAddressID" });
            DropIndex("dbo.Quotations", new[] { "InvoiceAddressID" });
            DropIndex("dbo.Quotations", new[] { "CurrencyID" });
            DropIndex("dbo.Quotations", new[] { "StatusID" });
            DropIndex("dbo.Quotations", new[] { "AssignedUserID" });
            DropIndex("dbo.Quotations", new[] { "ClientContactID" });
            DropIndex("dbo.Quotations", new[] { "ClientID" });
            DropIndex("dbo.Quotations", new[] { "CompanyID" });
            DropIndex("dbo.ClientAddresses", new[] { "ClientID" });
            DropIndex("dbo.Clients", new[] { "ClientTypeID" });
            DropIndex("dbo.Clients", new[] { "CompanyID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientContactID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientID" });
            DropIndex("dbo.ClientEvents", new[] { "OpportunityID" });
            DropIndex("dbo.ClientEvents", new[] { "CreatorID" });
            DropIndex("dbo.ClientEvents", new[] { "AssignedUserID" });
            DropIndex("dbo.ClientEvents", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "CompanyID" });
            DropIndex("dbo.CompanyAddresses", new[] { "CompanyID" });
            DropIndex("dbo.Companies", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.Companies", new[] { "ScreenLogoImageID" });
            DropIndex("dbo.Companies", new[] { "LogoImageID" });
            DropIndex("dbo.ActivityLogs", new[] { "ModuleTypeID" });
            DropIndex("dbo.ActivityLogs", new[] { "CompanyID" });
            DropTable("dbo.WorldPayResponses");
            DropTable("dbo.AspNetUserRoleOptions");
            DropTable("dbo.SubscriptionRequests");
            DropTable("dbo.SubscriptionPlanPayments");
            DropTable("dbo.SubscriptionCartItems");
            DropTable("dbo.StockLogs");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.ProductLinks");
            DropTable("dbo.Labours");
            DropTable("dbo.InternalLogs");
            DropTable("dbo.ErrorLogs");
            DropTable("dbo.EmailSettings");
            DropTable("dbo.DefaultPdfTemplates");
            DropTable("dbo.CustomQuotationFields");
            DropTable("dbo.CustomProductFields");
            DropTable("dbo.CompanyEmails");
            DropTable("dbo.ModuleTypes");
            DropTable("dbo.ZohoSettings");
            DropTable("dbo.TeamMembers");
            DropTable("dbo.Teams");
            DropTable("dbo.SubscriptionPlans");
            DropTable("dbo.StripeSettings");
            DropTable("dbo.SalesforceSettings");
            DropTable("dbo.SageExportSettings");
            DropTable("dbo.QuickBooksSettings");
            DropTable("dbo.InsightlySettings");
            DropTable("dbo.InfusionsoftSettings");
            DropTable("dbo.HubSpotSettings");
            DropTable("dbo.HierarchyMembers");
            DropTable("dbo.AspNetUserHelpTutorials");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.OpportunityTypes");
            DropTable("dbo.OpportunityStatus");
            DropTable("dbo.OpportunityLogs");
            DropTable("dbo.Opportunities");
            DropTable("dbo.ClientTypes");
            DropTable("dbo.ClientTags");
            DropTable("dbo.ClientTagItems");
            DropTable("dbo.QuotationStripePayments");
            DropTable("dbo.QuotationStatus");
            DropTable("dbo.QuotationNotifications");
            DropTable("dbo.QuotationLabourItems");
            DropTable("dbo.QuotationCostItems");
            DropTable("dbo.QuotationComments");
            DropTable("dbo.ClientContacts");
            DropTable("dbo.QuotationPreviewItems");
            DropTable("dbo.QuotationTemplates");
            DropTable("dbo.QuotationSections");
            DropTable("dbo.QuotationItemPriceBreaks");
            DropTable("dbo.QuotationItems");
            DropTable("dbo.QuotationItemImages");
            DropTable("dbo.QuotationSignedAgreements");
            DropTable("dbo.ProductPrices");
            DropTable("dbo.ProductKitItems");
            DropTable("dbo.DefaultProductAttachments");
            DropTable("dbo.Currencies");
            DropTable("dbo.ProductCosts");
            DropTable("dbo.Products");
            DropTable("dbo.ProductImages");
            DropTable("dbo.DefaultQuotationNotifications");
            DropTable("dbo.QuotationSettings");
            DropTable("dbo.DefaultQuotationAttachments");
            DropTable("dbo.ClientAttachments");
            DropTable("dbo.FileEntries");
            DropTable("dbo.QuotationAttachments");
            DropTable("dbo.Quotations");
            DropTable("dbo.ClientAddresses");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientEvents");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.CompanyAddresses");
            DropTable("dbo.Companies");
            DropTable("dbo.ActivityLogs");
        }
    }
}
