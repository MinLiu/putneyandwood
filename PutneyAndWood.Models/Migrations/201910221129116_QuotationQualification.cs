namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuotationQualification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuotationQualifications", "QuotationItemID", c => c.Guid());
            CreateIndex("dbo.QuotationQualifications", "QuotationItemID");
            AddForeignKey("dbo.QuotationQualifications", "QuotationItemID", "dbo.QuotationItems", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuotationQualifications", "QuotationItemID", "dbo.QuotationItems");
            DropIndex("dbo.QuotationQualifications", new[] { "QuotationItemID" });
            DropColumn("dbo.QuotationQualifications", "QuotationItemID");
        }
    }
}
