﻿namespace SnapSuite.Models.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SnapSuite.Models.SnapDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SnapSuite.Models.SnapDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

            context.Currencies.AddOrUpdate(
                new Currency { ID = 1, Code = "GBP", Name = "British Pound", Symbol = "£", UseInCommerce = true }
                //new currency { id = 2, code = "eur", name = "euro", symbol = "€", useincommerce = true },
                //new currency { id = 3, code = "usd", name = "united states dollar", symbol = "$", useincommerce = true }
                //new Currency { ID = 4, Code = "AUD", Name = "Australian Dollar", Symbol = "$", UseInCommerce = true },
                //new Currency { ID = 5, Code = "CAD", Name = "Canadian Dollar", Symbol = "$", UseInCommerce = true },
                //new Currency { ID = 6, Code = "NZD", Name = "New Zealand Dollar", Symbol = "$", UseInCommerce = true },
                //new Currency { ID = 8, Code = "ZAR", Name = "South African Rand", Symbol = "R", UseInCommerce = true },
                //new currency { id = 9, code = "cny", name = "chinese yuan", symbol = "¥" },
                //new currency { id = 10, code = "czk", name = "czech koruna", symbol = "kč" },
                //new currency { id = 11, code = "dkk", name = "danish krone", symbol = "kr." },
                //new currency { id = 12, code = "hkd", name = "hong kong dollar", symbol = "$" },
                //new currency { id = 13, code = "huf", name = "hungarian forint", symbol = "ft" },
                //new currency { id = 14, code = "inr", name = "indian rupee", symbol = "₹" },
                //new currency { id = 15, code = "ils", name = "israeli shekel", symbol = "₪" },
                //new currency { id = 16, code = "jpy", name = "japanese yen", symbol = "¥" },
                //new currency { id = 17, code = "myr", name = "malaysian ringgit", symbol = "rm" },
                //new currency { id = 18, code = "nok", name = "norwegian krone", symbol = "kr" },
                //new currency { id = 19, code = "pln", name = "polish zloty", symbol = "zł" },
                //new currency { id = 20, code = "rub", name = "russian ruble", symbol = "₽" },
                //new currency { id = 21, code = "sar", name = "saudi riyal", symbol = "ر.س" },
                //new currency { id = 22, code = "sgd", name = "singapore dollar", symbol = "$" },
                //new currency { id = 23, code = "krw", name = "south korean won", symbol = "₩" },
                //new currency { id = 24, code = "sek", name = "swedish krona", symbol = "kr" },
                //new currency { id = 25, code = "chf", name = "swiss franc", symbol = "fr" },
                //new currency { id = 26, code = "twd", name = "taiwan dollar", symbol = "$" },
                //new currency { id = 27, code = "thb", name = "thai baht", symbol = "	฿" },
                //new currency { id = 28, code = "try", name = "turkish lira", symbol = "₺" }
            );
            context.SaveChanges();


            //Add roles            
            context.Roles.AddOrUpdate(
                new IdentityRole { Id = "-1", Name = "Super Admin" },
                new IdentityRole { Id = "1", Name = "Admin" },
                new IdentityRole { Id = "2", Name = "User" }
            );

            context.UserRoleOptions.AddOrUpdate(
                new UserRoleOption { IdentityRoleID = "-1", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true },
                new UserRoleOption { IdentityRoleID = "1", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true },
                new UserRoleOption { IdentityRoleID = "2", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true }
            );
            context.SaveChanges();



            //Add Company     

            if (context.Companies.Count() <= 0)
            {
                context.Companies.AddOrUpdate(
                    new Company
                    {
                        ID = Guid.Parse("bff01aac-69fe-e611-bf05-e03f495631f0"),
                        Name = "Super User Company",
                        Email = "developer@fruitfulgroup.com",
                        SignupDate = DateTime.Now,
                        AccountClosed = false,
                        DefaultCurrencyID = 1,
                    }
                );
                context.SaveChanges();
            }

            var companyGiud = context.Companies.First().ID;

            //Add Default Users Passwords : "Dev123!"
            if (context.Users.Where(u => u.Id == "abe7ab8c-0063-48c2-9130-060ffc8169e1").LongCount() <= 0)
            {
                context.Users.Add(
                    new User
                    {
                        Id = "abe7ab8c-0063-48c2-9130-060ffc8169e1",
                        Email = "developer@fruitfulgroup.com",
                        UserName = "developer@fruitfulgroup.com",
                        PasswordHash = "AIh69MUSNlJKo1QGyT2tC1OSZK8NX3VM/x3gd6LLIRbhHO3WVNF+8FqVjv3XKnPN1A==",
                        SecurityStamp = "b75b58c7-4318-4cbf-99c7-8429856c9d0b",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        CompanyID = companyGiud,
                        Roles = { new IdentityUserRole { UserId = "abe7ab8c-0063-48c2-9130-060ffc8169e1", RoleId = "-1" } },
                        AccessCRM = true,
                        AccessDeliveryNotes = true,
                        AccessInvoices = true,
                        AccessProducts = true,
                        AccessQuotations = true,
                        AccessJobs = true,
                    }
                );
            }

            context.SaveChanges();



            context.ModuleTypes.AddOrUpdate(
                new ModuleType { ID = ModuleTypeValues.PRODUCTS, Name = "Products", Code = "PDTS", IndexURL = "/Products/", EditURL = "/Products/Index/" },
                new ModuleType { ID = ModuleTypeValues.CLIENTS, Name = "Clients", Code = "CLTS", IndexURL = "/Clients/", EditURL = "/Clients/Index/" },

                new ModuleType { ID = ModuleTypeValues.QUOTATIONS, Name = "Quotations", Code = "QS", IndexURL = "/Quotations/", EditURL = "/Quotations/Edit/" }
                //new ModuleType { ID = ModuleTypeValues.SUPPLIERS, Name = "Suppliers", Code = "SPLR", IndexURL = "/Suppliers/", EditURL = "/Suppliers/Index/" },
                //new ModuleType { ID = ModuleTypeValues.JOBS, Name = "Jobs", Code = "JOBS", IndexURL = "/Jobs/", EditURL = "/Jobs/Edit/" },
                //new ModuleType { ID = ModuleTypeValues.DELIVERY_NOTES, Name = "Delivery Notes", Code = "DEL", IndexURL = "/DeliveryNotes/", EditURL = "/DeliveryNotes/Edit/" },
                //new ModuleType { ID = ModuleTypeValues.INVOICES, Name = "Invoices", Code = "INV", IndexURL = "/Invoices/", EditURL = "/Invoices/Edit/" },
                //new ModuleType { ID = ModuleTypeValues.PURCHASE_ORDERS, Name = "Purchase Orders", Code = "POS", IndexURL = "/PurchaseOrders/", EditURL = "/PurchaseOrders/Edit/" },
                //new ModuleType { ID = ModuleTypeValues.STOCK_CONTROL, Name = "Stock Control", Code = "SC", IndexURL = "/StockControl/", EditURL = "/StockControl/" }
            );

            context.ProjectStatuses.AddOrUpdate(
                new ProjectStatus { ID = ProjectStatusValues.Declined, Name = "Declined", Description = "Declined", HexColour = "#34495e", SortPos = 1 },
                new ProjectStatus { ID = ProjectStatusValues.PreTender, Name = "Pre-Tender", Description = "Pre-Tender", HexColour = "#217dbb", SortPos = 2 },
                new ProjectStatus { ID = ProjectStatusValues.OutToTender, Name = "Out to tender", Description = "Out to tender", HexColour = "#2ecc71", SortPos = 3 },
                new ProjectStatus { ID = ProjectStatusValues.MainContractAwarded, Name = "Main contract awarded", Description = "Main contract awarded", HexColour = "#e74c3c", SortPos = 4 },
                new ProjectStatus { ID = ProjectStatusValues.OnHold, Name = "On hold", Description = "On hold", HexColour = "#ffcc00", SortPos = 5 },
                new ProjectStatus { ID = ProjectStatusValues.Dead, Name = "Dead", Description = "Dead", HexColour = "#ff6600", SortPos = 6 },
                new ProjectStatus { ID = ProjectStatusValues.Won, Name = "Won", Description = "Won", HexColour = "#5a417c", SortPos = 7 },
                new ProjectStatus { ID = ProjectStatusValues.Lost, Name = "Lost", Description = "Lost", HexColour = "#094f08", SortPos = 8 }
            );

            context.QuotationStatuses.AddOrUpdate(
                new QuotationStatus { ID = QuotationStatusValues.DRAFT, Name = "Draft", Description = "Draft", HexColour = "#34495e", SortPos = 1 },
                new QuotationStatus { ID = QuotationStatusValues.SENT, Name = "Sent", Description = "Sent", HexColour = "#217dbb", SortPos = 2 },
                new QuotationStatus { ID = QuotationStatusValues.ACCEPTED, Name = "Project Won", Description = "Project Won", HexColour = "#2ecc71", SortPos = 3 },
                new QuotationStatus { ID = QuotationStatusValues.DECLINED, Name = "Project Lost", Description = "Project Lost", HexColour = "#e74c3c", SortPos = 4 },
                new QuotationStatus { ID = QuotationStatusValues.DEPRECATED, Name = "Delete", Description = "Delete", HexColour = "#ffcc00", SortPos = 5 }
                //new QuotationStatus { ID = QuotationStatusValues.COMMENCED, Name = "Commenced", Description = "Commenced", HexColour = "#ff6600", SortPos = 6 },
                //new QuotationStatus { ID = QuotationStatusValues.COMPLETE, Name = "Completed", Description = "Completed", HexColour = "#5a417c", SortPos = 7 }
            );

            if (context.EmailSettings.Count() <= 0)
            {
                context.EmailSettings.AddOrUpdate(
                    new Fruitful.Email.EmailSetting
                    {
                        Email = "noreply@quikflw.com",
                        Password = "dskj29nA1$",
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSSL = true
                    }
                );
            }

            context.EnquirySources.AddOrUpdate(
                new EnquirySource { ID = 1, Description = "Existing customer enquiry", SortPos = 0 },
                new EnquirySource { ID = 2, Description = "Potential customer enquiry - Target", SortPos = 1 },
                new EnquirySource { ID = 3, Description = "Potential customer enquiry - Unsolicited", SortPos = 2 },
                new EnquirySource { ID = 4, Description = "Misc. email enquiry", SortPos = 3 },
                new EnquirySource { ID = 5, Description = "Other enquiry", SortPos = 4 },
                new EnquirySource { ID = 6, Description = "ABI/Glenigan lead", SortPos = 5 },
                new EnquirySource { ID = 7, Description = "Colleague lead", SortPos = 6 },
                new EnquirySource { ID = 8, Description = "Customer lead", SortPos = 7 },
                new EnquirySource { ID = 9, Description = "Supplier lead", SortPos = 8 },
                new EnquirySource { ID = 10, Description = "Other lead", SortPos = 9 }
            );

            context.SaveChanges();


            //context.QuotationTemplates.AddOrUpdate(
            //new QuotationTemplate { ID = Guid.Parse("00000000-0000-0000-0000-000000000001"), CompanyID = null, Filename = "Default", Format = TemplateFormat.Docx, Type = TemplateType.Body, FilePath = "/Templates/Quotations/QuotationTemplate1 - Default.docx" },
            //new QuotationTemplate { ID = Guid.Parse("00000000-0000-0000-0000-000000000002"), CompanyID = null, Filename = "Blue", Format = TemplateFormat.Docx, Type = TemplateType.Body, FilePath = "/Templates/Quotations/QuotationTemplate2 - Blue.docx" },
            //new QuotationTemplate { ID = Guid.Parse("00000000-0000-0000-0000-000000000003"), CompanyID = null, Filename = "Default - No Photo", Format = TemplateFormat.Docx, Type = TemplateType.Body, FilePath = "/Templates/Quotations/QuotationTemplate1 - Default - No Photo.docx" },
            //new QuotationTemplate { ID = Guid.Parse("00000000-0000-0000-0000-000000000004"), CompanyID = null, Filename = "Blue - No Photo", Format = TemplateFormat.Docx, Type = TemplateType.Body, FilePath = "/Templates/Quotations/QuotationTemplate2 - Blue - No Photo.docx" },
            //new QuotationTemplate { ID = Guid.Parse("00000000-0000-0000-0000-000000000005"), CompanyID = null, Filename = "Product Specifications", Format = TemplateFormat.Docx, Type = TemplateType.Back, FilePath = "/Templates/All/Sample Back Template 1.docx" }
            //);

            //context.DefaultPdfTemplates.AddOrUpdate(
            //    new DefaultPdfTemplate { ID = Guid.NewGuid(), FileName = "Audio Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, FilePath = "/Templates/Quotations/Audio Front Cover.docx", ModuleTypeID = ModuleTypeValues.QUOTATIONS, ThumbnailPath = "/Templates/Quotations/Audio Front Cover.png" }
            //);
        }
    }
}
