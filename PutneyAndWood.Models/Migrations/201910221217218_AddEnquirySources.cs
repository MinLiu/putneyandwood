namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEnquirySources : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnquirySources",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Projects", "EnquirySourceID", c => c.Int());
            CreateIndex("dbo.Projects", "EnquirySourceID");
            AddForeignKey("dbo.Projects", "EnquirySourceID", "dbo.EnquirySources", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "EnquirySourceID", "dbo.EnquirySources");
            DropIndex("dbo.Projects", new[] { "EnquirySourceID" });
            DropColumn("dbo.Projects", "EnquirySourceID");
            DropTable("dbo.EnquirySources");
        }
    }
}
