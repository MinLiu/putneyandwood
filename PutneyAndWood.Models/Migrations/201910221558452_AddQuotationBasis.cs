namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuotationBasis : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuotationBasisOfEstimates",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        QuotationID = c.Guid(nullable: false),
                        Description = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Quotations", t => t.QuotationID)
                .Index(t => t.QuotationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuotationBasisOfEstimates", "QuotationID", "dbo.Quotations");
            DropIndex("dbo.QuotationBasisOfEstimates", new[] { "QuotationID" });
            DropTable("dbo.QuotationBasisOfEstimates");
        }
    }
}
