namespace SnapSuite.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductQualifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductQualifications",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Description = c.String(),
                        SortPos = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Company_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.Company_ID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.Company_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductQualifications", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductQualifications", "Company_ID", "dbo.Companies");
            DropIndex("dbo.ProductQualifications", new[] { "Company_ID" });
            DropIndex("dbo.ProductQualifications", new[] { "ProductID" });
            DropTable("dbo.ProductQualifications");
        }
    }
}
