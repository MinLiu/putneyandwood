﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Used in the generation of Word Document images
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace SnapSuite.Integrations.Salesforce
{
    public class SalesforceApiHelper
    {
        private static string ClientID = ConfigurationManager.AppSettings["SalesforceClientID"];
        private static string ClientSecret = ConfigurationManager.AppSettings["SalesforceClientSecret"];
        private static string RedirectUrl = ConfigurationManager.AppSettings["SalesforceCallBackUrl"];
        private static string TokenEndPoint = "https://login.salesforce.com/services/oauth2/token";
        private static string RevokeEndPoint = "https://login.salesforce.com/services/oauth2/revoke";
        private static string ApiVersion = "/services/data/v42.0";
        private static string QueryEndPoint = ApiVersion + "/query";
        private static string AccountsEndPoint = ApiVersion + "/sobjects/Account";
        private static string ContactsEndPoint = ApiVersion + "/sobjects/Contact";

        public SalesforceApiHelper()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        public async Task<SalesforceTokenResponse> GetAccessTokenByCode(string code)
        {
            var client = new HttpClient();

            var para = new Dictionary<string, string>();
            para.Add("code", code);
            para.Add("redirect_uri", RedirectUrl);
            para.Add("client_id", ClientID);
            para.Add("client_secret", ClientSecret);
            para.Add("grant_type", "authorization_code");

            var paraList = new List<string>();
            foreach (var item in para)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint + "?" + String.Join("&", paraList));
            var response = await client.SendAsync(request);
            var tokenResponse = new SalesforceTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<SalesforceTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<SalesforceTokenResponse> RefreshAccessToken(string refreshToken)
        {
            var client = new HttpClient();

            var data = new Dictionary<string, string>();
            data.Add("grant_type", "refresh_token");
            data.Add("client_id", ClientID);
            data.Add("client_secret", ClientSecret);
            data.Add("refresh_token", refreshToken);

            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint + "?" + String.Join("&", paraList));
            var response = await client.SendAsync(request);
            var tokenResponse = new SalesforceTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<SalesforceTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }
        public async Task<HttpResponseMessage> GetAllAccounts(string accessToken, string instanceURL, string nextRecordsURL)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            if (!string.IsNullOrEmpty(nextRecordsURL))
            {
                return await client.GetAsync(instanceURL + nextRecordsURL).ConfigureAwait(false);
            }

            var data = new Dictionary<string, string>();
            data.Add("q", "SELECT Name,Phone,Id,BillingAddress,ShippingAddress,Description,Website from Account");
            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var response = await client.GetAsync(instanceURL + QueryEndPoint + "?" + String.Join("&", paraList)).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> GetAllContacts(string accessToken, string instanceURL, string nextRecordsURL)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            if (!string.IsNullOrEmpty(nextRecordsURL))
            {
                return await client.GetAsync(instanceURL + nextRecordsURL).ConfigureAwait(false);
            }

            var data = new Dictionary<string, string>();
            data.Add("q", "SELECT name,IsDeleted,AccountId,LastName,FirstName,Salutation,OtherAddress,MailingAddress,Phone,Fax,MobilePhone,HomePhone,OtherPhone,AssistantPhone,Email,Title,Department,Birthdate,Description,PhotoUrl from Contact");
            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var response = await client.GetAsync(instanceURL + QueryEndPoint + "?" + String.Join("&", paraList)).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> RevokeToken(string refreshToken)
        {
            var client = new HttpClient();
            var response = await client.PostAsync(RevokeEndPoint + "?token=" + refreshToken, null).ConfigureAwait(false);
            return response;
        }
    }

}
