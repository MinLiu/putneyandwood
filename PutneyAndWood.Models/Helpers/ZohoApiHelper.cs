﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Used in the generation of Word Document images
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace SnapSuite.Integrations.Zoho
{
    public class ZohoApiHelper
    {
        private static string ClientID = ConfigurationManager.AppSettings["ZohoClientID"];
        private static string ClientSecret_US = ConfigurationManager.AppSettings["ZohoClientSecret_US"];
        private static string ClientSecret_EU = ConfigurationManager.AppSettings["ZohoClientSecret_EU"];
        private static string RedirectUrl = ConfigurationManager.AppSettings["ZohoCallBackUrl"];
        private static string TokenEndPoint_US = "https://accounts.zoho.com/oauth/v2/token";
        private static string TokenEndPoint_EU = "https://accounts.zoho.eu/oauth/v2/token";
        private static string AccountsEndPoint = "/crm/v2/accounts";
        private static string ContactsEndPoint = "/crm/v2/contacts";

        public ZohoApiHelper()
        {
        }

        public async Task<ZohoTokenResponse> GetAccessTokenByCode(string code, string location = "eu")
        {
            var client = new HttpClient();

            var para = new Dictionary<string, string>();
            para.Add("code", code);
            para.Add("redirect_uri", RedirectUrl);
            para.Add("client_id", ClientID);
            para.Add("client_secret", location == "eu"? ClientSecret_EU : ClientSecret_US);
            para.Add("grant_type", "authorization_code");

            var paraList = new List<string>();
            foreach (var item in para)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var request = new HttpRequestMessage(HttpMethod.Post, (location == "eu"? TokenEndPoint_EU : TokenEndPoint_US) + "?" + String.Join("&", paraList));
            var response = await client.SendAsync(request);
            var tokenResponse = new ZohoTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<ZohoTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<ZohoTokenResponse> RefreshAccessToken(string refreshToken, string location = "eu")
        {
            var client = new HttpClient();

            var data = new Dictionary<string, string>();
            data.Add("grant_type", "refresh_token");
            data.Add("client_id", ClientID);
            data.Add("client_secret", location == "eu" ? ClientSecret_EU : ClientSecret_US);
            data.Add("redirect_uri", RedirectUrl);
            data.Add("refresh_token", refreshToken);

            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var request = new HttpRequestMessage(HttpMethod.Post, (location == "eu"? TokenEndPoint_EU : TokenEndPoint_US) + "?" + String.Join("&", paraList));
            var response = await client.SendAsync(request);
            var tokenResponse = new ZohoTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<ZohoTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }
        public async Task<HttpResponseMessage> GetAllAccounts(string accessToken, string apiDomain, int page = 1)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Zoho-oauthtoken " + accessToken);

            var data = new Dictionary<string, string>();
            data.Add("page", page.ToString());

            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var response = await client.GetAsync(apiDomain + AccountsEndPoint + "?" + String.Join("&", paraList)).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> GetAllContacts(string accessToken, string apiDomain, int page = 1)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Zoho-oauthtoken " + accessToken);

            var data = new Dictionary<string, string>();
            data.Add("page", page.ToString());

            var paraList = new List<string>();
            foreach (var item in data)
            {
                paraList.Add(String.Format("{0}={1}", item.Key, HttpUtility.UrlEncode(item.Value)));
            }

            var response = await client.GetAsync(apiDomain + ContactsEndPoint + "?" + String.Join("&", paraList)).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> RevokeToken(string refreshToken, string location = "eu")
        {
            var client = new HttpClient();
            var response = await client.PostAsync((location == "eu"? TokenEndPoint_EU : TokenEndPoint_US) + "/revoke?token=" + refreshToken, null).ConfigureAwait(false);
            return response;
        }
    }

}
