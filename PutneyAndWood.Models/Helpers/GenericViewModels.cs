﻿using Newtonsoft.Json;
using SnapSuite.Internal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace SnapSuite.Models
{
    public class SelectItemViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class SelectPreviewItemViewModel
    {
        public Guid OrderID { get; set; }
        public string Name { get; set; }
        public Guid? TemplateAttachmentID { get; set; }
        public PreviewItemType Type { get; set; }
    }

    public class UserItemViewModel
    {
        public string ID { get; set; }
        public string Email { get; set; }
    }

    public enum TemplateType { Front, Body, Back }
    public enum TemplateFormat { Pdf, Docx }

    public enum MoveDirection { Top, Bottom, Up, Down }

    public class LineItemInfoViewModel
    {
        public string Description { get; set; }
        //public string DescriptionShort { get; set; }
        public string Category { get; set; }
        //public string VendorCode { get; set; }
        //public string Manufacturer { get; set; }
        public List<CustomField> CustomFieldValues { get; set; }
    }


    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static int WeekNumber(this DateTime dt, DayOfWeek startOfWeek)
        {
            CultureInfo culInfo = new CultureInfo("en-GB");
            Calendar cal = culInfo.Calendar;

            return cal.GetWeekOfYear(dt, CalendarWeekRule.FirstFullWeek, startOfWeek);
        }
    }

    public enum DateType
    {
        AllTime = 0,
        CurrentMonth = 1,
        LastMonth = 2,
        CustomDate = 3,
        CurrentWeek = 4, 
        LastWeek = 5
    }

    public class DataFileOutput
    {
        public byte[] DataArray;
        public string MimeType;
        public string Filename;
    }

    public static class DateTypeExtensions
    {
        public static void GetDates(this DateType dateType, ref DateTime outFromDate, ref DateTime outToDate, string inputFrom = "", string inputTo = "")
        {
            switch (dateType)
            {
                case DateType.CurrentMonth:
                    outFromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    outToDate = outFromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case DateType.LastMonth:
                    outFromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    outToDate = outFromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case DateType.CurrentWeek:
                    outFromDate = DateTime.Today.StartOfWeek(DayOfWeek.Sunday);
                    outToDate = outFromDate.AddDays(7).AddSeconds(-1);
                    break;
                case DateType.LastWeek:
                    outFromDate = DateTime.Today.StartOfWeek(DayOfWeek.Sunday).AddDays(7);
                    outToDate = outFromDate.AddDays(7).AddSeconds(-1);
                    break;
                case DateType.CustomDate:
                    try
                    {
                        outFromDate = DateTime.Parse(inputFrom);
                        outToDate = DateTime.Parse(inputTo).AddDays(1).AddSeconds(-1);
                    }
                    catch { DateType.AllTime.GetDates(ref outFromDate, ref outToDate); }
                    break;
                default:
                    outFromDate = DateTime.Today.AddYears(-20);
                    outToDate = DateTime.Today.AddYears(20);
                    break;
            }
        }
    }


    public class OrderEmailViewModel
    {
        public string Title { get; set;}
        public string Body { get; set; }
    }



    public static class LinqExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (!seenKeys.Contains(keySelector(element)))
                {
                    seenKeys.Add(keySelector(element));
                    yield return element;
                }
            }
        }
    }


    public static class HttpPostedFileBaseExtension
    {
        public static byte[] ToByteArray(this HttpPostedFileBase file)
        {
            using (var stream = new MemoryStream())
            {
                file.InputStream.CopyTo(stream);
                return stream.ToArray();
            }
        }
    }

    

}