﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Used in the generation of Word Document images
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Drawing;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;

namespace SnapSuite.Models
{
    public class HubSpotClientHelper
    {
        private static string BaseUrl = ConfigurationManager.AppSettings["HubSpotBaseUrl"];
        private static string ClientID = ConfigurationManager.AppSettings["HubSpotClientID"];
        private static string ClientSecret = ConfigurationManager.AppSettings["HubSpotClientSecret"];
        private static string RedirectUrl = ConfigurationManager.AppSettings["HubSpotCallBackUrl"];
        private static string TokenEndPoint = BaseUrl + "/oauth/v1/token";
        private static string CompanyEndPoint = BaseUrl + "/companies/v2/companies/paged";
        private static string AllContactsEndPoint = BaseUrl + "/contacts/v1/lists/all/contacts/all";
        private static string RevokeEndPoint = BaseUrl + "/oauth/v1/refresh-tokens";

        public HubSpotClientHelper()
        {
        }

        public async Task<HubSpotTokenResponse> GetAccessTokenByCode(string code)
        {
            var client = new HttpClient();

            var data = new Dictionary<string, string>();
            data.Add("grant_type", "authorization_code");
            data.Add("client_id", ClientID);
            data.Add("client_secret", ClientSecret);
            data.Add("redirect_uri", RedirectUrl);
            data.Add("code", code);

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint) { Content = new FormUrlEncodedContent(data), };
            var response = await client.SendAsync(request);
            var tokenResponse = new HubSpotTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<HubSpotTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<HubSpotTokenResponse> RefreshAccessToken(string refreshToken)
        {
            var client = new HttpClient();

            var data = new Dictionary<string, string>();
            data.Add("grant_type", "refresh_token");
            data.Add("client_id", ClientID);
            data.Add("client_secret", ClientSecret);
            data.Add("redirect_uri", RedirectUrl);
            data.Add("refresh_token", refreshToken);

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint) { Content = new FormUrlEncodedContent(data), };
            var response = await client.SendAsync(request);
            var tokenResponse = new HubSpotTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<HubSpotTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<HttpResponseMessage> GetAllCompanies(string accessToken, string offset = null)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            var parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("properties", "name"));
            parameters.Add(new KeyValuePair<string, string>("properties", "website"));
            parameters.Add(new KeyValuePair<string, string>("properties", "description"));
            parameters.Add(new KeyValuePair<string, string>("properties", "city"));
            parameters.Add(new KeyValuePair<string, string>("properties", "address"));
            parameters.Add(new KeyValuePair<string, string>("properties", "country"));
            parameters.Add(new KeyValuePair<string, string>("properties", "zip"));
            parameters.Add(new KeyValuePair<string, string>("properties", "phone"));
            if (!string.IsNullOrEmpty(offset))
            {
                parameters.Add(new KeyValuePair<string, string>("offset", offset));
            }

            var para = String.Format("?limit={0}", 250);
            foreach (var parameter in parameters)
            {
                para += String.Format("&{0}={1}", parameter.Key, parameter.Value);
            }

            var response = await client.GetAsync(CompanyEndPoint + para).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> GetAllContacts(string accessToken, string vidOffset = null)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            var parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("property", "associatedcompanyid"));
            parameters.Add(new KeyValuePair<string, string>("property", "company"));
            parameters.Add(new KeyValuePair<string, string>("property", "firstname"));
            parameters.Add(new KeyValuePair<string, string>("property", "lastname"));
            parameters.Add(new KeyValuePair<string, string>("property", "mobilephone"));
            parameters.Add(new KeyValuePair<string, string>("property", "phone"));
            parameters.Add(new KeyValuePair<string, string>("property", "email"));
            parameters.Add(new KeyValuePair<string, string>("property", "jobtitle"));
            if (!string.IsNullOrEmpty(vidOffset))
            {
                parameters.Add(new KeyValuePair<string, string>("vidOffset", vidOffset));
            }

            var para = "?count=100";
            foreach (var parameter in parameters)
            {
                para += String.Format("&{0}={1}", parameter.Key, parameter.Value);
            }

            var response = await client.GetAsync(AllContactsEndPoint + para).ConfigureAwait(false);
            return response;
        }

        //public async Task<HttpResponseMessage> Test(string accessToken)
        //{
        //    var client = new HttpClient();
        //    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

        //    var response = await client.GetAsync(BaseUrl + "/properties/v1/companies/properties/").ConfigureAwait(false);
        //    var data = response.Content.ReadAsStringAsync().Result.ToString();
        //    return response;
        //}

        public async Task<HttpResponseMessage> RevokeToken(string refreshToken)
        {
            var client = new HttpClient();
            var response = await client.DeleteAsync(RevokeEndPoint + "/" + refreshToken).ConfigureAwait(false);
            return response;
        }
    }

    public class HubSpotResponse
    {
        [JsonProperty("statusCode")]
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }

    public class HubSpotTokenResponse : HubSpotResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
    }

    public class HubSpotProperty
    {
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        [JsonProperty("source")]
        public string Source { get; set; }
        [JsonProperty("sourceId")]
        public string SourceID { get; set; }
    }

    #region HubSpot Company Response

    public class HubSpotCompaniesResponse : HubSpotResponse
    {
        [JsonProperty("companies")]
        public List<HubSpotCompany> Companies { get; set; }
        [JsonProperty("has-more")]
        public bool HasMore { get; set; }
        [JsonProperty("offset")]
        public long Offset { get; set; }
    }

    public class HubSpotCompany
    {
        [JsonProperty("companyId")]
        public string CompanyID { get; set; }
        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }
        [JsonProperty("properties")]
        public HubSpotCompanyProperties Properties { get; set; }
    }

    public class HubSpotCompanyProperties
    {
        [JsonProperty("name")]
        public HubSpotProperty Name { get; set; }
        [JsonProperty("description")]
        public HubSpotProperty Description { get; set; }
        [JsonProperty("website")]
        public HubSpotProperty Website { get; set; }
        [JsonProperty("city")]
        public HubSpotProperty City { get; set; }
        [JsonProperty("address")]
        public HubSpotProperty Address { get; set; }
        [JsonProperty("country")]
        public HubSpotProperty Country { get; set; }
        [JsonProperty("zip")]
        public HubSpotProperty Postcode { get; set; }
        [JsonProperty("phone")]
        public HubSpotProperty Phone { get; set; }
    }

    #endregion

    #region HubSpot Contact Response

    public class HubSpotContactResponse : HubSpotResponse
    {
        [JsonProperty("vid-offset")]
        public long VidOffSet { get; set; }
        [JsonProperty("has-more")]
        public bool HasMore { get; set; }
        [JsonProperty("contacts")]
        public List<HubSpotContact> Contacts { get; set; }

    }

    public class HubSpotContact
    {
        [JsonProperty("identity-profiles")]
        public List<HubSpotContactIdentityProfile> IdentityProfiles { get; set; }
        [JsonProperty("properties")]
        public HubSpotContactProperies Properties { get; set; }
        [JsonProperty("is-contact")]
        public bool IsContact { get; set; }
        [JsonProperty("vid")]
        public long Vid { get; set; }
        [JsonProperty("portal-id")]
        public long PortalId { get; set; }
        [JsonProperty("profile-url")]
        public string ProfileUrl { get; set; }
    }

    public class HubSpotContactProperies
    {
        [JsonProperty("firstname")]
        public HubSpotProperty FirstName { get; set; }
        [JsonProperty("lastname")]
        public HubSpotProperty LastName { get; set; }
        [JsonProperty("associatedcompanyid")]
        public HubSpotProperty AssociatedCompanyID { get; set; }
        [JsonProperty("company")]
        public HubSpotProperty Company { get; set; }
        [JsonProperty("lastmodifieddate")]
        public HubSpotProperty LastModifiedDate { get; set; }
        [JsonProperty("mobilephone")]
        public HubSpotProperty MobilePhone { get; set; }
        [JsonProperty("phone")]
        public HubSpotProperty Phone { get; set; }
        [JsonProperty("email")]
        public HubSpotProperty Email { get; set; }
        [JsonProperty("jobtitle")]
        public HubSpotProperty JobTitle { get; set; }

    }

    public class HubSpotContactIdentityProfile
    {
        [JsonProperty("vid")]
        public long Vid { get; set; }
        [JsonProperty("saved-at-timestamp")]
        public long SaveAtTimestamp { get; set; }
        [JsonProperty("deleted-changed-timestamp")]
        public long DeletedChangedTimestamp { get; set; }
        [JsonProperty("identities")]
        public List<HubSpotContactIdentity> Identities { get; set; }
    }

    public class HubSpotContactIdentity
    {
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        [JsonProperty("is-primary")]
        public bool IsPrimary { get; set; }
    }

    #endregion
}
