﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Used in the generation of Word Document images
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace SnapSuite.Integrations.Infusionsoft
{
    public class InfusionsoftApiHelper
    {
        private static string ClientID = ConfigurationManager.AppSettings["InfusionsoftClientID"];
        private static string ClientSecret = ConfigurationManager.AppSettings["InfusionsoftClientSecret"];
        private static string RedirectUrl = ConfigurationManager.AppSettings["InfusionsoftCallBackUrl"];
        private static string BaseUrl = "https://api.infusionsoft.com";
        private static string TokenEndPoint = BaseUrl + "/token";
        private static string CompaniesEndPoint = BaseUrl + "/crm/rest/v1/companies";
        private static string ContactsEndPoint = BaseUrl + "/crm/rest/v1/contacts";

        public InfusionsoftApiHelper()
        {
        }

        public async Task<InfusionsoftTokenResponse> GetAccessTokenByCode(string code)
        {
            var client = new HttpClient();

            var para = new Dictionary<string, string>();
            para.Add("code", code);
            para.Add("redirect_uri", RedirectUrl);
            para.Add("client_id", ClientID);
            para.Add("client_secret", ClientSecret);
            para.Add("grant_type", "authorization_code");

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint) { Content = new FormUrlEncodedContent(para) };
            var response = await client.SendAsync(request);
            var tokenResponse = new InfusionsoftTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<InfusionsoftTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<InfusionsoftTokenResponse> RefreshAccessToken(string refreshToken)
        {
            var client = new HttpClient();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(ClientID + ":" + ClientSecret)));

            var para = new Dictionary<string, string>();
            para.Add("grant_type", "refresh_token");
            para.Add("client_id", ClientID);
            para.Add("client_secret", ClientSecret);
            para.Add("redirect_uri", RedirectUrl);
            para.Add("refresh_token", refreshToken);

            var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint) { Content = new FormUrlEncodedContent(para) };
            //var request = new HttpRequestMessage(HttpMethod.Post, TokenEndPoint + "?" + String.Join("&", paraList));
            var response = await client.SendAsync(request);
            var tokenResponse = new InfusionsoftTokenResponse();
            if (response.Content != null)
            {
                var responseData = await response.Content.ReadAsStringAsync();
                tokenResponse = JsonConvert.DeserializeObject<InfusionsoftTokenResponse>(responseData);
                tokenResponse.StatusCode = response.StatusCode;
            }

            return tokenResponse;
        }

        public async Task<HttpResponseMessage> GetAllCompanies(string accessToken, string next = null)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            if (!string.IsNullOrEmpty(next))
            {
                return await client.GetAsync(next).ConfigureAwait(false);
            }
            var response = await client.GetAsync(CompaniesEndPoint).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> GetAllContacts(string accessToken, string next = null)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);

            if (!string.IsNullOrEmpty(next))
            {
                return await client.GetAsync(next).ConfigureAwait(false);
            }

            var response = await client.GetAsync(ContactsEndPoint).ConfigureAwait(false);
            return response;
        }

        public async Task<HttpResponseMessage> RevokeToken(string refreshToken)
        {
            var client = new HttpClient();
            var response = await client.PostAsync(TokenEndPoint + "/revoke?token=" + refreshToken, null).ConfigureAwait(false);
            return response;
        }
    }

}
