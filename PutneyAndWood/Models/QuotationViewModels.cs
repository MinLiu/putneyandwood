﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SnapSuite.Models
{
    
    public class QuotationPDFViewModel
    {
        //Used in PDF Generation
        public CompanyViewModel CompanyDetails { get; set; }
        public QuotationViewModel QuotationDetails { get; set; }
        //public QuotationPreviewViewModel QuotationPreview { get; set; }
        public List<QuotationItemViewModel> QuotationItems { get; set; }
        public ClientAddressViewModel InvoiceAddress { get; set; }
        public ClientAddressViewModel DeliveryAddress { get; set; }
        public string TermsConditions { get; set; }

        public string AssignedUserName { get; set; }
        public string AssignedUserEmail { get; set; }
        public string AssignedUserPhone { get; set; }

        public string DefaultFooterImageURL { get; set; }
    }

    /*
    public class QuotationReviewViewModel
    {
        //Used to edit the display settings
        public string QuotationID { get; set; }
        public string CheckSum { get; set; }
        public string Reference { get; set; }
        public int Number { get; set; }
        public int Version { get; set; }
        public int StatusID { get; set; }
        public string StatusHexColour { get; set; }
        public string StatusDescription { get; set; }
        public bool HasAgreement { get; set; }

        public List<QuotationAttachmentViewModel> QuotationAttachments { get; set; }
    }
    */

    public class QuotationAcceptDeclineViewModel
    {
        [Required]
        public string QuotationID { get; set; }
        public string CheckSum { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
        public string StatusHexColour { get; set; }
        public bool HasAgreement { get; set; }

        public bool Accept { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Message { get; set; }

        public string PoNumber { get; set; }
    }


    public class QuotationEmailViewModel
    {
        public string QuotationID { get; set; }
        public string ClientID { get; set; }
        public string CheckSum { get; set; }

        [Required]
        [Display(Name = "To Email Addresses")]
        public string ToEmails { get; set; }

        [EmailAddress]
        [Display(Name = "From Email Address")]
        public string FromEmail { get; set; }

        public string Bcc { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IncludePDF { get; set; }
        public bool SendCopyPDF { get; set; }

        public List<QuotationAttachmentViewModel> Attachments { get; set; }
        public IEnumerable<string> SelectedAttachments { get; set; }
    }
    

    public class QuotationsDashboardViewModel
    {
        public IEnumerable<DashboardChartValueViewModel> Stats { get; set; }
        public IEnumerable<QuotationGridViewModel> RecentQuotations { get; set; }
    }


    public class QuotationConvertPOViewModel
    {
        public QuotationViewModel QuotationDetails { get; set; }
        public bool CombineItems { get; set; }
        public bool UseAgreedCost { get; set; }
    }
    

}