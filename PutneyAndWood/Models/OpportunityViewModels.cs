﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SnapSuite.Models
{

    public class OpportunitySettingViewModel
    {
        public List<OpportunityTypeViewModel> OpportunityTypes { get; set; }
    }
}

