﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SnapSuite.Models
{

    public class JsonResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}

