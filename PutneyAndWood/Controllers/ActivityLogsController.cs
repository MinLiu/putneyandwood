﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ActivityLogsController : ServiceGridController<ActivityLog, ActivityLogViewModel, ActivityLogService<ActivityLogViewModel>>
    {

        public ActivityLogsController()
            : base(new ActivityLogService<ActivityLogViewModel>(null, Guid.Empty, new ActivityLogMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, int? moduleID)
        {
            var logs = _service.Search(filterText, moduleID);
            logs = ApplyQueryFiltersSort(request, logs, "Timestamp DESC");

            var logList = logs.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _service.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = logList.ToList(), Total = logs.Count() };
            return Json(result);
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var logs = _service.Search("", null);
            logs = ApplyQueryFiltersSort(request, logs, "Timestamp DESC");

            var logList = logs.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _service.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = logList.ToList(), Total = logs.Count() };
            return Json(result);
        }


        public override JsonResult Create(DataSourceRequest request, ActivityLogViewModel viewModel)
        {
            throw new Exception("Not Possible");
        }


        public override JsonResult Update(DataSourceRequest request, ActivityLogViewModel viewModel)
        {
            throw new Exception("Not Possible");
        }


        public override JsonResult Destroy(DataSourceRequest request, ActivityLogViewModel viewModel)
        {
            throw new Exception("Not Possible");
        }


        public static void CreateLog(int moduleID, string description, User user, string linkID = null)
        {
            try
            {
                var repo = new ActivityLogRepository();
                var mapper = new ActivityLogMapper();

                var model = new ActivityLog
                {
                    CompanyID = user.CompanyID,
                    Timestamp = DateTime.Now,
                    ModuleTypeID = moduleID,
                    Description = description,
                    ActivityByUserID = user.Id,
                    LinkID = linkID,
                };
                repo.Create(model);
            }
            catch { }
        }

    }

}