﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class SubscriptionPlanPaymentsController : GridController<SubscriptionPlanPayment, SubscriptionPlanPaymentViewModel>
    {

        public SubscriptionPlanPaymentsController()
            : base(new SubscriptionPlanPaymentRepository(), new SubscriptionPlanPaymentMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read(DataSourceRequest request, string cID)
        {
            var payments = _repo.Read().Where(p => (p.CompanyID.ToString() == cID))
                                        .Include(p => p.Company)
                                        .OrderByDescending(p => p.Timestamp);

            //payments = ApplyQueryFiltersSort(request, payments, "Timestamp DESC");

            var payList = payments.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = payList.ToList(), Total = payments.Count() };
            return Json(result);
        }



    }

}