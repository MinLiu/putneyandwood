﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SnapSuite.Models;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class SubscriptionController : BaseController
    {
        

        [Authorize]
        public ActionResult Index()
        {

            var _mapper = new SubscriptionPlanMapper();
            var _repo = new SubscriptionPlanRepository();


            var list = _repo.Read().Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(s => s.StartDate);

            if (list.Count() > 0)
            {
                var model = list.First();
                var viewModel = _mapper.MapToViewModel(model);
                return View(viewModel);
            }
            else
            {
                var viewModel = new SubscriptionPlanViewModel
                                {
                                    Name = "No Subscription Found",
                                    Description = "No Subscription Details have been found. Please signup for a subscription or contact us.",
                                    Created = DateTime.Now,
                                    Active = false
                                };

                return View(viewModel);
            }

        }

        /*
        public ActionResult Change()
        {
            var cartMapper = new SubscriptionCartItemMapper();

            try
            {
                var _repo = new SubscriptionPlanRepository();
                var currentSub = _repo.Read().Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(s => s.StartDate).First();

                //if(currentSub.Active)
                //    return RedirectToAction("Index");

                var viewModel = new SubscriptionCartItemViewModel 
                { 
                    NumberCRM = currentSub.NumberCRM, 
                    NumberProducts = currentSub.NumberProducts, 
                    NumberQuotations = currentSub.NumberQuotations,
                    NumberJobs = currentSub.NumberJobs,
                    NumberDeliveryNotes = currentSub.NumberDeliveryNotes,
                    NumberInvoices = currentSub.NumberInvoices,
                    NumberPurchaseOrders = currentSub.NumberPurchaseOrders,
                    NumberStockControl = currentSub.NumberStockControl,
                };

                var model = SubscriptionManager.Generate(viewModel, CurrentUser.CompanyID, currentSub);
                return View(cartMapper.MapToViewModel(model));
            }
            catch { }

            return View(new SubscriptionCartItemViewModel {  });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Change(SubscriptionCartItemViewModel viewModel, string submitButton)
        {
            var repo = new SubscriptionPlanRepository();
            var cartRepo = new SubscriptionCartItemRepository();
            var cartMapper = new SubscriptionCartItemMapper();

            SubscriptionPlan currentSub = null;
            
            try { currentSub = repo.Read().Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(s => s.StartDate).First(); }
            catch { }

            //Calculate subscription based on the users selected.
            var model = SubscriptionManager.Generate(viewModel, CurrentUser.CompanyID, currentSub);
           
            if ((submitButton ?? "") == "Confirm")
            {                
                cartRepo.Create(model);
                cartRepo.Update(model);

                return PartialView("Confirm", cartMapper.MapToViewModel(model));
            }
            else 
            {
                return PartialView(cartMapper.MapToViewModel(model));
            }
        }
        */

        public ActionResult Change()
        {
            var repo = new SubscriptionPlanRepository();
            try
            {
                var currentSub = repo.Read().Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(s => s.StartDate).First();

                if(currentSub.Active && currentSub.EndDate >= DateTime.Now)
                    return RedirectToAction("Index");
            }
            catch { }

            return View();
        }

        public ActionResult Confirm(SubscriptionPlanType type)
        {
            var repo = new SubscriptionPlanRepository();
            var cartRepo = new SubscriptionCartItemRepository();
            var cartMapper = new SubscriptionCartItemMapper();

            SubscriptionPlan currentSub = null;

            try { currentSub = repo.Read().Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(s => s.StartDate).First(); }
            catch { }

            //Calculate subscription based on the users selected.
            var model = SubscriptionManager.CreatePlan(type, CurrentUser.CompanyID, currentSub);
            cartRepo.Create(model);
            cartRepo.Update(model);
            return View("Confirm", cartMapper.MapToViewModel(model));        
        }


        public ActionResult RequestCancel()
        {
            var viewModel = new SubscriptionRequestViewModel
            {
                Timestamp = DateTime.Now,
                CompanyID = CurrentUser.Company.ID.ToString(),
                CompanyName = CurrentUser.Company.Name,
                Email = CurrentUser.Email,
                Message = "Please cancel my current subscription plan."
            };

            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestCancel(SubscriptionRequestViewModel viewModel)
        {
            try
            {
                var requestRepo = new SubscriptionRequestRepository();
                var requestMapper = new SubscriptionRequestMapper();

                var model = requestMapper.MapToModel(viewModel);

                model.Timestamp = DateTime.Now;
                model.CompanyID = CurrentUser.CompanyID;
                model.CompanyName = CurrentUser.Company.Name;
                model.Complete = false;

                requestRepo.Create(model);

                string message = string.Format("Cancel Subscription\n\nFrom: {0}\nEmail: {1}\nCompany ID: {2}\n\nMessage:-\n{3}",
                                              model.CompanyName,
                                              model.Email,
                                              model.CompanyID,
                                              model.Message);

                EmailService.SendEmail("info@quikflw.com", "Cancel Subscription", message, Server);
                ViewBag.SuccessMessage = "Your Cancel Subscription Request has been sent.";
            }
            catch {
                ViewBag.ErrorMessage = "Could not send your Cancel Subscription Request. Please try again.";
            }

            return View(viewModel);

        }

    }
}