﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SnapSuite.Models;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SnapSuite.Controllers
{
    
    public class WorldPayController : BaseController
    {

        private WorldPayResponseRepository _repo = new WorldPayResponseRepository();
        private SubscriptionCartItemRepository _cartRepo = new SubscriptionCartItemRepository();
        private SubscriptionPlanRepository _planRepo = new SubscriptionPlanRepository();
        private SubscriptionPlanPaymentRepository _paymentRepo = new SubscriptionPlanPaymentRepository();


        public ActionResult Index()
        {
            return Content("");

        }

        public ActionResult Payment()
        {

            

            var response = new WorldPayResponse { Timestamp = DateTime.Now };
            try { response.InstID = Request.Form["instId"]; } catch { }
            try { response.CartID = Request.Form["cartId"]; } catch { }
            try { response.Desc = Request.Form["desc"]; } catch { }
            try { response.Cost = decimal.Parse(Request.Form["cost"]); } catch { }
            try { response.Amount = decimal.Parse(Request.Form["amount"]); } catch { }
            try { response.AmountString = Request.Form["amountString"]; } catch { }
            try { response.Currency = Request.Form["currency"]; } catch { }
            try { response.AuthMode = Request.Form["authMode"]; } catch { }
            try { response.TestMode = Request.Form["testMode"]; } catch { }

            try { response.Name = Request.Form["name"]; } catch { }
            try { response.Address1 = Request.Form["address1"]; } catch { }
            try { response.Address2 = Request.Form["address2"]; } catch { }
            try { response.Address3 = Request.Form["address3"]; } catch { }
            try { response.Town = Request.Form["town"]; } catch { }
            try { response.Region = Request.Form["region"]; } catch { }
            try { response.Postcode = Request.Form["postcode"]; } catch { }
            try { response.Country = Request.Form["country"]; } catch { }
            try { response.CountryString = Request.Form["countryString"]; } catch { }
            try { response.Tel = Request.Form["tel"]; } catch { }
            try { response.Fax = Request.Form["fax"]; } catch { }
            try { response.Email = Request.Form["email"]; } catch { }

            try { response.DelvName = Request.Form["delvName"]; } catch { }
            try { response.DelvAddress1 = Request.Form["delvAddress1"]; } catch { }
            try { response.DelvAddress2 = Request.Form["delvAddress2"]; } catch { }
            try { response.DelvAddress3 = Request.Form["delvAddress3"]; } catch { }
            try { response.DelvTown = Request.Form["delvTown"]; } catch { }
            try { response.DelvRegion = Request.Form["delvRegion"]; } catch { }
            try { response.DelvPostcode = Request.Form["delvPostcode"]; } catch { }
            try { response.DelvCountry = Request.Form["delvCountry"]; } catch { }
            try { response.DelvCountryString = Request.Form["delvCountryString"]; } catch { }
           
            try { response.CompName = Request.Form["compName"]; } catch { }
            try { response.TransID = Request.Form["transId"]; } catch { }
            try { response.TransStatus = Request.Form["transStatus"]; } catch { }
            try { response.TransTime = Request.Form["transTime"]; } catch { }
            try { response.AuthAmount = decimal.Parse(Request.Form["authAmount"]); } catch { }
            try { response.AuthCost = decimal.Parse(Request.Form["authCost"]); } catch { }
            try { response.AuthCurrency = Request.Form["authCurrency"]; } catch { }
            try { response.AuthAmountString = Request.Form["authAmountString"]; } catch { }
            try { response.RawAuthMessage = Request.Form["rawAuthMessage"]; } catch { }
            try { response.RawAuthCode = Request.Form["rawAuthCode"]; } catch { }
            try { response.CallbackPW = Request.Form["callbackPW"]; } catch { }
            try { response.CardType = Request.Form["cardType"]; } catch { }
            try { response.CountryMatch = Request.Form["countryMatch"]; } catch { }
            try { response.AVS = Request.Form["AVS"]; } catch { }
            try { response.WafMerchMessage = Request.Form["wafMerchMessage"]; } catch { }
            try { response.Authentication = Request.Form["authentication"]; } catch { }
            try { response.IpAddress = Request.Form["ipAddress"]; } catch { }
            try { response.Charenc = Request.Form["charenc"]; } catch { }
            try { response._spCharEnc = Request.Form["_SP.charEnc"]; } catch { }

            try { response.FuturePayID = Request.Form["futurePayId"]; } catch { }
            try { response.FuturePayStatusChange = Request.Form["futurePayStatusChange"]; } catch { }


            if ((response.InstID ?? "") == "") return Content("");



            _repo.Create(response);

            try
            {
                if (response.RawAuthCode == "A") //Agreement completed OR Authorised payment made within a FuturePay agreement
                {

                    var list = _planRepo.Read().Where(p => p.FuturePayID == response.FuturePayID && p.FuturePayID != null);

                    //Create payment record
                    var payment = new SubscriptionPlanPayment { 
                                        Timestamp = DateTime.Now, 
                                        Description = response.Desc,
                                        Amount = response.Amount,
                                        Currency = response.Currency,
                                        FuturePayID = response.FuturePayID
                                    };


                    if (list.Count() > 0) //If a plan with the future id exists then update it.
                    {
                        var item = list.First();
                        item.Active = true;
                        item.StartDate = DateTime.Today;
                        item.EndDate = DateTime.Today.AddMonths(1);

                        _planRepo.Update(item);

                        payment.CompanyID = item.CompanyID;
                    }
                    else  //If a plan doesn't exist with the future id then create one.
                    {
                        var item = _cartRepo.Read().Where(c => ("QFLW-01-" + c.ID) == response.CartID).First();
                        var newSub = new SubscriptionPlan
                        {
                            CompanyID = item.CompanyID,
                            Name = item.Name,
                            Description = item.Description,
                            Created = DateTime.Now,
                            StartDate = DateTime.Today,
                            EndDate = DateTime.Today.AddMonths(1),
                            InitialAmount = item.InitialAmount,
                            RegularAmount = item.RegularAmount,
                            Active = true,
                            FuturePayID = response.FuturePayID,
                            InternalCartID = item.ID,
                            CartID = response.CartID,

                            NumberCRM = item.NumberCRM,
                            NumberDeliveryNotes = item.NumberDeliveryNotes,
                            NumberInvoices = item.NumberInvoices,
                            NumberProducts = item.NumberProducts,
                            NumberQuotations = item.NumberQuotations,
                            NumberJobs = item.NumberJobs,
                            NumberPurchaseOrders = item.NumberPurchaseOrders,
                            NumberStockControl = item.NumberStockControl,
                            EnableTeamManagement = item.EnableTeamManagement,
                        };

                        _planRepo.Create(newSub);
                        payment.CompanyID = item.CompanyID;


                        //Update the users of the company, de-assign users if less than subscription
                        try
                        {
                            var userRepo = new UserRepository();
                            var users = userRepo.Read().Where(u => u.CompanyID == item.CompanyID).ToList();

                            //Skip the users that are in the subscription range, but assign the users after.
                            foreach(var u in users.Where(u => u.AccessCRM == true).OrderBy(u => u.Id).Skip(item.NumberCRM)) { u.AccessCRM = false; }
                            foreach (var u in users.Where(u => u.AccessProducts == true).OrderBy(u => u.Id).Skip(item.NumberProducts)) { u.AccessProducts = false; }
                            foreach (var u in users.Where(u => u.AccessQuotations == true).OrderBy(u => u.Id).Skip(item.NumberQuotations)) { u.AccessQuotations = false; }
                            foreach (var u in users.Where(u => u.AccessJobs == true).OrderBy(u => u.Id).Skip(item.NumberJobs)) { u.AccessJobs = false; }
                            foreach (var u in users.Where(u => u.AccessInvoices == true).OrderBy(u => u.Id).Skip(item.NumberInvoices)) { u.AccessInvoices = false; }
                            foreach (var u in users.Where(u => u.AccessDeliveryNotes == true).OrderBy(u => u.Id).Skip(item.NumberDeliveryNotes)) { u.AccessDeliveryNotes = false; }
                            foreach (var u in users.Where(u => u.AccessPurchaseOrders == true).OrderBy(u => u.Id).Skip(item.NumberPurchaseOrders)) { u.AccessPurchaseOrders = false; }
                            foreach (var u in users.Where(u => u.AccessStockControl == true).OrderBy(u => u.Id).Skip(item.NumberStockControl)) { u.AccessStockControl = false; }

                            //update only the relevent information
                            userRepo.Update(users, new string[] { "AccessCRM", "AccessProducts", "AccessQuotations", "AccessJobs", "AccessInvoices", "AccessDeliveryNotes", "AccessPurchaseOrders", "AccessStockControl" });
                        }
                        catch { }
                    }
                    
                    _paymentRepo.Create(payment);
                }


                if (response.RawAuthCode == "D") //Payment declined
                {

                    var plan = _planRepo.Read().Where(p => p.FuturePayID == response.FuturePayID).First();

                    //Create payment record
                    var payment = new SubscriptionPlanPayment
                    {
                        CompanyID = plan.CompanyID,
                        Timestamp = DateTime.Now,
                        Description = "*Payment declined - " + response.Desc,
                        Amount = response.Amount, 
                        Currency = response.Currency,
                        FuturePayID = response.FuturePayID
                    };

                    _paymentRepo.Create(payment);

                }

                if (response.FuturePayStatusChange.ToLower().Contains("cancelled"))
                {
                    var plan = _planRepo.Read().Where(p => p.FuturePayID == response.FuturePayID).First();
                    plan.Active = false;

                    _planRepo.Update(plan, new string[] { "Active" });
                }
            }
            catch { }


            return Content("");
        }

        
    }
}