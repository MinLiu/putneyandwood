﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.ComponentModel;

namespace SnapSuite.Controllers
{
       
    [Authorize]
    public class EventsController : BaseController
    {

        public EventService<EventViewModel> _service { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewQuotations == false)
                Response.Redirect("/");

            _service = new EventService<EventViewModel>(CurrentUser.Id, new EventMapper(), new SnapDbContext());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string projectID, string quoteID, string clientID, string fromD, string toD, DateType dateType = DateType.AllTime, bool showComplete = true, string assignedUser = null)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromD, toD);

            var list = _service.Read()
                            .Where(t => t.Timestamp >= fmDate && t.Timestamp <= tDate)
                            .Where(t => showComplete == true || (showComplete == false && t.Complete == false))
                            .Where(t => quoteID == null || quoteID == "" || t.QuotationID.ToString() == quoteID)
                            .Where(t => projectID == null || projectID == "" || t.ProjectID.ToString() == projectID)
                            .Where(t => clientID == null || clientID == "" || t.ClientID.ToString() == clientID)
                            .Where(t => assignedUser == null || assignedUser == "" || t.AssignedUserID.ToString() == assignedUser)
                            .Where(t => t.Project == null || t.Project.Deleted == false)
                            .Where(t => t.Quotation == null || t.Quotation.Deleted == false)
                            .OrderByDescending(t => t.Timestamp);

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                switch (request.Sorts[x].Member)
                {
                    case "Timestamp":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.Timestamp);
                        else list = list.OrderByDescending(p => p.Timestamp);
                        break;
                    case "EventProjectName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.Project.Name);
                        else list = list.OrderByDescending(p => p.Project.Name);
                        break;
                    case "QuotationName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.Quotation.Description);
                        else list = list.OrderByDescending(p => p.Quotation.Description);
                        break;
                    case "EventClientName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.Client.Name);
                        else list = list.OrderByDescending(p => p.Client.Name);
                        break;
                    case "EventContactName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.ClientContact.FirstName + p.ClientContact.LastName);
                        else list = list.OrderByDescending(p => p.ClientContact.FirstName + p.ClientContact.LastName);
                        break;
                    case "EventAssignedUserID":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        else list = list.OrderByDescending(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        break;
                    case "EventCreatorID":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) list = list.OrderBy(p => p.Creator.FirstName + p.Creator.LastName);
                        else list = list.OrderByDescending(p => p.Creator.FirstName + p.Creator.LastName);
                        break;
                    default:
                        list = list.OrderByDescending(p => p.Timestamp);
                        break;
                }
            }

            var taskList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(t => _service.MapToViewModel(t));
            DataSourceResult result = new DataSourceResult { Data = taskList.ToList(), Total = list.Count() };
            return Json(result);

        }


        [HttpGet]
        public ActionResult _NewEvent(string eventAssignedUserID, string clientID, string contactID, string timestamp, string projectID, string quotationID)
        {
            var viewModel = new EventViewModel()
            {
                ID = Guid.NewGuid().ToString(),
                CompanyID = CurrentUser.CompanyID.ToString(),
                //EventAssignedUserID = eventAssignedUserID ?? project.AssignedUserID,
                EventCreatorID = CurrentUser.Id,
                EventClientID = clientID ?? "",
                EventContactID = contactID ?? "",
                EventProjectID = projectID ?? "",
                QuotationID = quotationID ?? "",
                Timestamp = !string.IsNullOrEmpty(timestamp) ? DateTime.Parse(timestamp) : DateTime.Now
            };
            return PartialView(viewModel);
        }


        [HttpPost]
        public ActionResult _NewEvent(EventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                //var model = mapper.MapToModel(viewModel);
                //model.CompanyID = CurrentUser.CompanyID;
                //model.CreatorID = CurrentUser.Id;
                _service.Create(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new  { EventProjectID = viewModel.EventProjectID, QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.EventClientID, ContactID = viewModel.EventContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new  { EventProjectID = viewModel.EventProjectID, QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.EventClientID, ContactID = viewModel.EventContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        [HttpGet]
        public ActionResult _EditEvent(Guid eventID)
        {
            var viewModel = _service.MapToViewModel(_service.Find(eventID));

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditEvent(EventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                _service.Update(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new  { EventProjectID = viewModel.EventProjectID, QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.EventClientID, ContactID = viewModel.EventContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { EventProjectID = viewModel.EventProjectID, QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.EventClientID, ContactID = viewModel.EventContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, EventViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
                var model = new QuotationEvent { ID = Guid.Parse(viewModel.ID) };
                _service.Destroy(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

    }
}