﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class UserModulesController : BaseController
    {

        UserRepository _repo = new UserRepository();

        // GET: Users
        public ActionResult Index()
        {
            if (!SubscriptionPlan.EnableTeamManagement)
                RedirectToAction("Index", "Users");

            return View();
        }


        public ActionResult Read([DataSourceRequest]DataSourceRequest request,
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false, 
            bool PurchaseOrders = false, 
            bool StockControl = false
        )
        {

            var users = _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID
                && (
                    (u.AccessCRM && CRM)
                    || (u.AccessProducts && Products)
                    || (u.AccessQuotations && Quotations)
                    || (u.AccessJobs && Jobs)
                    || (u.AccessInvoices && Invoices)
                    || (u.AccessDeliveryNotes && DeliveryNotes)
                    || (u.AccessPurchaseOrders && PurchaseOrders)
                    || (u.AccessStockControl && StockControl)
                    )
                )
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.FirstName)
                .ThenBy(u => u.Email)
                .ToList()
                .Select(u => new UserViewModel
                {
                    ID = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName
                });

            return Json(users.ToDataSourceResult(request));
        }


        public ActionResult _AddUser(string userId, 
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false)
        {

            var user = _repo.Find(userId);
            if (CRM && SubscriptionPlan.NumberCRM > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessCRM == true).Count()) user.AccessCRM = true;
            if (Products && SubscriptionPlan.NumberProducts > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessProducts == true).Count()) user.AccessProducts = true;
            if (Quotations && SubscriptionPlan.NumberQuotations > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessQuotations == true).Count()) user.AccessQuotations = true;
            if (Jobs && SubscriptionPlan.NumberJobs > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessJobs == true).Count()) user.AccessJobs = true;
            if (Invoices && SubscriptionPlan.NumberInvoices > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessInvoices == true).Count()) user.AccessInvoices = true;            
            if (DeliveryNotes && SubscriptionPlan.NumberDeliveryNotes > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessDeliveryNotes == true).Count()) user.AccessDeliveryNotes = true;
            if (PurchaseOrders && SubscriptionPlan.NumberPurchaseOrders > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessPurchaseOrders == true).Count()) user.AccessPurchaseOrders = true;
            if (StockControl && SubscriptionPlan.NumberStockControl > _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID && u.AccessStockControl == true).Count()) user.AccessStockControl = true;

            _repo.Update(user);
            return Json("Success");
        }

        public ActionResult _RemoveUser(string userId,
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false)
        {
            var user = _repo.Find(userId);
            if (CRM) user.AccessCRM = false;
            if (Products) user.AccessProducts = false;
            if (Quotations) user.AccessQuotations = false;
            if (Jobs) user.AccessJobs = false;
            if (Invoices) user.AccessInvoices = false;
            if (DeliveryNotes) user.AccessDeliveryNotes = false;
            if (PurchaseOrders) user.AccessPurchaseOrders = false;
            if (StockControl) user.AccessStockControl = false;

            _repo.Update(user);
            return Json("Success");
        }



        public JsonResult DropDownUsers(
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false
        )
        {

            var users = _repo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID
                && (
                    (!u.AccessCRM && CRM)
                    || (!u.AccessProducts && Products)
                    || (!u.AccessQuotations && Quotations)
                    || (!u.AccessJobs && Jobs)
                    || (!u.AccessInvoices && Invoices)
                    || (!u.AccessDeliveryNotes && DeliveryNotes)
                    || (!u.AccessPurchaseOrders && PurchaseOrders)
                    || (!u.AccessStockControl && StockControl)
                    )
                )
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.FirstName)
                .ThenBy(u => u.Email)
                .ToList()
                .Select(u => new SelectItemViewModel
                {
                    ID = u.Id,
                    Name = u.Email
                });

            return Json(users, JsonRequestBehavior.AllowGet);
        }


    }
}