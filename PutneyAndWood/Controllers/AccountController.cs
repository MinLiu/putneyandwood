﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SnapSuite.Models;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private UserSignInManager _signInManager;
        //private UserManager _userManager;

        public AccountController()
        {
            
        }

        public AccountController(UserSignInManager signInManager )
        {
            try
            {
                //UserManager = userManager;
                SignInManager = signInManager;
            }
            catch { }
        }

        public UserSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<UserSignInManager>(); }
            private set  {  _signInManager = value; }
        }


        //public UserManager UserManager
        //{
        //    get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>(); }
        //    private set { _userManager = value; }
        //}
        

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            Session["Blah"] = "Prevent Anti-XSRF token failed.";
            ViewBag.ReturnUrl = returnUrl;

            if (!String.IsNullOrEmpty(returnUrl) && User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Unauthorized", "Home");
            }

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");  
            }

            
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            Session["Blah"] = "Prevent Anti-XSRF token failed.";

            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Invalid login attempt";
                return View(model);
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var emailConfirmed = false;

            try { emailConfirmed = UserManager.IsEmailConfirmed(UserManager.FindByEmail(model.Email).Id); } //takes in a UserID to check if email is confirmed 
            catch {
                ViewBag.ErrorMessage = "Invalid login attempt";
                return View(model);
            }

            if (emailConfirmed.Equals(true)) {

                //AddSampleAccountItems(UserManager.FindByEmail(model.Email).Company);

                var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        SaveUserTokenCookie(model.Email);
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ViewBag.ErrorMessage = "Invalid login attempt";
                        return View(model);
                }
            }
            else
            {
                ViewBag.ConfirmEmail = model.Email;
                return View(model);
            }
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, bool CaptchaValid)
        {
            if (ModelState.IsValid)
            {

                if (!CaptchaValid)
                {
                    ViewBag.ErrorMessage = "reCaptcha failed. Please complete reCaptcha.";
                    return View(model);
                }

                if (!model.ConsentDataCollection)
                {
                    ViewBag.ErrorMessage = "The consent checkbox is required.";
                    return View(model);
                }

                if (!model.ReadTermsAndConditions)
                {
                    ViewBag.ErrorMessage = "The terms & conditions checkbox is required.";
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.Dummy)){
                    ViewBag.ErrorMessage = "Invalid form submission.";
                    return View(model);
                }

                if (!CheckUnwantedEmailDomains(model.Email))
                {
                    ViewBag.ErrorMessage = "Invalid Email address.";
                    return View(model);
                }

                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ViewBag.ErrorMessage = "The email address entered is already being used.";
                    return View(model);
                }

                var user = new User 
                { 
                    UserName = model.Email, 
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Company = new Company { ID = Guid.NewGuid(), Name = model.BusinessName, Email = model.Email, Telephone = model.PhoneNumber, SignupDate = DateTime.Now, DefaultCurrencyID = 1, ThemeColor = "#6e5097" },
                    AccessCRM = true,
                    AccessProducts = true,
                    AccessQuotations = true,
                    AccessJobs = true,
                    AccessInvoices = true,
                    AccessDeliveryNotes = true,
                    AccessPurchaseOrders = true,
                    AccessStockControl = true,
                };

                var result = await UserManager.CreateAsync(user, model.Password);
                
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Admin");

                    //Add demo/sample items here
                    try { AddSampleAccountItems(user.Company); }
                    catch { }
                    
                    try {  SetupTrial(user.Company); }
                    catch { }
                    
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with a confirmation link
                    
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    string message = String.Format("Hello,\n\nPlease click the following link to confirm your Quikflw account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                    message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                    message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. <a href=\"www.quikflw.com\">www.quikflw.com</a>");

                    EmailService.SendEmail(user.Email, "Confirm your Quikflw Account", message, Server);

#if !DEBUG

                    try
                    {
                        string newUserMessage = String.Format("Someone has signup for a Quikflw account. \n\nBusiness Name:{0}\nName: {1} {2}\nEmail: {3}\nPhone:{4}", 
                            model.BusinessName,
                            model.FirstName,
                            model.LastName,
                            model.Email,
                            model.PhoneNumber
                        );
                        
                        //EmailService.SendEmail("info@fruitfulgroup.com", "New Quikflw User Signup", newUserMessage, Server);
                        EmailService.SendEmail("tim@fruitfulgroup.com", "New Quikflw User Signup", newUserMessage, Server);
                        EmailService.SendEmail("zee@fruitfulgroup.com", "New Quikflw User Signup", newUserMessage, Server);
                        EmailService.SendEmail("min@fruitfulgroup.com ", "New Quikflw User Signup", newUserMessage, Server);
                         
                    }
                    catch { }
#endif

                    return RedirectToAction("RegistrationRedirect", "Account");                    
                }
                else{
                    
                    //AddErrors(result);
                    ViewBag.ErrorMessage = result.Errors.First().ToString();
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult SendConfirmEmail(string email)
        {
            var user = UserManager.FindByEmail(email);

            if (user == null)
            {
                ViewBag.ErrorMessage = "Could not find email address.";
                return View("Login");
            }
            else
            {
                string code = UserManager.GenerateEmailConfirmationToken(user.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                string message = String.Format("Hello,\n\nPlease click the following link to confirm your Quikflw account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. <a href=\"www.quikflw.com\">www.quikflw.com</a>");

                EmailService.SendEmail(user.Email, "Confirm your Quikflw Account", message, Server);

                ViewBag.SuccessMessage = "Confirmation email has been resent.";
                return View("Login");
            }

        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RegistrationRedirect()
        {
            return View();
        }


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                 string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                 var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                 string message = String.Format("Hello,\n\nPlease click the following link to reset your Quikflw account:\n\n<a href=\"{0}\">{0}</a>", callbackUrl);
                 message += String.Format("\n\nIf you received this message by mistake just delete it without clicking the link.\n\nNeed help? Contact Us on through our website");
                 message += String.Format("\n\n\n<hr />This is an automated message, please do not reply to this email address. You can contact us via our website. www.quikflw.com");

                 EmailService.SendEmail(user.Email, "Reset your Quikflw Password", message, Server);                
                 return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            //AddErrors(result);
            ViewBag.ErrorMessage = result.Errors.First().ToString();
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        

        #region Manage User

        public ActionResult Manage()
        {
            
            using (var context = new SnapDbContext())
            {
                //Get the currently logged in user.
                User CurrentUser = UserManager.FindById(User.Identity.GetUserId().ToString());
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);


                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = CurrentUser.Id,
                    Email = CurrentUser.Email,
                    FirstName = CurrentUser.FirstName,
                    LastName = CurrentUser.LastName,
                    PhoneNumber = CurrentUser.PhoneNumber,
                    RoleName = (CurrentUser.Roles.Count > 0) ? roleManager.FindById(CurrentUser.Roles.FirstOrDefault().RoleId).Name : null, //Get the first role name if existing
                };

                return View("Manage", model);
            }

            
         
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(UserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            try
            {
                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return View("Manage", model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                //Save Changes
                UserManager.Update(user);                
                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }

            return View(model);
        }


        // GET: Change Password
        public ActionResult ChangePassword()
        {
            try
            {
                //Get the currently logged in user.
                User CurrentUser = UserManager.FindById(User.Identity.GetUserId().ToString());
                
                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = CurrentUser.Id,
                    Email = CurrentUser.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index"); }
            
        }



        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index");

            if (!ModelState.IsValid)
            {
                return View("ChangePassword", model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return View("ChangePassword", model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                model.Password = "";
                model.ConfirmPassword = "";

                if (result.Succeeded)
                {
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return View(model);
        }


        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //if (_userManager != null)
                //{
                //    _userManager.Dispose();
                //    _userManager = null;
                //}

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        protected void SaveUserTokenCookie(string email){
            using (var context = new SnapDbContext())
            {
                //Update token in database
                var user = context.Users.Where(u => u.Email == email).First();
                user.Token = Guid.NewGuid().ToString("N");

                context.Users.Attach(user);
                context.Entry(user).Property(u => u.Token).IsModified = true;
                context.SaveChanges();

                //Save token in cookies
                HttpCookie cookie = Request.Cookies["GG"] ?? new HttpCookie("GG");
                cookie.Value = user.Token;
                cookie.Expires = DateTime.Now.AddMonths(3);
                Response.Cookies.Add(cookie);
            }
        }

        
        protected void SetupTrial(Company company)
        {
            var planRepo = new SubscriptionPlanRepository();
            var plan = new SubscriptionPlan
            {
                ID = Guid.NewGuid(),
                CompanyID = company.ID,
                Name = "Quikflw 14 Day Free Trial",
                Description = "14 day free trial of Quikflw; All modules. Limited to 5 users.",
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(14),
                Created = DateTime.Now,
                CartID = "FREE-TRIAL",
                NumberCRM = 5,
                NumberProducts = 5,
                NumberQuotations = 5,
                NumberJobs = 5,
                NumberDeliveryNotes = 5,
                NumberInvoices = 5,
                NumberPurchaseOrders = 5,
                NumberStockControl = 5,
                EnableTeamManagement = true,
                Active = false,
                Trial = true
            };

            planRepo.Create(plan);
        }
        
        protected void AddSampleAccountItems(Company company)
        {
            //Set up templates
            var context = new SnapDbContext();
            var companyService = new GenericService<Company>(context);
            company = companyService.Read().Where(i => i.ID == company.ID).Include(i => i.LogoImage).First();
            var fileService = new FileService(context);

            //SEt up company logo
            try
            {
                company.LogoImage = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/All/GenericCompanyLogo.png", "/Images/"), FileExtension = ".png", FileFormat = FileFormat.Image };
                companyService.Update(company);
            }
            catch { }

            var clientsRepo = new GenericService<Client>(context);
            var contactsRepo = new GenericService<ClientContact>(context);
            var productRepo = new GenericService<Product>(context);
            var customProductFieldService = new CustomProductFieldService<EmptyViewModel>(company.ID, null, null, context);
            var quoteRepo = new GenericService<Quotation>(context);
            var quoteSectionRepo = new GenericService<QuotationSection>(context);
            var quoteItemRepo = new GenericService<QuotationItem>(context);
            //var quoteTemplateRepo = new GenericService<QuotationTemplate>(context);
            var opportunityTypeRepo = new GenericService<OpportunityType>(context);
            var opportunityStatusRepo = new GenericService<OpportunityStatus>(context);

            var client = new Client { CompanyID = company.ID, Name = "Sample Client 1", Address1 = "40 Brunswick Square", Address2 = "Bloomsbury", Town = "London", Postcode = "WC1N 1AZ", Email = "sample@quikflw.com", Created = DateTime.Now };

            clientsRepo.Create(client);
            clientsRepo.Create(new Client { CompanyID = company.ID, Name = "Sample Client 2", Address1 = "16 Wharf Rd", Town = "London", Postcode = "N1 7RW", Email = "sample@quikflw.com", Created = DateTime.Now });
            clientsRepo.Create(new Client { CompanyID = company.ID, Name = "Sample Client 3", Address1 = "2516 Mission St", Town = "San Francisco", Postcode = "CA 94110", Country = "USA", Email = "sample@quikflw.com", Created = DateTime.Now });

            var contact = new ClientContact { ClientID = client.ID, Title = "Mr", FirstName = "Donald", LastName = "Draper", Position = "Head of Creative", Email = "sample@quikflw.com" };
            contactsRepo.Create(contact);

            contactsRepo.Create(new ClientContact { ClientID = client.ID, Title = "Ms", FirstName = "Peggy", LastName = "Olson", Position = "Marketing", Email = "sample@quikflw.com" });


            productRepo.Create(new Product { CompanyID = company.ID, ProductCode = "SAMPLE-1", Description = "Sample Product 1", CurrencyID = 1, VAT = 20, Cost = 5, Price = 10, Unit = "KGs", ImageURL = "/Templates/All/box.png" });
            productRepo.Create(new Product { CompanyID = company.ID, ProductCode = "SAMPLE-2", Description = "Sample Product 2", CurrencyID = 1, VAT = 20, Cost = 10, Price = 20, Unit = "Packs", ImageURL = "/Templates/All/box.png" });

            customProductFieldService.Create(new CustomProductField { CompanyID = company.ID, Name = "VendorCode", Label = "Vendor Code", SortPos = 0 });
            customProductFieldService.Create(new CustomProductField { CompanyID = company.ID, Name = "Manufacturer", Label = "Manufacturer", SortPos = 1 });

            var quote = new Quotation
            {
                CheckSum = Guid.NewGuid().ToString("N"),
                CompanyID = company.ID,
                ClientID = client.ID,
                ClientContactID = contact.ID,
                Reference = "Q", //Get from defaults
                Description = "Sample Quotation 1",
                Number = 1000,
                Version = 1,
                StatusID = QuotationStatusValues.DRAFT,
                CurrencyID = 1,
                Created = DateTime.Now,
                LastUpdated = DateTime.Now,
                EstimatedDelivery = DateTime.Now,
                TotalVat = 20,
                TotalNet = 100,
                TotalPrice = 120
            };

            quoteRepo.Create(quote);
            
            var section = new QuotationSection { QuotationID = quote.ID, IncludeInTotal = true };
            quoteSectionRepo.Create(section);

            quoteItemRepo.Create(new QuotationItem { QuotationID = quote.ID, SectionID = section.ID, ProductCode = "QL-1", Description = "Quotation Line Item 1", Cost = 10, Price = 10, VAT = 20, Quantity = 5, Total = 50, SortPos = 0, ImageURL = "/Templates/All/box.png" });
            quoteItemRepo.Create(new QuotationItem { QuotationID = quote.ID, SectionID = section.ID, ProductCode = "QL-2", Description = "Quotation Line Item 2", Cost = 5, Price = 5, VAT = 20, Quantity = 4, Total = 20, SortPos = 1, ImageURL = "/Templates/All/box.png" });
            quoteItemRepo.Create(new QuotationItem { QuotationID = quote.ID, SectionID = section.ID, ProductCode = "QL-3", Description = "Quotation Line Item 3", Cost = 30, Price = 30, VAT = 20, Quantity = 1, Total = 30, SortPos = 2, ImageURL = "/Templates/All/box.png" });
            //quoteItemRepo.Create(new QuotationItem { QuotationID = quote.ID, SectionID = section.ID, ProductCode = "SEV-23", Description = "Service Item", Cost = 0, Price = 0, VAT = 20, Quantity = 1, Total = 0, SortPos = 3 });

            new QuotationService<EmptyViewModel>(company.ID, null, null, new SnapDbContext()).RecalculateAllTotals(quote.ID);
            //QuotationsController.RecalulateTotals(quote.ID);


            //Quotation Templates
            List<QuotationTemplate> temps = new List<QuotationTemplate>();
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Audio Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Audio Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Basic Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Basic Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Catering Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Catering Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Construction Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Construction Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Covering Letter - inc Total", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Covering Letter - inc Total.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Events Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Events Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Kitchen Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Kitchen Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Office Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Office Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Plumbing Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Plumbing Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Windows Front Cover", Format = TemplateFormat.Docx, Type = TemplateType.Front, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Windows Front Cover.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            
            //try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Catering", Format = TemplateFormat.Docx, Type = TemplateType.Body.ToString(), File = new FileEntry { FilePath = baseCtrl.CopyFile("/Templates/Quotations/Catering Template.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Furniture", Format = TemplateFormat.Docx, Type = TemplateType.Body, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Furniture Template.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Green", Format = TemplateFormat.Docx, Type = TemplateType.Body, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Green Template.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Intercept", Format = TemplateFormat.Docx, Type = TemplateType.Body, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Intercept Template.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Floral", Format = TemplateFormat.Docx, Type = TemplateType.Body, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Floral Template.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }

            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Basic Specification", Format = TemplateFormat.Docx, Type = TemplateType.Back, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Basic Spec.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { temps.Add(new QuotationTemplate { CompanyID = company.ID, Filename = "Showcase Specification", Format = TemplateFormat.Docx, Type = TemplateType.Back, File = new FileEntry { FilePath = fileService.DeepCopyFile(company.ID, "/Templates/Quotations/Showcase Spec.docx", "/Templates/"), FileExtension = ".docx", FileFormat = FileFormat.Docx, FileSize = 0 } }); } catch { }
            try { new GenericService<QuotationTemplate>(context).Create(temps); } catch { }


            //Quotation Templates
            try
            {
                var QuotationTemplateRepo = new GenericService<QuotationTemplate>(context);
                List<QuotationTemplate> QuotationTemps = QuotationTemplateRepo.Read().Where(i => i.CompanyID == null).ToArray().Select(i => new QuotationTemplate { CompanyID = company.ID, Filename = i.Filename, Format = i.Format, Type = i.Type, FileID = i.FileID }).ToList();
                QuotationTemplateRepo.Create(QuotationTemps);
            }
            catch { }


            var oppType = new OpportunityType() { CompanyID = company.ID, Name = "Default", LabelValue1 = "Importance", LabelValue2 = "Probability" };
            opportunityTypeRepo.Create(oppType);

            List<OpportunityStatus> statuses = new List<OpportunityStatus>();
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 1, OpportunityTypeID = oppType.ID, Name = "Cold" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 2, OpportunityTypeID = oppType.ID, Name = "Warm" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 3, OpportunityTypeID = oppType.ID, Name = "In Quote" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 4, OpportunityTypeID = oppType.ID, Name = "Quote Sent" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 5, OpportunityTypeID = oppType.ID, Name = "Won" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 6, OpportunityTypeID = oppType.ID, Name = "Lost" });
            statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 7, OpportunityTypeID = oppType.ID, Name = "No Interest" });
            opportunityStatusRepo.Create(statuses);

        }

        /*
        public ActionResult NewCompany()
        {
            
            AddSampleAccountItems(CurrentUser.Company);
            return Content("Completed");
        }
        */

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private bool CheckUnwantedEmailDomains(string email)
        {
            string [] blockList = new string [] { "-delete", "@mail.ru"};

            foreach (string s in blockList)
            {
                if(email.ToLower().Contains(s)) return false;
            }

            return true;
        }
        #endregion
    }
}