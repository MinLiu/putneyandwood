﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;
using SnapSuite.Internal;
using System.Web.Routing;

namespace SnapSuite.Controllers
{
    public class ServiceEntityController<TModel, TViewModel, TService> : BaseController
        where TModel : class,  new()
        where TViewModel : class, new()
        where TService : class, IUserService, IService<TModel>, IViewModelService<TModel, TViewModel>
    {
        protected readonly TService _service;

        public ServiceEntityController(TService service)
        {
            _service = service;
        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            try
            {
                _service.CurrentUserID = CurrentUser.Id;
                var propInfo = _service.GetType().GetProperty("CompanyID");
                if (propInfo != null) propInfo.SetValue(_service, CurrentUser.CompanyID, null);
            }
            catch { }
        }

        protected override void Dispose(bool disposing)
        {
            if (_service != null)
                _service.Dispose();

            base.Dispose(disposing);
        }
    }
}