﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;
using System.IO;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.Drawing;

namespace SnapSuite.Controllers
{
    public class BaseController : Controller
    {
        private UserManager _userManager;
        public User CurrentUser;
        public UserRoleOption CurrentRoleOption;
        public SubscriptionPlan SubscriptionPlan;
        public SubscriptionUserAccessViewModel SubscriptionUserAccessViewModel;

        public UserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<UserManager>(); }
            private set { _userManager = value; }
        }
        

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            FileService.ServerFilePath = Server.MapPath("/");
            FileService.WebURL = (this.Request != null) ? this.Request.Url.GetLeftPart(UriPartial.Authority) : "https://customer.quikflw.com";

            try
            {
                if (!User.Identity.IsAuthenticated)
                {                    
                    return;
                }

                var context = new SnapDbContext();
                var userService = new UserService(context);
                CurrentUser = userService.FindById(User.Identity.GetUserId());

                //Check Login Misuse by looking at the cookies
                if (CurrentUser.Token != (Request.Cookies["GG"] ?? new HttpCookie("GG")).Value)
                {
                    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                    Response.Redirect("/Home/LoginMisuse");
                }
                                
                CurrentRoleOption = userService.GetUserRoleOption(CurrentUser.Id);
                var subService = new SubscriptionService<EmptyViewModel>(null, context);
                SubscriptionUserAccessViewModel = subService.GetSubscriptionUserAccess(CurrentUser, CurrentRoleOption);

                SetSubscriptionViewBag(SubscriptionUserAccessViewModel);
                SetCustomTheme();
                ViewBag.WizardStep = CurrentUser.Company.WizardStep;
                ViewBag.WizardComplete = CurrentUser.Company.WizardComplete;

            }
            catch
            {
                SetSubscriptionViewBag(new SubscriptionUserAccessViewModel { });
            }

        }


        public void SetSubscriptionViewBag(SubscriptionUserAccessViewModel plan)
        {
            ViewBag.TeamManagementEnabled = plan.TeamManagementEnabled;

            ViewBag.ViewQuotations = plan.ViewQuotations;
            ViewBag.EditQuotations = plan.EditQuotations;
            ViewBag.DeleteQuotations = plan.DeleteQuotations;
            ViewBag.ExportQuotations = plan.ExportQuotations;

            ViewBag.ViewCRM = plan.ViewCRM;
            ViewBag.EditCRM = plan.EditCRM;
            ViewBag.DeleteCRM = plan.DeleteCRM;
            ViewBag.ExportCRM = plan.ExportCRM;

            ViewBag.ViewProducts = plan.ViewProducts;
            ViewBag.EditProducts = plan.EditProducts;
            ViewBag.DeleteProducts = plan.DeleteProducts;
            ViewBag.ExportProducts = plan.ExportProducts;
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public void CheckSubscription() {

#if !DEBUG 
            try
            {
                //If a Super User then no need to check subscription information
                //if (User.IsInRole("Super Admin")) return;

                //When get the active subscription and a buffer of 2 days if not active but still valid.
                var currentTime = DateTime.Now.AddDays(-2);

                if (SubscriptionPlan.EndDate < currentTime)
                {
                    Response.Redirect("/Home/SubscriptionInvalid");
                    return;
                }

            }
            catch { }
#endif
        }


        public void SaveExceptionError(Exception ex)
        {
            try
            {
                var repo = new GenericService<ErrorLog>(new SnapDbContext());

                string url = "";
                string get = "";
                string post = "";

                try { url = Request.Url.AbsoluteUri; } catch { }

                try
                {
                    foreach (var key in Request.Form.AllKeys)
                        post += string.Format("{0} : {1}\n", key, Request.Form[key]);
                }
                catch { }

                try
                {
                    foreach (var key in Request.QueryString.AllKeys)
                        get += string.Format("{0} : {1}\n", key, Request.QueryString[key]);
                }
                catch { }

                repo.Create(new ErrorLog
                {
                    Timestamp = DateTime.Now,
                    CompanyID = (CurrentUser != null) ? CurrentUser.CompanyID.ToString() : "",
                    CompanyName = (CurrentUser != null && CurrentUser.Company != null) ? CurrentUser.Company.Name.ToString() : "",
                    UserID = (CurrentUser != null) ? CurrentUser.Id : "",
                    UserName = (CurrentUser != null) ? CurrentUser.Email : "",

                    Title = ex.Message,
                    Description = string.Format("{0} : {1}", ex.GetType().ToString(), ex.Message),
                    TargetSite = (ex.TargetSite != null) ? ex.TargetSite.ToString() : "",
                    SourceError = ex.Source,
                    StackTrace = ex.StackTrace,
                    URL = url,
                    GetValues = get,
                    PostValues = post
                });
            }
            catch { }
        }
        

        protected void SetCustomTheme(Company company)
        {
            if (!string.IsNullOrWhiteSpace(company.ThemeColor))
            {
                ViewBag.ThemeColor = company.ThemeColor;
                try
                {
                    Color color = ColorTranslator.FromHtml(company.ThemeColor);
                    try { ViewBag.ThemeColorDark = ColorTranslator.ToHtml(Color.FromArgb(color.A, (int)(color.R * 0.9), (int)(color.G * 0.9), (int)(color.B * 0.9))); } catch { ViewBag.ThemeColorDark = "#000000"; }

                    //Get Perceived Brightness using formula "brightness = sqrt( .241 R2 + .691 G2 + .068 B2 )". ref link:http://alienryderflex.com/hsp.html
                    try { ViewBag.ThemeBrightness = Math.Sqrt(color.R * color.R * .241 + color.G * color.G * .691 + color.B * color.B * .068); } catch { }
                }
                catch { }
            }
            
            ViewBag.ScreenLogoImageURL = company.ScreenLogoImageURL;
        }

        protected void SetCustomTheme()
        {
            SetCustomTheme(CurrentUser.Company);
        }

        protected void SetNoNavBar()
        {
            if (!User.Identity.IsAuthenticated)
                ViewBag.NoNavBar = true;
        }

    }

    

    public class MemoryPostedFile : HttpPostedFileBase
    {
        private readonly byte[] fileBytes;

        public MemoryPostedFile(byte[] fileBytes, string fileName = null)
        {
            this.fileBytes = fileBytes;
            this.FileName = fileName;
            this.InputStream = new MemoryStream(fileBytes);
        }

        public override int ContentLength => fileBytes.Length;

        public override string FileName { get; }

        public override Stream InputStream { get; }

        public override void SaveAs(string filename)
        {
            using (FileStream file = new FileStream(filename, FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[InputStream.Length];
                InputStream.Read(bytes, 0, (int)InputStream.Length);
                file.Write(bytes, 0, bytes.Length);
                InputStream.Close();
            }
        }
    }
}