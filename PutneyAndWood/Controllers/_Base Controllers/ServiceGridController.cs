﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;
using SnapSuite.Internal;
using System.Web.Routing;
using System.Net;

namespace SnapSuite.Controllers
{
    public class ServiceGridController<TModel, TViewModel, TService> : ServiceEntityController<TModel, TViewModel, TService>
        where TModel : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
        where TService : class, IUserService, IService<TModel>, IViewModelService<TModel, TViewModel>
    {
        
        public ServiceGridController(TService service)
            :base(service)
        {
           
        }
        

        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _service.Read();
            var viewModels = models.ToList().Select(s => _service.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Create([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _service.Create(viewModel);
            }
            else
            {
                var errors = new List<string>();
                foreach (var key in ModelState.Keys)
                {
                    var item = ModelState[key];
                    if (item.Errors.Any()) errors.AddRange(item.Errors.Select(e => e.ErrorMessage).ToArray());
                }

                throw new Exception(string.Join(", ", errors.ToArray()));
                //return this.Json(new DataSourceResult { Errors = string.Join(", ", errors.ToArray()) });
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Update([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {                
                _service.Update(viewModel);
            }
            else
            {
                var errors = new List<string>();
                foreach (var key in ModelState.Keys)
                {
                    var item = ModelState[key];
                    if (item.Errors.Any()) errors.AddRange(item.Errors.Select(e => e.ErrorMessage).ToArray());
                }

                throw new Exception(string.Join(", ", errors.ToArray()));
                //return this.Json(new DataSourceResult { Errors = string.Join(", ", errors.ToArray()) });
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            //if (ModelState.IsValid)
            //{
                _service.Destroy(viewModel);
            //}

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public IQueryable<TModel> ApplyQueryFiltersSort([DataSourceRequest]DataSourceRequest request, IQueryable<TModel> query, string defaultSortColumn = "ID")
        {
            //Filtering            
            if (request.Filters.Any())
            {
                var filter = request.Filters.GetFilter<TModel>();
                query = query.Where(filter);
            }

            //Sorting
            if (request.Sorts.Any())
            {
                foreach (var x in request.Sorts)
                {
                    query = query.OrderBy(x.Member + ((x.SortDirection == ListSortDirection.Ascending) ? " ASC" : " DESC")) as IQueryable<TModel>;
                }
            }
            else
            {
                query = query.OrderBy(defaultSortColumn);
            }

            return query;
        }

    }
    
}