﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;
using SnapSuite.Internal;
using System.Web.Routing;

namespace SnapSuite.Controllers
{
    public class ServiceDeleteableGridController<TModel, TViewModel, TService> : ServiceGridController<TModel, TViewModel, TService>
        where TModel : class, ICompanyEntity, IDeletable, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
        where TService : class, IUserService, IService<TModel>, IViewModelService<TModel, TViewModel>, IDeletableService<TModel, TViewModel>
    {

        public ServiceDeleteableGridController(TService service)
            :base(service)
        {

        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Delete([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            _service.Delete(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        [HttpPost]
        public ActionResult _Delete(string IDs)
        {
            //IDs are passed as a string e.g.  1,2,4,5,6,7,8
            var Guids = IDs.Split(',').Select(p => Guid.Parse(p));
            _service.Delete(Guids);
            return Json("Success");
        }

        [HttpPost]
        public ActionResult _RestoreDeleted(Guid ID)
        {
            _service.Restore(ID);
            return Json("Success");
        }


        [HttpPost]
        public ActionResult _ClearDeleted()
        {
            _service.ClearDeleted();
            return Json("Success");
        }

    }
    
}