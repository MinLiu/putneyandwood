﻿using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SnapSuite.Controllers
{
    public class DropdownsController : BaseController
    {
      
        public JsonResult DropDownUsers()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .ToList()
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.ToFullName()
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersQuotations()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.AccessQuotations == true)
                                    .Select( u => new UserItemViewModel { ID = u.Id, Email = u.FirstName + " " + u.LastName + " - " + u.Email })
                                    .GroupBy(p => p.ID).Select(g => g.First()).ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersJobs()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.AccessJobs == true)
                                    .Select(u => new UserItemViewModel { ID = u.Id, Email = u.Email })
                                    .GroupBy(p => p.ID).Select(g => g.First()).ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersInvoices()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.AccessInvoices == true)
                                    .Select(u => new UserItemViewModel { ID = u.Id, Email = u.Email })
                                    .GroupBy(p => p.ID).Select(g => g.First()).ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersDeliveryNotes()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.AccessDeliveryNotes == true)
                                    .Select(u => new UserItemViewModel { ID = u.Id, Email = u.Email })
                                    .GroupBy(p => p.ID).Select(g => g.First()).ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersPurchaseOrders()
        {
            //Get the teams where the user is in     
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.AccessPurchaseOrders == true)
                                    .Select(u => new UserItemViewModel { ID = u.Id, Email = u.Email })
                                    .GroupBy(p => p.ID).Select(g => g.First()).ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DropDownUsersWithNotAssigned()
        {
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Add(new UserItemViewModel { ID = "-1", Email = "Not Assigned" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEventUsers()
        {
            //Get the teams where the user is in                
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersSearch()
        {
            //Get the teams where the user is in                
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();
            
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersAll()
        {
            //Get the teams where the user is in  
            

            var users = new UserService(new SnapDbContext()).GetAllCompanyUsers(CurrentUser.CompanyID)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DropDownCurrencies()
        {
            using (var context = new SnapDbContext())
            {
                var currencies = context.Currencies.Where(c => c.UseInCommerce == true)
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Code
                    }).ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DropDownModuleTypes()
        {
            using (var context = new SnapDbContext())
            {
                
                bool accessProducts = ViewBag.ViewProducts;
                bool accessCRM = ViewBag.ViewCRM;
                bool accessQuotations = ViewBag.ViewQuotations;

                var items = context.ModuleTypes
                    .Where(m => 
                        (m.ID == ModuleTypeValues.PRODUCTS && accessProducts)
                        || (m.ID == ModuleTypeValues.CLIENTS && accessCRM)
                        || (m.ID == ModuleTypeValues.QUOTATIONS && accessQuotations)
                    )
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Name
                    }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownProjects()
        {
            //Get the teams where the user is in     
            using (var context = new SnapDbContext())
            {

                var items = context.Projects
                    .Where(x =>x.Deleted == false)
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Number + " - " + c.Name
                    }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownEnquirySources()
        {
            //Get the teams where the user is in     
            using (var context = new SnapDbContext())
            {
                var items = context.EnquirySources
                    .OrderBy(c => c.SortPos)
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Description
                    })
                    .ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
        }
    }



}