﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using System.Web.Routing;

namespace SnapSuite.Controllers
{

    public class HelpTutorialsController : Controller
    {
        UserHelpTutorials HelpTutorials;
        EntityRespository<UserHelpTutorials> repo = new EntityRespository<UserHelpTutorials>(new SnapDbContext());

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (!User.Identity.IsAuthenticated)
                Response.Redirect("_Blank");

            var userId = User.Identity.GetUserId();
            var query = repo.Read().Where(u => u.UserId == userId);

            if (query.Count() > 0)
            {
                HelpTutorials = query.First();
            }
            else
            {
                try
                {
                    HelpTutorials = new UserHelpTutorials { UserId = userId };
                    repo.Create(HelpTutorials);
                }
                catch { }
            }
            
        }

        
        public ActionResult _DashboardStartVideo() 
        {
            if (!HelpTutorials.DoNotDashboardStartVideo) return PartialView();
            else return Content("");
        }
        
        [HttpPost]
        public ActionResult _DashboardStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotDashboardStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _ProductsStartVideo()
        {
            if (!HelpTutorials.DoNotProductsStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _ProductsStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotProductsStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _CRMStartVideo()
        {
            if (!HelpTutorials.DoNotCRMStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _CRMStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotCRMStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _QuotationsStartVideo()
        {
            if (!HelpTutorials.DoNotQuotationsStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _QuotationsStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotQuotationsStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _JobsStartVideo()
        {
            if (!HelpTutorials.DoNotJobsStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _JobsStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotJobsStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _InvoicesStartVideo()
        {
            if (!HelpTutorials.DoNotInvoicesStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _InvoicesStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotInvoicesStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _DeliveryNotesStartVideo()
        {
            if (!HelpTutorials.DoNotDeliveryNotesStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _DeliveryNotesStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotDeliveryNotesStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _PurchaseOrdersStartVideo()
        {
            if (!HelpTutorials.DoNotPurchaseOrdersStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _PurchaseOrdersStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotPurchaseOrdersStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }

        
        public ActionResult _StockControlStartVideo()
        {
            if (!HelpTutorials.DoNotStockControlStartVideo) return PartialView();
            else return Content("");
        }

        [HttpPost]
        public ActionResult _StockControlStartVideo(bool DoNotShowAgain)
        {
            HelpTutorials.DoNotStockControlStartVideo = DoNotShowAgain;
            repo.Update(HelpTutorials);
            return Json("Success");
        }


        public ActionResult _Blank()
        {
            return Content("");
        }

    }

    
}