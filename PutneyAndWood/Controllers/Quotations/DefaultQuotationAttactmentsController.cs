﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class DefaultQuotationAttachmentsController : ServiceGridController<DefaultQuotationAttachment, DefaultQuotationAttachmentViewModel, DefaultQuotationAttachmentService<DefaultQuotationAttachmentViewModel>>
    {

        public DefaultQuotationAttachmentsController()
            : base(new DefaultQuotationAttachmentService<DefaultQuotationAttachmentViewModel>(Guid.Empty, null, new DefaultQuotationAttachmentMapper(), new SnapDbContext()))
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid settingsID)
        {
            var list = _service.Read(settingsID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<DefaultQuotationAttachmentViewModel>().ToDataSourceResult(request));
        }
        

        public ActionResult UploadDefaultAttachment(IEnumerable<HttpPostedFileBase> files, Guid settingsID)
        {
            if (files != null)
            {                
                foreach (var file in files)
                {
                    _service.UploadAttachment(settingsID, file);
                }
            }

            // Return an empty string to signify success
            return Content("");
        }


        [HttpPost]
        public ActionResult LinkDefaultAttachment(Guid settingsID, string link)
        {
            _service.LinkAttachment(settingsID, link);
            return JavaScript("");
        }

    }

}