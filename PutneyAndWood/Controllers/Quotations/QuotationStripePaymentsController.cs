﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;


namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationStripePaymentsController : ServiceEntityController<QuotationStripePayment, QuotationStripePaymentViewModel, QuotationStripePaymentService<QuotationStripePaymentViewModel>>
    {

        public QuotationStripePaymentsController()
            : base(new QuotationStripePaymentService<QuotationStripePaymentViewModel>(Guid.Empty, null, new QuotationStripePaymentMapper(), new SnapDbContext()))
        {

        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Quotations");
        }

        

        public ActionResult Edit(Guid ID)
        {
            var model = _service.FindByID(ID) ?? new QuotationStripePayment { };
            var viewModel = _service.MapToViewModel(model);
            ViewBag.OwnerID = ID.ToString();

            return PartialView(viewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuotationStripePaymentViewModel viewModel, string newPayment, string delete)
        {
            QuotationStripePayment model = null;

            if (!string.IsNullOrEmpty(newPayment))
            {
                var settings = CurrentUser.Company.QuotationSettings;
                model = _service.Create(new QuotationStripePayment
                {
                    OwnerID = viewModel.OwnerID,
                    Description = settings.DefaultStripeDescription,
                    FixedAmount = settings.DefaultStripeFixedAmount,
                    PercentageToPay = settings.DefaultStripePercentageToPay,
                    AmountToPay = settings.DefaultStripeAmountToPay
                });

                return PartialView(_service.MapToViewModel(model));
            }
            else if (!string.IsNullOrEmpty(delete))
            {
                model = _service.FindByID(viewModel.OwnerID);
                _service.Destroy(model);
                return Edit(viewModel.OwnerID);
            }
            else if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            model = _service.FindByID(viewModel.OwnerID);
            _service.Mapper.MapToModel(viewModel, model);
            _service.Update(model);
            ViewBag.StripeSaved = true;
            return Edit(viewModel.OwnerID);
        }
        

    }
}
