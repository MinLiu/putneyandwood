﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{

    [AllowAnonymous]
    public class ClientQuotationItemsController : ServiceGridController<QuotationItem, QuotationItemClientViewModel, QuotationItemService<QuotationItemClientViewModel>>
    {

        public ClientQuotationItemsController()
            : base(new QuotationItemService<QuotationItemClientViewModel>(Guid.Empty, Guid.Empty, null, new QuotationItemClientMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID, Guid sectionID)
        {
            var quote = new QuotationService<EmptyViewModel>(Guid.Empty, null, null, _service.DbContext).FindByID(quoteID);

            List<QuotationItemClientViewModel> list = new List<QuotationItemClientViewModel>();
            var query = _service.Read(quoteID).Where(p => p.SectionID == sectionID && p.HideOnPdf == false)
                                              .Include(o => o.PriceBreaks)
                                              .OrderBy(o => o.SortPos)
                                              .ThenBy(o => o.ParentItemID != null);

            if (quote.UsePriceBreaks)
            {
                var mapper = new QuotationItemPriceBreakClientMapper();
                foreach (var i in query.ToList())
                {
                    list.Add(_service.MapToViewModel(i));
                    list.AddRange(i.PriceBreaks.OrderBy(p => p.Quantity).ToArray().Select(p => mapper.MapToViewModel(p)).ToArray());
                }
            }
            else
            {                
                list = query.ToList().Select(p => _service.MapToViewModel(p)).ToList();
            }
            
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationItemViewModel>().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadImages([DataSourceRequest]DataSourceRequest request, Guid ID)
        {
            var imgService = new QuotationItemImageService<ImageViewModel>(Guid.Empty, null, null, new QuotationItemImageMapper(), new SnapDbContext());
            var list = imgService.Read(ID).Where(p => p.OwnerID == ID).OrderBy(p => p.SortPos)
                                   .ToList().Select(p => imgService.MapToViewModel(p)).ToList();

            return Json(list.ToDataSourceResult(request));
        }


        
        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Update(DataSourceRequest request, QuotationItemClientViewModel viewModel)
        {
            if (viewModel.IsPriceBreak)
            {
                //Do nothing
            }
            else
            {
                _service.Update(viewModel);
            }
            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Destroy(DataSourceRequest request, QuotationItemClientViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        
    }

}