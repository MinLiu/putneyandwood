﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Super User,Owner,Admin")]
    public class QuotationSettingsController : ServiceEntityController<QuotationSettings, QuotationSettingsViewModel, QuotationSettingsService<QuotationSettingsViewModel>>
    {


        public QuotationSettingsController()
            : base(new QuotationSettingsService<QuotationSettingsViewModel>(null, new QuotationSettingsMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            _service.CurrentUserID = CurrentUser.Id;
        }

        // GET: Settings
        public ActionResult Edit(string QuotationID)
        {
            var model = _service.FindByID(CurrentUser.CompanyID);
            var viewModel = _service.MapToViewModel(model);
            ViewBag.QuotationID = QuotationID;

            viewModel.CustomProducFields = new CustomProductFieldService<EmptyViewModel>(CurrentUser.CompanyID, null, null, _service.DbContext).Read().ToList();

            return View(viewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuotationSettingsViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Edit");

            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var model = _service.FindByID(CurrentUser.CompanyID);
            _service.Mapper.MapToModel(viewModel, model);
            _service.Update(model);
            ViewBag.SuccessMessage = "Settings Saved.";

            var customService = new CustomProductFieldService<EmptyViewModel>(CurrentUser.CompanyID, null, null, _service.DbContext);
            foreach(var item in viewModel.CustomProducFields)
            {
                var update = customService.FindByID(item.ID);
                update.ShowQuotations = item.ShowQuotations;
                customService.Update(update, new string[] { "ShowQuotations" });
            }

            return View(viewModel);
        }


    }
}