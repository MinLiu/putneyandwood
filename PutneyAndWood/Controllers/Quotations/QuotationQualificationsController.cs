﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationQualificationsController : GridController<QuotationQualification, QuotationQualificationViewModel>
    {

        public QuotationQualificationsController()
            : base(new QuotationQualificationRepository(), new QuotationQualificationMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID)
        {
           
            var list = _repo.Read()
                            .Where(x => x.QuotationID == quoteID)
                            .OrderBy(o => o.SortPos)
                            .ToList()
                            .Select(p => _mapper.MapToViewModel(p)).ToList(); 

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationCostItemViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, QuotationQualificationViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var last = _repo.Read().Where(x => x.QuotationID.ToString() == viewModel.QuotationID).OrderByDescending(x => x.SortPos).FirstOrDefault();

                var model = _mapper.MapToModel(viewModel);
                model.SortPos = last != null ? last.SortPos + 1 : 1;
                _repo.Create(model);

                viewModel.ID = model.ID.ToString();

                UpdateQuotationLastUpdateTime(model.QuotationID);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, QuotationQualificationViewModel viewModel)
        {
            UpdateQuotationLastUpdateTime(Guid.Parse(viewModel.QuotationID));
            return base.Update(request, viewModel);
        }

        public override JsonResult Destroy([DataSourceRequest] DataSourceRequest request, QuotationQualificationViewModel viewModel)
        {
            UpdateQuotationLastUpdateTime(Guid.Parse(viewModel.QuotationID));
            return base.Destroy(request, viewModel);
        }

        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid qualificationID, int oldIndex, int newIndex)
        {
           
            UpdatePositions(qualificationID, oldIndex, newIndex);

            return Json("Success");
        }

        public bool UpdatePositions(Guid qualificationID, int oldPosition, int newPosition)
        {
            QuotationQualification move = null;
            QuotationQualification top = null;
            QuotationQualification bottom = null;

            move = _repo.Find(qualificationID);

            var items = _repo.Read().Where(i => i.QuotationID == move.QuotationID).OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            top = (newPosition - 1 >= 0) ? items.ElementAt(newPosition - 1) : null;
            bottom = (newPosition < items.Count) ? items.ElementAt(newPosition) : null;

            items.Insert((newPosition >= 0) ? newPosition : 0, move);

            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
            }
            _repo.Update(items, new string[] { "SortPos" });

            UpdateQuotationLastUpdateTime(move.QuotationID);

            return true;
        }

        private void UpdateQuotationLastUpdateTime(Guid quoteID)
        {
            // Update quotation to make sure it generate PDF
            var quotatoinservice = new QuotationService<EmptyViewModel>(CurrentUser.CompanyID, CurrentUser.Id, null, new SnapDbContext());
            var quote = quotatoinservice.FindByID(quoteID);
            quote.LastUpdated = DateTime.Now;
            quotatoinservice.Update(quote, new string[] { "LastUpdated" });
        }

    }

}