﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Controllers
{

   
    [AllowAnonymous]
    public class ClientQuotationSectionsController : ServiceEntityController<QuotationSection, QuotationSectionClientViewModel, QuotationSectionService<QuotationSectionClientViewModel>> 
    {

        public ClientQuotationSectionsController()
            : base(new QuotationSectionService<QuotationSectionClientViewModel>(Guid.Empty, null, null, new QuotationSectionClientMapper(), new SnapDbContext()))
        {

        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditSection(QuotationSectionClientViewModel viewModel)
        {
            var model = _service.FindByID(Guid.Parse(viewModel.SectionID));
            _service.Mapper.MapToModel(viewModel, model);
            _service.Update(model);

            return JavaScript("RefreshHeader(); RefreshSectionTotals();");
        }
        
    }
}