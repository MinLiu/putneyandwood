﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Super User,Owner,Admin")]
    public class QuotationTemplatesController : ServiceGridController<QuotationTemplate, QuotationTemplateViewModel, QuotationTemplateService<QuotationTemplateViewModel>>
    {

        public QuotationTemplatesController()
            : base(new QuotationTemplateService<QuotationTemplateViewModel>(Guid.Empty, null, new QuotationTemplateMapper(), new SnapDbContext()))
        {

        }

        public ActionResult Index()
        {   
            return View();
        }


        public ActionResult Upload()
        {   
            var viewModel = new TemplateUploadViewModel
            {
                Name = "New Template",
                Type = TemplateType.Body.ToString(),
                Stage = 1,
            };
            
            return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(TemplateUploadViewModel viewModel, HttpPostedFileBase uploadedTemplate, string NextStage, string PreviousStage)

        {
            ModelState.Clear();

            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Unknown Error.";
                return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
            }

            if (!string.IsNullOrEmpty(NextStage))
            {
                viewModel.Stage++;
                return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
            }
            else if (!string.IsNullOrEmpty(PreviousStage))
            {
                viewModel.Stage--;
                return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
            }
            else if (uploadedTemplate == null)
            {
                ViewBag.ErrorMessage = "Not template file was uploaded";
                return View(string.Format("Upload{0}{1}", viewModel.Type, viewModel.Stage), viewModel);
            }

            try
            {
                viewModel.Format = _service.UploadTemplate(uploadedTemplate);
                viewModel.Name = Path.GetFileNameWithoutExtension(uploadedTemplate.FileName);
                ViewBag.SuccessMessage = "Upload Successful.";
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
        }
        

        public ActionResult UploadFinish(TemplateUploadViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Unknown Error.";
                return View(string.Format("Upload{0}", viewModel.Stage), viewModel);
            }

            _service.CompleteTemplateUpload(viewModel.Format, (TemplateType)(Enum.Parse(typeof(TemplateType), viewModel.Type)), viewModel.Name);
            return RedirectToAction("Index");
        }


        public ActionResult PreviewTemplate(TemplateFormat format, string id = "")
        {
            var pdfService = new QuotationPdfGenService(CurrentUser.CompanyID, null, new SnapDbContext());

            if (string.IsNullOrEmpty(id))
            {
                return File(pdfService.GenerateSamplePreview(_service.GetTemplatePhysicalFilePath(format)), "application/pdf");
            }
            else
            {
                return File(pdfService.GenerateBodyPDF(Guid.Parse(id), _service.GetTemplatePhysicalFilePath(format)), "application/pdf");
            }
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, TemplateType type)
        {
            var list = _service.Read().Where(p => p.Type == type)
                .OrderBy(o => o.Filename)
                .ToList().Select(p => _service.MapToViewModel(p));

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationTemplateViewModel>().ToDataSourceResult(request));
        }
        


        public JsonResult DropDownQuotations()
        {
            var users = new UserService(new SnapDbContext()).GetCompanyUsers(CurrentUser).Select(u => u.Id);

            var list = new QuotationRepository().Read().Where(
                q => q.CompanyID == CurrentUser.CompanyID
                && (users.Contains(q.AssignedUserID) || q.AssignedUserID == null))
                .Include(q => q.Client)
                .OrderByDescending(o => o.Created)
                .Take(5)
                .ToList()
                .Select(p => new SelectItemViewModel { ID = p.ID.ToString(), Name = p.ToLongLabel() });

            return Json(list, JsonRequestBehavior.AllowGet);

        }
        

    }

}