﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Controllers
{

   
    [Authorize]
    public class QuotationSectionsController : ServiceEntityController<QuotationSection, QuotationSectionViewModel, QuotationSectionService<QuotationSectionViewModel>> 
    {

        public QuotationSectionsController()
            : base(new QuotationSectionService<QuotationSectionViewModel>(Guid.Empty, null, null, new QuotationSectionMapper(), new SnapDbContext()))
        {

        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditSection(QuotationSectionViewModel viewModel)
        {
            var model = _service.FindByID(Guid.Parse(viewModel.SectionID));
            _service.Mapper.MapToModel(viewModel, model);
            _service.Update(model);

            return JavaScript("RefreshHeader(); ShowSavedMessage();");
            //return Json(new { status = "success", id = model.QuotationID.ToString() }, "text/plain");
        }


        [HttpPost]
        public ActionResult _AddSection(Guid quoteID)
        {
            
            _service.ParentID = quoteID;
            var model = new QuotationSection
            {
                QuotationID = quoteID,
                IncludeInTotal = true,
            };

            _service.Create(model);

            //return JavaScript(" ShowSavedMessage(); RefreshSections(); RefreshHeader();");
            return Json(new { status = "success", id = model.QuotationID.ToString() }, "text/plain");
        }

        [HttpPost]
        public ActionResult _CopySection(Guid sectionID)
        {
            var model = _service.Copy(sectionID);
            return Json(new { status = "success", id = model.QuotationID.ToString() }, "text/plain");
        }


        [HttpPost]
        public ActionResult _RemoveSection(Guid sectionID)
        {
            var model = _service.FindByID(sectionID);
            _service.Destroy(model);

            //return JavaScript("RefreshSections(); RefreshHeader();");
            return Json(new { status = "success", id = model.QuotationID.ToString() }, "text/plain");
        }


        [HttpPost]
        public ActionResult _ReorderSection(Guid quoteID, Guid sectionID, string direction)
        {
            _service.ParentID = quoteID;
            _service.UpdatePositions(sectionID, direction);

            return Json(new { status = "success", id = sectionID }, "text/plain");
        }


    }
}