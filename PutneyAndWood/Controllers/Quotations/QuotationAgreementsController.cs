﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace SnapSuite.Controllers
{
    public class QuotationAgreementsController : ServiceEntityController<QuotationSignedAgreement, EmptyViewModel, QuotationAgreementService<EmptyViewModel>>
    {

        public QuotationAgreementsController()
            : base(new QuotationAgreementService<EmptyViewModel>(Guid.Empty, null, null, new SnapDbContext()))
        {

        }
                
  
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Quotations");
        }
        

        [AllowAnonymous]
        public ActionResult _AcceptReject(Guid ID, string Jasq = "")
        {
            var quoteService = new QuotationService<QuotationViewModel>(Guid.Empty, null, new QuotationMapper(), _service.DbContext);
            var model = quoteService.Read(Guid.Empty)
                                    .Where(i => i.ID == ID && i.CheckSum == Jasq)                                    
                                    .Include(i => i.SignedAgreement)
                                    .DefaultIfEmpty(null)
                                    .FirstOrDefault();


            if (model == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {               
                if (model.SignedAgreement == null)
                {
                    var viewModel = new SubmitAgreementViewModel { ID = model.ID.ToString() };
                    return PartialView(viewModel);
                }
                else {
                    return Content("");
                }
            }
        }


        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AcceptReject(SubmitAgreementViewModel viewModel, int tabIndex,  string signatureFile, HttpPostedFileBase uploadSignature)
        {
            Session.Timeout = 60;

            try
            {
                if ((tabIndex == 0 || tabIndex == 1) && string.IsNullOrWhiteSpace(signatureFile)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No signature was uploaded");
                if (tabIndex == 2 && uploadSignature == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No signature was uploaded");
                if (tabIndex == 2 && !uploadSignature.ContentType.Contains("image")) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Uploaded signature was is not an Image.");
                if (tabIndex == 2 && uploadSignature.ContentLength > 1 * 1024 * 1024) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Uploaded signature is too big.");

                if (string.IsNullOrWhiteSpace(viewModel.Title)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The Title field is required.");
                if (string.IsNullOrWhiteSpace(viewModel.FirstName)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The First Name field is required.");
                if (string.IsNullOrWhiteSpace(viewModel.LastName)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The Last Name field is required.");
                if (string.IsNullOrWhiteSpace(viewModel.Email)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The Email field is required.");
                if (new EmailAddressAttribute().IsValid(viewModel.Email) == false) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The Email is not valid.");
                if (!viewModel.Accept) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "You have not accepted the terms and conditions.");


                if (tabIndex == 0 || tabIndex == 1)
                {
                    byte[] signatureBytes = Convert.FromBase64String(signatureFile);
                    _service.AcceptAgreement(Guid.Parse(viewModel.ID),
                                                viewModel.Title,
                                                viewModel.FirstName,
                                                viewModel.LastName,
                                                viewModel.Email,
                                                viewModel.DeviceInformation,
                                                HttpContext.Request.UserHostAddress,
                                                viewModel.City,
                                                viewModel.Country,
                                                viewModel.Latitude,
                                                viewModel.Longitude,
                                                "signature",
                                                ".png",
                                                signatureBytes,
                                                tabIndex == 1,
                                                viewModel.PoNumber);
                }
                else
                {
                    _service.AcceptAgreement(Guid.Parse(viewModel.ID),
                                                viewModel.Title,
                                                viewModel.FirstName,
                                                viewModel.LastName,
                                                viewModel.Email,
                                                viewModel.DeviceInformation,
                                                HttpContext.Request.UserHostAddress,
                                                viewModel.City,
                                                viewModel.Country,
                                                viewModel.Latitude,
                                                viewModel.Longitude,
                                                "signature",
                                                Path.GetExtension(uploadSignature.FileName),
                                                uploadSignature.ToByteArray(),
                                                false,
                                                viewModel.PoNumber);
                }

                return Content("Success");
                //return PartialView("_AcceptRejectComplete");

            }
            catch (Exception ex) {
                SaveExceptionError(ex);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        

    }
}
