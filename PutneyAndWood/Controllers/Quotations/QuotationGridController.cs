﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Kendo.Mvc;
using System.ComponentModel;

namespace SnapSuite.Controllers
{


    [Authorize]
    public class QuotationGridController : ServiceDeleteableGridController<Quotation, QuotationGridViewModel, QuotationService<QuotationGridViewModel>>
    {

        public QuotationGridController()
            : base(new QuotationService<QuotationGridViewModel>(Guid.Empty, null, new QuotationGridMapper(), new SnapDbContext()))
        {

        }

        public void SaveCookies(string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD, int page, int pageSize, string sort, string sortDirection)
        {
            //Save Search Settings in Cookies
            HttpCookie cookie = Request.Cookies["QuotationSearch"] ?? new HttpCookie("QuotationSearch");
            cookie["Filter"] = filterText;
            cookie["AssignedUser"] = assignedUser;
            cookie["Statuses"] = statuses;
            cookie["DateType"] = dateType.ToString();
            cookie["FromD"] = fromD;
            cookie["ToD"] = toD;
            cookie["Page"] = page.ToString();
            cookie["PageSize"] = pageSize.ToString();
            cookie["Sort"] = sort;
            cookie["SortDirection"] = sortDirection;

            cookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(cookie);
        }


        // READ: GridQuotations
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD)
        //{

        //    if (request.Sorts.Count() <= 0) SaveCookies(filterText, assignedUser, statuses, dateType, fromD, toD, request.Page, request.PageSize, "", "");
        //    else SaveCookies(filterText, assignedUser, statuses, dateType, fromD, toD, request.Page, request.PageSize, request.Sorts[0].Member, (request.Sorts[0].SortDirection == ListSortDirection.Ascending) ? "asc" : "desc");


        //    var orders = _service.Search(filterText, (DateType)dateType, !string.IsNullOrEmpty(statuses) ? statuses.Split(',').Select(i => int.Parse(i)).ToList() : new List<int>(), fromD, toD, assignedUser);


        //    //Correct sorts
        //    for (int x = request.Sorts.Count() - 1; x >= 0; x--)
        //    {
        //        if (request.Sorts[x].Member == "Reference") request.Sorts[x].Member = "Number";
        //        if (request.Sorts[x].Member == "ClientName") request.Sorts[x].Member = "Client.Name";
        //        if (request.Sorts[x].Member == "Currency") request.Sorts[x].Member = "Currency.Code";
        //    }

        //    orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

        //    var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _service.MapToViewModel(q)).ToList();
        //    KendoDataSourceResult result = new KendoDataSourceResult { Data = list, Total = orders.Count(), TotalNet = string.Format("{0:0.00}", (orders.Count() <= 0) ? 0 : orders.Sum(q => q.TotalNet)) };
        //    return Json(result);

        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, Guid? projectID, string filterText, string statuses)
        {
            if (projectID == null)
            { 
                SaveCookies(filterText, "", statuses, 0, DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"), request.Page, request.PageSize, "", "");
            }
            var orders = _service.Search(projectID, filterText, statuses);
            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "Reference") request.Sorts[x].Member = "Number";
                if (request.Sorts[x].Member == "ClientName") request.Sorts[x].Member = "Client.Name";
                if (request.Sorts[x].Member == "Currency") request.Sorts[x].Member = "Currency.Code";
            }
            orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

            var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _service.MapToViewModel(q)).ToList();
            DataSourceResult result = new DataSourceResult { Data = list, Total = orders.Count()};
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReadDeleted([DataSourceRequest]DataSourceRequest request, string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD)
        {
            var orders = _service.Search(filterText, (DateType)dateType, !string.IsNullOrEmpty(statuses) ? statuses.Split(',').Select(i => int.Parse(i)).ToList() : new List<int>(), fromD, toD, assignedUser, true);

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "Reference") request.Sorts[x].Member = "Number";
                if (request.Sorts[x].Member == "ClientName") request.Sorts[x].Member = "Client.Name";
                if (request.Sorts[x].Member == "Currency") request.Sorts[x].Member = "Currency.Code";
            }

            orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

            var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _service.MapToViewModel(q)).ToList();
            KendoDataSourceResult result = new KendoDataSourceResult { Data = list, Total = orders.Count(), TotalNet = string.Format("{0:0.00}", (orders.Count() <= 0) ? 0 : orders.Sum(q => q.TotalNet)) };
            return Json(result);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReadClientAssociated([DataSourceRequest]DataSourceRequest request, DateType dateType, string fromD, string toD, Guid cltID)
        {
            var orders = _service.Search(dateType, fromD, toD).Where(q => q.ClientID == cltID);

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "Reference") request.Sorts[x].Member = "Number";
                if (request.Sorts[x].Member == "ClientName") request.Sorts[x].Member = "Client.Name";
                if (request.Sorts[x].Member == "Currency") request.Sorts[x].Member = "Currency.Code";
            }

            orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

            var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _service.MapToViewModel(q)).ToList();
            KendoDataSourceResult result = new KendoDataSourceResult { Data = list, Total = orders.Count(), TotalNet = string.Format("{0:0.00}", (orders.Count() <= 0) ? 0 : orders.Sum(q => q.TotalNet)) };
            return Json(result);
        }


        public ActionResult ReadAssociate([DataSourceRequest]DataSourceRequest request, string filterText, int currencyID)
        {


            var orders = _service.Search(filterText, DateType.AllTime, null, null, null)
                                 .Where(q => q.StatusID != QuotationStatusValues.DRAFT && q.CurrencyID == currencyID);
            

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "Reference") request.Sorts[x].Member = "Number";
                if (request.Sorts[x].Member == "ClientName") request.Sorts[x].Member = "Client.Name";
                if (request.Sorts[x].Member == "Currency") request.Sorts[x].Member = "Currency.Code";
            }

            orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

            var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _service.MapToViewModel(q)).ToList();
            DataSourceResult result = new DataSourceResult { Data = list, Total = orders.Count() };
            return Json(result);

        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<QuotationViewModel>().ToDataSourceResult(request));
        }
        


        public FileResult Export(string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD)
        {

            var export = _service.Export(filterText, (DateType)dateType, statuses.Split(',').Select(i => int.Parse(i)).ToList(), fromD, toD, assignedUser);
            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user
            
        }
        

    }
}