﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationEventsController : GridController<QuotationEvent, QuotationEventViewModel>
    {

        public QuotationEventsController()
            : base(new QuotationEventRepository(), new QuotationEventMapper())
        {

        }


        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult ReadList([DataSourceRequest]DataSourceRequest request, Guid cltID)
        //{
        //    var list = _service.Read().Where(i => i.ClientID == cltID)
        //        .Include(p => p.Client)
        //        .Include(p => p.ClientContact)
        //        .Include(t => t.ClientContact)
        //        .Include(t => t.AssignedUser)
        //        .Include(t => t.Creator)
        //        .OrderBy(o => o.Timestamp).ToList().Select(p => _service.MapToViewModel(p));
        //    return Json(list.ToDataSourceResult(request));
        //}

      

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string projectID, string quoteID, string clientID, string fromD, string toD, DateType dateType = DateType.AllTime, bool showComplete = true)
        {
            DateTime fmDate = DateTime.Today;
            DateTime tDate = DateTime.Today;

            dateType.GetDates(ref fmDate, ref tDate, fromD, toD);

            var list = _repo.Read()
                            .Where(t => t.Timestamp >= fmDate && t.Timestamp <= tDate)
                            .Where(t => showComplete == true || (showComplete == false && t.Complete == false))
                            .Where(t => quoteID == null || quoteID == "" || t.QuotationID.ToString() == quoteID)
                            .Where(t => projectID == null || projectID == "" || t.Quotation.ProjectID.ToString() == projectID)
                            .Where(t => clientID == null || clientID == "" || t.Quotation.ClientID.ToString() == clientID)
                            .OrderByDescending(t => t.Timestamp);

            var taskList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(t => _mapper.MapToViewModel(t));
            DataSourceResult result = new DataSourceResult { Data = taskList.ToList(), Total = list.Count() };
            return Json(result);

        }
        

    }

}