﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationLabourItemsController : ServiceGridController<QuotationLabourItem, QuotationLabourItemViewModel, QuotationLabourItemService<QuotationLabourItemViewModel>>
    {

        public QuotationLabourItemsController()
            : base(new QuotationLabourItemService<QuotationLabourItemViewModel>(Guid.Empty, Guid.Empty, null, new QuotationLabourItemMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID)
        {
           
            var list = _service.Read(quoteID).OrderBy(o => o.SortPos)
                                            .ToList()
                                            .Select(p => _service.MapToViewModel(p)).ToList(); 

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationLabourItemViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, QuotationLabourItemViewModel viewModel)
        {
            throw new NotImplementedException();
        }


        public override JsonResult Update(DataSourceRequest request, QuotationLabourItemViewModel viewModel)
        {
            _service.Update(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
                
        

        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid ID, Guid itemID, int oldIndex, int newIndex)
        {
            _service.ParentID = ID;
            _service.UpdatePositions(itemID, oldIndex, newIndex);
            return Json("Success");
        }
        

    }

}