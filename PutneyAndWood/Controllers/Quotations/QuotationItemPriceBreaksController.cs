﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationItemPriceBreaksController : ServiceGridController<QuotationItemPriceBreak, QuotationItemPriceBreakViewModel, QuotationItemPriceBreakService<QuotationItemPriceBreakViewModel>>
    {

        public QuotationItemPriceBreaksController()
            : base(new QuotationItemPriceBreakService<QuotationItemPriceBreakViewModel>(Guid.Empty, Guid.Empty, null, new QuotationItemPriceBreakMapper(), new SnapDbContext()))
        {

        }

        public void SetColumnViewBags()
        {
            var settings = new QuotationSettingsService<EmptyViewModel>(_service.CurrentUserID, null, _service.DbContext).FindByID(CurrentUser.CompanyID);

            //Set editor settings based on the settings.
            //Note: There is a check to see if the values are null. This make overriding the function easier.
            ViewBag.ShowPhoto = settings.ShowPhoto;
            ViewBag.ShowProductCode = settings.ShowProductCode;
            ViewBag.ShowUnit = settings.ShowUnit;
            ViewBag.ShowMarkup = settings.ShowMarkup;
            ViewBag.ShowVAT = settings.ShowVAT;
            ViewBag.ShowDiscount = settings.ShowDiscount;
            ViewBag.ShowListPrice = settings.ShowListPrice;
            ViewBag.ShowCost = settings.ShowCost;
            ViewBag.ShowMargin = settings.ShowMargin;
            ViewBag.ShowMarginPercentage = settings.ShowMarginPercentage;
            ViewBag.ShowSetupCost = settings.ShowSetupCost;
            ViewBag.ShowCategory = settings.ShowCategory;
            //ViewBag.ShowVendorCode = settings.ShowVendorCode;
            //ViewBag.ShowManufacturer = settings.ShowManufacturer;
            ViewBag.ShowAccountCode = settings.ShowAccountCode;
            ViewBag.DefaultHourlyRate = settings.DefaultHourlyRate;            
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteItemID)
        {
            var item = new QuotationItemService<EmptyViewModel>(_service.CompanyID, Guid.Empty, null, null, _service.DbContext).FindByID(quoteItemID);
            var list = _service.Read(quoteItemID).OrderBy(p => p.Quantity).ToList().Select(p => _service.MapToViewModel(p)).ToList();

            var itemViewModel = new QuotationItemPriceBreakMapper().MapItemToViewModel(item);
            list.Insert(0, itemViewModel);

            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<QuotationItemViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, QuotationItemPriceBreakViewModel viewModel)
        {
            throw new Exception();
        }


        public override JsonResult Update(DataSourceRequest request, QuotationItemPriceBreakViewModel viewModel)
        {
            var itemService = new QuotationItemService<EmptyViewModel>(_service.CompanyID, Guid.Empty, null, null, _service.DbContext);

            if (viewModel.IsMainPrice)
            {   
                var model = itemService.FindByID(Guid.Parse(viewModel.QuotationItemID));

                model.SetupCost = viewModel.SetupCost;
                model.VAT = viewModel.VAT;
                model.Markup = viewModel.Markup;
                model.Discount = viewModel.Discount;
                model.Quantity = viewModel.Quantity;
                model.ListPrice = viewModel.ListPrice;
                model.Price = viewModel.Price;
                model.Cost = viewModel.Cost;
                model.Margin = viewModel.Margin;
                model.MarginPrice = viewModel.MarginPrice;
                model.Total = viewModel.Total;

                itemService.Update(model);
            }
            else
            {
                _service.Update(viewModel);
            }

            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        


        public ActionResult _EditQuotationItem(Guid quoteItemID)
        {
            SetColumnViewBags();

            var itemService = new QuotationItemService<QuotationItemViewModel>(_service.CompanyID, Guid.Empty, null, new QuotationItemMapper(), _service.DbContext);
            var item = itemService.FindByID(quoteItemID);
            var viewModel = itemService.MapToViewModel(item);

            return PartialView(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditQuotationItem(QuotationItemViewModel viewModel)
        {
            SetColumnViewBags();
            var itemService = new QuotationItemService<QuotationItemViewModel>(_service.CompanyID, Guid.Empty, null, new QuotationItemMapper(), _service.DbContext);
            itemService.Update(viewModel, false, false, true,  new string[] { "ProductCode", "Description", "Category", "VendorCode", "Manufacturer", "Unit", "AccountCode" });

            ViewBag.Saved = true;
            return JavaScript("ShowSavedMessage(); RefreshItems();");
        }




        [HttpPost]
        public ActionResult _AddPriceBreak(Guid quoteID, Guid id, decimal quantity)
        {   
            _service.AddPriceBreak(quoteID, id, quantity);

            return Json("");
        }


    }

}