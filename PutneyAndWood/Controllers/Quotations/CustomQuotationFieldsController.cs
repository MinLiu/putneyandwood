﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class CustomQuotationFieldsController : ServiceGridController<CustomQuotationField, CustomQuotationFieldViewModel, CustomQuotationFieldService<CustomQuotationFieldViewModel>>
    {

        public CustomQuotationFieldsController()
            : base(new CustomQuotationFieldService<CustomQuotationFieldViewModel>(Guid.Empty, null, new CustomQuotationFieldMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _service.Read().OrderBy(p => p.SortPos).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, CustomQuotationFieldViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = new List<string>();
                foreach (var key in ModelState.Keys)
                {
                    var item = ModelState[key];
                    if (item.Errors.Any()) errors.AddRange(item.Errors.Select(e => e.ErrorMessage).ToArray());
                }

                return this.Json(new DataSourceResult { Errors = string.Join(", ", errors.ToArray()) });
            }

            return base.Create(request, viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Update([DataSourceRequest] DataSourceRequest request, CustomQuotationFieldViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = new List<string>();
                foreach (var key in ModelState.Keys)
                {
                    var item = ModelState[key];
                    if (item.Errors.Any()) errors.AddRange(item.Errors.Select(e => e.ErrorMessage).ToArray());
                }

                return this.Json(new DataSourceResult { Errors = string.Join(", ", errors.ToArray()) });
            }

            return base.Update(request, viewModel);
        }


        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid itemID, int oldIndex, int newIndex)
        {
            _service.UpdatePositions(itemID, oldIndex, newIndex);
            return Json("Success");
        }


        public JsonResult DropDownCustomQuotationField()
        {
            var labour = _service.Read()
                .OrderBy(t => t.SortPos).ToArray()
                .Select(a => new SelectItemViewModel
                {
                    ID = a.ID.ToString(),
                    Name = a.ToShortLabel()
                }
            ).ToList();
            
            return Json(labour, JsonRequestBehavior.AllowGet);
        }

    }

}