﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationCostItemsController : ServiceGridController<QuotationCostItem, QuotationCostItemViewModel, QuotationCostItemService<QuotationCostItemViewModel>>
    {

        public QuotationCostItemsController()
            : base(new QuotationCostItemService<QuotationCostItemViewModel>(Guid.Empty, Guid.Empty, null, new QuotationCostItemMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID)
        {
           
            List<QuotationCostItemViewModel> list = _service.Read(quoteID)
                                    .OrderBy(o => o.SortPos)
                                    .ThenBy(o => o.ParentCostItemID != null)
                                    .ToList()
                                    .Select(p => _service.MapToViewModel(p)).ToList(); 

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationCostItemViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, QuotationCostItemViewModel viewModel)
        {
            //Items created directly on the items grid are comments
            viewModel.IsComment = true;
            return base.Create(request, viewModel);
        }


        public override JsonResult Update(DataSourceRequest request, QuotationCostItemViewModel viewModel)
        {                    
            _service.Update(viewModel);
            if (viewModel.Information.Description == null) viewModel.Information.Description = "";
            if (viewModel.Information.Category == null) viewModel.Information.Category = "";
            //if (viewModel.Information.VendorCode == null) viewModel.Information.VendorCode = "";
            //if (viewModel.Information.Manufacturer == null) viewModel.Information.Manufacturer = "";
            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        


        [HttpPost]
        public ActionResult _AddBlankItem(Guid quoteID)
        {            
            
            var comment = new QuotationCostItem
            {
                QuotationID = quoteID,
                IsComment = true,
                Description = "",
            };

            _service.Create(comment);
            
            return Json("");
        }



        [HttpPost]
        public ActionResult _ConvertItemToCustom(Guid id)
        {            
            _service.ConvertItemToCustom(id);            
            return Json("Converted to custom item.");
        }


        [HttpPost]
        public ActionResult _ConvertItemToKit(Guid id)
        {            
            _service.ConvertItemToKit(id);
            return Json("Converted item.");
        }


        [HttpPost]
        public ActionResult _SetUseKitPrice(Guid id, bool useKitPrice)
        {            
            _service.SetUseKitPrice(id, useKitPrice);
            return Json("Converted item.");
        }


        [HttpPost]
        public ActionResult _AddItemToProduct(Guid id, Guid quoteID)
        {
            var quoteService = new QuotationService<QuotationViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
            Quotation order = quoteService.FindByID(quoteID);
            QuotationCostItem item = _service.FindByID(id);
            

            Product model = new Product
            {
                ProductCode = item.ProductCode,
                Description = item.Description,
                Category = item.Category,
                //VendorCode = item.VendorCode,
                //Manufacturer = item.Manufacturer,
                Unit = item.Unit,
                JsonCustomFieldValues = item.JsonCustomFieldValues,
                IsKit = false,
                UseKitPrice = false,
                IsBought = false,
                VAT = item.VAT,
                Cost = item.Cost,
                CurrencyID = order.CurrencyID,
                Currency = order.Currency,
            };

            var toAdd = new ProductService<ProductViewModel>(_service.CompanyID, null, new ProductMapper(), _service.DbContext).MapToViewModel(model);
            return PartialView("_AddProductFromLineItem", toAdd);

        }


        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid ID, Guid itemID, int oldIndex, int newIndex)
        {
           
            _service.ParentID = ID;
            _service.UpdatePositions(itemID, oldIndex, newIndex);

            return Json("Success");
        }
        

    }

}