﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class DefaultQuotationNotificationsController : ServiceGridController<DefaultQuotationNotification, DefaultQuotationNotificationViewModel, DefaultQuotationNotificationService<DefaultQuotationNotificationViewModel>>
    {

        public DefaultQuotationNotificationsController()
            : base(new DefaultQuotationNotificationService<DefaultQuotationNotificationViewModel>(Guid.Empty, null, new DefaultQuotationNotificationMapper(), new SnapDbContext()))
        {
            
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid settingsID)
        {
            var list = _service.Read(settingsID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<DefaultQuotationNotificationViewModel>().ToDataSourceResult(request));
        }

    }

}