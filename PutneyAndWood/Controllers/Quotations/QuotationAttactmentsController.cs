﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationAttachmentsController : ServiceGridController<QuotationAttachment, QuotationAttachmentViewModel, QuotationAttachmentService<QuotationAttachmentViewModel>>
    {

        public QuotationAttachmentsController()
            : base(new QuotationAttachmentService<QuotationAttachmentViewModel>(Guid.Empty, Guid.Empty, null, new QuotationAttachmentMapper(), new SnapDbContext()))
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID)
        {
            var list = _service.Read(quoteID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<QuotationAttachmentViewModel>().ToDataSourceResult(request));
        }
        


        public ActionResult UploadAttachment(IEnumerable<HttpPostedFileBase> files, Guid quoteID)
        {
            if (files != null)
            {                
                foreach (var file in files)
                {
                    _service.UploadAttachment(quoteID, file);                    
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public ActionResult LinkAttachment(Guid quoteID, string link)
        {
            _service.LinkAttachment(quoteID, link);            
            return JavaScript("");
        }

    }

}