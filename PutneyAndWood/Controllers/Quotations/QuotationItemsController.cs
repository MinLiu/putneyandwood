﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationItemsController : ServiceGridController<QuotationItem, QuotationItemViewModel, QuotationItemService<QuotationItemViewModel>>
    {

        public QuotationItemsController()
            : base(new QuotationItemService<QuotationItemViewModel>(Guid.Empty, Guid.Empty, null, new QuotationItemMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID, Guid sectionID)
        {
            var quote = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext).FindByID(quoteID);

            List<QuotationItemViewModel> list = new List<QuotationItemViewModel>();
            var query = _service.Read(quoteID).Where(p => p.SectionID == sectionID)
                                              .Include(o => o.PriceBreaks)
                                              .OrderBy(o => o.SortPos)
                                              .ThenBy(o => o.ParentItemID != null);

            if (quote.UsePriceBreaks)
            {
                var mapper = new QuotationItemPriceBreakMapper();
                foreach (var i in query.ToList())
                {
                    list.Add(_service.MapToViewModel(i));
                    list.AddRange(i.PriceBreaks.OrderBy(p => p.Quantity).ToList().Select(p => mapper.MapPriceBreakToItemViewModel(p)).ToList());
                }
            }
            else
            {                
                list = query.ToList().Select(p => _service.MapToViewModel(p)).ToList();
            }
            
            foreach (var item in list)
            {
                if (string.IsNullOrEmpty(item.ImageURL) || item.ImageURL.Contains("NotFound"))
                    item.ImageURL = "/Product Images/AddPhoto.png";
            }

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationItemViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, QuotationItemViewModel viewModel)
        {
            //Items created directly on the items grid are comments
            viewModel.IsComment = true;
            
            var result = _service.Create(viewModel);
            //Update model
            if (viewModel.Information.Description == null) viewModel.Information.Description = "";
            if (viewModel.Information.Category == null) viewModel.Information.Category = "";
            //if (viewModel.Information.VendorCode == null) viewModel.Information.VendorCode = "";
            //if (viewModel.Information.Manufacturer == null) viewModel.Information.Manufacturer = "";

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public override JsonResult Update(DataSourceRequest request, QuotationItemViewModel viewModel)
        {         
   
            if(viewModel.IsPriceBreak)
            {
                //Do nothing
            }
            else
            {
                _service.Update(viewModel);
            }

            //Update model
            if (viewModel.Information.Description == null) viewModel.Information.Description = "";
            if (viewModel.Information.Category == null) viewModel.Information.Category = "";
            //if (viewModel.Information.VendorCode == null) viewModel.Information.VendorCode = "";
            //if (viewModel.Information.Manufacturer == null) viewModel.Information.Manufacturer = "";                        
            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        public override JsonResult Destroy(DataSourceRequest request, QuotationItemViewModel viewModel)
        {
            var repo = new QuotationQualificationRepository();
            var itemID = Guid.Parse(viewModel.ID);
            var relatedQualifications = repo.Read().Where(x => x.QuotationItemID == itemID);
            repo.Delete(relatedQualifications);

            base.Destroy(request, viewModel);
            
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState)); ;
        }

        [HttpPost]
        public ActionResult _AddBlankItem(Guid quoteID, Guid sectionID)
        {   
            var comment = new QuotationItem
            {
                QuotationID = quoteID,
                SectionID = sectionID,
                IsComment = true,
                Description = "",
            };
            
            _service.Create(comment);

            return Json("");
        }

        public ActionResult _AddItemToProduct(Guid id, Guid quoteID)
        {
            //This function converts a line item and adds it to the product list.

            var quote = new QuotationService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).FindByID(quoteID);
            QuotationItem item = _service.FindByID(id);
            
            Product model = new Product
            {
                ProductCode = item.ProductCode,
                Description = item.Description,
                Category = item.Category,
                //VendorCode = item.VendorCode,
                //Manufacturer = item.Manufacturer,
                Unit = item.Unit,
                JsonCustomFieldValues = item.JsonCustomFieldValues,
                //ImageURL = item.ImageURL,
                IsKit = false,
                UseKitPrice = false,
                IsBought = false,
                VAT = item.VAT,
                Cost = item.Cost,
                Price = item.Price,
                CurrencyID = quote.CurrencyID,
                Currency = quote.Currency,
            };

            var toAdd = new ProductService<ProductViewModel>(_service.CompanyID, null, new ProductMapper(), _service.DbContext).MapToViewModel(model);
            ViewBag.ImageFilePaths = item.Images.Select(i => i.File.FilePath).ToList();
            ViewBag.ImageFiles = item.Images.Select(i => i.File.ID).ToList();
            
            return PartialView("_AddProductFromLineItem", toAdd);

        }

        [HttpPost]
        public ActionResult _CopyItem(Guid id)
        {
            _service.CopyItem(id);
            
            return Json("Success");
        }

        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid ID, Guid itemID, Guid sectionID, int oldIndex, int newIndex)
        {
            _service.UpdatePositions(itemID, sectionID, oldIndex, newIndex);
            return Json("Success");
        }



        [HttpPost]
        public ActionResult _SwitchItem(Guid id, Guid productID, Guid quoteID)
        {
            _service.SwitchItemWithProduct(quoteID, id, productID);            
            return Json("Success");
        }

        [HttpPost]
        public ActionResult _HideItemOnPdf(Guid id)
        {
            _service.SetHideItemOnPdf(id, true);
            return Json("Success");
        }

        [HttpPost]
        public ActionResult _ShowItemOnPdf(Guid id)
        {
            _service.SetHideItemOnPdf(id, false);
            return Json("Success");
        }


        [HttpPost]
        public ActionResult _SetRequired(Guid id, bool required)
        {
            _service.SetRequired(id, required);
            return Json("Success");
        }




        //#region Item Image



        //public ActionResult UploadItemImage(HttpPostedFileBase uploadedImage, Guid ID)
        //{
        //    var model = _service.UploadImage(ID, uploadedImage);
        //    return Json(new { status = "success", message = model.ImageURL }, "text/plain");

        //}


        //public ActionResult RemoveItemImage(Guid id)
        //{
        //    _service.RemoveImage(id);
        //    return Json(new { status = "success", id = id }, "text/plain");
        //}

        //#endregion



    }

}