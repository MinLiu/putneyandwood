﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Controllers
{
       
    [Authorize]
    public class QuotationsController : ServiceEntityController<Quotation, QuotationViewModel, QuotationService<QuotationViewModel>> 
    {

        public QuotationsController()
            : base(new QuotationService<QuotationViewModel>(Guid.Empty, null, new QuotationMapper(), new SnapDbContext()))
        {

        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewQuotations == false)
                Response.Redirect("/");
        }

        #region Main Quotations Index

        #region Cookie And Status Stuff

        public void SetViewBags(){

            var statService = new QuotationStatusService<QuotationStatusViewModel>(new QuotationStatusMapper(), _service.DbContext);
            var stats = statService.Read().OrderBy(s => s.SortPos).ToArray().Select(s => statService.Mapper.MapToViewModel(s)).ToList();
            ViewBag.StatusOptions =  stats;

            HttpCookie cookie = Request.Cookies["QuotationSearch"];
            try 
            {
                if (cookie["Filter"] == null
                    || cookie["AssignedUser"] == null
                    || cookie["Statuses"] == null
                    || string.IsNullOrEmpty(cookie["DateType"])
                    || string.IsNullOrEmpty(cookie["FromD"])
                    || string.IsNullOrEmpty(cookie["ToD"])
                    || string.IsNullOrEmpty(cookie["Page"])
                    || string.IsNullOrEmpty(cookie["PageSize"])
                    || cookie["Sort"] == null
                    || cookie["SortDirection"] == null
                    )
                    throw new Exception();

                ViewBag.filterText = cookie["Filter"];
                ViewBag.assignedUser = cookie["AssignedUser"];
                ViewBag.statuses = cookie["Statuses"];
                ViewBag.dateType = cookie["DateType"];
                ViewBag.fromD = cookie["FromD"];
                ViewBag.toD = cookie["ToD"];
                ViewBag.page = cookie["Page"];
                ViewBag.pageSize = cookie["PageSize"];
                ViewBag.sort = cookie["Sort"];
                ViewBag.sortDirection = cookie["SortDirection"];
            }
            catch
            {
                ViewBag.filterText = "";
                ViewBag.assignedUser = "0";
                ViewBag.statuses = string.Join(",", stats.Select(s => s.ID));
                ViewBag.dateType = "0";
                ViewBag.fromD = DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.toD = DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.page = "1";
                ViewBag.pageSize = "50";
                ViewBag.sort = "Created";
                ViewBag.sortDirection = "desc";
            }
            

        }

        public void SetColumnViewBags()
        {
            var settings = new QuotationSettingsService<EmptyViewModel>(null, null, _service.DbContext).FindByID(CurrentUser.CompanyID);

            //Set editor settings based on the settings.
            //Note: There is a check to see if the values are null. This make overriding the function easier.
            ViewBag.ShowPhoto = settings.ShowPhoto;
            ViewBag.ShowProductCode = settings.ShowProductCode;
            ViewBag.ShowUnit = settings.ShowUnit;
            ViewBag.ShowMarkup = settings.ShowMarkup;
            ViewBag.ShowVAT = settings.ShowVAT;
            ViewBag.ShowDiscount = settings.ShowDiscount;
            ViewBag.ShowListPrice = settings.ShowListPrice;
            ViewBag.ShowListCost = settings.ShowListCost;
            ViewBag.ShowCost = settings.ShowCost;
            ViewBag.ShowMargin = settings.ShowMargin;
            ViewBag.ShowMarginPercentage = settings.ShowMarginPercentage;
            ViewBag.ShowSetupCost = settings.ShowSetupCost;
            ViewBag.ShowCategory = settings.ShowCategory;
            //ViewBag.ShowVendorCode = settings.ShowVendorCode;
            //ViewBag.ShowManufacturer = settings.ShowManufacturer;
            ViewBag.DefaultHourlyRate = settings.DefaultHourlyRate;
            ViewBag.ShowAccountCode = settings.ShowAccountCode;
            ViewBag.ShowPurchaseAccountCode = settings.ShowPurchaseAccountCode;

            var _customFieldService = new CustomProductFieldService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext);
            ViewBag.CustomFields = _customFieldService.Read().OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos, Show = i.ShowQuotations }).ToList();
        }

        #endregion


        // GET: Quotations
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult Index()
        {           
            SetViewBags();
            ViewBag.ListColumns = new CustomQuotationFieldService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).Read().Where(i => i.ShowMainList).OrderBy(i => i.SortPos).ToArray();
            return View();
        }

        public ActionResult Export()
        {
            return View();
        }

        public ActionResult Deleted()
        {
            SetViewBags();
            return View();
        }
        

        #endregion


      

        public ActionResult New(Guid projectID)
        {
            var model = _service.New(CurrentUser.Id, projectID);
            return RedirectToAction("Edit", new { ID = model.ID });
            
        }

        public ActionResult NewVersion(Guid ID)
        {
            var newOrder = _service.Version(ID, CurrentUser.Id);
            return RedirectToAction("Edit", new { ID = newOrder.ID });
        }

        public ActionResult Copy(Guid ID)
        {
            var newOrder = _service.Copy(ID, CurrentUser.Id);
            return RedirectToAction("Edit", new { ID = newOrder.ID });
        }

        

        //This is the preview link function. Redirects to the correct preview page for order. Must be overwritten.
        public ActionResult Preview(Guid id)
        {
            return RedirectToAction("Edit", "QuotationPreview", new { ID = id });
        }

        
        
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult Edit(Guid id)
        {
            SetColumnViewBags();

            var order = _service.MapToViewModel(_service.FindByID(id));
            return View(order);
        }


        public ActionResult Delete(Guid ID)
        {
            _service.Delete(ID);
            return RedirectToAction("Index");
        }


        public ActionResult _Header(Guid quoteID)
        {
            SetViewBags();
            SetColumnViewBags();

            var model = _service.FindByID(quoteID);
            var viewModel = _service.MapToViewModel(model);
            return PartialView(viewModel);
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Header(QuotationViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            SetViewBags();
            SetColumnViewBags();

            var model = _service.FindByID(Guid.Parse(viewModel.ID));

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                viewModel = _service.MapToViewModel(model);
                return PartialView(viewModel);
            }

            bool refreshPage = false; 
           
            //Refresh the whole page if the curreny changes.
            if (model.CurrencyID != viewModel.CurrencyID) refreshPage = true;
            _service.UpdateHeader(viewModel);

            if (refreshPage)
            {
                ViewBag.Reload = true;
                return JavaScript("location.reload();");
                //return PartialView(viewModel);
            }
            else
            {
                ViewBag.Saved = true;
                return JavaScript("ShowSavedMessage();");
                //return PartialView(viewModel);
            }
        }

        public ActionResult _HeaderTotal(Guid quoteID)
        {
            var model = _service.FindByID(quoteID);
            var viewModel = _service.MapToViewModel(model);
            return PartialView(viewModel);
        }


        public ActionResult ChangeStatus(Guid quoteID, int status)
        {
            _service.ChangeStatus(quoteID, status);
            
            //Refresh whole page after status change.
            return RedirectToAction("Edit", new { id = quoteID });
        }


        public ActionResult DeleteAgreement(Guid ID)
        {
            var agreementService = new QuotationAgreementService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
            var model = agreementService.FindByID(ID);
            agreementService.Destroy(model);
            
            return RedirectToAction("Edit", new { id = ID });
        }


        public ActionResult ResetOpenEmail(Guid ID)
        {
            _service.SetOpenEmailDateTime(ID, null, null);
            return RedirectToAction("Edit", new { id = ID });
        }

        public ActionResult SetPriceBreaks(Guid quoteID, bool use)
        {

            _service.SetPriceBreaks(quoteID, use);

            //Refresh whole page after status change.
            return RedirectToAction("Edit", new { id = quoteID });
        }

        

        public ActionResult _ApplyDiscount(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyDiscount(Guid quoteID, decimal discount)
        {
            ApplyQuotationValues(quoteID, discount, null, null, null);
            return RedirectToAction("Edit", new { id = quoteID });
        }

        public ActionResult _ApplyMargin(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyMargin(Guid quoteID, decimal margin)
        {
            ApplyQuotationValues(quoteID, null, margin, null, null);
            return RedirectToAction("Edit", new { id = quoteID });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyVAT(Guid quoteID, decimal vat)
        {
            ApplyQuotationValues(quoteID, null, null, null, vat);
            return RedirectToAction("Edit", new { id = quoteID });
        }

        public ActionResult _ApplyVAT(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyMarkup(Guid quoteID, decimal markup)
        {
            ApplyQuotationValues(quoteID, null, null, markup, null);
            return RedirectToAction("Edit", new { id = quoteID });
        }
        
        public ActionResult _ApplyMarkup(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }


        public ActionResult _ApplyAccountCode(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyAccountCode(Guid quoteID, string accountCode)
        {
            _service.ApplyAccountCode(quoteID, accountCode);
            return RedirectToAction("Edit", new { id = quoteID });
        }

        public ActionResult _ApplyPurchaseAccountCode(string quoteID)
        {
            ViewBag.QuotationID = quoteID;
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _ApplyPurchaseAccountCode(Guid quoteID, string accountCode)
        {
            _service.ApplyAccountCode(quoteID, accountCode);
            return RedirectToAction("Edit", new { id = quoteID });
        }


        [HttpGet]
        public ActionResult _Calculator(Guid quoteItemID)
        {
            var itemService = new QuotationItemService<QuotationItemViewModel>(_service.CompanyID, quoteItemID, _service.CurrentUserID, new QuotationItemMapper(), _service.DbContext);
            var viewModel = itemService.MapToViewModel(itemService.Find(quoteItemID));
            SetColumnViewBags();
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _ApplyCalculator(QuotationItemViewModel viewModel)
        {
            var itemService = new QuotationItemService<QuotationItemViewModel>(_service.CompanyID, Guid.Parse(viewModel.QuotationID), _service.CurrentUserID, new QuotationItemMapper(), _service.DbContext);
            itemService.Update(viewModel, true, false, true);
            return Json("Success");
        }


        public void ApplyQuotationValues(Guid id, decimal? discount, decimal? margin, decimal? markup, decimal? vat)
        {
            _service.ApplyDiscountMarginMarkupVat(id, discount, margin, markup, vat);
        }



        public ActionResult _AlternativeItemPopup(Guid itemID)
        {
            var item = new QuotationItemService<EmptyViewModel>(_service.CompanyID, Guid.Empty, null, null, _service.DbContext).FindByID(itemID);
            ViewBag.ItemID = item.ID;
            ViewBag.ProductCode = item.ProductCode;
            ViewBag.SectionID = item.SectionID;
            ViewBag.ByCategory = new ProductService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).ReadAlternatives(item.ProductCode).Count() <= 0;
            
            return PartialView();
        }

        public ActionResult _AssociativeItemPopup(Guid itemID)
        {
            var item = new QuotationItemService<EmptyViewModel>(_service.CompanyID, Guid.Empty, null, null, _service.DbContext).FindByID(itemID);
            ViewBag.ItemID = item.ID;
            ViewBag.ProductCode = item.ProductCode;
            ViewBag.SectionID = item.SectionID;
            return PartialView();
        }

        [HttpGet]
        public ActionResult _NewEvent(Guid quoteID, string eventAssignedUserID, string clientID, string contactID, string timestamp)
        {
            var quote = _service.FindByID(quoteID);
            var viewModel = new QuotationEventViewModel()
            {
                ID = Guid.NewGuid().ToString(),
                CompanyID = CurrentUser.CompanyID.ToString(),
                EventAssignedUserID = eventAssignedUserID ?? quote.AssignedUserID,
                EventCreatorID = CurrentUser.Id,
                ClientID = clientID ?? quote.ClientID.ToString(),
                ContactID = contactID ?? quote.ClientContactID.ToString(),
                QuotationID = quote.ID.ToString(),
                Timestamp = !string.IsNullOrEmpty(timestamp) ? DateTime.Parse(timestamp) : DateTime.Now
            };

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _NewEvent(QuotationEventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                var service = new QuotationEventService<QuotationEventViewModel>(CurrentUser.Id, new QuotationEventMapper(), _service.DbContext);

                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                //var model = mapper.MapToModel(viewModel);
                //model.CompanyID = CurrentUser.CompanyID;
                //model.CreatorID = CurrentUser.Id;
                service.Create(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        [HttpGet]
        public ActionResult _EditEvent(Guid eventID)
        {
            var eventService = new QuotationEventService<QuotationEventViewModel>(CurrentUser.Id, new QuotationEventMapper(), _service.DbContext);
            var viewModel = eventService.MapToViewModel(eventService.Find(eventID));

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditEvent(QuotationEventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                var service = new QuotationEventService<QuotationEventViewModel>(CurrentUser.Id, new QuotationEventMapper(), _service.DbContext);
                //var mapper = new OpportunityEventMapper();

                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                service.Update(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }


        public FileResult ExportItems(Guid quotationID, string type)
        {
            var itemService = new QuotationItemService<EmptyViewModel>(_service.CompanyID, quotationID, null, null, _service.DbContext);
            var export = itemService.Export(quotationID, (type == "AllItems"));

            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user              
        }

        public ActionResult _PickAssociateInvoice(string quoteID)
        {
            ViewBag.QuoteID = quoteID;
            return PartialView();
        }

        public ActionResult _PickAssociatePurchaseOrder(string quoteID)
        {
            ViewBag.QuoteID = quoteID;
            return PartialView();
        }


        public ActionResult _PickAssociateJob(string quoteID)
        {
            ViewBag.QuoteID = quoteID;
            return PartialView();
        }


        #region Order Items Tab



        public ActionResult _ItemsTab(QuotationViewModel model)
        {
            return _ItemsTabURL(Guid.Parse(model.ID));
        }

        public ActionResult _ItemsTabURL(Guid quoteID)
        {
            SetColumnViewBags();
            return PartialView("_ItemsTab", _service.MapToViewModel(_service.FindByID(quoteID)));
        }


        public ActionResult _SectionTotal(Guid sectionID)
        {
            var secService = new QuotationSectionService<QuotationSectionViewModel>(_service.CompanyID, null, null, new QuotationSectionMapper(), _service.DbContext);
            var model = secService.FindByID(sectionID);
            var viewModel = secService.Mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }


        public ActionResult _ItemImage(Guid id)
        {
            var item = new QuotationItemService<EmptyViewModel>(_service.CompanyID, Guid.Empty, null, null, _service.DbContext).FindByID(id);

            ViewBag.Controller = "QuotationItemImages";
            ViewBag.ID = id;
            ViewBag.CanUpload = item.Quotation.StatusID == QuotationStatusValues.DRAFT;
            return PartialView();

        }

        #endregion

        public ActionResult _AddBasis(QuotationViewModel viewModel)
        {
            return PartialView(viewModel);
        }

        public ActionResult _AddProducts(QuotationViewModel viewModel)
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult _AddProductItem(Guid id, Guid quoteID, Guid sectionID, decimal qty)
        {
            var itemService = new QuotationItemService<EmptyViewModel>(_service.CompanyID, quoteID, null, null, _service.DbContext);
            var addedItem = itemService.AddProduct(id, quoteID, sectionID, qty);
            _AddQualificationByProduct(id, quoteID, addedItem.ID);
            return Json("Success");
        }


        public ActionResult _AddCostProducts(QuotationViewModel viewModel)
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult _AddProductCostItem(Guid id, Guid quoteID, decimal qty)
        {
            var itemService = new QuotationCostItemService<EmptyViewModel>(_service.CompanyID, quoteID, CurrentUser.Id, null, _service.DbContext);
            itemService.AddProduct(id, quoteID, qty);
            
            return Json("Success");
        }


        public ActionResult _AddLabour(QuotationViewModel viewModel)
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult _AddLabourItem(Guid id, Guid quoteID, decimal qty)
        {
            var labourService = new QuotationLabourItemService<QuotationLabourItemViewModel>(CurrentUser.CompanyID, quoteID, CurrentUser.Id, null, _service.DbContext);
            labourService.AddLabour(id, qty);            
            return Json("Success");
        }
        

        public ActionResult _AddCustomProduct(string quoteID, string sectionID)
        {
            SetColumnViewBags();

            var m = new CustomItemViewModel
            {
                OrderID = quoteID,
                SectionID = sectionID,
                VAT = CurrentUser.Company.DefaultVatRate,
                CustomFieldValues = new CustomProductFieldService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).Read().Where(i => i.ShowQuotations == true).OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList(),
                Quantity = 1,
            };

            return PartialView(m);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddCustomProduct(CustomItemViewModel viewModel)
        {

            SetColumnViewBags();

            if (viewModel != null && ModelState.IsValid)
            {

                var toAdd = new QuotationItem
                {
                    QuotationID = Guid.Parse(viewModel.OrderID),
                    SectionID = Guid.Parse(viewModel.SectionID),
                    //ProductID = null,
                    ProductCode = viewModel.ProductCode,
                    Description = viewModel.Description,
                    Category = viewModel.Category,
                    //VendorCode = viewModel.VendorCode,
                    //Manufacturer = viewModel.Manufacturer,
                    CustomFieldValues = viewModel.CustomFieldValues,

                    Unit = viewModel.Unit,
                    VAT = viewModel.VAT,
                    Quantity = viewModel.Quantity,

                    //ImageURL = pdt.ImageURL,
                    ParentItemID = null,
                    IsKit = false,
                    UseKitPrice = false,
                    IsComment = false,

                    SetupCost = viewModel.SetupCost,
                    Cost = viewModel.Cost,
                    Price = viewModel.Price,
                    ListPrice = viewModel.Price
                };

                var itemService = new QuotationItemService<EmptyViewModel>(_service.CompanyID, toAdd.QuotationID, CurrentUser.Id, null, _service.DbContext);
                itemService.Create(toAdd);
                itemService.UpdateValues(toAdd, true, true);

                viewModel = new CustomItemViewModel { OrderID = viewModel.OrderID, SectionID = viewModel.SectionID, CustomFieldValues = viewModel.CustomFieldValues };

                ViewBag.Added = true;
            }

            return PartialView(viewModel);
        }


        public ActionResult _AddCustomCostProduct(string quoteID)
        {
            SetColumnViewBags();

            var m = new CustomItemViewModel
            {
                OrderID = quoteID,
                VAT = CurrentUser.Company.DefaultVatRate,
                CustomFieldValues = new CustomProductFieldService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).Read().Where(i => i.ShowQuotations == true).OrderBy(i => i.SortPos).ToArray().Select(i => new CustomField { Name = i.Name, Label = i.Label, SortPos = i.SortPos }).ToList(),
            };

            return PartialView(m);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddCustomCostProduct(CustomItemViewModel viewModel)
        {

            SetColumnViewBags();

            if (viewModel != null && ModelState.IsValid)
            {

                var toAdd = new QuotationCostItem
                {
                    QuotationID = Guid.Parse(viewModel.OrderID),
                    //ProductID = null,
                    ProductCode = viewModel.ProductCode,
                    Description = viewModel.Description,
                    Category = viewModel.Category,
                    //VendorCode = viewModel.VendorCode,
                   //Manufacturer = viewModel.Manufacturer,
                    CustomFieldValues = viewModel.CustomFieldValues,
                    Unit = viewModel.Unit,
                    VAT = viewModel.VAT,
                    Quantity = viewModel.Quantity,

                    //ImageURL = pdt.ImageURL,
                    ParentCostItemID = null,
                    IsKit = false,
                    UseKitPrice = false,
                    IsComment = false,

                    Cost = viewModel.Cost,
                };

                var itemService = new QuotationCostItemService<EmptyViewModel>(_service.CompanyID, toAdd.QuotationID, CurrentUser.Id, null, _service.DbContext);
                itemService.Create(toAdd);
                itemService.UpdateValues(toAdd, true, true);
                ViewBag.Added = true;

                return PartialView(new CustomItemViewModel { OrderID = viewModel.OrderID, CustomFieldValues = viewModel.CustomFieldValues });
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        [HttpGet]
        public ActionResult _ImportQuotationItems(string quoteID)
        {
            return PartialView(new QuotationViewModel { ID = quoteID });
        }

        [HttpPost]
        public ActionResult _ImportQuotationItems(HttpPostedFileBase upload, string id)
        {
            var importService = new QuotationItemImportService(CurrentUser.CompanyID, Guid.Parse(id), CurrentUser.Id, _service.DbContext);
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Items have been successfully imported.", result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            var viewModel = _service.MapToViewModel(_service.FindByID(Guid.Parse(id)));

            //SetColumnViewBags();
            //return View("Edit", viewModel);
            return RedirectToAction("Edit", new { id = id });
        }


        // GET: /Attachments Tab
        public ActionResult _AttachmentsTab(QuotationViewModel model)
        {
            return PartialView("_AttachmentsTab", model);
        }

        // GET: /Comments Tab
        public ActionResult _CommentsTab(QuotationViewModel model)
        {
            return PartialView("_CommentsTab", model);
        }


        // GET: /Notifications Tab
        public ActionResult _NotificationsTab(QuotationViewModel model)
        {
            return PartialView("_NotificationsTab", model);
        }

        public void _AddQualificationByProduct(Guid productID, Guid quoteID, Guid quotationItemID)
        {
            var lastPos = new QuotationRepository().Read().Where(x => x.ID == quoteID).Select(x => x.Qualifications).FirstOrDefault().Count;
            var qualificationsToAdd = new ProductRepository().Read().Where(x => x.ID == productID).Select(x => x.ProductQualifications).FirstOrDefault();

            var repo = new QuotationQualificationRepository();
            foreach (var qualification in qualificationsToAdd.OrderBy(x => x.SortPos))
            {
                repo.Create(new QuotationQualification
                {
                    QuotationItemID = quotationItemID,
                    QuotationID = quoteID,
                    SortPos = ++lastPos,
                    Description = qualification.Description,
                    IsSubHeading = qualification.IsSubHeading,
                    IsSubItem = qualification.IsSubItem,
                });
            }
        }
    }
}