﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;


namespace SnapSuite.Controllers
{
    [AllowAnonymous]
    public class QuotationReviewController : ServiceEntityController<Quotation, QuotationClientViewModel, QuotationService<QuotationClientViewModel>>
    {

        public QuotationReviewController()
            : base(new QuotationService<QuotationClientViewModel>(Guid.Empty, null, new QuotationClientMapper(), new SnapDbContext()))
        {

        }        
  
        public ActionResult Index(Guid? ID = null, string Jasq = "", string errorMessage = null)
        {   
            var model = _service.Read(Guid.Empty)
                                .Where(i => i.ID == ID && i.CheckSum == Jasq && i.IsClientVersion == false)
                                .Include(i => i.Company)
                                .Include(i => i.Attachments)
                                .DefaultIfEmpty(null)
                                .FirstOrDefault();

            if(model == null) return RedirectToAction("Index", "Home");

            var viewModel = _service.MapToViewModel(model);
            SetCustomTheme(model.Company);
            SetNoNavBar();

            var draft =  new GenericService<Quotation>(_service.DbContext).Read().Where(i => i.MainVersionID == ID && i.IsClientVersion == true && i.IsClientDraft == true).DefaultIfEmpty(null).FirstOrDefault();
            if (draft != null)
            {
                ViewBag.SavedDraftID = draft.ID;
                ViewBag.SavedDraftCheckSum = draft.CheckSum;
            }
            
            ViewBag.ErrorMessage = errorMessage;

            return View(viewModel);
        }


        public ActionResult CreateCustomize(Guid? ID = null, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) return RedirectToAction("Index", new { ID = ID, Jasq = Jasq });

            _service.CompanyID = model.CompanyID;
            var newOrder = _service.Version(ID.Value, null, true);

            return RedirectToAction("Customize", new { ID = newOrder.ID, Jasq = newOrder.CheckSum });
        }


        public ActionResult ResetCustomize(Guid? ID = null, string Jasq = "", Guid? MainID = null)
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq).DefaultIfEmpty(null).FirstOrDefault();
            var main = _service.Read(Guid.Empty).Where(i => i.ID == MainID).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null || main == null) return RedirectToAction("Index", "Home");

            //Destroy current client version
            _service.CompanyID = model.CompanyID;
            _service.Destroy(model);

            //Then current a new one.
            var newOrder = _service.Version(main.ID, null, true);

            return RedirectToAction("Customize", new { ID = newOrder.ID, Jasq = newOrder.CheckSum });
        }

        public ActionResult SubmitCustomizeForReview(Guid? ID = null, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) return RedirectToAction("Index", "Home");

            var newOrder = _service.SubmitClientVersionForReview(model);
            return RedirectToAction("SubmitComplete", new { ID = newOrder.ID });
        }
        


        public ActionResult Customize(Guid? ID = null, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty)
                                .Where(i => i.ID == ID && i.CheckSum == Jasq)
                                .Include(i => i.Company)
                                .Include(i => i.Attachments)
                                .DefaultIfEmpty(null)
                                .FirstOrDefault();

            if (model == null) return RedirectToAction("Index", "Home");
            if (!model.IsClientVersion || !model.IsClientDraft) return RedirectToAction("Index", new { ID = ID, Jasq = Jasq });

            var viewModel = _service.MapToViewModel(model);
            SetCustomTheme(model.Company);
            SetNoNavBar();

            return View(viewModel);
        }

        public ActionResult DeleteCustomize(Guid? ID = null, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq && i.IsClientVersion && i.IsClientDraft).DefaultIfEmpty(null).FirstOrDefault();
            var mainModel = _service.Read(Guid.Empty).Where(i => i.ID == model.MainVersionID).Include(i => i.Company).DefaultIfEmpty(null).FirstOrDefault();
            if (model == null) return RedirectToAction("Index", "Home");

            _service.Destroy(model);
            
            if (mainModel != null) return RedirectToAction("Index", new { ID = mainModel.ID, Jasq = mainModel.CheckSum });
            else return RedirectToAction("Index", "Home");
        }


        public ActionResult SubmitComplete(string ID)
        {
            var model = _service.Read(Guid.Empty)
                                .Where(i => i.ID.ToString() == ID)
                                .Include(i => i.Company)
                                .DefaultIfEmpty(null)
                                .FirstOrDefault();

            try
            {
                SetCustomTheme(model.Company);
                SetNoNavBar();
            }
            catch { }            
            return View();
        }


        public ActionResult _HeaderURL(Guid ID, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq).DefaultIfEmpty(null).FirstOrDefault();
            var viewModel = _service.MapToViewModel(model);
            return PartialView("_Header", viewModel);
        }

        public ActionResult _Header(QuotationClientViewModel viewModel)
        {
            return PartialView(viewModel);
        }
        
        public ActionResult _ItemsURL(Guid ID, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty).Where(i => i.ID == ID && i.CheckSum == Jasq).DefaultIfEmpty(null).FirstOrDefault();
            var viewModel = _service.MapToViewModel(model);
            return PartialView("_Items", viewModel);
        }


        public ActionResult _Items(QuotationClientViewModel viewModel)
        {
            return PartialView(viewModel);
        }


        public ActionResult _SectionTotal(Guid sectionID)
        {
            var secService = new QuotationSectionService<QuotationSectionClientViewModel>(Guid.Empty, null, null, new QuotationSectionClientMapper(), _service.DbContext);
            var model = secService.FindByID(sectionID);
            var viewModel = secService.Mapper.MapToViewModel(model);
            return PartialView(viewModel);
        }

        public ActionResult _ItemImage(Guid id)
        {
            ViewBag.ID = id;
            return PartialView();
        }


        [AllowAnonymous]
        public ActionResult _AcceptReject(Guid ID, string Jasq = "")
        {
            var model = _service.Read(Guid.Empty)
                                .Where(i => i.ID == ID && i.CheckSum == Jasq)
                                .DefaultIfEmpty(null)
                                .FirstOrDefault();

            if (model == null) return RedirectToAction("Index", "Home");
                        
            var viewModel = new QuotationAcceptDeclineViewModel
            {
                QuotationID = model.ID.ToString(),
                CheckSum = model.CheckSum,
                StatusID = model.Status.ID,
                StatusName = model.Status.Name,
                StatusDescription = model.Status.Description,
                StatusHexColour = model.Status.HexColour,
            };
            return PartialView(viewModel);
        }


        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AcceptReject(QuotationAcceptDeclineViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            if (!viewModel.Accept)
            {
                ViewBag.ErrorMessage = "Please select 'Accept'";
                return PartialView(viewModel);
            }

            var quoteService = new QuotationService<QuotationViewModel>(Guid.Empty, null, new QuotationMapper(), _service.DbContext);
            var quote = quoteService.FindByID(Guid.Parse(viewModel.QuotationID));

            quoteService.AcceptOrder(quote.ID, QuotationStatusValues.ACCEPTED, viewModel.Name, viewModel.Message, viewModel.PoNumber);
            return PartialView("_AcceptRejectComplete");
        }



        public ActionResult CompleteStripePayment(Guid ID, string Jasq = "", string stripeToken = "")
        {
            var model = _service.Read(Guid.Empty)
                                .Where(i => i.ID == ID && i.CheckSum == Jasq)
                                .DefaultIfEmpty(null)
                                .FirstOrDefault();

            if (model == null) return RedirectToAction("Index", "Home", new { errrorMessage = "Unable to complete payment. Quotation record not found." });
                        
            try
            {                
                var paymentService = new QuotationStripePaymentService<EmptyViewModel>(Guid.Empty, null, null, _service.DbContext);
                paymentService.CompleteChargePayment(model.ID, stripeToken);
                return RedirectToAction("Index", new { ID = ID, Jasq = Jasq });
            }
            catch(Exception ex) { return RedirectToAction("Index", new { ID = ID, Jasq = Jasq, errorMessage = ex.Message }); }
        }


    }
}
