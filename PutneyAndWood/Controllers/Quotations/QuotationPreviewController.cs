﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;


namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationPreviewController : ServiceGridController<QuotationPreviewItem, QuotationPreviewItemViewModel, QuotationPreviewItemService<QuotationPreviewItemViewModel>>
    {

        public QuotationPreviewController()
            : base(new QuotationPreviewItemService<QuotationPreviewItemViewModel>(Guid.Empty, null, null, new QuotationPreviewItemMapper(), new SnapDbContext()))
        {

        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewQuotations == false)
                Response.Redirect("/");
        }
  
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Quotations");
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult Edit(Guid ID)
        {
            this.CheckSubscription();
            var quoteService = new QuotationService<QuotationViewModel>(_service.CompanyID, _service.CurrentUserID, new QuotationMapper(), _service.DbContext);
            var model = quoteService.FindByID(ID);
            var viewModel = quoteService.MapToViewModel(model);
            
            return View(viewModel);
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            throw new NotImplementedException();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid ID)
        {
            var viewModels = _service.Read(ID).OrderBy(i => i.SortPos).ToList().Select(s => _service.MapToViewModel(s)).ToList();
            return Json(viewModels.ToDataSourceResult(request));
        }
        

        [AllowAnonymous]
        public ActionResult _GenerateQuotationPDF(Guid id, string checksum, bool download = false)
        {
            //try
            //{
                //Get PDF generation ready.
                string filename = "";
                string filePath = GeneratePDF(Server, id, checksum, ref filename);

                if (download) { return File(Server.MapPath(filePath), "application/pdf", filename); }
                else
                {
                    Response.AddHeader("Content-Disposition", string.Format("inline; filename={0};", filename.Replace(" ", "_")));
                    return File(Server.MapPath(filePath), "application/pdf");
                }
            //}
            //catch
            //{
            //    var pdfBytes = PDFService<QuotationPDFViewModel>.GenerateErrorPage();
            //    return File(pdfBytes, "application/pdf");
            //}
        }


        public ActionResult _EmailPDF(Guid ID)
        {            
            var settings = new QuotationSettingsService<EmptyViewModel>(null, null, _service.DbContext).FindByID(_service.CompanyID);
            var quoteService = new QuotationService<EmptyViewModel>(CurrentUser.CompanyID, CurrentUser.Id, null, _service.DbContext);
            var quote = quoteService.FindByID(ID);
            var emailTemplate = quoteService.GetEmailTitleMessage(quote.ID);

            var attachService = new QuotationAttachmentService<QuotationAttachmentViewModel>(_service.CompanyID, quote.ID, null, new QuotationAttachmentMapper(), _service.DbContext);
            var attachments = attachService.Read().Where(a => a.File.FileFormat != FileFormat.Link && a.File.FileSize <= (10 * 1024 * 1024)).Include(i => i.File).OrderBy(i => i.Filename).ToArray().Select(i => attachService.MapToViewModel(i)).ToList();
                
            var emailViewModel = new QuotationEmailViewModel
            {
                QuotationID = quote.ID.ToString(),
                ClientID = quote.ClientID.ToString(),
                CheckSum = quote.CheckSum,
                ToEmails = (quote.ClientContact != null && !string.IsNullOrWhiteSpace(quote.ClientContact.Email) ) ? quote.ClientContact.Email + ";" : "",
                FromEmail = (!string.IsNullOrWhiteSpace(settings.DefaultFromEmail)) ? settings.DefaultFromEmail : CurrentUser.Email,
                Bcc = CurrentUser.Email,
                Title = emailTemplate.Title,
                Message = emailTemplate.Body,
                IncludePDF = true,
                SendCopyPDF = true,
                Attachments = attachments,
                SelectedAttachments = attachments.Where(a => a.ShowInPreview == true).Select(a => a.ID.ToString()).ToArray()
            };

            return PartialView(emailViewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EmailPDF(QuotationEmailViewModel model)
        {
            try
            {
                List<string> sentEmails = new List<string>();
                List<string> failEmails = new List<string>();

                var quoteService = new QuotationService<QuotationViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new QuotationMapper(), _service.DbContext);
                quoteService.EmailPDF(Guid.Parse(model.QuotationID),
                                        model.Title,
                                        model.Message,
                                        model.FromEmail,
                                        model.ToEmails.Replace(" ", "").Split(';').Where(s => !string.IsNullOrWhiteSpace(s)).ToArray(),
                                        null,
                                        model.IncludePDF,
                                        (model.SendCopyPDF) ? CurrentUser.Id : "",
                                        (model.SelectedAttachments != null) ? model.SelectedAttachments.Select(i => Guid.Parse(i)).ToList() : null,
                                        ref sentEmails, ref failEmails);

                string output = "";

                if (sentEmails.Count() > 0)
                {
                    output += "Quotation has successfully been emailed to: \n";
                    output += string.Join("\n", sentEmails);
                    output += Environment.NewLine;
                }

                if (failEmails.Count() > 0)
                {
                    output += "Failed to send Quotation to: \n";
                    output += string.Join("\n", failEmails);
                }

                return JavaScript("EmailSent('" + HttpUtility.JavaScriptStringEncode(output) + "'); HideViews();");
            }
            catch(Exception ex) { return JavaScript("EmailSent('" + HttpUtility.JavaScriptStringEncode(ex.Message) + "'); HideViews();"); }
        }


        [AllowAnonymous]
        public ActionResult Review(Guid ID, string Jasq = "")
        {
            return RedirectToAction("Index", "QuotationReview", new { ID = ID, Jasq = Jasq });
        }


        public ActionResult _PDF(Guid id, string checksum)
        {
            ViewBag.ID = id;
            ViewBag.CheckSum = checksum;
            return PartialView();
        }


        public ActionResult _PdfSize(Guid id)
        {
            try
            {
                var filepath = FileService.GetUploadPhysicalFilePath(_service.CompanyID, "/_Temp/", id.ToString() + ".pdf");
                long length = new System.IO.FileInfo(filepath).Length;
                return Content(FileEntryExtension.FileSizeToLabel((int)new System.IO.FileInfo(filepath).Length));
            }
            catch { return Content("ERROR"); }            
        }


        public string GeneratePDF(HttpServerUtilityBase server, Guid id, string checksum, ref string filename)
        {
            var quoteService = new QuotationService<QuotationViewModel>(Guid.Empty, null, new QuotationMapper(), _service.DbContext);
            var quote = quoteService.Read(Guid.Empty).Where(i => i.ID == id && i.CheckSum == checksum).First();
            var pdfService = new QuotationPdfGenService(quote.CompanyID, null, _service.DbContext);
            filename = pdfService.GetPdfFilename(quote);
            return pdfService.GetDownloadUrl(quote);
        }


        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid itemID, int oldIndex, int newIndex)
        {
            _service.UpdatePositions(itemID, oldIndex, newIndex);
            return Json("Success");
        }


        public JsonResult DropDownQuotationPreviewItem(Guid ID, PreviewItemType Type)
        {
            if(Type == PreviewItemType.Template)
            {   
                var list = new QuotationTemplateRepository().Read()
                    //.Where(p => (p.CompanyID == CurrentUser.CompanyID || p.CompanyID == null))
                    .Where(p => (p.CompanyID == CurrentUser.CompanyID))
                    .OrderBy(o => o.Type)
                    .ThenBy(o => o.Filename)
                    .ToList()
                    .Select(p => new SelectPreviewItemViewModel { OrderID = ID, TemplateAttachmentID = p.ID, Name = string.Format("({0}) {1}", p.Type.ToString(), p.Filename), Type = PreviewItemType.Template });

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var list = new QuotationAttachmentService<EmptyViewModel>(_service.CompanyID, ID, null, null, _service.DbContext).Read()
                    .Where(p => p.File.FileFormat == FileFormat.Docx || p.File.FileFormat == FileFormat.Pdf)
                    .OrderBy(o => o.Filename)
                    .ToList()
                    .Select(p => new SelectPreviewItemViewModel { OrderID = ID, TemplateAttachmentID = p.ID, Name = p.Filename, Type = PreviewItemType.Attachment });

                return Json(list, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
