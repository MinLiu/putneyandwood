﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class QuotationNotificationsController : ServiceGridController<QuotationNotification, QuotationNotificationViewModel, QuotationNotificationService<QuotationNotificationViewModel>>
    {

        public QuotationNotificationsController()
            : base(new QuotationNotificationService<QuotationNotificationViewModel>(Guid.Empty, Guid.Empty, null, new QuotationNotificationMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid quoteID)
        {
            var list = _service.Read(quoteID)
                               .OrderByDescending(o => o.Email)
                               .ToList().Select(p => _service.MapToViewModel(p));

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationNotificationViewModel>().ToDataSourceResult(request));
        }
        
    }

}