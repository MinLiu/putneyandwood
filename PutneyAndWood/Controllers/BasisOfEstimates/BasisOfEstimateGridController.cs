﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Kendo.Mvc;
using System.ComponentModel;

namespace SnapSuite.Controllers
{


    [Authorize]
    public class BasisOfEstimateGridController : BaseController
    {
        public BasisOfEstimateService _service { get; set; }
        public BasisOfEstimateGridMapper _mapper { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _service = new BasisOfEstimateService(new SnapDbContext());
            _mapper = new BasisOfEstimateGridMapper();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(BasisOfEstimateGridViewModel viewModel)
        {
            var model = _mapper.MapToModel(viewModel);
            _service.Create(model);
            viewModel.ID = model.ID.ToString();
            return Json(viewModel);
        }

        // READ: GridProject
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var scopes = _service.Read();
            var list = scopes.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _mapper.MapToViewModel(q)).ToList();
            DataSourceResult result = new DataSourceResult { Data = list, Total = scopes.Count() };
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(BasisOfEstimateGridViewModel viewModel)
        {
            var model = _service.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _service.Update(model);
            return Json(viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest]DataSourceRequest request, BasisOfEstimateGridViewModel viewModel)
        {
            var model = _service.Delete(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public bool _UpdatePositions(Guid ID, int newIndex)
        {
            BasisOfEstimate move = null;

            move = _service.FindByID(ID);

            var items = _service.Read().OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            items.Insert((newIndex >= 0) ? newIndex : 0, move);

            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
                _service.Update(i, new string[] { "SortPos" });
            }
            return true;
        }
    }
}