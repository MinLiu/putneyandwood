﻿using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SnapSuite.Controllers
{

    public class EmailCheckController : Controller
    {

        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Quotation(string id, string email)
        {
            if (!string.IsNullOrWhiteSpace(id) && !string.IsNullOrWhiteSpace(email))
            {
                try
                {
                    var service = new QuotationService<EmptyViewModel>(Guid.Empty, null, null, new SnapDbContext());
                    service.SetOpenEmailDateTime(Guid.Parse(id), DateTime.Now, email);
                }
                catch { }
            }
            return File(Server.MapPath("/Content/blank.png"), "image/png");
        }

    }
}