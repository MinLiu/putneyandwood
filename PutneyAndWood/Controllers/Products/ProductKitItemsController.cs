﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductKitItemsController :  ServiceGridController<ProductKitItem, ProductKitItemViewModel, ProductKitItemService<ProductKitItemViewModel>>
    {

        public ProductKitItemsController()
            : base(new ProductKitItemService<ProductKitItemViewModel>(Guid.Empty, null, null, new ProductKitItemMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string pdtID)
        {
            var list = _service.Read().Where(p => p.KitID.ToString() == pdtID && (p.Product.Deleted == false || p.IsComment))
                                   .Include(p => p.Product)
                                   .OrderBy(p => p.SortPos).ThenBy(p => p.Product.ProductCode).ThenBy(p => p.Product.Description)
                                   .ToList().Select(p => _service.MapToViewModel(p)).ToList();
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ProductKitItemViewModel>().ToDataSourceResult(request));
        }
        

        public JsonResult AddCommentKitItem(Guid kitID)
        {
            var kitItem = new ProductKitItem { KitID =kitID, ProductID = null, Description = "New Comment Line", Quantity = 0, IsComment = true };
            _service.Create(kitItem);
            return Json("Success");
        }
        

        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid ID, Guid itemID, int oldIndex, int newIndex)
        {
            _service.ParentID = ID;
            _service.UpdatePositions(itemID, oldIndex, newIndex);
            return Json("Success");
        }


    }

}