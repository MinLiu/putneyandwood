﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class LabourController : ServiceGridController<Labour, LabourViewModel, LabourService<LabourViewModel>>
    {

        public LabourController()
            : base(new LabourService<LabourViewModel>(Guid.Empty, null, new LabourMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {
            var list = _service.Read().Where(p => (p.Description.Contains(filterText) || string.IsNullOrEmpty(filterText) ) )
                                .OrderBy(p => p.Description)
                                .ToList()
                                .Select(p => _service.MapToViewModel(p));

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _service.Read().OrderBy(p => p.Description).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }
        

        public JsonResult DropDownLabour()
        {
            var labour = _service.Read()
                .OrderBy(t => t.Description).ToArray()
                .Select(a => new SelectItemViewModel
                {
                    ID = a.ID.ToString(),
                    Name = a.ToShortLabel()
                }
            ).ToList();
            
            return Json(labour, JsonRequestBehavior.AllowGet);
        }

    }

}