﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using System.Web.UI;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductsController : ServiceEntityController<Product, ProductViewModel, ProductService<ProductViewModel>>
    {
                
        public ProductsController()
            : base(new ProductService<ProductViewModel>(Guid.Empty, null, new ProductMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewProducts == false)
                Response.Redirect("/");
        }

        
        // GET: Products
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.ProductID = ID;
            
            ViewBag.ProductColumns = new CustomProductFieldService<EmptyViewModel>(_service.CompanyID, null, null, _service.DbContext).Read().Where(i => i.ShowMainList).OrderBy(i => i.SortPos).ToArray();

            return View();
        }

        public ActionResult NewProduct()
        {
            ViewBag.NewProduct = true;
            return View("Index");
        }


        public ActionResult Deleted()
        {
            return View();
        }

       

        // GET: /New Product
        public ActionResult _NewProduct()
        {
            var model = new Product { CompanyID = CurrentUser.CompanyID, VAT = CurrentUser.Company.DefaultVatRate, Currency = new Currency { ID = 1 }, CurrencyID = 1 };
            var viewModel = _service.MapToViewModel(model);
            return PartialView(viewModel);
        }

        
        // POST: /New Product
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewProduct(ProductViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            _service.Create(viewModel);

            return Json(new { ID = viewModel.ID });
            
        }

        //public IEnumerable<string> SelectedAttachments { get; set; }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddProductFromLineItem(ProductViewModel viewModel, int DD_Currency2, IEnumerable<Guid> imageFileIds)
        {
            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            viewModel.CompanyID = CurrentUser.CompanyID.ToString();
            viewModel.CurrencyID = DD_Currency2;

            var model = _service.MapToModel(viewModel);
            _service.Create(model);


            if (imageFileIds != null) {
                var imgService = new ProductImageService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext);
                var files = new GenericService<FileEntry>(_service.DbContext).Read().Where(i => imageFileIds.Contains(i.ID)).ToArray();
                var imgModels = files.Select(i => new ProductImage { OwnerID = model.ID, FileID = i.ID }).ToList();
                imgService.Create(imgModels);
            }

            //return PartialView("_EditProduct", model);
            return Json(new { ID = viewModel.ID });
        }




        // GET: /Edit Product
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None", Location = OutputCacheLocation.None)] 
        public ActionResult _EditProduct(Guid ID)
        {
            var model = _service.FindByID(ID);

            if (model == null) return _NewProduct();

            var viewModel = _service.MapToViewModel(model);

            return PartialView(viewModel);            
        }

        
        // POST: /Edit Product
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        public ActionResult _EditProductDetails(ProductViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }
            
            var model = _service.Find(Guid.Parse(viewModel.ID));

            ViewBag.Refresh = (model.IsKit != viewModel.IsKit || model.UseKitPrice != viewModel.UseKitPrice || model.UsePriceBreaks != viewModel.UsePriceBreaks);
            _service.Mapper.MapToModel(viewModel, model);
            _service.Update(model);
            
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            
            return PartialView(viewModel);
          
        }
        


        public ActionResult _PricingTab(ProductViewModel model)
        {
            return View(model);
        }

        public ActionResult _KitItemsTab(ProductViewModel model)
        {
            return View(model);
        }


        public ActionResult _AlternativesTab(ProductViewModel model)
        {
            return View(model);
        }

        public ActionResult _AssociativesTab(ProductViewModel model)
        {
            return View(model);
        }


        #region Helpers

        public JsonResult DropDownProducts()
        {
            var products = _service.Read()
                .Where(p => p.IsKit == false && p.Deleted == false)
                .OrderBy(p => p.ProductCode)
                .ThenBy(p => p.Description)
                .ToArray().Select(
                p => new SelectItemViewModel
                {
                    ID = p.ID.ToString(),
                    Name = p.ToShortLabel()
                }).ToList();
            return Json(products, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DropDownProductCodes()
        {
            var products = _service.Read()
                .Where(p => p.IsKit == false && p.Deleted == false)
                .OrderBy(p => p.ProductCode)
                .ThenBy(p => p.Description)
                .ToArray().Select(
                p => new SelectItemViewModel
                {
                    ID = p.ProductCode.ToString(),
                    Name = p.ToShortLabel()
                }).ToList();
            return Json(products, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DropDownProducts_Virtualised([DataSourceRequest] DataSourceRequest request)
        {
            var products = GetProducts();

            // Apply filtering.
            if (request.Filters.Count > 0)
            {
                // For simplicity, assume that only one filter will be specified at a time and that
                // the filter type will be "contains".
                var filter = request.Filters[0].GetAllFilterDescriptors().First().Value.ToString();
                if (!String.IsNullOrEmpty(filter))
                {
                    products = products.Where(c => ("" + c.ProductCode + " - " + c.Description).ToLower().Contains(filter.ToLower()));
                }
            }

            // Store the total record count post filtering but pre paging.
            var clientCount = products.Count();

            // Apply paging.
            products = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);

            // Convert to view models.
            var clientVMs = products.ToList().Select(
                p => new SelectItemViewModel
                {
                    ID = p.ID.ToString(),
                    Name = p.ToShortLabel()
                }).ToList();

            return Json(new DataSourceResult { Data = clientVMs, Total = clientCount });
        }

        public ActionResult DropDownProductsNoKits_Virtualised([DataSourceRequest] DataSourceRequest request)
        {
            var products = GetProducts();

            // Apply filtering.
            if (request.Filters.Count > 0)
            {
                // For simplicity, assume that only one filter will be specified at a time and that
                // the filter type will be "contains".
                var filter = request.Filters[0].GetAllFilterDescriptors().First().Value.ToString();
                if (!String.IsNullOrEmpty(filter))
                {
                    products = products.Where(c => ("" + c.ProductCode + " - " + c.Description).ToLower().Contains(filter.ToLower()));
                }
            }

            products = products.Where(p => p.IsKit == false);

            // Store the total record count post filtering but pre paging.
            var clientCount = products.Count();

            // Apply paging.
            products = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);

            // Convert to view models.
            var clientVMs = products.ToList().Select(
                p => new SelectItemViewModel
                {
                    ID = p.ID.ToString(),
                    Name = p.ToShortLabel()
                }).ToList();

            return Json(new DataSourceResult { Data = clientVMs, Total = clientCount });
        }

        public ActionResult DropDownProducts_ValueMapper(string[] values)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;
                var clients = GetProducts().ToList();

                foreach (var client in clients)
                {
                    if (values.Contains(client.ID.ToString()))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<Product> GetProducts()
        {
            var products = _service.Read()
                               .Where(c => c.Deleted == false)
                               .OrderBy(c => c.ProductCode)
                               .AsQueryable();

            return products;
        }

        public static Product GetProduct(string id, decimal quantity, int currencyID, bool applySupplierDiscount = true)
        {
            var repo = new ProductRepository();
            var product = repo.Read().Where(p => p.ID.ToString() == id && p.Deleted == false)
                                    .Include(p => p.Prices)
                                    .Include(p => p.Costs)
                                    .First();

            decimal price = 0m;
            decimal cost = 0m;

            if (product.CurrencyID != currencyID && !product.UsePriceBreaks)
            {
                //Don't copy over the prices
            }
            else if (product.CurrencyID == currencyID && !product.UsePriceBreaks)
            {
                price = product.Price;
                cost = product.Cost;
            }
            else
            {
                try { price = product.Prices.Where(m => m.BreakPoint <= quantity && m.CurrencyID == currencyID).OrderByDescending(m => m.BreakPoint).Select(m => m.Price).First(); }
                catch { }

                try { cost = product.Costs.Where(m => m.BreakPoint <= quantity && m.CurrencyID == currencyID).OrderByDescending(m => m.BreakPoint).Select(m => m.Cost).First(); }
                catch { }
            }

            product.Price = price;
            product.Cost = cost;

            return product;

        }

        public static Product GetProduct(string id)
        {

            var repo = new ProductRepository();
            var product = repo.Read().Where(p => p.ID.ToString() == id && p.Deleted == false).First();
            return product;
        }

        public static Product FindProduct(string productcode, Guid companyID)
        {
            var repo = new ProductRepository();
            var product = repo.Read().Where(p => p.ProductCode == productcode && p.CompanyID == companyID && p.Deleted == false);

            if (product.Count() > 0) return product.First();
            else return null;
        }


        public static bool AddLog(string ID, string Message, User CurrentUser)
        {
            try
            {
                var model = new ProductRepository().Read().Where(p => p.ID.ToString().Equals(ID) && p.CompanyID.Equals(CurrentUser.CompanyID))
                    .First();

                var title = string.Format("Product '{0} - {1}'", model.ProductCode ?? "",
                                                                (model.Description ?? "").Split('\n')[0]);

                ActivityLogsController.CreateLog(ModuleTypeValues.PRODUCTS, string.Format(Message, title), CurrentUser, model.ID.ToString());
            }
            catch { }
            return true;
        }


        #endregion
    }

}