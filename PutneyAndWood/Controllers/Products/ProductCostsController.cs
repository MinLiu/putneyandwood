﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductCostsController : ServiceGridController<ProductCost, ProductCostViewModel, ProductCostService<ProductCostViewModel>>
    {
      
        public ProductCostsController()
            : base(new ProductCostService<ProductCostViewModel>(Guid.Empty, null, null, new ProductCostMapper(), new SnapDbContext()))
        {

        }

        public JsonResult ReadCosts([DataSourceRequest]DataSourceRequest request, Guid pdtID)
        {
            var list = _service.Read(pdtID).OrderBy(p => p.BreakPoint).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ProductCostViewModel>().ToDataSourceResult(request));
        }
        
    }

}