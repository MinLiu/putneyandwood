﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductPricesController : ServiceGridController<ProductPrice, ProductPriceViewModel, ProductPriceService<ProductPriceViewModel>>
    {
      
        public ProductPricesController()
            : base(new ProductPriceService<ProductPriceViewModel>(Guid.Empty, null, null, new ProductPriceMapper(), new SnapDbContext()))
        {

        }


        public JsonResult ReadPrices([DataSourceRequest]DataSourceRequest request, Guid pdtID)
        {
            var list = _service.Read(pdtID).OrderBy(p => p.BreakPoint).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ProductPriceViewModel>().ToDataSourceResult(request));
        }
        
    }

}