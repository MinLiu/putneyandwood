﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
//using Fruitful.Import;
using System.Text.RegularExpressions;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductLinksImportController : BaseController
    {

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.EditProducts == false)
                Response.Redirect("/");
        }

        // GET: ProductImport
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Products");
        }

        public ActionResult Upload()
        {
            return View();
        }


        public ActionResult UploadAlternatives()
        {
            return View("Upload");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAlternatives(HttpPostedFileBase upload, bool removeExisting)
        {
            var importService = new ProductAlternativeImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload, removeExisting);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Alternatives have been successfully imported.\n\nProduct Alternatives Updated: {1}\nProduct Alternatives Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;
            
            return View("Upload");

        }


        public ActionResult UploadAssociatives()
        {
            return View("Upload");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAssociatives(HttpPostedFileBase upload, bool removeExisting)
        {
            var importService = new ProductAssociativeImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload, removeExisting);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Associatives have been successfully imported.\n\nProduct Associatives Updated: {1}\nProduct Associatives Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;
            
            return View("Upload");

        }


    }
}