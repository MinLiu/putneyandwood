﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
//using Fruitful.Import;
using System.Text.RegularExpressions;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductImportController : BaseController
    {
       
        // GET: ProductImport
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult UploadProducts()
        {
            return View();
        }

        public ActionResult UploadPricesCosts()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadProducts(HttpPostedFileBase upload)
        {
            var importService = new ProductImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);
            
            if(result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Products have been successfully imported.\n\nProducts Updated: {1}\nProducts Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            return View("UploadProducts");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadProductsAjax(HttpPostedFileBase upload)
        {
            var importService = new ProductImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            var response = new JsonResponse();
            if (result.Success)
            {
                response.Success = result.Success;
                response.Message = string.Format("{0} Products have been successfully imported.\n\nProducts Updated: {1}\nProducts Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            }
            else
            {
                response.Success = result.Success;
                response.Message = result.Exception.Message;
            }

            return Json(response);

        }



        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult UploadProductImages()
        {
            return View();
        }

        public ActionResult UploadPhotos(IEnumerable<HttpPostedFileBase> images, DateTime importDate, bool ignoreCase = true, bool deleteExisting = false)
        {
            var importService = new ProductImageImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.Import(images, importDate, ignoreCase, deleteExisting);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Images have been successfully imported.\n\nProduct Images Updated: {1}\nProduct Images Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            return Content("");

        }


        public ActionResult UploadKits()
        {
            return View("UploadProducts");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadKits(HttpPostedFileBase upload)
        {
            var importService = new ProductKitItemImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Kit Items have been successfully imported.\n\nProduct Kit Items Updated: {1}\nProduct Kit Items Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            return View("UploadProducts");

        }


        public ActionResult UploadPrices()
        {
            return View("UploadPricesCosts");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadPrices(HttpPostedFileBase upload)
        {
            var importService = new ProductPriceImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Prices have been successfully imported.\n\nProduct Prices Updated: {1}\nProduct Prices Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            return View("UploadPricesCosts");

        }

        public ActionResult UploadCosts()
        {
            return View("UploadPricesCosts");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadCosts(HttpPostedFileBase upload)
        {
            var importService = new ProductCostImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Product Costs have been successfully imported.\n\nProduct Costs Updated: {1}\nProduct Costs Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;

            return View("UploadPricesCosts");

        }
         
    }
}