﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using Kendo.Mvc;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductGridController : ServiceDeleteableGridController<Product, ProductGridViewModel, ProductService<ProductGridViewModel>>
    {

        public ProductGridController()
            : base(new ProductService<ProductGridViewModel>(Guid.Empty, null, new ProductGridMapper(), new SnapDbContext()))
        {

        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, bool del = false)
        {
            
            //User passed parameters on filter results
            var products = _service.Search(filterText, "", null, del)
                                    .Include(p => p.Currency)
                                    .Include(p => p.Prices.Select(c => c.Currency))
                                    .OrderBy(p => p.SortPos);

            var productList = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(pdt => _service.MapToViewModel(pdt));
            DataSourceResult result = new DataSourceResult { Data = productList, Total = products.Count() };
            
            return Json(result);

        }

        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var products = _service.Search("", "", null, true)
                                    .Include(p => p.Currency)
                                    .Include(p => p.Prices.Select(c => c.Currency));

            products = ApplyQueryFiltersSort(request, products, "ProductCode");

            var productList = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(pdt => _service.MapToViewModel(pdt));
            DataSourceResult result = new DataSourceResult { Data = productList, Total = products.Count() };
            return Json(result);
        }

        public bool _UpdatePositions(Guid ID, int newIndex)
        {
            Product move = null;

            move = _service.FindByID(ID);

            var items = _service.Read().OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            items.Insert((newIndex >= 0) ? newIndex : 0, move);

            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
                _service.Update(i, new string[] { "SortPos" });
            }
            return true;
        }
    }

}