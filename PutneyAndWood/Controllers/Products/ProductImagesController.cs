﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductImagesController :  ServiceGridController<ProductImage, ImageViewModel, ProductImageService<ImageViewModel>>
    {

        public ProductImagesController()
            : base(new ProductImageService<ImageViewModel>(Guid.Empty, null, null, new ProductImageMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid ID)
        {
            var list = _service.Read(ID).Where(p => p.OwnerID == ID).OrderBy(p => p.SortPos)
                                   .ToList().Select(p => _service.MapToViewModel(p)).ToList();


            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ImageViewModel>().ToDataSourceResult(request));
        }


        public ActionResult UploadImages(IEnumerable<HttpPostedFileBase> uploadedImages, Guid ID)
        {
            if (uploadedImages != null)
            {
                foreach (var file in uploadedImages)
                {
                    _service.UploadImage(ID, file);
                }
            }

            return Content("");
        }
        

        public JsonResult _DeleteImage(Guid imageID)
        {
            var model = _service.FindByID(imageID);
            _service.Destroy(model);
            return Json("Success");
        }
        

        [HttpPost]
        public ActionResult _UpdateItemPositions(Guid ID, Guid imageID, int oldIndex, int newIndex)
        {
            //_service.ParentID = ID;
            _service.UpdatePositions(imageID, oldIndex, newIndex);
            return Json("Success");
        }

    }

}