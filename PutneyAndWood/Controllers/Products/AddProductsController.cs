﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class AddProductsController : ServiceGridController<Product, ProductGridViewModel, ProductService<ProductGridViewModel>>
    {

        public AddProductsController()
            : base(new ProductService<ProductGridViewModel>(Guid.Empty, null, new ProductGridMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, string category, int currencyID)
        {
            var products = _service.Search(filterText, category, null)
                                    .Include(p => p.Category)
                                    .Include(p => p.Prices)
                                    .Include(p => p.Prices.Select(c => c.Currency));
                    //.OrderBy(pdt => pdt.ProductCode).ToList();
            //.Select(p => ProductMapper.ModelFromEntity(p, currencyID));

            products = ApplyQueryFiltersSort(request, products, "ProductCode");

            var productList = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(pdt => _service.MapToViewModel(pdt));
            DataSourceResult result = new DataSourceResult { Data = productList, Total = products.Count() };
            return Json(result);
        }
        

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {

            var products = _service.Read()
                                    .Include(p => p.Category)
                                    .Include(p => p.Prices)
                                    .Include(p => p.Prices.Select(c => c.Currency));
            
            //.OrderBy(pdt => pdt.ProductCode).ToList();
            //.Select(p => ProductMapper.ModelFromEntity(p, currencyID));

            products = ApplyQueryFiltersSort(request, products, "ProductCode");

            var productList = products.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(pdt => _service.MapToViewModel(pdt));
            DataSourceResult result = new DataSourceResult { Data = productList, Total = products.Count() };
            return Json(result);
        }


        

    }

}