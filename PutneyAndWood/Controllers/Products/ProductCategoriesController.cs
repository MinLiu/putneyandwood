﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductCategoriesController :  BaseController
    {

        protected readonly ProductCategoryService _service;

        public ProductCategoriesController()
        {
            _service = new ProductCategoryService(Guid.Empty, new SnapDbContext());
        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            //_service.CurrentUserID = CurrentUser.Id;
            _service.CompanyID = CurrentUser.CompanyID;
        }

        public JsonResult DropDownProductCategories()
        {
            var _repo = new ProductRepository();
            var cats = _service.GetCategorys()
                                .Select(
                                t => new SelectItemViewModel
                                {
                                    ID = t,
                                    Name = t
                                }
                            ).ToList();
            return Json(cats, JsonRequestBehavior.AllowGet);
            
        }
        

        public ActionResult DropDownCategories_ValueMapper(string[] values)
        {
            var repo = new ProductRepository();

            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;
                var categories = repo.Read().Where(x => x.CompanyID == CurrentUser.CompanyID && !string.IsNullOrEmpty(x.Category) && x.Deleted == false).ToList();

                foreach (var category in categories)
                {
                    if (values.Contains(category.ID.ToString()))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DropDownCategories_Virtualised([DataSourceRequest] DataSourceRequest request)
        {
            var categories = _service.GetCategorys()
                               .Select(
                               t => new SelectItemViewModel
                               {
                                   ID = t,
                                   Name = t
                               }
                           );

            // Apply filtering.
            if (request.Filters.Count > 0)
            {
                // For simplicity, assume that only one filter will be specified at a time and that
                // the filter type will be "contains".
                var filter = request.Filters[0].GetAllFilterDescriptors().First().Value.ToString();
                if (!String.IsNullOrEmpty(filter))
                {
                    categories = categories.Where(c => (c.Name).ToLower().Contains(filter.ToLower()));
                }
            }

            // Store the total record count post filtering but pre paging.
            var categoryCount = categories.Count();

            // Convert to view models.
            var categoryVMs = categories.ToList();

            return Json(new DataSourceResult { Data = categoryVMs, Total = categoryCount });
        }
    }

}