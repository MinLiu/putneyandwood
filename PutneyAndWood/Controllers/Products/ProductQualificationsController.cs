﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductQualificationsController : GridController<ProductQualification, ProductQualificationGridViewModel>
    {

        public ProductQualificationsController()
            : base(new ProductQualificationRepository(), new ProductQualificationGridMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid productID)
        {
            var list = _repo.Read().Where(x => x.ProductID == productID)
                                   .OrderByDescending(o => o.SortPos)
                                   .ToList()
                                   .Select(p => _mapper.MapToViewModel(p));

            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<QuotationCommentViewModel>().ToDataSourceResult(request));
        }

        public override JsonResult Create([DataSourceRequest] DataSourceRequest request, ProductQualificationGridViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = _mapper.MapToModel(viewModel);

                var qulifications = _repo.Read().Where(x => x.ProductID == model.ProductID).OrderByDescending(x => x.SortPos).Select(x => x.SortPos).ToList();
                var lastPos = qulifications.Count() > 0 ? qulifications.First() : 0;
                model.SortPos = lastPos + 1;

                _repo.Create(model);
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult _UpdateQualificationPositions(Guid qualificationID, int oldIndex, int newIndex)
        {
            UpdatePositions(qualificationID, oldIndex, newIndex);
            return Json("Success");
        }

        public bool UpdatePositions(Guid qualificationID, int oldPosition, int newPosition)
        {
            ProductQualification move = null;
            ProductQualification top = null;
            ProductQualification bottom = null;

            move = _repo.Find(qualificationID);

            var items = _repo.Read().Where(i => i.ProductID == move.ProductID).OrderBy(i => i.SortPos).ToList();
            items.Remove(move);

            top = (newPosition - 1 >= 0) ? items.ElementAt(newPosition - 1) : null;
            bottom = (newPosition < items.Count) ? items.ElementAt(newPosition) : null;

            items.Insert((newPosition >= 0) ? newPosition : 0, move);

            int sortPos = 0;
            foreach (var i in items)
            {
                i.SortPos = sortPos;
                sortPos++;
                _repo.Update(i, new string[] { "SortPos" });
            }
            return true;
        }


    }

}