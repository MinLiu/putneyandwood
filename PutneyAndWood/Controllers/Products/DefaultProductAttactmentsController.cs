﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class DefaultProductAttachmentsController : ServiceGridController<DefaultProductAttachment, DefaultProductAttachmentViewModel, DefaultProductAttachmentService<DefaultProductAttachmentViewModel>>
    {

        public DefaultProductAttachmentsController()
            : base(new DefaultProductAttachmentService<DefaultProductAttachmentViewModel>(Guid.Empty, null, null, new DefaultProductAttachmentMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid ID)
        {
            var list = _service.Read(ID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<DefaultProductAttachmentViewModel>().ToDataSourceResult(request));
        }

        

        public ActionResult UploadDefaultAttachment(IEnumerable<HttpPostedFileBase> files, Guid ID)
        {            
            if (files != null)
            {                
                foreach (var file in files)
                {
                    _service.ParentID = ID;
                    _service.UploadAttachment(file);
                }

            }

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public ActionResult LinkDefaultAttachment(Guid ID, string link)
        {
            _service.ParentID = ID;
            _service.LinkAttachment(link);
            return JavaScript("");
        }

    }

}