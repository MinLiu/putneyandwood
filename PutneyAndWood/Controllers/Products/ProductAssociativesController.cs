﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ProductAssociativesController : ServiceGridController<ProductAssociative, ProductAssociativeViewModel, ProductAssociativeService<ProductAssociativeViewModel>>
    {

        public ProductAssociativesController()
            : base(new ProductAssociativeService<ProductAssociativeViewModel>(Guid.Empty, null, null, new ProductAssociativeMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid pdtID)
        {
            var list = _service.Read(pdtID)
                                   .Include(p => p.Product)
                                   .OrderBy(p => p.Product.ProductCode).ThenBy(p => p.Product.Description)
                                   .ToList().Select(p => _service.MapToViewModel(p)).ToList();
            return Json(list.ToDataSourceResult(request));
        }


        public JsonResult ReadAssociatives([DataSourceRequest]DataSourceRequest request, string productCode)
        {
            var pdtService = new ProductService<ProductGridViewModel>(_service.CompanyID, _service.CurrentUserID, new ProductGridMapper(), _service.DbContext);
            var list = pdtService.ReadAssociatives(productCode).ToList().Select(p => pdtService.MapToViewModel(p)).ToList();

            return Json(list.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ProductAssociativeViewModel>().ToDataSourceResult(request));
        }
        

    }

}