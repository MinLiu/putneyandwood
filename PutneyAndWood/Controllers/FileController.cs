﻿using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SnapSuite.Controllers
{

    public class FileController : Controller
    {

        [AllowAnonymous]
        public ActionResult Download(string n, string p)
        {
            if (!p.StartsWith("/")) p = p.Insert(0, "/");
            if (p.EndsWith("/")) p = p.Remove(p.Length - 1);

            if (string.IsNullOrWhiteSpace(n)) n = "file";

            var fileExtension = Path.GetExtension(p);
            var filename = n.Truncate(200);
            var mimeType = "application/unknown";

            if (!string.IsNullOrWhiteSpace(filename))
            {
                try
                {
                    var extension = Path.GetExtension(filename);
                    if(extension != fileExtension) filename = string.Format("{0}{1}", filename, fileExtension);
                }
                catch { filename = string.Format("{0}{1}", filename, fileExtension); }
            }

            try { mimeType = MimeMapping.GetMimeMapping(filename); } catch { }
            
            return File(Server.MapPath(p), mimeType, filename);
        }
        

    }
}