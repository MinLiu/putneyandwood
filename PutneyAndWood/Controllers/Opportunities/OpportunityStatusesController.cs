﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class OpportunityStatusesController : ServiceGridController<OpportunityStatus, OpportunityStatusViewModel, OpportunityStatusService<OpportunityStatusViewModel>>
    {
      
        public OpportunityStatusesController()
            : base(new OpportunityStatusService<OpportunityStatusViewModel>(Guid.Empty, null, new OpportunityStatusMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        [HttpGet]
        public ActionResult DropDownOpportunityStatuses(Guid oppTypeID)
        {
            var models = _service.Read()
                              .Where(x => x.OpportunityTypeID == oppTypeID)
                              .OrderBy(x => x.SortPos);
            var viewModels = models.ToList().Select(x => new SelectItemViewModel()
            {
                ID = x.ID.ToString(),
                Name = x.Name
            }).ToList();

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Reorder(Guid statusID, int offset)
        {
            _service.Reorder(statusID, offset);
            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public ActionResult Remove(Guid statusID)
        {
            var oppStatus = _service.FindByID(statusID);
            try
            {
                _service.Destroy(oppStatus);
                return Json(new { Status = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { Status = "Fail", ErrorMessage = ex.Message });
            }
        }

    }
    
}