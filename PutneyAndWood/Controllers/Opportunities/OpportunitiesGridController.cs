﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class OpportunitiesGridController : ServiceGridController<Opportunity, OpportunityViewModel, OpportunityService<OpportunityViewModel>>
    {
      
        public OpportunitiesGridController()
            : base(new OpportunityService<OpportunityViewModel>(Guid.Empty, null, new OpportunityMapper(), new SnapDbContext()))
        {

        }

        public void SaveCookies(string assignedUser, bool showArchived)
        {
            HttpCookie cookie = Request.Cookies["OpportunitySearch"] ?? new HttpCookie("OpportunitySearch");
            cookie["ShowArchived"] = showArchived.ToString();
            cookie["AssignedUser"] = assignedUser;

            cookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(cookie);
        }

        [HttpGet]
        public JsonResult Read([DataSourceRequest] DataSourceRequest request, Guid oppStatusID, string filterText, string assignedUser, bool showArchived = false)
        {
            SaveCookies(assignedUser, showArchived);

            var list = _service.Search(oppStatusID, filterText, assignedUser, showArchived)
                                .OrderBy(x => x.SortPos)
                                .ToList()
                                .Select(t => new OppotrunityListViewModel()
                                {
                                    ID = t.ID.ToString(),
                                    Name = t.Name,
                                    Importance = t.BarValue1,
                                    Probability = t.BarValue2,
                                    Value = t.Value,
                                    ClientName = t.Client != null ? t.Client.Name : "",
                                    Status = t.Events.Select(e => e.Weight).OrderByDescending(e => e).DefaultIfEmpty(-1).FirstOrDefault(),
                                    EventDescription = t.Events.OrderByDescending(e => e.Weight).Select(e => String.Format("{0} - {1}", e.Timestamp.ToString("dd/MM/yyyy HH:mm"), e.Message)).DefaultIfEmpty(null).FirstOrDefault(),
                                    LabelValue1 = t.OpportunityType.LabelValue1,
                                    LabelValue2 = t.OpportunityType.LabelValue2,
                                })
                                //.OrderByDescending(t => t.Status).ThenByDescending(t => t.Value * t.Importance).ThenByDescending(t => t.Probability)
                                .ToList();
            //.OrderByDescending(t => t.Timestamp < DateTime.Today)
            //.ThenByDescending(t => t.Timestamp);
            //.ToArray().Select(t => mapper.MapToViewModel(t)

            return Json(list, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult ReadClientAssociated([DataSourceRequest] DataSourceRequest request, DateType dateType, string fromD, string toD, string cltID, bool showArchived = false)
        {
            var mapper = new OpportunityGridMapper();
            var list = _service.Search(dateType, fromD, toD, showArchived)
                        .Where(t => (String.IsNullOrEmpty(cltID) || t.ClientID.ToString().Equals(cltID)))
                        .OrderByDescending(x => x.Created)
                        .ToList()
                        .Select(t => mapper.MapToViewModel(t))
                        .ToList();

            return Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }

        public JsonResult DropDownOpportunities(Guid? clientID)
        {
            var opps = _service.ReadList()
                             .Where(x => x.CompanyID == CurrentUser.CompanyID)
                             .Where(x => x.ClientID == clientID)
                             .ToList()
                             .Select(x => new SelectItemViewModel()
                             {
                                 ID = x.ID.ToString(),
                                 Name = String.Format("{0}", x.Name)
                             })
                             .ToList();

            return Json(opps, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ReadAll(Guid oppTypeID, string filterText, string assignedUser, bool showArchived = false)
        {
            SaveCookies(assignedUser, showArchived);
            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.

            var oppStatusService = new OpportunityStatusService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
            var list = oppStatusService.Read().Where(t => t.OpportunityType.ID == oppTypeID)
                .ToList()
                .Select(t => new 
                {
                    oppStatusID = t.ID,
                    oppStatusName = t.Name,
                    opportunities = t.Opportunities.Where(x => string.IsNullOrEmpty(assignedUser) || assignedUser == "-1" && x.AssignedUserID == null || x.AssignedUserID.Equals(assignedUser))
                                                   .Where(x => string.IsNullOrEmpty(filterText) || x.Name.ToLower().Contains(filterText.ToLower()) || x.Client.Name.ToLower().Contains(filterText.ToLower()))
                                                   .Where(x => showArchived == true || (showArchived == false && x.Archived == false))
                                                   .OrderBy(x => x.SortPos)
                                                   .ToList()
                                                   .Select(x => new OppotrunityListViewModel()
                                                   {
                                                       ID = x.ID.ToString(),
                                                       Name = x.Name,
                                                       Importance = x.BarValue1,
                                                       Probability = x.BarValue2,
                                                       Value = x.Value,
                                                       ClientName = x.Client != null ? x.Client.Name : "",
                                                       Status = x.Events.Select(e => e.Weight).OrderByDescending(e => e).DefaultIfEmpty(-1).FirstOrDefault(),
                                                       EventDescription = x.Events.OrderByDescending(e => e.Weight).Select(e => String.Format("{0} - {1}", e.Timestamp.ToString("dd/MM/yyyy HH:mm"), e.Message)).DefaultIfEmpty(null).FirstOrDefault(),
                                                       LabelValue1 = x.OpportunityType.LabelValue1,
                                                       LabelValue2 = x.OpportunityType.LabelValue2,
                                                   })

                })
                .ToList();

            return Json(list);

        }
    }
    
}