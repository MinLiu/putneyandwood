﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class OpportunitiesController : BaseController
    {
      

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }
        

        // GET: Opportunities
        public ActionResult Index(string id)
        {
            return RedirectToAction("Index", "Pipeline", new { id = id });
        }

        public ActionResult Edit(Guid id)
        {
            return RedirectToAction("Index", new { id = id });
        }
        
    }
    
}