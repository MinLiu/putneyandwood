﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class OpportunityEventsController : ServiceGridController<ClientEvent, ClientEventViewModel, ClientEventService<ClientEventViewModel>>
    {

        public OpportunityEventsController()
            : base(new ClientEventService<ClientEventViewModel>(Guid.Empty, null, null, new ClientEventMapper(), new SnapDbContext()))
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadList([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read().Where(i => i.ClientID == cltID)
                .Include(p => p.Client)
                .Include(p => p.ClientContact)
                .Include(t => t.ClientContact)
                .Include(t => t.AssignedUser)
                .Include(t => t.Creator)
                .OrderBy(o => o.Timestamp).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

      

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, Guid oppID)
        {
            var list = _service.Read()
                            .Where(t => t.OpportunityID == oppID)
                            .OrderByDescending(t => t.Timestamp);

            var taskList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(t => _service.MapToViewModel(t));
            DataSourceResult result = new DataSourceResult { Data = taskList.ToList(), Total = list.Count() };
            return Json(result);

        }
        

    }

}