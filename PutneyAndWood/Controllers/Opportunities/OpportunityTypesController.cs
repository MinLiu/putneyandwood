﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class OpportunityTypesController : ServiceGridController<OpportunityType, OpportunityTypeViewModel, OpportunityTypeService<OpportunityTypeViewModel>>
    {

        public OpportunityTypesController()
            : base(new OpportunityTypeService<OpportunityTypeViewModel>(Guid.Empty, null, new OpportunityTypeMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
                Response.Redirect("/");
        }


        [HttpGet]
        public ActionResult DropDownOpportunityTypes()
        {
            var models = _service.Read();
            var viewModels = models.ToList().Select(x => new SelectItemViewModel()
            {
                ID = x.ID.ToString(),
                Name = x.Name
            }).ToList();

            return Json(viewModels, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        public ActionResult Remove(Guid oppTypeID)
        {
            var oppType = _service.FindByID(oppTypeID);
            try
            {
                _service.Destroy(oppType);
                return Json(new { Status = "Success" });
            }
            catch(Exception ex)
            {
                return Json(new { Status = "Fail", ErrorMessage = ex.Message });
            }
        }
    }
    
}