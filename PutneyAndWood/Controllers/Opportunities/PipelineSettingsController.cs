﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Super User,Owner,Admin")]
    public class PipelineSettingsController : BaseController        
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
        }
        
        // GET: Settings
        public ActionResult Edit()
        {
            var service = new OpportunityTypeService<OpportunityTypeViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityTypeMapper(), new SnapDbContext());

            var viewModel = new OpportunitySettingViewModel()
            {
                OpportunityTypes = service.Read()
                                       .ToList()
                                       .Select(x => service.MapToViewModel(x))
                                       .ToList()
            };
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult _NewStatus(Guid oppTypeID)
        {
            return PartialView(new OpportunityStatusViewModel() { OpportunityTypeID = oppTypeID.ToString() });
        }

        [HttpPost]
        public ActionResult _NewStatus(OpportunityStatusViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }
            
            var service = new OpportunityStatusService<OpportunityStatusViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityStatusMapper(), new SnapDbContext());
            
            var model = service.MapToModel(viewModel, false);
            model.CompanyID = CurrentUser.CompanyID;
            model.SortPos = service.Read()
                                .Where(x => x.OpportunityTypeID == model.OpportunityTypeID)
                                .Select(x => x.SortPos)
                                .OrderByDescending(x => x)
                                .FirstOrDefault() + 1;

            service.Create(model);
            return Json(new { Status = "Success" });
        }

        [HttpGet]
        public ActionResult _EditStatus(Guid statusID)
        {
            var service = new OpportunityStatusService<OpportunityStatusViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityStatusMapper(), new SnapDbContext());
            var viewModel = service.MapToViewModel(service.FindByID(statusID));
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditStatus(OpportunityStatusViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var service = new OpportunityStatusService<OpportunityStatusViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityStatusMapper(), new SnapDbContext());
            service.Update(viewModel);
            return Json(new { Status = "Success" });
        }

        [HttpGet]
        public ActionResult _NewOpportunityType()
        {
            return PartialView(new OpportunityTypeViewModel() { LabelValue1 = "Importance", LabelValue2 = "Probability" });
        }

        [HttpPost]
        public ActionResult _NewOpportunityType(OpportunityTypeViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var service = new OpportunityTypeService<OpportunityTypeViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityTypeMapper(), new SnapDbContext());
            service.Create(viewModel);
            return Json(new { Status = "Success" });
        }

        [HttpGet]
        public ActionResult _EditOpportunityType(Guid oppTypeID)
        {
            var service = new OpportunityTypeService<OpportunityTypeViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityTypeMapper(), new SnapDbContext());            
            var viewModel = service.MapToViewModel(service.FindByID(oppTypeID));
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditOpportunityType(OpportunityTypeViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var service = new OpportunityTypeService<OpportunityTypeViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new OpportunityTypeMapper(), new SnapDbContext());
            service.Update(viewModel);

            return Json(new { Status = "Success" });
        }

    }
}