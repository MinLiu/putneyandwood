﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class PipelineController : ServiceEntityController<Opportunity, OpportunityViewModel, OpportunityService<OpportunityViewModel>>
    {
      
        public PipelineController()
            : base(new OpportunityService<OpportunityViewModel>(Guid.Empty, null, new OpportunityMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }


        #region Main Opportunity Page

        // GET: Opportunities
        public ActionResult Index(string id)
        {
            SetViewBags();
            if (!string.IsNullOrWhiteSpace(id))
                ViewBag.ID = id;
            return View();
        }

        public ActionResult Edit(Guid id)
        {
            return RedirectToAction("Index", new { id = id });
        }

        public void SaveCookies(string oppTypeID)
        {
            HttpCookie cookie = Request.Cookies["OpportunitySearch"] ?? new HttpCookie("OpportunitySearch");
            cookie["OppTypeID"] = oppTypeID;

            cookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(cookie);
        }

        public ActionResult _Opportunities(string oppTypeID)
        {
            if (!string.IsNullOrWhiteSpace(oppTypeID))
            {
                SaveCookies(oppTypeID);
                var oppStatusService = new OpportunityStatusService<OpportunityStatusViewModel>(_service.CompanyID, _service.CurrentUserID, new OpportunityStatusMapper(), _service.DbContext);
                var models = oppStatusService.Read().Where(x => x.OpportunityTypeID.ToString() == oppTypeID).OrderBy(x => x.SortPos);

                var viewModel = models.ToList().Select(x => oppStatusService.MapToViewModel(x)).ToList();
                return PartialView(viewModel);
            }
            else
            {
                return PartialView(new List<OpportunityStatusViewModel>());
            }
        }

        [HttpGet]
        public ActionResult _NewOpportunity(string typeID, string statusID)
        {
            var oppType = new OpportunityTypeService<OpportunityTypeViewModel>(_service.CompanyID, _service.CurrentUserID, new OpportunityTypeMapper(), _service.DbContext).Find(Guid.Parse(typeID));
            var viewModel = new OpportunityViewModel() { LabelValue1 = oppType.LabelValue1, LabelValue2 = oppType.LabelValue2, AssignedUserID = CurrentUser.Id };

            if (!string.IsNullOrWhiteSpace(typeID) && typeID != "undefined")
            {
                viewModel.OpportunityTypeID = typeID;
            }
            if (!string.IsNullOrWhiteSpace(statusID) && statusID != "undefined")
            {
                viewModel.StatusID = statusID;
            }
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _NewOpportunity(OpportunityViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }
            var model = _service.MapToModel(viewModel);
            model = _service.Create(model);
            
            // Add Log
            //AddLog(model.ID);

            return Json(new { ID = model.ID });
        }

        [HttpGet]
        public ActionResult _EditOpportunity(Guid ID)
        {
            var model = _service.FindByID(ID);
            var viewModel = _service.MapToViewModel(model);
            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditOpportunityDetails(OpportunityViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            _service.Update(viewModel);

            var model = _service.FindByID(Guid.Parse(viewModel.ID));
            viewModel = _service.MapToViewModel(model);
            ViewBag.Saved = true;
            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult _NewEvent(Guid oppID, string eventAssignedUserID, string clientID, string contactID, string timestamp)
        {
            var opportunity = _service.FindByID(oppID);
            var viewModel = new OpportunityEventViewModel()
            {
                EventAssignedUserID = eventAssignedUserID?? opportunity.AssignedUserID,
                EventCreatorID = CurrentUser.Id,
                ClientID = clientID?? opportunity.ClientID.ToString(),
                ContactID = contactID?? opportunity.ClientContactID.ToString(),
                OpportunityID = opportunity.ID.ToString(),
                Timestamp = !string.IsNullOrEmpty(timestamp) ? DateTime.Parse(timestamp) : DateTime.Now
            };

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _NewEvent(OpportunityEventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                var service = new ClientEventService<OpportunityEventViewModel>(_service.CompanyID, null, _service.CurrentUserID, new OpportunityEventMapper(), _service.DbContext);
                //var mapper = new OpportunityEventMapper();

                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                //var model = mapper.MapToModel(viewModel);
                //model.CompanyID = CurrentUser.CompanyID;
                //model.CreatorID = CurrentUser.Id;
                service.Create(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        [HttpGet]
        public ActionResult _EditEvent(Guid eventID)
        {
            var eventService = new ClientEventService<OpportunityEventViewModel>(_service.CompanyID, null, _service.CurrentUserID, new OpportunityEventMapper(), _service.DbContext);
            var viewModel = eventService.MapToViewModel(eventService.Find(eventID));

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult _EditEvent(OpportunityEventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                var service = new ClientEventService<OpportunityEventViewModel>(_service.CompanyID, null, _service.CurrentUserID, new OpportunityEventMapper(), _service.DbContext);
                //var mapper = new OpportunityEventMapper();

                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                //var model = service.FindByID(Guid.Parse(viewModel.ID));
                //mapper.MapToModel(viewModel, model);
                service.Update(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

        #endregion

        public ActionResult ChangeStatus(Guid id, Guid statusID, int pos)
        {
            var model = _service.ChangeStatus(id, statusID, pos);
            
            return Json(new { Status = "Success" });
        }

        [HttpPost]
        public ActionResult Reorder(Guid id, int offset)
        {
            _service.Reorder(id, offset);

            return Json(new { Status = "Success" });
        }

        //public bool AddLog(Guid OpportunityID)
        //{
        //    var opp = _repo.Read()
        //                   .Where(x => x.ID == OpportunityID)
        //                   .Include(x => x.Client)
        //                   .FirstOrDefault();

        //    return AddLog(opp);
        //}

        //public bool AddLog(Opportunity opp)
        //{
        //    var oppLog = new OpportunityLog()
        //    {
        //        OpportunityStatusID = opp.StatusID,
        //        CompanyID = opp.CompanyID,
        //        Timestamp = DateTime.Now,
        //        ClientName = opp.Client.Name,
        //        OpportunityID = opp.ID
        //    };

        //    new OpportunityLogRepository().Create(oppLog);

        //    return true;
        //}

        public void SetViewBags()
        {
            HttpCookie cookie = Request.Cookies["OpportunitySearch"] ?? new HttpCookie("OpportunitySearch");
            ViewBag.assignedUser = !string.IsNullOrEmpty(cookie["AssignedUser"]) ? cookie["AssignedUser"] : "";
            ViewBag.createdUser = !string.IsNullOrEmpty(cookie["CreatedUser"]) ? cookie["CreatedUser"] : "";
            ViewBag.dateType = !string.IsNullOrEmpty(cookie["DateType"]) ? cookie["DateType"] : "0";
            ViewBag.fromD = !string.IsNullOrEmpty(cookie["FromD"]) ? cookie["FromD"] : DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.toD = !string.IsNullOrEmpty(cookie["ToD"]) ? cookie["ToD"] : DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.client = !string.IsNullOrEmpty(cookie["Client"]) ? cookie["Client"] : "";
            ViewBag.showArchived = !string.IsNullOrEmpty(cookie["ShowArchived"]) ? cookie["ShowArchived"] : "false";
            ViewBag.oppTypeID = !string.IsNullOrEmpty(cookie["OppTypeID"]) ? cookie["OppTypeID"] : "";
        }
    }
    
}