﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Controllers
{

   
    [Authorize]
    public class ConversionController : BaseController
    {
        
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
        }


        /*
        public ActionResult QuotationToJobOld(string ID)
        {
            using (var context = new SnapDbContext())
            {

                //Get Last Order number based on Settings
                var defaults = new JobSettingsRepository().GetSettings(CurrentUser.CompanyID);
                int lastNum = defaults.StartingNumber - 1; //Get from defaults

                List<Job> last = context.Jobs.Where(q => q.CompanyID == CurrentUser.CompanyID).OrderByDescending(q => q.Number).Take(1).ToList();
                if (last.Count() > 0)
                {
                    if (lastNum < last[0].Number)
                        lastNum = last[0].Number;
                }

                //Get the original version; Include items, attachments; except for comments.
                var original = context.Quotations.Where(q => q.CompanyID == CurrentUser.CompanyID && q.ID.ToString() == ID)
                                                 .Include(q => q.Items)
                                                 .Include(q => q.Items.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.CostItems)
                                                 .Include(q => q.CostItems.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.Sections)
                                                 .Include(q => q.Attachments)
                                                 .Include(q => q.Attachments.Select(x => x.File))
                                                 .Include(q => q.Notifications)
                                                 .Include(q => q.CostItems)
                                                 .Include(q => q.LabourItems)
                                                 .First();

                //Can not convert is price breaks are enabled
                if (original.UsePriceBreaks) throw new Exception("Can not convert quotation if it uses price breaks");

                //Copy origignal
                var newOrder = new Job {

                    ClientID = original.ClientID,
                    ClientContactID = original.ClientContactID,
                    QuotationID = original.ID,
                    ClientReference = original.ClientReference,
                    Note = string.Format("Created from Quotation {0}{1}\n\n{2}", original.Reference ?? "", original.Number, original.Note),
                    PoNumber = original.PoNumber,
                    Description = original.Description,
                    JsonCustomFieldValues = original.JsonCustomFieldValues,
                    InvoiceAddressID = original.InvoiceAddressID,
                    DeliveryAddressID = original.DeliveryAddressID,
                    EstimatedStartDate = DateTime.Now,
                    EstimatedDelivery = original.EstimatedDelivery,

                    TotalVat = original.TotalVat,
                    TotalNet = original.TotalNet,
                    TotalPrice = original.TotalPrice,                    

                    CheckSum = Guid.NewGuid().ToString("N"),
                    CompanyID = CurrentUser.CompanyID,
                    Reference = defaults.DefaultReference, //Get from defaults
                    Number = lastNum + 1,
                    StatusID = JobStatusValues.DRAFT,
                    CurrencyID = original.CurrencyID,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    AssignedUserID = CurrentUser.Id, //Assign to order the user who is logged in.
                    CreatedBy = CurrentUser.Email,
                    IsApproved = false
                };


                context.Jobs.Add(newOrder);
                context.SaveChanges();

                var sections = original.Sections.Select(i => new JobSection
                {
                    ID = i.ID,
                    JobID = i.QuotationID,
                    Name = i.Name,
                    IncludeInTotal = i.IncludeInTotal,
                    SortPos = i.SortPos,
                    TotalNet = i.TotalNet,
                    TotalVat = i.TotalVat,
                    TotalPrice = i.TotalPrice,
                }).ToList();

                var sectionIds = sections.Select(i => i.ID).ToList();

                //Copy Order Items (Do not copy comments)                
                var items = original.Items.Select(i => new JobItem
                {
                    ID = i.ID,
                    JobID = i.QuotationID,
                    SectionID = i.SectionID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    AccountCode = i.AccountCode,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    SetupCost = i.SetupCost,
                    VAT = i.VAT,
                    Price = i.Price,
                    Total = i.Total,

                    ImageURL = i.ImageURL,
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentItemID = i.ParentItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment
                    
                }).ToList();

                var images = context.QuotationItemImages.Where(i => i.Owner.QuotationID == original.ID).Include(i => i.File)
                                    .ToList()
                                    .Select(i => new JobItemImage{ OwnerID = i.OwnerID, FileID = i.FileID, SortPos = i.SortPos })
                                    .ToList();

                var attachments = original.Attachments.Select(i => new JobAttachment
                {
                    ID = i.ID,
                    JobID = i.QuotationID,
                    Filename = i.Filename,
                    FileID = i.FileID,
                }).ToList();

                
                var costItems = original.CostItems.Select(i => new JobCostItem
                {
                    ID = i.ID,
                    JobID = i.QuotationID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    SetupCost = i.SetupCost,
                    VAT = i.VAT,
                    Cost = i.Cost,
                    Total = i.Total,
                    
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentCostItemID = i.ParentCostItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment

                }).ToList();


                var labourItems = original.LabourItems.Select(i => new JobLabourItem
                {
                    ID = i.ID,
                    JobID = i.QuotationID,
                    Description = i.Description,
                    EstimateHours = i.Hours,
                    Hours = i.Hours,
                    HourlyRate = i.HourlyRate,
                    Total = i.Total,                    
                    SortPos = i.SortPos,
                }).ToList();


                //When copying first detached. This insure copies are made instead of updating existing records
                foreach (var s in sections) { context.Entry(s).State = EntityState.Detached; s.JobID = newOrder.ID; s.ID = Guid.NewGuid(); }
                foreach (var i in items)
                {
                    context.Entry(i).State = EntityState.Detached;
                    i.JobID = newOrder.ID;
                    i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                    //New Guid assign below
                } 
                foreach (var a in attachments) { context.Entry(a).State = EntityState.Detached; a.JobID = newOrder.ID; a.ID = Guid.NewGuid(); }
                foreach (var i in costItems) { context.Entry(i).State = EntityState.Detached; i.JobID = newOrder.ID; i.ID = Guid.NewGuid(); }
                foreach (var l in labourItems) { context.Entry(l).State = EntityState.Detached; l.JobID = newOrder.ID; l.ID = Guid.NewGuid(); }

                context.JobSections.AddRange(sections);
                context.JobAttachments.AddRange(attachments);
                context.JobComments.Add(new JobComment { JobID = newOrder.ID, Timestamp = DateTime.Now, Email = CurrentUser.Email, Message = string.Format("Created from Quotation {0}{1}", original.Reference ?? "", original.Number) });
                context.JobLabourItems.AddRange(labourItems);

                //Add Default Notificatins
                foreach (var a in context.DefaultJobNotifications.Where(a => a.JobSettingsID == defaults.CompanyID && a.Email != CurrentUser.Email))
                {
                    context.JobNotifications.Add(new JobNotification
                    {
                        JobID = newOrder.ID,
                        Email = a.Email
                    });
                }

                //Add the users email address.
                context.JobNotifications.Add(new JobNotification { JobID = newOrder.ID, Email = CurrentUser.Email });

                context.SaveChanges();
                
                //Because items (kits) can have sub items. We have to add them with differently
                foreach (var i in items)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();

                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }

                        context.JobItems.Add(i);
                        context.SaveChanges();

                        Guid oldSubID = Guid.Empty;
                        foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                        {
                            sub.ParentItemID = i.ID;
                            oldSubID = sub.ID;
                            sub.ID = Guid.NewGuid();

                            foreach (var img in images.Where(z => z.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }

                            context.JobItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.JobItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.JobID = newOrder.ID;
                }


                context.JobItemImages.AddRange(images);
                context.SaveChanges();

                foreach (var i in costItems)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        context.JobCostItems.Add(i);
                        context.SaveChanges();

                        foreach (var sub in costItems.Where(s => s.ParentCostItemID == oldID))
                        {
                            sub.ParentCostItemID = i.ID;
                            sub.ID = Guid.NewGuid();
                            context.JobCostItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentCostItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        i.ID = Guid.NewGuid();
                        context.JobCostItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.JobID = newOrder.ID;
                }
                context.SaveChanges();


                //Add Preview
                var previews = context.JobPreviews.Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(r => r.Job.Created);
                var pview = (previews.Count() > 0) ? previews.First() : new JobPreview();
                context.Entry(pview).State = EntityState.Detached;
                pview.CompanyID = CurrentUser.CompanyID;
                pview.JobID = newOrder.ID;
                context.JobPreviews.Add(pview);
                context.SaveChanges();

                new JobService<EmptyViewModel>(CurrentUser.CompanyID, null, null, new SnapDbContext()).RecalculateAllTotals(newOrder.ID);
                //JobsController.RecalulateTotals(newOrder.ID);

                //Log Activity
                var text = string.Format("{{{0}}} created from Quotation {1}{2} v{3} - '{4}' ", 0,
                                                                                            original.Reference ?? "",
                                                                                            original.Number,
                                                                                            original.Version,
                                                                                            (original.Client != null) ? original.Client.Name : "");

                //JobsController.SendNotifications(newOrder.ID, "New Job Created", string.Format(text, original.ToShortLabel()), context, CurrentUser, this.Server, this.Request);
                //JobsController.AddLog(newOrder.ID.ToString(), text, CurrentUser);

              
                return RedirectToAction("Edit", "Jobs", new { ID = newOrder.ID });
            }
        }
        */

        /*
        public ActionResult QuotationToInvoice(string ID, bool proforma = false)
        {
            using (var context = new SnapDbContext())
            {

                //Get Last Order number based on Settings
                var defaults = new InvoiceSettingsRepository().GetSettings(CurrentUser.CompanyID);
                int lastNum = defaults.StartingNumber - 1; //Get from defaults

                List<Invoice> last = context.Invoices.Where(q => q.CompanyID == CurrentUser.CompanyID && q.IsProForma == proforma).OrderByDescending(q => q.Number).Take(1).ToList();
                if (last.Count() > 0)
                {
                    if (lastNum < last[0].Number)
                        lastNum = last[0].Number;
                }

                //Get the original version; Include items, attachments; except for comments.
                var original = context.Quotations.Where(q => q.CompanyID == CurrentUser.CompanyID && q.ID.ToString() == ID)
                                                 .Include(q => q.Items)
                                                 .Include(q => q.Items.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.Sections)
                                                 .Include(q => q.Attachments)
                                                 .Include(q => q.Attachments.Select(x => x.File))
                                                 .Include(q => q.Notifications)
                                                 .First();

                //Can not convert is price breaks are enabled
                if (original.UsePriceBreaks) throw new Exception("Can not convert quotation if it uses price breaks");

                //Copy origignal
                var newOrder = new Invoice
                {
                    ClientID = original.ClientID,
                    ClientContactID = original.ClientContactID,
                    QuotationID = original.ID,
                    ClientReference = original.ClientReference,
                    Note = string.Format("Created from Quotation {0}{1}\n\n{2}", original.Reference ?? "", original.Number, original.Note),
                    PoNumber = original.PoNumber,
                    Description = original.Description,
                    JsonCustomFieldValues = original.JsonCustomFieldValues,
                    InvoiceAddressID = original.InvoiceAddressID,
                    DeliveryAddressID = original.DeliveryAddressID,

                    TotalVat = original.TotalVat,
                    TotalNet = original.TotalNet,
                    TotalPrice = original.TotalPrice,

                    IsProForma = proforma,
                    CheckSum = Guid.NewGuid().ToString("N"),
                    CompanyID = CurrentUser.CompanyID,
                    Reference = (proforma) ? defaults.DefaultProFormaReference : defaults.DefaultReference, //Get from defaults
                    Number = lastNum + 1,
                    StatusID = InvoiceStatusValues.DRAFT,
                    CurrencyID = original.CurrencyID,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    InvoiceDate = DateTime.Now,
                    PaymentDate = DateTime.Now.AddDays(defaults.DefaultPaymentDays),
                    AssignedUserID = CurrentUser.Id, //Assign to order the user who is logged in.,
                    CreatedBy = CurrentUser.Email,
                };


                context.Invoices.Add(newOrder);
                context.SaveChanges();


                //Copy Order Items (Do not copy comments)    
                var sections = original.Sections.Select(i => new InvoiceSection
                {
                    ID = i.ID,
                    InvoiceID = i.QuotationID,
                    Name = i.Name,
                    IncludeInTotal = i.IncludeInTotal,
                    SortPos = i.SortPos,
                    TotalNet = i.TotalNet,
                    TotalVat = i.TotalVat,
                    TotalPrice = i.TotalPrice,
                }).ToList();

                var sectionIds = sections.Select(i => i.ID).ToList();

                            
                var items = original.Items.Select(i => new InvoiceItem
                {
                    ID = i.ID,
                    InvoiceID = i.QuotationID,
                    SectionID = i.SectionID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    AccountCode = i.AccountCode,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    SetupCost = i.SetupCost,
                    VAT = i.VAT,
                    Price = i.Price,
                    Total = i.Total,

                    ImageURL = i.ImageURL,
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentItemID = i.ParentItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment

                }).ToList();


                var images = context.QuotationItemImages.Where(i => i.Owner.QuotationID == original.ID).Include(i => i.File)
                                    .ToList()
                                    .Select(i => new InvoiceItemImage { OwnerID = i.OwnerID, FileID = i.FileID, SortPos = i.SortPos })
                                    .ToList();


                var attachments = original.Attachments.Select(i => new InvoiceAttachment
                {
                    ID = i.ID,
                    InvoiceID = i.QuotationID,
                    Filename = i.Filename,
                    FileID = i.FileID,
                }).ToList();

                

                //When copying first detached. This insure copies are made instead of updating existing records
                foreach (var s in sections) { context.Entry(s).State = EntityState.Detached; s.InvoiceID = newOrder.ID; s.ID = Guid.NewGuid(); }
                foreach (var i in items)
                {
                    context.Entry(i).State = EntityState.Detached;
                    i.InvoiceID = newOrder.ID;
                    i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                    //New Guid assign below
                } 
                foreach (var i in items) { context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID; }
                foreach (var a in attachments) { context.Entry(a).State = EntityState.Detached; a.InvoiceID = newOrder.ID; a.ID = Guid.NewGuid(); }

                context.InvoiceSections.AddRange(sections);
                context.InvoiceAttachments.AddRange(attachments);
                context.InvoiceComments.Add(new InvoiceComment { InvoiceID = newOrder.ID, Timestamp = DateTime.Now, Email = CurrentUser.Email, Message = string.Format("Created from Quotation {0}{1}", original.Reference ?? "", original.Number) });

                //Add Default Notificatins
                foreach (var a in context.DefaultInvoiceNotifications.Where(a => a.InvoiceSettingsID == defaults.CompanyID && a.Email != CurrentUser.Email))
                {
                    context.InvoiceNotifications.Add(new InvoiceNotification
                    {
                        InvoiceID = newOrder.ID,
                        Email = a.Email
                    });
                }

                //Add the users email address.
                context.InvoiceNotifications.Add(new InvoiceNotification { InvoiceID = newOrder.ID, Email = CurrentUser.Email });

                context.SaveChanges();

                //Because items (kits) can have sub items. We have to add them with differently
                foreach (var i in items)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();

                        Guid oldSubID = Guid.Empty;
                        foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                        {
                            sub.ParentItemID = i.ID;
                            oldSubID = sub.ID;
                            sub.ID = Guid.NewGuid();
                            foreach (var img in images.Where(z => z.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }
                            context.InvoiceItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID;
                }

                context.InvoiceItemImages.AddRange(images);
                context.SaveChanges();


                //Add Preview
                var previews = context.InvoicePreviews.Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(r => r.Invoice.Created);
                var pview = (previews.Count() > 0) ? previews.First() : new InvoicePreview();
                context.Entry(pview).State = EntityState.Detached;
                pview.CompanyID = CurrentUser.CompanyID;
                pview.InvoiceID = newOrder.ID;
                context.InvoicePreviews.Add(pview);
                context.SaveChanges();

                //Log Activity
                var text = string.Format("{{{0}}} created from Quotation {1}{2} v{3} - '{4}' ", 0,
                                                                                            original.Reference ?? "",
                                                                                            original.Number,
                                                                                            original.Version,
                                                                                            (original.Client != null) ? original.Client.Name : "");

                //InvoicesController.SendNotifications(newOrder.ID, "New Invoice Created", string.Format(text, original.ToShortLabel()), context, CurrentUser, this.Server, this.Request);
                //InvoicesController.AddLog(newOrder.ID.ToString(), text, CurrentUser);

                return RedirectToAction("Edit", "Invoices", new { ID = newOrder.ID });
            }
        }
        */

        /*
        public ActionResult JobToInvoice(string ID, bool proforma = false)
        {
            using (var context = new SnapDbContext())
            {

                //Get Last Order number based on Settings
                var defaults = new InvoiceSettingsRepository().GetSettings(CurrentUser.CompanyID);
                int lastNum = defaults.StartingNumber - 1; //Get from defaults

                List<Invoice> last = context.Invoices.Where(q => q.CompanyID == CurrentUser.CompanyID && q.IsProForma == proforma).OrderByDescending(q => q.Number).Take(1).ToList();
                if (last.Count() > 0)
                {
                    if (lastNum < last[0].Number)
                        lastNum = last[0].Number;
                }

                //Get the original version; Include items, attachments; except for comments.
                var original = context.Jobs.Where(q => q.CompanyID == CurrentUser.CompanyID && q.ID.ToString() == ID)
                                                 .Include(q => q.Items)
                                                 .Include(q => q.Items.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.Sections)
                                                 .Include(q => q.Attachments)
                                                 .Include(q => q.Attachments.Select(x => x.File))
                                                 .Include(q => q.Notifications)
                                                 .First();

                //Copy origignal
                var newOrder = new Invoice
                {
                    ClientID = original.ClientID,
                    ClientContactID = original.ClientContactID,
                    JobID = original.ID,
                    QuotationID = original.QuotationID,
                    ClientReference = original.ClientReference,
                    Note = string.Format("Created from Job {0}{1}\n\n{2}", original.Reference ?? "", original.Number, original.Note),
                    PoNumber = original.PoNumber,
                    Description = original.Description,
                    JsonCustomFieldValues = original.JsonCustomFieldValues,
                    InvoiceAddressID = original.InvoiceAddressID,
                    DeliveryAddressID = original.DeliveryAddressID,

                    TotalVat = original.TotalVat,
                    TotalNet = original.TotalNet,
                    TotalPrice = original.TotalPrice,

                    IsProForma = proforma,
                    CheckSum = Guid.NewGuid().ToString("N"),
                    CompanyID = CurrentUser.CompanyID,
                    Reference = (proforma) ? defaults.DefaultProFormaReference : defaults.DefaultReference, //Get from defaults
                    Number = lastNum + 1,
                    StatusID = InvoiceStatusValues.DRAFT,
                    CurrencyID = original.CurrencyID,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    InvoiceDate = DateTime.Now,
                    PaymentDate = DateTime.Now.AddDays(defaults.DefaultPaymentDays),
                    AssignedUserID = CurrentUser.Id, //Assign to order the user who is logged in.
                    CreatedBy = CurrentUser.Email,
                };


                context.Invoices.Add(newOrder);
                context.SaveChanges();

                //Copy Order Items (Do not copy comments)
                var sections = original.Sections.Select(i => new InvoiceSection
                {
                    ID = i.ID,
                    InvoiceID = i.JobID,
                    Name = i.Name,
                    IncludeInTotal = i.IncludeInTotal,
                    SortPos = i.SortPos,
                    TotalNet = i.TotalNet,
                    TotalVat = i.TotalVat,
                    TotalPrice = i.TotalPrice,
                }).ToList();

                var sectionIds = sections.Select(i => i.ID).ToList();

                var items = original.Items.Select(i => new InvoiceItem
                {
                    ID = i.ID,
                    InvoiceID = i.JobID,
                    SectionID = i.SectionID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    AccountCode = i.AccountCode,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    SetupCost = i.SetupCost,
                    VAT = i.VAT,
                    Price = i.Price,
                    Total = i.Total,

                    ImageURL = i.ImageURL,
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentItemID = i.ParentItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment

                }).ToList();

                var images = context.JobItemImages.Where(i => i.Owner.JobID == original.ID).Include(i => i.File)
                                    .ToList()
                                    .Select(i => new InvoiceItemImage { OwnerID = i.OwnerID, FileID = i.FileID, SortPos = i.SortPos })
                                    .ToList();

                var attachments = original.Attachments.Select(i => new InvoiceAttachment
                {
                    ID = i.ID,
                    InvoiceID = i.JobID,
                    Filename = i.Filename,
                    FileID = i.FileID,
                }).ToList();
                


                //When copying first detached. This insure copies are made instead of updating existing records
                foreach (var s in sections) { context.Entry(s).State = EntityState.Detached; s.InvoiceID = newOrder.ID; s.ID = Guid.NewGuid(); }
                foreach (var i in items)
                {
                    context.Entry(i).State = EntityState.Detached;
                    i.InvoiceID = newOrder.ID;
                    i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                    //New Guid assign below
                } 
                foreach (var i in items) { context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID; }
                foreach (var a in attachments) { context.Entry(a).State = EntityState.Detached; a.InvoiceID = newOrder.ID; a.ID = Guid.NewGuid(); }
                
                context.InvoiceSections.AddRange(sections);
                context.InvoiceAttachments.AddRange(attachments);
                context.InvoiceComments.Add(new InvoiceComment { InvoiceID = newOrder.ID, Timestamp = DateTime.Now, Email = CurrentUser.Email, Message = string.Format("Created from Job {0}{1}", original.Reference ?? "", original.Number) });

                //Add Default Notificatins
                foreach (var a in context.DefaultInvoiceNotifications.Where(a => a.InvoiceSettingsID == defaults.CompanyID && a.Email != CurrentUser.Email))
                {
                    context.InvoiceNotifications.Add(new InvoiceNotification
                    {
                        InvoiceID = newOrder.ID,
                        Email = a.Email
                    });
                }

                //Add the users email address.
                context.InvoiceNotifications.Add(new InvoiceNotification { InvoiceID = newOrder.ID, Email = CurrentUser.Email });

                context.SaveChanges();

                //Because items (kits) can have sub items. We have to add them with differently
                foreach (var i in items)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();

                        Guid oldSubID = Guid.Empty;
                        foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                        {
                            sub.ParentItemID = i.ID;
                            oldSubID = sub.ID;
                            sub.ID = Guid.NewGuid();
                            foreach (var img in images.Where(z => z.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }
                            context.InvoiceItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID;
                }

                context.InvoiceItemImages.AddRange(images);
                context.SaveChanges();


                //Add Preview
                var previews = context.InvoicePreviews.Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(r => r.Invoice.Created);
                var pview = (previews.Count() > 0) ? previews.First() : new InvoicePreview();
                context.Entry(pview).State = EntityState.Detached;
                pview.CompanyID = CurrentUser.CompanyID;
                pview.InvoiceID = newOrder.ID;
                context.InvoicePreviews.Add(pview);
                context.SaveChanges();

                //Log Activity
                var text = string.Format("{{{0}}} created from Job {1}{2} - '{3}' ", 0,
                                                                                            original.Reference ?? "",
                                                                                            original.Number,
                                                                                            (original.Client != null) ? original.Client.Name : "");

                //InvoicesController.SendNotifications(newOrder.ID, "New Invoice Created", string.Format(text, original.ToShortLabel()), context, CurrentUser, this.Server, this.Request);
                //InvoicesController.AddLog(newOrder.ID.ToString(), text, CurrentUser);

                return RedirectToAction("Edit", "Invoices", new { ID = newOrder.ID });
            }
        }
        */

        /*
        public ActionResult JobToDeliveryNote(string ID)
        {
            using (var context = new SnapDbContext())
            {

                //Get Last Order number based on Settings
                var defaults = new DeliveryNoteSettingsRepository().GetSettings(CurrentUser.CompanyID);
                int lastNum = defaults.StartingNumber - 1; //Get from defaults

                List<DeliveryNote> last = context.DeliveryNotes.Where(q => q.CompanyID == CurrentUser.CompanyID).OrderByDescending(q => q.Number).Take(1).ToList();
                if (last.Count() > 0)
                {
                    if (lastNum < last[0].Number)
                        lastNum = last[0].Number;
                }

                //Get the original version; Include items, attachments; except for comments.
                var original = context.Jobs.Where(q => q.CompanyID == CurrentUser.CompanyID && q.ID.ToString() == ID)
                                                 .Include(q => q.Items)
                                                 .Include(q => q.Items.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.Sections)
                                                 .Include(q => q.Attachments)
                                                 .Include(q => q.Attachments.Select(x => x.File))
                                                 .Include(q => q.Notifications)
                                                 .First();

                //Copy origignal
                var newOrder = new DeliveryNote
                {

                    ClientID = original.ClientID,
                    ClientContactID = original.ClientContactID,
                    JobID = original.ID,
                    ClientReference = original.ClientReference,
                    Note = string.Format("Created from Job {0}{1}\n\n{2}", original.Reference ?? "", original.Number, original.Note),
                    PoNumber = original.PoNumber,
                    Description = original.Description,
                    JsonCustomFieldValues = original.JsonCustomFieldValues,
                    DeliveryAddressID = original.DeliveryAddressID,
                    EstimatedDelivery = original.EstimatedDelivery,

                    CheckSum = Guid.NewGuid().ToString("N"),
                    CompanyID = CurrentUser.CompanyID,
                    Reference = defaults.DefaultReference, //Get from defaults
                    Number = lastNum + 1,
                    StatusID = DeliveryNoteStatusValues.DRAFT,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    AssignedUserID = CurrentUser.Id, //Assign to order the user who is logged in.
                    CreatedBy = CurrentUser.Email,
                };


                context.DeliveryNotes.Add(newOrder);
                context.SaveChanges();

                //Copy Order Items (Do not copy comments)
                var sections = original.Sections.Select(i => new DeliveryNoteSection
                {
                    ID = i.ID,
                    DeliveryNoteID = i.JobID,
                    Name = i.Name,
                    IncludeInTotal = i.IncludeInTotal,
                    SortPos = i.SortPos,
                    //TotalNet = i.TotalNet,
                    //TotalVat = i.TotalVat,
                    //TotalPrice = i.TotalPrice,
                }).ToList();

                var sectionIds = sections.Select(i => i.ID).ToList();


                var items = original.Items.Select(i => new DeliveryNoteItem
                {
                    ID = i.ID,
                    DeliveryNoteID = i.JobID,
                    JobItemID = i.ID, //Add link to job item
                    SectionID = i.SectionID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    AccountCode = i.AccountCode,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    ImageURL = i.ImageURL,
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentItemID = i.ParentItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment

                }).ToList();

                var images = context.JobItemImages.Where(i => i.Owner.JobID == original.ID).Include(i => i.File)
                                    .ToList()
                                    .Select(i => new DeliveryNoteItemImage { OwnerID = i.OwnerID, FileID = i.FileID, SortPos = i.SortPos })
                                    .ToList();

                var attachments = original.Attachments.Select(i => new DeliveryNoteAttachment
                {
                    ID = i.ID,
                    DeliveryNoteID = i.JobID,
                    Filename = i.Filename,
                    FileID = i.FileID,
                }).ToList();

               

                //When copying first detached. This insure copies are made instead of updating existing records
                foreach (var s in sections) { context.Entry(s).State = EntityState.Detached; s.DeliveryNoteID = newOrder.ID; s.ID = Guid.NewGuid(); }
                foreach (var i in items)
                {
                    context.Entry(i).State = EntityState.Detached;
                    i.DeliveryNoteID = newOrder.ID;
                    i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                    //New Guid assign below
                } 
                foreach (var i in items) { context.Entry(i).State = EntityState.Detached; i.DeliveryNoteID = newOrder.ID; } //Assign new ID below
                foreach (var a in attachments) { context.Entry(a).State = EntityState.Detached; a.DeliveryNoteID = newOrder.ID; a.ID = Guid.NewGuid(); }
                
                context.DeliveryNoteSections.AddRange(sections);
                context.DeliveryNoteAttachments.AddRange(attachments);
                context.DeliveryNoteComments.Add(new DeliveryNoteComment { DeliveryNoteID = newOrder.ID, Timestamp = DateTime.Now, Email = CurrentUser.Email, Message = string.Format("Created from Job {0}{1}", original.Reference ?? "", original.Number) });

                //Add Default Notificatins
                foreach (var a in context.DefaultDeliveryNoteNotifications.Where(a => a.DeliveryNoteSettingsID == defaults.CompanyID && a.Email != CurrentUser.Email))
                {
                    context.DeliveryNoteNotifications.Add(new DeliveryNoteNotification
                    {
                        DeliveryNoteID = newOrder.ID,
                        Email = a.Email
                    });
                }

                //Add the users email address.
                context.DeliveryNoteNotifications.Add(new DeliveryNoteNotification { DeliveryNoteID = newOrder.ID, Email = CurrentUser.Email });

                context.SaveChanges();

                //Because items (kits) can have sub items. We have to add them with differently
                foreach (var i in items)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.DeliveryNoteItems.Add(i);
                        context.SaveChanges();

                        Guid oldSubID = Guid.Empty;
                        foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                        {
                            sub.ParentItemID = i.ID;
                            sub.ID = Guid.NewGuid();
                            context.DeliveryNoteItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.DeliveryNoteItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.DeliveryNoteID = newOrder.ID;
                }

                context.DeliveryNoteItemImages.AddRange(images);
                context.SaveChanges();


                //Add Preview
                var previews = context.DeliveryNotePreviews.Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(r => r.DeliveryNote.Created);
                var pview = (previews.Count() > 0) ? previews.First() : new DeliveryNotePreview();
                context.Entry(pview).State = EntityState.Detached;
                pview.CompanyID = CurrentUser.CompanyID;
                pview.DeliveryNoteID = newOrder.ID;
                context.DeliveryNotePreviews.Add(pview);
                context.SaveChanges();

                //Log Activity
                var text = string.Format("{{{0}}} created from Job {1}{2} - '{3}' ", 0,
                                                                                            original.Reference ?? "",
                                                                                            original.Number,
                                                                                            (original.Client != null) ? original.Client.Name : "");

                //DeliveryNotesController.SendNotifications(newOrder.ID, "New Job Created", string.Format(text, original.ToShortLabel()), context, CurrentUser, this.Server, this.Request);
                //DeliveryNotesController.AddLog(newOrder.ID.ToString(), text, CurrentUser);

                return RedirectToAction("Edit", "DeliveryNotes", new { ID = newOrder.ID });
            }
        }
        */


        /*
        public ActionResult DeliveryNoteToInvoice(string ID, bool proforma = false)
        {
            using (var context = new SnapDbContext())
            {

                //Get Last Order number based on Settings
                var defaults = new InvoiceSettingsRepository().GetSettings(CurrentUser.CompanyID);
                int lastNum = defaults.StartingNumber - 1; //Get from defaults

                List<Invoice> last = context.Invoices.Where(q => q.CompanyID == CurrentUser.CompanyID && q.IsProForma == proforma).OrderByDescending(q => q.Number).Take(1).ToList();
                if (last.Count() > 0)
                {
                    if (lastNum < last[0].Number)
                        lastNum = last[0].Number;
                }

                //Get the original version; Include items, attachments; except for comments.
                var original = context.DeliveryNotes.Where(q => q.CompanyID == CurrentUser.CompanyID && q.ID.ToString() == ID)
                                                 .Include(q => q.Items)
                                                 .Include(q => q.Items.Select(x => x.CustomFieldValues))
                                                 .Include(q => q.Sections)
                                                 .Include(q => q.Items.Select(x => x.JobItem))
                                                 .Include(q => q.Attachments)
                                                 .Include(q => q.Attachments.Select(x => x.File))
                                                 .Include(q => q.Notifications)
                                                 .Include(q => q.Job)
                                                 .First();

                //Copy origignal
                var newOrder = new Invoice
                {

                    ClientID = original.ClientID,
                    ClientContactID = original.ClientContactID,
                    JobID = original.JobID,
                    QuotationID = (original.Job != null) ? original.Job.QuotationID : null,
                    DeliveryNoteID = original.ID,
                    ClientReference = original.ClientReference,
                    Note = string.Format("Created from Delivery Note {0}{1}\n\n{2}", original.Reference ?? "", original.Number, original.Note),
                    PoNumber = original.PoNumber,
                    Description = original.Description,
                    JsonCustomFieldValues = original.JsonCustomFieldValues,
                    InvoiceAddressID = (original.Job != null) ? original.Job.InvoiceAddressID : null,
                    DeliveryAddressID = original.DeliveryAddressID,
                    
                    //TotalVat = original.TotalVat,
                    //TotalNet = original.TotalNet,
                    //TotalPrice = original.TotalPrice,
                    //TotalVatServices = original.TotalVatServices,
                    //TotalNetServices = original.TotalNetServices,
                    //TotalServices = original.TotalServices,
                    IsProForma = proforma,
                    CheckSum = Guid.NewGuid().ToString("N"),
                    CompanyID = CurrentUser.CompanyID,
                    Reference = (proforma) ? defaults.DefaultProFormaReference : defaults.DefaultReference, //Get from defaults
                    Number = lastNum + 1,
                    StatusID = InvoiceStatusValues.DRAFT,
                    CurrencyID = (original.Job != null) ? original.Job.CurrencyID : CurrentUser.Company.DefaultCurrencyID,
                    Created = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    InvoiceDate = DateTime.Now,
                    PaymentDate = DateTime.Now.AddDays(defaults.DefaultPaymentDays),
                    AssignedUserID = CurrentUser.Id, //Assign to order the user who is logged in.
                    CreatedBy = CurrentUser.Email,
                };


                context.Invoices.Add(newOrder);
                context.SaveChanges();

                //Copy Order Items (Do not copy comments)
                var sections = original.Sections.Select(i => new InvoiceSection
                {
                    ID = i.ID,
                    InvoiceID = i.DeliveryNoteID,
                    Name = i.Name,
                    IncludeInTotal = i.IncludeInTotal,
                    SortPos = i.SortPos,
                    //TotalNet = i.TotalNet,
                    //TotalVat = i.TotalVat,
                    //TotalPrice = i.TotalPrice,
                }).ToList();

                var sectionIds = sections.Select(i => i.ID).ToList();


                var items = original.Items.Select(i => new InvoiceItem
                {
                    ID = i.ID,
                    InvoiceID = i.DeliveryNoteID,
                    SectionID = i.SectionID,
                    //ProductID = i.ProductID,
                    ProductCode = i.ProductCode,
                    Description = i.Description,
                    Category = i.Category,
                    VendorCode = i.VendorCode,
                    Manufacturer = i.Manufacturer,
                    AccountCode = i.AccountCode,
                    JsonCustomFieldValues = i.JsonCustomFieldValues,
                    Unit = i.Unit,
                    Quantity = i.Quantity,

                    SetupCost = (i.JobItem != null) ? i.JobItem.SetupCost : 0,
                    VAT = (i.JobItem != null) ? i.JobItem.VAT : 0,
                    Price = (i.JobItem != null) ? i.JobItem.Price : 0,
                    Total = (i.JobItem != null) ? i.JobItem.Total : 0,

                    ImageURL = i.ImageURL,
                    IsKit = i.IsKit,
                    UseKitPrice = i.UseKitPrice,
                    ParentItemID = i.ParentItemID,
                    SortPos = i.SortPos,
                    IsComment = i.IsComment

                }).ToList();


                var images = context.DeliveryNoteItemImages.Where(i => i.Owner.DeliveryNoteID == original.ID).Include(i => i.File)
                                    .ToList()
                                    .Select(i => new InvoiceItemImage { OwnerID = i.OwnerID, FileID = i.FileID, SortPos = i.SortPos })
                                    .ToList();

                var attachments = original.Attachments.Select(i => new InvoiceAttachment
                {
                    ID = i.ID,
                    InvoiceID = i.DeliveryNoteID,
                    Filename = i.Filename,
                    FileID = i.FileID,
                }).ToList();

               

                //When copying first detached. This insure copies are made instead of updating existing records
                foreach (var s in sections) { context.Entry(s).State = EntityState.Detached; s.InvoiceID = newOrder.ID; s.ID = Guid.NewGuid(); }
                foreach (var i in items)
                {
                    context.Entry(i).State = EntityState.Detached;
                    i.InvoiceID = newOrder.ID;
                    i.SectionID = sections.ElementAt(sectionIds.IndexOf(i.SectionID)).ID;
                    //New Guid assign below
                } 
                foreach (var i in items) { context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID; }
                foreach (var a in attachments) { context.Entry(a).State = EntityState.Detached; a.InvoiceID = newOrder.ID; a.ID = Guid.NewGuid(); }
                
                context.InvoiceSections.AddRange(sections);
                context.InvoiceAttachments.AddRange(attachments);
                context.InvoiceComments.Add(new InvoiceComment { InvoiceID = newOrder.ID, Timestamp = DateTime.Now, Email = CurrentUser.Email, Message = string.Format("Created from Delivery Note {0}{1}", original.Reference ?? "", original.Number) });

                //Add Default Notificatins
                foreach (var a in context.DefaultInvoiceNotifications.Where(a => a.InvoiceSettingsID == defaults.CompanyID && a.Email != CurrentUser.Email))
                {
                    context.InvoiceNotifications.Add(new InvoiceNotification
                    {
                        InvoiceID = newOrder.ID,
                        Email = a.Email
                    });
                }

                //Add the users email address.
                context.InvoiceNotifications.Add(new InvoiceNotification { InvoiceID = newOrder.ID, Email = CurrentUser.Email });

                context.SaveChanges();

                //Because items (kits) can have sub items. We have to add them with differently
                foreach (var i in items)
                {
                    if (i.IsKit)
                    {
                        //If the item is a kit then:
                        //1. Get old ID
                        //2. Add item then save to get new ID
                        //3. Get sub items, update parent ID then add them too
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();

                        Guid oldSubID = Guid.Empty;
                        foreach (var sub in items.Where(s => s.ParentItemID == oldID))
                        {
                            sub.ParentItemID = i.ID;
                            oldSubID = sub.ID;
                            sub.ID = Guid.NewGuid();
                            foreach (var img in images.Where(z => z.OwnerID == oldSubID)) { img.OwnerID = sub.ID; }
                            context.InvoiceItems.Add(sub);
                        }
                        context.SaveChanges();
                    }
                    else if (i.ParentItemID != null)
                    {
                        //Do nothing. Because they should added with the above code.
                    }
                    else
                    {
                        //Normal non-kit itme are added directly.
                        Guid oldID = i.ID;
                        i.ID = Guid.NewGuid();
                        foreach (var img in images.Where(p => p.OwnerID == oldID)) { img.OwnerID = i.ID; }
                        context.InvoiceItems.Add(i);
                        context.SaveChanges();
                    }

                    context.Entry(i).State = EntityState.Detached; i.InvoiceID = newOrder.ID;
                }


                context.InvoiceItemImages.AddRange(images);
                context.SaveChanges();
                new InvoiceService<EmptyViewModel>(CurrentUser.CompanyID, null, null, new SnapDbContext()).RecalculateAllTotals(newOrder.ID);
                //InvoicesController.RecalulateTotals(newOrder.ID);


                original.StatusID = DeliveryNoteStatusValues.INVOICED;
                context.DeliveryNotes.Attach(original);
                context.Entry(original).Property(q => q.StatusID).IsModified = true;
                context.SaveChanges();


                //Add Preview
                var previews = context.InvoicePreviews.Where(s => s.CompanyID == CurrentUser.CompanyID).OrderByDescending(r => r.Invoice.Created);
                var pview = (previews.Count() > 0) ? previews.First() : new InvoicePreview();
                context.Entry(pview).State = EntityState.Detached;
                pview.CompanyID = CurrentUser.CompanyID;
                pview.InvoiceID = newOrder.ID;
                context.InvoicePreviews.Add(pview);
                context.SaveChanges();

                //Log Activity
                var text = string.Format("{{{0}}} created from Delivery Note {1}{2} - '{3}' ", 0,
                                                                                            original.Reference ?? "",
                                                                                            original.Number,
                                                                                            (original.Client != null) ? original.Client.Name : "");
                //InvoicesController.AddLog(newOrder.ID.ToString(), text, CurrentUser);

                return RedirectToAction("Edit", "Invoices", new { ID = newOrder.ID });
            }            
        }
        */
    }
}