﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class CompanyAddressesController : ServiceGridController<CompanyAddress, CompanyAddressViewModel, CompanyAddressService<CompanyAddressViewModel>>
    {

        public CompanyAddressesController()
            : base(new CompanyAddressService<CompanyAddressViewModel>(Guid.Empty, null, new CompanyAddressMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid companyID)
        {
            var list = _service.Read(companyID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _service.Read(CurrentUser.CompanyID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }


        public JsonResult DropDownCompanyAddresses()
        {
            var addresses = _service.Read(CurrentUser.CompanyID)
                .OrderBy(t => t.Name).ToArray()
                .Select(a => new SelectItemViewModel
                {
                    ID = a.ID.ToString(),
                    Name = a.ToShortLabel()
                }
            ).ToList();


            addresses.Insert(0, new SelectItemViewModel
            {
                ID = null,
                Name = string.Join(", ", new string[] { CurrentUser.Company.Name, CurrentUser.Company.Address1, CurrentUser.Company.Address2, CurrentUser.Company.Town, CurrentUser.Company.Postcode }.Where(i => !string.IsNullOrEmpty(i)))
            });
                
            return Json(addresses, JsonRequestBehavior.AllowGet);
        }

    }

}