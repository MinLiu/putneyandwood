﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Super User,Owner,Admin")]
    public class SettingsController : ServiceEntityController<Company, CompanyViewModel, CompanyService<CompanyViewModel>>
    {
        public SettingsController()
            : base(new CompanyService<CompanyViewModel>(null, new CompanyMapper(), new SnapDbContext()))
        {

        }

        // GET: Settings
        public ActionResult Index()
        {
            return RedirectToAction("Business", "Settings");
        }

        public ActionResult Business()
        {
            var model = _service.FindByID(CurrentUser.CompanyID);
            var viewModel = _service.MapToViewModel(model);
            return View(viewModel);
        }

        public ActionResult Theme()
        {
            return RedirectToAction("Business", "Settings");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Business(CompanyViewModel viewModel, HttpPostedFileBase logoImage, string remove)
        {            
            if (viewModel == null) return RedirectToAction("Business", "Settings");

            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (!string.IsNullOrEmpty(remove))
            {
                try
                {
                    _service.RemoveLogoImage(CurrentUser.CompanyID);
                    viewModel.LogoImageURL = null;
                }
                catch (Exception ex) { ViewBag.ErrorMessage = ex.Message; return View(viewModel); }
            }
            //Save Logo if avaliable
            else if (logoImage != null)
            {
                try
                {
                    var m = _service.UploadLogoImage(CurrentUser.CompanyID, logoImage);
                    viewModel.LogoImageURL = m.LogoImageURL;
                }
                catch (Exception ex) { ViewBag.ErrorMessage = ex.Message; return View(viewModel); }
            }


            var model = _service.MapToModel(viewModel);            
            _service.Update(model);
            SetCustomTheme(model);
            Session["CurrentUser"] = null;
            ViewBag.SuccessMessage = "Business Details Saved.";
            return View(viewModel);
           
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Theme(HttpPostedFileBase screenLogoImage, string removeScreenLogo, string themeColor, string restColor)
        {
            var model = _service.FindByID(CurrentUser.CompanyID);

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Business", "Settings");
            }
            
            if (!string.IsNullOrEmpty(removeScreenLogo))
            {
                model = _service.RemoveScreenLogoImage(model.ID);
            }
            //Save Logo if avaliable
            else if (screenLogoImage != null)
            {
                model = _service.UploadScreenLogoImage(model.ID, screenLogoImage);
            }

            if (!string.IsNullOrEmpty(restColor))
            {
                model = _service.ResetThemeColour(model.ID);
            }
            else
            {
                model = _service.SetThemeColour(model.ID, themeColor);
            }
                
            SetCustomTheme(model);
            Session["CurrentUser"] = null;
            ViewBag.SuccessMessage = "Business Theme Saved.";
            return View("Business", _service.MapToViewModel(model));

        }

    }
}