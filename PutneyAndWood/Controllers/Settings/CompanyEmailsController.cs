﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace SnapSuite.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class CompanyEmailsController : ServiceGridController<CompanyEmail, CompanyEmailViewModel, CompanyEmailService<CompanyEmailViewModel>>
    {

        public CompanyEmailsController()
            : base(new CompanyEmailService<CompanyEmailViewModel>(Guid.Empty, null, new CompanyEmailMapper(), new SnapDbContext()))
        {

        }
        
        public ActionResult Index()
        {
            ViewBag.CompanyID = CurrentUser.CompanyID.ToString();
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText)
        {
            var list = _service.Read().Where(i => i.Email.ToLower().Contains(filterText.ToLower()) || string.IsNullOrEmpty(filterText))
                .OrderBy(p => p.Default == false)
                .ThenBy(i => i.Email)
                .ToList().Select(p => _service.MapToViewModel(p));

            return Json(list.ToDataSourceResult(request));
        }
        

        [HttpPost]
        public ActionResult _TestEmail(Guid ID)
        {
            return Json(_service.TestEmail(ID)); 
        }
        

    }

}