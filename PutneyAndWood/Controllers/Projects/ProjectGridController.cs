﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Kendo.Mvc;
using System.ComponentModel;

namespace SnapSuite.Controllers
{


    [Authorize]
    public class ProjectGridController: BaseController
    {
        public ProjectService _service { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _service = new ProjectService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
        }

        public void SaveCookies(string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD, int page, int pageSize, string sort, string sortDirection)
        {
            //Save Search Settings in Cookies
            HttpCookie cookie = Request.Cookies["ProjectSearch"] ?? new HttpCookie("ProjectSearch");
            cookie["Filter"] = filterText;
            cookie["AssignedUser"] = assignedUser;
            cookie["Statuses"] = statuses;
            cookie["DateType"] = dateType.ToString();
            cookie["FromD"] = fromD;
            cookie["ToD"] = toD;
            cookie["Page"] = page.ToString();
            cookie["PageSize"] = pageSize.ToString();
            cookie["Sort"] = sort;
            cookie["SortDirection"] = sortDirection;

            cookie.Expires = DateTime.Now.AddDays(1d);
            Response.Cookies.Add(cookie);
        }


        // READ: GridProject
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD)
        {
            if (request.Sorts.Count() <= 0) SaveCookies(filterText, assignedUser, statuses, dateType, fromD, toD, request.Page, request.PageSize, "", "");
            else SaveCookies(filterText, assignedUser, statuses, dateType, fromD, toD, request.Page, request.PageSize, request.Sorts[0].Member, (request.Sorts[0].SortDirection == ListSortDirection.Ascending) ? "asc" : "desc");

            var orders = _service.Search(filterText, (DateType)dateType, !string.IsNullOrEmpty(statuses) ? statuses.Split(',').Select(i => int.Parse(i)).ToList() : new List<int>(), fromD, toD, assignedUser).OrderByDescending(x => x.Created);
            
            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                switch (request.Sorts[x].Member)
                {
                    case "Status.ID":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.StatusID);
                        else orders = orders.OrderByDescending(p => p.StatusID);
                        break;
                    case "Name":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.Name);
                        else orders = orders.OrderByDescending(p => p.Name);
                        break;
                    case "StoneworkStartDate":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.StoneworkStartDate);
                        else orders = orders.OrderByDescending(p => p.StoneworkStartDate);
                        break;
                    case "Value":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.Value);
                        else orders = orders.OrderByDescending(p => p.Value);
                        break;
                    case "AssignedUserName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        else orders = orders.OrderByDescending(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        break;
                    case "Created":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) orders = orders.OrderBy(p => p.Created);
                        else orders = orders.OrderByDescending(p => p.Created);
                        break;
                    default:
                        orders = orders.OrderByDescending(p => p.Created);
                        break;
                }
            }

            //orders = ApplyQueryFiltersSort(request, orders, "Created DESC");

            var list = orders.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => new ProjectGridMapper().MapToViewModel(q)).ToList();
            DataSourceResult result = new DataSourceResult { Data = list, Total = orders.Count() };
            return Json(result);
        }

        public ActionResult ReadContactAssociate([DataSourceRequest]DataSourceRequest request, Guid ContactID)
        {
            var projects = _service.ReadContactAssociated(ContactID);

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                switch (request.Sorts[x].Member)
                {
                    case "Status.ID":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.StatusID);
                        else projects = projects.OrderByDescending(p => p.StatusID);
                        break;
                    case "Name":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.Name);
                        else projects = projects.OrderByDescending(p => p.Name);
                        break;
                    case "StoneworkStartDate":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.StoneworkStartDate);
                        else projects = projects.OrderByDescending(p => p.StoneworkStartDate);
                        break;
                    case "Value":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.Value);
                        else projects = projects.OrderByDescending(p => p.Value);
                        break;
                    case "AssignedUserName":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        else projects = projects.OrderByDescending(p => p.AssignedUser.FirstName + p.AssignedUser.LastName);
                        break;
                    case "Created":
                        if (request.Sorts[x].SortDirection == ListSortDirection.Ascending) projects = projects.OrderBy(p => p.Created);
                        else projects = projects.OrderByDescending(p => p.Created);
                        break;
                    default:
                        projects = projects.OrderByDescending(p => p.Created);
                        break;
                }
            }

            var list = projects.Skip(request.PageSize * (request.Page - 1))
                                  .Take(request.PageSize)
                                  .ToList()
                                  .Select(q => new ProjectGridMapper().MapToViewModel(q))
                                  .ToList();

            DataSourceResult result = new DataSourceResult { Data = list, Total = projects.Count() };
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, ProjectViewModel viewModel)
        {
            var model = _service.Delete(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public FileResult Export(string filterText, string assignedUser, string statuses, int dateType, string fromD, string toD)
        {

            var export = _service.Export(filterText, (DateType)dateType, statuses.Split(',').Select(i => int.Parse(i)).ToList(), fromD, toD, assignedUser);
            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user

        }

    }
}