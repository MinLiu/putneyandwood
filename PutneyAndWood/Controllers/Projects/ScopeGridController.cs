﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Kendo.Mvc;
using System.ComponentModel;

namespace SnapSuite.Controllers
{


    [Authorize]
    public class ScopeGridController : BaseController
    {
        public ScopeService _service { get; set; }
        public ScopeGridMapper _mapper { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _service = new ScopeService(new SnapDbContext());
            _mapper = new ScopeGridMapper();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(ScopeGridViewModel viewModel)
        {
            var model = _mapper.MapToModel(viewModel);
            _service.Create(model);
            viewModel.ID = model.ID.ToString();
            return Json(viewModel);
        }

        // READ: GridProject
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var scopes = _service.Read();
            var list = scopes.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(q => _mapper.MapToViewModel(q)).ToList();
            DataSourceResult result = new DataSourceResult { Data = list, Total = scopes.Count() };
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(ScopeGridViewModel viewModel)
        {
            var model = _service.Find(int.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            _service.Update(model);
            return Json(viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest]DataSourceRequest request, ScopeGridViewModel viewModel)
        {
            var model = _service.Delete(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult DropDownScopes()
        {
            var results = _service.Read().Select(x => new DropDownListItem { Text = x.Name, Value = x.ID.ToString() });
            return Json(results.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}