﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace SnapSuite.Controllers
{
       
    [Authorize]
    public class ProjectsController : BaseController
    {

        public ProjectService _service { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewQuotations == false)
                Response.Redirect("/");

            _service = new ProjectService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
        }

        #region Main Quotations Index

        #region Cookie And Status Stuff

        public void SetViewBags(){

            var statService = new ProjectStatusService(_service.DbContext);
            var stats = statService.Read().ToArray().Select(s => ProjectStatusMapper.MapToViewModel(s)).ToList();
            ViewBag.StatusOptions =  stats;

            HttpCookie cookie = Request.Cookies["ProjectSearch"];
            try 
            {
                if (cookie["Filter"] == null
                    || cookie["AssignedUser"] == null
                    || cookie["Statuses"] == null
                    || string.IsNullOrEmpty(cookie["DateType"])
                    || string.IsNullOrEmpty(cookie["FromD"])
                    || string.IsNullOrEmpty(cookie["ToD"])
                    || string.IsNullOrEmpty(cookie["Page"])
                    || string.IsNullOrEmpty(cookie["PageSize"])
                    || cookie["Sort"] == null
                    || cookie["SortDirection"] == null
                    )
                    throw new Exception();

                ViewBag.filterText = cookie["Filter"];
                ViewBag.assignedUser = cookie["AssignedUser"];
                ViewBag.statuses = cookie["Statuses"];
                ViewBag.dateType = cookie["DateType"];
                ViewBag.fromD = cookie["FromD"];
                ViewBag.toD = cookie["ToD"];
                ViewBag.page = cookie["Page"];
                ViewBag.pageSize = cookie["PageSize"];
                ViewBag.sort = cookie["Sort"];
                ViewBag.sortDirection = cookie["SortDirection"];
            }
            catch
            {
                ViewBag.filterText = "";
                ViewBag.assignedUser = "0";
                ViewBag.statuses = "";
                ViewBag.dateType = "0";
                ViewBag.fromD = DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.toD = DateTime.Now.ToString("dd/MM/yyyy");
                ViewBag.page = "1";
                ViewBag.pageSize = "50";
                ViewBag.sort = "Created";
                ViewBag.sortDirection = "desc";
            }
            

        }

        #endregion


        // GET: Quotations
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult Index()
        {           
            SetViewBags();
            return View();
        }

        //public ActionResult Export()
        //{
        //    return View();
        //}

        //public ActionResult Deleted()
        //{
        //    SetViewBags();
        //    return View();
        //}


        #endregion

        public ActionResult New()
        {

            var model = _service.New(CurrentUser.Id);
            return RedirectToAction("Edit", new { ID = model.ID });

        }

        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult Edit(Guid id)
        {
            var project = new ProjectMapper().MapToViewModel(_service.FindByID(id));
            return View(project);
        }

        public ActionResult _Header(Guid projectID)
        {
            SetViewBags();

            var model = _service.FindByID(projectID);
            var viewModel = new ProjectMapper().MapToViewModel(model);
            return PartialView(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Header(ProjectViewModel viewModel)
        {
            var mapper = new ProjectMapper();
            if (viewModel == null) return RedirectToAction("Index");

            SetViewBags();

            var model = _service.FindByID(Guid.Parse(viewModel.ID));

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                viewModel = mapper.MapToViewModel(model);
                return PartialView(viewModel);
            }

            _service.Update(viewModel);

            ViewBag.Saved = true;
            return JavaScript("ShowSavedMessage();");
        }


        public ActionResult ChangeStatus(Guid projectID, int status)
        {
            _service.ChangeStatus(projectID, status);

            //Refresh whole page after status change.
            return RedirectToAction("Edit", new { id = projectID });
        }

        public FileResult ExportItems(Guid projectID)
        {
            var export = _service.Export(projectID);

            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user              
        }

        [HttpGet]
        public ActionResult _NewEvent(Guid projectID, string eventAssignedUserID, string clientID, string contactID, string timestamp)
        {
            var project = _service.FindByID(projectID);
            var viewModel = new QuotationEventViewModel()
            {
                ID = Guid.NewGuid().ToString(),
                CompanyID = CurrentUser.CompanyID.ToString(),
                EventAssignedUserID = eventAssignedUserID ?? project.AssignedUserID,
                EventCreatorID = CurrentUser.Id,
                ClientID = clientID ?? "",
                ContactID = contactID ?? "",
                ProjectID = projectID.ToString(),
                Timestamp = !string.IsNullOrEmpty(timestamp) ? DateTime.Parse(timestamp) : DateTime.Now
            };
            return PartialView(viewModel);
        }


        [HttpPost]
        public ActionResult _NewEvent(QuotationEventViewModel viewModel, string MarkComplete, string MarkCompleteD1, string MarkCompleteD7, string MarkNotComplete)
        {
            if (ModelState.IsValid)
            {
                var service = new QuotationEventService<QuotationEventViewModel>(CurrentUser.Id, new QuotationEventMapper(), _service.DbContext);

                if (MarkComplete != null || MarkCompleteD1 != null || MarkCompleteD7 != null)
                    viewModel.Complete = true;
                else if (MarkNotComplete != null)
                    viewModel.Complete = false;

                //var model = mapper.MapToModel(viewModel);
                //model.CompanyID = CurrentUser.CompanyID;
                //model.CreatorID = CurrentUser.Id;
                service.Create(viewModel);

                if (MarkCompleteD1 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(1).ToString() } });
                }
                else if (MarkCompleteD7 != null)
                {
                    return Json(new { Status = "Success", CreateNew = new { QuotationID = viewModel.QuotationID, EventAssignedUserID = viewModel.EventAssignedUserID, ClientID = viewModel.ClientID, ContactID = viewModel.ContactID, Timestamp = viewModel.Timestamp.AddDays(7).ToString() } });
                }
                else
                {
                    return Json(new { Status = "Success" });
                }
            }
            else
            {
                return PartialView(viewModel);
            }
        }

    }
}