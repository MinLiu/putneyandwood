﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientEventsController : ServiceGridController<ClientEvent, ClientEventViewModel, ClientEventService<ClientEventViewModel>>
    {

        public ClientEventsController()
            : base(new ClientEventService<ClientEventViewModel>(Guid.Empty, null, null, new ClientEventMapper(), new SnapDbContext()))
        {

        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _service.CompanyID = CurrentUser.CompanyID;
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadList([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read().Where(i => i.ClientID == cltID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }
        
        

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string assignedUser, string createdUser, DateType dateType, string fromD, string toD, bool showComplete = true)
        {            
            var list = _service.Search(dateType, assignedUser, createdUser, fromD, toD, showComplete, false);

            var taskList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(t => _service.MapToViewModel(t));
            DataSourceResult result = new DataSourceResult { Data = taskList.ToList(), Total = list.Count() };
            return Json(result);

        }


        public override JsonResult Create(DataSourceRequest request, ClientEventViewModel viewModel)
        {

            viewModel = _service.Create(viewModel);

            // Update view model with Contact, User and Creator data following the creation of the new entity.
            var model = _service.FindByID(Guid.Parse(viewModel.ID));
            viewModel = _service.MapToViewModel(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        public override JsonResult Update(DataSourceRequest request, ClientEventViewModel viewModel)
        {
            viewModel = _service.Update(viewModel);
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        
    }

}