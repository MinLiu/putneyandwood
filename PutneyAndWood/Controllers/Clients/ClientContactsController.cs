﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientContactsController : ServiceGridController<ClientContact, ClientContactViewModel, ClientContactService<ClientContactViewModel> >
    {

        public ClientContactsController()
            : base(new ClientContactService<ClientContactViewModel>(Guid.Empty, null, null, new ClientContactMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read(cltID)
                .OrderBy(o => o.FirstName).ThenBy(o => o.LastName)
                .ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));           
        }


        public JsonResult ReadSearch([DataSourceRequest]DataSourceRequest request, string filterText)
        {     
            var list = _service.Search(CurrentUser.CompanyID, filterText);
            var conList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(c => _service.MapToViewModel(c));
            DataSourceResult result = new DataSourceResult { Data = conList.ToList(), Total = list.Count() };
            return Json(result);
        }
        

        public JsonResult DropDownClientContacts(Guid? cltID)
        {
            if (cltID == null)
                return Json(new List<ContactItemViewModel>(), JsonRequestBehavior.AllowGet);

            var list = _service.Read(cltID)
                .OrderBy(t => t.FirstName)
                .ThenBy(t => t.LastName)
                .ToArray()
                .Select(c =>
                    new ContactItemViewModel
                    {
                        ID = c.ID.ToString(),
                        ClientID = c.ClientID.ToString(),
                        Name = string.Format("{0} {1} {2}", c.Title, c.FirstName, c.LastName),
                        Email = c.Email,
                        Mobile = c.Mobile,
                        Position = c.Position,
                        Telephone = c.Telephone,
                        Title = c.Title,
                        FirstName = c.FirstName,
                        LastName = c.LastName
                    }
            ).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownProjectClientContacts(Guid? cltID, Guid projectID)
        {
            if (cltID == null)
                return Json(new List<ContactItemViewModel>(), JsonRequestBehavior.AllowGet);

            var project = new ProjectService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext()).FindByID(projectID);
            var contacts = new List<ClientContact>();

            if (project.ClientContact1 != null)
            {
                contacts.Add(project.ClientContact1);
            }

            if (project.ClientContact2 != null)
            {
                contacts.Add(project.ClientContact2);
            }

            if (project.ClientContact3 != null)
            {
                contacts.Add(project.ClientContact3);
            }

            if (project.ClientContact4 != null)
            {
                contacts.Add(project.ClientContact4);
            }

            var list = contacts.Where(c => c.ClientID == cltID)
                .Select(c => new ContactItemViewModel
                {
                    ID = c.ID.ToString(),
                    ClientID = c.ClientID.ToString(),
                    Name = string.Format("{0} {1} {2}", c.Title, c.FirstName, c.LastName),
                    Email = c.Email,
                    Mobile = c.Mobile,
                    Position = c.Position,
                    Telephone = c.Telephone,
                    Title = c.Title,
                    FirstName = c.FirstName,
                    LastName = c.LastName
                })
                .ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

}