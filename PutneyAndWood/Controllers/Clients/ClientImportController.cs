﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
//using Fruitful.Import;
using System.Threading.Tasks;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientImportController : BaseController
    {

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.EditCRM == false)
                Response.Redirect("/");
        }

        // GET: ProductImport
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Clients");
        }

        public ActionResult UploadClients()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadClients(HttpPostedFileBase upload)
        {
            var importService = new ClientImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Clients have been successfully imported.\n\nClients Updated: {1}\nClients Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;
            
            return View("UploadClients");
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadClientsAjax(HttpPostedFileBase upload)
        {
            var importService = new ClientImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            var response = new JsonResponse();
            if (result.Success)
            {
                response.Success = true;
                response.Message = string.Format("{0} Clients have been successfully imported.\n\nClients Updated: {1}\nClients Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            }
            else
            {
                response.Success = false;
                response.Message = result.Exception.Message;
            }

            return Json(response);
        }

        public ActionResult UploadContacts()
        {
            return View("UploadClients");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadContacts(HttpPostedFileBase upload)
        {
            var importService = new ClientContactImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Client Contacts have been successfully imported.\n\nClient Contacts Updated: {1}\nClient Contacts Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;
            
            return View("UploadClients");

        }

        public ActionResult UploadAddresses()
        {
            return View("UploadClients");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAddresses(HttpPostedFileBase upload)
        {
            var importService = new ClientAddressImportService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext());
            var result = importService.ImportExcelFile(upload);

            if (result.Success)
                ViewBag.SuccessMessage = string.Format("{0} Client Addresses have been successfully imported.\n\nClient Addresses Updated: {1}\nClient Addresses Added: {2}", result.NumberCreated + result.NumberUpdated, result.NumberUpdated, result.NumberCreated);
            else
                ViewBag.ErrorMessage = result.Exception.Message;
            return View("UploadClients");
        }

    }
}