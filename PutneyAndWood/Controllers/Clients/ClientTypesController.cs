﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientTypesController : ServiceGridController<ClientType, ClientTypeViewModel, ClientTypeService<ClientTypeViewModel>>
    {

        public ClientTypesController()
            : base(new ClientTypeService<ClientTypeViewModel>(Guid.Empty, null, new ClientTypeMapper(), new SnapDbContext()))
        {

        }
        

        public JsonResult DropDownClientTypes()
        {
            var tags = _service.Read()
                .OrderBy(t => t.Name).ToArray()
                .Select(t => _service.MapToViewModel(t)
            ).ToList();

            return Json(tags, JsonRequestBehavior.AllowGet);
        }

    }

}