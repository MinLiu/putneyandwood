﻿using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientAddressesController : ServiceGridController<ClientAddress, ClientAddressViewModel, ClientAddressService<ClientAddressViewModel>>
    {

        public ClientAddressesController()
            : base(new ClientAddressService<ClientAddressViewModel>(Guid.Empty, null, null, new ClientAddressMapper(), new SnapDbContext()))
        {  

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read(cltID).OrderBy(i => i.Name).ThenBy(i => i.Address1).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ClientAddressViewModel>().ToDataSourceResult(request));
        }
        

        public JsonResult DropDownClientAddresses(Guid? cltID)
        {
            if (cltID == null)
                return Json(new List<SelectItemViewModel>(), JsonRequestBehavior.AllowGet);

            var addresses = _service.Read(cltID).OrderBy(t => t.Name).ToArray()
                                                                        .Select(a => new SelectItemViewModel { ID = a.ID.ToString(), Name = a.ToShortLabel() })
                                                                        .ToList();

            //Insert main clt address at front
            var main = new ClientService<ClientViewModel>(CurrentUser.CompanyID, CurrentUser.Id, null, _service.DbContext).FindByID(cltID.Value);
            addresses.Insert(0, new SelectItemViewModel { ID = null, Name = main.ToAddressShortLabel() });
            return Json(addresses, JsonRequestBehavior.AllowGet);
        }

    }

}