﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientGridController : ServiceDeleteableGridController<Client, ClientViewModel, ClientService<ClientViewModel>>
    {

        public ClientGridController()
            : base(new ClientService<ClientViewModel>(Guid.Empty, null, new ClientMapper(), new SnapDbContext()))
        {

        }
        

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, Guid? type, Guid? tag)
        {
            //User passed parameters oo filter results
            var clients = _service.Search(filterText, type, tag);
            clients = ApplyQueryFiltersSort(request, clients, "Name");

            var clientList = clients.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _service.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = clientList.ToList(), Total = clients.Count() };
            return Json(result);

        }


        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var clients = _service.Search(null, null, null, true);
            var viewModels = clients.OrderBy(c => c.Name).ToList().Select(s => _service.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<ClientViewModel>().ToDataSourceResult(request));
        }
        
        

        public FileResult Export(string filterText, Guid? type, Guid? tag)
        {
            var export = _service.Export(filterText, type, tag, false);

            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user
        }


        public FileResult ExportContacts(string filterText, Guid? type, Guid? tag)
        {

            var export = new ClientContactService<ClientContactViewModel>(CurrentUser.CompanyID, null, CurrentUser.Id, null, _service.DbContext).Export(filterText, type, tag, false);
            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user

        }



        public FileResult ExportAddresses(string filterText, Guid? type, Guid? tag)
        {
            var export = new ClientAddressService<ClientAddressViewModel>(CurrentUser.CompanyID, null, CurrentUser.Id, null, _service.DbContext).Export(filterText, type, tag, false);
            return File(export.DataArray,   //The binary data of the XLS file
                        export.MimeType, //MIME type of Excel files
                        export.Filename);     //Suggested file name in the "Save as" dialog which will be displayed to the end user

        }


    }
    
}