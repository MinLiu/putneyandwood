﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientAttachmentsController : ServiceGridController<ClientAttachment, ClientAttachmentViewModel, ClientAttachmentService<ClientAttachmentViewModel>>
    {

        public ClientAttachmentsController()
           : base(new ClientAttachmentService<ClientAttachmentViewModel>(Guid.Empty, null, null, new ClientAttachmentMapper(), new SnapDbContext()))
        {
            //If larger then 5mbs then skip
            //_service.MaxContentLengthInMbs = 5;
            //_service.RelativeSavePath = "/Attachments/";
        }

       

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read(cltID).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }
        

        public ActionResult UploadClientAttachment(IEnumerable<HttpPostedFileBase> files, Guid cltID)
        {
            _service.ParentID = cltID;
            if (files != null)
            {                
                foreach (var file in files)
                {   
                    _service.UploadAttachment(file);
                }
            }

            return Content("");
        }

       
        [HttpPost]
        public ActionResult LinkClientAttachment(Guid cltID, string link)
        {
            _service.ParentID = cltID;
            _service.LinkAttachment(link);
            return JavaScript("");
        }
    }

}