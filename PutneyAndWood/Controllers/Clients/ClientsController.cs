﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientsController : ServiceEntityController<Client, ClientViewModel, ClientService<ClientViewModel>>
    {
      
        public ClientsController()
            : base(new ClientService<ClientViewModel>(Guid.Empty, null, new ClientMapper(), new SnapDbContext()))
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
            _service.CurrentUserID = CurrentUser.Id;
            _service.CompanyID = CurrentUser.CompanyID;

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        
        #region Main Clients Page

        // GET: Clients
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.ClientID = ID;

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }

        

        // GET: Events
        public ActionResult Events(string eventID = null)
        {            
            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            if (eventID != null)
            {
                ViewBag.EventID = eventID;
            }
            return View();
        }


        #endregion


        #region Edit Client Panel

        // GET: /New Client
        public ActionResult _NewClient()
        {
            return PartialView(new ClientViewModel { CompanyID = CurrentUser.CompanyID.ToString()  });             
        }


        // POST: /New Client
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewClient(ClientViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            _service.Create(viewModel);
            return Json(new { ID = viewModel.ID }); 
        }



        // GET: /Edit Client
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditClient(string ID)
        {
            try
            {
                var model = _service.FindByID(Guid.Parse(ID));
                var viewModel = _service.MapToViewModel(model);
                return PartialView(viewModel);

            }
            catch {
                return PartialView("_NewClient", new ClientViewModel());
            }

        }


        // POST: /Edit Client
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditClientDetails(ClientViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            _service.Update(viewModel);
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }


       


        #endregion


        public ActionResult _ContactsTab(ClientViewModel model)
        {
            return View(model);
        }

        // GET: /AddressesTab
        public ActionResult _AddressesTab(ClientViewModel model)
        {
            return View(model);
        }

        // GET: /Logs Tab
        public ActionResult _LogsTab(ClientViewModel model)
        {
            return View(model);
        }


        // GET: /ClientTags Tab
        public ActionResult _TagsTab(ClientViewModel model)
        {
            return View(model);
        }

        // GET: /Events Tab
        public ActionResult _EventsTab(ClientViewModel model)
        {
            return View(model);
        }


        // GET: /Categories Tab
        public ActionResult _CategoriesTab(ClientViewModel model)
        {
            return View(model);

        }
                
        // GET: /Attachments Tab
        public ActionResult _AttachmentsTab(ClientViewModel model)
        {
            return View(model);
        }

        public ActionResult _AddClientPopup(string OrderID, string Type, int Index = 0)
        {
            ViewBag.ID = OrderID;
            ViewBag.Type = Type;
            ViewBag.Index = Index.ToString();
            return PartialView(new ClientViewModel { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddClientPopup(ClientViewModel viewModel, string OrderID, string Type, string Index = "0")
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ID = OrderID;
                ViewBag.Type = Type;
                ViewBag.Index = Index;
                return PartialView(viewModel);
            }

            var client = _service.MapToModel(viewModel, false);
            client = _service.Create(client);
            //var client = _service.MapToModel(viewModel, false);

            if (Type == "Quotation")
            {
                var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                var model = oServ.Find(Guid.Parse(OrderID));
                model.ClientID = client.ID;
                model.LastUpdated = DateTime.Now;
                oServ.Update(model, new string[] { "ClientID", "LastUpdated" });
            }
            else if (Type == "Project")
            {
                var pServ = new ProjectService(_service.CompanyID, _service.CurrentUserID, new SnapDbContext());
                var model = pServ.FindByID(Guid.Parse(OrderID));
                switch (Index)
                {
                    case "1":
                        model.Client1ID = client.ID;
                        break;
                    case "2":
                        model.Client2ID = client.ID;
                        break;
                    case "3":
                        model.Client3ID = client.ID;
                        break;
                    case "4":
                        model.Client4ID = client.ID;
                        break;
                    case "5":
                        model.Consultant1ID = client.ID;
                        break;
                    case "6":
                        model.Consultant2ID = client.ID;
                        break;
                    case "7":
                        model.Consultant3ID = client.ID;
                        break;
                    case "8":
                        model.Consultant4ID = client.ID;
                        break;
                    default:
                        break;
                }
                pServ.Update(model);

            }
            
            return Json(new { Status = "Success", ID = viewModel.ID });
        }


        public ActionResult _AddClientAddressPopup(string OrderID, string Type, bool Delivery = false)
        {
            try
            {
                string clientID = "";
                string clientName = "";

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));

                    if (model.ClientID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Delivery = Delivery, Type = Type });
                    clientID = model.Client.ID.ToString();
                    clientName = model.Client.Name;
                }

                ViewBag.ID = OrderID;
                ViewBag.Delivery = Delivery;
                ViewBag.Type = Type;
                return PartialView(new ClientAddressViewModel { ClientID = clientID, Name = clientName });
            }
            catch 
            {
                return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddClientAddressPopup(ClientAddressViewModel viewModel, string OrderID, string Type, bool Delivery)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ID = OrderID;
                ViewBag.Delivery = Delivery;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            if (_service.FindByID(Guid.Parse(viewModel.ClientID)) != null)
            {
                var addressService = new ClientAddressService<ClientAddressViewModel>(_service.CompanyID, Guid.Parse(viewModel.ClientID), _service.CurrentUserID, new ClientAddressMapper(), _service.DbContext);
                var address = addressService.MapToModel(viewModel);
                address = addressService.Create(address);

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));
                    if (Delivery) model.DeliveryAddressID = address.ID;
                    else model.InvoiceAddressID = address.ID;
                    model.LastUpdated = DateTime.Now;
                    oServ.Update(model, new string[] { "DeliveryAddressID", "InvoiceAddressID", "LastUpdated" });
                }
            }
            
            return JavaScript("OnEditClientContactAddress();");
        }


        public ActionResult _AddClientContactPopup(string OrderID, string Type, string ClientID = "", string Index = "0")
        {

            if (Type == "Quotation")
            {
                var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                var model = oServ.Find(Guid.Parse(OrderID));

                if (model.ClientID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type });
                ClientID = model.Client.ID.ToString();
            }
            if (Type == "Project")
            {
                var pServ = new ProjectService(_service.CompanyID, _service.CurrentUserID, new SnapDbContext());
                var model = pServ.FindByID(Guid.Parse(OrderID));
                switch (Index)
                {
                    case "1":
                        if (model.Client1ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Client1ID.ToString();
                        break;
                    case "2":
                        if (model.Client2ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Client2ID.ToString();
                        break;
                    case "3":
                        if (model.Client3ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Client3ID.ToString();
                        break;
                    case "4":
                        if (model.Client4ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Client4ID.ToString();
                        break;
                    case "5":
                        if (model.Consultant1ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Consultant1ID.ToString();
                        break;
                    case "6":
                        if (model.Consultant2ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Consultant2ID.ToString();
                        break;
                    case "7":
                        if (model.Consultant3ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Consultant3ID.ToString();
                        break;
                    case "8":
                        if (model.Consultant4ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                        ClientID = model.Consultant4ID.ToString();
                        break;
                    default:
                        break;
                }
                pServ.Update(model);
            }
            
            ViewBag.ID = OrderID;
            ViewBag.Type = Type;
            ViewBag.Index = Index;
            return PartialView(new ClientContactViewModel { ClientID = ClientID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddClientContactPopup(ClientContactViewModel viewModel, string OrderID, string Type, string Index = "0")
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ID = OrderID;
                ViewBag.Type = Type;
                ViewBag.Index = Index;
                return PartialView(viewModel);
            }

            if (_service.FindByID(Guid.Parse(viewModel.ClientID)) != null)
            {
                var contactService = new ClientContactService<ClientContactViewModel>(_service.CompanyID, Guid.Parse(viewModel.ClientID), _service.CurrentUserID, new ClientContactMapper(), _service.DbContext);
                var contact = contactService.MapToModel(viewModel);
                contact = contactService.Create(contact);

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));
                    model.ClientContactID = contact.ID;
                    model.LastUpdated = DateTime.Now;
                    oServ.Update(model, new string[] { "ClientContactID", "LastUpdated" });
                }
                else if (Type == "Project")
                {
                    var pServ = new ProjectService(_service.CompanyID, _service.CurrentUserID, new SnapDbContext());
                    var model = pServ.FindByID(Guid.Parse(OrderID));
                    switch (Index)
                    {
                        case "1":
                            if (model.Client1ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ClientContact1ID = contact.ID;
                            break;
                        case "2":
                            if (model.Client2ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ClientContact2ID = contact.ID;
                            break;
                        case "3":
                            if (model.Client3ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ClientContact3ID = contact.ID;
                            break;
                        case "4":
                            if (model.Client4ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ClientContact4ID = contact.ID;
                            break;
                        case "5":
                            if (model.Consultant1ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ConsultantContact1ID = contact.ID;
                            break;
                        case "6":
                            if (model.Consultant2ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ConsultantContact2ID = contact.ID;
                            break;
                        case "7":
                            if (model.Consultant3ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ConsultantContact3ID = contact.ID;
                            break;
                        case "8":
                            if (model.Consultant4ID == null) return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
                            model.ConsultantContact4ID = contact.ID;
                            break;
                        default:
                            break;
                    }
                    pServ.Update(model);
                }
                

                return Json(new { Status = "Success", ID = contact.ID.ToString() });

            }

            return Json(new { Status = "Success" });
        }


        public ActionResult _EditClientPopup(string OrderID, string Type, string ClientID, string Index = "0")
        {
            ViewBag.ID = OrderID;
            ViewBag.Type = Type;
            ViewBag.Index = Index;
            
            try
            {
                Client client = null;

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));
                    client = _service.FindByID(model.ClientID.Value);
                }
                if (Type == "Opportunity")
                {
                    client = _service.FindByID(Guid.Parse(ClientID));
                }
                if (Type == "Project")
                {
                    var pServ = new ProjectService(_service.CompanyID, _service.CurrentUserID, new SnapDbContext());
                    var model = pServ.FindByID(Guid.Parse(OrderID));
                    switch (Index)
                    {
                        case "1":
                            client = _service.FindByID(model.Client1ID.Value);
                            break;
                        case "2":
                            client = _service.FindByID(model.Client2ID.Value);
                            break;
                        case "3":
                            client = _service.FindByID(model.Client3ID.Value);
                            break;
                        case "4":
                            client = _service.FindByID(model.Client4ID.Value);
                            break;
                        case "5":
                            client = _service.FindByID(model.Consultant1ID.Value);
                            break;
                        case "6":
                            client = _service.FindByID(model.Consultant2ID.Value);
                            break;
                        case "7":
                            client = _service.FindByID(model.Consultant3ID.Value);
                            break;
                        case "8":
                            client = _service.FindByID(model.Consultant4ID.Value);
                            break;
                    }
                }
                   
                var viewModel = _service.MapToViewModel(client);
                return PartialView(viewModel);
            }
            catch
            {
                return RedirectToAction("_AddClientPopup", new { OrderID = OrderID, Type = Type, Index = Index });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditClientPopup(ClientViewModel viewModel, string OrderID, string Type, int Index = 0)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ID = OrderID;
                ViewBag.Type = Type;
                ViewBag.Index = Index;
                return PartialView(viewModel);
            }
            
            _service.Update(viewModel);

            return Json(new { Status = "Success", ID = viewModel.ID });
            //return JavaScript("OnEditClientContactAddress();");
        }


        public ActionResult _EditClientAddressPopup(string OrderID, string Type, bool Delivery = false)
        {
            ViewBag.ID = OrderID;
            ViewBag.Delivery = Delivery;
            ViewBag.Type = Type;

            try
            {
                ClientAddress address = null;

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));

                    if (Delivery && model.DeliveryAddressID == null) return RedirectToAction("_EditClientPopup", new { OrderID = OrderID, Delivery = Delivery, Type = Type });
                    else if (!Delivery && model.InvoiceAddressID == null) return RedirectToAction("_EditClientPopup", new { OrderID = OrderID, Delivery = Delivery, Type = Type });

                    address = (Delivery) ? new ClientAddressService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.DeliveryAddressID) : new ClientAddressService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.InvoiceAddressID);
                }

                var viewModel = new ClientAddressMapper().MapToViewModel(address);
                return PartialView(viewModel);
            }
            catch
            {
                //return _AddCustomerAddress(quoteID, delivery);
                return RedirectToAction("_AddClientAddressPopup", new { OrderID = OrderID, Type = Type, Delivery = Delivery });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditClientAddressPopup(ClientAddressViewModel viewModel, string OrderID, string Type)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            var addressService = new ClientAddressService<ClientAddressViewModel>(_service.CompanyID, null, _service.CurrentUserID, new ClientAddressMapper(), _service.DbContext);
            addressService.Update(viewModel);
            
            return JavaScript("OnEditClientContactAddress();");
        }


        public ActionResult _EditClientContactPopup(string OrderID, string Type, string ClientContactID, string Index = "0")
        {
            ViewBag.ID = OrderID;
            ViewBag.Type = Type;

            try
            {
                ClientContact contact = null;

                if (Type == "Quotation")
                {
                    var oServ = new QuotationService<EmptyViewModel>(_service.CompanyID, _service.CurrentUserID, null, _service.DbContext);
                    var model = oServ.Find(Guid.Parse(OrderID));
                    contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ClientContactID);
                }
                
                if (Type == "Opportunity")
                {
                    contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(Guid.Parse(ClientContactID));
                }

                if (Type == "Project")
                {
                    var pServ = new ProjectService(_service.CompanyID, _service.CurrentUserID, new SnapDbContext());
                    var model = pServ.FindByID(Guid.Parse(OrderID));
                    switch (Index)
                    {
                        case "1":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ClientContact1ID);
                            break;
                        case "2":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ClientContact2ID);
                            break;
                        case "3":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ClientContact3ID);
                            break;
                        case "4":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ClientContact4ID);
                            break;
                        case "5":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ConsultantContact1ID);
                            break;
                        case "6":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ConsultantContact2ID);
                            break;
                        case "7":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ConsultantContact3ID);
                            break;
                        case "8":
                            contact = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, null, null, _service.DbContext).Find(model.ConsultantContact4ID);
                            break;
                    }
                }

                var viewModel = new ClientContactMapper().MapToViewModel(contact);
                return PartialView(viewModel);
            }
            catch
            {
                //return _AddCustomerContact(quoteID, delivery);
                return RedirectToAction("_AddClientContactPopup", new { OrderID = OrderID, Type = Type });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditClientContactPopup(ClientContactViewModel viewModel, string OrderID, string Type, int Index = 0)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                ViewBag.Index = Index;
                return PartialView(viewModel);
            }

            var addressService = new ClientContactService<ClientContactViewModel>(_service.CompanyID, null, _service.CurrentUserID, new ClientContactMapper(), _service.DbContext);
            addressService.Update(viewModel);

            return Json(new { Status = "Success", ID = viewModel.ID });
        }


        public JsonResult DropDownClients(string text, string cID)
        {
            if (text == null) text = "";

            var list = _service.Read()
                            .Where(t => t.CompanyID == CurrentUser.CompanyID && t.Deleted == false)
                            .Where(c => c.Name.ToLower().Contains(text) || text == "" || c.ID.ToString() == cID)
                            .OrderByDescending(t => t.ID.ToString() == cID)
                            .ThenBy(c => c.Name)
                            .Take(500)
                            .ToArray()
                            .Select(t => _service.MapToViewModel(t)
            ).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult DropDownClients_Virtualised([DataSourceRequest] DataSourceRequest request)
        {
            var clients = GetClients();

            // Apply filtering.
            if (request.Filters.Count > 0)
            {
                // For simplicity, assume that only one filter will be specified at a time and that
                // the filter type will be "contains".
                var filter = request.Filters[0].GetAllFilterDescriptors().First().Value.ToString();
                if (!String.IsNullOrEmpty(filter))
                {
                    clients = clients.Where(c => c.Name.ToLower().Contains(filter.ToLower()));
                }
            }

            // Store the total record count post filtering but pre paging.
            var clientCount = clients.Count();

            // Apply paging.
            clients = clients.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
            
            // Convert to view models.
            var clientVMs = clients.ToList().Select(c => _service.MapToViewModel(c)).ToList();

            return Json(new DataSourceResult { Data = clientVMs, Total = clientCount });
        }

        public ActionResult DropDownClients_ValueMapper(string[] values)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;
                var clients = GetClients().ToList();

                foreach (var client in clients)
                {
                    if (values.Contains(client.ID.ToString()))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<Client> GetClients()
        {
            var clients = _service.Read()
                               .Where(c => c.CompanyID == CurrentUser.CompanyID && c.Deleted == false)
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return clients;
        }

        public JsonResult DropdownClientEmails(Guid cltID)
        {

            var contactsService = new ClientContactService<EmptyViewModel>(_service.CompanyID, null, _service.CurrentUserID, null, _service.DbContext);

            //GEt Other addresses
            var emails = contactsService.Read().Where(a => a.ClientID == cltID && !string.IsNullOrEmpty(a.Email))
                    .OrderBy(a => a.Email)
                    .Select(a => new SelectItemViewModel { ID = a.ID.ToString(), Name = a.Email })
                    .ToList();

            //Insert main clt address at front
            var main = _service.Read().Where(a => a.ID == cltID && !string.IsNullOrEmpty(a.Email))
                                .Select(a => new SelectItemViewModel { ID = null, Name = a.Email });

            if (main.Count() > 0) emails.Insert(0, main.First());
                        

            return Json(emails, JsonRequestBehavior.AllowGet);
            
        }


        public ActionResult DropDownProjectClients([DataSourceRequest] DataSourceRequest request, Guid projectID)
        {
            var project = new ProjectService(CurrentUser.CompanyID, CurrentUser.Id, new SnapDbContext()).FindByID(projectID);
            var clients = new List<Client>();

            if (project.Client1 != null)
            {
                clients.Add(project.Client1);
            }
            if (project.Client2 != null)
            {
                clients.Add(project.Client2);
            }
            if (project.Client3 != null)
            {
                clients.Add(project.Client3);
            }
            if (project.Client4 != null)
            {
                clients.Add(project.Client4);
            }

            // Convert to view models.
            var clientVMs = clients.Distinct().ToList().Select(c => _service.MapToViewModel(c)).ToList();

            return Json(clientVMs, JsonRequestBehavior.AllowGet);
        }

    }
    
}