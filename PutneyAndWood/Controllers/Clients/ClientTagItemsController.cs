﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SnapSuite.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class ClientTagItemsController : ServiceGridController<ClientTagItem, ClientTagItemViewModel, ClientTagItemService<ClientTagItemViewModel>>
    {

        public ClientTagItemsController()
            : base(new ClientTagItemService<ClientTagItemViewModel>(Guid.Empty, null, null, new ClientTagItemMapper(), new SnapDbContext()))
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, Guid cltID)
        {
            var list = _service.Read(cltID).OrderBy(o => o.ClientTag.Name).ToList().Select(p => _service.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {           
            return Json(new List<ClientTagItemViewModel>().ToDataSourceResult(request));
        }
        
    }

}