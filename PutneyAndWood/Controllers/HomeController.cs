﻿using Kendo.Mvc.Extensions;
using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace SnapSuite.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            string path = System.IO.Directory.GetCurrentDirectory();

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Projects");

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
                        
            //return View();
        }


        public ActionResult Dashboard()
        {           
            return View();
        }

        public ActionResult _StockLogs()
        {
            return PartialView();
        }

        public ActionResult _ActivityLogs()
        {
            return PartialView();
        }
        
        public ActionResult SubscriptionInvalid()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult _404()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Oops()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Test()
        {
            var company = CurrentUser.Company;
            var productService = new ProductService<EmptyViewModel>(company.ID, null, null, new TestDbContext());
            var p1 = productService.Create(new Product { ProductCode = "TEST123", Description = "DEstroy me.", CurrencyID = 1 });

            var costService = new ProductCostService<EmptyViewModel>(company.ID, p1.ID, null, null, productService.DbContext);
            costService.Create(new ProductCost { ProductID = p1.ID, CurrencyID = 1, Cost = 89.99m, BreakPoint = 2m });

            productService.Destroy(p1);

            var count = productService.Read().Where(i => i.ID == p1.ID).Count();

            return View();
        }


        public ActionResult LoginMisuse()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Inactive()
        {
            return View();
        }


        public ActionResult UpdateResults()
        {
            var controller = new QuotationsController();
            var jobController = new QuotationsController();

            var ids = new QuotationRepository().Read().Select(q => q.ID);


            foreach(var i in ids){
                //QuotationsController.RecalulateTotals(i);
            }

            return Content("Complete");
        }
        
        /*
        public ActionResult CreateOpp()
        {
            var companies = new CompanyRepository().Read();

            var oppTypeRepo = new OpportunityTypeRepository();

            int count = 0;
            foreach (var company in companies)
            {
                if (oppTypeRepo.Read().Where(x => x.CompanyID == company.ID).Count() == 0)
                {
                    var oppType = new OpportunityType() { CompanyID = company.ID, Name = "Default", LabelValue1 = "Importance", LabelValue2 = "Probability" };
                    oppTypeRepo.Create(oppType);

                    var oppStatusRepo = new OpportunityStatusRepository();
                    List<OpportunityStatus> statuses = new List<OpportunityStatus>();
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 1, OpportunityTypeID = oppType.ID, Name = "Cold" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 2, OpportunityTypeID = oppType.ID, Name = "Warm" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 3, OpportunityTypeID = oppType.ID, Name = "In Quote" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 4, OpportunityTypeID = oppType.ID, Name = "Quote Sent" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 5, OpportunityTypeID = oppType.ID, Name = "Won" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 6, OpportunityTypeID = oppType.ID, Name = "Lost" });
                    statuses.Add(new OpportunityStatus() { CompanyID = company.ID, SortPos = 7, OpportunityTypeID = oppType.ID, Name = "No Interest" });
                    oppStatusRepo.Create(statuses);
                    count++;
                }
            }

            return Content(String.Format("Create {0} Opportunity Types.", count));
        }
        */

        public ActionResult CleanFiles()
        {
            var fileService = new FileService(new SnapDbContext());
            fileService.CleanUpFiles();

            return Content("Success!");
        }
        
    }

    
}