﻿using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;

namespace SnapSuite.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult _QuotationsPanel()
        {           
            var _service = new QuotationService<QuotationGridViewModel>(CurrentUser.CompanyID, CurrentUser.Id, new QuotationGridMapper(), new SnapDbContext());                
            var recent = _service.Read().OrderByDescending(i => i.LastUpdated).Take(3).ToArray().Select(i => _service.MapToViewModel(i)).ToList();
            
            //Get stats for the past 30 days
            DateTime fromDate = DateTime.Today.AddDays(-30);
            DateTime toDate = DateTime.Today.AddDays(1).AddSeconds(-1);

            var stats = new GenericService<QuotationStatus>(_service.DbContext).Read().OrderBy(i => i.SortPos).ToArray()
                            .Select(s => new DashboardChartValueViewModel
                            {      
                                Name = s.Name,
                                HexColour = s.HexColour,
                                Count = _service.Read().Where(i => i.StatusID == s.ID).Count(),
                                SortPos = s.ID
                            }
                            ).ToList();

            var dashboardViewModel = new QuotationsDashboardViewModel { RecentQuotations = recent, Stats = stats };

            return PartialView(dashboardViewModel);
        }

        public ActionResult _BlogPanel()
        {
            try
            {
                var reader = XmlReader.Create(@"https://quikflw.wordpress.com/feed/");
                var feed = SyndicationFeed.Load<SyndicationFeed>(reader);
                return PartialView(feed.Items.Where(x => !x.Categories.Where(c => c.Name == "Software Analysis").Any()).OrderByDescending(x => x.PublishDate).Take(10).ToList());
            }
            catch
            {
                return PartialView(new List<SyndicationItem>());
            }
        }

 
    }
}
