﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kendo.Mvc.UI
{
    public class KendoDataSourceResult : Kendo.Mvc.UI.DataSourceResult
    {
        public string TotalNet { get; set; }
    }
}