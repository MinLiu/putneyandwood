﻿$.ajaxSetup ({
   // Disable caching of AJAX responses
   cache: false
});


function KendoGridDatabound(e) {

    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() === 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg;
        if (options.pageable.messages !== undefined) {
            msg = options.pageable.messages.empty;
        }
        if (!msg) msg = "No Records Found"; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");

        grid.pager.element.hide();
    }
    else {
        _kendoGridHandlePagingDataBound(grid);
    }

}

function _kendoGridHandlePagingDataBound(grid) {

    var pages = grid.dataSource.total() / grid.dataSource.pageSize();
    if (grid.dataSource.pageSize() === null) {
        //if the grid doesn't have a page size (paging not set) then do nothing
    }
    else if (grid.dataSource.page() > 1 && grid.dataSource.data().length <= 0) {
        //The grid page happens to not 1 when the total returned is zero
        //then rest back to page one then do another read.
        grid.dataSource.page(1);
        grid.dataSource.read();
    }
    else if (grid.dataSource.total() > 10) {
        //The total items is lager then 15 then show pager anyway.
        grid.pager.element.show();
    }
    else if (pages < 1) {
        //If the page number is less 1 then hide pager.
        grid.pager.element.hide();
    }
    else {
        grid.pager.element.show();
    }
}

function ProductsKendoGridDatabound(e) {

    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() === 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();        
        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row'><td style='text-align:center;' colspan='" + colCount + "'><h1 style='color:lightgray; margin:40px;'>No Products Found</h1><a href='/Products/NewProduct' target='_blank' class='btn btn-sm btn-info'>Click here to create a product.</a><br /><br /></td></tr>");

        grid.pager.element.hide();
    }
    else {
        _kendoGridHandlePagingDataBound(grid);
    }

}


function KendoGridSave(e) {
    var grid = e.sender;
    grid.refresh();
}




$(document).ready(function () {
    SetKendoNumericTextBoxFocus();
});

$(document).ajaxSuccess(function () {
    SetKendoNumericTextBoxFocus();
});

function SetKendoNumericTextBoxFocus() {
    $("input[data-role='numerictextbox']").focus(function () {
        if ($(this).data("kendoNumericTextBox").value() === 0) {
            $(this).data("kendoNumericTextBox").value("");
        }
    });

    $("input[data-role='numerictextbox']").focusout(function () {
        if ($(this).data("kendoNumericTextBox").value() === null) {
            $(this).data("kendoNumericTextBox").value(0);
        }
    });
}

// Set default color picker to HSV
kendo.ui.editor.ColorTool.prototype.options.palette = null;



function convertValues(value) {
    var data = {};
    value = $.isArray(value) ? value : [value];
    for (var idx = 0; idx < value.length; idx++) { data["values[" + idx + "]"] = value[idx]; }
    return data;
}

function ChangeNewLine(text) {
    if(text === null) return '';

    var regexp = new RegExp('\n', 'g');
    return text.replace(regexp, '<br />');
}

$(document).ready(function(){
  $('.dropdown-submenu a.option').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});


function customFieldTemplate(customFields) {
  var template = "";

  for(var i=0; i<customFields.length; i++){    
     var info = customFields[i];  
    if(info.Show)
        template += kendo.format("<strong>{0}:</strong> {1}&nbsp;&nbsp", info.Label, (info.Value === null) ? "" : info.Value);
   }
   
   return template;
}

function addCustomFieldValue(customFields, name) {
  
  for(var i=0; i<customFields.length; i++){    
    var info = customFields[i];  
    if(info.Name === name) return (info.Value === null) ? "" : info.Value;
   }
   
   return "";
}

function MakeDownloadSafe(value) {
    return value.replace(/[\\\/\:\*\?\"\<\>\|\%\+\&\'\#]/gi,'_');
 }


