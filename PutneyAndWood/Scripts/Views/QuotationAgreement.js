﻿var _latitude = "Unknown";
var _longitude = "Unknown";
var _city = "Unknown";
var _country = "Unknown";

//Get GPS data
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
        _latitude = position.coords.latitude;
        _longitude = position.coords.longitude;

        $.ajax({
            type: 'GET',
            dataType: "json",
            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ position.coords.latitude+","+ position.coords.longitude+"&sensor=false",
            data: {},
            success: function(data) {
                $('#city').html(data);
                $.each( data['results'],function(i, val) {
                    $.each( val['address_components'],function(i, val) {
                        if (val['types'] == "locality,political") {
                            if (val['long_name']!="") { _city = val['long_name']; }
                        }

                        if (val['types'] == "country,political") {
                            if (val['long_name']!="") { _country = val['long_name']; }
                        }
                    });
                });
            },
            error: function () { _city = val['long_name']; _country = val['long_name']; }
        });
    });
}

$("#btnSubmitAgreement" ).dblclick(function(e) {
    e.preventDefault();
    return false;
});

$('#SubmitAgreementForm').submit(function (e) {

    if($('#Accept').is(":checked") == false) {
        alert("Please tick the checkbox stating you accept the terms and conditions before submitting.");
        return false;
    }

    var $divSuccess = $("#divSuccessMessage");
    var $divError = $("#divErrorMessage");
    var $btnSubmit = $("#btnSubmitAgreement");
    var $btnProgress = $("#btnProgress");

    $divSuccess.hide();
    $divError.hide();

    var tabIndex = $(".tab-pane.fade.active").attr("tabindex");
    var data = new FormData(this);
    data.append('tabIndex', tabIndex);

    if(tabIndex == 0) {
        if(!signatureDrawReady()){
            alert("Please draw your sigature on the box provided.");
            e.preventDefault();
            return false;
        }
        else{
            var canvas = document.getElementById("drawSignature"); // save canvas image as data url (png format by default)
            var imageData = canvas.toDataURL("image/png");
            imageData = imageData.replace('data:image/png;base64,', '');
            data.append('signatureFile', imageData);
        }
        try { data.delete('uploadSignature'); } catch(ex){ }
    }
    else if(tabIndex == 1) {
        if(!signatureTypeReady()){
            alert("Please type your name in the field and create a signature.");
            e.preventDefault();
            return false;
        }
        else {
            var canvas = document.getElementById("typeSignature"); // save canvas image as data url (png format by default)
            var imageData = canvas.toDataURL("image/png");
            imageData = imageData.replace('data:image/png;base64,', '');
            data.append('signatureFile', imageData);
        }
        try { data.delete('uploadSignature'); } catch(ex){ }
    }
    else if(tabIndex == 2) {
        if(!$('#uploadSignature')[0].files[0]) {
            alert("Please upload a signature image.");
            e.preventDefault();
            return false;
        }
        else {
            data.append('uploadSignature', $('#uploadSignature')[0].files[0]);
        }
    }

    //Get Device Information
    data.append('DeviceInformation', navigator.userAgent);
    data.append('Latitude', _latitude);
    data.append('Longitude', _longitude);
    data.append('City', _city);
    data.append('Country', _country);

    $btnSubmit.hide();
    $btnProgress.show();

    $.ajax({
        async: true,
        url: '/QuotationAgreements/_AcceptReject/',
        type: 'POST',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            $divError.hide();
            location.reload(true);
        },
        error: function (request, status, error) {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            $btnSubmit.show();
            $btnProgress.hide();

            if (error) {
                $divError.html("<strong>Error:</strong> " + error).show();
                alert(error);
            }
            else  {
                $divError.html("<strong>Error:</strong> Failed to submit form. Please try again.<br />If this issue continues please contact us.<br />Code: " + request.status).show();
            }
        }
    });

    e.preventDefault();
    return false;

});