﻿

//Important:
//---------------------
//The global variables are declared and set on the main edit page.
// - quoteID
// - sectionID

function Refresh() {
    RefreshHeader();
    RefreshItems();
    KeepScrollPosition();
}

function RefreshHeader() {
    setTimeout(function () {
        var url = '/QuotationReview/_HeaderURL?ID=' + quoteID + '&Jasq=' + jasq;
        $("#div_Header").load(url);
    }, 500);
}

function RefreshSections() {
    setTimeout(function () {
        var url = '/QuotationReview/_ItemsTabURL?ID=' + quoteID + '&Jasq=' + jasq;
        var ts = $("#Items_Tabstrip").data("kendoTabStrip");
        var item = ts.contentElement(0);
        $(item).load(url);
        KeepScrollPosition();
    }, 200);
}

function RefreshItems() {

    $('[id*="GridItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/QuotationReview/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}

function RefreshSectionTotals() {

    $('[id*="GridItems"]').each(function () {
       $('[id*="divSectionTotal"]').each(function () {
            var url = '/QuotationReview/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}


function OnGridItemsCancel(e) {
    RefreshItems();
}

function KeepScrollPosition() {
    $("html, body").animate({ scrollTop: window.pageYOffset }, 100);
}

function GoToBottomPage() {
    //Scroll to the bottom of page.
    //Delay the scroll to stop conflict with GridCreated scroll animation.
    setTimeout(function () {
        $("html, body").animate({ scrollTop: $(document).height() }, 100);
    }, 130);
}

function OnGridItemsRequestEnd(e) {
    //RequestEnd handler code
    if (e.type !== 'read') {
        Refresh();
    }
}

function GridItemsDatabound(e) {
    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() === 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg = options.pageable.messages.empty;
        if (!msg) msg = "No Records Found."; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row no-drag'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");
    }

    //If the page number is less 1 then hide pager.
    var pages = grid.dataSource.total() / grid.dataSource.pageSize();
    if (pages < 1) {
        grid.pager.element.hide();
    }

    /*
    //Selects all delete buttons
    $("#GridItems tbody tr .k-grid-delete").each(function () {
        var currentDataItem = $("#GridItems").data("kendoGrid").dataItem($(this).closest("tr"));

        //Check in the current dataItem if the row is deletable
        if (currentDataItem.IsKitItem) {
            $(this).remove();
        }
    })
    */
    SetColours(grid);    
}

function SetColours(grid) {

    var rows = grid.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        try {
            var row = $(rows[j]);
            var dataItem = grid.dataItem(row);

            if (dataItem.get("IsKitItem") === true) {
                row.css('background-color', 'LightGrey');
                row.css('font-style', 'italic');
            }

            if (dataItem.get("Quantity") <= 0 || dataItem.get("ParentQuantity") <= 0) {
                row.css('color', 'grey');
            }

            if (dataItem.get("IsPriceBreak") === true) {
                row.addClass("no-drag");
                row.css('background-color', 'LightBlue');
                row.css('font-style', 'italic');
            }
            
            if(dataItem.get("IsPriceBreak") === true || dataItem.get("IsRequired") === true) {
                row.find(".k-grid-edit").each(function () { $(this).remove(); });
                row.find(".k-grid-delete").each(function () { $(this).remove(); });
            }
            
        }
        catch (err) { }
    }

}

function OnGridItemsEdit(e) {

    //Hide input based on the type of line item
    if (e.model.IsComment || e.model.ID === 0) {
        e.container.find("input[name='Quantity']").each(function () { $(this).hide() });
    }
    if (e.model.HidePrices) {
        //e.container.find("input[name='Quantity']").each(function () { $(this).hide() });
    }
    
    setTimeout(function () {
        SetColours(e.sender);
    }, 40);
    

}

