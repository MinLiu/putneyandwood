﻿

//Important:
//---------------------
//The global variables are declared and set on the main edit page.
// - quoteID
// - sectionID

function Refresh() {
    KeepScrollPosition();
}

function RefreshHeader() {
    setTimeout(function () {
        var url = '/Projects/_Header?projectID=' + projectID;
        $("#div_Header").load(url);
    }, 500);
}

function AddClient(index) {
    var url = '/Clients/_AddClientPopup?OrderID=' + projectID + "&Type=Project" + "&Index=" + index;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClientContact(index) {
    var url = '/Clients/_AddClientContactPopup?OrderID=' + projectID + "&Type=Project" + "&Index=" + index;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClient(index) {
    var url = '/Clients/_EditClientPopup?OrderID=' + projectID + "&Type=Project" + "&Index=" + index;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClientContact(index) {
    var url = '/Clients/_EditClientContactPopup?OrderID=' + projectID + "&Type=Project" + "&Index=" + index;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function HideViews() {
    $("#SlideContent_AddProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCostProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustomProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_EditQuoteItem").animate({ height: '0%' }, { queue: false, duration: 300 });
    setTimeout(function () {
        $("#SlideContent_AddProducts").fadeOut();
        $("#SlideContent_AddCostProducts").fadeOut();
        $("#SlideContent_AddCustom").fadeOut();
        $("#SlideContent_AddCustomProducts").fadeOut();
        $("#Button_Close_Content").fadeOut();
        $("#Content_ItemPhoto").fadeOut(); $("#Content_ItemPhoto").css("max-width", "600px");        
        $("#SlideContent_EditQuoteItem").fadeOut();
        $("#div_itemPhoto").html("");
        $("#div_AddCustom").empty();
    }, 300);
    KeepScrollPosition();
}

function KeepScrollPosition() {
    $("html, body").animate({ scrollTop: window.pageYOffset }, 100);
}

function GoToBottomPage() {
    //Scroll to the bottom of page.
    //Delay the scroll to stop conflict with GridCreated scroll animation.
    setTimeout(function () {
        $("html, body").animate({ scrollTop: $(document).height() }, 100);
    }, 130);
}


function ShowSavedMessage() {
    $("#div_Saved").fadeIn();
    $(".field-validation-error").each(function(){ $(this).remove(); })
    setTimeout(function () { $("#div_Saved").fadeOut(); }, 2000);
}


function OnGridItemsRequestEnd(e) {
    //RequestEnd handler code
    if (e.type !== 'read') {
        Refresh();
    }
}

function OnEditClientContactAddress() { HideViews(); RefreshHeader(); }
function OnEditClientSuccess() { HideViews(); }
function OnAddClientContactSuccess() { HideViews(); RefreshHeader(); }
function OnAddClientSuccess() { HideViews(); }
