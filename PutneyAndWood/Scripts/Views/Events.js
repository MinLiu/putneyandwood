﻿

function NewEvent(projectID, quotationID, clientID, contactID) {
        $.ajax({
            url: '/Events/_NewEvent',
            type: 'GET',
            data: {
                projectID: projectID,
                quotationID: quotationID,
                clientID: clientID,
                contactID: contactID
            },
            success: function(response) {
                $('#Modal1 .modal-body').html(response);
                $('#Modal1').modal('show');
            }
        });
    }

    function EditEvent(id) {
        $.ajax({
            url: '/Events/_EditEvent',
            type: 'GET',
            data: {
                eventID: id
            },
            success: function(response) {
                $('#Modal1 .modal-body').html(response);
                $('#Modal1').modal('show');
            }
        });
    }

    function OnSaveEventSuccess(e) {
        $('#GridEvents').data('kendoGrid').dataSource.read();
        if (e.Status == "Success") {
            if (e.CreateNew != null) {
                $.ajax({
                    url: '/Events/_NewEvent',
                    type: 'GET',
                    data: {
                        projectID: e.CreateNew.EventProjectID,
                        quotationID: e.CreateNew.QuotationID,
                        eventAssignedUserID: e.CreateNew.EventAssignedUserID,
                        clientID: e.CreateNew.ClientID,
                        contactID: e.CreateNew.ContactID,
                        timestamp: e.CreateNew.Timestamp,
                    },
                    success: function(response) {
                        $('#Modal1 .modal-body').html(response);
                        $('#Modal1').modal('show');
                    }
                });
            }
            else {
                $('#Modal1').modal('hide');
            }
        }
    }