

function signatureTypeCapture(_width, _height) {
    var textInput = document.getElementById("txt_Signature");    
	var canvas = document.getElementById("typeSignature");
	var context = canvas.getContext("2d");
	canvas.width = _width;
	canvas.height = _height;
    context.font = '40pt Satisfy';
    context.textAlign = 'center';
    context.fillStyle = 'white';
	context.fillRect(0, 0, canvas.width, canvas.height);
	{ 	//functions

        function redrawText() {
          var x = canvas.width / 2;
          var y = canvas.height / 2 + 20;
          context.fillStyle = 'white';
          context.fillRect(0, 0, canvas.width, canvas.height);   
          context.fillStyle = 'black';      
          context.fillText(textInput.value, x, y);
        };
	}

    textInput.addEventListener("keyup", redrawText);
}

function signatureTypeSave() {

	var canvas = document.getElementById("typeSignature");// save canvas image as data url (png format by default)
	var dataURL = canvas.toDataURL("image/png");
	document.getElementById("saveSignature").src = dataURL;
};

function signatureTypeReady() {
    console.log("Signature length: " + document.getElementById("txt_Signature").value.length);
    return document.getElementById("txt_Signature").value.length > 1;    
}

