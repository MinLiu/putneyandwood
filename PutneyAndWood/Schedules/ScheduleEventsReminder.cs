﻿using System;
using Quartz;
using SnapSuite.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using RazorEngine;
using RazorEngine.Templating;
using System.Data.Entity;
using System.IO;
using System.Text;
using System.Web.Hosting;

namespace SnapSuite.Schedule
{
    [DisallowConcurrentExecution]
    public class ScheduleEventsReminder : IJob
    {
        protected static string EmailTemplateFolderRootPath { get { return @"\Views\\Email Templates\"; } }
        public Task Execute(IJobExecutionContext context)
        {
            var dbContext = new SnapDbContext();
            var eventService = new EventService<EmptyViewModel>(null, null, dbContext);

            var inCompleteEvents = eventService.Read().Where(x => x.Complete == false).OrderBy(x => x.Timestamp).ToList();

            var recipientEventList = new Dictionary<string, List<QuotationEvent>>();
            foreach (var ev in inCompleteEvents)
            {
                if (ev.AssignedUser != null)
                {
                    var key = ev.AssignedUser.Email;
                    if (recipientEventList.ContainsKey(key)) recipientEventList[key].Add(ev);
                    else recipientEventList.Add(key, new List<QuotationEvent>() { ev });
                }
            }

            var emailer = new Fruitful.Email.Emailer(dbContext);
            foreach (var recipientEvent in recipientEventList)
            {
                var email = recipientEvent.Key;
                var events = recipientEvent.Value.Distinct().ToList();
                var message = Engine.Razor.RunCompile(GetEmailTemplate("_IncompleteEvents.cshtml"), Guid.NewGuid().ToString(), typeof(List<QuotationEvent>), events);
                var cc = "barry.putney@putneyandwood.co.uk";
                emailer.SendEmail(email, "Your events reminder", message, cc);
            }

            return Task.FromResult<object>(null);
        }


        protected virtual string GetEmailTemplate(string templateName)
        {
            //var templateFilePath = Path.Combine(ServerRootPath, EmailTemplateFolderRootPath, EmailTemplateFolderName, templateName);
            var templateFilePath = HostingEnvironment.MapPath(Path.Combine(EmailTemplateFolderRootPath, templateName));

            if (File.Exists(templateFilePath))
            {
                var template = File.ReadAllText(templateFilePath);
                return template;
            }

            return "";
        }
    }
}
