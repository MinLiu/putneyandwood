﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;

namespace SnapSuite.Schedule
{
    public class Scheduler
    {
        private static IScheduler sched;

        public static async Task ScheduleStart()
        {
            try
            {
                // Construct a scheduler factory
                ISchedulerFactory schedFact = new StdSchedulerFactory();

                // Get a scheduler
                sched = await schedFact.GetScheduler();
                await sched.Start();

                await sched.ScheduleJob(ReminderScheduleSetting.Job, ReminderScheduleSetting.Trigger);
            }
            catch (Exception e) { }
        }

        private static ScheduleSetting ReminderScheduleSetting = new ScheduleSetting()
        {
            Job = JobBuilder.Create<ScheduleEventsReminder>()
                    .WithIdentity("Job_EventsRemider")
                    .Build(),
            Trigger = TriggerBuilder.Create()
                    .WithIdentity("Trigger_EventsRemider")
                    .WithCronSchedule("0 0 8 ? * TUE")
                    .StartNow()
                    .Build()
        };

        public class ScheduleSetting
        {
            public IJobDetail Job { get; set; }
            public ITrigger Trigger { get; set; }
        }

    }
}