﻿using Xero.Api.Infrastructure.Interfaces;

namespace SnapSuite.Xero
{
    public class ApiUser : IUser
    {
        public string Name { get; set; }
        public string OrganisationId { get; set; }
    }
}