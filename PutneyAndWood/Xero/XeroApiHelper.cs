﻿using System;
using Xero.Api.Core;
using Xero.Api.Example.Applications.Partner;
using Xero.Api.Example.Applications.Public;
using Xero.Api.Infrastructure.Interfaces;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using SnapSuite.Xero;
using System.Configuration;
using SnapSuite.Models;

namespace SnapSuite.Xero
{
    public class ApplicationSettings
    {
        public string BaseApiUrl { get; set; }
        public XeroConsumer Consumer { get; set; }
        public object Authenticator { get; set; }
    }

    public static class XeroApiHelper
    {
        private static ApplicationSettings _applicationSettings;

        static XeroApiHelper()
        {
            // Refer to README.md for details
            var callbackUrl = ConfigurationManager.AppSettings["XeroCallbackUrl"];
            var memoryStore = new MemoryAccessTokenStore();
            var requestTokenStore = new MemoryRequestTokenStore();
            var baseApiUrl = ConfigurationManager.AppSettings["XeroBaseURL"];

            // Consumer details for Application
            var consumerKey = ConfigurationManager.AppSettings["XeroConsumerKey"];
            var consumerSecret = ConfigurationManager.AppSettings["XeroConsumerSecret"];

            // Signing certificate details for Partner Applications
            //var signingCertificatePath = @"C:\Dev\your_public_privatekey.pfx";
            //var signingCertificatePassword = "Your_signing_cert_password - leave empty if you didn't set one when creating the cert";

            // Public Application Settings
            var publicConsumer = new XeroConsumer(consumerKey, consumerSecret);

            var publicAuthenticator = new PublicMvcAuthenticator(baseApiUrl, baseApiUrl, callbackUrl, memoryStore,
                publicConsumer, requestTokenStore);

            var publicApplicationSettings = new ApplicationSettings
            {
                BaseApiUrl = baseApiUrl,
                Consumer = publicConsumer,
                Authenticator = publicAuthenticator
            };

            // Partner Application Settings
            //var partnerConsumer = new XeroConsumer(consumerKey, consumerSecret);

            //var partnerAuthenticator = new PartnerMvcAuthenticator(baseApiUrl, baseApiUrl, callbackUrl,
            //        memoryStore, signingCertificatePath, 
            //        partnerConsumer, requestTokenStore);

            //var partnerApplicationSettings = new ApplicationSettings
            //{
            //    BaseApiUrl = baseApiUrl,
            //    Consumer = partnerConsumer,
            //    Authenticator = partnerAuthenticator
            //};

            // Pick one
            // Choose what sort of application is appropriate. Comment out the above code (Partner Application Settings/Public Application Settings) that are not used.

            _applicationSettings = publicApplicationSettings;
            //_applicationSettings = partnerApplicationSettings;
        }

        public static ApiUser User(Company company)
        {
            return new ApiUser { Name = company.ID.ToString() };
        }

        public static ApiUser User(User currentUser)
        {
            return User(currentUser.Company);
        }

        public static IConsumer Consumer()
        {
            return _applicationSettings.Consumer;
        }

        public static IMvcAuthenticator MvcAuthenticator()
        {
            return (IMvcAuthenticator)_applicationSettings.Authenticator;
        }

        public static IXeroCoreApi CoreApi(User currentUser)
        {
            if (_applicationSettings.Authenticator is IAuthenticator)
            {
                return new XeroCoreApi(_applicationSettings.BaseApiUrl, _applicationSettings.Authenticator as IAuthenticator,
                    _applicationSettings.Consumer, User(currentUser), new DefaultMapper(), new DefaultMapper());
            }

            return null;
        }
    }
}