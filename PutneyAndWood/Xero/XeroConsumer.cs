﻿using Xero.Api.Infrastructure.Interfaces;

namespace SnapSuite.Xero
{
    public class XeroConsumer : IConsumer
    {
        // For serialization
        public XeroConsumer()
        {
        }

        public XeroConsumer(string consumerKey, string consumerSecret)
        {
            ConsumerKey = consumerKey;
            ConsumerSecret = consumerSecret;
        }

        public string ConsumerKey { get; internal set; }
        public string ConsumerSecret { get; internal set; }
    }
}
